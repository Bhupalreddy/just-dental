package hCue.Hcue;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.database.Cursor;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hCue.Hcue.DAO.ReminderDetailsDO;
import hCue.Hcue.adapters.RemindersDbAdapter;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Preference;

public class OnAlarmReceiver extends BroadcastReceiver {

	private static final String TAG = ComponentInfo.class.getCanonicalName();

	Context context;
	Preference preference;

	@Override
	public void onReceive(Context context, Intent intent) {

		this.context = context;
		preference = new Preference(context);

		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			Intent service = new Intent(context, IndefiniteService.class);
			context.startService(service);
			Log.v("BootCompleteReceiver", " __________BootCompleteReceiver _________");

		}else{
			Log.d(TAG, "Received wake up from alarm manager.");
			long rowid = intent.getExtras().getLong(RemindersDbAdapter.KEY_ROWID);

//			WakeReminderIntentService.acquireStaticLock(context);

			/*Intent i = new Intent(context, ReminderService.class);
			i.putExtra(RemindersDbAdapter.KEY_ROWID, rowid);
			context.startService(i);*/

			doReminderWork(rowid);
		}
	}


	void doReminderWork(long rowid) {
//		Long rowId = intent.getExtras().getLong(RemindersDbAdapter.KEY_ROWID);
		String DATE_FORMAT = "yyyy-MM-dd";

		Log.d(TAG, "doReminderWork() called.");

//		preference.saveBooleanInPreference("reminderDismissed",false);
//		preference.commitPreference();
		ApplicationConstants.getDbAdapter(context);

		try {
			Cursor cursor = ApplicationConstants.dbAdapter.checkIfRecordExist(rowid);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				String date = cursor.getString(cursor.getColumnIndex(RemindersDbAdapter.KEY_DATE_TIME));
				String time = cursor.getString(cursor.getColumnIndex(RemindersDbAdapter.KEY_TIME));

				Calendar cal = Calendar.getInstance(Locale.getDefault());
				cal.set(Calendar.SECOND, 0);

				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
				cal  = Calendar.getInstance();
				cal.setTime(sdf.parse(date));

				int hour = Integer.parseInt(time.split(":")[0]);
				int minutes = Integer.parseInt(time.split(":")[1].split(" ")[0]);
				String dayMode = (time.split(":")[1].split(" ")[1]);
				if (dayMode.equalsIgnoreCase("PM")){
					hour = hour + 12;
				}

				/*SimpleDateFormat timeDF = new SimpleDateFormat("hh:mm a");
				timeDF.setTimeZone(TimeZone.getDefault());
				Date tDate = null;
				try {
					tDate = timeDF.parse(time);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if(tDate == null)
					return;*/

				cal.set(Calendar.HOUR_OF_DAY, hour);
				cal.set(Calendar.MINUTE, minutes);
				cal.set(Calendar.SECOND, 0);

				Calendar curCalendar = Calendar.getInstance(Locale.getDefault());
				curCalendar.set(Calendar.SECOND, 0);

				if (cal.getTimeInMillis() + (60*1000) >= curCalendar.getTimeInMillis()){
					ArrayList<ReminderDetailsDO> listReminder = ApplicationConstants.dbAdapter.fetchAllRecordsAtSpecifiedTime(date, time);
					Intent in = new Intent(context, ReminderActivity.class);
					in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					in.putExtra(RemindersDbAdapter.KEY_ROWID, rowid);
					in.putExtra("listReminders", listReminder);

					for (int i = 0; i < listReminder.size(); i++)
						ApplicationConstants.dbAdapter.deleteReminder(listReminder.get(i).getRowID());

					context.startActivity(in);
				}

			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}
