package hCue.Hcue;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.pushwoosh.fragment.PushEventListener;
import com.pushwoosh.fragment.PushFragment;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.model.AddUserDevice;
import hCue.Hcue.model.AddUserDeviceRequest;
import hCue.Hcue.model.GetPatientDetailsReq;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.getAddUserReponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.CookieService;
import hCue.Hcue.utils.GetPatientsResSingleton;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;


/**
 * Created by Appdest on 01-06-2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements PushEventListener
{

    /* Base member variables declaration */
    public LinearLayout llLeftMenu, llTop, llBody, llGroup, llLocation, llDownarrow, llRight,
            llBack, llClose, llPatients, lldropdownpatients,llBottomBar,llSave, llFilter;
    public ImageView ivMenu, ivBack,ivSearch,ivClose,ivArrow, ivFilter;
    public TextView tvSearchDoctors, tvProfile, tvTopTitle, tvName, tvHistory, tvLogout, tvPastRecords, tvLocationHeader, tvLocation,tvspecialityCount,tvEmail,tvPhone,tvSave;
    public static Context mContext;
    public LayoutInflater inflater;
    public DrawerLayout.LayoutParams drawerParams;
    public ViewGroup.LayoutParams baseLayoutParams;
    private boolean enableDrawer = true;
    public ImageView imagePopup, ivMenuProfilePic;
    public ProgressDialog progressdialog;
    public boolean isFromLogout, isFromExit, isFromRegisterfail;
    protected Dialog dialog;
    protected ProgressDialog progressDialog;
    private View mImage;
    protected GoogleApiClient mGoogleApiClient;
    Boolean requestcancel = true;
    private  Preference preference ;
    public Bundle mBundle;
    private Animation slide_up, slide_down;
    private CookieService cookieService ;
    private ListView lvPatients ;
    public static boolean isSinglePatient = true ;
    private int selectedposition = 0;
    private PatientListAdapter patientListAdapter ;
    public Bundle savedInstanceState ;
    public Uri menuPicURi ;
    private static int selectedTabID = 0;
    private static boolean isFirstTime = true;
    public LinearLayout llHome, llAppointments, llHealthRecords, llProfile, llFabContainer;
    public TextView tvHome, tvAppointment, tvHealthRecords, tvProfile1;
    public ImageView ivHome, ivAppointment, ivHealthRecords,ivProfile1, ivOrder;
    private boolean isFabOpen;
    private ImageView fab;
    private Animation rotate_forward,rotate_backward;
    protected FrameLayout flBottomBar;
    protected TextView tvProfile1Highlight, tvHealthRecordsHighlight, tvAppointmentHighlight, tvHomeHighlight, tvOrder,
            tvOrderHighlight, tvPharmacyOrders, tvAskPublicQuestion;
    protected LinearLayout llPharmacyOrders, llFabProfile, llOrder, llAskPubilQuestion;
    public static int selTabPos = 1;
    public abstract void initialize();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.base_activity);
        PushFragment.init(this);

        Resources res = getResources();
        DisplayMetrics metrics = res.getDisplayMetrics();
        Log.e("Density ", metrics.density + "");

        ApplicationConstants.DEVICE_DENSITY = metrics.density;
        ApplicationConstants.DISPLAY_WIDTH = metrics.widthPixels;
        ApplicationConstants.DISPLAY_HEIGHT = metrics.heightPixels;
        initBaseControls();
        initialize();
        if(ApplicationConstants.WALSHEIM_MEDIUM == null && ApplicationConstants.WALSHEIM_SEMI_BOLD == null)
            initializeFonts();

    }

    private void initBaseControls() {
        mContext = BaseActivity.this;
        inflater = this.getLayoutInflater();
        baseLayoutParams = new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        drawerParams = new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.MATCH_PARENT);

        llTop                                       = (LinearLayout) findViewById(R.id.llTop);
        llBody                                      = (LinearLayout) findViewById(R.id.llBody);
        llLeftMenu                                  = (LinearLayout) findViewById(R.id.llLeftMenu);
        llBack                                      = (LinearLayout) findViewById(R.id.llBack);
        llGroup                                     = (LinearLayout) findViewById(R.id.llGroup);
        llPatients                                  = (LinearLayout) findViewById(R.id.llPatients);
        llLocation                                  = (LinearLayout) findViewById(R.id.llLocation);
        llDownarrow                                 = (LinearLayout) findViewById(R.id.llDownarrow);
        llRight                                     = (LinearLayout) findViewById(R.id.llRight);
        llClose                                     = (LinearLayout) findViewById(R.id.llClose);
        lldropdownpatients                          = (LinearLayout) findViewById(R.id.lldropdownpatients);
        llBottomBar                                 = (LinearLayout) findViewById(R.id.llBottomBar);
        llSave                                      = (LinearLayout) findViewById(R.id.llSave);
        llFabContainer                              = (LinearLayout) findViewById(R.id.llFabContainer);
        ivMenu                                      = (ImageView) findViewById(R.id.ivMenu);
        ivClose                                     = (ImageView) findViewById(R.id.ivClose);
        ivBack                                      = (ImageView) findViewById(R.id.ivBack);
        ivSearch                                    = (ImageView) findViewById(R.id.ivSearch);
        ivArrow                                     = (ImageView) findViewById(R.id.ivArrow);
        ivMenuProfilePic                            = (ImageView) findViewById(R.id.ivMenuProfilePic);
        ivFilter                                    = (ImageView) findViewById(R.id.ivFilter);
        tvTopTitle                                  = (TextView) findViewById(R.id.tvTopTitle);
        tvProfile                                   = (TextView) findViewById(R.id.tvProfile);
        tvPastRecords                               = (TextView) findViewById(R.id.tvPastRecords);
        tvName                                      = (TextView) findViewById(R.id.tvName);
        tvHistory                                   = (TextView) findViewById(R.id.tvHistory);
        tvSearchDoctors                             = (TextView) findViewById(R.id.tvSearchDoctors);
        tvLogout                                    = (TextView) findViewById(R.id.tvLogout);
        tvLocationHeader                            = (TextView) findViewById(R.id.tvLocationHeader);
        tvspecialityCount                           = (TextView) findViewById(R.id.tvspecialityCount);
        tvLocation                                  = (TextView) findViewById(R.id.tvLocation);
        tvEmail                                     = (TextView) findViewById(R.id.tvEmail);
        tvPhone                                     = (TextView) findViewById(R.id.tvPhone);
        tvSave                                      = (TextView) findViewById(R.id.tvSave);
        //tvPharmacyOrders                            = (TextView) findViewById(R.id.tvPharmacyOrders);
        flBottomBar                                 = (FrameLayout) findViewById(R.id.flBottomBar);
        lvPatients                                  = (ListView)    findViewById(R.id.lvPatients);
        llHome                                      = (LinearLayout) findViewById(R.id.llHome);
        llAppointments                              = (LinearLayout) findViewById(R.id.llAppointments);
        llHealthRecords                             = (LinearLayout) findViewById(R.id.llHealthRecords);
        llProfile                                   = (LinearLayout) findViewById(R.id.llProfile);
        llFilter                                    = (LinearLayout) findViewById(R.id.llFilter);
        llOrder                                     = (LinearLayout) findViewById(R.id.llOrder);
        tvHome                                      = (TextView) findViewById(R.id.tvHome);
        tvAppointment                               = (TextView) findViewById(R.id.tvAppointment);
        tvHealthRecords                             = (TextView) findViewById(R.id.tvHealthRecords);
        tvProfile1                                  = (TextView) findViewById(R.id.tvProfile1);
        tvOrder                                     = (TextView) findViewById(R.id.tvOrder);
        tvHomeHighlight                             = (TextView) findViewById(R.id.tvHomeHighlight);
        tvAppointmentHighlight                      = (TextView) findViewById(R.id.tvAppointmentHighlight);
        tvHealthRecordsHighlight                    = (TextView) findViewById(R.id.tvHealthRecordsHighlight);
        tvProfile1Highlight                         = (TextView) findViewById(R.id.tvProfile1Highlight);
        tvOrderHighlight                            = (TextView) findViewById(R.id.tvOrderHighlight);
        tvAskPublicQuestion                         = (TextView) findViewById(R.id.tvAskPublicQuestion);
        ivHome                                      = (ImageView) findViewById(R.id.ivHome);
        ivAppointment                               = (ImageView) findViewById(R.id.ivAppointment);
        ivHealthRecords                             = (ImageView) findViewById(R.id.ivHealthRecords);
        ivProfile1                                  = (ImageView) findViewById(R.id.ivProfile1);
        ivOrder                                     = (ImageView) findViewById(R.id.ivOrder);

        //llPharmacyOrders                            = (LinearLayout) findViewById(R.id.llPharmacyOrders);
        //llFabProfile                                = (LinearLayout) findViewById(R.id.llFabProfile);
        llAskPubilQuestion                          = (LinearLayout) findViewById(R.id.llAskPubilQuestion);
        slide_up                                    = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slide_down                                  = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        fab                                         = (ImageView) findViewById(R.id.fab);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fab.setImageDrawable(getResources().getDrawable(R.drawable.fab, BaseActivity.this.getTheme()));
        } else {
            fab.setImageDrawable(getResources().getDrawable(R.drawable.fab));
        }
        tvPastRecords.setVisibility(View.VISIBLE);

        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);

        preference = new Preference(BaseActivity.this);

        String temp = preference.getStringFromPreference("LOCALITY","");
        if (temp != null && temp.split(",").length > 1){
            if (temp.split(",")[1].equalsIgnoreCase(" null")){
                tvLocation.setText(temp.split(",")[0]);
            }else{
                tvLocation.setText(temp.split(",")[0] + temp.split(",")[1]);
            }
        }

        setSpecificTypeFace(llGroup, ApplicationConstants.WALSHEIM_MEDIUM);
        tvTopTitle.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvLocationHeader.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvLocation.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvspecialityCount.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvName.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvEmail.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvPhone.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvProfile.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvSearchDoctors.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvHistory.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvPastRecords.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvLogout.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvSave.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        //tvPharmacyOrders.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvAskPublicQuestion.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        tvHome.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvAppointment.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvHealthRecords.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvProfile1.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvOrder.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        isSinglePatient = preference.getbooleanFromPreference(Preference.ISSINGLEPATIENT, true);

        llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selTabPos != 1){
                    selTabPos = 1;
                    Intent home = new Intent(BaseActivity.this, Specialities.class);
                    home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    startActivity(home, bndlanimation);
                    finish();
                }

            }
        });

        llAppointments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selTabPos != 3) {
                    selTabPos = 3;
                    Intent history = new Intent(BaseActivity.this, MyAppointments.class);
                    history.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                    startActivity(history, bndlanimation);
                    finish();
                }
            }
        });

        llHealthRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selTabPos != 4) {
                    selTabPos = 4;
                    Intent home = new Intent(BaseActivity.this, MyHealthRecords.class);
                    home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                    startActivity(home, bndlanimation);
                    finish();
                }
            }
        });

       /* llPharmacyOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
                *//*Intent home = new Intent(BaseActivity.this, MyPharmaOrders.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                startActivity(home, bndlanimation);*//*
            }
        });*/

        /*llFabProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
                callgetPatientDataService();

            }
        });*/

        llAskPubilQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
                Intent in = new Intent(BaseActivity.this, MyQuestions.class);
                startActivity(in);
            }
        });

        llOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Calendar cal = Calendar.getInstance(Locale.getDefault());
                Calendar calMorning = Calendar.getInstance(Locale.getDefault());
                Calendar calNight = Calendar.getInstance(Locale.getDefault());
                cal.set(Calendar.SECOND, 0);

                calMorning.set(Calendar.HOUR_OF_DAY, 7);
                calMorning.set(Calendar.MINUTE, 0);
                calMorning.set(Calendar.SECOND, 0);

                calNight.set(Calendar.HOUR_OF_DAY, 21);
                calNight.set(Calendar.MINUTE, 0);
                calNight.set(Calendar.SECOND, 0);
//                if((calMorning.getTimeInMillis() <= cal.getTimeInMillis()) && (cal.getTimeInMillis() < calNight.getTimeInMillis())){

                    /*if (selTabPos != 2) {
                        selTabPos = 2;
                        Intent home = new Intent(BaseActivity.this, UploadPrescription.class);
                        home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                        startActivity(home, bndlanimation);
                        finish();
                    }*/
//                }else{
//                    ShowAlertDialog("Whoops!","This Service is available from\n 7 AM to 9 PM only.\n Sorry for the inconvenience.",R.drawable.worng);
//                }

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });

        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callgetPatientDataService();
            }
        });


        lldropdownpatients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(llGroup.getVisibility() == View.VISIBLE) {
                    llGroup.setVisibility(View.GONE);
                    ivArrow.setBackgroundResource(R.drawable.down);
                }
                else {
                    llGroup.setVisibility(View.VISIBLE);
                    ivArrow.setBackgroundResource(R.drawable.up);
                }
            }
        });
        llBack.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    llBack.setBackgroundColor(Color.parseColor("#33000000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    llBack.setBackgroundColor(Color.parseColor("#00000000"));
                    String curActivityName = (String) v.getTag();

                    if (curActivityName != null){
                        if ((curActivityName.equalsIgnoreCase("MyPharmaOrders") || curActivityName.equalsIgnoreCase("UploadPrescription"))){
                            gotoBack();
                        }else if(curActivityName.equalsIgnoreCase("ProgressOrderDetails")){
                            if (ApplicationConstants.isFromNewOrder){
                                gotoBack();
                                ApplicationConstants.isFromNewOrder = false;
                            }
//                            else
//                                gotoMyOrdersBack();
                        }else if(curActivityName.equalsIgnoreCase("MedicineConfirmation")){
//                            gotoBackDeliveryAddressList();
                        }else if(curActivityName.equalsIgnoreCase("ContactDetails")){
//                            goToDeliveryAddress();
                        }
                        else
                            finish();

                    }else
                        finish();
                    overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                }
                return true;
            }
        });



        llLeftMenu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    llLeftMenu.setBackgroundColor(Color.parseColor("#33000000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    llLeftMenu.setBackgroundColor(Color.parseColor("#00000000"));
                }
                return true;
            }
        });
    }

    private void callgetPatientDataService()
    {
        showLoader("Loading");
        GetPatientDetailsReq patientDetailsReq = new GetPatientDetailsReq();
        patientDetailsReq.setPatientID(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID());
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientDetails(patientDetailsReq, new RestCallback<LoginResponse>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                Intent profile = new Intent(BaseActivity.this, MyProfileActivity1.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                startActivity(profile, bndlanimation);
            }

            @Override
            public void success(LoginResponse loginResponse, Response response) {
                hideLoader();
                selTabPos = 0;
                if(loginResponse != null)
                    LoginResponseSingleton.getSingleton(loginResponse, BaseActivity.this);
                Intent home = new Intent(BaseActivity.this, MyProfileActivity1.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                startActivity(home, bndlanimation);
                finish();


            }
        });

    }

    private void callgetPatientDataService(long patientID)
    {
        showLoader("Loading");
        GetPatientDetailsReq patientDetailsReq = new GetPatientDetailsReq();
        patientDetailsReq.setPatientID(patientID);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientDetails(patientDetailsReq, new RestCallback<LoginResponse>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(final LoginResponse loginResponse, Response response) {
                llGroup.setVisibility(View.VISIBLE);
                ivArrow.setBackgroundResource(R.drawable.up);
                hideLoader();
                if(loginResponse != null) {
                    LoginResponseSingleton.getSingleton(loginResponse, BaseActivity.this);
                    tvName.setText(loginResponse.getArrPatients().get(0).getFullName());
                    if(loginResponse.getListPatientEmail() != null && !loginResponse.getListPatientEmail().isEmpty())
                    {
                        tvEmail.setVisibility(View.VISIBLE);
                        tvEmail.setText(loginResponse.getListPatientEmail().get(0).getEmailID());
                    }else
                    {
                        tvEmail.setVisibility(View.GONE);
                    }
                    if(loginResponse.getListPatientPhone() != null && !loginResponse.getListPatientPhone().isEmpty())
                    {
                        tvPhone.setVisibility(View.VISIBLE);
                        tvPhone.setText(loginResponse.getListPatientPhone().get(0).getPhNumber() + "");
                    }
                    else
                    {
                        tvPhone.setVisibility(View.GONE);
                    }
                    if(LoginResponseSingleton.getSingleton() != null)
                        if(LoginResponseSingleton.getSingleton() != null)
                            if(LoginResponseSingleton.getSingleton().getPatientImage() != null && !LoginResponseSingleton.getSingleton().getPatientImage().isEmpty())
                            {
                                Picasso.with(BaseActivity.this).load(LoginResponseSingleton.getSingleton().getPatientImage()).into(ivMenuProfilePic);
                                preference.saveStringInPreference("PhotoUrl", "");
                                preference.commitPreference();
                            }
                            else
                            {   final String PhotoUrl = preference.getStringFromPreference("PhotoUrl", "");
                                if (!TextUtils.isEmpty(PhotoUrl))
                                    Picasso.with(BaseActivity.this).load(PhotoUrl).into(ivMenuProfilePic);
                                else
                                {
                                    ivMenuProfilePic.setBackgroundDrawable(null);
                                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.profile_picture);
                                    ivMenuProfilePic.setImageBitmap(largeIcon);
                                }
                            }
                }
                patientListAdapter.notifyDataSetChanged();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        /*tvLocation.setText(preference.getStringFromPreference("LOCALITY",""));
        if(LoginResponseSingleton.getSingleton() != null)
            if(LoginResponseSingleton.getSingleton() != null)
                if(LoginResponseSingleton.getSingleton().getPatientImage() != null && !LoginResponseSingleton.getSingleton().getPatientImage().isEmpty())
                {
                    Picasso.with(BaseActivity.this).load(LoginResponseSingleton.getSingleton().getPatientImage()).into(ivMenuProfilePic);
                }
                else
                {   final String PhotoUrl = preference.getStringFromPreference("PhotoUrl", "");
                    if (!TextUtils.isEmpty(PhotoUrl))
                        Picasso.with(BaseActivity.this).load(PhotoUrl).into(ivMenuProfilePic);
                    else
                    {
                        ivMenuProfilePic.setBackgroundDrawable(null);
                        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.profile_picture);
                        ivMenuProfilePic.setImageBitmap(largeIcon);
                    }
                }

        if(LoginResponseSingleton.getSingleton() != null && LoginResponseSingleton.getSingleton().getArrPatients()!= null && !LoginResponseSingleton.getSingleton().getArrPatients().isEmpty())
        {
            tvName.setText(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFullName());
            if(LoginResponseSingleton.getSingleton().getListPatientEmail() != null && !LoginResponseSingleton.getSingleton().getListPatientEmail().isEmpty()) {
                tvEmail.setVisibility(View.VISIBLE);
                tvEmail.setText(LoginResponseSingleton.getSingleton().getListPatientEmail().get(0).getEmailID());
            }
            else
            {
                tvEmail.setVisibility(View.GONE);
            }
            if(LoginResponseSingleton.getSingleton().getListPatientPhone() != null && !LoginResponseSingleton.getSingleton().getListPatientPhone().isEmpty()) {
                tvPhone.setVisibility(View.VISIBLE);
                tvPhone.setText(LoginResponseSingleton.getSingleton().getListPatientPhone().get(0).getPhNumber() + "");
            }
            else
            {
                tvPhone.setVisibility(View.GONE);
            }
//            tvAge.setText(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getCurrentAge().getYear() + "Y " + LoginResponseSingleton.getSingleton().getArrPatients().get(0).getCurrentAge().getMonth() + "M");
        }*/
            /*
            if (LoginResponseSingleton.getSingleton().getListPatientAddress() != null && !LoginResponseSingleton.getSingleton().getListPatientAddress().isEmpty())
                if (LoginResponseSingleton.getSingleton().getPatientImage() != null && !LoginResponseSingleton.getSingleton().getPatientImage().isEmpty()) {
                    Drawable drawable = getResources().getDrawable(R.drawable.profileimg);
                    Bitmap decodedByte = convertURLtoBitmap(String.valueOf(LoginResponseSingleton.getSingleton().getPatientImage()));
                    if (decodedByte != null) {
                        Bitmap resized = Bitmap.createScaledBitmap(decodedByte, drawable.getIntrinsicWidth(), drawable.getIntrinsicWidth(), true);
                        Bitmap conv_bm = getRoundedRectBitmap(resized);
                        Log.e("Byte Code", decodedByte + "");
                        if (decodedByte != null)
                            ivMenuProfilePic.setImageBitmap(conv_bm);
                    }
                }
        }*/
    }

    private void callgetPatientsService()
    {
        LoginResponseSingleton.setContext(BaseActivity.this);
        GetPatientsRequest getPatientsRequest = new GetPatientsRequest();
        if(LoginResponseSingleton.getSingleton().getListPatientPhone() != null && !LoginResponseSingleton.getSingleton().getListPatientPhone().isEmpty())
            getPatientsRequest.setPhoneNumber(LoginResponseSingleton.getSingleton().getListPatientPhone().get(0).getPhNumber()+"");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(getPatientsRequest, new RestCallback<GetPatientsResponse>() {
            @Override
            public void failure(RestError restError)
            {
                isSinglePatient = true ;
            }

            @Override
            public void success(GetPatientsResponse getPatientsResponse, Response response) {
                if(getPatientsResponse != null)
                {
                    Preference preference = new Preference(BaseActivity.this);

                    if(getPatientsResponse.getCount()==1)
                    {
                        isSinglePatient = true ;
                        preference.saveBooleanInPreference(Preference.ISSINGLEPATIENT,true);
                    }else
                    {
                        isSinglePatient = false ;
                        preference.saveBooleanInPreference(Preference.ISSINGLEPATIENT,false);
                    }
                    preference.commitPreference();
                    {
                        GetPatientsResSingleton.getSingleton(getPatientsResponse, BaseActivity.this);
                        if(!isSinglePatient)
                        {
                            updatePatientsList();
                            ivArrow.setVisibility(View.VISIBLE);
                            lldropdownpatients.setClickable(true);

                        }else
                        {
                            ivArrow.setVisibility(View.GONE);
                            lldropdownpatients.setClickable(false);
                        }
                    }
                }
            }
        });
    }

    private void updatePatientsList()
    {
        patientListAdapter = new PatientListAdapter(GetPatientsResSingleton.getSingleton().getPatientRows());
        lvPatients.setAdapter(patientListAdapter);
    }

    class PatientListAdapter extends BaseAdapter
    {
        private ArrayList<PatientRow> patientRows ;

        public PatientListAdapter(ArrayList<PatientRow> patientRows)
        {
            this.patientRows = patientRows ;
        }

        @Override
        public int getCount() {
            return patientRows.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(BaseActivity.this).inflate(R.layout.drawer_list_cell, null);
            TextView tvDrawerOperation = (TextView) convertView.findViewById(R.id.tvDrawerOperation);
//            TextView tvAlaphabet       = (TextView) convertView.findViewById(R.id.tvAlaphabet);
            final RadioButton tvCount        = (RadioButton) convertView.findViewById(R.id.tvCount);
            final PatientRow patientRow = patientRows.get(position);
            tvDrawerOperation.setText(patientRow.getArrPatients().get(0).getFullName());
//            tvAlaphabet.setBackgroundResource(getImageResource());
//            tvAlaphabet.setText((patientRow.getArrPatients().get(0).getFullName().charAt(0)+"").toUpperCase());

//            convertView.animate().translationY(position);

//            tvAlaphabet.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvDrawerOperation.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            if(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID()== patientRow.getArrPatients().get(0).getPatientID())
            {
                tvCount.setChecked(true);
//                convertView.setBackgroundColor(getResources().getColor(R.color.transparentOne));
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvCount.setChecked(true);
                    selectedposition = position ;
                    callgetPatientDataService(patientRow.getArrPatients().get(0).getPatientID());
                }
            });
            return convertView;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder binder) {
            CookieService.LocalBinder b = (CookieService.LocalBinder) binder;
            cookieService = b.getServiceInstance();
            cookieService.getCookies(0);
            boolean islogout = preference.getbooleanFromPreference("ISLOGOUT", true);
            if(islogout)
            {
                // New User
                Intent splashActivty = new Intent(getApplicationContext(), LaunchActivity.class);
                startActivity(splashActivty);
                finish();
            }
            else
            {
                // Existing user
                Intent splashActivty = new Intent(getApplicationContext(), Specialities.class);
                startActivity(splashActivty);
                finish();
            }
            //Toast.makeText(SplashActivity.this, "Connected", Toast.LENGTH_SHORT).show();
        }
        public void onServiceDisconnected(ComponentName className) {
            cookieService = null;
        }
    };

    public Bitmap getRoundedRectBitmap(Bitmap bitmap) {
        Bitmap result = null;
        try {
            Drawable drawable = getResources().getDrawable(R.drawable.profile_picture);
            result = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicWidth(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);

            int color = 0xffffff42;
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicWidth());

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(Color.WHITE);
            canvas.drawCircle(drawable.getIntrinsicWidth() / 2, drawable.getIntrinsicWidth() / 2, drawable.getIntrinsicWidth() / 2, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

        } catch (NullPointerException e) {
        } catch (OutOfMemoryError o) {
        }
        return result;
    }






    public static void setSpecificTypeFace(ViewGroup group, Typeface typeface) {
        int count = group.getChildCount();
        View v;
        for (int i = 0; i < count; i++) {
            v = group.getChildAt(i);
            if (v instanceof TextView || v instanceof Button || v instanceof EditText/*etc.*/)
                ((TextView) v).setTypeface(typeface);
            else if (v instanceof ViewGroup)
                setSpecificTypeFace((ViewGroup) v, typeface);
        }
    }

    public void hideKeyBoard(View view) {
        View view1 = this.getCurrentFocus();
        if (view1 != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }
    }

    public void showLoader(String str)
    {

        runOnUiThread(new RunShowLoader(str));
    }


    /**
     * Name:         RunShowLoader
     Description:  This is to show the loading progress dialog when some other functionality is taking place.**/
    class RunShowLoader implements Runnable {
        private String strMsg;

        public RunShowLoader(String strMsg) {
            this.strMsg = strMsg;
        }

        public void run() {
            if(dialog == null)
            {
                try {

                    dialog = new Dialog(BaseActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialog.setContentView(R.layout.loader_custom);
                    mImage = (ImageView) dialog.findViewById(R.id.ivLoaderIcon);
                    dialog.show();

                    System.out.println("---------------------show loader");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("---------------------show loader exception ");
                    progressDialog = null;
                }
            }
        }
    }

    /**
     * Name:         RunShowLoader
     Description:  For hiding progress dialog (Loader ).**/

    public void hideLoader() {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run() {
                try {

                    if (dialog != null && dialog.isShowing())
                    {
                        System.out.println("---------------------show loader not null");
                        dialog.dismiss();
                        dialog.hide();
                    } else {
                        System.out.println("---------------------show loader null");
                    }
                    dialog = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }



    @Override
    protected void onPause() {
        super.onPause();
        //	unregisterReceivers();
        if (progressdialog != null && progressdialog.isShowing()) {
            progressdialog.dismiss();
        }
    }

    public Bitmap convertURLtoBitmap(String src) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {

            URL url = new URL(src);

            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();

            connection.setDoInput(true);
            connection.connect();

            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;

        } catch (IOException e) {

            e.printStackTrace();
            return null;

        }
    }

    public void ShowAlertDialog(String header, String message, int resourceid)
    {

        try{
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BaseActivity.this, R.style.MyDialogTheme);
            View dialogView = LayoutInflater.from(BaseActivity.this).inflate(R.layout.home_pickup_dialog, null);
            dialogBuilder.setView(dialogView);
            TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
            final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
            final TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
            TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
            ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

            tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
            tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
            tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();

            tvInfo.setText(message);
//        tvHeading.setText(header);
//        tvHeading.setTextSize(20);
            ivLogo.setBackgroundResource(resourceid);
            tvInfo.setTextSize(18);
            if (!isFromLogout && !isFromExit) {
                tvYes.setText("OK");
                tvNo.setVisibility(View.GONE);
            }
            if(isFromRegisterfail)
            {
                tvYes.setText("Login");
                tvNo.setText("Cancel");
                tvNo.setVisibility(View.VISIBLE);

            }

            tvHeading.setVisibility(View.GONE);
            ivLogo.setVisibility(View.VISIBLE);

            tvNo.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction()==MotionEvent.ACTION_DOWN)
                    {
                        v.setBackgroundColor(Color.parseColor("#33110000"));

                    }
                    else if(event.getAction()==MotionEvent.ACTION_UP)
                    {
                        v.setBackgroundColor(Color.parseColor("#33000000"));
                        if (isFromLogout) {
                            isFromLogout = false;
                        }
                        if (isFromExit) {
                            isFromExit = false;
                        }
                        if(isFromRegisterfail)
                        {
                            isFromRegisterfail = false;
                        }
                        alertDialog.dismiss();
                    }
                    return true;
                }
            });
            tvYes.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction()==MotionEvent.ACTION_DOWN)
                    {
                        v.setBackgroundColor(Color.parseColor("#33110000"));

                    }
                    else if(event.getAction()==MotionEvent.ACTION_UP)
                    {
                        v.setBackgroundColor(Color.parseColor("#33000000"));
                        alertDialog.dismiss();
                        if (isFromLogout) {
                            Preference preference = new Preference(BaseActivity.this);
                            String locality  = preference.getStringFromPreference("LOCALITY","");
                            String latlong = preference.getStringFromPreference("LATLONG","");
                            String currentlatlong = preference.getStringFromPreference("CURRENTLATLONG","");
                            preference.clearPreferences();
                            preference.commitPreference();
                            preference.saveStringInPreference("LOCALITY",locality);
                            preference.saveStringInPreference("LATLONG",latlong);
                            preference.saveStringInPreference("CURRENTLATLONG",currentlatlong);
                            preference.saveStringInPreference("LOGINRESPONSE", "");
                            preference.saveBooleanInPreference("ISLOGOUT",true);
                            preference.commitPreference();
                            Intent logout = new Intent(BaseActivity.this, LaunchActivity.class);
                            startActivity(logout);
                            finishAffinity();
                            finish();
                        }
                        if(isFromExit)
                            finishAffinity();
                        if(isFromRegisterfail)
                        {
                            isFromRegisterfail = false;
                            Intent in = new Intent(BaseActivity.this, Login.class);
                            startActivity(in);
                            finish();
                        }
                    }
                    return true;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void initializeFonts()
    {

        ApplicationConstants.WALSHEIM_BOLD 		= Typeface.createFromAsset(getApplicationContext().getAssets(), "ProximaNova-Bold.otf");;
        ApplicationConstants.WALSHEIM_LIGHT 	= Typeface.createFromAsset(getApplicationContext().getAssets(), "ProximaNova-Light.otf");;
        ApplicationConstants.WALSHEIM_MEDIUM 	= Typeface.createFromAsset(getApplicationContext().getAssets(), "ProximaNova-Regular.otf");;
        ApplicationConstants.WALSHEIM_SEMI_BOLD	= Typeface.createFromAsset(getApplicationContext().getAssets(), "proximanova-semibold-webfont.ttf");;

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        /*final CookieValues cookieValues = CookieSingleton.getInstance();
        if(cookieValues.getDoctorCloudFront_Key_Pair_Id()==null || cookieValues.getDoctorCloudFront_Key_Pair_Id().isEmpty())
        {
            Intent intent = new Intent(BaseActivity.this, CookieService.class);
            startService(intent);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }*/
        if(mGoogleApiClient != null)
            mGoogleApiClient.connect();
        requestcancel = true;
    }

    @Override
    public void onBackPressed()
    {
        finish();
        overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
    }

    public int getImageResource() {

        Random random = new Random();
        switch (random.nextInt(4)) {
            case 0:
                return R.drawable.menu_circle_blue;
            case 1:
                return R.drawable.menu_circle_green;
            case 2:
                return R.drawable.menu_circle_red;
            case 3:
                return R.drawable.menu_circle_vilot;
            case 4:
            default:
                return R.drawable.menu_circle_red;

        }

    }

    public ImageLoader initImageLoader(Context context, int resId)
    {
        ImageLoader imgLoader = ImageLoader.getInstance();
        DisplayImageOptions defaultOptions = getDisplayOptions(resId);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        imgLoader.init(config);
        return imgLoader;
    }

    public DisplayImageOptions getDisplayOptions(int resId)
    {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .showImageOnLoading(resId)
                .showImageOnFail(resId)
                .showImageForEmptyUri(resId)
                .build();
        return defaultOptions;
    }

    @Override
    public void doOnUnregisteredError(String s) {
    }

    @Override
    public void doOnRegisteredError(String s) {
    }

    @Override
    public void doOnRegistered(final String s) {
        Log.e("device registration id", s);
        /*if (LoginResponseSingleton.getSingleton() != null && LoginResponseSingleton.getSingleton().getArrPatients() != null) {
            if (!LoginResponseSingleton.getSingleton().getArrPatients().isEmpty())
                if (SplashActivityOne.device_uuid != null) {
                    AddUserDeviceRequest addUserDeviceRequest = new AddUserDeviceRequest();
                    addUserDeviceRequest.setUUID(SplashActivityOne.device_uuid);
                    addUserDeviceRequest.setIDs(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID());
                    RestClient.getAPI(Constants.DEVICE_REGISTRATION).getAddUserDevice(addUserDeviceRequest, new RestCallback<ArrayList<getAddUserReponse>>() {
                        @Override
                        public void failure(RestError restError) {
                            // hideLoader();
                        }

                        @Override
                        public void success(ArrayList<getAddUserReponse> getAddUserReponse, Response response) {
                            //hideLoader();
                            if (getAddUserReponse == null || getAddUserReponse.isEmpty()) {
                                calladdUserDevice(SplashActivityOne.device_uuid, s);
                            }
                        }
                    });
                }
        }*/
    }
    private void calladdUserDevice(String device_uuid, String deviceid) {
        AddUserDevice addUserDevicereq = new AddUserDevice();
        addUserDevicereq.setUUID(device_uuid);
        addUserDevicereq.setDeviceID(deviceid);
        addUserDevicereq.setUSRId(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID());
        long[] ids = new long[1];
        ids[0] = LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID();
        addUserDevicereq.setIDs(ids);
        RestClient.getAPI(Constants.DEVICE_REGISTRATION).AddUserDevice(addUserDevicereq, new RestCallback<Object>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(Object result, Response response) {
                hideLoader();
            }
        });
    }

    @Override
    public void doOnMessageReceive(String s) {
//        Toast.makeText(BaseActivity.this, "Received Msg"+ s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void doOnUnregistered(String s) {

    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        PushFragment.onNewIntent(this,intent);
    }

    public void animateFAB(){

        if(isFabOpen){
            llFabContainer.setVisibility(View.GONE);
            llFabContainer.startAnimation(slide_down);
            llBody.setVisibility(View.VISIBLE);
            fab.startAnimation(rotate_backward);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {
            llFabContainer.setVisibility(View.VISIBLE);
            llFabContainer.startAnimation(slide_up);
            llBody.setVisibility(View.GONE);
            fab.startAnimation(rotate_forward);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }
    private void gotoBack() {
        selTabPos = 1;
        Intent slideactivity = new Intent(this, Specialities.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
        slideactivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(slideactivity, bndlanimation);
        finish();
    }
    /*private void gotoBackDeliveryAddressList() {
        Intent slideactivity = new Intent(this, DeliveryAddress.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
        slideactivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(slideactivity, bndlanimation);
        finish();
    }*/

    /*private void gotoMyOrdersBack() {
        Intent slideactivity = new Intent(this, MyPharmaOrders.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
        slideactivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(slideactivity, bndlanimation);
        finish();
    }*/
    /*private void goToDeliveryAddress() {
        Intent slideactivity = new Intent(this, DeliveryAddress.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
        slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(slideactivity, bndlanimation);
        finish();
    }*/
}
