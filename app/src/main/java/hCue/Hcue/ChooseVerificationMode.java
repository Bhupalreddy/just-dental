package hCue.Hcue;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import hCue.Hcue.model.ForgotPasswordUpdatePinRequest;
import hCue.Hcue.model.OTPRequest;
import hCue.Hcue.model.OTPResponse;
import hCue.Hcue.model.OtpViaEmailRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.Utils;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 09/06/16.
 */
public class ChooseVerificationMode extends AppCompatActivity implements View.OnClickListener
{
    private TextView tvResetViaPhone, tvResetViaEmail, tvContinue,tvTitle;
    BaseActivity baseActivity;
    private boolean isResetViaMobile = true;
    public ProgressDialog progressdialog;
    private LinearLayout llChooseVerificationMode , llBack;
    private String   phone , emailID;
    private OtpViaEmailRequest otpviaemailReq ;
    private ForgotPasswordUpdatePinRequest forgotpasswdReq ;
    private static  AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        emailID = getIntent().getStringExtra("EmailID");
        phone   = getIntent().getStringExtra("Mobile");
        setContentView(R.layout.choose_verificationmode);

        llChooseVerificationMode = (LinearLayout) findViewById(R.id.llChooseVerificationMode);
        llBack = (LinearLayout)findViewById(R.id.llBack);
        tvResetViaPhone     = (TextView)findViewById(R.id.tvResetViaPhone);
        tvResetViaEmail     = (TextView)findViewById(R.id.tvResetViaEmail);
        tvContinue          = (TextView)findViewById(R.id.tvContinue);
        tvTitle             = (TextView)findViewById(R.id.tvTitle);
        tvResetViaPhone.setOnClickListener(this);
        tvResetViaEmail.setOnClickListener(this);
        tvContinue.setOnClickListener(this);

        tvTitle.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });

        BaseActivity.setSpecificTypeFace(llChooseVerificationMode, ApplicationConstants.WALSHEIM_MEDIUM);

        if (phone != null && phone.length()== 10)
            tvResetViaPhone.setText("Text a code to my Phone ending in " + ((phone != null && phone.length() == 10) ? phone.substring(7, 10) : ""));
        if (emailID != null)
            tvResetViaEmail.setText(String.format("Email a link to %s ******@%s",emailID.substring(0,2),emailID.split("@")[1]));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvResetViaPhone){
            resetBackground();
            isResetViaMobile = true;
            tvResetViaPhone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_bob_forgot, 0, 0, 0);

        }else if (v.getId() == R.id.tvResetViaEmail){
            resetBackground();
            isResetViaMobile = false;
            tvResetViaEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_bob_forgot, 0 ,0 , 0);

        }else if (v.getId() == R.id.tvContinue){
            showLoader("Loading...");
            //need to build the Request Object
            forgotpasswdReq = new ForgotPasswordUpdatePinRequest();
            forgotpasswdReq.setUSRId((int)ApplicationConstants.forgotPasswordFindDetailsResponse.getDoctorID());
            forgotpasswdReq.setPatientLoginID(ApplicationConstants.forgotPasswordFindDetailsResponse.getEmailID());

            //Need to make passCode dynamic
            final String passcode = Utils.generateRandomString();
            ApplicationConstants.PASS_CODE = passcode;
            forgotpasswdReq.setPinPassCode(passcode);
            if (Connectivity.isConnected(ChooseVerificationMode.this)) {
                RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).updateForgotPinPasscode(forgotpasswdReq, new RestCallback<String>() {
                    @Override
                    public void failure(RestError restError)
                    {
                        hideLoader();
                    }

                    @Override
                    public void success(String defaulResponse, Response response) {
                        hideLoader();
                        if (isResetViaMobile) {
                            sendSms(passcode);
                        } else {
                            if (Connectivity.isConnected(ChooseVerificationMode.this)) {
                                sendOTPViaEmail();
                            } else {
                                ShowAlertDialog("Please check internet connection.");
                            }

                        }
                    }
                });
            }else
            {
                ShowAlertDialog("Please check internet connection.");
            }
        }
    }

    private void sendSms(String passcode)
    {
        OTPRequest otpRequest = new OTPRequest();
        otpRequest.setMailContent(""+passcode);
        otpRequest.setText("Your hcue OTP is "+passcode);
        otpRequest.setUSRId("0");
        otpRequest.setTo("91"+phone);
        otpRequest.setToAddress("");

        if (Connectivity.isConnected(ChooseVerificationMode.this))
        {
            callSendOTP(otpRequest,phone);
        }else
        {
            ShowAlertDialog("Please check internet connection.");
        }

    }

    private void callSendOTP(final OTPRequest otpRequest, final String phone)
    {
        RestClient.getAPI("https://api.checkmobi.com").sendOPT(otpRequest, new RestCallback<OTPResponse>() {
            @Override
            public void failure(RestError restError) {
                ShowAlertDialog("Failed to send OTP try again.");
            }

            @Override
            public void success(OTPResponse otpResponse, Response response)
            {
                Intent in = new Intent(ChooseVerificationMode.this, ForgotPassowordCheckPhoneActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                in.putExtra("Mobile",phone);
                in.putExtra("Emailid",emailID);
                in.putExtra("OTPREQ",otpRequest);
                in.putExtra("isResetViaMobile",isResetViaMobile);
                startActivity(in, bndlanimation);

            }
        });
    }

    public void showLoader(String str) {
        runOnUiThread(new RunShowLoader(str));
    }

    public void hideLoader()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressdialog != null && progressdialog.isShowing()) {
                    progressdialog.dismiss();
                }
            }
        });
    }

    /**
     * Name:         RunShowLoader
     Description:  This is to show the loading progress dialog when some other functionality is taking place.**/
    class RunShowLoader implements Runnable {
        private String strMsg;

        public RunShowLoader(String strMsg) {
            this.strMsg = strMsg;
        }

        @Override
        public void run()
        {
            try {
                if(progressdialog == null ||(progressdialog != null && !progressdialog.isShowing())) {
                    progressdialog = ProgressDialog.show(ChooseVerificationMode.this, "", strMsg);
                    progressdialog.setCancelable(false);
                } else {
                }
            } catch(Exception e) {
                e.printStackTrace();
                progressdialog = null;
            }
        }
    }
    public void resetBackground(){
        tvResetViaPhone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.un_check_bob_forgot, 0 ,0 , 0);
        tvResetViaEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.un_check_bob_forgot, 0 ,0 , 0);
    }
    public void sendOTPViaEmail(){

        otpviaemailReq = new OtpViaEmailRequest();
        otpviaemailReq.setUSRId((int)ApplicationConstants.forgotPasswordFindDetailsResponse.getDoctorID());
        otpviaemailReq.setToAddress(ApplicationConstants.forgotPasswordFindDetailsResponse.getEmailID());
        otpviaemailReq.setMailContent(ApplicationConstants.PASS_CODE);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).sendOTPViaMail(otpviaemailReq, new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
                ShowAlertDialog("Failed to send OTP try again.");
            }

            @Override
            public void success(String s, Response response) {
                Intent in = new Intent(ChooseVerificationMode.this, ForgotPassowordCheckPhoneActivity.class);
                in.putExtra("Mobile",phone);
                in.putExtra("Emailid",emailID);
                in.putExtra("EMAILREQ",otpviaemailReq);
                in.putExtra("isResetViaMobile",isResetViaMobile);
                startActivity(in);
            }
        });
    }

    public void ShowAlertDialog(String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChooseVerificationMode.this);
        View dialogView = LayoutInflater.from(ChooseVerificationMode.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        tvInfo.setTextSize(18);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.VISIBLE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }

}
