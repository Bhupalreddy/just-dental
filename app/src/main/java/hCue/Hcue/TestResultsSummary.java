package hCue.Hcue;

import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.CaseDoctorDetails;
import hCue.Hcue.DAO.CaseLabHistory;
import hCue.Hcue.DAO.DoctorSpecialisation;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by shyamprasadg on 30/07/16.
 */
public class TestResultsSummary extends BaseActivity
{
    private ScrollView svTest;
    private TextView tvDoctorname,tvDate,tvTime,tvspeciality,tvClinic,tvTestType,tvDescription,tvInstructions,tvTestResult;
    private PatientCaseRow patientCaseRow;
    private LinearLayout llLabTestPrescribed, llLabTestInstructions, llLabTestResults;
    @Override
    public void initialize()
    {
        svTest    =   (ScrollView) getLayoutInflater().inflate(R.layout.test_results_summary,null,false);
        llBody.addView(svTest,baseLayoutParams);

        if(getIntent().hasExtra("SELECTEDCASE"))
            patientCaseRow = (PatientCaseRow) getIntent().getSerializableExtra("SELECTEDCASE");

        llLabTestPrescribed     =   (LinearLayout) findViewById(R.id.llLabTestPrescribed);
        llLabTestInstructions   =   (LinearLayout) findViewById(R.id.llLabTestInstructions);
        llLabTestResults        =   (LinearLayout) findViewById(R.id.llLabTestResults);
        tvDoctorname        =   (TextView)  svTest.findViewById(R.id.tvDoctorname);
        tvDate              =   (TextView)  svTest.findViewById(R.id.tvDate);
        tvTime              =   (TextView)  svTest.findViewById(R.id.tvTime);
        tvspeciality        =   (TextView)  svTest.findViewById(R.id.tvspeciality);
        tvClinic            =   (TextView)  svTest.findViewById(R.id.tvClinic);
        tvTestType          =   (TextView)  svTest.findViewById(R.id.tvTestType);
        tvInstructions      =   (TextView)  svTest.findViewById(R.id.tvInstructions);
        tvTestResult        =   (TextView)  svTest.findViewById(R.id.tvTestResult);
        tvDescription       =   (TextView)  svTest.findViewById(R.id.tvDescription);

        setSpecificTypeFace(svTest, ApplicationConstants.WALSHEIM_MEDIUM);

//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        tvTopTitle.setText("TEST RESULTS");
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyy");
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(patientCaseRow.getConsultationDt());
        tvDate.setText(simpleDateFormat.format(calendar.getTime()));
        tvTime.setText(patientCaseRow.getStartTime1()+" Hrs");
        CaseLabHistory caseLabHistory = patientCaseRow.getCaseLabHistory();
        if (caseLabHistory != null && caseLabHistory.getLabWrkFlowStatusID().equalsIgnoreCase("LCOM"))
        {
            tvTestResult.setText("Completed.");
            tvTestResult.setTextColor(getResources().getColor(R.color.bg_color));
        }
        else
        {
            tvTestResult.setText("Results Pending.");
            tvTestResult.setTextColor(getResources().getColor(R.color.red));
        }


        CaseDoctorDetails caseDoctorDetails = patientCaseRow.getCaseLabHistory().getListDoctorDetails();
        if(caseDoctorDetails != null)
        {
            if(caseDoctorDetails.getDoctorName().startsWith("Dr"))
                tvDoctorname.setText(caseDoctorDetails.getDoctorName());
            else
                tvDoctorname.setText("Dr."+caseDoctorDetails.getDoctorName());
            tvClinic.setText(caseDoctorDetails.getClinicName());

            StringBuilder specialitybuilder = new StringBuilder();
            for(DoctorSpecialisation doctorSpecialisation : caseDoctorDetails.getListDoctorSpecolisations())
                specialitybuilder.append(doctorSpecialisation.getSepcialityDesc()+", ");

            if(!specialitybuilder.toString().isEmpty())
                tvspeciality.setText(specialitybuilder.toString().substring(0,specialitybuilder.toString().length()-2));
        }

        if(caseLabHistory != null)
        {
            llLabTestInstructions.setVisibility(View.VISIBLE);
            llLabTestPrescribed.setVisibility(View.VISIBLE);
            tvTestType.setText(caseLabHistory.getLabTestName());
            tvInstructions.setText(caseLabHistory.getLabNotes());
        }
        else
        {
            llLabTestInstructions.setVisibility(View.GONE);
            llLabTestPrescribed.setVisibility(View.GONE);
        }
    }
}
