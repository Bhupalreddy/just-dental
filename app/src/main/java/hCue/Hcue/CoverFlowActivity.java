package hCue.Hcue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import hCue.Hcue.DAO.SpecialityDO;
import hCue.Hcue.adapters.CoverFlowAdapter;
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;


public class CoverFlowActivity extends ActionBarActivity {

    private FeatureCoverFlow mCoverFlow;
    private CoverFlowAdapter mAdapter;
    private ArrayList<SpecialityDO> mData = new ArrayList<>(0);
//    private TextSwitcher mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coverflow);

//        mData.add(new SpecialityDO(R.drawable.dentist, R.string.dentist, getResources().getString(R.string.generic_desc),R.drawable.btn_five));
//        mData.add(new SpecialityDO(R.drawable.general_prac, R.string.general_practitioner, getResources().getString(R.string.generic_desc),R.drawable.btn_four));
//        mData.add(new SpecialityDO(R.drawable.phyiotherapist, R.string.physiotherapist, getResources().getString(R.string.generic_desc),R.drawable.btn_three));
//        mData.add(new SpecialityDO(R.drawable.dermatologist, R.string.dermatologist, getResources().getString(R.string.generic_desc),R.drawable.btn_two));
//        mData.add(new SpecialityDO(R.drawable.homeo, R.string.homeopathy, getResources().getString(R.string.generic_desc),R.drawable.btn_one));
//        mData.add(new SpecialityDO(R.drawable.cardiologist, R.string.cardiologist, getResources().getString(R.string.generic_desc),R.drawable.btn_five));
//        mData.add(new SpecialityDO(R.drawable.neuroogist, R.string.neurologist, getResources().getString(R.string.generic_desc),R.drawable.btn_four));
//        mData.add(new SpecialityDO(R.drawable.gastrologist, R.string.gastroenterologist, getResources().getString(R.string.generic_desc),R.drawable.btn_three));
//        mData.add(new SpecialityDO(R.drawable.gynecologist, R.string.gynecologist, getResources().getString(R.string.generic_desc),R.drawable.btn_two));
//        mData.add(new SpecialityDO(R.drawable.ent, R.string.ent, getResources().getString(R.string.generic_desc),R.drawable.btn_one));
//        mData.add(new SpecialityDO(R.drawable.paediatrician, R.string.paediatrician, getResources().getString(R.string.generic_desc),R.drawable.btn_five));
//        mData.add(new SpecialityDO(R.drawable.ophtahlmologist, R.string.orhthalmologist, getResources().getString(R.string.generic_desc),R.drawable.btn_four));
//        mData.add(new SpecialityDO(R.drawable.diabetologist, R.string.diabetologist, getResources().getString(R.string.generic_desc),R.drawable.btn_three));
//        mData.add(new SpecialityDO(R.drawable.urologist, R.string.urologist, getResources().getString(R.string.generic_desc),R.drawable.btn_two));
//        mData.add(new SpecialityDO(R.drawable.orthopedist, R.string.orthopedist, getResources().getString(R.string.generic_desc),R.drawable.btn_one));
//        mData.add(new SpecialityDO(R.drawable.nephrologist, R.string.nephrologist, getResources().getString(R.string.generic_desc),R.drawable.btn_five));
//        mData.add(new SpecialityDO(R.drawable.pastic_surgeon, R.string.plastic_surgeon, getResources().getString(R.string.generic_desc),R.drawable.btn_four));
//        mData.add(new SpecialityDO(R.drawable.accupuncture, R.string.acupuncture, getResources().getString(R.string.generic_desc),R.drawable.btn_three));
//        mData.add(new SpecialityDO(R.drawable.ayurveda, R.string.ayurveda, getResources().getString(R.string.generic_desc),R.drawable.btn_two));
//        mData.add(new SpecialityDO(R.drawable.anesthetist, R.string.anesthesist, getResources().getString(R.string.generic_desc),R.drawable.btn_one));

        mAdapter = new CoverFlowAdapter(this);
        mAdapter.setData(mData);
        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCoverFlow.setReflectionOpacity(0);
        mCoverFlow.setAdapter(mAdapter);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(CoverFlowActivity.this,
                        getResources().getString(mData.get(position).getTitleResId()),
                        Toast.LENGTH_SHORT).show();
            }
        });

        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
//                mTitle.setText(getResources().getString(mData.get(position).titleResId));
            }

            @Override
            public void onScrolling() {
//                mTitle.setText("");
            }
        });
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_coverflow_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
