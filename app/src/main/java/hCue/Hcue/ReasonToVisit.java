package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.LinkedHashMap;

import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.SelectedConsSingleton;
import hCue.Hcue.widget.RippleView;

/**
 * Created by shyamprasadg on 06/07/16.
 */
public class ReasonToVisit extends BaseActivity
{
    private LinearLayout llReasontoVisit;
    private ImageView    ivDoctorPic;
    private TextView     tvDoctorname,tvspecialist,tvHospitalName,tvDateTime,tvDate,tvDay,tvTime;
    private EditText     edtReason;
    private RippleView rvAdd;
    private SpecialityResRow specialityResRow ;
    private PatientRow selectedPatienRow ;
    private boolean isFromReschedule = false;
    private long appointmentID;
    private char appointmentStatus;
    @Override
    public void initialize()
    {
        llReasontoVisit = (LinearLayout) getLayoutInflater().inflate(R.layout.reason_to_visit, null, false);
        llBody.addView(llReasontoVisit);

        specialityResRow  = (SpecialityResRow) getIntent().getSerializableExtra("SELECTED_DATAILS");
        selectedPatienRow = (PatientRow) getIntent().getSerializableExtra("SELECTEDPATIENT");
        if (getIntent().hasExtra("ISFROMRESCHEDULE")) {
            isFromReschedule = getIntent().getBooleanExtra("ISFROMRESCHEDULE", false);
            appointmentID = getIntent().getLongExtra("AppointmentID", 0);
            appointmentStatus = getIntent().getCharExtra("AppointmentStatus", '0');
        }


        ivDoctorPic     =   (ImageView)     llReasontoVisit.findViewById(R.id.ivDoctorPic);

        tvDoctorname    =   (TextView)      llReasontoVisit.findViewById(R.id.tvDoctorname);
        tvspecialist    =   (TextView)      llReasontoVisit.findViewById(R.id.tvspecialist);
        tvHospitalName  =   (TextView)      llReasontoVisit.findViewById(R.id.tvHospitalName);
        tvDate          =   (TextView)      llReasontoVisit.findViewById(R.id.tvDate);
        tvDay           =   (TextView)      llReasontoVisit.findViewById(R.id.tvDay);
        tvTime          =   (TextView)      llReasontoVisit.findViewById(R.id.tvTime);

        edtReason       =   (EditText)      llReasontoVisit.findViewById(R.id.edtReason);

        rvAdd           =   (RippleView)    llReasontoVisit.findViewById(R.id.rvAdd);
        setSpecificTypeFace(llReasontoVisit, ApplicationConstants.WALSHEIM_MEDIUM);

        tvTopTitle.setText("REASON TO VISIT");

//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy (EEEE)");
        Calendar calendar = SelectedConsSingleton.getSingleton().getSelected_date();
        int year = calendar.get(Calendar.YEAR);;
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int week = calendar.get(Calendar.DAY_OF_WEEK);

        String strMonth = DateUtils.getMonthInFullForm(month);
        String strDay = day + DateUtils.getExtension(day);

        tvDate.setText(strDay.toLowerCase()+" "+strMonth+" "+year);
        if (specialityResRow.getOtherVisitReason() != null && !specialityResRow.getOtherVisitReason().equalsIgnoreCase("")){
            edtReason.setText(specialityResRow.getOtherVisitReason());
        }

        tvTime.setText(SelectedConsSingleton.getSingleton().getStarttime()+" Hrs");
        if(specialityResRow != null)
        {
            if(specialityResRow.getProfileImage() != null && !specialityResRow.getProfileImage().isEmpty()) {
                Picasso.with(ReasonToVisit.this).load(Uri.parse(specialityResRow.getProfileImage())).into(ivDoctorPic);
            }
            else
            {
                if(specialityResRow.getGender() == 'M')
                {
                    Picasso.with(this).load(R.drawable.male).into(ivDoctorPic);
                }
                else {
                    Picasso.with(this).load(R.drawable.female).into(ivDoctorPic);
                }
            }
            if(specialityResRow.getFullName().startsWith("Dr."))
                tvDoctorname.setText(specialityResRow.getFullName());
            else
                tvDoctorname.setText("Dr. "+specialityResRow.getFullName());
            //tvDoctorQualification.setText(specialityResRow.getQualification());
            Gson gson = new Gson();
            LinkedHashMap<String, String> speciality = gson.fromJson(specialityResRow.getSpecialityID(), new TypeToken<LinkedHashMap<String, String>>(){}.getType());
            StringBuilder specialitybuilder = new StringBuilder();
            for(String specialityid : speciality.values())
            {
                specialitybuilder.append(Constants.specialitiesMap.get(specialityid)+", ");
            }
            tvspecialist.setText(specialitybuilder.toString().substring(0, (specialitybuilder.toString().length()-2)));
            //tvspeciality.setText(specialityResRow.getSpecialityID()+"");
            tvHospitalName.setText(specialityResRow.getClinicName());
            /*tvDoctorEmail.setText(specialityResRow.getEmailID());
            tvDoctorNo.setText(specialityResRow.getPhoneNumber());
            tvDoctorAddress.setText(specialityResRow.getAddress());*/
        }




        tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvDate.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvTime.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        rvAdd.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent slideactivity = new Intent(ReasonToVisit.this, ConfirmAppointment.class);
                slideactivity.putExtra("REASONTOVISIT", edtReason.getText().toString());
                if(selectedPatienRow != null)
                {
                    slideactivity.putExtra("SELECTEDPATIENT",selectedPatienRow) ;
                }
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                slideactivity.putExtra("SELECTED_DATAILS", specialityResRow);
                slideactivity.putExtra("ISFROMRESCHEDULE", isFromReschedule);
                slideactivity.putExtra("AppointmentID", appointmentID);
                slideactivity.putExtra("AppointmentStatus", appointmentStatus);
                startActivity(slideactivity, bndlanimation);
            }
        });

    }
}
