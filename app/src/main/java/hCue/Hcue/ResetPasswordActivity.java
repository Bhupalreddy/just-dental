package hCue.Hcue;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hCue.Hcue.model.ResetPasswordRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.Utils;
import retrofit.client.Response;


/**
 * Created by CVLHYD-161 on 13-03-2016.
 */
public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView tvDoctorname, tvSubmit, tvpasswordStrength,tvResetPasswordDesc,tvTitle;
    BaseActivity baseActivity;
    private EditText etPassword, etReTypePassword;
    private ImageView ivProfileIcon, ivPasswordMatched;
    private boolean isChecked, isResetViaMobile;
    public ProgressDialog progressdialog;
    private LinearLayout llResetpwd , llBack;
    private TextInputLayout tilConfirmPassword,tilPassword;
//    String text = "Strong passwords include numbers, letters \n and punctuation marks.<font color=#029393> Learn more</font>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.reset_password);

        isResetViaMobile = getIntent().getBooleanExtra("isResetViaMobile",false);

        llResetpwd              =   (LinearLayout)      findViewById(R.id.llResetpwd);
        llBack                  =   (LinearLayout)      findViewById(R.id.llBack);
        tvDoctorname            =   (TextView)          findViewById(R.id.tvDoctorname);
        tvpasswordStrength      =   (TextView)          findViewById(R.id.tvpasswordStrength);
        tvSubmit                =   (TextView)          findViewById(R.id.tvSubmit);
        etPassword              =   (EditText)          findViewById(R.id.etPassword);
        etReTypePassword        =   (EditText)          findViewById(R.id.etReTypePassword);
        ivPasswordMatched       =   (ImageView)         findViewById(R.id.ivPasswordMatched);
        tvResetPasswordDesc     =   (TextView)          findViewById(R.id.tvResetPasswordDesc);
        tvTitle                 =   (TextView)          findViewById(R.id.tvTitle);
        tilPassword             =   (TextInputLayout)   findViewById(R.id.tilPassword);
        tilConfirmPassword      =   (TextInputLayout)   findViewById(R.id.tilConfirmPassword);

        tvTitle.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tilPassword.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilConfirmPassword.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

//        tvResetPasswordDesc.setText(Html.fromHtml(text));

        tvSubmit.setOnClickListener(this);

        BaseActivity.setSpecificTypeFace(llResetpwd, ApplicationConstants.WALSHEIM_MEDIUM);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int rating = (int) getRating(s.toString());
                if (rating == 1) {
                    tvpasswordStrength.setText("Too Low");
                } else if (rating == 2) {
                    tvpasswordStrength.setText("Low");
                } else if (rating == 3) {
                    tvpasswordStrength.setText("Good");
                } else if (rating == 4) {
                    tvpasswordStrength.setText("Excellent");
                }
            }
        });

        etReTypePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwd = etPassword.getText().toString().trim();
                if (pwd.equalsIgnoreCase(s.toString())){
                    ivPasswordMatched.setVisibility(View.VISIBLE);
                }else
                    ivPasswordMatched.setVisibility(View.INVISIBLE);
            }
        });

    }

    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.tvSubmit){

            String pwd = etPassword.getText().toString().trim();
            boolean value = true ;
            if (pwd.isEmpty()){
                etPassword.setError("Please enter Password.");
                value = false ;
            }else if (etReTypePassword.getText().toString().trim().isEmpty()){
                etReTypePassword.setError("Please enter Re- Type Password.");
                value = false ;
            }else if (!etReTypePassword.getText().toString().trim().equalsIgnoreCase(pwd)){
                etReTypePassword.setError("Password and Re-Type Password should be same.");
                value = false ;
            }else if (etReTypePassword.getText().toString().trim().length()<6){
                Toast.makeText(ResetPasswordActivity.this,"Minimum password length should be 6.",Toast.LENGTH_LONG).show();
                value = false ;
            }
            else if(value){

                showLoader("Loading");
                ResetPasswordRequest request = new ResetPasswordRequest();
                request.setUSRId((int) ApplicationConstants.forgotPasswordFindDetailsResponse.getDoctorID());
                request.setPatientLoginID(ApplicationConstants.forgotPasswordFindDetailsResponse.getEmailID());
                request.setPatientNewPassword(pwd);

                request.setPatientCurrentPassword(ApplicationConstants.PASS_CODE);
                request.setPinPassCode(ApplicationConstants.PASS_CODE);

                RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).resetPasscode(request, new RestCallback<String>() {
                    @Override
                    public void failure(RestError restError) {
                        hideLoader();
                        Toast.makeText(ResetPasswordActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void success(String defaulResponse, Response response) {
                        hideLoader();
                        Intent in = new Intent(ResetPasswordActivity.this, PasswordResetCompletedActivity.class);
                        startActivity(in);

                    }
                });
            }
        }
    }
    private float getRating(String pwd) throws IllegalArgumentException {
        if (pwd == null) {throw new IllegalArgumentException();}
        int Strength = 0;
        if (pwd.length() > 5) {Strength++;}
        if (pwd.toLowerCase()!= pwd) {Strength++;}
        if (pwd.length() > 8) {Strength++;}
        int numDigits= Utils.getNumberDigits(pwd);
        if (numDigits > 0 && numDigits != pwd.length()) {Strength++;}
        return Strength;
    }

    public void showLoader(String str) {
        runOnUiThread(new RunShowLoader(str));
    }

    public void hideLoader()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressdialog != null && progressdialog.isShowing()) {
                    progressdialog.dismiss();
                }
            }
        });
    }

    /**
     * Name:         RunShowLoader
     Description:  This is to show the loading progress dialog when some other functionality is taking place.**/
    class RunShowLoader implements Runnable {
        private String strMsg;

        public RunShowLoader(String strMsg) {
            this.strMsg = strMsg;
        }

        @Override
        public void run()
        {
            try {
                if(progressdialog == null ||(progressdialog != null && !progressdialog.isShowing())) {
                    progressdialog = ProgressDialog.show(ResetPasswordActivity.this, "", strMsg);
//					progressdialog.setContentView(R.layout.progress_dialog);
                    progressdialog.setCancelable(false);
                } else {
                }
            } catch(Exception e) {
                e.printStackTrace();
                progressdialog = null;
            }
        }
    }
}
