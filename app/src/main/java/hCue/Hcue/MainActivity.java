package hCue.Hcue;

import android.widget.LinearLayout;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import hCue.Hcue.adapters.BannersPagerAdapter;
import hCue.Hcue.utils.ApplicationConstants;

public class MainActivity extends BaseActivity
{
    private LinearLayout llMain;
    private AutoScrollViewPager autoScrollPager;
    private PageIndicator mIndicator;

    @Override
    public void initialize()
    {
        llMain  =   (LinearLayout)  getLayoutInflater().inflate(R.layout.activity_main,null,false);
        llBody.addView(llMain);

        autoScrollPager = (AutoScrollViewPager) findViewById(R.id.autoScrollPager);
        mIndicator      = (CirclePageIndicator) findViewById(R.id.indicator);

        setSpecificTypeFace(llMain, ApplicationConstants.WALSHEIM_SEMI_BOLD);


        autoScrollPager.setAdapter(new BannersPagerAdapter(MainActivity.this));
        autoScrollPager.startAutoScrollPager(autoScrollPager);
        mIndicator.setViewPager(autoScrollPager);

    }


}
