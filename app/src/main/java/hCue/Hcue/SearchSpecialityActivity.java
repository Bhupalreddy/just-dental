package hCue.Hcue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import hCue.Hcue.DAO.AskPublicQuestionRequestDO;
import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.DAO.SpecialityRequestDO;
import hCue.Hcue.DAO.SpecialityResponseDO;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.PatientVisitRespone;
import hCue.Hcue.model.PatientsVisitsRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import retrofit.client.Response;

/**
 * Created by User on 11/10/2016.
 */

public class SearchSpecialityActivity extends  BaseActivity
{
    private View llConsultation ;
    private ListView lvSpeciality;
    private SearchSpecialityAdapter SpecialityAdapter;
    private TextView tvAdd;
    private EditText etSearchbox;
    private ImageView ivClose;
    private ProgressBar pbHeaderProgress;
    private static AlertDialog alertDialog;
    private LinearLayout llClear;
    private Timer timer = new Timer();
    private final long DELAY = 1000;
    private ArrayList<SpecialityResponseDO> listSpeciality ;

    @Override
    public void initialize()
    {
        llConsultation =  getLayoutInflater().inflate(R.layout.search_speciality, null, false);
        llBody.addView(llConsultation,baseLayoutParams);

        lvSpeciality = (ListView) llConsultation.findViewById(R.id.lvSpeciality);
        tvAdd = (TextView) llConsultation.findViewById(R.id.tvAdd);
        etSearchbox = (EditText) llConsultation.findViewById(R.id.etSearchbox);
        llClear =   (LinearLayout)          llConsultation.findViewById(R.id.llclose);
        ivClose         =   (ImageView)          llConsultation.findViewById(R.id.ivClose);
        pbHeaderProgress = (ProgressBar) llConsultation.findViewById(R.id.pbHeaderProgress);

        BaseActivity.setSpecificTypeFace(llBody, ApplicationConstants.WALSHEIM_MEDIUM);

        tvTopTitle.setText("Search Speciality");
        flBottomBar.setVisibility(View.GONE);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.GONE);
        llClose.setVisibility(View.VISIBLE);
        ivBack.setBackgroundResource(R.drawable.ic_arrow_back);

        SpecialityAdapter = new SearchSpecialityAdapter(SearchSpecialityActivity.this, new ArrayList<SpecialityResponseDO>());
        lvSpeciality.setAdapter(SpecialityAdapter);

        listSpeciality = new ArrayList<>();
        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        if (Connectivity.isConnected(SearchSpecialityActivity.this)) {
            getDoctorSpeciality("");
        } else {
            ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
        }

        llClear.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                etSearchbox.setText("");
                getDoctorSpeciality("");
            }
        });

        etSearchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(timer != null)
                    timer.cancel();
                if (pbHeaderProgress.getVisibility() != View.VISIBLE && s.toString().length()>3){
                    pbHeaderProgress.setVisibility(View.VISIBLE);
                    ivClose.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(final Editable s) {

                if (s.toString().length() == 0) {
                    llClear.setVisibility(View.GONE);
                    getDoctorSpeciality("");
                }else{
                    llClear.setVisibility(View.VISIBLE);
                }

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // TODO: do what you need here (refresh list)
                        // you will probably need to use
                        // runOnUiThread(Runnable action) for some specific
                        // actions
                        if (Connectivity.isConnected(SearchSpecialityActivity.this))
                        {
                            if(s.toString().length()>3) {
                                getDoctorSpeciality(s.toString());
                            }
                            else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if( listSpeciality != null){
                                            listSpeciality.clear();
                                            SpecialityAdapter.refreshAdapter(new ArrayList<SpecialityResponseDO>());
                                        }
                                    }
                                });
                            }
                        }else
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                }
                            });

                        }

                    }

                }, DELAY);

            }
        });

    }

    private void getDoctorSpeciality(String medicineName) {
        //showLoader("");
        SpecialityRequestDO requestDO = new SpecialityRequestDO();
        requestDO.setSearchText(medicineName);

        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getdoctorSpeciality(requestDO, new RestCallback<ArrayList<SpecialityResponseDO>>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                if (pbHeaderProgress.getVisibility() == View.VISIBLE){
                    pbHeaderProgress.setVisibility(View.GONE);
                    ivClose.setVisibility(View.VISIBLE);
                }
//                ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
            }

            @Override
            public void success(ArrayList<SpecialityResponseDO> specialityResponseDOs, Response response) {
                hideLoader();
                if (specialityResponseDOs != null && !specialityResponseDOs.isEmpty()) {
                    listSpeciality.clear();
                    listSpeciality = specialityResponseDOs;
                    SpecialityAdapter.refreshAdapter(listSpeciality);
                } else {
                    listSpeciality.clear();
//                    etSearchbox.setText("");
                    SpecialityAdapter.refreshAdapter(listSpeciality);
                    ShowAlertDialogNo("Whoops!", "No Speciality found.", R.drawable.worng);
                }
                setProgressBarIndeterminateVisibility(false);
                if (pbHeaderProgress.getVisibility() == View.VISIBLE){
                    pbHeaderProgress.setVisibility(View.GONE);
                    ivClose.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public class SearchSpecialityAdapter extends BaseAdapter {
        private Context context;
        ArrayList<SpecialityResponseDO> responseDOs;
        public SearchSpecialityAdapter(Context context, ArrayList<SpecialityResponseDO> responseDOs) {
            this.context = context;
            this.responseDOs = responseDOs;
        }


        @Override
        public int getCount() {
            if (responseDOs == null || responseDOs.isEmpty())
                return 0;
            else
                return responseDOs.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(context).inflate(R.layout.speciality_list_cell, parent, false);

            TextView tvSpecialityName = (TextView) convertView.findViewById(R.id.tvSpecialityName);
            RadioButton rbSelect      = (RadioButton) convertView.findViewById(R.id.rbSelect);

            tvSpecialityName.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

            final SpecialityResponseDO responseDO = responseDOs.get(position);

            tvSpecialityName.setText(responseDO.getDoctorSpecialityDesc());
            convertView.setTag(responseDO);
            rbSelect.setTag(responseDO);


            rbSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpecialityResponseDO specialityDO = (SpecialityResponseDO) v.getTag();
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("specialityDO",specialityDO);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SpecialityResponseDO specialityDO = (SpecialityResponseDO) v.getTag();
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("specialityDO",specialityDO);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
            });

            return convertView;
        }

        public void refreshAdapter(ArrayList<SpecialityResponseDO> responseDO) {
            this.responseDOs = responseDO;
            notifyDataSetChanged();
        }
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
    }
    public void ShowAlertDialogNo(String header,String message, int resourceid)
    {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SearchSpecialityActivity.this);
        View dialogView = LayoutInflater.from(SearchSpecialityActivity.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvHeading.setText(header);
        tvHeading.setTextSize(20);
        tvInfo.setText(message);
        tvInfo.setTextSize(18);
        ivLogo.setBackgroundResource(resourceid);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.VISIBLE);
        ivLogo.setVisibility(View.VISIBLE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
                /*Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();*/

            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }

}
