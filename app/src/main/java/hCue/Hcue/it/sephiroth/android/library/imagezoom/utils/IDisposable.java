package hCue.Hcue.it.sephiroth.android.library.imagezoom.utils;

public interface IDisposable {

	void dispose();
}
