package hCue.Hcue;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.vision.barcode.Barcode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.adapters.PlaceAutocompleteAdapter;
import hCue.Hcue.gps.common.GPSErrorCode;
import hCue.Hcue.gps.lib.GPSCallback;
import hCue.Hcue.gps.lib.GPSUtills;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Locationtext;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;

/**
 * Created by shyamprasadg on 27/06/16.
 */
public class LocationSearchActivity extends BaseActivity implements GPSCallback , GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,Locationtext,
        ResultCallback<LocationSettingsResult>{
    private LinearLayout llLocationSearch;
    private RelativeLayout rlAutodetect;
    private EditText actLocation;
    private TextView tvSearch;
    private ImageView ivClose;
    public GPSUtills gpsUtills;
    private boolean isGpsProviderEnabled;
    private LatLng currentLatLng = null;
    private String input;
    private ArrayList<String> locations;
    private ArrayList<String> descrptions;
    private RippleView rvAutodetect;
    private LinearLayout llclose ;
    protected static final String TAG = "Locationsearch";
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;
    Boolean requestcancel = true;
    private Boolean islocation = false;
    private RecyclerView mRecyclerView;
    PlaceAutocompleteAdapter mAdapter;
    LinearLayoutManager llm;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(-0, 0), new LatLng(0, 0));
    protected GoogleApiClient mGoogleApiClient1;
    public boolean isFromProfile = false, isFromContactDetails = false;
    private final long DELAY = 1000;
    private Timer timer = new Timer();
    private ProgressBar pbHeaderProgress;

    @Override
    public void initialize()
    {
        if (getIntent().hasExtra("isFromProfile")){
            isFromProfile = getIntent().getBooleanExtra("isFromProfile", false);
        }
        if (getIntent().hasExtra("isFromContactDetails")){
            isFromContactDetails = getIntent().getBooleanExtra("isFromContactDetails", false);
            tvLocationHeader.setText("DELIVERY LOCATION");
        }else{
            tvLocationHeader.setText("YOUR LOCATION");
        }


        llLocationSearch = (LinearLayout) getLayoutInflater().inflate(R.layout.location_search, null, false);
        llBody.addView(llLocationSearch);

        actLocation     = (EditText)                llLocationSearch.findViewById(R.id.actLocation);
        rlAutodetect    = (RelativeLayout)          llLocationSearch.findViewById(R.id.rlAutodetect);
        tvSearch        = (TextView)                llLocationSearch.findViewById(R.id.tvSearch);
        ivClose         = (ImageView)               llLocationSearch.findViewById(R.id.ivClose);
        rvAutodetect    = (RippleView)              llLocationSearch.findViewById(R.id.rvAutodetect);
        llclose         = (LinearLayout)            llLocationSearch.findViewById(R.id.llclose);
        pbHeaderProgress = (ProgressBar)            llLocationSearch.findViewById(R.id.pbHeaderProgress);
        locations       = new ArrayList<>();
        descrptions     = new ArrayList<>();

        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        tvTopTitle.setVisibility(View.GONE);
        llLocation.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        gpsUtills = GPSUtills.getInstance(LocationSearchActivity.this);
        gpsUtills.setLogEnable(true);
        gpsUtills.setPackegeName(getPackageName());
        gpsUtills.setListner(LocationSearchActivity.this);
        gpsUtills.isGpsProviderEnabled();

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();

        mContext = LocationSearchActivity.this;
        mGoogleApiClient1 = new GoogleApiClient.Builder(this).enableAutoManage(this, 0 ,this).addApi(Places.GEO_DATA_API).build();

        mRecyclerView   = (RecyclerView) llLocationSearch.findViewById(R.id.rvLocationlist);

        mRecyclerView.setHasFixedSize(true);
        llm = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(llm);

        setSpecificTypeFace(llLocationSearch, ApplicationConstants.WALSHEIM_MEDIUM);
        actLocation.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvSearch.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.view_placesearch, mGoogleApiClient1, BOUNDS_INDIA, null,tvLocation);
        mRecyclerView.setAdapter(mAdapter);

        llclose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                actLocation.setText("");
            }
        });

        actLocation.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(timer != null)
                    timer.cancel();
                if (pbHeaderProgress.getVisibility() != View.VISIBLE && s.toString().length()>3){
                    pbHeaderProgress.setVisibility(View.VISIBLE);
                    ivClose.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void afterTextChanged(final Editable s)
            {
                if (s.toString().length() == 0) {
                    llclose.setVisibility(View.GONE);
                }else{
                    llclose.setVisibility(View.VISIBLE);
                }

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // TODO: do what you need here (refresh list)
                        // you will probably need to use
                        // runOnUiThread(Runnable action) for some specific
                        // actions
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                llTop.setVisibility(View.VISIBLE);
                                if (s.toString().length() > 0) {
                                    if (mAdapter != null) {
                                        mRecyclerView.setAdapter(mAdapter);
                                    }
                                } else {
                                    mAdapter.clearList();
                                }
                                if (!s.toString().equals("") && mGoogleApiClient1.isConnected()) {
                                    mAdapter.getFilter().filter(s.toString());
                                } else if (!mGoogleApiClient1.isConnected())
                                {
                                    Log.e("", "NOT CONNECTED");
                                }

                                if (pbHeaderProgress.getVisibility() == View.VISIBLE){
                                    pbHeaderProgress.setVisibility(View.GONE);
                                    ivClose.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }

                }, DELAY);
            }
        });

        rvAutodetect.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView)
            {
                gpsUtills.isGpsProviderEnabled();
                showLoader("");
                if (isGpsProviderEnabled) {
                    new Handler().postDelayed(new Runnable()
                    {

                        @Override
                        public void run()
                        {
                            gpsUtills.getCurrentLatLng();
                            if (currentLatLng.latitude > 0.0 && currentLatLng.longitude > 0.0) {
                                String latLng = currentLatLng.latitude + "," + currentLatLng.longitude;
                                //tvLatLang.setText(latLng);
                                GetAddress(currentLatLng.latitude, currentLatLng.longitude);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        llTop.setVisibility(View.VISIBLE);
                                        hideLoader();
                                        finish();
                                    }
                                });

                            } else
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideLoader();
                                    }
                                });

                        }
                    }, 2 * 1000);
                }
                else{
                    checkLocationSettings();
                }
            }
        });

//        rlAutodetect.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v) {
//                gpsUtills.isGpsProviderEnabled();
//                if (isGpsProviderEnabled) {
//                    new Handler().postDelayed(new Runnable()
//                    {
//
//                        @Override
//                        public void run()
//                        {
//                            showLoader("");
//                            gpsUtills.getCurrentLatLng();
//                            if (currentLatLng.latitude > 0.0 && currentLatLng.longitude > 0.0) {
//                                String latLng = currentLatLng.latitude + "," + currentLatLng.longitude;
//                                //tvLatLang.setText(latLng);
//                                GetAddress(currentLatLng.latitude, currentLatLng.longitude);
//                                hideLoader();
//
//                            } else
//                            hideLoader();
////                            ShowAlertDialog("Unable to fetch the current location");
//                        }
//                    }, 2 * 1000);
//                }
//            }
//        });
    }

    public String GetAddress(double lat,double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        String ret = "";
        String cityName = null, countryCode = null, countryName = null, postalCode = null,fullAddress;
        String stateName = null;
        try {
            List<Address> addresses = geocoder.getFromLocation(lat,lon, 1);
            if (addresses != null)
            {
                android.location.Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("Address:\n");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                cityName = addresses.get(0).getLocality();
                stateName = addresses.get(0).getAdminArea();
                addresses.get(0).getCountryCode();
                addresses.get(0).getCountryName();
                addresses.get(0).getPostalCode();

                if (isFromProfile){
                    Preference preference = new Preference(mContext);
                    preference.saveStringInPreference("PROFILE_LATLONG",lat+", "+lon);
                    preference.saveStringInPreference("PROFILE_ADDRESS", stateName+", "+cityName);
                    preference.saveStringInPreference("CITY_NAME", cityName);
                    preference.saveStringInPreference("STATE", stateName);
                    preference.commitPreference();
                }else if (isFromContactDetails){
                    Preference preference = new Preference(mContext);
                    preference.saveStringInPreference("CONTACT_LATLONG",lat+", "+lon);
                    preference.saveStringInPreference("CONTACT_ADDRESS", strReturnedAddress.toString());
                    preference.saveStringInPreference("CONTACT_COUNTRY_CODE", countryCode);
                    preference.saveStringInPreference("CONTACT_COUNTRY_NAME", countryName);
                    preference.saveStringInPreference("CONTACT_POST_CODE", postalCode);
                    preference.saveStringInPreference("CONTACT_CITY_NAME", cityName);
                    preference.saveStringInPreference("CONTACT_STATE", stateName);
                    preference.commitPreference();
                } else
                {
                    Preference preference = new Preference(LocationSearchActivity.this);
                    preference.saveStringInPreference("LOCALITY",stateName+", "+cityName);
                    // preference.saveStringInPreference("LATLONG",lat+", "+lon);
                    preference.saveStringInPreference("CURRENTLATLONG",lat+", "+lon);
                    preference.commitPreference();
                    tvLocation.setText(stateName+", "+cityName);
                }

                ret = strReturnedAddress.toString();
//                moveToNextScreen();
            } else {
                ret = "No Address returned!";
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = "Can't get Address!";
        }
        return ret;
    }

    public void getCityStateFromAddress(String address_) {
        if(address_.contains(",")) {
            String address1[] = address_.split(",");
            if (address1.length == 2) {
                address_ = address1[0] + " , " + address1[1];
            } else if (address1.length == 3) {
                address_ = address1[0] + " , " + address1[1];
            } else if (address1.length == 4) {
                address_ = address1[0] + " , " + address1[1];
            } else if (address1.length == 5) {
                address_ = address1[1] + " , " + address1[2];
            } else if (address1.length == 6) {
                address_ = address1[2] + " , " + address1[3];
            } else if (address1.length == 7) {
                address_ = address1[3] + " , " + address1[4];
            }
        }
    }

    @Override
    public void gotGpsValidationResponse(Object response, GPSErrorCode code)
    {

        //TODO Auto-generated method stub
        if(code == GPSErrorCode.EC_GPS_PROVIDER_NOT_ENABLED)
        {
            isGpsProviderEnabled = false;
            //showSettingsAlert();
        }
        else if(code == GPSErrorCode.EC_GPS_PROVIDER_ENABLED)
        {
            isGpsProviderEnabled = true;
        }
        else if(code == GPSErrorCode.EC_UNABLE_TO_FIND_LOCATION)
        {
            currentLatLng = (LatLng) response;
        }
        else if(code == GPSErrorCode.EC_LOCATION_FOUND)
        {
            currentLatLng = (LatLng) response;

        }
        else if(code == GPSErrorCode.EC_CUSTOMER_LOCATION_IS_VALID)
        {
        }
        else if(code == GPSErrorCode.EC_CUSTOMER_lOCATION_IS_INVAILD)
        {
        }

    }
    @Override
    protected void onStart()
    {
        super.onStart();
        gpsUtills.connectGoogleApiClient();
        mGoogleApiClient.connect();
        mGoogleApiClient1.connect();
        requestcancel= true;

    }

    @Override
    public void onStop()
    {
        super.onStop();
        gpsUtills.disConnectGoogleApiClient();
        mGoogleApiClient1.disconnect();

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        gpsUtills.stopLocationUpdates();
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        gpsUtills.startLocationUpdates();
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
            buildGoogleApiClient();
            createLocationRequest();
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        gpsUtills.stopLocationUpdates();
    }

    @Override
    public void setlocation(final String s)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvLocation.setText(s+" ");
                getLocationFromAddress(s);
            }
        });

    }

    public class getData extends AsyncTask<String, String, String> {

        HttpURLConnection urlConnection;

        @Override
        protected String doInBackground(String... args) {
            String url1 = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + args[0] + "&types=geocode&key=AIzaSyCwL6NhBOYgZaLsxmtZTU-DJkPbFRxGWyk";
            StringBuilder result = new StringBuilder();
            try {
                URL url = new URL(url1);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return result.toString();

        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                locations.clear();
                descrptions.clear();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);


                    for (int i = 0; i < jsonObject.getJSONArray("predictions").length(); i++) {
                        JSONObject area = jsonObject.getJSONArray("predictions").getJSONObject(i);

                        String arealo = area.getString("description");
                        descrptions.add(arealo);
                        String[] arealoc = arealo.split(",");
                        if (arealoc.length == 3) {
                            locations.add(arealoc[0]);
                        } else if (arealoc.length == 4) {
                            locations.add(arealoc[0] + " ," + arealoc[1]);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

    }

    public void getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        Barcode.GeoPoint p1 = null;

        try {
            address = coder.getFromLocationName(strAddress,1);
            if (address==null)
            {
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            Preference preference = new Preference(LocationSearchActivity.this);
            preference.saveStringInPreference("CURRENTLATLONG",location.getLatitude()+", "+location.getLongitude());
            preference.saveStringInPreference("LOCALITY",location.getLocality()+", "+location.getSubLocality());
            preference.commitPreference();
            finish();

        }catch (Exception e)
        {

        }
    }
    protected synchronized void buildGoogleApiClient()
    {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    protected void createLocationRequest()
    {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().setAlwaysShow(true);
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }
    protected void checkLocationSettings()
    {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(LocationSearchActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;


        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        islocation = true;
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        requestcancel = false;
                        islocation = false;
                        hideLoader();
                        break;
                }
                break;
        }
    }

    public void stopUpdatesButtonHandler(View view) {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        stopLocationUpdates();
    }
    protected void startLocationUpdates()
    {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
            }
        });

    }
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            if (islocation) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (mCurrentLocation.getLatitude() > 0.0 && mCurrentLocation.getLongitude() > 0.0) {
                            String latLng = mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude();
                            //tvLatLang.setText(latLng);
                            GetAddress(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    llTop.setVisibility(View.VISIBLE);
                                    hideLoader();
                                }
                            });

                        } else
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hideLoader();
                                }
                            });

                    }
                }, 2 * 1000);
                // String address = GetAddress(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
                //
                // Toast.makeText(getApplicationContext(), "addreess" + address, Toast.LENGTH_LONG).show();
                stopLocationUpdates();
            }
        }
    }
    protected void stopLocationUpdates()
    {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");
        requestcancel = true;

        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            updateLocationUI();
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
/*        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());*/
        updateLocationUI();

 /*       Toast.makeText(this, getResources().getString(R.string.location_updated_message),
                Toast.LENGTH_SHORT).show();*/
    }
    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }
}
