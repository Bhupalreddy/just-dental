package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import hCue.Hcue.DAO.CaseDoctorDetails;
import hCue.Hcue.DAO.CaseLabHistory;
import hCue.Hcue.DAO.DoctorSpecialisation;
import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.DAO.PatientDocuments;
import hCue.Hcue.DAO.PatientFileRow;
import hCue.Hcue.DAO.UpdatePatientRecordRequestDO;
import hCue.Hcue.DAO.UploadfileRequest;
import hCue.Hcue.Fragments.HealthRecordFragment;
import hCue.Hcue.Fragments.MedicationsFragment;
import hCue.Hcue.Fragments.MyVisitsFragment;
import hCue.Hcue.Fragments.TestResultsFragment;
import hCue.Hcue.model.FileDeleteReq;
import hCue.Hcue.model.PatientFilesResposne;
import hCue.Hcue.model.PatientVisitRespone;
import hCue.Hcue.model.PatientVisitdetailsReq;
import hCue.Hcue.model.PatientsVisitsRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LargeImageLoader;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.widget.FABToolbarLayout;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by shyamprasadg on 28/07/16.
 */
public class MyHealthRecords extends BaseActivity
{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private LinearLayout llPastRecords;
    private int REQUEST_CAMERA = 2;
    private int PICK_IMAGE_REQUEST = 1;
    private int PICK_FILE_REQUEST = 3;
    private long patinetid;
    private  ViewPagerAdapter adapter;

    @Override
    public void initialize() {
        ApplicationConstants.getDbAdapter(MyHealthRecords.this);
        llPastRecords = (LinearLayout) getLayoutInflater().inflate(R.layout.my_past_records, null, false);
        llBody.addView(llPastRecords, baseLayoutParams);
        llHealthRecords.setBackgroundResource(R.drawable.menu_selected_bg);
        tvHealthRecordsHighlight.setVisibility(View.VISIBLE);
//        ivHealthRecords.setImageResource(R.drawable.records_hov);
//        tvHealthRecords.setTextColor(getResources().getColor(R.color.colorPrimary));

        tvTopTitle.setText("RECORDS");
        patinetid = LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID();

        viewPager = (ViewPager) llPastRecords.findViewById(R.id.viewpager);

        tabLayout = (TabLayout) llPastRecords.findViewById(R.id.tabs);

        setSpecificTypeFace(llPastRecords, ApplicationConstants.WALSHEIM_SEMI_BOLD);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
        changeTabsFont();

        if (Connectivity.isConnected(MyHealthRecords.this)) {

        } else {
            ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
        }

        llBack.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    llBack.setBackgroundColor(Color.parseColor("#33000000"));

                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    llBack.setBackgroundColor(Color.parseColor("#00000000"));
                    Intent slideactivity = new Intent(MyHealthRecords.this, Specialities.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_left_in, R.anim.push_left_out).toBundle();
                    startActivity(slideactivity, bndlanimation);
                    finish();
                }
                return true;
            }
        });

        reminderManager = new ReminderManager(getApplicationContext());

    }

    ReminderManager reminderManager;

    private void changeTabsFont()
    {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                }
            }
        }
    }


    private void setupViewPager(ViewPager viewPager) {
         adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(MyVisitsFragment.getInstance(patinetid), "My Visits");
        adapter.addFragment(MedicationsFragment.getInstance(patinetid), "Medications");
        adapter.addFragment(TestResultsFragment.getInstance(patinetid), "Test Results");
        adapter.addFragment(HealthRecordFragment.getInstance(patinetid), "Health Records");
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onResume() {

        super.onResume();
//        mDbHelper.open();

    }

    @Override
    protected void onPause() {
        super.onPause();
//        mDbHelper.close();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    public void onBackPressed()
    {
        Intent slideactivity = new Intent(MyHealthRecords.this, Specialities.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
        slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(slideactivity, bndlanimation);
        finish();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            Fragment fr3 = adapter.getItem(3);
            ((HealthRecordFragment) fr3).PICK_FILE_REQUEST(data);

        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            Fragment fr3 = adapter.getItem(3);
            ((HealthRecordFragment) fr3).PICK_IMAGE_REQUEST(data);
        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK && data != null && data.getExtras() != null)
        {
            Fragment fr3 = adapter.getItem(3);
            ((HealthRecordFragment) fr3).REQUEST_CAMERA(data);
        }
    }




}
