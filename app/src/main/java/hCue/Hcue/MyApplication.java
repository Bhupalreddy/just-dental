package hCue.Hcue;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by Appdest on 03-10-2016.
 */

public class MyApplication extends Application
{
    private static Context mContext;
    public String identity ;
    public static MyApplication instace;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        instace = this;
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(getApplicationContext()));

    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public static MyApplication getIntance() {
        return instace;
    }
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }
}
