package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;

import hCue.Hcue.CallBackInterfaces.OnRegistrationStepOneCallBack;
import hCue.Hcue.DAO.CountryCodeDO;
import hCue.Hcue.adapters.CustomCountryListAdapter;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.widget.RippleView;

/**
 * Created by Appdest on 16-09-2016.
 */
@SuppressLint("ValidFragment")
public class RegisterStepFour extends Fragment {

    public OnRegistrationStepOneCallBack mCallBack;
    public Context context;
    private EditText etMobile;
    private Button btnContinue;
    private RippleView rvContinue;
    private TextView tvCountryCode;
    private ArrayList<CountryCodeDO> listcountryCodeDOs;
    private TextInputLayout tvMobile;
    public RegisterStepFour()
    {
        super();
    }

    public RegisterStepFour(Context context, OnRegistrationStepOneCallBack onRegistrationStepOneCallBack) {
        this.context = context;
        this.mCallBack = onRegistrationStepOneCallBack;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register_step_four, container, false);

        etMobile        =   (EditText)          v.findViewById(R.id.etMobile);
        tvCountryCode   =   (TextView)          v.findViewById(R.id.tvCountryCode);
        rvContinue      =   (RippleView)        v.findViewById(R.id.rvContinue);
        btnContinue     =   (Button)            v.findViewById(R.id.btnContinue);
        tvMobile        =   (TextInputLayout)   v.findViewById(R.id.tvMobile);

        etMobile.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvCountryCode.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        btnContinue.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvMobile.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        generateCountryCode();
        tvCountryCode.setText("+"+tvCountryCode.getText().toString());

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                validateInputFields();
            }
        });
        tvCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallBack = (OnRegistrationStepOneCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public void validateInputFields() {
        String mobile = etMobile.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)){
            ShowAlertDialog("Alert","Mobile Number should not be empty.",R.drawable.alert);
        }else if(mobile.length() !=10){
            ShowAlertDialog("Alert","Mobile number should be 10 digits.",R.drawable.alert);
        }else {
            RegistrationActivity.registrationDO.mobile = mobile;
            RegistrationActivity.registrationDO.countryCode = tvCountryCode.getText().toString().trim();
            mCallBack.onContinueClickListener(0);
        }
    }

    private void showDialog(){

        final Dialog dialog = new Dialog(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.country_list, null);

        ListView lv = (ListView) view.findViewById(R.id.custom_list);

        // Change MyActivity.this and myListOfItems to your own values
        CustomCountryListAdapter clad = new CustomCountryListAdapter(getActivity(), listcountryCodeDOs);

        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                tvCountryCode.setText("+"+listcountryCodeDOs.get(position).getCountrycode());
            }
        });

        dialog.setContentView(view);

        dialog.show();

    }


    public void generateCountryCode()
    {
        listcountryCodeDOs  =   new ArrayList<>();
        CountryCodeDO   doIndia =   new CountryCodeDO();
        doIndia.setCountrycode(+91);
        doIndia.setCountryName("India");
        listcountryCodeDOs.add(doIndia);

        CountryCodeDO   doSingapore =   new CountryCodeDO();
        doSingapore.setCountrycode(+65);
        doSingapore.setCountryName("Singapore");
        listcountryCodeDOs.add(doSingapore);

        CountryCodeDO   doBrazil =   new CountryCodeDO();
        doBrazil.setCountrycode(+55);
        doBrazil.setCountryName("Brazil");
        listcountryCodeDOs.add(doBrazil);

        CountryCodeDO   doIndonesia =   new CountryCodeDO();
        doIndonesia.setCountrycode(+62);
        doIndonesia.setCountryName("Indonesia");
        listcountryCodeDOs.add(doIndonesia);

        CountryCodeDO   doPhilippines =   new CountryCodeDO();
        doPhilippines.setCountrycode(+63);
        doPhilippines.setCountryName("Philippines");
        listcountryCodeDOs.add(doPhilippines);

        CountryCodeDO   doAfganistan =   new CountryCodeDO();
        doAfganistan.setCountrycode(+93);
        doAfganistan.setCountryName("Afganistan");
        listcountryCodeDOs.add(doAfganistan);

        CountryCodeDO   doAlbania =   new CountryCodeDO();
        doAlbania.setCountrycode(+355);
        doAlbania.setCountryName("Albania");
        listcountryCodeDOs.add(doAlbania);

        CountryCodeDO   doAlgeria =   new CountryCodeDO();
        doAlgeria.setCountrycode(+213);
        doAlgeria.setCountryName("Algeria");
        listcountryCodeDOs.add(doAlgeria);

        CountryCodeDO   doAmericanSamoa =   new CountryCodeDO();
        doAmericanSamoa.setCountrycode(+1684);
        doAmericanSamoa.setCountryName("AmericanSamoa");
        listcountryCodeDOs.add(doAmericanSamoa);

        CountryCodeDO   doAndorra =   new CountryCodeDO();
        doAndorra.setCountrycode(+376);
        doAndorra.setCountryName("Andorra");
        listcountryCodeDOs.add(doAndorra);

        CountryCodeDO   doAngola =   new CountryCodeDO();
        doAngola.setCountrycode(+244);
        doAngola.setCountryName("Angola");
        listcountryCodeDOs.add(doAngola);

        CountryCodeDO   doAnguilla =   new CountryCodeDO();
        doAnguilla.setCountrycode(+1264);
        doAnguilla.setCountryName("Anguilla");
        listcountryCodeDOs.add(doAnguilla);

        CountryCodeDO   doAntarctica =   new CountryCodeDO();
        doAntarctica.setCountrycode(+672);
        doAntarctica.setCountryName("Antarctica");
        listcountryCodeDOs.add(doAntarctica);

        CountryCodeDO   doAntiguaandBarbuda =   new CountryCodeDO();
        doAntiguaandBarbuda.setCountrycode(+1268);
        doAntiguaandBarbuda.setCountryName("Antigua and Barbuda");
        listcountryCodeDOs.add(doAntiguaandBarbuda);

        CountryCodeDO   doArgentina =   new CountryCodeDO();
        doArgentina.setCountrycode(+54);
        doArgentina.setCountryName("Argentina");
        listcountryCodeDOs.add(doArgentina);

        CountryCodeDO   doArmenia =   new CountryCodeDO();
        doArmenia.setCountryName("Armenia");
        doArmenia.setCountrycode(+374);
        listcountryCodeDOs.add(doArmenia);

        CountryCodeDO   doAruba =   new CountryCodeDO();
        doAruba.setCountryName("Aruba");
        doAruba.setCountrycode(+297);
        listcountryCodeDOs.add(doAruba);

        CountryCodeDO   doAustralia =   new CountryCodeDO();
        doAustralia.setCountryName("Australia");
        doAustralia.setCountrycode(+61);
        listcountryCodeDOs.add(doAustralia);

        CountryCodeDO   doAustria =   new CountryCodeDO();
        doAustria.setCountryName("Austria");
        doAustria.setCountrycode(+43);
        listcountryCodeDOs.add(doAustria);

        CountryCodeDO   doAzerbaijan =   new CountryCodeDO();
        doAzerbaijan.setCountryName("Azerbaijan");
        doAzerbaijan.setCountrycode(+994);
        listcountryCodeDOs.add(doAzerbaijan);

        CountryCodeDO   doBahamas =   new CountryCodeDO();
        doBahamas.setCountryName("Bahamas");
        doBahamas.setCountrycode(+1242);
        listcountryCodeDOs.add(doBahamas);

        CountryCodeDO   doBahrain =   new CountryCodeDO();
        doBahrain.setCountryName("Bahrain");
        doBahrain.setCountrycode(+973);
        listcountryCodeDOs.add(doBahrain);

        CountryCodeDO   doBangladesh =   new CountryCodeDO();
        doBangladesh.setCountryName("Bangladesh");
        doBangladesh.setCountrycode(+880);
        listcountryCodeDOs.add(doBangladesh);

        CountryCodeDO   doBarbados =   new CountryCodeDO();
        doBarbados.setCountryName("Barbados");
        doBarbados.setCountrycode(+1246);
        listcountryCodeDOs.add(doBarbados);

        CountryCodeDO   doBelarus =   new CountryCodeDO();
        doBelarus.setCountryName("Belarus");
        doBelarus.setCountrycode(+375);
        listcountryCodeDOs.add(doBelarus);

        CountryCodeDO   doBelguim =   new CountryCodeDO();
        doBelguim.setCountryName("Belguim");
        doBelguim.setCountrycode(+32);
        listcountryCodeDOs.add(doBelguim);

        CountryCodeDO   doBelize =   new CountryCodeDO();
        doBelize.setCountryName("Belize");
        doBelize.setCountrycode(+501);
        listcountryCodeDOs.add(doBelize);

        CountryCodeDO   doBenin =   new CountryCodeDO();
        doBenin.setCountryName("Benin");
        doBenin.setCountrycode(+229);
        listcountryCodeDOs.add(doBenin);

        CountryCodeDO   doBermuda =   new CountryCodeDO();
        doBermuda.setCountryName("Bermuda");
        doBermuda.setCountrycode(+1441);
        listcountryCodeDOs.add(doBermuda);

        CountryCodeDO   doBhutan =   new CountryCodeDO();
        doBhutan.setCountryName("Bhutan");
        doBhutan.setCountrycode(+975);
        listcountryCodeDOs.add(doBhutan);

        CountryCodeDO   doBolivia =   new CountryCodeDO();
        doBolivia.setCountryName("Bolivia, Plurinational State of");
        doBolivia.setCountrycode(+591);
        listcountryCodeDOs.add(doBolivia);

        CountryCodeDO   doBosina =   new CountryCodeDO();
        doBosina.setCountryName("Bosina and Herzegovina");
        doBosina.setCountrycode(+387);
        listcountryCodeDOs.add(doBosina);

        CountryCodeDO   doBotswana =   new CountryCodeDO();
        doBotswana.setCountryName("Botswana");
        doBotswana.setCountrycode(+267);
        listcountryCodeDOs.add(doBotswana);

        CountryCodeDO   doBritishIndian =   new CountryCodeDO();
        doBritishIndian.setCountryName("British Indian Ocean Territory");
        doBritishIndian.setCountrycode(+246);
        listcountryCodeDOs.add(doBritishIndian);

        CountryCodeDO   doBrunei =   new CountryCodeDO();
        doBrunei.setCountryName("Brunei Darussalam");
        doBrunei.setCountrycode(+673);
        listcountryCodeDOs.add(doBrunei);

        CountryCodeDO   doBulgaria =   new CountryCodeDO();
        doBulgaria.setCountryName("Bulgaria");
        doBulgaria.setCountrycode(+359);
        listcountryCodeDOs.add(doBulgaria);

        CountryCodeDO   doBurkina =   new CountryCodeDO();
        doBurkina.setCountryName("Burkina Faso");
        doBurkina.setCountrycode(+226);
        listcountryCodeDOs.add(doBurkina);

        CountryCodeDO   doBurundi =   new CountryCodeDO();
        doBurundi.setCountryName("Burundi");
        doBurundi.setCountrycode(+257);
        listcountryCodeDOs.add(doBurundi);

        CountryCodeDO   doCambodia =   new CountryCodeDO();
        doCambodia.setCountryName("Cambodia");
        doCambodia.setCountrycode(+855);
        listcountryCodeDOs.add(doCambodia);

        CountryCodeDO   doCameroon =   new CountryCodeDO();
        doCameroon.setCountryName("Cameroon");
        doCameroon.setCountrycode(+237);
        listcountryCodeDOs.add(doCameroon);

        CountryCodeDO   docanada =   new CountryCodeDO();
        docanada.setCountryName("canada");
        docanada.setCountrycode(+1);
        listcountryCodeDOs.add(docanada);

        CountryCodeDO   doCape =   new CountryCodeDO();
        doCape.setCountryName("Cape Verde");
        doCape.setCountrycode(+238);
        listcountryCodeDOs.add(doCape);

        CountryCodeDO   doCayman =   new CountryCodeDO();
        doCayman.setCountryName("Cayman Islands");
        doCayman.setCountrycode(+345);
        listcountryCodeDOs.add(doCayman);

        CountryCodeDO   doCentralAfrican =   new CountryCodeDO();
        doCentralAfrican.setCountryName("Central African Republic");
        doCentralAfrican.setCountrycode(+236);
        listcountryCodeDOs.add(doCentralAfrican);

        CountryCodeDO   doChad =   new CountryCodeDO();
        doChad.setCountryName("Chad");
        doChad.setCountrycode(+235);
        listcountryCodeDOs.add(doChad);

        CountryCodeDO   doChile =   new CountryCodeDO();
        listcountryCodeDOs.add(doArgentina);
        doChile.setCountryName("Chile");
        doChile.setCountrycode(+56);

        CountryCodeDO   doChina =   new CountryCodeDO();
        doChina.setCountryName("China");
        doChina.setCountrycode(+86);
        listcountryCodeDOs.add(doChina);

        CountryCodeDO   doChristmas =   new CountryCodeDO();
        doChristmas.setCountryName("Christmas Island");
        doChristmas.setCountrycode(+61);
        listcountryCodeDOs.add(doChristmas);

        CountryCodeDO   doCocos =   new CountryCodeDO();
        doCocos.setCountryName("Cocos (Keeling) Islands");
        doCocos.setCountrycode(+61);
        listcountryCodeDOs.add(doCocos);

        CountryCodeDO   doColombia =   new CountryCodeDO();
        doColombia.setCountryName("Colombia");
        doColombia.setCountrycode(+57);
        listcountryCodeDOs.add(doColombia);

        CountryCodeDO   doComoros =   new CountryCodeDO();
        doComoros.setCountryName("Comoros");
        doComoros.setCountrycode(+269);
        listcountryCodeDOs.add(doComoros);

        CountryCodeDO   doCongo =   new CountryCodeDO();
        doCongo.setCountryName("Congo");
        doCongo.setCountrycode(+242);
        listcountryCodeDOs.add(doCongo);

        CountryCodeDO   doCongoDemocratic =   new CountryCodeDO();
        doCongoDemocratic.setCountryName("Congo, The Democratic Republic of the");
        doCongoDemocratic.setCountrycode(+243);
        listcountryCodeDOs.add(doCongoDemocratic);

        CountryCodeDO   doCook =   new CountryCodeDO();
        doCook.setCountryName("Cook Islands");
        doCook.setCountrycode(+682);
        listcountryCodeDOs.add(doCook);

        CountryCodeDO   doCostaRica =   new CountryCodeDO();
        doCostaRica.setCountryName("Costa Rica");
        doCostaRica.setCountrycode(+506);
        listcountryCodeDOs.add(doCostaRica);

        CountryCodeDO   doCote =   new CountryCodeDO();
        doCote.setCountryName("Cote d'lvoire");
        doCote.setCountrycode(+225);
        listcountryCodeDOs.add(doCote);

        CountryCodeDO   doCroatia =   new CountryCodeDO();
        doCroatia.setCountryName("Croatia");
        doCroatia.setCountrycode(+385);
        listcountryCodeDOs.add(doCroatia);

        CountryCodeDO   doCuba =   new CountryCodeDO();
        doCuba.setCountryName("Cuba");
        doCuba.setCountrycode(+53);
        listcountryCodeDOs.add(doCuba);

        CountryCodeDO   doCyprus =   new CountryCodeDO();
        doCyprus.setCountryName("Cyprus");
        doCyprus.setCountrycode(+537);
        listcountryCodeDOs.add(doCyprus);

        CountryCodeDO   doCzechRepublic =   new CountryCodeDO();
        doCzechRepublic.setCountryName("Czech Republic");
        doCzechRepublic.setCountrycode(+420);
        listcountryCodeDOs.add(doCzechRepublic);

        CountryCodeDO doDenmark =   new CountryCodeDO();
        doDenmark.setCountryName("Denmark");
        doDenmark.setCountrycode(+45);
        listcountryCodeDOs.add(doDenmark);

        CountryCodeDO   doDjibouti =   new CountryCodeDO();
        doDjibouti.setCountryName("Djibouti");
        doDjibouti.setCountrycode(+253);
        listcountryCodeDOs.add(doDjibouti);

        CountryCodeDO   doDominica =   new CountryCodeDO();
        doDominica.setCountryName("Dominica");
        doDominica.setCountrycode(+1767);
        listcountryCodeDOs.add(doDominica);

        CountryCodeDO   doDominician =   new CountryCodeDO();
        doDominician.setCountryName("Dominician Republic");
        doDominician.setCountrycode(+1849);
        listcountryCodeDOs.add(doDominician);

        CountryCodeDO   doEcudor =   new CountryCodeDO();
        doEcudor.setCountryName("Ecudor");
        doEcudor.setCountrycode(+593);
        listcountryCodeDOs.add(doEcudor);

        CountryCodeDO   doEgypt =   new CountryCodeDO();
        doEgypt.setCountryName("Egypt");
        doEgypt.setCountrycode(+20);
        listcountryCodeDOs.add(doEgypt);

        CountryCodeDO   doElSalvador =   new CountryCodeDO();
        doElSalvador.setCountryName("El Salvador");
        doElSalvador.setCountrycode(+503);
        listcountryCodeDOs.add(doElSalvador);

        CountryCodeDO   doEquatorialGuinea =   new CountryCodeDO();
        doEquatorialGuinea.setCountryName("Equatorial Guinea");
        doEquatorialGuinea.setCountrycode(+240);
        listcountryCodeDOs.add(doEquatorialGuinea);

        CountryCodeDO   doEritrea =   new CountryCodeDO();
        doEritrea.setCountryName("Eritrea");
        doEritrea.setCountrycode(+291);
        listcountryCodeDOs.add(doEritrea);

        CountryCodeDO   doEstonia =   new CountryCodeDO();
        doEstonia.setCountryName("Estonia");
        doEstonia.setCountrycode(+372);
        listcountryCodeDOs.add(doEstonia);

        CountryCodeDO   doEthiopia =   new CountryCodeDO();
        doEthiopia.setCountryName("Ethiopia");
        doEthiopia.setCountrycode(+251);
        listcountryCodeDOs.add(doEthiopia);

        CountryCodeDO   doFalkland =   new CountryCodeDO();
        doFalkland.setCountryName("Falkland Islands (Malvinas)");
        doFalkland.setCountrycode(+500);
        listcountryCodeDOs.add(doFalkland);

        CountryCodeDO   doFaroe =   new CountryCodeDO();
        doFaroe.setCountryName("Faroe Islands");
        doFaroe.setCountrycode(+298);
        listcountryCodeDOs.add(doFaroe);

        CountryCodeDO   doFiji =   new CountryCodeDO();
        doFiji.setCountryName("Fiji");
        doFiji.setCountrycode(+679);
        listcountryCodeDOs.add(doFiji);

        CountryCodeDO   doFinland =   new CountryCodeDO();
        doFinland.setCountryName("Finland");
        doFinland.setCountrycode(+358);
        listcountryCodeDOs.add(doFinland);

        CountryCodeDO   doFrance =   new CountryCodeDO();
        doFrance.setCountryName("France");
        doFrance.setCountrycode(+33);
        listcountryCodeDOs.add(doFrance);

        CountryCodeDO   doFrench =   new CountryCodeDO();
        doFrench.setCountryName("French Guiana");
        doFrench.setCountrycode(+594);
        listcountryCodeDOs.add(doFrench);

        CountryCodeDO   doFrenchPolynesia =   new CountryCodeDO();
        doFrenchPolynesia.setCountryName("French Polynesia");
        doFrenchPolynesia.setCountrycode(+689);
        listcountryCodeDOs.add(doFrenchPolynesia);

        CountryCodeDO   doGabon =   new CountryCodeDO();
        doGabon.setCountryName("Gabon");
        doGabon.setCountrycode(+241);
        listcountryCodeDOs.add(doGabon);

        CountryCodeDO   doGambia =   new CountryCodeDO();
        doGambia.setCountryName("Gambia");
        doGambia.setCountrycode(+220);
        listcountryCodeDOs.add(doGambia);

        CountryCodeDO   doGeorgia =   new CountryCodeDO();
        doGeorgia.setCountryName("Georgia");
        doGeorgia.setCountrycode(+995);
        listcountryCodeDOs.add(doGeorgia);

        CountryCodeDO   doGermany =   new CountryCodeDO();
        doGermany.setCountryName("Germany");
        doGermany.setCountrycode(+49);
        listcountryCodeDOs.add(doGermany);

        CountryCodeDO   doGhana =   new CountryCodeDO();
        doGhana.setCountryName("Ghana");
        doGhana.setCountrycode(+233);
        listcountryCodeDOs.add(doGhana);

        CountryCodeDO   doGibraltar =   new CountryCodeDO();
        doGibraltar.setCountryName("Gibraltar");
        doGibraltar.setCountrycode(+350);
        listcountryCodeDOs.add(doGibraltar);

        CountryCodeDO   doGreece =   new CountryCodeDO();
        doGreece.setCountryName("Greece");
        doGreece.setCountrycode(+30);
        listcountryCodeDOs.add(doGreece);

        CountryCodeDO   doGreenland =   new CountryCodeDO();
        doGreenland.setCountryName("Greenland");
        doGreenland.setCountrycode(+299);
        listcountryCodeDOs.add(doGreenland);

        CountryCodeDO   doGreenada =   new CountryCodeDO();
        doGreenada.setCountryName("Greenada");
        doGreenada.setCountrycode(+1473);
        listcountryCodeDOs.add(doGreenada);

        CountryCodeDO   doguinea =   new CountryCodeDO();
        doguinea.setCountryName("Guinea");
        doguinea.setCountrycode(+224);
        listcountryCodeDOs.add(doguinea);

        CountryCodeDO   doGuadeloupe =   new CountryCodeDO();
        doGuadeloupe.setCountryName("Guadeloupe");
        doGuadeloupe.setCountrycode(+590);
        listcountryCodeDOs.add(doGuadeloupe);

        CountryCodeDO   doGuam =   new CountryCodeDO();
        doGuam.setCountryName("Guam");
        doGuam.setCountrycode(+1671);
        listcountryCodeDOs.add(doGuam);

        CountryCodeDO   doGuatemala =   new CountryCodeDO();
        doGuatemala.setCountryName("Guatemala");
        doGuatemala.setCountrycode(+502);
        listcountryCodeDOs.add(doGuatemala);

        CountryCodeDO   doGuernsey =   new CountryCodeDO();
        doGuernsey.setCountryName("Guernsey");
        doGuernsey.setCountrycode(+44);
        listcountryCodeDOs.add(doGuernsey);

        CountryCodeDO   doGuineaBissau =   new CountryCodeDO();
        doGuineaBissau.setCountryName("Guinea-Bissau");
        doGuineaBissau.setCountrycode(+245);
        listcountryCodeDOs.add(doGuineaBissau);

        CountryCodeDO   doGuyana =   new CountryCodeDO();
        doGuyana.setCountryName("Guyana");
        doGuyana.setCountrycode(+595);
        listcountryCodeDOs.add(doGuyana);

        CountryCodeDO   doHaiti =   new CountryCodeDO();
        doHaiti.setCountryName("Haiti");
        doHaiti.setCountrycode(+509);
        listcountryCodeDOs.add(doHaiti);

        CountryCodeDO   doHolySee =   new CountryCodeDO();
        doHolySee.setCountryName("Holy See (Vatican City State)");
        doHolySee.setCountrycode(+379);
        listcountryCodeDOs.add(doHolySee);

        CountryCodeDO   doHonduras =   new CountryCodeDO();
        doHonduras.setCountryName("Honduras");
        doHonduras.setCountrycode(+504);
        listcountryCodeDOs.add(doHonduras);

        CountryCodeDO   doHongKong =   new CountryCodeDO();
        doHongKong.setCountryName("Hong Kong");
        doHongKong.setCountrycode(+852);
        listcountryCodeDOs.add(doHongKong);

        CountryCodeDO   doHungary =   new CountryCodeDO();
        doHungary.setCountryName("Hungary");
        doHungary.setCountrycode(+36);
        listcountryCodeDOs.add(doHungary);

        CountryCodeDO   doIceland =   new CountryCodeDO();
        doIceland.setCountryName("Iceland");
        doIceland.setCountrycode(+354);
        listcountryCodeDOs.add(doIceland);

        CountryCodeDO   doIran =   new CountryCodeDO();
        doIran.setCountryName("Iran");
        doIran.setCountrycode(+98);
        listcountryCodeDOs.add(doIran);

        CountryCodeDO   doIreland =   new CountryCodeDO();
        doIreland.setCountryName("Ireland");
        doIreland.setCountrycode(+353);
        listcountryCodeDOs.add(doIreland);

        CountryCodeDO   doIsleofMan =   new CountryCodeDO();
        doIsleofMan.setCountryName("Isle of Man");
        doIsleofMan.setCountrycode(+44);
        listcountryCodeDOs.add(doIsleofMan);

        CountryCodeDO   doIsrael =   new CountryCodeDO();
        doIsrael.setCountryName("Israel");
        doIsrael.setCountrycode(+972);
        listcountryCodeDOs.add(doIsrael);


        CountryCodeDO   doItaly =   new CountryCodeDO();
        doItaly.setCountryName("Italy");
        doItaly.setCountrycode(+39);
        listcountryCodeDOs.add(doItaly);


        CountryCodeDO   doJamaica =   new CountryCodeDO();
        doJamaica.setCountryName("Jamaica");
        doJamaica.setCountrycode(+1876);
        listcountryCodeDOs.add(doJamaica);

        CountryCodeDO   doJapan =   new CountryCodeDO();
        doJapan.setCountryName("Japan");
        doJapan.setCountrycode(+81);
        listcountryCodeDOs.add(doJapan);

        CountryCodeDO   doJersey =   new CountryCodeDO();
        doJersey.setCountryName("Jersey");
        doJersey.setCountrycode(+44);
        listcountryCodeDOs.add(doJersey);

        CountryCodeDO   doJordan =   new CountryCodeDO();
        doJordan.setCountryName("Jordan");
        doJordan.setCountrycode(+962);
        listcountryCodeDOs.add(doJordan);

        CountryCodeDO   doKazakhstan =   new CountryCodeDO();
        doKazakhstan.setCountryName("Kazakhstan");
        doKazakhstan.setCountrycode(+77);
        listcountryCodeDOs.add(doKazakhstan);

        CountryCodeDO   doKenya =   new CountryCodeDO();
        doKenya.setCountryName("Kenya");
        doKenya.setCountrycode(+254);
        listcountryCodeDOs.add(doKenya);

        CountryCodeDO   doKiribati =   new CountryCodeDO();
        doKiribati.setCountryName("Kiribati");
        doKiribati.setCountrycode(+686);
        listcountryCodeDOs.add(doKiribati);

        CountryCodeDO   doKoreaDemocratic =   new CountryCodeDO();
        doKoreaDemocratic.setCountryName("Korea, Democratic People's Republic of");
        doKoreaDemocratic.setCountrycode(+850);
        listcountryCodeDOs.add(doKoreaDemocratic);

        CountryCodeDO   doKoreaRepublic =   new CountryCodeDO();
        doKoreaRepublic.setCountryName("Korea, Republic of");
        doKoreaRepublic.setCountrycode(+82);
        listcountryCodeDOs.add(doKoreaRepublic);

        CountryCodeDO   doKuwait =   new CountryCodeDO();
        doKuwait.setCountryName("Kuwait");
        doKuwait.setCountrycode(+965);
        listcountryCodeDOs.add(doKuwait);

        CountryCodeDO   doKyrgyzstan =   new CountryCodeDO();
        doKyrgyzstan.setCountryName("Kyrgyzstan");
        doKyrgyzstan.setCountrycode(+996);
        listcountryCodeDOs.add(doKyrgyzstan);

        CountryCodeDO   doLaoPeoples =   new CountryCodeDO();
        doLaoPeoples.setCountryName("Lao People's Democratic Republic");
        doLaoPeoples.setCountrycode(+856);
        listcountryCodeDOs.add(doLaoPeoples);

        CountryCodeDO   doLatvia =   new CountryCodeDO();
        doLatvia.setCountryName("Latvia");
        doLatvia.setCountrycode(+371);
        listcountryCodeDOs.add(doLatvia);

        CountryCodeDO   doLebanon =   new CountryCodeDO();
        doLebanon.setCountryName("Lebanon");
        doLebanon.setCountrycode(+961);
        listcountryCodeDOs.add(doLebanon);


        CountryCodeDO   doLesotho =   new CountryCodeDO();
        doLesotho.setCountryName("Lesotho");
        doLesotho.setCountrycode(+266);
        listcountryCodeDOs.add(doLesotho);

        CountryCodeDO   doLiberia =   new CountryCodeDO();
        doLiberia.setCountryName("Liberia");
        doLiberia.setCountrycode(+231);
        listcountryCodeDOs.add(doLiberia);

        CountryCodeDO   doLibyan =   new CountryCodeDO();
        doLibyan.setCountryName("Libyan Arab Jamahiriya");
        doLibyan.setCountrycode(+218);
        listcountryCodeDOs.add(doLibyan);

        CountryCodeDO   doLiechtenstein =   new CountryCodeDO();
        doLiechtenstein.setCountryName("Liechtenstein");
        doLiechtenstein.setCountrycode(+423);
        listcountryCodeDOs.add(doLiechtenstein);

        CountryCodeDO   doLithunia =   new CountryCodeDO();
        doLithunia.setCountryName("Lithunia");
        doLithunia.setCountrycode(+370);
        listcountryCodeDOs.add(doLithunia);

        CountryCodeDO   doLuxembourg =   new CountryCodeDO();
        doLuxembourg.setCountryName("Luxembourg");
        doLuxembourg.setCountrycode(+352);
        listcountryCodeDOs.add(doLuxembourg);

        CountryCodeDO   doMacao =   new CountryCodeDO();
        doMacao.setCountryName("Macao");
        doMacao.setCountrycode(+853);
        listcountryCodeDOs.add(doMacao);

        CountryCodeDO   doMacedoniaTheFormer =   new CountryCodeDO();
        doMacedoniaTheFormer.setCountryName("Macedonia, The Former Yugoslav Republic of");
        doMacedoniaTheFormer.setCountrycode(+389);
        listcountryCodeDOs.add(doMacedoniaTheFormer);

        CountryCodeDO   doMadagascar =   new CountryCodeDO();
        doMadagascar.setCountryName("Madagascar");
        doMadagascar.setCountrycode(+261);
        listcountryCodeDOs.add(doMadagascar);

        CountryCodeDO   doMalawi =   new CountryCodeDO();
        doMalawi.setCountryName("Malawi");
        doMalawi.setCountrycode(+265);
        listcountryCodeDOs.add(doMalawi);

        CountryCodeDO   doMalaysia =   new CountryCodeDO();
        doMalaysia.setCountryName("Malaysia");
        doMalaysia.setCountrycode(+60);
        listcountryCodeDOs.add(doMalaysia);

        CountryCodeDO   doMaldives =   new CountryCodeDO();
        doMaldives.setCountryName("Maldives");
        doMaldives.setCountrycode(+960);
        listcountryCodeDOs.add(doMaldives);

        CountryCodeDO   doMali =   new CountryCodeDO();
        doMali.setCountryName("Mali");
        doMali.setCountrycode(+223);
        listcountryCodeDOs.add(doMali);

        CountryCodeDO   doMalta =   new CountryCodeDO();
        doMalta.setCountryName("Malta");
        doMalta.setCountrycode(+356);
        listcountryCodeDOs.add(doMalta);

        CountryCodeDO   doMarshallIslands =   new CountryCodeDO();
        doMarshallIslands.setCountryName("Marshall Islands");
        doMarshallIslands.setCountrycode(+692);
        listcountryCodeDOs.add(doMarshallIslands);

        CountryCodeDO   doMartinque =   new CountryCodeDO();
        doMartinque.setCountryName("Martinque");
        doMartinque.setCountrycode(+596);
        listcountryCodeDOs.add(doMartinque);

        CountryCodeDO   doMauritania =   new CountryCodeDO();
        doMauritania.setCountryName("Mauritania");
        doMauritania.setCountrycode(+222);
        listcountryCodeDOs.add(doMauritania);

        CountryCodeDO   doMauritius =   new CountryCodeDO();
        doMauritius.setCountryName("Mauritius");
        doMauritius.setCountrycode(+230);
        listcountryCodeDOs.add(doMauritius);

        CountryCodeDO   doMayotte =   new CountryCodeDO();
        doMayotte.setCountryName("Mayotte");
        doMayotte.setCountrycode(+262);
        listcountryCodeDOs.add(doMayotte);

        CountryCodeDO   doMexico =   new CountryCodeDO();
        doMexico.setCountryName("Mexico");
        doMexico.setCountrycode(+52);
        listcountryCodeDOs.add(doMexico);

        CountryCodeDO   doMicronesiaF =   new CountryCodeDO();
        doMicronesiaF.setCountryName("Micronesia, Federated States of");
        doMicronesiaF.setCountrycode(+691);
        listcountryCodeDOs.add(doMicronesiaF);

        CountryCodeDO   doMoldovaR =   new CountryCodeDO();
        doMoldovaR.setCountryName("Moldova, Republic of");
        doMoldovaR.setCountrycode(+373);
        listcountryCodeDOs.add(doMoldovaR);

        CountryCodeDO   doMonaco =   new CountryCodeDO();
        doMonaco.setCountryName("Monaco");
        doMonaco.setCountrycode(+377);
        listcountryCodeDOs.add(doMonaco);

        CountryCodeDO   doMongolia =   new CountryCodeDO();
        doMongolia.setCountryName("Mongolia");
        doMongolia.setCountrycode(+976);
        listcountryCodeDOs.add(doMongolia);

        CountryCodeDO   doMontenegro =   new CountryCodeDO();
        doMontenegro.setCountryName("Montenegro");
        doMontenegro.setCountrycode(+382);
        listcountryCodeDOs.add(doMontenegro);

        CountryCodeDO   doMontserrat =   new CountryCodeDO();
        doMontserrat.setCountryName("Montserrat");
        doMontserrat.setCountrycode(+1664);
        listcountryCodeDOs.add(doMontserrat);

        CountryCodeDO   doMorocco =   new CountryCodeDO();
        doMorocco.setCountryName("Morocco");
        doMorocco.setCountrycode(+212);
        listcountryCodeDOs.add(doMorocco);

        CountryCodeDO   doMozambique =   new CountryCodeDO();
        doMozambique.setCountryName("Mozambique");
        doMozambique.setCountrycode(+258);
        listcountryCodeDOs.add(doMozambique);

        CountryCodeDO   doMyanmar =   new CountryCodeDO();
        doMyanmar.setCountryName("Myanmar");
        doMyanmar.setCountrycode(+95);
        listcountryCodeDOs.add(doMyanmar);

        CountryCodeDO   doNamibia =   new CountryCodeDO();
        doNamibia.setCountryName("Namibia");
        doNamibia.setCountrycode(+264);
        listcountryCodeDOs.add(doNamibia);

        CountryCodeDO   doNauru =   new CountryCodeDO();
        doNauru.setCountryName("Nauru");
        doNauru.setCountrycode(+674);
        listcountryCodeDOs.add(doNauru);

        CountryCodeDO   doNepal =   new CountryCodeDO();
        doNepal.setCountryName("Nepal");
        doNepal.setCountrycode(+977);
        listcountryCodeDOs.add(doNepal);

        CountryCodeDO   doNetharlands =   new CountryCodeDO();
        doNetharlands.setCountryName("Netharlands");
        doNetharlands.setCountrycode(+31);
        listcountryCodeDOs.add(doNetharlands);

        CountryCodeDO   doNetharlandsAntilles =   new CountryCodeDO();
        doNetharlandsAntilles.setCountryName("Netharlands Antilles");
        doNetharlandsAntilles.setCountrycode(+599);
        listcountryCodeDOs.add(doNetharlandsAntilles);

        CountryCodeDO   doNewCaledonia =   new CountryCodeDO();
        doNewCaledonia.setCountryName("New Caledonia");
        doNewCaledonia.setCountrycode(+687);
        listcountryCodeDOs.add(doNewCaledonia);

        CountryCodeDO   doNewXealand =   new CountryCodeDO();
        doNewXealand.setCountryName("New Zealand");
        doNewXealand.setCountrycode(+64);
        listcountryCodeDOs.add(doNewXealand);

        CountryCodeDO   doNicaragua =   new CountryCodeDO();
        doNicaragua.setCountryName("Nicaragua");
        doNicaragua.setCountrycode(+505);
        listcountryCodeDOs.add(doNicaragua);

        CountryCodeDO   doNiger =   new CountryCodeDO();
        doNiger.setCountryName("Niger");
        doNiger.setCountrycode(+227);
        listcountryCodeDOs.add(doNiger);

        CountryCodeDO   doNigeria =   new CountryCodeDO();
        doNigeria.setCountryName("Nigeria");
        doNigeria.setCountrycode(+234);
        listcountryCodeDOs.add(doNigeria);

        CountryCodeDO   doNiue =   new CountryCodeDO();
        doNiue.setCountryName("Niue");
        doNiue.setCountrycode(+683);
        listcountryCodeDOs.add(doNiue);

        CountryCodeDO   doNorfolkIsland =   new CountryCodeDO();
        doNorfolkIsland.setCountryName("Norfolk Island");
        doNorfolkIsland.setCountrycode(+672);
        listcountryCodeDOs.add(doNorfolkIsland);

        CountryCodeDO   doNorthernMariana =   new CountryCodeDO();
        doNorthernMariana.setCountryName("Northern Mariana Islands");
        doNorthernMariana.setCountrycode(+1670);
        listcountryCodeDOs.add(doNorthernMariana);

        CountryCodeDO   doNorway =   new CountryCodeDO();
        doNorway.setCountryName("Norway");
        doNorway.setCountrycode(+47);
        listcountryCodeDOs.add(doNorway);

        CountryCodeDO   doOman =   new CountryCodeDO();
        doOman.setCountryName("Oman");
        doOman.setCountrycode(+968);
        listcountryCodeDOs.add(doOman);

        CountryCodeDO   doPakistan =   new CountryCodeDO();
        doPakistan.setCountryName("Pakistan");
        doPakistan.setCountrycode(+92);
        listcountryCodeDOs.add(doPakistan);

        CountryCodeDO   doPalau =   new CountryCodeDO();
        doPalau.setCountryName("Palau");
        doPalau.setCountrycode(+680);
        listcountryCodeDOs.add(doPalau);

        CountryCodeDO   doPalestinianTerritory =   new CountryCodeDO();
        doPalestinianTerritory.setCountryName("Palestinian Territory, Occupied");
        doPalestinianTerritory.setCountrycode(+970);
        listcountryCodeDOs.add(doPalestinianTerritory);

        CountryCodeDO   doPapuaNewGuinea =   new CountryCodeDO();
        doPapuaNewGuinea.setCountryName("Papua New Guinea");
        doPapuaNewGuinea.setCountrycode(+675);
        listcountryCodeDOs.add(doPapuaNewGuinea);

        CountryCodeDO   doParaguay =   new CountryCodeDO();
        doParaguay.setCountryName("Paraguay");
        doParaguay.setCountrycode(+595);
        listcountryCodeDOs.add(doParaguay);

        CountryCodeDO   doPeru =   new CountryCodeDO();
        doPeru.setCountryName("Peru");
        doPeru.setCountrycode(+51);
        listcountryCodeDOs.add(doPeru);

        CountryCodeDO   doPitcairn =   new CountryCodeDO();
        doPitcairn.setCountryName("Pitcairn");
        doPitcairn.setCountrycode(+872);
        listcountryCodeDOs.add(doPitcairn);

        CountryCodeDO   doPoland =   new CountryCodeDO();
        doPoland.setCountryName("Poland");
        doPoland.setCountrycode(+48);
        listcountryCodeDOs.add(doPoland);

        CountryCodeDO   doPortugal =   new CountryCodeDO();
        doPortugal.setCountryName("Portugal");
        doPortugal.setCountrycode(+351);
        listcountryCodeDOs.add(doPortugal);

        CountryCodeDO   doPuertoRico =   new CountryCodeDO();
        doPuertoRico.setCountryName("Puerto Rico");
        doPuertoRico.setCountrycode(+1939);
        listcountryCodeDOs.add(doPuertoRico);

        CountryCodeDO   doQatar =   new CountryCodeDO();
        doQatar.setCountryName("Qatar");
        doQatar.setCountrycode(+974);
        listcountryCodeDOs.add(doQatar);

        CountryCodeDO   doRomania =   new CountryCodeDO();
        doRomania.setCountryName("Romania");
        doRomania.setCountrycode(+40);
        listcountryCodeDOs.add(doRomania);

        CountryCodeDO   doRussia =   new CountryCodeDO();
        doRussia.setCountryName("Russia");
        doRussia.setCountrycode(+7);
        listcountryCodeDOs.add(doRussia);

        CountryCodeDO   doRwanda =   new CountryCodeDO();
        doRwanda.setCountryName("Rwanda");
        doRwanda.setCountrycode(+250);
        listcountryCodeDOs.add(doRwanda);

        CountryCodeDO   doReunion =   new CountryCodeDO();
        doReunion.setCountryName("Reunion");
        doReunion.setCountrycode(+262);
        listcountryCodeDOs.add(doReunion);

        CountryCodeDO   doSaintBarthelemy =   new CountryCodeDO();
        doSaintBarthelemy.setCountryName("Saint Barthelemy");
        doSaintBarthelemy.setCountrycode(+590);
        listcountryCodeDOs.add(doSaintBarthelemy);

        CountryCodeDO   doSaintHelena =   new CountryCodeDO();
        doSaintHelena.setCountryName("Saint Helena, Ascension and Tristan Da Cunha");
        doSaintHelena.setCountrycode(+290);
        listcountryCodeDOs.add(doSaintHelena);

        CountryCodeDO   doSaintKitts =   new CountryCodeDO();
        doSaintKitts.setCountryName("Saint Kitts and Nevis");
        doSaintKitts.setCountrycode(+1869);
        listcountryCodeDOs.add(doSaintKitts);

        CountryCodeDO   doSaintLucia =   new CountryCodeDO();
        doSaintLucia.setCountryName("Saint Lucia");
        doSaintLucia.setCountrycode(+1758);
        listcountryCodeDOs.add(doSaintLucia);

        CountryCodeDO   doSaintMartin =   new CountryCodeDO();
        doSaintMartin.setCountryName("Saint Martin");
        doSaintMartin.setCountrycode(+590);
        listcountryCodeDOs.add(doSaintMartin);

        CountryCodeDO   doSaintPierre =   new CountryCodeDO();
        doSaintPierre.setCountryName("Saint Pierre and Miquelon");
        doSaintPierre.setCountrycode(+508);
        listcountryCodeDOs.add(doSaintPierre);

        CountryCodeDO   doSaintVincent =   new CountryCodeDO();
        doSaintVincent.setCountryName("Saint Vincent and the Grenadines");
        doSaintVincent.setCountrycode(+1784);
        listcountryCodeDOs.add(doSaintVincent);

        CountryCodeDO   doSamoa =   new CountryCodeDO();
        doSamoa.setCountryName("Samoa");
        doSamoa.setCountrycode(+685);
        listcountryCodeDOs.add(doSamoa);

        CountryCodeDO   doSanMarino =   new CountryCodeDO();
        doSanMarino.setCountryName("San Marino");
        doSanMarino.setCountrycode(+378);
        listcountryCodeDOs.add(doSanMarino);

        CountryCodeDO   doSaoTome =   new CountryCodeDO();
        doSaoTome.setCountryName("Sao Tome and Principe");
        doSaoTome.setCountrycode(+239);
        listcountryCodeDOs.add(doSaoTome);

        CountryCodeDO   doSaudiArabia =   new CountryCodeDO();
        doSaudiArabia.setCountryName("Saudi Arabia");
        doSaudiArabia.setCountrycode(+966);
        listcountryCodeDOs.add(doSaudiArabia);

        CountryCodeDO   doSenegal =   new CountryCodeDO();
        doSenegal.setCountryName("Senegal");
        doSenegal.setCountrycode(+221);
        listcountryCodeDOs.add(doSenegal);

        CountryCodeDO   doSerbia =   new CountryCodeDO();
        doSerbia.setCountryName("Serbia");
        doSerbia.setCountrycode(+381);
        listcountryCodeDOs.add(doSerbia);

        CountryCodeDO   doSeychelles =   new CountryCodeDO();
        doSeychelles.setCountryName("Seychelles");
        doSeychelles.setCountrycode(+248);
        listcountryCodeDOs.add(doSeychelles);

        CountryCodeDO   doSierraLeone =   new CountryCodeDO();
        doSierraLeone.setCountryName("Sierra Leone");
        doSierraLeone.setCountrycode(+232);
        listcountryCodeDOs.add(doSierraLeone);

        CountryCodeDO   doSlovenia =   new CountryCodeDO();
        doSlovenia.setCountryName("Slovenia");
        doSlovenia.setCountrycode(+386);
        listcountryCodeDOs.add(doSlovenia);

        CountryCodeDO   doSlovakia =   new CountryCodeDO();
        doSlovakia.setCountryName("Slovakia");
        doSlovakia.setCountrycode(+421);
        listcountryCodeDOs.add(doSlovakia);

        CountryCodeDO   doSolomonIslands =   new CountryCodeDO();
        doSolomonIslands.setCountryName("Solomon Islands");
        doSolomonIslands.setCountrycode(+677);
        listcountryCodeDOs.add(doSolomonIslands);

        CountryCodeDO   doSomalia =   new CountryCodeDO();
        doSomalia.setCountryName("Somalia");
        doSomalia.setCountrycode(+252);
        listcountryCodeDOs.add(doSomalia);

        CountryCodeDO   doSouthAfrica =   new CountryCodeDO();
        doSouthAfrica.setCountryName("South Africa");
        doSouthAfrica.setCountrycode(+27);
        listcountryCodeDOs.add(doSouthAfrica);

        CountryCodeDO   doSouthGeorgia =   new CountryCodeDO();
        doSouthGeorgia.setCountryName("South Georgia and the South Sandwich Islands");
        doSouthGeorgia.setCountrycode(+500);
        listcountryCodeDOs.add(doSouthGeorgia);

        CountryCodeDO   doSpain =   new CountryCodeDO();
        doSpain.setCountryName("Spain");
        doSpain.setCountrycode(+34);
        listcountryCodeDOs.add(doSpain);

        CountryCodeDO   doSriLanka =   new CountryCodeDO();
        doSriLanka.setCountryName("Sri Lanka");
        doSriLanka.setCountrycode(+94);
        listcountryCodeDOs.add(doSriLanka);

        CountryCodeDO   doSudan =   new CountryCodeDO();
        doSudan.setCountryName("Sudan");
        doSudan.setCountrycode(+249);
        listcountryCodeDOs.add(doSudan);

        CountryCodeDO   doSuriname =   new CountryCodeDO();
        doSuriname.setCountryName("Suriname");
        doSuriname.setCountrycode(+597);
        listcountryCodeDOs.add(doSuriname);

        CountryCodeDO   doSvalbardand =   new CountryCodeDO();
        doSvalbardand.setCountryName("Svalbard and Jan Mayen");
        doSvalbardand.setCountrycode(+47);
        listcountryCodeDOs.add(doSvalbardand);

        CountryCodeDO   doSwaziland =   new CountryCodeDO();
        doSwaziland.setCountryName("Swaziland");
        doSwaziland.setCountrycode(+268);
        listcountryCodeDOs.add(doSwaziland);

        CountryCodeDO   doSweden =   new CountryCodeDO();
        doSweden.setCountryName("Sweden");
        doSweden.setCountrycode(+46);
        listcountryCodeDOs.add(doSweden);


        CountryCodeDO   doSwitzerland =   new CountryCodeDO();
        doSwitzerland.setCountryName("Switzerland");
        doSwitzerland.setCountrycode(+41);
        listcountryCodeDOs.add(doSwitzerland);

        CountryCodeDO   doSyrianArab =   new CountryCodeDO();
        doSyrianArab.setCountryName("Syrian Arab Republic");
        doSyrianArab.setCountrycode(+963);
        listcountryCodeDOs.add(doSyrianArab);

        CountryCodeDO   doTaiwanProvince =   new CountryCodeDO();
        doTaiwanProvince.setCountryName("Taiwan, Province of China");
        doTaiwanProvince.setCountrycode(+886);
        listcountryCodeDOs.add(doTaiwanProvince);


        CountryCodeDO   doTajikistan =   new CountryCodeDO();
        doTajikistan.setCountryName("Tajikistan");
        doTajikistan.setCountrycode(+992);
        listcountryCodeDOs.add(doTajikistan);

        CountryCodeDO   doTanzaniaUnitedRepublic =   new CountryCodeDO();
        doTanzaniaUnitedRepublic.setCountryName("Tanzania, United Republic of");
        doTanzaniaUnitedRepublic.setCountrycode(+255);
        listcountryCodeDOs.add(doTanzaniaUnitedRepublic);

        CountryCodeDO   doThailand =   new CountryCodeDO();
        doThailand.setCountryName("Thailand");
        doThailand.setCountrycode(+66);
        listcountryCodeDOs.add(doThailand);

        CountryCodeDO   doTimor_Leste =   new CountryCodeDO();
        doTimor_Leste.setCountryName("Timor-Leste");
        doTimor_Leste.setCountrycode(+670);
        listcountryCodeDOs.add(doTimor_Leste);

        CountryCodeDO   doTogo =   new CountryCodeDO();
        doTogo.setCountryName("Togo");
        doTogo.setCountrycode(+228);
        listcountryCodeDOs.add(doTogo);

        CountryCodeDO   doTokelau =   new CountryCodeDO();
        doTokelau.setCountryName("Tokelau");
        doTokelau.setCountrycode(+690);
        listcountryCodeDOs.add(doTokelau);


        CountryCodeDO   doTonga =   new CountryCodeDO();
        doTonga.setCountryName("Tonga");
        doTonga.setCountrycode(+676);
        listcountryCodeDOs.add(doTonga);

        CountryCodeDO   doTrinidadandTobago =   new CountryCodeDO();
        doTrinidadandTobago.setCountryName("Trinidad and Tobago");
        doTrinidadandTobago.setCountrycode(+1868);
        listcountryCodeDOs.add(doTrinidadandTobago);

        CountryCodeDO   doTunisia =   new CountryCodeDO();
        doTunisia.setCountryName("Tunisia");
        doTunisia.setCountrycode(+216);
        listcountryCodeDOs.add(doTunisia);

        CountryCodeDO   doTurkey =   new CountryCodeDO();
        doTurkey.setCountryName("Turkey");
        doTurkey.setCountrycode(+90);
        listcountryCodeDOs.add(doTurkey);

        CountryCodeDO   doTurkmenistan =   new CountryCodeDO();
        doTurkmenistan.setCountryName("Turkmenistan");
        doTurkmenistan.setCountrycode(+993);
        listcountryCodeDOs.add(doTurkmenistan);

        CountryCodeDO   doTurksandCaicos =   new CountryCodeDO();
        doTurksandCaicos.setCountryName("Turks and Caicos Islands");
        doTurksandCaicos.setCountrycode(+1649);
        listcountryCodeDOs.add(doTurksandCaicos);

        CountryCodeDO   doTuvalu =   new CountryCodeDO();
        doTuvalu.setCountryName("Tuvalu");
        doTuvalu.setCountrycode(+688);
        listcountryCodeDOs.add(doTuvalu);

        CountryCodeDO   doUganda =   new CountryCodeDO();
        doUganda.setCountryName("Uganda");
        doUganda.setCountrycode(+256);
        listcountryCodeDOs.add(doUganda);

        CountryCodeDO   doUkraine =   new CountryCodeDO();
        doUkraine.setCountryName("Ukraine");
        doUkraine.setCountrycode(+380);
        listcountryCodeDOs.add(doUkraine);

        CountryCodeDO   doUAE =   new CountryCodeDO();
        doUAE.setCountryName("United Arab Emirates");
        doUAE.setCountrycode(+971);
        listcountryCodeDOs.add(doUAE);

        CountryCodeDO   doUK =   new CountryCodeDO();
        doUK.setCountryName("United Kingdom");
        doUK.setCountrycode(+44);
        listcountryCodeDOs.add(doUK);

        CountryCodeDO   doUS =   new CountryCodeDO();
        doUS.setCountryName("United States");
        doUS.setCountrycode(+1);
        listcountryCodeDOs.add(doUS);

        CountryCodeDO   doUruguay =   new CountryCodeDO();
        doUruguay.setCountryName("Uruguay");
        doUruguay.setCountrycode(+598);
        listcountryCodeDOs.add(doUruguay);

        CountryCodeDO   doUzbekistan =   new CountryCodeDO();
        doUzbekistan.setCountryName("Uzbekistan");
        doUzbekistan.setCountrycode(+998);
        listcountryCodeDOs.add(doUzbekistan);

        CountryCodeDO   doVanuatu =   new CountryCodeDO();
        doVanuatu.setCountryName("Vanuatu");
        doVanuatu.setCountrycode(+678);
        listcountryCodeDOs.add(doVanuatu);

        CountryCodeDO   doVenezuelaBolivarian =   new CountryCodeDO();
        doVenezuelaBolivarian.setCountryName("Venezuela, Bolivarian Republic of");
        doVenezuelaBolivarian.setCountrycode(+58);
        listcountryCodeDOs.add(doVenezuelaBolivarian);

        CountryCodeDO   doVietNam =   new CountryCodeDO();
        doVietNam.setCountryName("Viet Nam");
        doVietNam.setCountrycode(+84);
        listcountryCodeDOs.add(doVietNam);

        CountryCodeDO   doVirginIslands =   new CountryCodeDO();
        doVirginIslands.setCountryName("Virgin Islands, British");
        doVirginIslands.setCountrycode(+1284);
        listcountryCodeDOs.add(doVirginIslands);

        CountryCodeDO   doVirginIslandsUS =   new CountryCodeDO();
        doVirginIslandsUS.setCountryName("Virgin Islands, U.S.");
        doVirginIslandsUS.setCountrycode(+1340);
        listcountryCodeDOs.add(doVirginIslandsUS);

        CountryCodeDO   doWallisandFutuna =   new CountryCodeDO();
        doWallisandFutuna.setCountryName("Wallis and Futuna");
        doWallisandFutuna.setCountrycode(+681);
        listcountryCodeDOs.add(doWallisandFutuna);

        CountryCodeDO   doYemen =   new CountryCodeDO();
        doYemen.setCountryName("Yemen");
        doYemen.setCountrycode(+967);
        listcountryCodeDOs.add(doYemen);

        CountryCodeDO   doZambia =   new CountryCodeDO();
        doZambia.setCountryName("Zambia");
        doZambia.setCountrycode(+260);
        listcountryCodeDOs.add(doZambia);

        CountryCodeDO   doZimbabwe =   new CountryCodeDO();
        doZimbabwe.setCountryName("Zimbabwe");
        doZimbabwe.setCountrycode(+263);
        listcountryCodeDOs.add(doZimbabwe);
    }

    public void ShowAlertDialog(String header, String message, int resourceid)
    {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
        final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        final TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        ivLogo.setBackgroundResource(resourceid);
        tvInfo.setTextSize(18);

        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.VISIBLE);

        tvNo.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        tvYes.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });


    }
}