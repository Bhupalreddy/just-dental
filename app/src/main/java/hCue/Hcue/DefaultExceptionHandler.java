package hCue.Hcue;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Appdest on 03-10-2016.
 */

public class DefaultExceptionHandler implements Thread.UncaughtExceptionHandler{
    private Thread.UncaughtExceptionHandler defaultUEH;
    Context activity;

    public DefaultExceptionHandler(Context activity) {
        this.activity = activity;
    }

    @SuppressWarnings("WrongConstant")
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        ex.printStackTrace();
        Intent intent = new Intent(activity, SplashActivityOne.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                MyApplication.getIntance().getBaseContext(), 0, intent, intent.getFlags());

        //Following code will restart your application after 2 seconds
        AlarmManager mgr = (AlarmManager) MyApplication.getIntance().getBaseContext()
                .getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000,
                pendingIntent);

        //This will finish your activity manually
        //activity.finish();

        //This will stop your application and take out from it.
        System.exit(2);

    }
}
