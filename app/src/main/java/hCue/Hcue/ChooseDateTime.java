package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Locale;

import hCue.Hcue.DAO.AppointmentRows;
import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.SlotList;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.adapters.TimeSlotAdapter;
import hCue.Hcue.model.GetAvailableAppointmentsReq;
import hCue.Hcue.model.GetAvailableAppointmentsRes;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.SelectedConsSingleton;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 23/06/16.
 */
public class ChooseDateTime extends BaseActivity
{
    private RelativeLayout  rlChooseDateTime;
    private LinearLayout    llAvailability, llSLots, llevening, llmorning, llrightarrow, llleftarrow;
    private GridView        gvMorningSlots,gvEveningSlots;
    private TextView        tvDoctorname, tvDoctorQualification, tvspeciality, tvClinic,tvNotAvailable,tvSubmit,tvDate,
            tvNoEveSlots, tvNoMorSlots, tvNextAvailalbe, tvnextdate, tvnextday;
    private ImageView       ivDoctorPic;
    private TimeSlotAdapter morningslotAdapter, eveningSlotAdapter;
    private SpecialityResRow specialityResRow ;
    private ArrayList<SlotList> morningslotLists, eveningslotLists;
    private Calendar selecteddate , selecteddate1;
    private SimpleDateFormat simpleDateformat = new SimpleDateFormat("dd MMM yyyy (EEEE)");
    private LinearLayout llevngslotsnotfound , llmrngslotsnotfound , llnotimeslots;
    private boolean isMrngslotsAvaulable, isEvngSlotsAvailable ;
    private boolean hasmultipleusers ;
    private ArrayList<PatientRow> listPatientRows ;
    private PopupWindow popupWindow ;
    private PatientRow selectedPatienRow ;
    private boolean isRightClicked = true ;
    private boolean isFromReschedule = false;
    private long appointmentID;
    private char appointmentStatus;
    private Double currentlat, currentlong ;
    @Override
    public void initialize()
    {
        rlChooseDateTime    =   (RelativeLayout)getLayoutInflater().inflate(R.layout.choose_date_time_new,null,false);
        llBody.addView(rlChooseDateTime, baseLayoutParams);

        selecteddate   = Calendar.getInstance();
        selecteddate1  = Calendar.getInstance();
        specialityResRow = (SpecialityResRow) getIntent().getSerializableExtra("SELECTED_DATAILS");
        if (getIntent().hasExtra("ISFROMRESCHEDULE")) {
            isFromReschedule = getIntent().getBooleanExtra("ISFROMRESCHEDULE", false);
            appointmentID = getIntent().getLongExtra("AppointmentID", 0);
            appointmentStatus = getIntent().getCharExtra("AppointmentStatus", '0');
        }


        tvnextdate              = (TextView)        rlChooseDateTime.findViewById(R.id.tvnextdate);
        tvnextday               = (TextView)        rlChooseDateTime.findViewById(R.id.tvnextday);
        tvNoMorSlots            = (TextView)        rlChooseDateTime.findViewById(R.id.tvNoMorSlots);
        tvNoEveSlots            = (TextView)        rlChooseDateTime.findViewById(R.id.tvNoEveSlots);
        tvDoctorname            = (TextView)        rlChooseDateTime.findViewById(R.id.tvDoctorname);
        tvDoctorQualification   = (TextView)        rlChooseDateTime.findViewById(R.id.tvDoctorQualification);
        tvspeciality            = (TextView)        rlChooseDateTime.findViewById(R.id.tvspeciality);
        tvClinic                = (TextView)        rlChooseDateTime.findViewById(R.id.tvClinic);
        tvNotAvailable          = (TextView)        rlChooseDateTime.findViewById(R.id.tvNotAvailable);
        tvSubmit                = (TextView)        rlChooseDateTime.findViewById(R.id.tvSubmit);
        tvDate                  = (TextView)        rlChooseDateTime.findViewById(R.id.tvDate);
        tvNextAvailalbe         = (TextView)        rlChooseDateTime.findViewById(R.id.tvNextAvailalbe);

        llevngslotsnotfound     = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llevngslotsnotfound);
        llmrngslotsnotfound     = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llmrngslotsnotfound);
        llmorning               = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llmorning);
        llevening               = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llevening);
        llAvailability          = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llAvailability);
        llSLots                 = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llSLots);
        llrightarrow            = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llrightarrow);
        llleftarrow             = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llleftarrow);
        llnotimeslots           = (LinearLayout)    rlChooseDateTime.findViewById(R.id.llnotimeslots);


        gvMorningSlots          = (GridView)        rlChooseDateTime.findViewById(R.id.gvMorningSlots);
        gvEveningSlots          = (GridView)        rlChooseDateTime.findViewById(R.id.gvEveningSlots);

        ivDoctorPic             = (ImageView)       rlChooseDateTime.findViewById(R.id.ivDoctorPic);

        tvTopTitle.setText("BOOK APPOINTMENT");
        llleftarrow.setVisibility(View.INVISIBLE);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        setSpecificTypeFace(rlChooseDateTime, ApplicationConstants.WALSHEIM_MEDIUM);
        tvSubmit.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvNoEveSlots.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvNoMorSlots.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        tvDate.setText(simpleDateformat.format(selecteddate.getTime()));
        if(specialityResRow != null)
        {
            if(specialityResRow.getProfileImage() != null && !specialityResRow.getProfileImage().isEmpty()) {
                Picasso.with(this).load(Uri.parse(specialityResRow.getProfileImage())).into(ivDoctorPic);
            }
            else
            {
                if(specialityResRow.getGender() == 'M')
                {
                    Picasso.with(this).load(R.drawable.male).into(ivDoctorPic);
                }
                else
                {
                    Picasso.with(this).load(R.drawable.female).into(ivDoctorPic);
                }
            }
            if(specialityResRow.getFullName().startsWith("Dr."))
                tvDoctorname.setText(specialityResRow.getFullName());
            else
                tvDoctorname.setText("Dr. "+specialityResRow.getFullName());
            tvClinic.setText(specialityResRow.getClinicName());
            Gson gson = new Gson();
            LinkedHashMap<String, String> speciality = gson.fromJson(specialityResRow.getSpecialityID(), new TypeToken<LinkedHashMap<String, String>>(){}.getType());
            StringBuilder specialitybuilder = new StringBuilder();
            for(String specialityid : speciality.values())
            {
                specialitybuilder.append(Constants.specialitiesMap.get(specialityid)+", ");
            }
            tvspeciality.setText(specialitybuilder.toString().substring(0, (specialitybuilder.toString().length()-2)));
        }
        SelectedConsSingleton.getSingleton().setSelected_date(selecteddate);
        morningslotAdapter         =   new TimeSlotAdapter(ChooseDateTime.this , morningslotLists,tvSubmit);
        eveningSlotAdapter         =   new TimeSlotAdapter(ChooseDateTime.this, eveningslotLists,tvSubmit);
        gvMorningSlots.setAdapter(morningslotAdapter);
        gvEveningSlots.setAdapter(eveningSlotAdapter);


        llleftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRightClicked = false ;
                SelectedConsSingleton.getSingleton().setStarttime(null);
                SelectedConsSingleton.getSingleton().setEndtime(null);
                isMrngslotsAvaulable = false;
                isEvngSlotsAvailable = false ;
                gvEveningSlots.setVisibility(View.GONE);
                gvMorningSlots.setVisibility(View.GONE);
                llevngslotsnotfound.setVisibility(View.GONE);
                llmrngslotsnotfound.setVisibility(View.GONE);
                selecteddate.add(Calendar.DATE,-1);
                SelectedConsSingleton.getSingleton().setSelected_date(selecteddate);
                if(DateUtils.isToday(selecteddate))
                {
                    llleftarrow.setVisibility(View.INVISIBLE);
                }
                tvDate.setText(simpleDateformat.format(selecteddate.getTime()));
                callgetAvailableAppointmentsService();
            }
        });

        llrightarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRightClicked = true ;
                SelectedConsSingleton.getSingleton().setStarttime(null);
                SelectedConsSingleton.getSingleton().setEndtime(null);
                isMrngslotsAvaulable = false;
                isEvngSlotsAvailable = false ;
                gvEveningSlots.setVisibility(View.GONE);
                gvMorningSlots.setVisibility(View.GONE);
                llevngslotsnotfound.setVisibility(View.GONE);
                llmrngslotsnotfound.setVisibility(View.GONE);
                llleftarrow.setVisibility(View.VISIBLE);
                selecteddate.add(Calendar.DATE,+1);
                SelectedConsSingleton.getSingleton().setSelected_date(selecteddate);
                tvDate.setText(simpleDateformat.format(selecteddate.getTime()));
                callgetAvailableAppointmentsService();
            }
        });

        tvNextAvailalbe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(ChooseDateTime.this))
                {
                    selecteddate.setTime(selecteddate1.getTime());
                    SelectedConsSingleton.getSingleton().setSelected_date(selecteddate);
                    if(DateUtils.isToday(selecteddate))
                    {
                        llleftarrow.setVisibility(View.INVISIBLE);
                    }else
                    {
                        llleftarrow.setVisibility(View.VISIBLE);
                    }
                    tvDate.setText(simpleDateformat.format(selecteddate.getTime()));
                    callgetAvailableAppointmentsService();
                }else
                {
                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                }
            }
        });


        if (Connectivity.isConnected(ChooseDateTime.this))
        {
            callgetAvailableAppointmentsService();
        }else
        {
            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
        }


        llmorning.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                clearBackground();
                llmorning.setBackgroundResource(R.drawable.morning_selected);
                gvEveningSlots.setVisibility(View.GONE);
                llevngslotsnotfound.setVisibility(View.GONE);
                if(isMrngslotsAvaulable) {
                    gvMorningSlots.setVisibility(View.VISIBLE);
                    morningslotAdapter.removeviewAdapter();
                    eveningSlotAdapter.removeviewAdapter();
                }
                else {
                    llmrngslotsnotfound.setVisibility(View.VISIBLE);
                }
            }
        });
        llevening.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                clearBackground();
                llevening.setBackgroundResource(R.drawable.evening_selected);
                gvMorningSlots.setVisibility(View.GONE);
                llmrngslotsnotfound.setVisibility(View.GONE);
                if(isEvngSlotsAvailable) {
                    gvEveningSlots.setVisibility(View.VISIBLE);
                    morningslotAdapter.removeviewAdapter();
                    eveningSlotAdapter.removeviewAdapter();
                }
                else
                {
                    llevngslotsnotfound.setVisibility(View.VISIBLE);
                }
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(SelectedConsSingleton.getSingleton().getStarttime() ==null)
                {
                    ShowAlertDialog("Alert","Please select your preferred time for appointment.",R.drawable.alert);
                }else
                if (Connectivity.isConnected(ChooseDateTime.this))
                {
                    hasmultipleusers = false;
                    movetoConfirmappointment();
                }else
                {
                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                }

            }
        });


    }

    private void callgetPatientsService()
    {
        showLoader("");
        GetPatientsRequest getPatientsRequest = new GetPatientsRequest();
        getPatientsRequest.setPhoneNumber((LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFamilyHdID()+"").substring(0,10));
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(getPatientsRequest, new RestCallback<GetPatientsResponse>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
                hasmultipleusers = false ;
                movetoConfirmappointment();
            }

            @Override
            public void success(GetPatientsResponse getPatientsResponse, Response response) {
                hideLoader();
                if(getPatientsResponse != null)
                {
                    if(getPatientsResponse.getCount()==0 || getPatientsResponse.getCount()==1)
                    {
                        hasmultipleusers = false;
                        movetoConfirmappointment();
                    }else
                    {
                        hasmultipleusers = true;
                        listPatientRows = getPatientsResponse.getPatientRows();
                        selectedPatienRow = listPatientRows.get(0);
                        showDropdownPatients();
                    }
                }
            }
        });
    }
    private void showDropdownPatients()
    {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.patientlist_custom, null);
        ListView lvpatients = (ListView) dialoglayout.findViewById(R.id.lvpatients);
        TextView tvchoose   = (TextView) dialoglayout.findViewById(R.id.tvchoose);
        TextView tvHeading   = (TextView) dialoglayout.findViewById(R.id.tvHeading);

        tvchoose.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialoglayout);
        final PatientsAdapter patientsAdapter = new PatientsAdapter();
        lvpatients.setAdapter(patientsAdapter);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        tvchoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                selectedPatienRow = listPatientRows.get(patientsAdapter.getSelectedposition());
                movetoConfirmappointment();
            }
        });
    }

    public class PatientsAdapter extends BaseAdapter
    {

        private int selectedposition ;
        @Override
        public int getCount() {
            return listPatientRows.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            convertView = LayoutInflater.from(ChooseDateTime.this).inflate(R.layout.choose_patient,null);

            PatientRow patientRow = listPatientRows.get(position);
            RadioButton rbPeople    =   (RadioButton) convertView.findViewById(R.id.rbPeople);
            rbPeople.setText(patientRow.getArrPatients().get(0).getFullName());
            rbPeople.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            if(getSelectedposition() == position)
            {
                rbPeople.setChecked(true);
            }else
            {
                rbPeople.setChecked(false);
            }

            rbPeople.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelectedposition(position);
                    notifyDataSetChanged();
                }
            });

            return convertView;
        }

        public int getSelectedposition() {
            return selectedposition;
        }

        public void setSelectedposition(int selectedposition) {
            this.selectedposition = selectedposition;
        }
    }


    private void movetoConfirmappointment()
    {
        if(SelectedConsSingleton.getSingleton().getStarttime() ==null)
        {
            ShowAlertDialog("Alert","Please select your preferred time for appointment.",R.drawable.alert);
        }else
        {
            SelectedConsSingleton.getSingleton().setSelected_date(selecteddate);

            Intent slideactivity = new Intent(ChooseDateTime.this, ReasonToVisit.class);
            if(hasmultipleusers && selectedPatienRow != null)
            {
                slideactivity.putExtra("SELECTEDPATIENT",selectedPatienRow) ;
            }
            Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
            slideactivity.putExtra("SELECTED_DATAILS", specialityResRow);
            slideactivity.putExtra("ISFROMRESCHEDULE", isFromReschedule);
            slideactivity.putExtra("AppointmentID", appointmentID);
            slideactivity.putExtra("AppointmentStatus", appointmentStatus);
            startActivity(slideactivity, bndlanimation);
        }
    }

    private void callgetAvailableAppointmentsService()
    {

        showLoader("");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        GetAvailableAppointmentsReq availableAppointmentsReq = new GetAvailableAppointmentsReq();
        availableAppointmentsReq.setAddressID(specialityResRow.getAddressID());
        availableAppointmentsReq.setDoctorID(specialityResRow.getDoctorID());
        availableAppointmentsReq.setFilterByDate(simpleDateFormat.format(selecteddate.getTime()));
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getAvailableAppointments(availableAppointmentsReq, new RestCallback<GetAvailableAppointmentsRes>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(GetAvailableAppointmentsRes getAvailableAppointmentsRes, Response response) {
                hideLoader();
                ArrayList<SlotList> morningslotlists = new ArrayList<SlotList>();
                ArrayList<SlotList> eveningslotlists = new ArrayList<SlotList>();
                if(getAvailableAppointmentsRes != null && getAvailableAppointmentsRes.getArrappointmentRows()!= null)
                {
                    if(!getAvailableAppointmentsRes.getArrappointmentRows().isEmpty())
                    {
                        for(AppointmentRows appointmentRows :getAvailableAppointmentsRes.getArrappointmentRows())
                            for(SlotList slotList :appointmentRows.getListslotLists())
                            {
                                if(appointmentRows.getHospitalInfo()!= null && appointmentRows.getHospitalInfo().getHospitalCD()!=null)
                                    slotList.setHospitalCode(appointmentRows.getHospitalInfo().getHospitalCD());
                                if(appointmentRows.getHospitalInfo()!= null && appointmentRows.getHospitalInfo().getBranchCD()!=null)
                                    slotList.setBranchCode(appointmentRows.getHospitalInfo().getBranchCD());
                                if(appointmentRows.getHospitalInfo()!= null && appointmentRows.getHospitalInfo().getHospitalID()!=0)
                                    slotList.setHospitalID(appointmentRows.getHospitalInfo().getHospitalID());
                                if(appointmentRows.getHospitalInfo()!= null && appointmentRows.getHospitalInfo().getParentHospitalID() != 0)
                                    slotList.setBranchCode(appointmentRows.getHospitalInfo().getBranchCD());
                                slotList.setAddressConsultid(appointmentRows.getAddressConsultID());
                                String hour = slotList.getStartTime().split(":")[0];
                                if(Integer.valueOf(hour) >13)
                                {
                                    eveningslotlists.add(slotList) ;
                                }
                                else
                                {
                                    morningslotlists.add(slotList) ;
                                }
                            }
                        if(morningslotlists.isEmpty())
                        {
                            isMrngslotsAvaulable = false ;
                        }else
                        {
                            isMrngslotsAvaulable = true ;
                            morningslotAdapter.refreshAdapter(morningslotlists, getAvailableAppointmentsRes.getArrappointmentRows().get(0).getAddressConsultID());
                        }
                        if(eveningslotlists.isEmpty())
                        {
                            isEvngSlotsAvailable = false ;
                        }else
                        {
                            isEvngSlotsAvailable = true ;
                            eveningSlotAdapter.refreshAdapter(eveningslotlists, getAvailableAppointmentsRes.getArrappointmentRows().get(0).getAddressConsultID());
                        }
                        if(!isMrngslotsAvaulable && !isEvngSlotsAvailable)
                        {
                            selecteddate1.setTime(selecteddate.getTime());
                            handlenotimeslots();
                            return ;
                        }else
                        {
                            llnotimeslots.setVisibility(View.GONE);
                            tvNextAvailalbe.setVisibility(View.GONE);
                            llSLots.setVisibility(View.VISIBLE);
                        }
                    }else
                    {
                        selecteddate1.setTime(selecteddate.getTime());
                        handlenotimeslots();
                        return ;
                    }
                }else
                {
                    selecteddate1.setTime(selecteddate.getTime());
                    handlenotimeslots();
                    return ;
                }
                Calendar calendar1 = Calendar.getInstance(Locale.getDefault());
                int hour = calendar1.get(Calendar.HOUR_OF_DAY);
                if(DateUtils.isToday(selecteddate))
                {
                    if (hour > 13) {
                        llevening.performClick();
                    } else {
                        llmorning.performClick();
                    }
                }else
                {
                    if(isMrngslotsAvaulable)
                        llmorning.performClick();
                    else
                        llevening.performClick();
                }
            }
        });
    }

    private void handlenotimeslots()
    {
        /*if(DateUtils.isToday(selecteddate) && !isRightClicked)
        {
            isRightClicked = true;
            selecteddate1.add(Calendar.DATE, +1);
            callgetAvailableAppointmentsService();
        }else*/
        {

            SelectedConsSingleton.getSingleton().setStarttime(null);
            tvSubmit.setVisibility(View.GONE);
            if (isRightClicked)
                selecteddate1.add(Calendar.DATE, +1);
            else
                selecteddate1.add(Calendar.DATE, -1);
              {
                //llleftarrow.setVisibility(View.VISIBLE);
                  if(DateUtils.isBeforeDay(selecteddate1, Calendar.getInstance()))
                  {
                      hideLoader();
                      isRightClicked = true;
                      selecteddate1.setTime(Calendar.getInstance().getTime());
                      llnotimeslots.setVisibility(View.VISIBLE);
                      tvNextAvailalbe.setVisibility(View.VISIBLE);
                      llSLots.setVisibility(View.GONE);
                      SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMMM yyy y");
                      SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE");
                      tvNextAvailalbe.setText("Next Available Date");
                      tvnextdate.setText("");
                      tvnextday.setText("");
                      tvDate.setText(simpleDateformat.format(selecteddate1.getTime()));
                      return;
                  }
                  if (DateUtils.isToday(selecteddate1))
                  {
                      llleftarrow.setVisibility(View.INVISIBLE);
                  }
                showLoader("");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                GetAvailableAppointmentsReq availableAppointmentsReq = new GetAvailableAppointmentsReq();
                availableAppointmentsReq.setAddressID(specialityResRow.getAddressID());
                availableAppointmentsReq.setDoctorID(specialityResRow.getDoctorID());
                availableAppointmentsReq.setFilterByDate(simpleDateFormat.format(selecteddate1.getTime()));
                RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getAvailableAppointments(availableAppointmentsReq, new RestCallback<GetAvailableAppointmentsRes>() {
                    @Override
                    public void failure(RestError restError) {
                        hideLoader();
                    }

                    @Override
                    public void success(GetAvailableAppointmentsRes getAvailableAppointmentsRes, Response response) {
                        ArrayList<SlotList> morningslotlists = new ArrayList<SlotList>();
                        ArrayList<SlotList> eveningslotlists = new ArrayList<SlotList>();
                        if (getAvailableAppointmentsRes != null && getAvailableAppointmentsRes.getArrappointmentRows() != null) {
                            if (!getAvailableAppointmentsRes.getArrappointmentRows().isEmpty()) {

                                {
                                    hideLoader();
                                    llnotimeslots.setVisibility(View.VISIBLE);
                                    tvNextAvailalbe.setVisibility(View.VISIBLE);
                                    llSLots.setVisibility(View.GONE);
                                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMMM yyyy");
                                    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE");
                                    tvNextAvailalbe.setText("Go to " + simpleDateFormat1.format(selecteddate1.getTime()));
                                    tvnextdate.setText(simpleDateFormat1.format(selecteddate1.getTime()));
                                    tvnextday.setText(" [" + simpleDateFormat2.format(selecteddate1.getTime()) + "]");
                                }
                            } else {

                                handlenotimeslots();
                            }
                        } else {
                            hideLoader();
                            handlenotimeslots();
                        }
                    }
                });
            }
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //SelectedConsSingleton.getSingleton().setStarttime(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SelectedConsSingleton.getSingleton().setStarttime(null);
        finish();
    }

    public void clearBackground()
    {
        llmorning.setBackgroundResource(R.drawable.morning_unselected);
        llevening.setBackgroundResource(R.drawable.evening_unselected);
    }

}
