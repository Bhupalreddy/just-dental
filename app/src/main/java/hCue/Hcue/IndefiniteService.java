package hCue.Hcue;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.io.File;
import java.io.IOException;

/**
 * Created by bhupalreddy on 15/10/16.
 */



public class IndefiniteService extends Service implements Runnable{

    private static final String TAG = "MyAlarmService";

    private Thread thread = null;
    private boolean running ;

    private File rootImage;


    public void setRunning(boolean running) {
        this.running = running;
    }

    private void startThread(){
        thread = new Thread(this);
        thread.start();
    }

    private void stopThread(){
        setRunning(false);
    }

    @Override
    public void onCreate() {
// TODO Auto-generated method stub


    }

    @Override
    public IBinder onBind(Intent intent) {
// TODO Auto-generated method stub

        return null;
    }

    @Override
    public void onDestroy() {
// TODO Auto-generated method stub
        super.onDestroy();
        stopThread();

    }

    @Override
    public void onStart(Intent intent, int startId) {
// TODO Auto-generated method stub
        super.onStart(intent, startId);
        thread = new Thread(this);
        startThread();

    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void run(){
        while(true) {
            try {
                Thread.sleep(15000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}