package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;


import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.widget.RippleView;

/**
 * Created by shyamprasadg on 23/06/16.
 */
public class BookingSucess extends BaseActivity
{
    private LinearLayout llSucess;
    private Button btnMyAppointments;
    private RippleView rvMyAppointments;
    private SpecialityResRow specialityResRow;
    private PatientRow selectedPatienRow;
    private String reasontovisit;
    @Override
    public void initialize()
    {
        llSucess    =  (LinearLayout) getLayoutInflater().inflate(R.layout.booking_sucess,null,false);
        llBody.addView(llSucess,baseLayoutParams);

        specialityResRow = (SpecialityResRow) getIntent().getSerializableExtra("SELECTED_DATAILS");
        selectedPatienRow = (PatientRow) getIntent().getSerializableExtra("SELECTEDPATIENT");
        reasontovisit = getIntent().getStringExtra("REASONTOVISIT");

        btnMyAppointments   =   (Button)        llSucess.findViewById(R.id.btnMyAppointments);

        rvMyAppointments    =   (RippleView)    llSucess.findViewById(R.id.rvMyAppointments);

        setSpecificTypeFace(llSucess, ApplicationConstants.WALSHEIM_MEDIUM);

        btnMyAppointments.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        llTop.setVisibility(View.GONE);
        flBottomBar.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent slideactivity = new Intent(BookingSucess.this, AppointmentDetails.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_up_in,R.anim.push_up_out).toBundle();
                slideactivity.putExtra("SELECTED_DATAILS",specialityResRow);
                slideactivity.putExtra("SELECTEDPATIENT",selectedPatienRow);
                slideactivity.putExtra("REASONTOVISIT",reasontovisit);
                slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(slideactivity, bndlanimation);
                finish();
            }
        }, 800);

        rvMyAppointments.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView)
            {

            }
        });


    }

    @Override
    public void onBackPressed() {
    }
}
