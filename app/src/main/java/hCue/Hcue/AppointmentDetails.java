package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.SelectedConsSingleton;

/**
 * Created by shyamprasadg on 27/07/16.
 */
public class AppointmentDetails extends BaseActivity
{
    private RelativeLayout rlAppointment;
    private TextView tvDoctorname,tvToken,tvDateTime,tvPatientName,tvPhoneNo,tvClinic,tvDoctorAddress,tvCallNow,tvDirections,tvKM;
    private Button   btnConfirm;
    private SpecialityResRow specialityResRow;
    private PatientRow selectedPatienRow;
    private Patient patient;
    @Override
    public void initialize()
    {
        rlAppointment   = (RelativeLayout) getLayoutInflater().inflate(R.layout.appointment_details,null,false);
        llBody.addView(rlAppointment);

        specialityResRow = (SpecialityResRow) getIntent().getSerializableExtra("SELECTED_DATAILS");
        selectedPatienRow = (PatientRow) getIntent().getSerializableExtra("SELECTEDPATIENT");

        llLeftMenu.setVisibility(View.INVISIBLE);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        tvDoctorname        =   (TextView)  rlAppointment.findViewById(R.id.tvDoctorname);
        tvToken             =   (TextView)  rlAppointment.findViewById(R.id.tvToken);
        tvDateTime          =   (TextView)  rlAppointment.findViewById(R.id.tvDateTime);
        tvPatientName       =   (TextView)  rlAppointment.findViewById(R.id.tvPatientName);
        tvPhoneNo           =   (TextView)  rlAppointment.findViewById(R.id.tvPhoneNo);
        tvClinic            =   (TextView)  rlAppointment.findViewById(R.id.tvClinic);
        tvDoctorAddress     =   (TextView)  rlAppointment.findViewById(R.id.tvDoctorAddress);
        tvCallNow           =   (TextView)  rlAppointment.findViewById(R.id.tvCallNow);
        tvDirections        =   (TextView)  rlAppointment.findViewById(R.id.tvDirections);
        tvKM                =   (TextView)  rlAppointment.findViewById(R.id.tvKM);

        btnConfirm          =   (Button)    rlAppointment.findViewById(R.id.btnConfirm);

        setSpecificTypeFace(rlAppointment, ApplicationConstants.WALSHEIM_MEDIUM);
        flBottomBar.setVisibility(View.GONE);
        tvTopTitle.setText("APPOINTMENT DETAILS");
        Calendar calendar = SelectedConsSingleton.getSingleton().getSelected_date();
        int year = calendar.get(Calendar.YEAR);;
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int week = calendar.get(Calendar.DAY_OF_WEEK);

        String strMonth = DateUtils.getMonthInShortForm(month);
        String strDay = day + DateUtils.getExtension(day);
        String strWeek = DateUtils.getWeekInShortForm(week);

        tvDateTime.setText(strDay.toLowerCase()+" "+strMonth+" "+year +" "+strWeek+ " - " + SelectedConsSingleton.getSingleton().getStarttime()+" Hrs");

        if (selectedPatienRow == null)
            patient = LoginResponseSingleton.getSingleton().getArrPatients().get(0);
        else
            patient = selectedPatienRow.getArrPatients().get(0);
        tvPatientName.setText(patient.getFullName());
        tvPhoneNo.setText(LoginResponseSingleton.getSingleton().getListPatientPhone().get(0).getPhNumber()+"");
        tvToken.setText(""+SelectedConsSingleton.getSingleton().getTokennumber());


        if (specialityResRow != null) {
            //initData();
            /*if (specialityResRow.getProfileImage() != null && !specialityResRow.getProfileImage().isEmpty())
                Picasso.with(AppointmentDetails.this).load(Uri.parse(specialityResRow.getProfileImage())).into(ivDoctorPic);
            else
                Picasso.with(this).load(R.drawable.profileimg_male).into(ivDoctorPic);*/
            if(specialityResRow.getFullName().startsWith("Dr."))
                tvDoctorname.setText(specialityResRow.getFullName());
            else
                tvDoctorname.setText("Dr. "+specialityResRow.getFullName());
            //tvDoctorQualification.setText(specialityResRow.getQualification());
            //tvDoctorQualification.setVisibility(View.GONE);
            Gson gson = new Gson();
            LinkedHashMap<String, String> speciality = gson.fromJson(specialityResRow.getSpecialityID(), new TypeToken<LinkedHashMap<String, String>>() {
            }.getType());
            StringBuilder specialitybuilder = new StringBuilder();
            for(String specialityid : speciality.values())
            {
                specialitybuilder.append(Constants.specialitiesMap.get(specialityid)+", ");
            }
            if (specialityResRow.getDistance() != null) {
                if (specialityResRow.getDistance().length() >= 4)
                    tvKM.setText(specialityResRow.getDistance().substring(0, 4) + " kms" + " - from you");
                else
                    tvKM.setText(specialityResRow.getDistance() + " kms" + " - from you");
            }else
             tvKM.setVisibility(View.INVISIBLE);
            //tvspeciality.setText(specialitybuilder.toString().substring(0, (specialitybuilder.toString().length()-2)));
            //tvspeciality.setText(specialityResRow.getSpecialityID()+"");
            tvClinic.setText(specialityResRow.getClinicName());
            //tvDoctorEmail.setText(specialityResRow.getEmailID());
            //tvDoctorNo.setText(specialityResRow.getPhoneNumber());
            tvDoctorAddress.setText(specialityResRow.getAddress());
            tvCallNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + specialityResRow.getPhoneNumber()));
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", specialityResRow.getPhoneNumber(), null));
                    startActivity(intent);
                }
            });
            tvDirections.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Uri location = Uri.parse(String.format("http://maps.google.com/?daddr="+specialityResRow.getLatitude()+","+ specialityResRow.getLongitude()));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                    PackageManager packageManager = getPackageManager();
                    List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                    boolean isIntentSafe = activities.size() > 0;

                    if (isIntentSafe) {
                        startActivity(mapIntent);
                    }
                    else
                    {
                        ShowAlertDialog("Whoops!","Doctor Latlongs not available", R.drawable.alert);
                    }
                }
            });
        }
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectedConsSingleton.resetInstance();Intent slideactivity = new Intent(AppointmentDetails.this, Specialities.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_up_in,R.anim.push_up_out).toBundle();
                slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(slideactivity, bndlanimation);
                finish();
            }
        });


    }

    /*private void setUpMap(LatLng doctorLatLng) {
        if (doctorLatLng == null) {
            latLng = new LatLng(Double.parseDouble(specialityResRow.getLatitude()), Double.parseDouble(specialityResRow.getLongitude()));
        } else {
            latLng = doctorLatLng;
        }

        mapView.addMarker(new MarkerOptions().position(latLng));
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 14.0f);
        mapView.moveCamera(yourLocation);


        mapPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*Intent myIntent = new Intent(Intent.ACTION_VIEW,
                        ContentURI.create("http://maps.google.com/?daddr=53.316518,-1.029282"));
                startActivity(myIntent);*//*

                Uri location = Uri.parse(String.format("http://maps.google.com/?daddr=" + latLng.latitude + "," + latLng.longitude));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                boolean isIntentSafe = activities.size() > 0;

                if (isIntentSafe) {
                    startActivity(mapIntent);
                } else {
                    //showCustomDialog(RestaurantFullViewActivity.this, "Alert!", "No map applications installed on phone", "Ok", "", "");
                }
            }
        });

        mapView.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mapView.snapshot(new GoogleMap.SnapshotReadyCallback() {

                    @Override
                    public void onSnapshotReady(Bitmap bitmap) {
                        fMapView.removeViewAt(0);
                        mapPreview.setLayoutParams(new FrameLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        mapPreview.setImageBitmap(bitmap);
                        //						llLoader.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    private void initData() {
        setUpMap(null);
    }*/

    @Override
    public void onBackPressed() {
    }
}
