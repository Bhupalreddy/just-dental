package hCue.Hcue;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import hCue.Hcue.DAO.DoctorSpecialityDO;
import hCue.Hcue.DAO.SpecialityDAO;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.adapters.BookingAdapter;
import hCue.Hcue.model.AdditionalFiltersDO;
import hCue.Hcue.model.SearchBySpecialityReq;
import hCue.Hcue.model.SearchBySpecialityRes;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.MyautoScroolView;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.utils.SelectedConsSingleton;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 22/06/16.
 */
public class BookAppointment extends BaseActivity implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener
{

    private LinearLayout    llBooking,llExperiance,llDistance,llMapView,llAvailability,llCompare,llDoctorCard;
    private TextView        tvDistance,tvDoctorname,tvDoctorQualification,tvspecialist,tvHospitalName,tvExperince,tvFee,tvSymbol,tvBookAppointment,tvDis,tvExp,tvArea,tvNotAvailable;
    private ImageView       ivExpArrow,ivDisArrow,ivExpDownArrow,ivDisDownArrow,ivDoctorPic,ivExpArrowDown, ivDisArrowDown, ivNotAvailable;
    private ListView        lvBooking;
    private BookingAdapter  bookingAdapter;
    private BookingAdapter  bookingAdapter2;
    private ArrayList<SpecialityResRow> specialityResRowArrayList ;
    public String searchvalue="";
    private FrameLayout fMapView;
    private GoogleMap mapView;
    private LatLng latLng;
    private ImageView mapPreview;
    private Vector<Marker> veMarkers;
    private SpecialityResRow specialityResRow ;
    private Animation slideinright;
    private boolean isserachbySpeciality, isSearchByService;
    private boolean isserachbyDoctor ;
    private String doctorname, specialityname, speciality, serviceName;
    private SearchBySpecialityReq searchBySpecialityReq ;
    private Button btnAddMore ;
    private ViewPager viewpager ;
    private MyMapsPagerAdapter myMapsPagerAdapter ;
    private Context context ;
    private BitmapDescriptor bitmapDescriptor, bitmapDescriptor1 ;
    private SpecialityDAO selectedSpecialityDAO ;
    private Marker lastselectedmarker ;
    private Double currentlat, currentlong ;
    private Double mylat, mylong ;
    private String doctorIds;
    private MyautoScroolView actDoctors;
    AdditionalFiltersDO filtersDO = new AdditionalFiltersDO();


    private Timer timer = new Timer();
    private final long DELAY = 1000;
    private ProgressBar pbHeaderProgress;

    @Override
    public void initialize()
    {
        llBooking   =   (LinearLayout)  getLayoutInflater().inflate(R.layout.booking_list,null,false);
        llBody.addView(llBooking,baseLayoutParams);
        llFilter.setVisibility(View.VISIBLE);
        context = this;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.greenmark);
        bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);

        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(),R.drawable.orangemark);
        bitmapDescriptor1 = BitmapDescriptorFactory.fromBitmap(bitmap1);

        Preference preference = new Preference(context);
        String latlong = preference.getStringFromPreference("CURRENTLATLONG","");
        if(latlong.contains(","))
        {
            currentlat  = Double.parseDouble(latlong.split(",")[0]);
            currentlong = Double.parseDouble(latlong.split(",")[1]);
        }

        String latlong1 = preference.getStringFromPreference("LATLONG","");
        if(latlong.contains(","))
        {
            mylat  = Double.parseDouble(latlong1.split(",")[0]);
            mylong = Double.parseDouble(latlong1.split(",")[1]);
        }

        llExperiance            =   (LinearLayout)  llBooking.findViewById(R.id.llExperiance);
        llCompare               =   (LinearLayout)  llBooking.findViewById(R.id.llCompare);
        llAvailability          =   (LinearLayout)  llBooking.findViewById(R.id.llAvailability);
        llDistance              =   (LinearLayout)  llBooking.findViewById(R.id.llDistance);
        llMapView               =   (LinearLayout)  llBooking.findViewById(R.id.llMapView);
        llDoctorCard            =   (LinearLayout)  llBooking.findViewById(R.id.llDoctorCard);
        viewpager               =   (ViewPager)     llBooking.findViewById(R.id.viewpager);


        lvBooking               =   (ListView)      llBooking.findViewById(R.id.lvBooking);
        tvNotAvailable          =   (TextView)      llBooking.findViewById(R.id.tvNotAvailable);
        tvExp                   =   (TextView)      llBooking.findViewById(R.id.tvExp);
        tvDis                   =   (TextView)      llBooking.findViewById(R.id.tvDis);

        ivExpArrow              =   (ImageView)     llBooking.findViewById(R.id.ivExpArrow);
        ivDisArrow              =   (ImageView)     llBooking.findViewById(R.id.ivDisArrow);
        ivExpDownArrow          =   (ImageView)     llBooking.findViewById(R.id.ivExpDownArrow);
        ivDisDownArrow          =   (ImageView)     llBooking.findViewById(R.id.ivDisDownArrow);
        ivExpArrowDown          =   (ImageView)     llBooking.findViewById(R.id.ivExpArrowDown);
        ivDisArrowDown          =   (ImageView)     llBooking.findViewById(R.id.ivDisArrowDown);
        ivNotAvailable          =   (ImageView)     llBooking.findViewById(R.id.ivNotAvailable);

        ivDoctorPic             =   (ImageView)     llBooking.findViewById(R.id.ivDoctorPic);
        tvDoctorname            =   (TextView)      llBooking.findViewById(R.id.tvDoctorname);
        tvDoctorQualification   =   (TextView)      llBooking.findViewById(R.id.tvDoctorQualification);
        tvspecialist            =   (TextView)      llBooking.findViewById(R.id.tvspecialist);
        tvHospitalName          =   (TextView)      llBooking.findViewById(R.id.tvHospitalName);
        tvDistance              =   (TextView)      llBooking.findViewById(R.id.tvDistance);
        tvExperince             =   (TextView)      llBooking.findViewById(R.id.tvExperince);
        tvFee                   =   (TextView)      llBooking.findViewById(R.id.tvFee);
        tvSymbol                =   (TextView)      llBooking.findViewById(R.id.tvSymbol);
        tvArea                  =   (TextView)      llBooking.findViewById(R.id.tvArea);
        tvBookAppointment       =   (TextView)      llBooking.findViewById(R.id.tvBookAppointment);

        actDoctors      =   (MyautoScroolView)  llBooking.findViewById(R.id.actDoctors);
        actDoctors.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        actDoctors.setVisibility(View.VISIBLE);
        pbHeaderProgress = (ProgressBar) llBooking.findViewById(R.id.pbHeaderProgress);

        ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mapView = googleMap;
                mapView.setOnMarkerClickListener(BookAppointment.this);
                mapView.setMyLocationEnabled(true);
            }
        });
        mapPreview			    =	(ImageView)	 	llBooking.findViewById(R.id.mapPreview);
        fMapView			    =   (FrameLayout)	llBooking.findViewById(R.id.flMaps);

        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        slideinright            = AnimationUtils.loadAnimation(mContext, R.anim.push_left_in);

        setSpecificTypeFace(llBooking,ApplicationConstants.WALSHEIM_MEDIUM);
        tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvBookAppointment.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);


        ivSearch.setVisibility(View.VISIBLE);
        //ivSearch.setBackgroundResource(R.drawable.mapsearch);
        tvTopTitle.setVisibility(View.GONE);
        tvLocationHeader.setVisibility(View.GONE);
        llLocation.setVisibility(View.VISIBLE);
        tvspecialityCount.setVisibility(View.VISIBLE);
        llDownarrow.setVisibility(View.GONE);

        //llRight.setTag("List");



        bookingAdapter  =   new BookingAdapter(BookAppointment.this, specialityResRowArrayList);


        ViewGroup.LayoutParams myLayoutParams = new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        btnAddMore = new Button(BookAppointment.this);
        //btnAddMore.setLayoutParams(myLayoutParams);
        btnAddMore.setText("Load More");
        btnAddMore.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        btnAddMore.setBackgroundResource(R.color.white);
        btnAddMore.setTextColor(getResources().getColor(R.color.gray));
        btnAddMore.setVisibility(View.GONE);
        lvBooking.addFooterView(btnAddMore);
        lvBooking.setAdapter(bookingAdapter);
        // Disable clip to padding
        viewpager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        viewpager.setPadding(40, 0, 40, 0);
        // sets a margin b/w individual pages to ensure that there is a gap b/w them
        viewpager.setPageMargin(-10);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(lastselectedmarker != null)
                {
                    lastselectedmarker.setIcon(bitmapDescriptor);
                }

                /*final SpecialityResRow specialityResRow = specialityResRowArrayList.get(position);
                String latitude = specialityResRow.getLatitude();
                String longitude = specialityResRow.getLongitude() ;
                LatLng sydney = new LatLng(Float.valueOf(latitude), Float.valueOf(longitude));*/
                lastselectedmarker = veMarkers.get(position);
                lastselectedmarker.setIcon(bitmapDescriptor1);
                //map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(lastselectedmarker.getPosition(), 15);
                mapView.animateCamera(cameraUpdate);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        selectedSpecialityDAO = SelectedConsSingleton.getSingleton().getSpecialityDAO();
        if(getIntent().getStringExtra("DOCTORNAME") != null)
        {
            doctorname = getIntent().getStringExtra("DOCTORNAME");
            isserachbyDoctor = true ;
        }else if(getIntent().getStringExtra("SPECIALITY") != null)
        {
            specialityname = getIntent().getStringExtra("SPECIALITY") ;
            speciality = getIntent().getStringExtra("SPECIALITY_NAME") ;
            isserachbySpeciality = true ;
        }else if(getIntent().hasExtra("DoctorID")){
            isSearchByService = true;
            doctorIds = getIntent().getStringExtra("DoctorID");
            serviceName = getIntent().getStringExtra("ServiceName");
        }
        if(SelectedConsSingleton.getSingleton().getSpecialityDAO() != null)
            tvspecialityCount.setText(SelectedConsSingleton.getSingleton().getSpecialityDAO().getSpecialityname());
        if(isserachbySpeciality)
            tvspecialityCount.setText(speciality);

        if(isserachbyDoctor)
            tvspecialityCount.setText(doctorname);
        else if(isSearchByService)
            tvspecialityCount.setText(serviceName);

        distanceClicked(selectedSpecialityDAO,  false);

        lvBooking.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if(searchBySpecialityReq != null)
                    if(lastInScreen == searchBySpecialityReq.getPageSize()-1||lastInScreen == searchBySpecialityReq.getPageSize()-2)
                    {

                        btnAddMore.setVisibility(View.VISIBLE);
                        btnAddMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callservicegetDoctors(searchBySpecialityReq.getPageSize()+25);
                                btnAddMore.setVisibility(View.GONE);
                            }
                        });
                    }
            }
        });


        llExperiance.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tvExp.setTextColor(Color.parseColor("#f47204"));
                tvDis.setTextColor(Color.parseColor("#000000"));
                ivExpDownArrow.setVisibility(View.INVISIBLE);
                ivDisArrow.setVisibility(View.INVISIBLE);
                ivDisArrowDown.setVisibility(View.INVISIBLE);
                ivDisDownArrow.setVisibility(View.VISIBLE);
                if(llExperiance.getTag() ==null)
                {
                    ivExpArrow.setVisibility(View.VISIBLE);
                    ivExpArrowDown.setVisibility(View.INVISIBLE);
                    llExperiance.setTag("ASC");
                    llExperiance.setTag("ASC");
                }else
                {
                    if(((String)llExperiance.getTag()).equalsIgnoreCase("ASC"))
                    {
                        ivExpArrowDown.setVisibility(View.VISIBLE);
                        ivExpArrow.setVisibility(View.INVISIBLE);
                        llExperiance.setTag("DESC");
                    }else
                    {
                        ivExpArrow.setVisibility(View.VISIBLE);
                        ivExpArrowDown.setVisibility(View.INVISIBLE);
                        llExperiance.setTag("ASC");
                    }
                }
                if(Connectivity.isConnected(BookAppointment.this))
                {
                    callservicetogetDoctors(selectedSpecialityDAO, "EXP",(String)llExperiance.getTag(),"");
                }else
                {
                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                }

            }
        });
        llDistance.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                distanceClicked(selectedSpecialityDAO, true);

            }
        });


        /*llRight.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {

                System.out.println("llRight click event call");

                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    llRight.setBackgroundColor(Color.parseColor("#33000000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    llRight.setBackgroundColor(Color.parseColor("#00000000"));
                    if (v.getTag().toString().equalsIgnoreCase("list"))
                    {
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(lastselectedmarker.getPosition(), 12.0f);
                        mapView.animateCamera(cameraUpdate);
                        lvBooking.setVisibility(View.GONE);
                        llMapView.setVisibility(View.VISIBLE);
                        ivSearch.setBackgroundResource(R.drawable.listsearch);
                        llDistance.setVisibility(View.GONE);
                        llExperiance.setVisibility(View.GONE);
                        v.setTag("Map");
                    } else {
                        lvBooking.setVisibility(View.VISIBLE);
                        llDistance.setVisibility(View.VISIBLE);
                        llExperiance.setVisibility(View.VISIBLE);
                        llMapView.setVisibility(View.GONE);
                        ivSearch.setBackgroundResource(R.drawable.mapsearch);
                        v.setTag("list");
                        showLoader("");
                        if (bookingAdapter == null)
                            lvBooking.setAdapter(bookingAdapter);
                        else
                            bookingAdapter.refresh(specialityResRowArrayList);
                        hideLoader();
                    }
                }

                return true;
            }
        });*/


       /* llRight.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {

                System.out.println("llRight click event call");

                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    llRight.setBackgroundColor(Color.parseColor("#33000000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    llRight.setBackgroundColor(Color.parseColor("#00000000"));

                    Intent intent = new Intent(BookAppointment.this,DoctorSearch.class);
                    startActivity(intent);

                }

                return true;
            }
        });*/


        llFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("llFilter click event call");

                Intent in = new Intent(BookAppointment.this, AdditionalFiltersActivity.class);
                in.putExtra("FiltersDO",filtersDO);
                startActivityForResult(in, 100);
            }
        });





        actDoctors.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(timer != null)
                    timer.cancel();
                if (pbHeaderProgress.getVisibility() != View.VISIBLE && s.toString().length()>3){
                    pbHeaderProgress.setVisibility(View.VISIBLE);
                    ivClose.setVisibility(View.GONE);
                }/*else{
                    ivClose.setVisibility(View.VISIBLE);
                }*/
            }

            @Override
            public void afterTextChanged(final Editable s) {

                if (s.toString().length() > 0 && s.toString().length() < 3){
                    ivClose.setVisibility(View.VISIBLE);
                }else{
                    ivClose.setVisibility(View.GONE);
                }

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // TODO: do what you need here (refresh list)
                        // you will probably need to use
                        // runOnUiThread(Runnable action) for some specific
                        // actions
                        if (Connectivity.isConnected(BookAppointment.this))
                        {
//                    callgetSpecialityDoctorsService(s.toString());
                            if(s.toString().length()>3) {

                                if(Connectivity.isConnected(BookAppointment.this))
                                {
                                    /*isserachbyDoctor=true;
                                    doctorname="Tulasi Reddy V";*/
                                    callservicetogetDoctors(selectedSpecialityDAO, "EXP",(String)llExperiance.getTag(),s.toString());
                                }else
                                {
                                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                }

                            }
                            else if (s.toString().length()<1) {

                                callservicetogetDoctors(selectedSpecialityDAO, "EXP",(String)llExperiance.getTag(),"");
                                /*runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (pbHeaderProgress.getVisibility() == View.VISIBLE){
                                            pbHeaderProgress.setVisibility(View.GONE);
                                            ivClose.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });*/

                            }

                        }else
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                }
                            });

                        }

                    }

                }, DELAY);

            }
        });






    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    private void callservicegetDoctors(int i)
    {
        System.out.println("callservicegetDoctors method enter");
        showLoader("");
        searchBySpecialityReq.setPageSize(i);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).searchBySpeciality(searchBySpecialityReq, new RestCallback<SearchBySpecialityRes>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(SearchBySpecialityRes searchBySpecialityRes, Response response) {
                hideLoader();
                if(specialityResRowArrayList != null)
                    specialityResRowArrayList.clear();
                if(searchBySpecialityRes != null && searchBySpecialityRes.getSpecialityResRowArrayList()!= null && searchBySpecialityRes.getSpecialityResRowArrayList().size() > 0)
                {
                    specialityResRowArrayList = searchBySpecialityRes.getSpecialityResRowArrayList();
                    Gson gson = new Gson();
                    for(SpecialityResRow row : specialityResRowArrayList)
                    {
                        row.setDistance(getDistance(row.getLatitude(), row.getLongitude()));

                        LinkedHashMap<String, String> speciality =
                                gson.fromJson(
                                        row.getSpecialityID(),
                                        new TypeToken<LinkedHashMap<String, String>>(){}.getType()
                                );
                        StringBuilder specialityBuilder = new StringBuilder();
                        for(String specialityId : speciality.values())
                        {
                            specialityBuilder.append(Constants.specialitiesMap.get(specialityId)+", ");
                        }
                        int index = specialityBuilder.toString().length() - 2;
                        String specialities = specialityBuilder.toString().substring(0, index);
                        row.setSpecialityDesc(specialities);
                    }
                    btnAddMore.setVisibility(View.GONE);
                    llAvailability.setVisibility(View.GONE);
                    llMapView.setVisibility(View.GONE);
                    lvBooking.setVisibility(View.VISIBLE);
                    //ivSearch.setVisibility(View.VISIBLE);
                    bookingAdapter.refreshAdapter(specialityResRowArrayList);
                    if(SelectedConsSingleton.getSingleton().getSpecialityDAO() != null)
                        tvspecialityCount.setText(SelectedConsSingleton.getSingleton().getSpecialityDAO().getSpecialityname());
                    if(isserachbySpeciality)
                        tvspecialityCount.setText(speciality);

                    if(isserachbyDoctor)
                        tvspecialityCount.setText(doctorname);
                    veMarkers = (Vector<Marker>) addMarkers(specialityResRowArrayList, mapView);
                    myMapsPagerAdapter = new MyMapsPagerAdapter(specialityResRowArrayList);
                    viewpager.setAdapter(myMapsPagerAdapter);


                }else {
                    llAvailability.setVisibility(View.VISIBLE);
                    llMapView.setVisibility(View.GONE);
                    lvBooking.setVisibility(View.GONE);
                    tvNotAvailable.setText("No doctor records found.");
                    ivNotAvailable.setBackgroundResource(R.drawable.doctor);
                    //ivSearch.setVisibility(View.GONE);
                }
            }
        });
    }

    private void distanceClicked(SpecialityDAO selectedSpecialityDAO, boolean b)
    {
        tvDis.setTextColor(Color.parseColor("#f47204"));
        tvExp.setTextColor(Color.parseColor("#000000"));
        ivExpArrow.setVisibility(View.INVISIBLE);
        //ivExpDownArrow.setVisibility(View.INVISIBLE);
        ivExpArrowDown.setVisibility(View.INVISIBLE);
        ivDisDownArrow.setVisibility(View.INVISIBLE);
        ivExpDownArrow.setVisibility(View.VISIBLE);

        if(llDistance.getTag() ==null)
        {
            llDistance.setTag("ASC");
            ivDisArrow.setVisibility(View.VISIBLE);
            ivDisArrowDown.setVisibility(View.INVISIBLE);
        }else
        {
            if(b)
            {
                if (((String) llDistance.getTag()).equalsIgnoreCase("ASC")) {
                    llDistance.setTag("DESC");
                    ivDisArrowDown.setVisibility(View.VISIBLE);
                    ivDisArrow.setVisibility(View.INVISIBLE);
                } else {
                    llDistance.setTag("ASC");
                    ivDisArrow.setVisibility(View.VISIBLE);
                    ivDisArrowDown.setVisibility(View.INVISIBLE);
                }
            }
        }
        if(Connectivity.isConnected(BookAppointment.this))
        {
            try {
                callservicetogetDoctors(selectedSpecialityDAO, "DIST", (String) llDistance.getTag(),"");
            }catch (Exception e)
            {
                e.printStackTrace();
                hideLoader();
                finish();
            }
        }else
        {
            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
        }

    }


    private void callservicetogetDoctors(SpecialityDAO selectedSpecialityDAO, String exp, String sortname,String searchtext)
    {

        System.out.println("callservicetogetDoctors method enter");

        searchvalue=searchtext;
        Preference preference = new Preference(BookAppointment.this);

        showLoader("");
        searchBySpecialityReq = new SearchBySpecialityReq();
        searchBySpecialityReq.setOrderBy(sortname);
        searchBySpecialityReq.setLatitude(currentlat.toString());
        searchBySpecialityReq.setLongitude(currentlong.toString());
        searchBySpecialityReq.setSort(exp);
        HashMap<String, String> hmAdditionalFilters = new HashMap<>();
        hmAdditionalFilters.put("hospitalCd","RVUYMISZZF");
        searchBySpecialityReq.setAdditionalSearch(hmAdditionalFilters);
        if(isserachbyDoctor)
        {
            searchBySpecialityReq.setDoctorName(doctorname);
        }else if(isserachbySpeciality)
        {
            searchBySpecialityReq.setSpecialityID(specialityname);
        }else if(isSearchByService){
            searchBySpecialityReq.setDoctorIds(doctorIds);
        }else
        {
            try{
                searchBySpecialityReq.setSpecialityID(selectedSpecialityDAO.getSpeciality_code());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).searchBySpeciality(searchBySpecialityReq, new RestCallback<SearchBySpecialityRes>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(SearchBySpecialityRes searchBySpecialityRes, Response response) {

                setProgressBarIndeterminateVisibility(false);
                if (pbHeaderProgress.getVisibility() == View.VISIBLE){
                    pbHeaderProgress.setVisibility(View.GONE);
                }
                ivClose.setVisibility(View.VISIBLE);

                hideLoader();
                if(specialityResRowArrayList != null)
                    specialityResRowArrayList.clear();
                if(searchBySpecialityRes != null && searchBySpecialityRes.getSpecialityResRowArrayList()!= null && !searchBySpecialityRes.getSpecialityResRowArrayList().isEmpty())
                {
                    specialityResRowArrayList = searchBySpecialityRes.getSpecialityResRowArrayList();
                    Gson gson = new Gson();
                    for(SpecialityResRow row : specialityResRowArrayList)
                    {
                        row.setDistance(getDistance(row.getLatitude(), row.getLongitude()));

                        LinkedHashMap<String, String> speciality =
                                gson.fromJson(
                                        row.getSpecialityID(),
                                        new TypeToken<LinkedHashMap<String, String>>(){}.getType()
                                );
                        StringBuilder specialityBuilder = new StringBuilder();
                        for(String specialityId : speciality.values())
                        {
                            specialityBuilder.append(Constants.specialitiesMap.get(specialityId)+", ");
                        }
                        int index = specialityBuilder.toString().length() - 2;
                        String specialities = specialityBuilder.toString().substring(0, index);
                        row.setSpecialityDesc(specialities);
                    }

                    //doctorsearchlist=specialityResRowArrayList;
                    //bookingAdapter.refreshAdapter(specialityResRowArrayList);
                    if(searchvalue.equalsIgnoreCase(""))
                    {
                        bookingAdapter.refreshAdapter(specialityResRowArrayList);

                        if(SelectedConsSingleton.getSingleton().getSpecialityDAO() != null)
                            tvspecialityCount.setText(SelectedConsSingleton.getSingleton().getSpecialityDAO().getSpecialityname());

                        if(isserachbySpeciality)
                            tvspecialityCount.setText(speciality);

                        if(isserachbyDoctor)
                            tvspecialityCount.setText(doctorname);

                        veMarkers = (Vector<Marker>) addMarkers(specialityResRowArrayList, mapView);
                        myMapsPagerAdapter = new MyMapsPagerAdapter(specialityResRowArrayList);
                        viewpager.setAdapter(myMapsPagerAdapter);
                    }else{

                        ArrayList<SpecialityResRow> doctorsearchlist=new ArrayList<SpecialityResRow>();

                        for(SpecialityResRow row: specialityResRowArrayList) {

                            if ((row.getFullName() != null && row.getFullName().matches("(?i)(" + searchvalue + ").*"))||(
                                    row.getLocation() != null && row.getLocation().matches("(?i)(" + searchvalue + ").*")
                            )) {
                                System.out.println("The full name values " + row.getFullName());
                                doctorsearchlist.add(row);
                            }
                        }

                        bookingAdapter.refreshAdapter(doctorsearchlist);
                        if(SelectedConsSingleton.getSingleton().getSpecialityDAO() != null)
                            tvspecialityCount.setText(SelectedConsSingleton.getSingleton().getSpecialityDAO().getSpecialityname());

                        if(isserachbySpeciality)
                            tvspecialityCount.setText(speciality);

                        if(isserachbyDoctor)
                            tvspecialityCount.setText(doctorname);
                        veMarkers = (Vector<Marker>) addMarkers(doctorsearchlist, mapView);
                        myMapsPagerAdapter = new MyMapsPagerAdapter(doctorsearchlist);
                        viewpager.setAdapter(myMapsPagerAdapter);
                    }




                    /*final SpecialityResRow specialityResRow = specialityResRowArrayList.get(0);
                    String latitude = specialityResRow.getLatitude();
                    String longitude = specialityResRow.getLongitude() ;

                    LatLng sydney = new LatLng(Float.valueOf(latitude), Float.valueOf(longitude));
                    mapView.addMarker(new MarkerOptions().position(sydney).title(specialityResRow.getClinicName()).icon(bitmapDescriptor));
                    //map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(sydney, 17);
                    mapView.animateCamera(cameraUpdate);*/
                }
                else
                {
                    lvBooking.setVisibility(View.GONE);
                    llCompare.setVisibility(View.GONE);
                    llAvailability.setVisibility(View.VISIBLE);
                    llRight.setVisibility(View.GONE);
                    tvspecialityCount.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private void setUpMap(final View svDoctors, LatLng doctorLatLng)
    {
        if(doctorLatLng == null)
        {
            latLng = new LatLng(Double.parseDouble(specialityResRow.getLatitude()), Double.parseDouble(specialityResRow.getLongitude()));
        }
        else
        {
            latLng = doctorLatLng;
        }
        mapView.addMarker(new MarkerOptions().position(latLng));
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 14.0f);
        mapView.moveCamera(yourLocation);

        mapPreview.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Uri location = Uri.parse(String.format(Locale.ENGLISH, "geo:%f,%f", latLng.latitude, latLng.longitude));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                boolean isIntentSafe = activities.size() > 0;

                if (isIntentSafe) {
                    startActivity(mapIntent);
                }

            }
        });

        mapView.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback()
        {
            @Override
            public void onMapLoaded() {
                mapView.snapshot(new GoogleMap.SnapshotReadyCallback()
                {

                    @Override
                    public void onSnapshotReady(Bitmap bitmap) {
                        fMapView.removeViewAt(0);
                        mapPreview.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        mapPreview.setImageBitmap(bitmap);
                    }
                });
            }
        });

        mapView.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marker)
            {
                SpecialityResRow doctorDetails = specialityResRowArrayList.get(veMarkers.indexOf(marker));
                llDoctorCard.setVisibility(View.GONE);
                return true;
            }
        });

    }

    private void initData(View svDoctors)
    {
        setUpMap(svDoctors, null);
    }

    public Vector<Marker> addMarkers(final ArrayList<SpecialityResRow> arrayListEvent2, GoogleMap mapview)
    {
        if(arrayListEvent2 != null && arrayListEvent2.size() > 0)
        {
            if(mapview != null)
            {
                veMarkers = new Vector<>();
                mapview.clear();
                int i=0;
                for (final SpecialityResRow mallsDetails : arrayListEvent2)
                {

                    LatLng latLang = new LatLng(Float.valueOf(mallsDetails.getLatitude()),Float.valueOf(mallsDetails.getLongitude()));
                    if(i==0)
                    {
                        Marker marker = mapview.addMarker(new MarkerOptions().position(latLang).icon(bitmapDescriptor1));
                        veMarkers.add(marker);
                        lastselectedmarker = marker;
                    }else
                    {
                        Marker marker = mapview.addMarker(new MarkerOptions().position(latLang).icon(bitmapDescriptor));
                        veMarkers.add(marker);

                    }
                    i=i+1;
                }

                LatLng latLang = new LatLng(Float.valueOf(arrayListEvent2.get(0).getLatitude()),Float.valueOf(arrayListEvent2.get(0).getLongitude()));
                mapview.moveCamera(CameraUpdateFactory.newLatLngZoom(latLang, 13.0f));
                return veMarkers;
            }
        }else{
            mapview.clear();
        }
        return null;
    }

    @Override
    public boolean onMarkerClick(Marker marker)
    {
        if(viewpager == null || veMarkers == null)
            return true;
        int index = veMarkers.indexOf(marker);
        viewpager.setCurrentItem(index, true);



        return true;
    }

    public class MyMapsPagerAdapter extends PagerAdapter
    {
        private ArrayList<SpecialityResRow> specialityResRowArrayList ;

        public MyMapsPagerAdapter(ArrayList<SpecialityResRow> specialityResRowArrayList)
        {
            this.specialityResRowArrayList = specialityResRowArrayList;
        }

        @Override
        public int getCount() {
            return specialityResRowArrayList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public float getPageWidth(int position) {

            return 1f;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View convertView = LayoutInflater.from(BookAppointment.this).inflate(R.layout.doctor_list2, container, false);

            ImageView       ivDoctorPic             =   (ImageView)     convertView.findViewById(R.id.ivDoctorPic);
            TextView        tvDoctorname            =   (TextView)      convertView.findViewById(R.id.tvDoctorname);
            TextView        tvDoctorQualification   =   (TextView)      convertView.findViewById(R.id.tvDoctorQualification);
            TextView        tvspecialist            =   (TextView)      convertView.findViewById(R.id.tvspecialist);
            TextView        tvHospitalName          =   (TextView)      convertView.findViewById(R.id.tvHospitalName);
            final TextView  tvDistance              =   (TextView)      convertView.findViewById(R.id.tvDistance);
            TextView        tvExperince             =   (TextView)      convertView.findViewById(R.id.tvExperince);
            TextView        tvFee                   =   (TextView)      convertView.findViewById(R.id.tvFee);
            TextView        tvSymbol                =   (TextView)      convertView.findViewById(R.id.tvSymbol);
            TextView        tvYrs                   =   (TextView)      convertView.findViewById(R.id.tvYrs);
            TextView        tvBookAppointment       =   (TextView)      convertView.findViewById(R.id.tvBookAppointment);
            final TextView  tvArea                  =   (TextView)      convertView.findViewById(R.id.tvArea);
            RippleView rvClick                 =   (RippleView)    convertView.findViewById(R.id.rvClick);
            RippleView      rvBook                  =   (RippleView)    convertView.findViewById(R.id.rvBook);

            tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvBookAppointment.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvArea.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvExperince.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvFee.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvSymbol.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvspecialist.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvDoctorQualification.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvHospitalName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvDistance.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvExperince.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvYrs.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            final SpecialityResRow specialityResRow = specialityResRowArrayList.get(position);
            if(specialityResRow.getProspect().equalsIgnoreCase("Y"))
            {
                tvBookAppointment.setText("Call");
                tvBookAppointment.setBackgroundResource(R.drawable.green_slot_bg);
                tvBookAppointment.setTextColor(Color.parseColor("#4ebb50"));

            }
            if(specialityResRow.getLocation() != null)
            {
                tvArea.setText(specialityResRow.getLocation());
            }
            else
                tvArea.setText("");

            if(specialityResRow.getProfileImage() != null && !specialityResRow.getProfileImage().isEmpty()) {
                Picasso.with(context).load(Uri.parse(specialityResRow.getProfileImage())).into(ivDoctorPic);
            }
            else
            {
                if(specialityResRow.getGender() == 'M')
                    Picasso.with(context).load(R.drawable.male).into(ivDoctorPic);
                else
                    Picasso.with(context).load(R.drawable.female).into(ivDoctorPic);
            }

            if(specialityResRow.getFullName().startsWith("Dr."))
                tvDoctorname.setText(specialityResRow.getFullName());
            else
                tvDoctorname.setText("Dr. "+specialityResRow.getFullName());

            if(specialityResRow.getExp()!=0)
            {
                tvExperince.setText(specialityResRow.getExp() + "");
                tvYrs.setVisibility(View.VISIBLE);
            }
            else
            {
                tvExperince.setText("N/A");
                tvYrs.setVisibility(View.GONE);
            }

            if(specialityResRow.getFees() !=0)
            {
                tvFee.setText(specialityResRow.getFees()+"");

            }
            else
            {
                tvSymbol.setVisibility(View.GONE);
                tvFee.setText("N/A");

            }

            String distance = specialityResRow.getDistance();
            if(TextUtils.isEmpty(distance))
                tvDistance.setVisibility(View.INVISIBLE);
            else
                tvDistance.setText(specialityResRow.getDistance());


            tvHospitalName.setText(specialityResRow.getClinicName());
            Gson gson = new Gson();
            tvDoctorQualification.setVisibility(View.GONE);
            LinkedHashMap<String, String> qualification = gson.fromJson(specialityResRow.getQualification(), new TypeToken<LinkedHashMap<String, String>>(){}.getType());
            //tvDoctorQualification.setText(qualification.values().toString());
            LinkedHashMap<String, String> speciality = gson.fromJson(specialityResRow.getSpecialityID(), new TypeToken<LinkedHashMap<String, String>>(){}.getType());
            StringBuilder specialitybuilder = new StringBuilder();
            for(String specialityid : speciality.values())
            {
                specialitybuilder.append(Constants.specialitiesMap.get(specialityid)+", ");
            }
            tvspecialist.setText(specialitybuilder.toString().substring(0, (specialitybuilder.toString().length()-2)));

            rvClick.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
            {
                @Override
                public void onComplete(RippleView rippleView)
                {
                    bookingAdapter.callgetDoctorDetails(specialityResRow.getDoctorID(), specialityResRow.getAddressID());

                }
            });




            rvBook.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                @Override
                public void onComplete(RippleView rippleView)
                {
                    if(specialityResRow.getProspect().equalsIgnoreCase("Y"))
                    {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:"+specialityResRow.getPhoneNumber()));
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", specialityResRow.getPhoneNumber(), null));
                        context.startActivity(intent);
//                        ((BookAppointment)context).ShowAlertDialog("Booking is not available for selected doctor.");
                    }
                    else
                    {
                        Intent slideactivity = new Intent(context, ChooseDateTime.class);
                        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(context.getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                        slideactivity.putExtra("SELECTED_DATAILS",specialityResRow);
                        slideactivity.putExtra("ISFROMRESCHEDULE", false);
                        context.startActivity(slideactivity, bndlanimation);
                    }
                }
            });

            tvDistance.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //Uri location = Uri.parse(String.format("http://maps.google.com/?daddr="+Float.valueOf(specialityResRow.getLatitude()), Float.valueOf(specialityResRow.getLongitude())));
                    Uri location = Uri.parse(String.format("http://maps.google.com/?daddr="+specialityResRow.getLatitude()+","+ specialityResRow.getLongitude()));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                    PackageManager packageManager = context.getPackageManager();
                    List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                    boolean isIntentSafe = activities.size() > 0;

                    if (isIntentSafe) {
                        context.startActivity(mapIntent);
                    }
                    else
                    {
                        ((BookAppointment) context).ShowAlertDialog("Whoops!","Doctor Latlongs not available",R.drawable.alert);
                    }
                }
            });

            container.addView(convertView);
            if(position == specialityResRowArrayList.size()-1)
                convertView.setPadding(20,10,30,0);

            return  convertView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            ((ViewPager) container).removeView((View) object);
        }
    }

    private double getDouble(String doubleString)
    {
        double doubleValue = -1d;
        try{
            doubleValue = Double.parseDouble(doubleString);
        }
        catch (Exception e){}
        return doubleValue;
    }


    private String getDistance(String eLatitude, String eLongitude){

        double latitude = getDouble(eLatitude);
        double longitude = getDouble(eLongitude);

        String distance = "NA";
        if(mylat == null || mylong == null || (latitude == -1d || longitude == -1d))
            return distance;
        Location locationA = new Location("point A");

        locationA.setLatitude(mylat);
        locationA.setLongitude(mylong);

        Location locationB = new Location("point B");

        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);

        float distanceValue = locationA.distanceTo(locationB);
        Float obj = distanceValue/1000;
        if(obj.floatValue() < 100.0f)
        {
            distance = "~" + String.valueOf(distanceValue / 1000).substring(0, 4) + " kms";
        }
        else
        {
            distance = "";
        }
        return distance;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK){

            //ivSearch.setVisibility(View.VISIBLE);
            //ivSearch.setBackgroundResource(R.drawable.mapsearch);

            if(data.getExtras().containsKey("FiltersDO"))
            {
                filtersDO = (AdditionalFiltersDO) data.getExtras().getSerializable("FiltersDO");

                searchBySpecialityReq.setGender(filtersDO.getGender());
                searchBySpecialityReq.setBook(filtersDO.getBook());
                searchBySpecialityReq.setCall(filtersDO.getCall());
                searchBySpecialityReq.setAvailable(filtersDO.getAvailability());
                searchBySpecialityReq.setStartTime(filtersDO.getStartTime());
                searchBySpecialityReq.setEndTime(filtersDO.getEndTime());
                searchBySpecialityReq.setFeeStartRange(filtersDO.getMinFee() != null ? Integer.parseInt(filtersDO.getMinFee()) :  0);
                searchBySpecialityReq.setFeeEndRange(filtersDO.getMaxFee() != null ? Integer.parseInt(filtersDO.getMaxFee()) :  null);
                searchBySpecialityReq.setFilterDayList(filtersDO.getListSelectedDay() != null ? filtersDO.getListSelectedDay() : null);
                searchBySpecialityReq.setSort(filtersDO.getSortCategory());
                searchBySpecialityReq.setOrderBy(filtersDO.getSortType());

                callservicegetDoctors(searchBySpecialityReq.getPageSize()+25);

            }
        }
    }
}
