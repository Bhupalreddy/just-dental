package hCue.Hcue;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.media.AudioManager;
import android.os.Vibrator;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.DAO.ReminderDetailsDO;
import hCue.Hcue.adapters.RemindersDbAdapter;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.widget.SeekBarHint;

import static org.xmlpull.v1.XmlPullParser.TYPES;

/**
 * Created by shyamprasadg on 06/08/16.
 */
public class ReminderUpdateActivity extends BaseActivity implements View.OnClickListener {
    private ScrollView llThankyou;
    private LinearLayout llContinue, llMorningExpand, llAfternoonExpand, llNightExpand, llMorningCollpased, llAfternoonCollpased, llNightCollpased;
    private ImageView divider1, divider2, divider3, ivMorning, ivAfternoon, ivNight;
    AudioManager mode;
    private Vibrator vibrator;
    private ImageView ivMorningReminderStatus, ivAfternoonReminderStatus, ivNightReminderStatus, ivMorningDone, ivAfternoonDone, ivNightDone;
    private boolean isMorningSelected = false, isAfternoonSelected = false, isNighSelected = false;
    private boolean isMorningCollapsed = true, isAfternoonCollapsed = true, isNighCollapsed = true;
    private TextView tvMorningTiming, tvAfternoonTiming, tvNightTiming, tvMedicineName, tvMedicineQuantity, tvMedicineDate, tvDosage, tvNoofDays;
    private SeekBarHint seekbarOne, seekbarTwo, seekbarThree;
    PatientCaseRow patientCaseRow;
    private TextView tvMorningTitle, tvAfternoonTitle, tvNightTitle;
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private Calendar curCal;
    private int seekbar1TextProgress = 27, seekbar2TextProgress = 18, seekbar3TextProgress = 60;
    ArrayList<ReminderDetailsDO> listReminders = new ArrayList<>();
    String reminderDateTime;

    @Override
    public void initialize() {

        curCal = Calendar.getInstance();

        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATE_FORMAT);
        reminderDateTime = dateTimeFormat.format(curCal.getTime());

        if (getIntent().hasExtra("SELECTEDCASE"))
            patientCaseRow = (PatientCaseRow) getIntent().getSerializableExtra("SELECTEDCASE");
        mode = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        llThankyou = (ScrollView) getLayoutInflater().inflate(R.layout.reminder_update, null, false);
        llBody.addView(llThankyou, baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);
        tvTopTitle.setText("REMINDER");
        llClose.setVisibility(View.VISIBLE);
        llLeftMenu.setVisibility(View.GONE);
        llSave.setVisibility(View.VISIBLE);


        ivMorningReminderStatus = (ImageView) findViewById(R.id.ivMorningReminderStatus);
        ivAfternoonReminderStatus = (ImageView) findViewById(R.id.ivAfternoonReminderStatus);
        ivNightReminderStatus = (ImageView) findViewById(R.id.ivNightReminderStatus);
        ivMorningDone = (ImageView) findViewById(R.id.ivMorningDone);
        ivAfternoonDone = (ImageView) findViewById(R.id.ivAfternoonDone);
        ivNightDone = (ImageView) findViewById(R.id.ivNightDone);

        ivMorning = (ImageView) findViewById(R.id.ivMorning);
        ivAfternoon = (ImageView) findViewById(R.id.ivAfternoon);
        ivNight = (ImageView) findViewById(R.id.ivNight);

        llMorningExpand = (LinearLayout) findViewById(R.id.llMorningExpand);
        llAfternoonExpand = (LinearLayout) findViewById(R.id.llAfternoonExpand);
        llNightExpand = (LinearLayout) findViewById(R.id.llNightExpand);
        llMorningCollpased = (LinearLayout) findViewById(R.id.llMorningCollpased);
        llAfternoonCollpased = (LinearLayout) findViewById(R.id.llAfternoonCollpased);
        llNightCollpased = (LinearLayout) findViewById(R.id.llNightCollpased);

        tvMorningTiming = (TextView) findViewById(R.id.tvMorningTiming);
        tvAfternoonTiming = (TextView) findViewById(R.id.tvAfternoonTiming);
        tvNightTiming = (TextView) findViewById(R.id.tvNightTiming);

        tvMorningTitle = (TextView) findViewById(R.id.tvMorningTitle);
        tvAfternoonTitle = (TextView) findViewById(R.id.tvAfternoonTitle);
        tvNightTitle = (TextView) findViewById(R.id.tvNightTitle);

        tvMedicineName = (TextView) findViewById(R.id.tvMedicineName);
        tvMedicineQuantity = (TextView) findViewById(R.id.tvMedicineQuantity);
        tvMedicineDate = (TextView) findViewById(R.id.tvMedicineDate);
        tvDosage = (TextView) findViewById(R.id.tvDosage);
        tvNoofDays = (TextView) findViewById(R.id.tvNoofDays);

        setSpecificTypeFace(llThankyou,ApplicationConstants.WALSHEIM_MEDIUM);

        PatientCasePrescribObj patientCasePrescribObj = patientCaseRow.getPatientprHistory();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(patientCaseRow.getConsultationDt());

        Calendar calendar1 = Calendar.getInstance(Locale.getDefault());
        calendar1.setTimeInMillis(patientCaseRow.getConsultationDt());
        int noofdays = patientCasePrescribObj.getNumberofDays();
        calendar1.add(Calendar.DATE, noofdays);

        llClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                overridePendingTransition(R.anim.push_down_in,R.anim.push_down_out);
                finish();
            }
        });
        llSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                overridePendingTransition(R.anim.push_down_in,R.anim.push_down_out);
                finish();
            }
        });

        if (patientCaseRow != null) {
            tvMedicineName.setText(patientCaseRow.getMedicine());
            tvMedicineDate.setText(simpleDateFormat.format(calendar.getTime()) + " - " + simpleDateFormat.format(calendar1.getTime()));
            if (patientCasePrescribObj != null) {
                tvDosage.setText((patientCasePrescribObj.getDosage1().isEmpty() ? "0" : patientCasePrescribObj.getDosage1()) + "-" + (patientCasePrescribObj.getDosage2().isEmpty() ? "0" : patientCasePrescribObj.getDosage2()) + "-" + (patientCasePrescribObj.getDosage4().isEmpty() ? "0" : patientCasePrescribObj.getDosage4()));
            }
            tvNoofDays.setText(noofdays + "");
        }

        seekbarOne = (SeekBarHint) findViewById(R.id.sbMorning);
        seekbarOne.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.morning_time), PorterDuff.Mode.MULTIPLY));
        seekbarOne.getThumb().setColorFilter(getResources().getColor(R.color.morning_time), PorterDuff.Mode.SRC_IN);

        seekbarTwo = (SeekBarHint) findViewById(R.id.sbAfternoon);
        seekbarTwo.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.afternoon_time), PorterDuff.Mode.MULTIPLY));
        seekbarTwo.getThumb().setColorFilter(getResources().getColor(R.color.afternoon_time), PorterDuff.Mode.SRC_IN);

        seekbarThree = (SeekBarHint) findViewById(R.id.sbNight);
        seekbarThree.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.night_time), PorterDuff.Mode.MULTIPLY));
        seekbarThree.getThumb().setColorFilter(getResources().getColor(R.color.night_time), PorterDuff.Mode.SRC_IN);

        divider1 = (ImageView) findViewById(R.id.divider1);
        divider2 = (ImageView) findViewById(R.id.divider2);
        divider3 = (ImageView) findViewById(R.id.divider3);

        listReminders = ApplicationConstants.dbAdapter.getAllTodayRecords(patientCaseRow.getCaseID(), patientCaseRow.getRowID(), reminderDateTime);

        for (int i = 0; i < listReminders.size(); i++){
            if (listReminders.get(i).getDayID() == 1){
                isMorningSelected = true;
                int position = Arrays.asList(SeekBarHint.seekbarOne).indexOf(listReminders.get(i).getTime());
                if (position != -1)
                {
                    tvMorningTiming.setText(Html.fromHtml("<u>"+listReminders.get(i).getTime()+"</u>"));
                    tvMorningTiming.setTextColor(getResources().getColor(R.color.bg_color));
                    tvMorningTiming.setTag(listReminders.get(i).getRowID());
                    seekbar1TextProgress = position * 9;
                }

            }else if (listReminders.get(i).getDayID() == 2){
                isAfternoonSelected = true;
                int position = Arrays.asList(SeekBarHint.seekbarTwo).indexOf(listReminders.get(i).getTime());
                if (position != -1) {
                    seekbar2TextProgress = position * 10;
                    tvAfternoonTiming.setText(Html.fromHtml("<u>"+listReminders.get(i).getTime()+"</u>"));
                    tvAfternoonTiming.setTag(listReminders.get(i).getRowID());
                }
            }else if (listReminders.get(i).getDayID() == 3){
                isNighSelected = true;
                int position = Arrays.asList(SeekBarHint.seekbarThree).indexOf(listReminders.get(i).getTime());
                if (position != -1){
                    tvNightTiming.setText(Html.fromHtml("<u>"+listReminders.get(i).getTime()+"</u>"));
                    seekbar3TextProgress = position * 9;
                    tvNightTiming.setTag(listReminders.get(i).getRowID());
                }
            }
        }

        ivMorningReminderStatus.setOnClickListener(this);
        ivAfternoonReminderStatus.setOnClickListener(this);
        ivNightReminderStatus.setOnClickListener(this);
        ivMorningDone.setOnClickListener(this);
        ivAfternoonDone.setOnClickListener(this);
        ivNightDone.setOnClickListener(this);
        llMorningCollpased.setOnClickListener(this);
        llAfternoonCollpased.setOnClickListener(this);
        llNightCollpased.setOnClickListener(this);

        seekbarOne.hidePopup();
        seekbarTwo.hidePopup();
        seekbarThree.hidePopup();

        seekbarOne.mPopupTextView.setVisibility(View.GONE);
        seekbarOne.mPopupTextView.setBackgroundResource(R.drawable.slider_morning);
        seekbarTwo.mPopupTextView.setVisibility(View.GONE);
        seekbarTwo.mPopupTextView.setBackgroundResource(R.drawable.slider_afternoon);
        seekbarThree.mPopupTextView.setVisibility(View.GONE);
        seekbarThree.mPopupTextView.setBackgroundResource(R.drawable.slider_night);

        if (patientCasePrescribObj.getDosage1().equalsIgnoreCase("0")){
            isMorningSelected = true;
            ivMorningReminderStatus.performClick();
            ivMorningReminderStatus.setClickable(false);
            ivMorningReminderStatus.setEnabled(false);
            llMorningCollpased.setEnabled(false);
            llMorningCollpased.setClickable(false);
        }
        if (patientCasePrescribObj.getDosage2().equalsIgnoreCase("0")){
            isAfternoonSelected = true;
            ivAfternoonReminderStatus.performClick();
            ivAfternoonReminderStatus.setClickable(false);
            ivAfternoonReminderStatus.setEnabled(false);
            llAfternoonCollpased.setEnabled(false);
            llAfternoonCollpased.setClickable(false);
        }
        if (patientCasePrescribObj.getDosage4().equalsIgnoreCase("0")){
            isNighSelected = true;
            ivNightReminderStatus.performClick();
            ivNightReminderStatus.setClickable(false);
            ivNightReminderStatus.setEnabled(false);
            llNightCollpased.setEnabled(false);
            llNightCollpased.setClickable(false);
        }

        if (!isMorningSelected){
            ivMorningReminderStatus.setImageResource(R.drawable.reminder_inactive);
            ivMorning.setImageResource(R.drawable.morning_inactive);
            tvMorningTitle.setTextColor(getResources().getColor(R.color.time_grey));
            tvMorningTiming.setTextColor(getResources().getColor(R.color.time_grey));
        }

        if (!isAfternoonSelected){
            ivAfternoonReminderStatus.setImageResource(R.drawable.reminder_inactive);
            ivAfternoon.setImageResource(R.drawable.afternoon_inactive);
            tvAfternoonTitle.setTextColor(getResources().getColor(R.color.time_grey));
            tvAfternoonTiming.setTextColor(getResources().getColor(R.color.time_grey));
        }

        if (!isNighSelected){
            ivNightReminderStatus.setImageResource(R.drawable.reminder_inactive);
            ivNight.setImageResource(R.drawable.night_inactive);
            tvNightTitle.setTextColor(getResources().getColor(R.color.time_grey));
            tvNightTiming.setTextColor(getResources().getColor(R.color.time_grey));
        }
    }

    String time;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivMorningReminderStatus:
                if (isMorningSelected) {
                    isMorningSelected = false;
                    ivMorningReminderStatus.setImageResource(R.drawable.reminder_inactive);
                    ivMorning.setImageResource(R.drawable.morning_inactive);
                    tvMorningTitle.setTextColor(getResources().getColor(R.color.time_grey));
                    tvMorningTiming.setTextColor(getResources().getColor(R.color.time_grey));
                    tvMorningTiming.setText(tvMorningTiming.getText().toString());
                } else {
                    isMorningSelected = true;
                    ivMorningReminderStatus.setImageResource(R.drawable.reminder_active);
                    ivMorning.setImageResource(R.drawable.morning_active);
                    tvMorningTitle.setTextColor(getResources().getColor(R.color.time_grey));
                    tvMorningTiming.setTextColor(getResources().getColor(R.color.bg_color));
                    tvMorningTiming.setText(Html.fromHtml("<u>"+tvMorningTiming.getText().toString()+"</u>"));
                }
                enableDisableAlaram(tvMorningTiming.getText().toString(), 1, isMorningSelected);
                break;
            case R.id.ivAfternoonReminderStatus:
                if (isAfternoonSelected) {
                    isAfternoonSelected = false;
                    ivAfternoonReminderStatus.setImageResource(R.drawable.reminder_inactive);
                    ivAfternoon.setImageResource(R.drawable.afternoon_inactive);
                    tvAfternoonTitle.setTextColor(getResources().getColor(R.color.time_grey));
                    tvAfternoonTiming.setTextColor(getResources().getColor(R.color.time_grey));
                    tvAfternoonTiming.setText(tvAfternoonTiming.getText().toString());

                } else {
                    isAfternoonSelected = true;
                    ivAfternoonReminderStatus.setImageResource(R.drawable.reminder_active);
                    ivAfternoon.setImageResource(R.drawable.afternoon_active);
                    tvAfternoonTitle.setTextColor(getResources().getColor(R.color.time_grey));
                    tvAfternoonTiming.setTextColor(getResources().getColor(R.color.bg_color));
                    tvAfternoonTiming.setText(Html.fromHtml("<u>"+tvAfternoonTiming.getText().toString()+"</u>"));
                }
                enableDisableAlaram(tvAfternoonTiming.getText().toString(), 2, isAfternoonSelected);
                break;
            case R.id.ivNightReminderStatus:
                if (isNighSelected) {
                    isNighSelected = false;
                    ivNightReminderStatus.setImageResource(R.drawable.reminder_inactive);
                    ivNight.setImageResource(R.drawable.night_inactive);
                    tvNightTitle.setTextColor(getResources().getColor(R.color.time_grey));
                    tvNightTiming.setTextColor(getResources().getColor(R.color.time_grey));
                    tvNightTiming.setText(tvNightTiming.getText().toString());
                } else {
                    isNighSelected = true;
                    ivNightReminderStatus.setImageResource(R.drawable.reminder_active);
                    ivNight.setImageResource(R.drawable.night_active);
                    tvNightTitle.setTextColor(getResources().getColor(R.color.time_grey));
                    tvNightTiming.setTextColor(getResources().getColor(R.color.bg_color));
                    tvNightTiming.setText(Html.fromHtml("<u>"+tvNightTiming.getText().toString()+"</u>"));
                }
                enableDisableAlaram(tvNightTiming.getText().toString(), 3, isAfternoonSelected);
                break;
            case R.id.ivMorningDone:
                time = seekbarOne.mPopupTextView.getText().toString();
                tvMorningTiming.setText(time);
                llMorningCollpased.performClick();
                updateAlaram(time, 1);
                break;
            case R.id.ivAfternoonDone:
                time = seekbarTwo.mPopupTextView.getText().toString();
                tvAfternoonTiming.setText(time);
                llAfternoonCollpased.performClick();
                updateAlaram(time, 2);
                break;
            case R.id.ivNightDone:
                time = seekbarThree.mPopupTextView.getText().toString();
                tvNightTiming.setText(time);
                llNightCollpased.performClick();
                updateAlaram(time, 3);
                break;
            case R.id.llMorningCollpased:
                hideAllExpandedLayouts();
                if (isMorningCollapsed && isMorningSelected) {
                    isMorningCollapsed = false;
                    llMorningExpand.setVisibility(View.VISIBLE);
                    divider1.setVisibility(View.VISIBLE);
                    seekbarOne.showPopup();

                    int position = Arrays.asList(SeekBarHint.seekbarOne).indexOf(tvMorningTiming.getText().toString().trim());
                    if (position != -1){
                        seekbar1TextProgress = (position+1) * 9;
                    }
                    seekbarOne.setProgress(seekbar1TextProgress);
                    seekbarOne.onStartTrackingTouch(seekbarOne);
                    seekbarOne.mPopupTextView.setVisibility(View.VISIBLE);
                } else
                    isMorningCollapsed = true;
                break;
            case R.id.llAfternoonCollpased:
                hideAllExpandedLayouts();
                if (isAfternoonCollapsed && isAfternoonSelected) {
                    isAfternoonCollapsed = false;
                    llAfternoonExpand.setVisibility(View.VISIBLE);
                    divider2.setVisibility(View.VISIBLE);
                    seekbarTwo.showPopup();
                    int position = Arrays.asList(SeekBarHint.seekbarTwo).indexOf(tvAfternoonTiming.getText().toString().trim());
                    if (position != -1){
                        seekbar2TextProgress = (position+1) * 10;
                    }
                    seekbarTwo.setProgress(seekbar2TextProgress);
                    seekbarTwo.onStartTrackingTouch(seekbarOne);
                    seekbarTwo.mPopupTextView.setVisibility(View.VISIBLE);
                } else
                    isAfternoonCollapsed = true;
                break;
            case R.id.llNightCollpased:
                hideAllExpandedLayouts();
                if (isNighCollapsed  && isNighSelected) {
                    isNighCollapsed = false;
                    llNightExpand.setVisibility(View.VISIBLE);
                    divider3.setVisibility(View.VISIBLE);
                    seekbarThree.showPopup();
                    int position = Arrays.asList(SeekBarHint.seekbarThree).indexOf(tvNightTiming.getText().toString().trim());
                    if (position != -1){
                        seekbar3TextProgress = (position+1) * 9;
                    }
                    seekbarThree.setProgress(seekbar3TextProgress);
                    seekbarThree.onStartTrackingTouch(seekbarOne);
                    seekbarThree.mPopupTextView.setVisibility(View.VISIBLE);
                } else
                    isNighCollapsed = true;
                break;
        }

    }

    private void updateAlaram(String time, int dayID){

        int hour = Integer.parseInt(time.split(":")[0]);
        int minutes = Integer.parseInt(time.split(":")[1].split(" ")[0]);
        String dayMode = (time.split(":")[1].split(" ")[1]);
        if (dayMode.equalsIgnoreCase("PM")){
            hour = hour + 12;
        }

        String patientName = LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFullName();
        String medicineName = patientCaseRow.getMedicine();
        long rowID = patientCaseRow.getRowID();

        PatientCasePrescribObj patientCasePrescribObj = patientCaseRow.getPatientprHistory();
        int noofdays = patientCasePrescribObj.getNumberofDays();

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(patientCaseRow.getConsultationDt());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, 0);

        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATE_FORMAT);
        for (int i = 0 ; i <= noofdays; i++) {

            if (DateUtils.isToday(calendar) || (!DateUtils.isBeforeDay((calendar), Calendar.getInstance()))) {
                if (calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis())
                    try {
                        String reminderDateTime = dateTimeFormat.format(calendar.getTime());
                        Cursor c = ApplicationConstants.dbAdapter.checkIfRecordExist((patientCaseRow.getCaseID()),rowID, dayID, reminderDateTime);
                        if (c != null && c.getCount() > 0) {
                            ApplicationConstants.dbAdapter.deleteReminder(c.getLong(c.getColumnIndex(RemindersDbAdapter.KEY_ROWID)));
                            new ReminderManager(getApplicationContext()).cancelReminder(c.getLong(c.getColumnIndex(RemindersDbAdapter.KEY_ROWID)), dayID, calendar);
                        }
                        long recordID = ApplicationConstants.dbAdapter.createReminder(medicineName, reminderDateTime, rowID, patientName, dayID, time, (patientCaseRow.getCaseID()));
                        new ReminderManager(getApplicationContext()).setReminder(recordID, dayID, calendar);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
            calendar.add(Calendar.DATE, 1);
        }
    }

    private void enableDisableAlaram(String time, int dayID, boolean isEnable){

        int hour = Integer.parseInt(time.split(":")[0]);
        int minutes = Integer.parseInt(time.split(":")[1].split(" ")[0]);
        String dayMode = (time.split(":")[1].split(" ")[1]);
        if (dayMode.equalsIgnoreCase("PM")){
            hour = hour + 12;
        }

        String patientName = LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFullName();
        String medicineName = patientCaseRow.getMedicine();
        String rowID = String.valueOf(patientCaseRow.getRowID());

        PatientCasePrescribObj patientCasePrescribObj = patientCaseRow.getPatientprHistory();
        int noofdays = patientCasePrescribObj.getNumberofDays();
        int pastDaysCount = 0;

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(patientCaseRow.getConsultationDt());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, 0);

        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATE_FORMAT);
        for (int i = 0 ; i <= noofdays; i++) {

            if (DateUtils.isToday(calendar) || (!DateUtils.isBeforeDay((calendar), Calendar.getInstance()))) {
                if (calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis())
                    try {
                        String reminderDateTime = dateTimeFormat.format(calendar.getTime());
                        long recordID;
                        Cursor c = ApplicationConstants.dbAdapter.checkIfRecordExist((patientCaseRow.getCaseID()), Long.parseLong(rowID), dayID, reminderDateTime);
                        if (c == null || c.getCount() == 0) {
                            recordID = ApplicationConstants.dbAdapter.createReminder(medicineName, reminderDateTime, patientCaseRow.getRowID(), patientName, dayID, time, patientCaseRow.getCaseID());
                            new ReminderManager(getApplicationContext()).setReminder(recordID, dayID, calendar);
                        }else{
                            recordID = c.getLong(c.getColumnIndex(RemindersDbAdapter.KEY_ROWID));
                            ApplicationConstants.dbAdapter.deleteReminder(recordID);
                            new ReminderManager(getApplicationContext()).cancelReminder(recordID, dayID, calendar);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
            calendar.add(Calendar.DATE, 1);
        }
    }

    public void hideAllExpandedLayouts() {
        llMorningExpand.setVisibility(View.GONE);
        llAfternoonExpand.setVisibility(View.GONE);
        llNightExpand.setVisibility(View.GONE);

        divider1.setVisibility(View.GONE);
        divider2.setVisibility(View.GONE);
        divider3.setVisibility(View.GONE);

        seekbarOne.hidePopup();
        seekbarTwo.hidePopup();
        seekbarThree.hidePopup();

        seekbarOne.mPopupTextView.setVisibility(View.GONE);
        seekbarTwo.mPopupTextView.setVisibility(View.GONE);
        seekbarThree.mPopupTextView.setVisibility(View.GONE);
    }
}