package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hCue.Hcue.CallBackInterfaces.OnRegistrationStepOneCallBack;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;

/**
 * Created by Appdest on 16-09-2016.
 */
@SuppressLint("ValidFragment")
public class RegisterStepOne extends Fragment {

    public OnRegistrationStepOneCallBack mCallBack;
    public Context context;
    private EditText etEmail, etPassword;
    private TextView tvLogin, tvTerms,tvYouAgreee,tvAreyou,tvShow;
    private Button btnContinue;
    private RippleView rvContinue;
    private TextInputLayout tvEmail,tvPassword;
    private Preference preference;

    public RegisterStepOne() {
    }

    public RegisterStepOne(Context context, OnRegistrationStepOneCallBack onRegistrationStepOneCallBack)
    {
        this.context = context;
        this.mCallBack = onRegistrationStepOneCallBack;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.register_step_one, container, false);

        preference = new Preference(context);
        preference.saveBoolean("ShowText", false);
        preference.commitPreference();

        etEmail     =   (EditText)          v.findViewById(R.id.etEmail);
        etPassword  =   (EditText)          v.findViewById(R.id.etPassword);
        tvLogin     =   (TextView)          v.findViewById(R.id.tvLogin);
        tvTerms     =   (TextView)          v.findViewById(R.id.tvTerms);
        tvShow      =   (TextView)          v.findViewById(R.id.tvShow);
        tvYouAgreee =   (TextView)          v.findViewById(R.id.tvYouAgreee);
        tvAreyou    =   (TextView)          v.findViewById(R.id.tvAreyou);
        rvContinue  =   (RippleView)        v.findViewById(R.id.rvContinue);
        tvEmail     =   (TextInputLayout)   v.findViewById(R.id.tvEmail);
        tvPassword  =   (TextInputLayout)   v.findViewById(R.id.tvEmail);
        btnContinue =   (Button)            v.findViewById(R.id.btnContinue);

        etEmail.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etPassword.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvLogin.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvTerms.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        btnContinue.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvEmail.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvPassword.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvYouAgreee.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvAreyou.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvShow.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        if(RegistrationActivity.isGplusLogin){
            etPassword.setVisibility(View.GONE);
            tvShow.setVisibility(View.GONE);
        }

        if (RegistrationActivity.registrationDO.email != null && !RegistrationActivity.registrationDO.email.equalsIgnoreCase("")){
            etEmail.setText(RegistrationActivity.registrationDO.email);
        }

        tvShow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                setPasswordInputtype();
            }
        });

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                validateInputFields();
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent slideactivity = new Intent(getActivity(), TermsAndConditions.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.push_up_in,R.anim.push_up_out).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent slideactivity = new Intent(getActivity(), Login.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.push_up_in,R.anim.push_up_out).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

        return v;
    }

    public void validateInputFields() {
        String emaild = etEmail.getText().toString().trim();
        String pwd = etPassword.getText().toString().trim();
        if (TextUtils.isEmpty(emaild)) {
            ShowAlertDialog("Alert","Email should not be empty.",R.drawable.alert);
        } else if (!isValidEmail(emaild)) {
            ShowAlertDialog("Alert","Please enter a valid emailid.",R.drawable.alert);
        } else if (!RegistrationActivity.isGplusLogin) {

            if (TextUtils.isEmpty(pwd)) {
                ShowAlertDialog("Alert","Password should not be empty.",R.drawable.alert);
            } else if (pwd.length() < 6) {
                ShowAlertDialog("Alert","Password must Contain atleast 6 Characters.",R.drawable.alert);
            }else {
                RegistrationActivity.registrationDO.email = emaild;
                RegistrationActivity.registrationDO.password = pwd;
                mCallBack.onContinueClickListener(1);
            }
        }else {
            RegistrationActivity.registrationDO.email = emaild;
            RegistrationActivity.registrationDO.password = pwd;
            mCallBack.onContinueClickListener(1);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallBack = (OnRegistrationStepOneCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    private boolean isValidEmail(String et_Username) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(et_Username);
        return matcher.matches();
    }

    private void setPasswordInputtype() {
        //Set input Type Password

        if (preference.getbooleanFromPreference("ShowText", false)) {
            tvShow.setText("SHOW");
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            preference.saveBoolean("ShowText", false);
            preference.commitPreference();
        } else {
            //Set input Type Text

            tvShow.setText("HIDE");
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            preference.saveBoolean("ShowText", true);
            preference.commitPreference();
        }
    }

    public void ShowAlertDialog(String header, String message, int resourceid)
    {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
        final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        final TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        ivLogo.setBackgroundResource(resourceid);
        tvInfo.setTextSize(18);

        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.VISIBLE);

        tvNo.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        tvYes.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });
    }
}