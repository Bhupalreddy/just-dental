package hCue.Hcue;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.doctoror.geocoder.Address;
import com.doctoror.geocoder.Geocoder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import hCue.Hcue.utils.CookieService;
import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import hCue.Hcue.model.PatientLookupResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestClient2;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.ConnectivityReceiver;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.CookieSingleton;
import hCue.Hcue.utils.CookieValues;
import hCue.Hcue.utils.Preference;
import io.fabric.sdk.android.Fabric;
import pl.droidsonroids.gif.GifImageView;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Appdest on 10-08-2016.
 */
public class SplashActivityOne extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult>,
        ConnectivityReceiver.ConnectivityReceiverListener
{
        private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        private static final int SPLASHTIME = 5000;
        protected static final String TAG = "MainActivity";
        protected static final int REQUEST_CHECK_SETTINGS = 0x1;
        public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
        public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
                UPDATE_INTERVAL_IN_MILLISECONDS / 2;
        protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
        protected final static String KEY_LOCATION = "location";
        protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
        protected GoogleApiClient mGoogleApiClient;
        protected LocationRequest mLocationRequest;
        protected LocationSettingsRequest mLocationSettingsRequest;
        protected Location mCurrentLocation;
        protected Boolean mRequestingLocationUpdates;
        protected String mLastUpdateTime;
        private Preference preference;
        private static AlertDialog alertDialog;
        private CookieService cookieService ;
        private ConnectivityReceiver connectivityReceiver;
        private Bundle savedInstanceState;
        public static String device_uuid;
        private GifImageView gifImage;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.splash);
            this.savedInstanceState = savedInstanceState;
            connectivityReceiver = new ConnectivityReceiver();
            connectivityReceiver.setConnectivityListener(this);
            mRequestingLocationUpdates = false;
            mLastUpdateTime = "";
            preference = new Preference(this);
            gifImage    =   (GifImageView)  findViewById(R.id.gifImage);

            Intent service = new Intent(SplashActivityOne.this, IndefiniteService.class);
            startService(service);

            initializeFonts();
            ApplicationConstants.dbAdapter = ApplicationConstants.getDbAdapter(SplashActivityOne.this);
            device_uuid = android.provider.Settings.Secure.getString(this.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            if (Connectivity.isConnected(SplashActivityOne.this))
            {
                    ContinueFromSplash(savedInstanceState);
            }else
            {
                ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
            }
        }

    private void ContinueFromSplash(Bundle savedInstanceState)
    {
        final CookieValues cookieValues = CookieSingleton.getInstance();
        callCookieServices(cookieValues);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        String lookupata = preference.getStringFromPreference(Constants.DOCTOR_LOOKUP, "");
        //if(lookupata.isEmpty())
        {
            if (checkPlayServices()) {
                // Update values using data stored in the Bundle.
                updateValuesFromBundle(savedInstanceState);
                // Kick off the process of building the GoogleApiClient, LocationRequest, and
                // LocationSettingsRequest objects.

                int currentAPIVersion = Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M)
                {
                    if (ContextCompat.checkSelfPermission(SplashActivityOne.this, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    {
                        ActivityCompat.requestPermissions(SplashActivityOne.this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1234);
                    }
                    else
                    {
                        buildGoogleApiClient();
                        createLocationRequest();
                        buildLocationSettingsRequest();
                    }

                }else {
                    buildGoogleApiClient();
                    createLocationRequest();
                    buildLocationSettingsRequest();
                }
            }
        }
        /*else
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    moveToNextScreen();
                }
            },SPLASHTIME);
        }*/
    }

    private void callCookieServices(final CookieValues cookieValues)
    {
        RestClient2.getAPI(Constants.DOCTOR_CONSTANT_URL).getDoctorCookies(new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(String s, Response response)
            {
                Log.e("DOCTOR Headers",response.getHeaders().toString());
                for ( Header header :response.getHeaders())
                {
                    if(header.getName().toString().contains("CloudFront-Signature"))
                    {
                        cookieValues.setDoctorCloudFront_Signature(header.getValue());
                    } else if(header.getName().toString().equalsIgnoreCase("CloudFront-Policy"))
                    {
                        cookieValues.setDoctorCloudFront_Policy(header.getValue());
                    }else if(header.getName().toString().equalsIgnoreCase("CloudFront-Key-Pair-Id"))
                    {
                        cookieValues.setDoctorCloudFront_Key_Pair_Id(header.getValue());
                    }
                }
            }
        });
        RestClient2.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientCookies(new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(String s, Response response) {
                Log.e("PATIENT  Headers",response.getHeaders().toString());
                for ( Header header :response.getHeaders())
                {
                    if(header.getName().toString().equalsIgnoreCase("CloudFront-Signature"))
                    {
                        cookieValues.setPatientCloudFront_Signature(header.getValue());
                    } else if(header.getName().toString().equalsIgnoreCase("CloudFront-Policy"))
                    {
                        cookieValues.setPatientCloudFront_Policy(header.getValue());
                    }else if(header.getName().toString().equalsIgnoreCase("CloudFront-Key-Pair-Id"))
                    {
                        cookieValues.setPatientCloudFront_Key_Pair_Id(header.getValue());
                    }
                }
            }
        });
        RestClient2.getAPI(Constants.DEVICE_REGISTRATION).getPaltformCookies(new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(String s, Response response) {
                Log.e("PLATFORM  Headers",response.getHeaders().toString());

                for ( Header header :response.getHeaders())
                {
                    if(header.getName().toString().equalsIgnoreCase("CloudFront-Signature"))
                    {
                        cookieValues.setPlatformCloudFront_Signature(header.getValue());
                    } else if(header.getName().toString().equalsIgnoreCase("CloudFront-Policy"))
                    {
                        cookieValues.setPlatformCloudFront_Policy(header.getValue());
                    }else if(header.getName().toString().equalsIgnoreCase("CloudFront-Key-Pair-Id"))
                    {
                        cookieValues.setPlatformCloudFront_Key_Pair_Id(header.getValue());
                    }
                }
            }
        });
    }

    private void initializeFonts()
    {

        ApplicationConstants.WALSHEIM_MEDIUM 	= Typeface.createFromAsset(getApplicationContext().getAssets(), "ProximaNova-Regular.otf");;
        ApplicationConstants.WALSHEIM_SEMI_BOLD	= Typeface.createFromAsset(getApplicationContext().getAssets(), "proximanova-semibold-webfont.ttf");
        ApplicationConstants.WALSHEIM_BOLD	= Typeface.createFromAsset(getApplicationContext().getAssets(), "proximanova-semibold-webfont.ttf");

    }


    /**
         * Updates fields based on data stored in the bundle.
         *
         * @param savedInstanceState The activity state saved in the Bundle.
         */
        private void updateValuesFromBundle(Bundle savedInstanceState) {
            if (savedInstanceState != null) {
                // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
                // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
                if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                    mRequestingLocationUpdates = savedInstanceState.getBoolean(
                            KEY_REQUESTING_LOCATION_UPDATES);
                }

                // Update the value of mCurrentLocation from the Bundle and update the UI to show the
                // correct latitude and longitude.
                if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                    // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                    // is not null.
                    mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
                }

                // Update the value of mLastUpdateTime from the Bundle and update the UI.
                if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                    mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
                }
                //updateUI();
            }
        }

        /**
         * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
         * LocationServices API.
         */
        protected synchronized void buildGoogleApiClient() {
            Log.i(TAG, "Building GoogleApiClient");
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        /**
         * Sets up the location request. Android has two location request settings:
         * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
         * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
         * the AndroidManifest.xml.
         * <p/>
         * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
         * interval (5 seconds), the Fused Location Provider API returns location updates that are
         * accurate to within a few feet.
         * <p/>
         * These settings are appropriate for mapping applications that show real-time location
         * updates.
         */
        protected void createLocationRequest() {
            mLocationRequest = new LocationRequest();

            // Sets the desired interval for active location updates. This interval is
            // inexact. You may not receive updates at all if no location sources are available, or
            // you may receive them slower than requested. You may also receive updates faster than
            // requested if other applications are requesting location at a faster interval.
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

            // Sets the fastest rate for active location updates. This interval is exact, and your
            // application will never receive updates faster than this value.
            mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }

        /**
         * Uses a {@link LocationSettingsRequest.Builder} to build
         * a {@link LocationSettingsRequest} that is used for checking
         * if a device has the needed location settings.
         */
        protected void buildLocationSettingsRequest() {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
        }

        /**
         * Check if the device's location settings are adequate for the app's needs using the
         * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
         * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
         */
        protected void checkLocationSettings() {
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(
                            mGoogleApiClient,
                            mLocationSettingsRequest
                    );
            result.setResultCallback(this);
        }

        /**
         * The callback invoked when
         * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
         * LocationSettingsRequest)} is called. Examines the
         * {@link LocationSettingsResult} object and determines if
         * location settings are adequate. If they are not, begins the process of presenting a location
         * settings dialog to the user.
         */
        @Override
        public void onResult(LocationSettingsResult locationSettingsResult) {
            final Status status = locationSettingsResult.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    Log.i(TAG, "All location settings are satisfied.");
                    startLocationUpdates();
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings ");

                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(SplashActivityOne.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        Log.i(TAG, "PendingIntent unable to execute request.");
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                            "not created.");
                    break;
            }
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                // Check for the integer request code originally supplied to startResolutionForResult().
                case REQUEST_CHECK_SETTINGS:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            Log.i(TAG, "User agreed to make required location settings changes.");
                            startLocationUpdates();
                            break;
                        case Activity.RESULT_CANCELED:
                            Log.i(TAG, "User chose not to make required location settings changes.");
                            finish();
                            break;
                    }
                    break;
            }
        }

        /**
         * Handles the Start Updates button and requests start of location updates. Does nothing if
         * updates have already been requested.
         */
        public void startUpdatesButtonHandler(View view) {
            checkLocationSettings();
        }

        /**
         * Handles the Stop Updates button, and requests removal of location updates.
         */
        public void stopUpdatesButtonHandler(View view) {
            // It is a good practice to remove location requests when the activity is in a paused or
            // stopped state. Doing so helps battery performance and is especially
            // recommended in applications that request frequent location updates.
            stopLocationUpdates();
        }

        /**
         * Requests location updates from the FusedLocationApi.
         */
        protected void startLocationUpdates() {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                }
            });

        }

        /**
         * Updates all UI fields.
         */
        private void updateUI()
        {
            setButtonsEnabledState();
            updateLocationUI();
        }

        /**
         * Disables both buttons when functionality is disabled due to insuffucient location settings.
         * Otherwise ensures that only one button is enabled at any time. The Start Updates button is
         * enabled if the user is not requesting location updates. The Stop Updates button is enabled
         * if the user is requesting location updates.
         */
        private void setButtonsEnabledState()
        {

        }

        /**
         * Sets the value of the UI fields for the location latitude, longitude and last update time.
         */
        private void updateLocationUI() {
            if (mCurrentLocation != null) {

                String address = GetAddress(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                stopLocationUpdates();
                moveToNextScreen();
                Log.e("CURRRENT LATLONG", ""+mCurrentLocation.getProvider()+" , "+mCurrentLocation.getLatitude()+","+mCurrentLocation.getLongitude());
            }
        }

    public String GetAddress(final double lat, final double lon) {
        final Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        final StringBuilder ret = new StringBuilder();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Address> addresses = null;
                    try {
                        addresses = geocoder.getFromLocation(lat,lon, 1, true);
                    } catch (Geocoder.LimitExceededException e) {
                        e.printStackTrace();
                    }
                    if (addresses != null && !addresses.isEmpty())
                    {
                        Address returnedAddress = addresses.get(0);
                        StringBuilder strReturnedAddress = new StringBuilder("Address:\n");
                        //for (int i = 0; i < returnedAddress.getFormattedAddress(); i++)
                        {

                            strReturnedAddress.append(returnedAddress.getFormattedAddress()).append("\n");
                        }
                        final String cityName = addresses.get(0).getLocality();
                        final String stateName = addresses.get(0).getSubLocality();
                        Preference preference = new Preference(SplashActivityOne.this);
                        preference.saveStringInPreference("LOCALITY",stateName+", "+cityName);
                        preference.saveStringInPreference("LATLONG",lat+", "+lon);
                        preference.saveStringInPreference("CURRENTLATLONG",lat+", "+lon);

                        ret.append(strReturnedAddress.toString());
                        preference.saveStringInPreference("ADDRESS",ret.toString());
                        preference.commitPreference();
//                tvaddress.setText(ret);
                    } else {
                        ret.append("No Address returned!");
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    ret.append("Can't get Address!");
                }
            }
        }).start();

//        Toast.makeText(SplashActivity.this,"IS GEO CODER PRESENT : "+Geocoder.isPresent(),Toast.LENGTH_LONG).show();
            return ret.toString();
    }

    public void moveToNextScreen()
    {
        boolean islogout = preference.getbooleanFromPreference("ISLOGOUT", true);
        if(islogout){

            Intent splashActivty = new Intent(getApplicationContext(), LaunchActivity.class);
            startActivity(splashActivty);
            finish();

        }else{
            if (Connectivity.isConnected(SplashActivityOne.this))
            {
                Intent intent = new Intent(SplashActivityOne.this, CookieService.class);
                startService(intent);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);


            }else
            {
                ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
            }
        }
    }

    public void ShowAlertDialog(String header,String message, int resourceid)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SplashActivityOne.this);
        View dialogView = LayoutInflater.from(SplashActivityOne.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvHeading.setText(header);
        tvHeading.setTextSize(20);
        tvInfo.setText(message);
        tvInfo.setTextSize(18);
        ivLogo.setBackgroundResource(resourceid);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.VISIBLE);
        ivLogo.setVisibility(View.VISIBLE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder binder) {
            CookieService.LocalBinder b = (CookieService.LocalBinder) binder;
            cookieService = b.getServiceInstance();
            cookieService.getCookies((8*(60*1000)));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    moveToNextScreen1();

                    /*Intent splashActivty = new Intent(getApplicationContext(), Specialities.class);
                    startActivity(splashActivty);
                    finish();*/
                    moveToNextScreen1();

                }
            },1000);


            //Toast.makeText(SplashActivity.this, "Connected", Toast.LENGTH_SHORT).show();
        }
        public void onServiceDisconnected(ComponentName className) {
            cookieService = null;
        }
    };

    public void moveToNextScreen1() {
        boolean islogout = preference.getbooleanFromPreference("ISLOGOUT", true);
        if(islogout)
        {
            // New User
            Intent splashActivty = new Intent(getApplicationContext(), LaunchActivity.class);
            startActivity(splashActivty);
            finish();
        }
        else
        {
            // Existing user
            Intent splashActivty = new Intent(getApplicationContext(), Specialities.class);
            startActivity(splashActivty);
            finish();
        }
    }

    /*private void callLookupTableService()
    {
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getdoctorLookup(new RestCallback<PatientLookupResponse>() {
            @Override
            public void failure(RestError restError) {

                callLookupTableService();
            }

            @Override
            public void success(PatientLookupResponse patientLookupResponse, Response response) {
                Gson gson = new Gson();
                String json = gson.toJson(patientLookupResponse);
                preference.saveStringInPreference(Constants.DOCTOR_LOOKUP, json);
                preference.commitPreference();
                boolean islogout = preference.getbooleanFromPreference("ISLOGOUT", true);
                if(islogout)
                {
                    // New User
                    Intent splashActivty = new Intent(getApplicationContext(), LaunchActivity.class);
                    startActivity(splashActivty);
                    finish();
                }
                else
                {
                    // Existing user
                    Intent splashActivty = new Intent(getApplicationContext(), Specialities.class);
                    startActivity(splashActivty);
                    finish();
                }
            }
        });
    }*/

    /**
         * Removes location updates from the FusedLocationApi.
         */
        protected void stopLocationUpdates() {
            // It is a good practice to remove location requests when the activity is in a paused or
            // stopped state. Doing so helps battery performance and is especially
            // recommended in applications that request frequent location updates.
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = false;
                    setButtonsEnabledState();
                }
            });
        }

        @Override
        protected void onStart() {
            super.onStart();
            if(mGoogleApiClient != null)
            mGoogleApiClient.connect();
        }

        @Override
        public void onResume() {
            super.onResume();
            // Within {@code onPause()}, we pause location updates, but leave the
            // connection to GoogleApiClient intact.  Here, we resume receiving
            // location updates if the user has requested them.
            if (mGoogleApiClient != null &&mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                startLocationUpdates();
            }
        }

        @Override
        protected void onPause() {
            super.onPause();
            // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                stopLocationUpdates();
            }
        }

        @Override
        protected void onStop() {
            super.onStop();
            if(mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        }

        /**
         * Runs when a GoogleApiClient object successfully connects.
         */
        @Override
        public void onConnected(Bundle connectionHint) {
            Log.i(TAG, "Connected to GoogleApiClient");

            // If the initial location was never previously requested, we use
            // FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
            // its value in the Bundle and check for it in onCreate(). We
            // do not request it again unless the user specifically requests location updates by pressing
            // the Start Updates button.
            //
            // Because we cache the value of the initial location in the Bundle, it means that if the
            // user launches the activity,
            // moves to a new location, and then changes the device orientation, the original location
            // is displayed as the activity is re-created.
            if (mCurrentLocation == null) {
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                //mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                checkLocationSettings();
            }
        }

        /**
         * Callback that fires when the location changes.
         */
        @Override
        public void onLocationChanged(Location location) {
            mCurrentLocation = location;
            updateLocationUI();
        }

        @Override
        public void onConnectionSuspended(int cause) {
            Log.i(TAG, "Connection suspended");
        }

        @Override
        public void onConnectionFailed(ConnectionResult result) {
            // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
            // onConnectionFailed.
            Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
        }

        /**
         * Stores activity data in the Bundle.
         */
        public void onSaveInstanceState(Bundle savedInstanceState) {
            savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
            savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
            savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
            super.onSaveInstanceState(savedInstanceState);
        }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                Toast.makeText(SplashActivityOne.this, "This device is not supported GooglePlayService",Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected)
    {
        if(isConnected)
        {
            ContinueFromSplash(savedInstanceState);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults)
    {
        switch (requestCode)
        {
            case 1234:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    buildGoogleApiClient();
                    if(mGoogleApiClient != null)
                        mGoogleApiClient.connect();
                    createLocationRequest();
                    buildLocationSettingsRequest();
                } else
                {
                    finish();
                }
                break;
            }
        }
    }}

