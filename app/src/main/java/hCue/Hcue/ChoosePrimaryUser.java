package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.RegisterReqSingleton;
import hCue.Hcue.adapters.ChoosePrimaryUserAdapter;
import hCue.Hcue.model.RegisterRequest;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.widget.RippleView;

/**
 * Created by shyamprasadg on 08/07/16.
 */
public class ChoosePrimaryUser extends BaseActivity
{
    private LinearLayout llPrimaryUser;
    private TextView     tvPhoneNo,tvFindName;
    private ListView     lvPeople;
    private RippleView rvContinue;
    private Button       btnContinue;
    private ChoosePrimaryUserAdapter    choosePrimaryUserAdapter;
    private ArrayList<PatientRow> patientRows;
    @Override
    public void initialize()
    {
        llPrimaryUser = (LinearLayout) getLayoutInflater().inflate(R.layout.people_list,null,false);
        llBody.addView(llPrimaryUser,baseLayoutParams);

        patientRows = (ArrayList<PatientRow>) getIntent().getSerializableExtra("PATIENTS");

        tvPhoneNo       =   (TextView)      llPrimaryUser.findViewById(R.id.tvPhoneNo);
        tvFindName      =   (TextView)      llPrimaryUser.findViewById(R.id.tvFindName);
        lvPeople        =   (ListView)      llPrimaryUser.findViewById(R.id.lvPeople);
        rvContinue      =   (RippleView)    llPrimaryUser.findViewById(R.id.rvContinue);
        btnContinue     =   (Button)        llPrimaryUser.findViewById(R.id.btnContinue);


        tvTopTitle.setText("CHOOSE YOUR PRIMARY USER");
        tvPhoneNo.setText("+91 "+(patientRows.get(0).getArrPatients().get(0).getPatientID()+"").substring(0,10));
        ivSearch.setVisibility(View.INVISIBLE);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        btnContinue.setVisibility(View.GONE);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        setSpecificTypeFace(llPrimaryUser, ApplicationConstants.WALSHEIM_MEDIUM);
        tvPhoneNo.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvFindName.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        choosePrimaryUserAdapter   =   new ChoosePrimaryUserAdapter(ChoosePrimaryUser.this, patientRows,btnContinue);
        lvPeople.setAdapter(choosePrimaryUserAdapter);

        tvFindName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                btnContinue.setVisibility(View.VISIBLE);
            }
        });

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent slideactivity = new Intent(ChoosePrimaryUser.this, BloodgroupActivity.class);
                if(choosePrimaryUserAdapter.getSelectedposition() != -1)
                {
                    PatientRow patientRow = patientRows.get(choosePrimaryUserAdapter.getSelectedposition());
                    RegisterRequest registerRequest = RegisterReqSingleton.getSingleton();
                    registerRequest.setPatientPhones(patientRow.getListPatientPhone());
                    Patient patient = patientRow.getArrPatients().get(0);
                    registerRequest.getPatientDetails2().setTitle(patient.getTitle());
                    registerRequest.getPatientDetails2().setFullName(patient.getFullName());
                    registerRequest.getPatientDetails2().setFirstName(patient.getFirstName());

                    Calendar calendar = Calendar.getInstance(Locale.getDefault());
                    calendar.setTimeInMillis(patient.getDOB());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    registerRequest.getPatientDetails2().setDOB(simpleDateFormat.format(calendar.getTime()));

                    slideactivity.putExtra("UPDATEPATINET", true);
                    slideactivity.putExtra("SELECTEDPATIENT", patientRow);
                }
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

    }
}
