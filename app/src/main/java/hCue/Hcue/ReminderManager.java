package hCue.Hcue;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

import hCue.Hcue.adapters.RemindersDbAdapter;

public class ReminderManager {

	private Context mContext;
	private AlarmManager mAlarmManager;
	
	public ReminderManager(Context context) {
		mContext = context; 
		mAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
	}
	
	public void setReminder(Long recordID, int taskId, Calendar when) {
		
        Intent i = new Intent(mContext, OnAlarmReceiver.class);
        i.putExtra(RemindersDbAdapter.KEY_ROWID, recordID);
//		i.putExtra(RemindersDbAdapter.KEY_DAY_ID, taskId);

        PendingIntent pi = PendingIntent.getBroadcast(mContext, (int)(long)recordID, i, PendingIntent.FLAG_UPDATE_CURRENT);
		mAlarmManager.cancel(pi);
		pi.cancel();
		pi = PendingIntent.getBroadcast(mContext, (int)(long)recordID, i, PendingIntent.FLAG_UPDATE_CURRENT);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, when.getTimeInMillis(), pi);
		}else
			mAlarmManager.set(AlarmManager.RTC_WAKEUP, when.getTimeInMillis(), pi);
	}

	public void cancelReminder(Long recordID, int taskId, Calendar when) {

		Intent i = new Intent(mContext, OnAlarmReceiver.class);
		i.putExtra(RemindersDbAdapter.KEY_ROWID, recordID);
//		i.putExtra(RemindersDbAdapter.KEY_DAY_ID, taskId);

		PendingIntent pi = PendingIntent.getBroadcast(mContext, (int)(long)recordID, i, PendingIntent.FLAG_UPDATE_CURRENT);
		mAlarmManager.cancel(pi);
		pi.cancel();
	}
}
