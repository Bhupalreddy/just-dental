package hCue.Hcue;

import android.app.ActivityOptions;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.gsm.SmsManager;
import android.telephony.gsm.SmsMessage;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import hCue.Hcue.model.ForgotPasswordCheckPhoneRequest;
import hCue.Hcue.model.OTPRequest;
import hCue.Hcue.model.OTPResponse;
import hCue.Hcue.model.OtpViaEmailRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;

/**
 * Created by CVLHYD-161 on 13-03-2016.
 */
public class ForgotPassowordCheckPhoneActivity extends AppCompatActivity implements View.OnClickListener{

    private static EditText etVerificationCode;
    private TextView tvSubmit,tvCheckPhoneText,tvResendOtp,tvTitle;
    BaseActivity baseActivity;
    public ProgressDialog progressdialog;
    private LinearLayout llVerifyOTP , llBack;
    private String phone , emailID;
    private RippleView rvResendOtp;
    private OtpViaEmailRequest otpviaemailReq ;
    private boolean isResetViaMobile ;
    private static  AlertDialog alertDialog;
    private OTPRequest otpRequest ;
    private TextInputLayout tilOTP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_otp);

        phone     = getIntent().getStringExtra("Mobile");
        emailID   = getIntent().getStringExtra("Emailid");
        isResetViaMobile   = getIntent().getBooleanExtra("isResetViaMobile",false);
        otpviaemailReq   = (OtpViaEmailRequest) getIntent().getSerializableExtra("EMAILREQ");
        otpRequest = (OTPRequest) getIntent().getSerializableExtra("OTPREQ");

        llVerifyOTP         =   (LinearLayout)      findViewById(R.id.llVerifyOTP);
        llBack              =   (LinearLayout)      findViewById(R.id.llBack);
        etVerificationCode  =   (EditText)          findViewById(R.id.etVerificationCode);
        tvSubmit            =   (TextView)          findViewById(R.id.tvSubmit);
        tvCheckPhoneText    =   (TextView)          findViewById(R.id.tvCheckPhoneText);
        tvTitle             =   (TextView)          findViewById(R.id.tvTitle);
        tvResendOtp         =   (TextView)          findViewById(R.id.tvResendOtp);
        rvResendOtp         =   (RippleView)        findViewById(R.id.rvResendOtp);
        tilOTP              =   (TextInputLayout)   findViewById(R.id.tilOTP);

        tilOTP.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvTitle.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        if(isResetViaMobile)
        ShowAlertDialog("Automatically detecting OTP sent by SMS.Please wait...");

        if(isResetViaMobile) {
            if (phone != null && phone.length() == 10)
                tvCheckPhoneText.setText("We have texted a code to the phone number ending in " + ((phone != null && phone.length() == 10) ? phone.substring(7, 10) : "") + ". Once you receive the code, \n enter it below to reset your password.");
        }else
        {
            tvCheckPhoneText.setText(String.format("We have texted a code to your email id %s ******@%s . Once you receive the code, \n enter it below to reset your password.",emailID.substring(0,2),emailID.split("@")[1]));
        }
        tvSubmit.setOnClickListener(this);


        BaseActivity.setSpecificTypeFace(llVerifyOTP, ApplicationConstants.WALSHEIM_MEDIUM);
        llBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });

        rvResendOtp.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView)
            {
                if(!isResetViaMobile)
                {
                    if (Connectivity.isConnected(ForgotPassowordCheckPhoneActivity.this)) {
                        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).sendOTPViaMail(otpviaemailReq, new RestCallback<String>() {
                            @Override
                            public void failure(RestError restError) {
                                ShowAlertDialog("Failed to send OTP try again.");
                                //Toast.makeText(ForgotPassowordUpdatePinPasswordActivity.this, "failure", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void success(String s, Response response)
                            {
                                ShowAlertDialog("An OTP has sent to your email please check and enter.");
                            }
                        });
                    } else {
                        ShowAlertDialog("Please check internet connection.");
                    }

                }else
                {
                    if (Connectivity.isConnected(ForgotPassowordCheckPhoneActivity.this)) {
                        RestClient.getAPI("https://api.checkmobi.com").sendOPT(otpRequest, new RestCallback<OTPResponse>() {
                            @Override
                            public void failure(RestError restError) {
                                ShowAlertDialog("Failed to send OTP try again.");
                                //Toast.makeText(ChooseVerificationMode.this, "Failed to send OTP try again.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void success(OTPResponse otpResponse, Response response) {

                                ShowAlertDialog("Automatically detecting OTP sent by SMS.Please wait...");
                            }
                        });
                    } else {
                        ShowAlertDialog("Please check internet connection.");
                    }

                }
            }
        });
    }

    private void callSendOTP(OTPRequest otpRequest)
    {
        RestClient.getAPI("https://api.checkmobi.com").sendOPT(otpRequest, new RestCallback<OTPResponse>() {
            @Override
            public void failure(RestError restError) {
                ShowAlertDialog("Failed to send OTP try again.");
            }

            @Override
            public void success(OTPResponse otpResponse, Response response) {

                ShowAlertDialog("Automatically detecting OTP sent by SMS.Please wait...");
            }
        });
    }

    private void sendViaNotification(String passcode) {

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // intent triggered, you can add other intent for other actions

        // this is it, we'll build the notification!
        // in the addAction method, if you don't want any icon, just set the first param to 0
        Notification mNotification = null;
        if (Build.VERSION.SDK_INT < 16) {
            mNotification = new Notification.Builder(this)
                    .setContentTitle("hCue Passcode")
                    .setContentText(passcode)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setSound(soundUri)
                    .getNotification();
        } else {
            mNotification = new Notification.Builder(this)
                    .setContentTitle("hCue Passcode")
                    .setContentText(passcode)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setSound(soundUri).setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(new long[0])
                    .build();
        }
        PowerManager pm = (PowerManager) this
                .getSystemService(Context.POWER_SERVICE);

        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        wl.acquire(15000);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, mNotification);
    }

    private void sendOTPViaEmail() {
        OtpViaEmailRequest request = new OtpViaEmailRequest();
        request.setUSRId((int)ApplicationConstants.forgotPasswordFindDetailsResponse.getDoctorID());
        request.setToAddress(ApplicationConstants.forgotPasswordFindDetailsResponse.getEmailID());
        request.setMailContent(ApplicationConstants.PASS_CODE);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).sendOTPViaMail(request, new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(String s, Response response) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvSubmit) {

            if (etVerificationCode.getText().toString().trim().isEmpty())
            {
                ShowAlertDialog("Please enter passcode.");
                return;
            }

            if (etVerificationCode.getText().toString().trim().equalsIgnoreCase(ApplicationConstants.PASS_CODE)) {

                ForgotPasswordCheckPhoneRequest request = new ForgotPasswordCheckPhoneRequest();
                request.setPinPassCode(etVerificationCode.getText().toString().trim());
                request.setPatientLoginID(ApplicationConstants.forgotPasswordFindDetailsResponse.getEmailID());
                if (Connectivity.isConnected(ForgotPassowordCheckPhoneActivity.this))
                {
                    callService(request);
                }else
                {
                    ShowAlertDialog("Please check internet connection.");
                }

            } else
            {
                ShowAlertDialog("Enter correct verification code.");
            }
        }
    }

    private void callService(ForgotPasswordCheckPhoneRequest request) {

        showLoader("Loading");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).validateForgotPinPasscode(request, new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Passcode update failed. Please enter correct passcode");
            }

            @Override
            public void success(String defaulResponse, Response response) {

                hideLoader();
                Intent slideactivity = new Intent(ForgotPassowordCheckPhoneActivity.this, ResetPasswordActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                slideactivity.putExtra("isResetViaMobile",isResetViaMobile);
                startActivity(slideactivity, bndlanimation);
            }
        });
    }
    public void showLoader(String str) {
        runOnUiThread(new RunShowLoader(str));
    }

    public void hideLoader()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressdialog != null && progressdialog.isShowing()) {
                    progressdialog.dismiss();
                }
            }
        });
    }

    /**
     * Name:         RunShowLoader
     Description:  This is to show the loading progress dialog when some other functionality is taking place.**/
    class RunShowLoader implements Runnable {
        private String strMsg;

        public RunShowLoader(String strMsg) {
            this.strMsg = strMsg;
        }

        @Override
        public void run()
        {
            try {
                if(progressdialog == null ||(progressdialog != null && !progressdialog.isShowing())) {
                    progressdialog = ProgressDialog.show(ForgotPassowordCheckPhoneActivity.this, "", strMsg);
//					progressdialog.setContentView(R.layout.progress_dialog);
                    progressdialog.setCancelable(false);
                } else {
                }
            } catch(Exception e) {
                e.printStackTrace();
                progressdialog = null;
            }
        }
    }

    public static class GRegisterSms extends BroadcastReceiver {

        // Get the object of SmsManager
        final SmsManager sms = SmsManager.getDefault();
        public String tmessage;





        public void onReceive(Context context, Intent intent) {

            // Retrieves a map of extended data from the intent.
            final Bundle bundle = intent.getExtras();

            try {

                if (bundle != null) {

                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    for (int i = 0; i < pdusObj.length; i++) {

                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                        String senderNum = phoneNumber;
                        String message = currentMessage.getDisplayMessageBody();

                        Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);
                        tmessage=message.substring(message.length()-6);


                         alertDialog.dismiss();
                         etVerificationCode.setText(tmessage);

                        // Show Alert
                        int duration = Toast.LENGTH_LONG;
                        //  Toast toast = Toast.makeText(context,
                        //          "senderNum: "+ senderNum + ", message: " + message, duration);
                        //  toast.show();

                    } // end for loop
                } // bundle is null

            } catch (Exception e) {
                Log.e("SmsReceiverhg", "Exception smsReceiver" +e);

            }
        }
    }

    public void ShowAlertDialog(String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ForgotPassowordCheckPhoneActivity.this);
        View dialogView = LayoutInflater.from(ForgotPassowordCheckPhoneActivity.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        tvInfo.setTextSize(18);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.GONE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }
}
