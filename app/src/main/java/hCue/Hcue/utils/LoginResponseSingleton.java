package hCue.Hcue.utils;


import android.content.Context;

import com.google.gson.Gson;

import hCue.Hcue.model.AddUpdatePatientOrderRequest;
import hCue.Hcue.model.LoginResponse;

/**
 * Created by Appdest on 08-06-2016.
 */
public class LoginResponseSingleton
{
    private static LoginResponse loginResponse ;
    private static Context context;

    public static LoginResponse getSingleton(LoginResponse loginResponse, Context context)
    {
        LoginResponseSingleton.context = context ;
        if(loginResponse != null)
        {
            Gson gson = new Gson();
            String loginresponse = gson.toJson(loginResponse);
            Preference preference = new Preference(context);
            preference.saveStringInPreference("LOGINRESPONSE", loginresponse);
            preference.commitPreference();
            LoginResponseSingleton.loginResponse = loginResponse;
        }
        return  loginResponse ;
    }

    public static LoginResponse getSingleton()
    {
        if(loginResponse == null && context != null)
        {
            Gson gson = new Gson();
            Preference preference = new Preference(context);
            String value= preference.getStringFromPreference("LOGINRESPONSE", "");
            if(value.isEmpty())
            {
                return null ;
            }
            loginResponse = gson.fromJson(value, LoginResponse.class);
        }
        return  loginResponse ;
    }

    public Object clone()
            throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }

    public static void setContext(Context context) {
        LoginResponseSingleton.context = context;
    }
}
