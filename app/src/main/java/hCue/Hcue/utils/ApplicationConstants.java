package hCue.Hcue.utils;


import android.content.Context;
import android.graphics.Typeface;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import hCue.Hcue.DAO.ContactDetailsTempDO;
import hCue.Hcue.DAO.DoctorSpecialityDO;
import hCue.Hcue.DAO.PharmaDetails;
import hCue.Hcue.DAO.SpecialityDAO;
import hCue.Hcue.adapters.RemindersDbAdapter;
import hCue.Hcue.model.AddUpdatePatientOrderRequest;
import hCue.Hcue.model.CityLookupResponse;
import hCue.Hcue.model.CountryLookupResponse;
import hCue.Hcue.model.ForgotPasswordFindDetailsResponse;
import hCue.Hcue.model.ListOrderAddresResponce;
import hCue.Hcue.model.LocationLookupResponse;
import hCue.Hcue.model.OrderAddressRequest;
import hCue.Hcue.model.StateLookupResponse;


/**
 * Created by cvluser on 27-07-2015.
 */
public class ApplicationConstants {
    //Rest client configuration constants
    public static final int CONNECTION_TIME_OUT = 15000;
    public static final int READ_TIME_OUT = 15000;
    public static final int WRITE_TIME_OUT = 15000;

    //App constants
    public static final int TAKE_PHOTO_REQUEST = 1001;
    public static final int PICK_PHOTO_REQUEST = 1002;
    public static final int TAKE_PHOTO_RESULT = 1003;
    public static final int PICK_PHOTO_RESULT = 1004;
    //Dialog fragments constants
    public static final int BUTTON_OK = 1;
    public static final int BUTTON_TRY_AGAIN = 2;
    public static final int BUTTON_PROCEED = 3;
    public static final int BUTTON_CANCEL = 4;
    public static final int OPERATION_FAILED = 5;
    public static int SELECTED_CLINIC_POSITION = 0;
    public static int SELECTED_ADDRESSCONSULTID = 0;
    public static Typeface WALSHEIM_BOLD;
    public static Typeface WALSHEIM_LIGHT;
    public static Typeface WALSHEIM_MEDIUM;
    public static Typeface WALSHEIM_SEMI_BOLD;

    public static float 	DEVICE_DENSITY        = 0;
    public static int 		DISPLAY_WIDTH 		= 0;
    public static int 		DISPLAY_HEIGHT 		= 0;
    public static final int IMG_MENU_TYPE 	= 5;

    public static String 	currentLat 			  = "17.4387171";
    public static String 	currentLng 			  = "78.3957388";

    public static boolean isHomepickchoosen = false ;
    public static boolean isFromLab = false ;

    public static String PASS_CODE;

    public static ForgotPasswordFindDetailsResponse forgotPasswordFindDetailsResponse ;


    public static final int API_KEY = 45591512;
    public static final String API_SECRET = "94f8eba64e7148cbe4fd8b03c140f5f57d988635";
    public static final String SESSION_ID = "2_MX40NTU5MTUxMn5-MTQ2MzY0MzcyNzY4OX55VnlvUWxUMndVTGNjMHdwWXBDOXArRUp-fg";
    public static final String TOKEN = "T1==cGFydG5lcl9pZD00NTU5MTUxMiZzaWc9MjI1ZWY2OTY0ZGFkNDA3ODg4Zjk5MmUyODczZDIwYmNmNTQ2YWUwNzpzZXNzaW9uX2lkPTJfTVg0ME5UVTVNVFV4TW41LU1UUTJNelkwTXpjeU56WTRPWDU1Vm5sdlVXeFVNbmRWVEdOak1IZHdXWEJET1hBclJVcC1mZyZjcmVhdGVfdGltZT0xNDYzNjQzODM1Jm5vbmNlPTAuNTAxODExMDgyOTQwNTQ4NyZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDY2MjM1ODI4";

    public  static RemindersDbAdapter dbAdapter;
    public static RemindersDbAdapter getDbAdapter(Context context) {
            if (dbAdapter == null){
                dbAdapter = new RemindersDbAdapter(context);
                dbAdapter.open();
            }
        return dbAdapter;
    }


    public static HashMap<String, ContactDetailsTempDO> mapDeliveryAddr = new HashMap<>();
    public static ArrayList<String> listPresciptions = new ArrayList<>();
    public static AddUpdatePatientOrderRequest request = new AddUpdatePatientOrderRequest();
    public static AddUpdatePatientOrderRequest getOrderRequest() {
        return request;
    }

    public static final ArrayList<DoctorSpecialityDO> listSpecialityDO = new ArrayList<>();
    public static boolean isFromLocationSearch = false;
    public static boolean isFromNewOrder = false;

    public static PharmaDetails selPharmaDetails = new PharmaDetails();
    public static String pharmaID = "0";

    public static ArrayList<LocationLookupResponse> listLocationDO = new ArrayList<>();
    public static ArrayList<CityLookupResponse> listCityDO = new ArrayList<>();
    public static ArrayList<StateLookupResponse> listStatesDO = new ArrayList<>();
    public static ArrayList<CountryLookupResponse> listCountryDO = new ArrayList<>();
    public static boolean isFromCamera = false;
    public static File imageFile = null;
    public static String fileExtension;


}
