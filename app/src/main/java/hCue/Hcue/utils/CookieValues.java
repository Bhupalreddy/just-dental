package hCue.Hcue.utils;

/**
 * Created by Appdest on 30-05-2016.
 */
public class CookieValues {

    private String patientCloudFront_Signature ;
    private String patientCloudFront_Policy ;
    private String patientCloudFront_Key_Pair_Id ;

    private String doctorCloudFront_Signature ;
    private String doctorCloudFront_Policy ;
    private String doctorCloudFront_Key_Pair_Id ;

    private String platformCloudFront_Signature ;
    private String platformCloudFront_Policy ;
    private String platformCloudFront_Key_Pair_Id ;

    public String getPatientCloudFront_Signature() {
        return patientCloudFront_Signature;
    }

    public void setPatientCloudFront_Signature(String patientCloudFront_Signature) {
        this.patientCloudFront_Signature = patientCloudFront_Signature;
    }

    public String getPatientCloudFront_Policy() {
        return patientCloudFront_Policy;
    }

    public void setPatientCloudFront_Policy(String patientCloudFront_Policy) {
        this.patientCloudFront_Policy = patientCloudFront_Policy;
    }

    public String getPatientCloudFront_Key_Pair_Id() {
        return patientCloudFront_Key_Pair_Id;
    }

    public void setPatientCloudFront_Key_Pair_Id(String patientCloudFront_Key_Pair_Id) {
        this.patientCloudFront_Key_Pair_Id = patientCloudFront_Key_Pair_Id;
    }

    public String getDoctorCloudFront_Signature() {
        return doctorCloudFront_Signature;
    }

    public void setDoctorCloudFront_Signature(String doctorCloudFront_Signature) {
        this.doctorCloudFront_Signature = doctorCloudFront_Signature;
    }

    public String getDoctorCloudFront_Policy() {
        return doctorCloudFront_Policy;
    }

    public void setDoctorCloudFront_Policy(String doctorCloudFront_Policy) {
        this.doctorCloudFront_Policy = doctorCloudFront_Policy;
    }

    public String getDoctorCloudFront_Key_Pair_Id() {
        return doctorCloudFront_Key_Pair_Id;
    }

    public void setDoctorCloudFront_Key_Pair_Id(String doctorCloudFront_Key_Pair_Id) {
        this.doctorCloudFront_Key_Pair_Id = doctorCloudFront_Key_Pair_Id;
    }

    public String getPlatformCloudFront_Signature() {
        return platformCloudFront_Signature;
    }

    public void setPlatformCloudFront_Signature(String platformCloudFront_Signature) {
        this.platformCloudFront_Signature = platformCloudFront_Signature;
    }

    public String getPlatformCloudFront_Policy() {
        return platformCloudFront_Policy;
    }

    public void setPlatformCloudFront_Policy(String platformCloudFront_Policy) {
        this.platformCloudFront_Policy = platformCloudFront_Policy;
    }

    public String getPlatformCloudFront_Key_Pair_Id() {
        return platformCloudFront_Key_Pair_Id;
    }

    public void setPlatformCloudFront_Key_Pair_Id(String platformCloudFront_Key_Pair_Id) {
        this.platformCloudFront_Key_Pair_Id = platformCloudFront_Key_Pair_Id;
    }
}
