package hCue.Hcue.utils;

import java.io.Serializable;

import hCue.Hcue.DAO.SelectedConsultation;


/**
 * Created by Appdest on 08-06-2016.
 */
public class SelectedConsSingleton implements Serializable
{
    private static SelectedConsultation selectedConsultation ;

    private SelectedConsSingleton ()
    {

    }

    public static SelectedConsultation getSingleton()
    {
        if(selectedConsultation == null)
        {
            selectedConsultation = new SelectedConsultation();
        }
        return  selectedConsultation ;
    }

    public static void resetInstance()
    {
        selectedConsultation = null ;
    }

    public Object clone()
            throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }
}
