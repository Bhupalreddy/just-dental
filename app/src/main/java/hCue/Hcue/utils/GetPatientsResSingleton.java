package hCue.Hcue.utils;

import android.content.Context;

import com.google.gson.Gson;

import java.io.Serializable;

import hCue.Hcue.model.GetPatientsResponse;

/**
 * Created by Appdest on 29-07-2016.
 */
public class GetPatientsResSingleton implements Serializable
{
    private static GetPatientsResponse getPatientsResponse ;
    private static Context context;

    public static GetPatientsResponse getSingleton(GetPatientsResponse getPatientsResponse, Context context)
    {
        GetPatientsResSingleton.context = context ;
        if(getPatientsResponse != null)
        {
            Gson gson = new Gson();
            String loginresponse = gson.toJson(getPatientsResponse);
            Preference preference = new Preference(context);
            preference.saveStringInPreference(Preference.GETPATIENTSRESPONSE, loginresponse);
            preference.commitPreference();
            GetPatientsResSingleton.getPatientsResponse = getPatientsResponse;
        }
        return  getPatientsResponse ;
    }

    public static GetPatientsResponse getSingleton()
    {
        if(getPatientsResponse == null && context != null)
        {
            Gson gson = new Gson();
            Preference preference = new Preference(context);
            String value= preference.getStringFromPreference(Preference.GETPATIENTSRESPONSE, "");
            if(value.isEmpty())
            {
                return null ;
            }
            getPatientsResponse = gson.fromJson(value, GetPatientsResponse.class);
        }
        return  getPatientsResponse ;
    }

    public Object clone()
            throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }

    public static void setContext(Context context) {
        GetPatientsResSingleton.context = context;
    }
}
