package hCue.Hcue.utils;

/**
 * Created by prasanna on 10/4/16.
 */

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class GPSTracker extends Service implements LocationListener {
    private static Timer timer = new Timer();
    private  Context mContext;
    private  Context mContext1;
    SharedPreferences pref;
    String empname = "";
    String empid = "";
    String clientid = "";
    // flag for GPS status
    boolean isGPSEnabled = false;
    public int test=0;
    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    public static double latitude; // latitude
    public static double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;


    public GPSTracker(){




    }


    public GPSTracker(Context context) {
        this.mContext = context;
        getLocation();
        startService();
    }
    public void onCreate()
    {
        super.onCreate();
        mContext = this;
        pref =mContext.getSharedPreferences(PatientUtils.patient_preference_file, Context.MODE_PRIVATE);

        startService();
    }
    private void startService()
    {
        timer.scheduleAtFixedRate(new mainTask(), 0, 600000);
        timer.scheduleAtFixedRate(new acttask(), 0, 600000);
    }
    private class mainTask extends TimerTask
    {
        public void run()
        {
            toastHandler.sendEmptyMessage(0);
            Log.e("ee", "SERVICE");
        }
    }

    private class acttask extends TimerTask
    {
        public void run()
        {
            actHandler.sendEmptyMessage(0);
            Log.e("ee", "SERVICEqqq");
        }
    }
    private final Handler toastHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            //  ParamLocationTrack param = new ParamLocationTrack();
            try {

//
//                        param.setemployeeid(empid);
//                        param.setLatitude(String.valueOf(getLocation().getLatitude()));
//                        param.setLongitude(String.valueOf(getLocation().getLongitude()));
//                        param.setClientid(clientid);

                //     String endpoint = ProdyConst.URL_API_BASE + ProdyConst.ENDPOINT_LOCATION_TRACK;
                //    Log.e("ee", endpoint + "?" + param.toString());
                //    HttpRequest request = new HttpPostRequest(endpoint, param);

                //  LocationTrack t=new LocationTrack();
                //  String data1="";
                //  data1=t.test1(endpoint,clientid,empid,String.valueOf(getLocation().getLatitude()),String.valueOf(getLocation().getLongitude()));


            } catch (IllegalStateException e) {

            } catch (NullPointerException e) {

            }
            //Toast.makeText(getApplicationContext(),empname+"--"+clientid+"---"+getLocation().getLatitude()+"--"+getLocation().getLongitude(), Toast.LENGTH_SHORT).show();
        }
    };


    private final Handler actHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {

            try {
                latitude=getLocation().getLatitude();
                longitude=getLocation().getLongitude();

                //    Toast.makeText(getApplicationContext(),"asd"+getLocation().getLongitude(), Toast.LENGTH_SHORT).show();

            } catch (IllegalStateException e) {

            } catch (NullPointerException e) {

            }

        }
    };


    public Location getLocation() {
        try {
            Log.e("PRRRRRR" ,"asdadsasdasd" + Build.VERSION.SDK_INT +"--"+Build.VERSION_CODES.M);
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);
            if (locationManager != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    isGPSEnabled = locationManager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);


                    // getting network status
                    isNetworkEnabled = locationManager
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (!isGPSEnabled && !isNetworkEnabled) {
                        // no network provider is enabled
                        Log.e("PPP","INSIDE" +isNetworkEnabled);
                        Intent intent1 = new Intent("android.location.GPS_ENABLED_CHANGE");
                        intent1.putExtra("enabled", true);
                        //    this.sendBroadcast(intent1);

                        boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        if (!gpsStatus) {
                            Settings.Secure.putString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED, "network,gps");
                        }
                    } else {
                        this.canGetLocation = true;
                        // First get location from Network Provider
                        if (isNetworkEnabled) {
                            locationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            Log.d("Network", "Network");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                        // if GPS Enabled get lat/long using GPS Services
                        if (isGPSEnabled) {
                            if (location == null) {
                                locationManager.requestLocationUpdates(
                                        LocationManager.GPS_PROVIDER,
                                        MIN_TIME_BW_UPDATES,
                                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                                Log.d("GPS Enabled", "GPS Enabled");
                                if (locationManager != null) {
                                    location = locationManager
                                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    if (location != null) {
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                    }
                                }
                            }
                        }
                    }
                }else{

                    //   locationManager = (LocationManager) mContext
                    //   .getSystemService(LOCATION_SERVICE);

                    // getting GPS status
                    isGPSEnabled = locationManager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);


                    // getting network status
                    isNetworkEnabled = locationManager
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (!isGPSEnabled && !isNetworkEnabled) {
                        // no network provider is enabled
                        Log.e("PPP","INSIDE" +isNetworkEnabled);
                        Intent intent1 = new Intent("android.location.GPS_ENABLED_CHANGE");
                        intent1.putExtra("enabled", true);
                        //    this.sendBroadcast(intent1);

                        boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        if (!gpsStatus) {
                            Settings.Secure.putString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED, "network,gps");
                        }
                    } else {
                        this.canGetLocation = true;
                        // First get location from Network Provider
                        if (isNetworkEnabled) {
                            locationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            Log.d("Network", "Network");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                        // if GPS Enabled get lat/long using GPS Services
                        if (isGPSEnabled) {
                            if (location == null) {
                                locationManager.requestLocationUpdates(
                                        LocationManager.GPS_PROVIDER,
                                        MIN_TIME_BW_UPDATES,
                                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                                Log.d("GPS Enabled", "GPS Enabled");
                                if (locationManager != null) {
                                    location = locationManager
                                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    if (location != null) {
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                Log.e("PRRP","OUTSIDE");
            }

        } catch (Exception e) {
            Log.e("ERROR",e.toString());
        }

        return location;
    }




    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            //  locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     * */
    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}