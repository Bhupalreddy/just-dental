package hCue.Hcue.utils;

import android.app.ActivityOptions;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import hCue.Hcue.MyApplication;
import hCue.Hcue.model.LoginRequest;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;

public class CookieService extends Service {
    private final IBinder mBinder = new LocalBinder();
    private Timer timer;
    private TimerTask timerTask ;
    private final long timelimit = (8*(60*1000));
    public CookieService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return mBinder ;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Do what you need in onStartCommand when service has been started
        return START_NOT_STICKY;
    }


    public void getCookies(int i)
    {
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("ONE MINUTE UP","STARTED CALLING SERVICE");
                Preference preference = new Preference(MyApplication.getIntance());
                String username = preference.getStringFromPreference("userName", "");
                String password = preference.getStringFromPreference("pwd", "");
                preference.commitPreference();
                LoginRequest loginRequest = new LoginRequest();

                boolean isFromGoogle = preference.getbooleanFromPreference("IsFromGoogle",false);
                loginRequest.setPatientLoginID(username);
                if(isFromGoogle)
                {
                    loginRequest.setSocialIDType("GOOGLE");
                    loginRequest.setSocialID(password);
                }
                else
                    loginRequest.setPatientPassword(password);

                RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).validateLogin(loginRequest, new RestCallback<LoginResponse>()
                {
                    @Override
                    public void failure(RestError restError)
                    {
                        Log.e("Error", restError.getErrorMessage());
                    }

                    @Override
                    public void success(LoginResponse loginResponse, Response response)
                    {
                        if (loginResponse != null)
                        {
                            MyApplication myApplication = MyApplication.getIntance();
                            myApplication.setIdentity(loginResponse.getIdentity());
                        }
                    }
                });
            }
        } ;

        timer = new Timer();
        timer.scheduleAtFixedRate(timerTask,i,timelimit);
    }


    public class LocalBinder extends Binder {
        public CookieService getServiceInstance(){
            return CookieService.this;
        }
    }
}
