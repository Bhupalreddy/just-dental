package hCue.Hcue.utils;

/**
 * Created by User on 3/22/2016.
 */
import java.util.Calendar;
import java.util.Date;

/**
 * Helper for date handle
 *
 * @author Jerome RADUGET
 */
public abstract class DateHelper {

    /**
     * @param birthdate
     * @return Age from the current date
     */
    public static int getAge(final Date birthdate) {
        return getAge(Calendar.getInstance().getTime(), birthdate);
    }

    /**
     * Calculating age from a current date
     *
     * @param current
     * @param birthdate
     * @return Age from the current (arg) date
     */
    public static int getAge(final Date current, final Date birthdate) {

        if (birthdate == null) {
            return 0;
        }
        if (current == null) {
            return getAge(birthdate);
        } else {
            int years = 0;
            int months = 0;
            int days = 0;
            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthdate.getTime());
            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;
            //Get difference between months
            months = currMonth - birthMonth;
            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0)
            {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
            {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
            {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else
            {
                days = 0;
                if (months == 12)
                {
                    years++;
                    months = 0;
                }
            }
            return years;
        }

    }

    public static int[] getAgeAndMonth(final Date birthdate) {
        return getAgeAndMonth(Calendar.getInstance().getTime(), birthdate);
    }

    /**
     * Calculating age from a current date
     *
     * @param current
     * @param birthdate
     * @return Age from the current (arg) date
     */
    public static int[] getAgeAndMonth(final Date current, final Date birthdate) {

        int[] ageAndMonth = new int[3];

        if (birthdate == null) {
            return ageAndMonth;
        }
        if (current == null) {
            return getAgeAndMonth(birthdate);
        } else {
            int years = 0;
            int months = 0;
            int days = 0;
            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthdate.getTime());
            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;
            //Get difference between months
            months = currMonth - birthMonth;
            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0)
            {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
            {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
            {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else
            {
                days = 0;
                if (months == 12)
                {
                    years++;
                    months = 0;
                }
            }
            ageAndMonth[0] = years;
            ageAndMonth[1] = months;
            ageAndMonth[2] = days;
            return ageAndMonth;
        }

    }
}
