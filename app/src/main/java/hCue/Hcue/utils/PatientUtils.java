package hCue.Hcue.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by prasanna on 28/3/16.
 */
public class PatientUtils {
    public static final String patient_preference_file = "com.hcue.patient.preference";
    public static final String GCM_REG_ID = "gcmRegId";
    public static final String APP_VERSION = "appVersion";
    public static final String PUSH_MESSAGE = "pushMessage";
    public static final String REG_ID = "regId";
    public static final String GCM_REGID_SHARED = "gcmRegIdShared";
    public static final String FRAGMENT_INDEX = "fragmentIndex";
    public static final String PAY_ONLINE_JSON = "payOnlineJSON";

    public static final int NOTIFICATION_FRAGMENT_INDEX = 8;
    private static final SimpleDateFormat inputDateFormat = new SimpleDateFormat(
            "dd/mm/yyyy");
    private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat(
            "yyyy-mm-dd");

    private static final SimpleDateFormat outputDateFormat1 = new SimpleDateFormat(
            "dd-mm-yyyy");

    private static final SimpleDateFormat inputDateFormat1 = new SimpleDateFormat(
            "yyyy-mm-dd");
    private static final SimpleDateFormat inputDateFormat2 = new SimpleDateFormat(
            "dd-mm-yyyy");

    public static boolean isOnline(Context context) {// requires network state
        // access permisstion
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static String convertToYYYY_MM_DD(String dd_mm_yyyy) {
        String output = null;
        try {
            output = outputDateFormat.format(inputDateFormat.parse(dd_mm_yyyy));
        } catch (ParseException e) {
            System.out.println(e);
        }
        return output;
    }

    public static String convertToYYYY_MM_DD1(String dd_mm_yyyy) {
        String output = null;
        try {
            output = outputDateFormat1.format(inputDateFormat1.parse(dd_mm_yyyy));
        } catch (ParseException e) {
            System.out.println(e);
        }
        return output;
    }

    public static String convertToYYYY_MM_DD2(String dd_mm_yyyy) {
        String output = null;
        try {
            output = outputDateFormat.format(inputDateFormat2.parse(dd_mm_yyyy));
        } catch (ParseException e) {
            System.out.println(e);
        }
        return output;
    }

public static Hashtable<String,String> Specialitycode(){
  Hashtable<String,String> specialitycode=new Hashtable<String, String>();
    specialitycode.put("ACP","Acupuncture");
    specialitycode.put("ANE","Anesthesiologist");
    specialitycode.put("AYU","Ayurveda");
    specialitycode.put("CAI","Cardiologist - Interventional");
    specialitycode.put("CTS","Cardio Thoracic Surgeon");
    specialitycode.put("CRD","Consultant Cardiologist");
    specialitycode.put("COS","Cosmetic Surgeon");
    specialitycode.put("DNT","Dentist");
    specialitycode.put("DER","Dermatologist");
    specialitycode.put("DBT","Diabetologist");
    specialitycode.put("ENT","Ear Nose Throat Specialist");
    specialitycode.put("END","Endocrinologist");
    specialitycode.put("GAS","Gastroenterologist");
    specialitycode.put("GLS","General & Laparoscopic Surgeon");
    specialitycode.put("GEP","General Practitioner");
    specialitycode.put("GNS","General Surgeon");
    specialitycode.put("GTR","Geriatrician");
    specialitycode.put("GYN","Gynaecologist");
    specialitycode.put("HEM","Hematologist");
    specialitycode.put("HEP","Hepatologist");
    specialitycode.put("HYM","Homeopathy");
    specialitycode.put("IML","Immunologist");
    specialitycode.put("IFD","Infectious Diseases");
    specialitycode.put("IMD","Internal Medicine");
    specialitycode.put("NEG","Neonatologist");
    specialitycode.put("NEP","Nephrologist");
    specialitycode.put("NEU","Neurologist");
    specialitycode.put("NES","Neuro Surgeon");
    specialitycode.put("OBS","Obstetrician");
    specialitycode.put("OCT","Occupational Therapist");
    specialitycode.put("ONC","Oncologist");
    specialitycode.put("OPT","Ophthalmologist");
    specialitycode.put("ORT","Orthopedician");
    specialitycode.put("OTH","Others");
    specialitycode.put("OTO","Otolaryngologist");
    specialitycode.put("PAE","Paediatrician");
    specialitycode.put("PLC","Palliative Care");
    specialitycode.put("PAT","Pathologist");
    specialitycode.put("PES","Pediatric Surgeon");
    specialitycode.put("PHY","Physiologist");
    specialitycode.put("PST","Physiotherapist");
    specialitycode.put("PLS","Plastic Surgeon");
    specialitycode.put("PDT","Podiatrist");
    specialitycode.put("PCT","Psychiatrist");
    specialitycode.put("PSY","Psychologist");
    specialitycode.put("PML","Pulmonologist");
    specialitycode.put("RAD","Radiologist");
    specialitycode.put("RHT","Rehabilitation Therapist");
    specialitycode.put("RHE","Rheumatologist");
    specialitycode.put("SHT","Sexual Health");
    specialitycode.put("SID","Siddha");
    specialitycode.put("SLT","Speech And Language Therapist");
    specialitycode.put("SMD","Sports Medicine");
    specialitycode.put("UNI","Unani");
    specialitycode.put("URO","Urologist");





    return specialitycode;
}



    public static ArrayList<HashMap<String, String>> getsearchdoctorList(JSONArray ja) {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, ArrayList> list1 = new  HashMap<String, ArrayList>();
        Hashtable<String,String> specialitycode=new Hashtable<String, String>();
        JSONObject jObject = null;


        try {
            Map<Integer, Hashtable<String,String>> leadlist = new HashMap<Integer, Hashtable<String,String>>();
            for (int i = 0, size = ja.length(); i < size; i++) {
                jObject = new JSONObject(ja.getJSONObject(i).toString());
                Iterator keys = jObject.keys();

                Hashtable<String,String> val=new Hashtable<String,String>();
                int j=0;

                while (keys.hasNext()) {

                   // int key = Integer.parseInt((String) keys.next());
                   // JSONArray jArray = new JSONArray(jObject.getString(String.valueOf(key)));
                    String key=String.valueOf((String) keys.next());
                    String value="";
                   value =jObject.getString(key);

                    val.put(key, value);
                    j++;


                }
                leadlist.put(i+1, val);
            }
             Map<Integer, Hashtable<String,String>> sortedOrders = new TreeMap<Integer, Hashtable<String,String>>();

            sortedOrders.putAll(leadlist);

            Set<Map.Entry<Integer, Hashtable<String,String>>> entrySet = sortedOrders.entrySet();
            for (Map.Entry<Integer, Hashtable<String,String>> entry : entrySet) {


                HashMap<String, String> map = new HashMap<String, String>();
                Hashtable<String,String> val=new Hashtable<String,String>();
               // String val1[] = entry.getValue();
                val=entry.getValue();
                double d = 0;

                NumberFormat nf = NumberFormat.getInstance(Locale.US);
                nf.setMinimumIntegerDigits(1);
                nf.setMaximumFractionDigits(2);


                d=Double.parseDouble(val.get("Distance"));
              //  DecimalFormat f = new DecimalFormat("##.00");
             //   System.out.println();


                        String image="";
                if(val.containsKey("ProfileImage")) {

                            if(val.get("ProfileImage").toString().trim().length()>0) {
                                image = Uri.parse(val.get("ProfileImage").toString()).toString();
                            }else{
                                if(val.get("Gender").equals("F")){
                                    image=Uri.parse("@drawable/profileimg_female").toString();
                                }else{
                                    image=Uri.parse("@drawable/profileimg_male").toString();
                                }

                                //   image=Uri.parse(val[13]).toString();
                            }

                        }else{
                    if(val.get("Gender").equals("F")){
                        image=Uri.parse("@drawable/profileimg_female").toString();
                    }else{
                        image=Uri.parse("@drawable/profileimg_male").toString();
                    }

                         //   image=Uri.parse(val[13]).toString();
                        }
                String specialist = "";
                if(val.containsKey("SpecialityID")) {
                    JSONObject spec = new JSONObject(val.get("SpecialityID"));
                    Iterator keys = spec.keys();


                    while (keys.hasNext()) {
                        String key = String.valueOf((String) keys.next());
                        specialitycode = Specialitycode();
                        if (specialist.length() > 0) {

                            specialist = specialist + "," + specialitycode.get(spec.getString(key));
                        } else {
                            specialist = specialitycode.get(spec.getString(key));
                        }
                    }
                }
                String qualification = " ";

if(val.containsKey("Qualification")) {
    if (val.get("Qualification") != "null") {
        JSONObject qual = new JSONObject(val.get("Qualification"));
        Iterator quals = qual.keys();


        while (quals.hasNext()) {
            String key = String.valueOf((String) quals.next());

            if (qualification.length() > 1) {

                qualification = qualification + "," + (qual.getString(key));
            } else {
                if (qual.getString(key).equals("")) {
                    qualification = " ";
                } else {
                    qualification = (qual.getString(key));
                }
            }
        }
    } else {
        qualification = " ";
    }
}

                map.put("searchid", val.get("DoctorID"));
                map.put("ClinicName", val.get("ClinicName"));
                map.put("FullName", val.get("FullName"));
                map.put("Exp", val.get("Exp"));
                map.put("Fees", val.get("Fees"));
                map.put("Distance", nf.format(d));
                map.put("Phone", val.get("PhoneNumber"));
                map.put("ProfileImage",image);
                map.put("Specialist",specialist);
                map.put("callnow",val.get("PhoneNumber"));
                map.put("Gender",val.get("Gender"));
                map.put("AddressID",val.get("AddressID"));
                map.put("Qualification",qualification);
                map.put("Prospect",val.get("Prospect"));
                String email="";
                if(val.containsKey("EmailID")) {
                    if (val.get("EmailID").equals("")) {
                        email = "Not Available";
                    } else {
                        email = val.get("EmailID");
                    }
                }
                map.put("REMAIN",val.get("Latitude")+"::"+val.get("Longitude")+"::"+val.get("Address")+"::"+email);

                list.add(map);

            }

			/*
			 * while (keys.hasNext()) { String key = (String) keys.next();
			 * JSONArray jArray = new JSONArray(jObject.getString(key));
			 * HashMap<String, String> map = new HashMap<String, String>();
			 * map.put("orderid", key); map.put("amount", jArray.getString(0));
			 * map.put("quantity", jArray.getString(1)); map.put("status",
			 * jArray.getString(2)); map.put("pickupdate", jArray.getString(3));
			 * map.put("deliverydate", jArray.getString(4)); map.put("item",
			 * jArray.getString(5)); list.add(map); }
			 */
        } catch (JSONException e) {
            Log.e("ee", "Error parsing data " + e);
        } catch (NullPointerException e) {
            Log.e("ee", "Result String is empty :" + e);
        }
        return list;
    }


    public static HashMap<String, String> getdoctorprofile(JSONArray ja) {

        HashMap<String, String> list1 = new  HashMap<String, String>();
        Hashtable<String,String> specialitycode=new Hashtable<String, String>();
        JSONObject jObject = null;


        try {

            for (int i = 0, size = ja.length(); i < size; i++) {
                jObject = new JSONObject(ja.getJSONObject(i).toString());
                Iterator keys = jObject.keys();


                while (keys.hasNext()) {

                    // int key = Integer.parseInt((String) keys.next());
                    // JSONArray jArray = new JSONArray(jObject.getString(String.valueOf(key)));
                    String key=String.valueOf((String) keys.next());
                    String value="";
                    value =jObject.getString(key);


                    list1.put(key, value);

                }

            }

        } catch (JSONException e) {
            Log.e("ee", "Error parsing data " + e);
        } catch (NullPointerException e) {
            Log.e("ee", "Result String is empty :" + e);
        }
        return list1;
    }


    public static Hashtable<String, HashMap> getdoctoraddress(JSONArray ja) {


        Hashtable<String, HashMap> list2 = new  Hashtable<String, HashMap>();

        JSONObject jObject = null;


        try {

            for (int i = 0, size = ja.length(); i < size; i++) {
                jObject = new JSONObject(ja.getJSONObject(i).toString());
                Iterator keys = jObject.keys();
String addressid="";
                HashMap<String, String> list1 = new  HashMap<String, String>();
                while (keys.hasNext()) {

                    String key=String.valueOf((String) keys.next());
                    String value="";
                    value =jObject.getString(key);

if(key.equals("AddressID")){
    addressid=value;
}
                    list1.put(key, value);

                }

                list2.put(addressid,list1);

            }

        } catch (JSONException e) {
            Log.e("ee", "Error parsing data " + e);
        } catch (NullPointerException e) {
            Log.e("ee", "Result String is empty :" + e);
        }
        return list2;
    }


    public static Hashtable<String, HashMap> getdoctorconstulation(JSONArray ja) {


        Hashtable<String, HashMap> list2 = new  Hashtable<String, HashMap>();

        JSONObject jObject = null;


        try {

            for (int i = 0, size = ja.length(); i < size; i++) {
                jObject = new JSONObject(ja.getJSONObject(i).toString());
                Iterator keys = jObject.keys();
                String addressconsultid="";
                HashMap<String, String> list1 = new  HashMap<String, String>();
                while (keys.hasNext()) {

                    String key=String.valueOf((String) keys.next());
                    String value="";
                    value =jObject.getString(key);

                    if(key.equals("AddressConsultID")){
                        addressconsultid=value;
                    }
                    list1.put(key, value);

                }

                list2.put(addressconsultid,list1);

            }

        } catch (JSONException e) {
            Log.e("ee", "Error parsing data " + e);
        } catch (NullPointerException e) {
            Log.e("ee", "Result String is empty :" + e);
        }
        return list2;
    }


    public static ArrayList<HashMap<String, String>> getupcomingappoint(JSONArray ja) {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, ArrayList> list1 = new  HashMap<String, ArrayList>();
        Hashtable<String,String> specialitycode=new Hashtable<String, String>();
        JSONObject jObject = null;


        try {
            Map<Integer, Hashtable<String,String>> leadlist = new HashMap<Integer, Hashtable<String,String>>();
            for (int i = 0, size = ja.length(); i < size; i++) {
                jObject = new JSONObject(ja.getJSONObject(i).toString());
                Iterator keys = jObject.keys();
             //   String[] val=new String[2];
                Hashtable<String,String> val=new Hashtable<String,String>();
                int j=0;

                while (keys.hasNext()) {

                    // int key = Integer.parseInt((String) keys.next());
                    // JSONArray jArray = new JSONArray(jObject.getString(String.valueOf(key)));
                    String key=String.valueOf((String) keys.next());
                    String value="";
                    value =jObject.getString(key);

                    val.put(key, value);
                    //val[j]=value;
                    j++;


                }
                leadlist.put(i+1, val);
            }
          Map<Integer,Hashtable<String,String>> sortedOrders = new TreeMap<Integer, Hashtable<String,String>>();

            sortedOrders.putAll(leadlist);

            Set<Map.Entry<Integer, Hashtable<String,String>>> entrySet = sortedOrders.entrySet();
            for (Map.Entry<Integer, Hashtable<String,String>> entry : entrySet) {
                String lead=String.valueOf(entry.getKey());

                HashMap<String, String> map = new HashMap<String, String>();
                Hashtable<String,String> val=new Hashtable<String,String>();
                 val = entry.getValue();



                JSONObject jObject1 = new JSONObject(val.get("AppointmentDetails"));
                    map.put("ClinicName", jObject1.getString("ClinicName"));
                    map.put("DayCD", jObject1.getString("DayCD"));
                    map.put("ConsultationDt", jObject1.getString("ConsultationDt"));
                    map.put("StartTime", jObject1.getString("StartTime"));
                    map.put("PatientID", jObject1.getString("PatientID"));
                    map.put("AppointmentID", jObject1.getString("AppointmentID"));
                map.put("AppointmentStatus",jObject1.getString("AppointmentStatus"));
                map.put("searchid", jObject1.getString("DoctorID"));

                JSONObject jObject2 = new JSONObject(val.get("DoctorDetails"));


                map.put("doctorFullName", jObject2.getString("doctorFullName"));
                map.put("ProfileImage", "");
                if(jObject2.has("ProfileImage")){
                    map.put("ProfileImage", jObject2.getString("ProfileImage"));
                }else{
                    map.put("ProfileImage", "@drawable/profileimg_male");

                }
                map.put("gender", jObject2.getString("gender"));


                JSONArray dphone=new JSONArray(jObject2.getString("doctorPhone"));
                JSONArray dadd=new JSONArray(jObject2.getString("doctorAddress"));
                JSONArray email=new JSONArray(jObject2.getString("doctorEmail"));
                String EmailID="";

                for (int i = 0, size = email.length(); i < size; i++) {
                    JSONObject emailobj = new JSONObject(email.getJSONObject(i).toString());


                    if(emailobj.has("EmailID")){
                        EmailID=emailobj.getString("EmailID");
                    }else{
                        EmailID="Not Available";
                    }
                }



                for (int i = 0, size = dphone.length(); i < size; i++) {
                    JSONObject phobj = new JSONObject(dphone.getJSONObject(i).toString());
                    map.put("doctorphone", phobj.getString("PhNumber"));
                }

                if(dphone.length()==0){
                    map.put("doctorphone","");
                }

                for (int i = 0, size = dadd.length(); i < size; i++) {
                    JSONObject phobj = new JSONObject(dadd.getJSONObject(i).toString());

                    String address="";
                    if( phobj.has("Address2")) {
                        address=phobj.getString("Address2");
                    }else{
                        address="";
                    }

                    map.put("doctoraddress", address);
                    map.put("AddressID", phobj.getString("AddressID"));
                    JSONObject phobj1 = new JSONObject(phobj.getString("ExtDetails"));
                    map.put("doclat", phobj1.getString("Latitude"));
                    map.put("doclan", phobj1.getString("Longitude"));




                    map.put("REMAIN", phobj1.getString("Latitude") + "::" + phobj1.getString("Longitude") + "::" + address + "::" + EmailID);
                }





                JSONObject spec = new JSONObject(jObject2.getString("doctorSpecialization"));
                Iterator keys = spec.keys();
                String specialist="";

                while (keys.hasNext()) {
                    String key =String.valueOf((String) keys.next());
                    specialitycode=Specialitycode();
                    if(specialist.length()>0) {

                        specialist =    specialist +","+specialitycode.get(spec.getString(key));
                    }else{
                        specialist =specialitycode.get(spec.getString(key));
                    }
                }
                map.put("specialist",specialist);


                String qualification = "";

               if(jObject2.has("doctorQualification")) {
                   JSONObject qual = new JSONObject(jObject2.getString("doctorQualification").toString());
                   Iterator quals = qual.keys();


                   while (quals.hasNext()) {
                       String key = String.valueOf((String) quals.next());

                       if (qualification.length() > 0) {

                           qualification = qualification + ", " + (qual.getString(key));
                       } else {
                           qualification = (qual.getString(key));
                       }
                   }

               }

                map.put("qualification",qualification);











                list.add(map);

            }

			/*
			 * while (keys.hasNext()) { String key = (String) keys.next();
			 * JSONArray jArray = new JSONArray(jObject.getString(key));
			 * HashMap<String, String> map = new HashMap<String, String>();
			 * map.put("orderid", key); map.put("amount", jArray.getString(0));
			 * map.put("quantity", jArray.getString(1)); map.put("status",
			 * jArray.getString(2)); map.put("pickupdate", jArray.getString(3));
			 * map.put("deliverydate", jArray.getString(4)); map.put("item",
			 * jArray.getString(5)); list.add(map); }
			 */
        } catch (JSONException e) {
            Log.e("ee", "Error parsing data " + e);
        } catch (NullPointerException e) {
            Log.e("ee", "Result String is empty :" + e);
        }
        return list;
    }



    public static ArrayList<HashMap<String, String>> getpastappoint(JSONArray ja) {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, ArrayList> list1 = new  HashMap<String, ArrayList>();
        Hashtable<String,String> specialitycode=new Hashtable<String, String>();
        JSONObject jObject = null;


        try {
         //   Map<Integer, String[]> leadlist = new HashMap<Integer, String[]>();
            Map<Integer, Hashtable<String,String>> leadlist = new HashMap<Integer, Hashtable<String,String>>();
            for (int i = 0, size = ja.length(); i < size; i++) {
                jObject = new JSONObject(ja.getJSONObject(i).toString());
                Iterator keys = jObject.keys();
                Hashtable<String,String> val=new Hashtable<String,String>();
                String[] val1=new String[4];
                int j=0;

                while (keys.hasNext()) {

                    // int key = Integer.parseInt((String) keys.next());
                    // JSONArray jArray = new JSONArray(jObject.getString(String.valueOf(key)));
                    String key=String.valueOf((String) keys.next());
                    String value="";
                    value =jObject.getString(key);

                    val.put(key, value);
                  //  val[j]=value;
                    j++;


                }
                leadlist.put(i+1, val);
            }
             Map<Integer,  Hashtable<String,String>> sortedOrders = new TreeMap<Integer,  Hashtable<String,String>>();

          sortedOrders.putAll(leadlist);

            Set<Map.Entry<Integer,  Hashtable<String,String>>> entrySet = sortedOrders.entrySet();
            for (Map.Entry<Integer,  Hashtable<String,String>> entry : entrySet) {
                String lead=String.valueOf(entry.getKey());

                HashMap<String, String> map = new HashMap<String, String>();
                Hashtable<String,String> val=new Hashtable<String,String>();
                 val = entry.getValue();

               // JSONObject rating = new JSONObject(val.get("RatingEntered"));

                 map.put("RatingEntered", val.get("RatingEntered"));


                JSONObject jObject1 = new JSONObject(val.get("AppointmentDetails"));
                map.put("ClinicName", jObject1.getString("ClinicName"));
                map.put("DayCD", jObject1.getString("DayCD"));
                map.put("ConsultationDt", jObject1.getString("ConsultationDt"));
                map.put("StartTime", jObject1.getString("StartTime"));
                map.put("PatientID", jObject1.getString("PatientID"));
                map.put("AppointmentID", jObject1.getString("AppointmentID"));
                map.put("searchid", jObject1.getString("DoctorID"));

                JSONObject jObject2 = new JSONObject(val.get("DoctorDetails"));


                map.put("doctorFullName", jObject2.getString("doctorFullName"));
                map.put("ProfileImage", "");
                if(jObject2.has("ProfileImage")){
                    map.put("ProfileImage", jObject2.getString("ProfileImage"));
                }else{
                    map.put("ProfileImage", "@drawable/profileimg_male");

                }
                map.put("gender", jObject2.getString("gender"));

                JSONArray dphone=new JSONArray(jObject2.getString("doctorPhone"));
                JSONArray dadd=new JSONArray(jObject2.getString("doctorAddress"));
                JSONArray email=new JSONArray(jObject2.getString("doctorEmail"));
                String EmailID="";

                for (int i = 0, size = email.length(); i < size; i++) {
                    JSONObject emailobj = new JSONObject(email.getJSONObject(i).toString());


                    if(emailobj.has("EmailID")){
                        EmailID=emailobj.getString("EmailID");
                    }else{
                        EmailID="Not Available";
                    }
                }

                for (int i = 0, size = dphone.length(); i < size; i++) {
                    JSONObject phobj = new JSONObject(dphone.getJSONObject(i).toString());
                    map.put("doctorphone", phobj.getString("PhNumber"));
                }

                if(dphone.length()==0){
                    map.put("doctorphone","");
                }

                for (int i = 0, size = dadd.length(); i < size; i++) {
                    JSONObject phobj = new JSONObject(dadd.getJSONObject(i).toString());
                    String address="";
                    if( phobj.has("Address2")) {
                        address=phobj.getString("Address2");
                    }else{
                        address="";
                    }

                    map.put("doctoraddress", address);
                    map.put("AddressID", phobj.getString("AddressID"));
                    JSONObject phobj1 = new JSONObject(phobj.getString("ExtDetails"));
                    map.put("doclat", phobj1.getString("Latitude"));
                    map.put("doclan", phobj1.getString("Longitude"));
                    map.put("REMAIN", phobj1.getString("Latitude")+"::"+phobj1.getString("Longitude")+"::"+address+"::"+EmailID);
                }





                JSONObject spec = new JSONObject(jObject2.getString("doctorSpecialization"));
                Iterator keys = spec.keys();
                String specialist="";

                while (keys.hasNext()) {
                    String key =String.valueOf((String) keys.next());
                    specialitycode=Specialitycode();
                    if(specialist.length()>0) {

                        specialist =    specialist +","+specialitycode.get(spec.getString(key));
                    }else{
                        specialist =specialitycode.get(spec.getString(key));
                    }
                }
                map.put("specialist",specialist);


                String qualification = "";

                if(jObject2.has("doctorQualification")) {
                    JSONObject qual = new JSONObject(jObject2.getString("doctorQualification").toString());
                    Iterator quals = qual.keys();


                    while (quals.hasNext()) {
                        String key = String.valueOf((String) quals.next());

                        if (qualification.length() > 0) {

                            qualification = qualification + ", " + (qual.getString(key));
                        } else {
                            qualification = (qual.getString(key));
                        }
                    }

                }

                map.put("qualification",qualification);











                list.add(map);

            }

			/*
			 * while (keys.hasNext()) { String key = (String) keys.next();
			 * JSONArray jArray = new JSONArray(jObject.getString(key));
			 * HashMap<String, String> map = new HashMap<String, String>();
			 * map.put("orderid", key); map.put("amount", jArray.getString(0));
			 * map.put("quantity", jArray.getString(1)); map.put("status",
			 * jArray.getString(2)); map.put("pickupdate", jArray.getString(3));
			 * map.put("deliverydate", jArray.getString(4)); map.put("item",
			 * jArray.getString(5)); list.add(map); }
			 */
        } catch (JSONException e) {
            Log.e("ee", "Error parsing data " + e);
        } catch (NullPointerException e) {
            Log.e("ee", "Result String is empty :" + e);
        }catch (Exception e) {
            Log.e("eRee", " :" + e.toString());
        }
        return list;
    }



}


