package hCue.Hcue.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

@SuppressLint("CommitPrefEdits")
public class Preference {

    private SharedPreferences preferences;
    private SharedPreferences.Editor edit;
    public static final String BUILD_INSTALLATIONDATE = "BUILD_INSTALLATION_DATE";
    public static final String ADMIN_ID = "ADMIN_ID"; //string
    public static final String ADMIN_PASSWORD = "ADMIN_PASSWORD"; //string
    public static final String DOCTOR_ID = "DOCTOR_ID";
    public static final String ISSINGLEPATIENT = "ISSINGLEPATIENT";
    public static final String GETPATIENTSRESPONSE = "GETPATIENTSRESPONSE";
    public static final String CLINIC_MENU_COUNT		= 	"Clinic_Menu_Count";
    public static final String CLINIC_MENU_IMG_BASE		= 	"Clinic_Menu_Img_Base";



    public Preference(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        edit = preferences.edit();
    }

    public void saveStringInPreference(String strKey, String strValue) {
        edit.putString(strKey, strValue);
    }

    public void saveIntInPreference(String strKey, int value) {
        edit.putInt(strKey, value);
    }

    public void saveBooleanInPreference(String strKey, boolean value) {
        edit.putBoolean(strKey, value);
    }

    public void saveLongInPreference(String strKey, Long value) {
        edit.putLong(strKey, value);
    }

    public void saveDoubleInPreference(String strKey, String value) {
        edit.putString(strKey, value);
    }

    public void removeFromPreference(String strKey) {
        edit.remove(strKey);
    }

    public void commitPreference() {
        edit.commit();
    }

    public String getStringFromPreference(String strKey, String defaultValue) {
        return preferences.getString(strKey, defaultValue);
    }

    public boolean getbooleanFromPreference(String strKey, boolean defaultValue) {
        return preferences.getBoolean(strKey, defaultValue);
    }

    public int getIntFromPreference(String strKey, int defaultValue) {
        return preferences.getInt(strKey, defaultValue);
    }

    public double getDoubleFromPreference(String strKey, double defaultValue) {
        return Double.parseDouble(preferences.getString(strKey, ""
                + defaultValue));
    }

    public long getLongInPreference(String strKey) {
        return preferences.getLong(strKey, 0);
    }

    public void clearPreferences() {
        edit.clear();
        edit.commit();
    }
    public void saveBoolean(String strKey,boolean value)
    {
        edit.putBoolean(strKey, value);
    }
}
