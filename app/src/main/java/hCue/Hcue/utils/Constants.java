package hCue.Hcue.utils;

import android.os.Environment;

import java.util.HashMap;

/**
 * Created by Saidi Reddy S on 8/18/2015.
 */
public class Constants {

//   public static final String BASE_URL = "http://staging.covalense.net:86"; //Client testing server web api  06 nov 2015
    // Staging URL as obtained from Mouli for testing the OpenFire.
    public static final String BASE_URL = "http://52.62.156.79/";
    public static final boolean TESTING = false;

    public static final String JOB_ID = "jobID";
    public static final String VISIT = "visit";
    public static final String EVENT_TIME = "eventTime";
    public static final String SP_ID = "spId";

    public static final String ACTION_REFRESH = "com.cvl.tradie.enduser.ACTION_REFRESH";

    //Non Cookie Urls for Development

//    public static final String DOCTOR_CONSTANT_URL  = "http://dct4avjn1lfw.cloudfront.net";//"http://d318m5cseah7np.cloudfront.net";
//    public static final String PATIENT_CONSTANT_URL = "http://d1lmwj8jm5d3bc.cloudfront.net";//http://d318m5cseah7np.cloudfront.net
//    public static final String DEVICE_REGISTRATION  = "http://d318m5cseah7np.cloudfront.net";//http://d318m5cseah7np.cloudfront.net

    //Pharma Production url
    public static  final String PHARMA_URL = "http://hcue-api-lite.azurewebsites.net";
    //Pharma Dev Url
//    public static  final String PHARMA_URL = "http://hcue-dev-lite.azurewebsites.net";


    //Development  cookies
    /*public static final String DOCTOR_CONSTANT_URL    = "http://d3n4mps28nrjm1.cloudfront.net";
    public static final String PATIENT_CONSTANT_URL     = "http://dt1nglnprgi4m.cloudfront.net";
    public static final String DEVICE_REGISTRATION      = "http://d1qsr4o526ihbg.cloudfront.net";*/

    //Development
    public static final String DOCTOR_CONSTANT_URL    = "http://dct4avjn1lfw.cloudfront.net";
    public static final String PATIENT_CONSTANT_URL     = "http://d1lmwj8jm5d3bc.cloudfront.net";
    public static final String DEVICE_REGISTRATION      = "http://d318m5cseah7np.cloudfront.net";

    //Staging
/*
    public static final String DOCTOR_CONSTANT_URL    = "https://d30oe2oqdfj01g.cloudfront.net";//https://dto5hbrfv8wm2.cloudfront.net
    public static final String PATIENT_CONSTANT_URL   = "https://d3uq3il3ejsqoq.cloudfront.net";//"https://d2i9dx4plgn9xi.cloudfront.net";
    public static final String DEVICE_REGISTRATION    = "https://d1o21p7csgww7s.cloudfront.net";//https://d1o21p7csgww7s.cloudfront.net
*/

       //PRODUCTION
   /*public static final String DOCTOR_CONSTANT_URL    = "https://d3c1s4h5i8j0yu.cloudfront.net";//"https://d2x8htkzb34f8r.cloudfront.net";
    public static final String PATIENT_CONSTANT_URL   = "https://d9bfdxyln9850.cloudfront.net";//"https://d1aunqc1cts68e.cloudfront.net";
    public static final String DEVICE_REGISTRATION    = "https://d1r5yiqcw3t3fn.cloudfront.net";//"https://d2uy4gg09d0916.cloudfront.net";
*/
    //Development cookies
  /*  public static final String DOCTOR_HOST      = "d3n4mps28nrjm1";
    public static final String PATIENT_HOST       = "dt1nglnprgi4m";
    public static final String PLATFORM_HOST      = "d1qsr4o526ihbg";*/

    //Development
    public static final String DOCTOR_HOST      = "dct4avjn1lfw";
    public static final String PATIENT_HOST       = "d1lmwj8jm5d3bc";
    public static final String PLATFORM_HOST      = "d318m5cseah7np";

    //Staging
//    public static final String DOCTOR_HOST        = "dto5hbrfv8wm2";
//    public static final String PATIENT_HOST       = "d2i9dx4plgn9xi";
//    public static final String PLATFORM_HOST      = "d1lbbchq8szj3q";

    //PRODUCTION
   /* public static final String DOCTOR_HOST        = "d2x8htkzb34f8r";
    public static final String PATIENT_HOST       = "d1aunqc1cts68e";
    public static final String PLATFORM_HOST      = "d2uy4gg09d0916";*/


    //DEVELOPMENT
    /*public static final String DOCTOR_COOKIE  = "AKIAIDMY2DW2BCF6AHQA";
    public static final String PATIENT_COOKIE   = "AKIAIDMY2DW2BCF6AHQA";
    public static final String PLATFORM_COOKIE  = "AKIAIDMY2DW2BCF6AHQA";*/

    //STAGING
//    public static final String DOCTOR_COOKIE    = "QVdTaEN1ZURvY3RvckFwcFdlYkFDTFNUUjAzMDIyMDE2";
//    public static final String PATIENT_COOKIE   = "QVdTaEN1ZVBhdGllbnRBcHBXZWJBQ0xTVFIwMzAyMjAxNg==";
//    public static final String PLATFORM_COOKIE  = "QVdTaEN1ZVBQYXJ0bmVyQXBwV2ViQUNMU1RSMDMwMjIwMTY=";

    //PRODUCTION
    public static final String DOCTOR_COOKIE    = "QVdTaEN1ZURvY3RvckFwcFdlYkFDTFNUUjE3MDIyMDE2";
    public static final String PATIENT_COOKIE   = "QVdTaEN1ZVBhdGllbnRBcHBXZWJBQ0xTVFIxNzAyMjAxNg==";
    public static final String PLATFORM_COOKIE  = "QVdTaEN1ZVBQYXJ0bmVyQXBwV2ViQUNMU1RSMTcwMjIwMTY=";


    public static final String DOCTOR_DETAILS   = "DOCTORDETAILS";
    public static final String DOCTOR_LOOKUP    = "DOCTOR_LOOKUP";

    public static int appointment_count = 0;

    public static HashMap<String, String> specialitiesMap = new HashMap<>();

    public static final String SDCARD = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String IMAGEFORMAT = ".jpg";
    public static final String IMAGE_NAME = "ProfilePicture" + IMAGEFORMAT;
}
