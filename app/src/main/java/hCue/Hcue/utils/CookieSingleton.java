package hCue.Hcue.utils;


/**
 * Created by Appdest on 30-05-2016.
 */
public class CookieSingleton {

    private static CookieValues cookieValues ;
    private CookieSingleton() {
    }

    public static CookieValues getInstance() {
        if(cookieValues == null) {
            cookieValues = new CookieValues();
        }
        return cookieValues;
    }

    public Object clone()
            throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }
}
