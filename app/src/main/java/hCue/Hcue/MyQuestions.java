package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hCue.Hcue.DAO.ListQuestionsRequestDO;
import hCue.Hcue.DAO.ListQuestionsResponseDO;
import hCue.Hcue.DAO.Patient;
import hCue.Hcue.Fragments.MyQuestionsFragment;
import hCue.Hcue.Fragments.PastFragment;
import hCue.Hcue.Fragments.PublicQuestionsFragment;
import hCue.Hcue.Fragments.UpcomingFragment;
import hCue.Hcue.model.AddUpdatePatientOrderRequest;
import hCue.Hcue.model.AddUpdatePatientOrderResponce;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 22/06/16.
 */
public class MyQuestions extends BaseActivity
{
    private TabLayout tabLayout;
    private LinearLayout llMyQuestions;
    private Patient selectedPatienRow ;
    private RippleView rvContinue;
    private Button btnContinue;


    @Override
    public void initialize()
    {
        llMyQuestions =   (LinearLayout)  getLayoutInflater().inflate(R.layout.my_questions,null,false);
        llBody.addView(llMyQuestions,baseLayoutParams);
        llAppointments.setBackgroundResource(R.drawable.menu_selected_bg);
        tvAppointmentHighlight.setVisibility(View.VISIBLE);
        tvTopTitle.setText("Questions");
        ViewPager viewPager     = (ViewPager) llMyQuestions.findViewById(R.id.viewpager);
        tabLayout               = (TabLayout) llMyQuestions.findViewById(R.id.tabs);
        rvContinue              =   (RippleView)llMyQuestions.findViewById(R.id.rvContinue);
        btnContinue             =   (Button)llMyQuestions.findViewById(R.id.btnContinue);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
        changeTabsFont();

        setSpecificTypeFace(llMyQuestions, ApplicationConstants.WALSHEIM_SEMI_BOLD);

        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        llLocation.setVisibility(View.GONE);
        tvTopTitle.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView)
            {

                ShowAlertDialog();
                /*Intent in = new Intent(MyQuestions.this, AskPublicQuestionActivity.class);
                startActivity(in);*/
            }
        });


        /*llBack.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    llBack.setBackgroundColor(Color.parseColor("#33000000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    llBack.setBackgroundColor(Color.parseColor("#00000000"));
                    Intent slideactivity = new Intent(MyQuestions.this, Specialities.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_left_in,R.anim.push_left_out).toBundle();
                    startActivity(slideactivity, bndlanimation);
                    finish();
                }
                return true;
            }
        });*/
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(MyQuestionsFragment.getInstance(), "YOUR QUESTIONS");
        adapter.addFragment(PublicQuestionsFragment.getInstance(), "PUBLIC QUESTIONS");
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    public void onBackPressed() {
        Intent slideactivity = new Intent(MyQuestions.this, Specialities.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
        slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(slideactivity, bndlanimation);
        finish();
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                }
            }
        }
    }

    public void ShowAlertDialog()
    {

        try{
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MyQuestions.this, R.style.MyQuestionDialogTheme);
            View dialogView = LayoutInflater.from(MyQuestions.this).inflate(R.layout.question_terms_condition_dialog, null);
            dialogBuilder.setView(dialogView);
            TextView tvDisclaimer = (TextView) dialogView.findViewById(R.id.tvDisclaimer);
            final TextView tvDisclaimerInfo = (TextView) dialogView.findViewById(R.id.tvDisclaimerInfo);
            final TextView tvPrivacy = (TextView) dialogView.findViewById(R.id.tvPrivacy);
            TextView tvPrivacyInfo = (TextView) dialogView.findViewById(R.id.tvPrivacyInfo);
            ImageView ivClose = (ImageView) dialogView.findViewById(R.id.ivClose);
            RippleView rvContinue = (RippleView) dialogView.findViewById(R.id.rvContinue);
            TextView tvContinue = (TextView) dialogView.findViewById(R.id.tvContinue);

            tvDisclaimer.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvDisclaimerInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvPrivacy.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvPrivacyInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvContinue.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();

            rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                @Override
                public void onComplete(RippleView rippleView) {
                    alertDialog.dismiss();
                    Intent in = new Intent(MyQuestions.this, AskPublicQuestionActivity.class);
                    startActivity(in);
                }
            });

            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
