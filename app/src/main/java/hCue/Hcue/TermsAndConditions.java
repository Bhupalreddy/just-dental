package hCue.Hcue;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hCue.Hcue.utils.ApplicationConstants;


/**
 * Created by shyamprasadg on 15/06/16.
 */
public class TermsAndConditions extends AppCompatActivity
{
    private  TextView text1,text2,text3,text4,text5,text6,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20,text21,text22,text23,text24,text25,text26,text27,text28,text29,text30,text31,text32,text33,text34,text35,text36,text37,text38,text39,text40,text41,text42,text43,text44,text45,text46,text47,text48,text49,text50,text51,text52,text53,text54,text55,text56,text57,text58,text59,text60,text61,text62,text63,text64,text65,text66,text67,text68,text69,text70,text71,text72,text73,text74,text75,text76,text77,text78,text79,text80,text81,text82,text83,text84,text85,text86,text87,text88,text89,text90,text91,text92,text93,text94,text95,text96,text97,text98;
    private final static float STEP = 200;
    private float mRatio = 1.0f;
    private int mBaseDist;
    private float mBaseRatio;
    private float fontsize = 16;
    private ImageView ivClose;
    private LinearLayout llTerms;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_conditions);

        text1   = (TextView) findViewById(R.id.text1);
        text2   = (TextView) findViewById(R.id.text2);
        text3   = (TextView) findViewById(R.id.text3);
        text4   = (TextView) findViewById(R.id.text4);
        text5   = (TextView) findViewById(R.id.text5);
        text6   = (TextView) findViewById(R.id.text6);
        text7   = (TextView) findViewById(R.id.text7);
        text8   = (TextView) findViewById(R.id.text8);
        text9   = (TextView) findViewById(R.id.text9);
        text10  = (TextView) findViewById(R.id.text10);
        text11  = (TextView) findViewById(R.id.text11);
        text12  = (TextView) findViewById(R.id.text12);
        text13  = (TextView) findViewById(R.id.text13);
        text14  = (TextView) findViewById(R.id.text14);
        text15  = (TextView) findViewById(R.id.text15);
        text16  = (TextView) findViewById(R.id.text16);
        text17  = (TextView) findViewById(R.id.text17);
        text18  = (TextView) findViewById(R.id.text18);
        text19  = (TextView) findViewById(R.id.text19);
        text20  = (TextView) findViewById(R.id.text20);
        text21  = (TextView) findViewById(R.id.text21);
        text22  = (TextView) findViewById(R.id.text22);
        text23  = (TextView) findViewById(R.id.text23);
        text24  = (TextView) findViewById(R.id.text24);
        text25  = (TextView) findViewById(R.id.text25);
        text26  = (TextView) findViewById(R.id.text26);
        text27  = (TextView) findViewById(R.id.text27);
        text28  = (TextView) findViewById(R.id.text28);
        text29  = (TextView) findViewById(R.id.text29);
        text30  = (TextView) findViewById(R.id.text30);
        text31  = (TextView) findViewById(R.id.text31);
        text32  = (TextView) findViewById(R.id.text32);
        text33  = (TextView) findViewById(R.id.text33);
        text34  = (TextView) findViewById(R.id.text34);
        text35  = (TextView) findViewById(R.id.text35);
        text36  = (TextView) findViewById(R.id.text36);
        text37  = (TextView) findViewById(R.id.text37);
        text38  = (TextView) findViewById(R.id.text38);
        text39  = (TextView) findViewById(R.id.text39);
        text40  = (TextView) findViewById(R.id.text40);
        text41  = (TextView) findViewById(R.id.text41);
        text42  = (TextView) findViewById(R.id.text42);
        text43  = (TextView) findViewById(R.id.text43);
        text44  = (TextView) findViewById(R.id.text44);
        text45  = (TextView) findViewById(R.id.text45);
        text46  = (TextView) findViewById(R.id.text46);
        text47  = (TextView) findViewById(R.id.text47);
        text48  = (TextView) findViewById(R.id.text48);
        text49  = (TextView) findViewById(R.id.text49);
        text50  = (TextView) findViewById(R.id.text50);
        text51  = (TextView) findViewById(R.id.text51);
        text52  = (TextView) findViewById(R.id.text52);
        text53  = (TextView) findViewById(R.id.text53);
        text54  = (TextView) findViewById(R.id.text54);
        text55  = (TextView) findViewById(R.id.text55);
        text56  = (TextView) findViewById(R.id.text56);
        text57  = (TextView) findViewById(R.id.text57);
        text58  = (TextView) findViewById(R.id.text58);
        text59  = (TextView) findViewById(R.id.text59);
        text60  = (TextView) findViewById(R.id.text60);
        text61  = (TextView) findViewById(R.id.text61);
        text62  = (TextView) findViewById(R.id.text62);
        text63  = (TextView) findViewById(R.id.text63);
        text64  = (TextView) findViewById(R.id.text64);
        text65  = (TextView) findViewById(R.id.text65);
        text66  = (TextView) findViewById(R.id.text66);
        text67  = (TextView) findViewById(R.id.text67);
        text68  = (TextView) findViewById(R.id.text68);
        text69  = (TextView) findViewById(R.id.text69);
        text70  = (TextView) findViewById(R.id.text70);
        text71  = (TextView) findViewById(R.id.text71);
        text72  = (TextView) findViewById(R.id.text72);
        text73  = (TextView) findViewById(R.id.text73);
        text74  = (TextView) findViewById(R.id.text74);
        text75  = (TextView) findViewById(R.id.text75);
        text76  = (TextView) findViewById(R.id.text76);
        text77  = (TextView) findViewById(R.id.text77);
        text78  = (TextView) findViewById(R.id.text78);
        text79  = (TextView) findViewById(R.id.text79);
        text80  = (TextView) findViewById(R.id.text80);
        text81  = (TextView) findViewById(R.id.text81);
        text82  = (TextView) findViewById(R.id.text82);
        text83  = (TextView) findViewById(R.id.text83);
        text84  = (TextView) findViewById(R.id.text84);
        text85  = (TextView) findViewById(R.id.text85);
        text86  = (TextView) findViewById(R.id.text86);
        text87  = (TextView) findViewById(R.id.text87);
        text88  = (TextView) findViewById(R.id.text88);
        text89  = (TextView) findViewById(R.id.text89);
        text90  = (TextView) findViewById(R.id.text90);
        text91  = (TextView) findViewById(R.id.text91);
        text92  = (TextView) findViewById(R.id.text92);
        text93  = (TextView) findViewById(R.id.text93);
        text94  = (TextView) findViewById(R.id.text94);
        text95  = (TextView) findViewById(R.id.text95);
        text96  = (TextView) findViewById(R.id.text96);
        text97  = (TextView) findViewById(R.id.text97);
        text98  = (TextView) findViewById(R.id.text98);

        llTerms = (LinearLayout)findViewById(R.id.llTerms);

        ivClose = (ImageView)findViewById(R.id.ivClose);

        text1.setTextSize(mRatio + 13);
        text2.setTextSize(mRatio + 13);
        text3.setTextSize(mRatio + 13);
        text4.setTextSize(mRatio + 13);
        text5.setTextSize(mRatio + 13);
        text6.setTextSize(mRatio + 13);
        text7.setTextSize(mRatio + 13);
        text8.setTextSize(mRatio + 13);
        text9.setTextSize(mRatio + 13);
        text10.setTextSize(mRatio + 13);
        
        BaseActivity.setSpecificTypeFace(llTerms, ApplicationConstants.WALSHEIM_MEDIUM);

        text1.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text3.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text16.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text34.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text42.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text60.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text63.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text66.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text68.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text73.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text75.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text82.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text84.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text88.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text91.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        text95.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        ivClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                overridePendingTransition(R.anim.push_down_in,R.anim.push_down_out);
                finish();
            }
        });



        text1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text1.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });



        text2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text2.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });



        text3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text3.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });

        text4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text4.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });

        text5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text5.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });

        text6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text6.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });


        text7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text7.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });

        text8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text8.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });

        text9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text9.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });

        text10.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist = getDistance(event);
                        mBaseRatio = mRatio;
                    } else {
                        float delta = (getDistance(event) - mBaseDist) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                        text10.setTextSize(mRatio + 13);
                    }
                }
                return true;
            }
        });
    }
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getPointerCount() == 2) {
            int action = event.getAction();
            int pureaction = action & MotionEvent.ACTION_MASK;
            if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                mBaseDist = getDistance(event);
                mBaseRatio = mRatio;
            } else {
                float delta = (getDistance(event) - mBaseDist) / STEP;
                float multi = (float) Math.pow(2, delta);
                mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                text2.setTextSize(mRatio + 13);
            }
        }
        return true;
    }

    int getDistance(MotionEvent event) {
        int dx = (int) (event.getX(0) - event.getX(1));
        int dy = (int) (event.getY(0) - event.getY(1));
        return (int) (Math.sqrt(dx * dx + dy * dy));
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
        
    }

