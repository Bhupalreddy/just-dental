package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import hCue.Hcue.DAO.DoctorAddress;
import hCue.Hcue.DAO.PastFutureRow;
import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.Fragments.PastFragment;
import hCue.Hcue.Fragments.UpcomingFragment;
import hCue.Hcue.model.CancelAppointmentReq;
import hCue.Hcue.model.MyAppointmentRequest;
import hCue.Hcue.model.PastFutureAppointmentsRes;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 22/06/16.
 */
public class MyAppointments extends BaseActivity
{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private LinearLayout llmyappointment;
    private ImageView ivCamera,ivNameUpdate,ivProfilePic;
    private TextView tvProfileName;
    private Patient selectedPatienRow ;
    private PopupWindow popupWindow ;
    private static Fragment instance;


    @Override
    public void initialize()
    {
        llmyappointment =   (LinearLayout)  getLayoutInflater().inflate(R.layout.my_appointments,null,false);
        llBody.addView(llmyappointment,baseLayoutParams);
        llAppointments.setBackgroundResource(R.drawable.menu_selected_bg);
        tvAppointmentHighlight.setVisibility(View.VISIBLE);
//        ivAppointment.setImageResource(R.drawable.appointment_hov);
//        tvAppointment.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvTopTitle.setText("MY APPOINTMENTS");
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) llmyappointment.findViewById(R.id.tabs);
        selectedPatienRow = LoginResponseSingleton.getSingleton().getArrPatients().get(0);

        /*if (Connectivity.isConnected(MyAppointments.this))
        {
            callgetPatientsService();
        }else
        {
            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
        }*/

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
        changeTabsFont();

        setSpecificTypeFace(llmyappointment, ApplicationConstants.WALSHEIM_SEMI_BOLD);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        llLocation.setVisibility(View.GONE);
        tvTopTitle.setVisibility(View.VISIBLE);

        llLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDropdownPatients();
            }
        });

        llBack.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    llBack.setBackgroundColor(Color.parseColor("#33000000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    llBack.setBackgroundColor(Color.parseColor("#00000000"));
                    Intent slideactivity = new Intent(MyAppointments.this, Specialities.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_left_in,R.anim.push_left_out).toBundle();
                    startActivity(slideactivity, bndlanimation);
                    finish();
                }
                return true;
            }
        });


    }

    /*private void showDropdownPatients()
    {
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.custom_dialog,null);
        if(popupWindow == null)
        {
            popupWindow = new PopupWindow(view, llLocation.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
            ListView lv_source_names = (ListView) view.findViewById(R.id.lv_source_names);

            PatientsAdapter  patientsAdapter = new PatientsAdapter();
            lv_source_names.setAdapter(patientsAdapter);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            popupWindow.setOutsideTouchable(true);
            tvLocation.setTag(listPatientRows.get(0));
            tvLocation.setText(listPatientRows.get(0).getArrPatients().get(0).getFullName());
            setSpecificTypeFace(lv_source_names, ApplicationConstants.WALSHEIM_MEDIUM);
            popupWindow.showAsDropDown(llLocation);
            lv_source_names.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));
                    tvLocation.setTag(listPatientRows.get(position));
                    tvLocation.setText(listPatientRows.get(position).getArrPatients().get(0).getFullName());
                    popupWindow.dismiss();
                    popupWindow = null;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
           *//* lv_source_names.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));
                    tvTitle.setTag((String) parent.getItemAtPosition(position));
                    tvTitle.setText((String) parent.getItemAtPosition(position));
                    popupWindow.dismiss();
                }
            });*//*
        }else
        {

            popupWindow.dismiss();
            popupWindow = null;
        }
    }

    public class PatientsAdapter extends BaseAdapter
    {

        @Override
        public int getCount() {
            return listPatientRows.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(MyAppointments.this).inflate(R.layout.location_text,null);
            TextView tvLocationId = (TextView) convertView.findViewById(R.id.tvLocationId);
            tvLocationId.setText(listPatientRows.get(position).getArrPatients().get(0).getFullName());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPatienRow = listPatientRows.get(position);
                    tvLocation.setTag(listPatientRows.get(position));
                    tvLocation.setText(listPatientRows.get(position).getArrPatients().get(0).getFullName());
                    popupWindow.dismiss();
                    popupWindow = null;
                    callgetPastFutureAppointmentsService(upcomingAdapter);
                    callgetPastFutureAppointmentsService(pastAdapter);

                }
            });
            return convertView;
        }
    }*/

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(UpcomingFragment.getInstance(), "UPCOMING");
        adapter.addFragment(PastFragment.getInstance(), "PAST");
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onResume() {

        super.onResume();
        if(selectedPatienRow != null)
            tvLocation.setText(selectedPatienRow.getFullName());

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }








    /*private void callgetPatientsService()
    {
        showLoader("");
        GetPatientsRequest getPatientsRequest = new GetPatientsRequest();
        getPatientsRequest.setPhoneNumber((LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFamilyHdID()+"").substring(0,10));
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(getPatientsRequest, new RestCallback<GetPatientsResponse>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
                hasmultipleusers = false ;
                setupViewPager(viewPager);
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
                tabLayout.setSelectedTabIndicatorHeight(10);
                tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
                changeTabsFont();
            }

            @Override
            public void success(GetPatientsResponse getPatientsResponse, Response response) {
                hideLoader();
                if(getPatientsResponse != null)
                {
                    if(getPatientsResponse.getCount()==0 || getPatientsResponse.getCount()==1)
                    {
                        hasmultipleusers = false;
                        setupViewPager(viewPager);
                        tabLayout.setupWithViewPager(viewPager);
                        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
                        tabLayout.setSelectedTabIndicatorHeight(10);
                        tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
                        changeTabsFont();
                    }else
                    {
                        hasmultipleusers = true;
                        listPatientRows = getPatientsResponse.getPatientRows();
                        selectedPatienRow = listPatientRows.get(0);
                        tvLocationHeader.setText("APPOINTMENTS FOR");
                        tvLocation.setText(listPatientRows.get(0).getArrPatients().get(0).getFullName());
                        setupViewPager(viewPager);
                        tabLayout.setupWithViewPager(viewPager);
                        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
                        tabLayout.setSelectedTabIndicatorHeight(10);
                        tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
                        changeTabsFont();
//                        tvTopTitle.setVisibility(View.GONE);
//                        llLocation.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }*/





    @Override
    public void onBackPressed() {
        Intent slideactivity = new Intent(MyAppointments.this, Specialities.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
        slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(slideactivity, bndlanimation);
        finish();
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                }
            }
        }
    }

}
