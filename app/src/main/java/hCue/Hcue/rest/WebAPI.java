package hCue.Hcue.rest;


import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

import hCue.Hcue.DAO.AskPublicQuestionRequestDO;
import hCue.Hcue.DAO.CancelOrderByPatientRequest;
import hCue.Hcue.DAO.CancelOrderByPatientResponse;
import hCue.Hcue.DAO.GlobalSearchRequest;
import hCue.Hcue.DAO.GlobalSearchResponseDO;
import hCue.Hcue.DAO.LikeDisLikeAnswerRequestDO;
import hCue.Hcue.DAO.ListQuestionsRequestDO;
import hCue.Hcue.DAO.ListQuestionsResponseDO;
import hCue.Hcue.DAO.MedicineListingRequestDO;
import hCue.Hcue.DAO.MedicineListingResponseDO;
import hCue.Hcue.DAO.PatientOrderRequest;
import hCue.Hcue.DAO.PatientOrderResponse;
import hCue.Hcue.DAO.PatientOrdersResponseDAO;
import hCue.Hcue.DAO.PharmaDetailsDo;
import hCue.Hcue.DAO.QuestionsResponsDO;
import hCue.Hcue.DAO.SpecialityRequestDO;
import hCue.Hcue.DAO.SpecialityResponseDO;
import hCue.Hcue.DAO.UpdateDoctorAppointmentRequestDO;
import hCue.Hcue.DAO.UpdateDoctorAppointmentResponseDO;
import hCue.Hcue.DAO.UpdatePatientRecordRequestDO;
import hCue.Hcue.DAO.UploadfileRequest;
import hCue.Hcue.model.AddUpdatePatientCaseDocumentImageRequest;
import hCue.Hcue.model.AddUpdatePatientCaseDocumentImageResponce;
import hCue.Hcue.model.AddUpdatePatientOrderRequest;
import hCue.Hcue.model.AddUpdatePatientOrderResponce;
import hCue.Hcue.model.AddUserDevice;
import hCue.Hcue.model.AddUserDeviceRequest;
import hCue.Hcue.model.CancelAppointmentReq;
import hCue.Hcue.model.CityLookupResponse;
import hCue.Hcue.model.ConfirmAppointmentReq;
import hCue.Hcue.model.ConfirmAppointmentRes;
import hCue.Hcue.model.CountryLookupResponse;
import hCue.Hcue.model.DeletePatientCaseDocumentImageRequest;
import hCue.Hcue.model.DoctorDetailsResponse;
import hCue.Hcue.model.DoctorRatingReq;
import hCue.Hcue.model.FileDeleteReq;
import hCue.Hcue.model.ForgotPasswordCheckPhoneRequest;
import hCue.Hcue.model.ForgotPasswordFindDetailsResponse;
import hCue.Hcue.model.ForgotPasswordUpdatePinRequest;
import hCue.Hcue.model.GetAvailableAppointmentsReq;
import hCue.Hcue.model.GetAvailableAppointmentsRes;
import hCue.Hcue.model.GetDoctorDetailsRequest;
import hCue.Hcue.model.GetPatientDetailsReq;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.model.ListPatientOrderAddressRequest;
import hCue.Hcue.model.ListPatientOrderAddressResponce;
import hCue.Hcue.model.ListPatientOrderRequest;
import hCue.Hcue.model.LocationLookupResponse;
import hCue.Hcue.model.LoginRequest;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.LookupRequest;
import hCue.Hcue.model.MyAppointmentRequest;
import hCue.Hcue.model.OTPRequest;
import hCue.Hcue.model.OTPResponse;
import hCue.Hcue.model.OrderAddressRequest;
import hCue.Hcue.model.OrderAddressResponce;
import hCue.Hcue.model.OtpViaEmailRequest;
import hCue.Hcue.model.PastFutureAppointmentsRes;
import hCue.Hcue.model.PatientFilesResposne;
import hCue.Hcue.model.PatientLookupResponse;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.model.PatientVisitRespone;
import hCue.Hcue.model.PatientVisitdetailsReq;
import hCue.Hcue.model.PatientsVisitsRequest;
import hCue.Hcue.model.RegisterRequest;
import hCue.Hcue.model.ResetPasswordRequest;
import hCue.Hcue.model.SearchBySpecialityReq;
import hCue.Hcue.model.SearchBySpecialityRes;
import hCue.Hcue.model.SearchHospitalRequest;
import hCue.Hcue.model.SearchHospitalResponce;
import hCue.Hcue.model.StateLookupResponse;
import hCue.Hcue.model.UpdatePatientOrderStatusRequest;
import hCue.Hcue.model.VerifyPatientReq;
import hCue.Hcue.model.getAddUserReponse;
import hCue.Hcue.utils.Constants;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by cvluser on 27-07-2015.
 */
public interface WebAPI {


    @Headers({
            "Content-Type: application/json",
            "User-Agent: Hcue"
    })
    @POST("/doctors/getDoctor")
    void getDoctorDetails(@Body GetDoctorDetailsRequest getdoctorRequest, RestCallback<DoctorDetailsResponse> restCallback);

   @POST("/patients/validate/patientLogin")
   void validateLogin(@Body LoginRequest loginRequest, RestCallback<LoginResponse> restCallback);

   @POST("/patients/getPatients")
   void getPatients(@Body GetPatientsRequest getPatientsRequest, RestCallback<GetPatientsResponse> restCallback);

   @Headers({
           "Content-Type: application/json",
           "Authorization: 47F7D969-B6F9-4860-8EE4-085F1BF3CCE7"
   })
   @POST("/v1/sms/send")
   void sendOPT(@Body OTPRequest otpRequest, RestCallback<OTPResponse> restCallback);

   @POST("/patients/addPatient")
   void patientRegister(@Body RegisterRequest registerRequest, RestCallback<LoginResponse> restCallback);

   @POST("/patients/updatePatient")
   void updatePatient(@Body PatientUpdateRequest patientUpdateRequest, RestCallback<LoginResponse> restCallback);

   @POST("/patients/updtpatientPinPass")
   void updateForgotPinPasscode(@Body ForgotPasswordUpdatePinRequest forgotPasswordUpdatePinRequest, RestCallback<String> restCallback);

   @POST("/patient/sendEmail")
   void sendOTPViaMail(@Body OtpViaEmailRequest otpViaEmailRequest, RestCallback<String> restCallback);

   @POST("/patients/validate/patientPinPass")
   void validateForgotPinPasscode(@Body ForgotPasswordCheckPhoneRequest forgotPasswordCheckPhoneRequest, RestCallback<String> restCallback);

   @POST("/patients/validate/reSetPassword")
   void resetPasscode(@Body ResetPasswordRequest resetPasswordRequest, RestCallback<String> restCallback);

   @POST("/patients/forgotPassword")
   void forgotPasswordFindDetails(@Body HashMap<Object, Object> forgotPasswordFindDetailsRequest, RestCallback<ForgotPasswordFindDetailsResponse> restCallback);

   @POST("/patient/appointment/getPatientAppointments")
   void getPastFutureAppointments(@Body MyAppointmentRequest getPastFutureReq, RestCallback<PastFutureAppointmentsRes> restCallback);

   @POST("/patients/getDoctorsHospitalWise")
   void searchBySpeciality(@Body SearchBySpecialityReq bySpecialityReq, RestCallback<SearchBySpecialityRes> restCallback);

   @POST("/doctor/consultation/availableAppointment")
   void getAvailableAppointments(@Body GetAvailableAppointmentsReq bySpecialityReq, RestCallback<GetAvailableAppointmentsRes> restCallback);

   @POST("/doctors/addDoctorsAppointments")
   void confirAppointment(@Body ConfirmAppointmentReq confirmAppointmentReq, RestCallback<ConfirmAppointmentRes> callback);

   @GET("/doctors/readLookup")
   void getdoctorLookup(RestCallback<PatientLookupResponse> restCallback);

   @POST("/patients/getPatient")
   void getPatientDetails(@Body GetPatientDetailsReq getPatientDetailsReq, RestCallback<LoginResponse> restCallback);

   @POST("/doctors/updtDoctorAppointmentsStatus")
   void cancelDocotAppointment(@Body CancelAppointmentReq cancelAppointmentReq, RestCallback<String> restCallback);

   @POST("/patient/DoctorRating")
   void giveDocotorRating(@Body DoctorRatingReq doctorRatingReq , RestCallback<String> restCallback);

   @Headers({
           "Content-Type: application/json",
           "User-Agent: Hcue",
   })
   @GET("/getDoctorCookie")
   void getDoctorCookies(RestCallback<String> restCallback);

   @Headers({
           "Content-Type: application/json",
           "User-Agent: Hcue",
   })
   @GET("/getPatientCookie")
   void getPatientCookies(RestCallback<String> restCallback);

   @Headers({
           "Content-Type: application/json",
           "User-Agent: Hcue",
   })
   @GET("/getPlatformCookie")
   void getPaltformCookies(RestCallback<String> restCallback);

   @POST("/patients/verifyPatientLogin")
   void verifyPatientLogin(@Body VerifyPatientReq verifyPatientReq, RestCallback<JsonObject> restCallback );

   @POST("/patientCase/listPatientCase")
   void getPatientsVisits(@Body PatientsVisitsRequest patientsVisitsRequest, RestCallback<PatientVisitRespone> restCallback );

   @POST("/patientCase/listPatientCase")
   void getPatientsFiles(@Body PatientsVisitsRequest patientsVisitsRequest, RestCallback<PatientFilesResposne> restCallback );

   @POST("/patientCase/listPatientCase")
   void getPatientsVisitsDetails(@Body PatientVisitdetailsReq patientsVisitsRequest, RestCallback<PatientVisitRespone> restCallback );

   @Multipart
   @POST("/patient/uploadFile")
   void uploadFile(@Part("fileUpload")TypedFile file, @Part ("typeDoc")String typeDoc, @Part ("patientID")String patientID, @Part ("fileExtn")String fileExtn, @Part ("RequestImageURL")String RequestImageURL,@Part ("doctorID")String doctorID, RestCallback<AddUpdatePatientCaseDocumentImageResponce> restCallback);

   @POST("/patientCase/addUpdatePatientCaseDocumentImage")
   void uploadFile(@Body UploadfileRequest uploadfileRequest, RestCallback<String> restCallback);

   @POST("/patientCase/deletePatientCaseDocumentImage")
   void deleteFile(@Body FileDeleteReq fileDeleteReq, RestCallback<String> restCallback);

   @POST("/doctors/updtDoctorAppointments")
   void updateDoctorAppointments(@Body UpdateDoctorAppointmentRequestDO fileDeleteReq, RestCallback<UpdateDoctorAppointmentResponseDO> restCallback);

   @POST("/patients/globalSearch")
   void globalSearch(@Body GlobalSearchRequest fileDeleteReq, RestCallback<GlobalSearchResponseDO> restCallback);

   @POST("/patients/updatePatient")
   void updatePatientRecord(@Body UpdatePatientRecordRequestDO recordRequestDO, RestCallback<Object> restCallback);

   @POST("/platformPartners/addUserDevice")
   void AddUserDevice(@Body AddUserDevice addUserDevicereq, RestCallback<Object> restCallback);

   @POST("/platformPartners/getAddUserDevice")
   void getAddUserDevice(@Body AddUserDeviceRequest addUserDeviceRequest, RestCallback<ArrayList<getAddUserReponse>> restCallback);

   @POST("/platformPartners/searchHospital")
   void getHospitalDetails(@Body SearchHospitalRequest searchHospitalRequest, RestCallback<SearchHospitalResponce> restCallback);

   @POST("/doctor/readCountryLookups")
   void getCountryLookup(@Body LookupRequest lookupRequest, RestCallback<ArrayList<CountryLookupResponse>> countryLookupRes);

   @POST("/doctor/readStateLookups")
   void getStateLookup(@Body LookupRequest lookupRequest, RestCallback<ArrayList<StateLookupResponse>> countryLookupRes);

   @POST("/doctor/readCityLookups")
   void getCityLookup(@Body LookupRequest lookupRequest, RestCallback<ArrayList<CityLookupResponse>> countryLookupRes);

   @POST("/doctor/readLocationLookups")
   void getLocationLookup(@Body LookupRequest lookupRequest, RestCallback<ArrayList<LocationLookupResponse>> countryLookupRes);

   @POST("/platformPartners/searchHospital")
   void getHospitalDetails(@Body LookupRequest lookupRequest, RestCallback<ArrayList<LocationLookupResponse>> countryLookupRes);

   @POST("/patient/addUpdatePatientOrderAddress")
   void addUpdatePatientOrderAddress(@Body OrderAddressRequest orderAddressRequest, RestCallback<OrderAddressResponce> orderAddressResponceRestCallback);

   @POST("/patient/listPatientOrderAddress")
   void listPatientOrderAddress(@Body ListPatientOrderAddressRequest listPatientOrderAddressRequest, RestCallback<ListPatientOrderAddressResponce> listPatientOrderAddressResponceRestCallback);

   @POST("/patient/addUpdatePatientOrder")
   void addUpdatePatientOrder(@Body AddUpdatePatientOrderRequest addUpdatePatientOrderRequest, RestCallback<AddUpdatePatientOrderResponce> listPatientOrderAddressResponceRestCallback);

   @POST("/patient/listPatientOrder")
   void listPatientOrder(@Body ListPatientOrderRequest listPatientOrderRequest, RestCallback<AddUpdatePatientOrderResponce> listPatientOrderResponceRestCallback);

   @POST("/patient/updatePatientOrderStatus")
   void updatePatientOrderStatus(@Body UpdatePatientOrderStatusRequest listPatientOrderRequest, RestCallback<OrderAddressResponce> orderAddressResponceRestCallback);

   @POST("/patientCase/deletePatientCaseDocumentImage")
   void deletePatientCaseDocumentImage(@Body DeletePatientCaseDocumentImageRequest listPatientOrderRequest, RestCallback<String> stringRestCallback);

   @POST("/patientCase/addUpdatePatientCaseDocumentImage")
   void addUpdatePatientCaseDocumentImage(@Body AddUpdatePatientCaseDocumentImageRequest listPatientOrderRequest, RestCallback<AddUpdatePatientCaseDocumentImageResponce> stringRestCallback);

   @POST("/doctor/MedicineListing")
   void medicineListing(@Body MedicineListingRequestDO listPatientOrderRequest, RestCallback<ArrayList<MedicineListingResponseDO>> stringRestCallback);

   @POST("/patient/listPatientOrder")
   void getPharamOrders(@Body PatientOrderRequest listPatientOrderRequest, RestCallback<PatientOrdersResponseDAO> stringRestCallback);

   @POST("/patient/CancelOrderByPatient")
   void CancelOrderByPatient(@Query("orderId") int orderId, @Query("patientId") String patientId, RestCallback<CancelOrderByPatientResponse> restCallback);

   @GET("/patient/ListNetworkPharmacies")
   void getPharmacyList(@Query("patientLat") double patientLat, @Query("patientLon") double patientLon , RestCallback<ArrayList<PharmaDetailsDo>> restCallback);

   @Multipart
   @POST("/patient/addUpdatePatientImage")
   void uploadProfilePic(@Part("fileUpload")TypedFile file, @Part ("fileExtn")String fileExtn, @Part ("patientID")String patientID, RestCallback<AddUpdatePatientCaseDocumentImageResponce> restCallback);

   @POST("/platformPartners/listQuestionsAnswers")
   void listQuestionaries(@Body ListQuestionsRequestDO listQuestionsRequestDO, RestCallback<QuestionsResponsDO> restCallback);

   @POST("/platformPartners/addpatientquestion")
   void askPublicQuestion(@Body AskPublicQuestionRequestDO askPublicQuestionRequestDO, RestCallback<String> restCallback);

   @POST("/doctor/readSpecialityLookup")
   void getdoctorSpeciality(@Body SpecialityRequestDO requestDO, RestCallback<ArrayList<SpecialityResponseDO>> restCallback);

   @POST("/platformPartners/addupdateanswer")
   void updateLikeDislikeAnswer(@Body LikeDisLikeAnswerRequestDO requestDO, RestCallback<String> restCallback);

}
