package hCue.Hcue.rest;


import android.util.Log;

import com.squareup.okhttp.OkHttpClient;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.concurrent.TimeUnit;

import hCue.Hcue.MyApplication;
import hCue.Hcue.rest.WebAPI;
import hCue.Hcue.utils.Constants;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by cvluser on 27-07-2015.
 */
public class RestClient {
    private static WebAPI webAPI;
    private static HttpCookie httpCookie1, httpCookie2, httpCookie3;

//    static {
//        setupRestClient();
//    }

    //
    private RestClient() {
    }

    /**
     * Method to create and return web api
     *
     * @return
     */
    public static WebAPI getAPI(String url)
    {
        setupRestClient(url);
        return webAPI;
    }

    /*
    creating the rest adapter with required configuration
     */
    private static void setupRestClient(final String url)
    {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(15000/*CONNECTION_TIME_OUT*/, TimeUnit.MILLISECONDS);
        okHttpClient.setWriteTimeout(15000/*WRITE_TIME_OUT*/, TimeUnit.MILLISECONDS);
        okHttpClient.setReadTimeout(15000/*READ_TIME_OUT*/, TimeUnit.MILLISECONDS);

        ContinueServices(okHttpClient, url );
    }


    private static void ContinueServices(OkHttpClient okHttpClient, final String url)
    {
        final MyApplication myApplication = MyApplication.getIntance();
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request)
            {
//Staging Urls
                if(url.contains("d30oe2oqdfj01g"))
                    //Doctor Base Url
                    request.addHeader("Authorization","QVdTaEN1ZURvY3RvckFwcFdlYkFDTFNUUjAzMDIyMDE2");
                else if(url.contains("d3uq3il3ejsqoq"))
                    //Patient Base Url
                    request.addHeader("Authorization","QVdTaEN1ZVBhdGllbnRBcHBXZWJBQ0xTVFIwMzAyMjAxNg==");
                else if(url.contains("d1o21p7csgww7s"))
                    //Platform Base Url
                    request.addHeader("Authorization","QVdTaEN1ZVBQYXJ0bmVyQXBwV2ViQUNMU1RSMDMwMjIwMTY=");
//Production Urls
                /*if(url.contains("d3c1s4h5i8j0yu"))
                    //Doctor Base Url
                    request.addHeader("Authorization","QVdTaEN1ZURvY3RvckFwcFdlYkFDTFNUUjE3MDIyMDE2");
                else if(url.contains("d9bfdxyln9850"))
                    //Patient Base Url
                    request.addHeader("Authorization","QVdTaEN1ZVBhdGllbnRBcHBXZWJBQ0xTVFIxNzAyMjAxNg==");
                else if(url.contains("d1r5yiqcw3t3fn"))
                    //Platform Base Url
                    request.addHeader("Authorization","QVdTaEN1ZVBQYXJ0bmVyQXBwV2ViQUNMU1RSMTcwMjIwMTY=");
                if(myApplication.getIdentity() != null && myApplication.getIdentity() != null && !myApplication.getIdentity().equalsIgnoreCase(" "))
                    request.addHeader("X-Access-Token", myApplication.getIdentity());*/
            }
        };
        RestAdapter.Builder builder = new RestAdapter.Builder();

            builder.setRequestInterceptor(requestInterceptor);
        builder.setEndpoint(url/*BASE_URL*/);
        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setClient(new OkClient(okHttpClient));

        RestAdapter restAdapter = builder.build();
        webAPI = restAdapter.create(WebAPI.class);
    }

}
