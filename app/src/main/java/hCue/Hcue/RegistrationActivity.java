package hCue.Hcue;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import hCue.Hcue.CallBackInterfaces.OnRegistrationStepOneCallBack;
import hCue.Hcue.DAO.PatientDetails2;
import hCue.Hcue.DAO.PatientEmail;
import hCue.Hcue.DAO.PatientPhone;
import hCue.Hcue.DAO.RegisterReqSingleton;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.model.OTPRequest;
import hCue.Hcue.model.OTPResponse;
import hCue.Hcue.model.RegisterRequest;
import hCue.Hcue.model.VerifyPatientReq;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.CircleImageView;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateHelper;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;

public class RegistrationActivity extends BaseActivity implements OnRegistrationStepOneCallBack {

    private LinearLayout llFragmentContainer;
    private ImageView ivIndicator1, ivIndicator2, ivIndicator3, ivIndicator4;
    public static RegistrationDO registrationDO = new RegistrationDO();
    private ImageView ivProgress, ivImage;
    private TextView tvText;
    private CircleImageView ivProfileImage;
    private LinearLayout llOtp;
    public static boolean isGplusLogin = false;
    private String GOOGLEID;

        @Override
        public void initialize() {
            llOtp = (LinearLayout) getLayoutInflater().inflate(R.layout.register_activity, null, false);
            llBody.addView(llOtp, baseLayoutParams);

            flBottomBar.setVisibility(View.GONE);
        llLeftMenu.setVisibility(View.GONE);
        tvTopTitle.setVisibility(View.VISIBLE);
        llClose.setVisibility(View.VISIBLE);
        tvTopTitle.setText("Create an Account");
        llFragmentContainer = (LinearLayout) findViewById(R.id.llFragmentContainer);
        ivIndicator1 = (ImageView) findViewById(R.id.ivIndicator1);
        ivIndicator2 = (ImageView) findViewById(R.id.ivIndicator2);
        ivIndicator3 = (ImageView) findViewById(R.id.ivIndicator3);
        ivIndicator4 = (ImageView) findViewById(R.id.ivIndicator4);
        ivProfileImage = (CircleImageView) findViewById(R.id.ivProfileImage);

        ivProgress = (ImageView) findViewById(R.id.ivProgress);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        tvText = (TextView) findViewById(R.id.tvText);

        BaseActivity.setSpecificTypeFace(llOtp, ApplicationConstants.WALSHEIM_MEDIUM);

        Intent in = getIntent();
        if (in != null) {
            registrationDO.fullName = in.getStringExtra("Name");
            registrationDO.email = in.getStringExtra("Email");
            if (in.getStringExtra("Gender") != null) {
                if (in.getStringExtra("Gender").equalsIgnoreCase("0"))
                    registrationDO.gender = 'M';
                else
                    registrationDO.gender = 'F';
            }
            GOOGLEID = in.getStringExtra("GOOGLEID");
        }
        if (in.hasExtra("isGplusLogin")) {
            isGplusLogin = true;
        }

        callRegistrationStep1();
        llClose.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Intent slideactivity = new Intent(RegistrationActivity.this, LaunchActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(RegistrationActivity.this, R.anim.push_down_in, R.anim.push_down_out).toBundle();
                startActivity(slideactivity, bndlanimation);
                finish();
            }
        });

    }

    public void callRegistrationStep1() {
        Fragment frag = new RegisterStepOne(RegistrationActivity.this, RegistrationActivity.this);
        FragmentManager fm1 = RegistrationActivity.this.getSupportFragmentManager();
        FragmentTransaction ft1 = fm1.beginTransaction();
        ft1.replace(R.id.llFragmentContainer, frag);
        ft1.commit();
    }

    public void callRegistrationStep2() {
        Fragment frag = new RegisterStepTwo(RegistrationActivity.this,  RegistrationActivity.this);
        FragmentManager fm1 = RegistrationActivity.this.getSupportFragmentManager();
        FragmentTransaction ft1 = fm1.beginTransaction();
        ft1.replace(R.id.llFragmentContainer, frag);
        ft1.commit();
    }

    public void callRegistrationStep3() {
        Fragment frag = new RegisterStepThree(RegistrationActivity.this, RegistrationActivity.this);
        FragmentManager fm1 = RegistrationActivity.this.getSupportFragmentManager();
        FragmentTransaction ft1 = fm1.beginTransaction();
        ft1.replace(R.id.llFragmentContainer, frag);
        ft1.commit();
    }

    public void callRegistrationStep4() {
        Fragment frag = new RegisterStepFour(RegistrationActivity.this, RegistrationActivity.this);
        FragmentManager fm1 = RegistrationActivity.this.getSupportFragmentManager();
        FragmentTransaction ft1 = fm1.beginTransaction();
        ft1.replace(R.id.llFragmentContainer, frag);
        ft1.commit();
    }

    @Override
    public void onContinueClickListener(int position) {
        if (position == 1){
            tvTopTitle.setText("Your Name & Photo");
            tvText.setText("Your Name & Photo");
            ivProgress.setImageResource(R.drawable.progress_bar_step2);
            clearAllBackgroundIndicators();
            ivIndicator2.setImageResource(R.drawable.circle_indicator_hover);
            callRegistrationStep2();
        }else if (position == 2){
            tvTopTitle.setText("Date of Birth & Gender");
            ivImage.setVisibility(View.GONE);
            ivProfileImage.setVisibility(View.VISIBLE);
            tvText.setText("Hi,"+registrationDO.fullName+"!");
            if(registrationDO.profilePic == null || registrationDO.profilePic.equalsIgnoreCase(""))
            {
                Picasso.with(this).load(R.drawable.photo_container).into(ivProfileImage);
            }
            else{
                setLayoutParams(ivProfileImage, R.drawable.photo_container);
                Picasso.with(this).load(registrationDO.profilePicUri).into(ivProfileImage);

              //  Picasso.with(this).load(registrationDO.profilePicUri).resize((int)getResources().getDimension(R.dimen.register_small_image),(int)getResources().getDimension(R.dimen.register_small_image)).into(ivProfileImage);
            }

            ivProgress.setImageResource(R.drawable.progress_bar_step3);
            clearAllBackgroundIndicators();
            ivIndicator3.setImageResource(R.drawable.circle_indicator_hover);
            callRegistrationStep3();
        }else if (position == 3)
        {
            tvTopTitle.setText("Mobile Number");
            ivImage.setVisibility(View.GONE);
            ivProfileImage.setVisibility(View.VISIBLE);
            tvText.setText("Hi,"+registrationDO.fullName+"!");
            if(registrationDO.profilePic == null || registrationDO.profilePic.equalsIgnoreCase(""))
            {
                Picasso.with(this).load(R.drawable.default_photo).into(ivProfileImage);
            }
            else
            {
                setLayoutParams(ivProfileImage,R.drawable.photo_container);
                Picasso.with(this).load(registrationDO.profilePicUri).into(ivProfileImage);
            }

            ivProgress.setImageResource(R.drawable.progress_bar_step4);
            clearAllBackgroundIndicators();
            ivIndicator4.setImageResource(R.drawable.circle_indicator_hover);
            callRegistrationStep4();
        }else if(position == 0){
            tvTopTitle.setVisibility(View.VISIBLE);
            tvTopTitle.setText("Create Account");
            ivImage.setVisibility(View.VISIBLE);
            ivProfileImage.setVisibility(View.GONE);
            setLayoutParams(ivProfileImage,R.drawable.photo_container);
            Picasso.with(this).load(registrationDO.profilePic).into(ivProfileImage);
            ivProgress.setImageResource(R.drawable.progress_bar_step1);
            clearAllBackgroundIndicators();
            ivIndicator1.setImageResource(R.drawable.circle_indicator_hover);
            callRegisterPatient();
        }
    }

    public void clearAllBackgroundIndicators() {
        ivIndicator1.setImageResource(R.drawable.circle_indicator_normal);
        ivIndicator2.setImageResource(R.drawable.circle_indicator_normal);
        ivIndicator3.setImageResource(R.drawable.circle_indicator_normal);
        ivIndicator4.setImageResource(R.drawable.circle_indicator_normal);
    }

    public static class RegistrationDO implements Serializable {
        public String email, password, fullName, mobile, DOB, profilePic, DOB_DD_MM_YYYY;
        public Date date1;
        public char gender;
        public String countryCode;
        public String profilePicUri;

    }
    private void callRegisterPatient() {
        VerifyPatientReq verifyPatientReq = new VerifyPatientReq();
        verifyPatientReq.setPatientLoginID(registrationDO.email);
        verifyPatientReq.setPhoneNumber(Long.valueOf(registrationDO.mobile));
        showLoader("");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).verifyPatientLogin(verifyPatientReq, new RestCallback<JsonObject>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
            }
            @Override
            public void success(JsonObject jsonObject, Response response) {
                hideLoader();
                if(jsonObject != null)
                {
                    if(jsonObject.has("Message") && jsonObject.has("EmailID"))
                    {
                        isFromRegisterfail = true;
                        ShowAlertDialog("Alert","The given mobile number is already registered with "+jsonObject.get("EmailID").getAsString(),R.drawable.alert);
                    }else if(jsonObject.has("Message"))
                    {
                        String value = jsonObject.get("Message").getAsString();
                        String[] items = {"Mr", "Miss" , "Baby"};
                        if (value.equalsIgnoreCase("New User"))
                        {
                            PatientDetails2 patientDetails2 = new PatientDetails2();
                            patientDetails2.setFirstName(registrationDO.fullName);
                            patientDetails2.setFullName(registrationDO.fullName);
                            patientDetails2.setPatientLoginID(registrationDO.email);
                            if(!isGplusLogin)
                                patientDetails2.setPatientPassword(registrationDO.password);
                            patientDetails2.setMobileID(Long.valueOf(registrationDO.mobile));
                            patientDetails2.setDOB((String) registrationDO.DOB);

                            int age = DateHelper.getAge((Date) registrationDO.date1);
                            if(age >3)
                            {
                                if (registrationDO.gender == 'M') {
                                    patientDetails2.setTitle(items[0]);
                                    patientDetails2.setGender('M');
                                } else {
                                    patientDetails2.setTitle(items[1]);
                                    patientDetails2.setGender('F');
                                }
                            }else
                            {
                                if (registrationDO.gender == 'M')
                                {
                                    patientDetails2.setGender('M');
                                } else
                                {
                                    patientDetails2.setGender('F');
                                }
                                patientDetails2.setTitle(items[2]);
                            }

                            PatientEmail patientEmail = new PatientEmail();
                            patientEmail.setEmailID(registrationDO.email.trim());
                            patientEmail.setEmailIDType("P");
                            patientEmail.setPrimaryIND("Y");

                            ArrayList<PatientEmail> patientEmails = new ArrayList<>();
                            patientEmails.add(patientEmail);

                            PatientPhone patientPhone = new PatientPhone();
                            patientPhone.setPhNumber(Long.valueOf(registrationDO.mobile.toString()));
                            patientPhone.setPhType('M');
                            patientPhone.setPrimaryIND('Y');
                            patientPhone.setPhAreaCD(Integer.parseInt(registrationDO.mobile.substring(4, 10)));
                            patientPhone.setPhCntryCD(91);
                            patientPhone.setPhStateCD(Integer.parseInt(registrationDO.mobile.substring(0, 4)));

                            ArrayList<PatientPhone> patientPhones = new ArrayList<PatientPhone>();
                            patientPhones.add(patientPhone);

                            RegisterRequest registerRequest = RegisterReqSingleton.getSingleton();
                            registerRequest.setPatientDetails2(patientDetails2);
                            registerRequest.setPatientEmails(patientEmails);
                            registerRequest.setPatientPhones(patientPhones);
//                            registerRequest.setProfileImages(registrationDO.profilePic);

                            GetPatientsRequest patientsRequest = new GetPatientsRequest();
                            patientsRequest.setPhoneNumber(registrationDO.mobile);
                            showLoader("");
                            RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(patientsRequest, new RestCallback<GetPatientsResponse>() {
                                @Override
                                public void failure(RestError restError) {
                                    hideLoader();
                                }

                                @Override
                                public void success(GetPatientsResponse getPatientsResponse, Response response) {
                                    hideLoader();
                                    if (getPatientsResponse != null) {
                                        if (getPatientsResponse.getCount() == 0) {
                                            int otp = generateRandom(6);
                                            OTPRequest otpRequest = new OTPRequest();
                                            otpRequest.setMailContent("" + otp);
                                            otpRequest.setText("Your hcue OTP is " + otp);
                                            otpRequest.setUSRId("0");
                                            otpRequest.setTo(registrationDO.countryCode + registrationDO.mobile);
                                            otpRequest.setToAddress("");
                                            callSendOTP(otpRequest, registrationDO.mobile);
                                        } else {
                                            int otp = generateRandom(6);
                                            OTPRequest otpRequest = new OTPRequest();
                                            otpRequest.setMailContent("" + otp);
                                            otpRequest.setText("Your hcue OTP is " + otp);
                                            otpRequest.setUSRId("" + getPatientsResponse.getPatientRows().get(0).getArrPatients().get(0).getPatientID());
                                            otpRequest.setTo(registrationDO.countryCode + registrationDO.mobile);
                                            otpRequest.setToAddress("");
                                            callSendOTP(otpRequest, registrationDO.mobile);
                                        }
                                    }
                                }
                            });
                        }else
                        {
                            isFromRegisterfail = true;
                            ShowAlertDialog("Alert","Entered email is already registered.",R.drawable.alert);
                        }
                    }

                }
            }
        });
    }

    private void callSendOTP(final OTPRequest otpRequest, final String mobilenumber)
    {
        RestClient.getAPI("https://api.checkmobi.com").sendOPT(otpRequest, new RestCallback<OTPResponse>() {
            @Override
            public void failure(RestError restError)
            {
                ShowAlertDialog("Whoops!","Failed to send OTP please try again.",R.drawable.worng);
            }

            @Override
            public void success(OTPResponse otpResponse, Response response) {

/*                PatientPhone patientPhone = new PatientPhone();
                patientPhone.setPhNumber(Long.valueOf(mobilenumber));
                patientPhone.setPhType('M');
                patientPhone.setPrimaryIND('Y');
                patientPhone.setPhAreaCD(Integer.valueOf(mobilenumber.substring(4,10)));
                patientPhone.setPhCntryCD(91);
                patientPhone.setPhStateCD(Integer.valueOf(mobilenumber.substring(0,4)));

                ArrayList<PatientPhone> patientPhones = new ArrayList<PatientPhone>();
                patientPhones.add(patientPhone);
                RegisterReqSingleton.getSingleton().setPatientPhones(patientPhones);
                RegisterReqSingleton.getSingleton().getPatientDetails2().setMobileID(Long.valueOf(mobilenumber));*/

                Intent slideactivity = new Intent(RegistrationActivity.this, EnterOtpActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                slideactivity.putExtra("OTPREQUEST",otpRequest);
                slideactivity.putExtra("OTPVALUE",otpRequest.getMailContent());
                slideactivity.putExtra("Mobile", registrationDO.countryCode+" "+registrationDO.mobile);
                if(isGplusLogin) {
                    slideactivity.putExtra("isGplusLogin", true);
                    slideactivity.putExtra("GOOGLEID",GOOGLEID);
                }
                startActivity(slideactivity, bndlanimation);

            }
        });
    }

    public int generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Integer.parseInt(new String(digits));
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    public void setLayoutParams(View v, int drawableId)
    {
        int [] dDimens = getDrawableDimensions(v.getContext(), drawableId);
        v.getLayoutParams().width = dDimens[0];
        v.getLayoutParams().height = dDimens[1];
    }
    public int[] getDrawableDimensions(Context context, int id)
    {
        int[] dimensions = new int[2];
        if(context == null)
            return dimensions;
        Drawable d = getResources().getDrawable(id);
        if(d != null)
        {
            dimensions[0] = d.getIntrinsicWidth();
            dimensions[1] = d.getIntrinsicHeight();
        }
        return dimensions;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof RegisterStepTwo)
                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
