package hCue.Hcue;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.JsonObject;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import java.util.ArrayList;

import hCue.Hcue.adapters.PromotionsPagerAdapter;
import hCue.Hcue.adapters.RemindersDbAdapter;
import hCue.Hcue.model.LoginRequest;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.VerifyPatientReq;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;

/**
 * Created by Appdest on 10-08-2016.
 */
public class LaunchActivity extends BaseActivity implements
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener
{


    private static final String TAG = "LaunchActivity";

    /* RequestCode for resolutions involving sign-in */
    private static final int RC_SIGN_IN = 1;

    /* RequestCode for resolutions to get GET_ACCOUNTS permission on M */
    private static final int RC_PERM_GET_ACCOUNTS = 2;

    /* Keys for persisting instance variables in savedInstanceState */
    private static final String KEY_IS_RESOLVING = "is_resolving";
    private static final String KEY_SHOULD_RESOLVE = "should_resolve";

    /* Client for accessing Google APIs */
    private GoogleApiClient mGoogleApiClient;

    /* View to display current status (signed-in, signed-out, disconnected, etc) */
    private TextView mStatus;

    // [START resolution_variables]
    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;
    // [END resolution_variables]

    private RippleView rvLogin,rvRegister,rvGoogle;
    private LinearLayout lllaunchscreen;
    private Preference preference ;
    private AutoScrollViewPager autoScrollPager;
    private TextView tvpagedescription,tvTitle;
    private Button   btnLogin,btnRegister;
    private MyPagerAdapter pageAdapter;
    private ViewPager pager;
    private PageIndicator mIndicator;
    private static final int PROFILE_PIC_SIZE = 100;
    protected ProgressDialog progressDialog;
    private View mImage;
    protected Dialog dialog;

    @Override
    public void initialize() {

//        ApplicationConstants.dbAdapter.open();

        lllaunchscreen =   (LinearLayout) getLayoutInflater().inflate(R.layout.launchscreen,null,false);
        llBody.addView(lllaunchscreen,baseLayoutParams);

        preference = new Preference(LaunchActivity.this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        llTop.setVisibility(View.GONE);
        flBottomBar.setVisibility(View.GONE);

        btnLogin            =   (Button)        findViewById(R.id.btnlogin);
        btnRegister         =   (Button)        findViewById(R.id.btnregister);
        rvLogin             =   (RippleView)    findViewById(R.id.rvLogin);
        rvRegister          =   (RippleView)    findViewById(R.id.rvRegister);
        rvGoogle            =   (RippleView)    findViewById(R.id.rvGoogle);
        tvpagedescription   =   (TextView)      findViewById(R.id.tvpagedescription);
        tvTitle             =   (TextView)      findViewById(R.id.tvTitle);

        BaseActivity.setSpecificTypeFace(lllaunchscreen, ApplicationConstants.WALSHEIM_MEDIUM);
        btnRegister.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        btnLogin.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        autoScrollPager = (AutoScrollViewPager) findViewById(R.id.autoScrollPager);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        PromotionsPagerAdapter promotionsPagerAdapter = new PromotionsPagerAdapter(LaunchActivity.this);
        autoScrollPager.setAdapter(promotionsPagerAdapter);
//        autoScrollPager.startAutoScrollPager(autoScrollPager);
        mIndicator.setViewPager(autoScrollPager);
        autoScrollPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position)
                {
                    case 0:
                        tvTitle.setText("Select and view");
                        tvpagedescription.setText("a Doctor's Profile");
                        break;
                    case 1:
                        tvTitle.setText("Choose Doctor by");
                        tvpagedescription.setText(R.string.exp);
                        break;
                    case 2:
                        tvTitle.setText("Find and book");
                        tvpagedescription.setText("appointment instantly");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        rvGoogle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onSignInClicked();
            }
        });

                rvRegister.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
                {
                    @Override
                    public void onComplete(RippleView rippleView)
                    {
                        Intent slideactivity = new Intent(LaunchActivity.this, RegistrationActivity.class);
                        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_up_in,R.anim.push_up_out).toBundle();
                        startActivity(slideactivity, bndlanimation);
                        finish();
            }
        });

        rvLogin.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView)
            {
                Intent slideactivity = new Intent(LaunchActivity.this, Login.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_up_in,R.anim.push_up_out).toBundle();
                startActivity(slideactivity, bndlanimation);
                finish();
            }
        });

        if (savedInstanceState != null) {
            mIsResolving = savedInstanceState.getBoolean(KEY_IS_RESOLVING);
            mShouldResolve = savedInstanceState.getBoolean(KEY_SHOULD_RESOLVE);
        }
        // [END restore_saved_instance_state]


        // [START create_google_api_client]
        // Build GoogleApiClient with access to basic profile
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PLUS_ME))
                .addScope(new Scope(Scopes.PLUS_LOGIN))
                .build();
        // [END create_google_api_client]
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<Fragment> fragments= new ArrayList<Fragment>();


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            if (currentPerson != null) {
                // Show signed-in user's name
                String name = currentPerson.getDisplayName();
                //Toast.makeText(LaunchActivity.this,"This is "+name,Toast.LENGTH_LONG).show();
                //onDisconnectClicked();
                // Show users' email address (which requires GET_ACCOUNTS permission)
                if (checkAccountsPermission()) {
                    final String Email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                    Log.e("GOOGLE ID", currentPerson.getId());
                    String PhotoUrl = currentPerson.getImage().getUrl();

                    PhotoUrl = PhotoUrl.substring(0,
                            PhotoUrl.length() - 2)
                            + PROFILE_PIC_SIZE;
                    preference.saveStringInPreference("PhotoUrl", PhotoUrl);
                    preference.commitPreference();
                    hideLoader();
                    callVerifyPatientLogin(name, Email, PhotoUrl, currentPerson.getId());
                    //Toast.makeText(LaunchActivity.this,"This is "+name+","+Email,Toast.LENGTH_LONG).show();


                }
            } else {
                // If getCurrentPerson returns null there is generally some error with the
                // configuration of the application (invalid Client ID, Plus API not enabled, etc).
                //onDisconnectClicked();
                hideLoader();
                Log.w(TAG, "ERROR");
            }

            // Set button visibility
        } else
        {
            hideLoader();
            // Show signed-out message and clear email field
            //onDisconnectClicked();
        }
    }

    private void callVerifyPatientLogin(String name, final String email, String photoUrl, final String id)
    {
        VerifyPatientReq verifyPatientReq = new VerifyPatientReq();
        verifyPatientReq.setSocialIDType("GOOGLE");
        verifyPatientReq.setSocialID(id);
        verifyPatientReq.setPatientLoginID(email);

        showLoader("");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).verifyPatientLogin(verifyPatientReq, new RestCallback<JsonObject>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
            }
            @Override
            public void success(JsonObject jsonObject, Response response) {
                hideLoader();
                if(jsonObject != null)
                {
                    if(jsonObject.has("Message") && jsonObject.has("EmailID") )
                    {
                        onDisconnectClicked();
                        callLoginService(email, id);
                    }else if(jsonObject.has("Message"))
                    {
                        String value = jsonObject.get("Message").getAsString();
                        if (value.equalsIgnoreCase("New User"))
                        {
                            Person currentPerson = Plus.PeopleApi
                                    .getCurrentPerson(mGoogleApiClient);
                            String id = currentPerson.getId();
                            Log.e("GOOGLE ID", currentPerson.getId());

                            String Name = currentPerson.getDisplayName();
                            String PhotoUrl = currentPerson.getImage().getUrl();
                            String GooglePlusProfile = currentPerson.getUrl();

                            PhotoUrl = PhotoUrl.substring(0,
                                    PhotoUrl.length() - 2)
                                    + PROFILE_PIC_SIZE;
                            String gender="";
                            int gender_ = currentPerson.getGender();
                            if(gender_==0)
                            {
                                gender = "Male";
                            }
                            else{
                                gender = "Female";
                            }
                            String birthday="";
                            if(currentPerson.hasBirthday())
                            {
                                birthday = currentPerson.getBirthday();
                            }
                            onDisconnectClicked();
                            hideLoader();
                                    preference.saveStringInPreference("Email", email);
                                    preference.saveStringInPreference("PhotoUrl", PhotoUrl);
                                    preference.commitPreference();
                                    Intent in = new Intent(LaunchActivity.this, RegistrationActivity.class);
                                    in.putExtra("Name", Name);
                                    in.putExtra("Email", email);
                                    in.putExtra("PhotoUrl", PhotoUrl);
                                    in.putExtra("GooglePlusProfile", GooglePlusProfile);
                                    in.putExtra("Gender", gender);
                                    in.putExtra("Birthday", birthday);
                                    in.putExtra("isGplusLogin", true);
                                    in.putExtra("GOOGLEID",id);

                                    startActivity(in);
                        }else
                        {
                            onDisconnectClicked();
                            callLoginService(email, id);
                        }
                    }

                }
            }
        });

    }

    private void callLoginService(String email, String id)
    {
        showLoader("");
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPatientLoginID(email);
        loginRequest.setSocialIDType("GOOGLE");
        loginRequest.setSocialID(id);

        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).validateLogin(loginRequest, new RestCallback<LoginResponse>() {
            @Override
            public void failure(RestError restError)
            {
                //revokeGplusAccess();
                hideLoader();
                Toast.makeText(LaunchActivity.this,"UserName or Password is incorret", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(LoginResponse loginResponse, Response response)
            {
                hideLoader();
                Preference preference = new Preference(LaunchActivity.this);
                LoginResponseSingleton.getSingleton(loginResponse, LaunchActivity.this);
                Toast.makeText(LaunchActivity.this, "Welcome :"+loginResponse.getArrPatients().get(0).getFullName(), Toast.LENGTH_SHORT).show();

                preference.saveBooleanInPreference("ISLOGOUT", false);
                preference.commitPreference();

                Intent slideactivity = new Intent(LaunchActivity.this, Specialities.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
                finish();
            }
        });
    }

    /**
     * Check if we have the GET_ACCOUNTS permission and request it if we do not.
     * @return true if we have the permission, false if we do not.
     */
    private boolean checkAccountsPermission() {
        final String perm = Manifest.permission.GET_ACCOUNTS;
        int permissionCheck = ContextCompat.checkSelfPermission(this, perm);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            // We have the permission
            return true;
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
            // Need to show permission rationale, display a snackbar and then request
            // the permission again when the snackbar is dismissed.
            ActivityCompat.requestPermissions(LaunchActivity.this,
                    new String[]{perm},
                    RC_PERM_GET_ACCOUNTS);
/*            Snackbar.make(findViewById(R.id.main_layout),
                    R.string.contacts_permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Request the permission again.
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{perm},
                                    RC_PERM_GET_ACCOUNTS);
                        }
                    }).show();*/
            return false;
        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{perm},
                    RC_PERM_GET_ACCOUNTS);
            return false;
        }
    }

    private void showSignedInUI() {
        updateUI(true);
    }

    private void showSignedOutUI() {
        updateUI(false);
    }

    // [START on_start_on_stop]
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }
    // [END on_start_on_stop]

    // [START on_save_instance_state]
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_IS_RESOLVING, mIsResolving);
        outState.putBoolean(KEY_SHOULD_RESOLVE, mShouldResolve);
    }
    // [END on_save_instance_state]

    // [START on_activity_result]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);

        if (requestCode == RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (resultCode != RESULT_OK) {
                mShouldResolve = false;
            }

            mIsResolving = false;
            mGoogleApiClient.connect();
        }
    }
    // [END on_activity_result]

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult:" + requestCode);
        if (requestCode == RC_PERM_GET_ACCOUNTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showSignedInUI();
            } else {
                Log.d(TAG, "GET_ACCOUNTS Permission Denied.");
            }
        }
    }

    // [START on_connected]
    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        showLoader("");
        Log.d(TAG, "onConnected:" + bundle);
        mShouldResolve = false;

        // Show the signed-in UI
        showSignedInUI();
    }
    // [END on_connected]

    @Override
    public void onConnectionSuspended(int i) {
        // The connection to Google Play services was lost. The GoogleApiClient will automatically
        // attempt to re-connect. Any UI elements that depend on connection to Google APIs should
        // be hidden or disabled until onConnected is called again.
        Log.w(TAG, "onConnectionSuspended:" + i);
    }

    // [START on_connection_failed]
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, RC_SIGN_IN);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "Could not resolve ConnectionResult.", e);
                    mIsResolving = false;
                    mGoogleApiClient.connect();
                }
            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
                showErrorDialog(connectionResult);
            }
        } else {
            // Show the signed-out UI
            showSignedOutUI();
        }
    }
    // [END on_connection_failed]

    private void showErrorDialog(ConnectionResult connectionResult) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, RC_SIGN_IN,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                mShouldResolve = false;
                                showSignedOutUI();
                            }
                        }).show();
            } else {
                Log.w(TAG, "Google Play Services Error:" + connectionResult);
                String errorString = apiAvailability.getErrorString(resultCode);
                Toast.makeText(this, errorString, Toast.LENGTH_SHORT).show();

                mShouldResolve = false;
                showSignedOutUI();
            }
        }
    }

    // [START on_click]
    // [END on_click]

    // [START on_sign_in_clicked]
    private void onSignInClicked() {
        // User clicked the sign-in button, so begin the sign-in process and automatically
        // attempt to resolve any errors that occur.
        mShouldResolve = true;
        mGoogleApiClient.connect();

        // Show a message to the user that we are signing in.
    }

    // [START on_sign_out_clicked]
    private void onSignOutClicked() {
        // Clear the default account so that GoogleApiClient will not automatically
        // connect in the future.
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }

        showSignedOutUI();
    }
    // [END on_sign_out_clicked]

    // [START on_disconnect_clicked]
    private void onDisconnectClicked() {
        // Revoke all granted permissions and clear the default account.  The user will have
        // to pass the consent screen to sign in again.
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }

        showSignedOutUI();
    }
    // [END on_disconnect_clicked]

}
