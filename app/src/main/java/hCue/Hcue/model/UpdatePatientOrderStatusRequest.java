package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 11/17/2016.
 */

public class UpdatePatientOrderStatusRequest implements Serializable
{

    @SerializedName("UsrType")
    private String USRType = "PATIENT";

    @SerializedName("UsrID")
    private long UsrID ;

    @SerializedName("PatientOrderID")
    private int PatientOrderID ;

    @SerializedName("OrderStatusID")
    private String OrderStatusID ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUsrID() {
        return UsrID;
    }

    public void setUsrID(long usrID) {
        UsrID = usrID;
    }

    public int getPatientOrderID() {
        return PatientOrderID;
    }

    public void setPatientOrderID(int patientOrderID) {
        PatientOrderID = patientOrderID;
    }

    public String getOrderStatusID() {
        return OrderStatusID;
    }

    public void setOrderStatusID(String orderStatusID) {
        OrderStatusID = orderStatusID;
    }
}
