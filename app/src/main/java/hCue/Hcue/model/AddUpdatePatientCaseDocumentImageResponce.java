package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 11/17/2016.
 */

public class AddUpdatePatientCaseDocumentImageResponce
{
    @SerializedName("DocumentNumber")
    private String DocumentNumber ;

    @SerializedName("ImageURL")
    private String ImageURL ;

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }
}
