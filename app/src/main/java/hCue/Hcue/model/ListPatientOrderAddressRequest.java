package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 11/17/2016.
 */

public class ListPatientOrderAddressRequest implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("PatientID")
    private long PatientID ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }
}
