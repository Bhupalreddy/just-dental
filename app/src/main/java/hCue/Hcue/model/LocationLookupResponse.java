package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 05-10-2016.
 */

public class LocationLookupResponse implements Serializable
{
    @SerializedName("LocationID")
    private String LocationID;

    @SerializedName("LocationIDDesc")
    private String LocationIDDesc;

    @SerializedName("CityID")
    private String CityID;

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public String getLocationIDDesc() {
        return LocationIDDesc;
    }

    public void setLocationIDDesc(String locationIDDesc) {
        LocationIDDesc = locationIDDesc;
    }

    public String getCityID() {
        return CityID;
    }

    public void setCityID(String cityID) {
        CityID = cityID;
    }

    @Override
    public String toString() {
        return this.LocationIDDesc;
    }

    @Override
    public int hashCode() {
        return this.CityID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        else if((obj instanceof CityLookupResponse))
        {
            CityLookupResponse response = (CityLookupResponse) obj;
            if(response.getCityID().equals(this.CityID))
                return true;
            else
                return false;
        }

        else
        {
            LocationLookupResponse response = (LocationLookupResponse) obj;
            if(response.CityID.equals(this.CityID) && response.getLocationIDDesc() == this.LocationIDDesc)
                return  true;
        }
        return false;

    }
}
