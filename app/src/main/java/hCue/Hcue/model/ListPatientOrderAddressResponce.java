package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 11/17/2016.
 */

public class ListPatientOrderAddressResponce implements Serializable
{
    @SerializedName("status")
    private String status ;

    @SerializedName("response")
    private ArrayList<OrderAddressRequest> response ;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<OrderAddressRequest> getResponse() {
        return response;
    }

    public void setResponse(ArrayList<OrderAddressRequest> response) {
        this.response = response;
    }
}
