package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 05-10-2016.
 */

public class StateLookupResponse implements Serializable
{
    @SerializedName("StateID")
    private String StateID;

    @SerializedName("StateDesc")
    private String StateDesc;

    @SerializedName("CountryID")
    private String CountryID;

    public String getStateID() {
        return StateID;
    }

    public void setStateID(String stateID) {
        StateID = stateID;
    }

    public String getStateDesc() {
        return StateDesc;
    }

    public void setStateDesc(String stateDesc) {
        StateDesc = stateDesc;
    }

    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }

    @Override
    public String toString() {
        return this.StateDesc;
    }

    @Override
    public int hashCode() {
        return CountryID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        else if(obj instanceof CountryLookupResponse)
        {
            CountryLookupResponse response = (CountryLookupResponse) obj;
            if(response.getCountryID().equals(this.CountryID))
                return  true;
            else
                return false;
        }
        else
        {
            StateLookupResponse response = (StateLookupResponse) obj;
            if(response.StateID.equals(this.StateID) && response.getStateDesc() == this.StateDesc)
                return  true;
        }
        return false;

    }
}
