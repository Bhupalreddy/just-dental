package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import hCue.Hcue.DAO.NumberInfo;


/**
 * Created by Appdest on 04-06-2016.
 */
public class OTPResponse implements Serializable
{
    @SerializedName("id")
    private String id ;

    @SerializedName("number_info")
    private NumberInfo number_info ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NumberInfo getNumber_info() {
        return number_info;
    }

    public void setNumber_info(NumberInfo number_info) {
        this.number_info = number_info;
    }
}
