package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 01-08-2016.
 */
public class PatientVisitdetailsReq implements Serializable
{
    @SerializedName("PageSize")
    private int PageSize = 10;

    @SerializedName("PageNumber")
    private int PageNumber = 1;

    @SerializedName("PatientCaseID")
    private long PatientCaseID ;

    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("ScreenType")
    private String ScreenType = "VISIT";

    @SerializedName("RowID")
    private int RowID = 1;

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public long getPatientCaseID() {
        return PatientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        PatientCaseID = patientCaseID;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getScreenType() {
        return ScreenType;
    }

    public void setScreenType(String screenType) {
        ScreenType = screenType;
    }

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }
}
