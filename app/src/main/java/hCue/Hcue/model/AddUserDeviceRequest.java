package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Appdest on 10-05-2016.
 */
public class AddUserDeviceRequest {

    @SerializedName("ClientType")
    private String ClientType = "PATIENT";

    @SerializedName("AppType")
    private String AppType = "FAMILYAPP";

    @SerializedName("IDs")
    private long IDs ;

    @SerializedName("UUID")
    private String UUID ;

    public String getClientType() {
        return ClientType;
    }

    public void setClientType(String clientType) {
        ClientType = clientType;
    }

    public long getIDs() {
        return IDs;
    }

    public void setIDs(long IDs) {
        this.IDs = IDs;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }
}
