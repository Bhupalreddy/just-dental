package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 11/17/2016.
 */

public class DeletePatientCaseDocumentImageRequest implements Serializable
{

    @SerializedName("FileExtn")
    private String FileExtn ;

    @SerializedName("UpLoadType")
    private String UpLoadType = "PatientPrescription";

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("DocumentNumber")
    private String DocumentNumber ;

    public String getFileExtn() {
        return FileExtn;
    }

    public void setFileExtn(String fileExtn) {
        FileExtn = fileExtn;
    }

    public String getUpLoadType() {
        return UpLoadType;
    }

    public void setUpLoadType(String upLoadType) {
        UpLoadType = upLoadType;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }
}

