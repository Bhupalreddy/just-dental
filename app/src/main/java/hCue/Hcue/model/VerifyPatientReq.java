package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 07-07-2016.
 */
public class VerifyPatientReq implements Serializable
{

    @SerializedName("PatientLoginID")
    private String PatientLoginID ;

    @SerializedName("PhoneNumber")
    private long PhoneNumber ;

    @SerializedName("SocialID")
    private String SocialID ;

    @SerializedName("SocialIDType")
    private String SocialIDType ;

    public String getPatientLoginID() {
        return PatientLoginID;
    }

    public void setPatientLoginID(String patientLoginID) {
        PatientLoginID = patientLoginID;
    }

    public long getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getSocialID() {
        return SocialID;
    }

    public void setSocialID(String socialID) {
        SocialID = socialID;
    }

    public String getSocialIDType() {
        return SocialIDType;
    }

    public void setSocialIDType(String socialIDType) {
        SocialIDType = socialIDType;
    }
}
