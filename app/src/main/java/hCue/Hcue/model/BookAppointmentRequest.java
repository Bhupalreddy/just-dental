package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 13-06-2016.
 */
public class BookAppointmentRequest implements Serializable
{
    @SerializedName("DoctorVisitRsnID")
    private String DoctorVisitRsnID = "ALLMRDRR";

    @SerializedName("ConsultationDt")
    private String ConsultationDt ;

    @SerializedName("VisitUserTypeID")
    private String VisitUserTypeID = "OPATIENT";

    @SerializedName("AddressConsultID")
    private int AddressConsultID ;

    @SerializedName("AppointmentStatus")
    private String AppointmentStatus = "H";

    @SerializedName("SpecialityCD")
    private String SpecialityCD ;

    @SerializedName("HospitalID")
    private int HospitalID ;

    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("DayCD")
    private String DayCD ;

    @SerializedName("HospitalCD")
    private String HospitalCode = "GOCLINIC";

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("PatientID")
    private long PatientID ;

    public String getDoctorVisitRsnID() {
        return DoctorVisitRsnID;
    }

    public void setDoctorVisitRsnID(String doctorVisitRsnID) {
        DoctorVisitRsnID = doctorVisitRsnID;
    }

    public String getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(String consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getVisitUserTypeID() {
        return VisitUserTypeID;
    }

    public void setVisitUserTypeID(String visitUserTypeID) {
        VisitUserTypeID = visitUserTypeID;
    }

    public int getAddressConsultID() {
        return AddressConsultID;
    }

    public void setAddressConsultID(int addressConsultID) {
        AddressConsultID = addressConsultID;
    }

    public String getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public String getSpecialityCD() {
        return SpecialityCD;
    }

    public void setSpecialityCD(String specialityCD) {
        SpecialityCD = specialityCD;
    }

    public int getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(int hospitalID) {
        HospitalID = hospitalID;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public String getHospitalCode() {
        return HospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        HospitalCode = hospitalCode;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }
}
