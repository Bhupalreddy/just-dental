package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 11/17/2016.
 */

public class ListOrderAddresResponce implements Serializable
{

    @SerializedName("USRType")
    private String USRType ;

    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("PatientAddressID")
    private Integer PatientAddressID ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("Address2")
    private String Address2 ;

    @SerializedName("Address1")
    private String Address1 ;

    @SerializedName("Latitude")
    private String Latitude ;

    @SerializedName("Longitude")
    private String Longitude ;

    @SerializedName("PinCode")
    private long PinCode ;

    @SerializedName("State")
    private String State ;

    @SerializedName("Street")
    private String Street ;

    @SerializedName("Country")
    private String Country ;

    @SerializedName("CityTown")
    private String CityTown ;

    @SerializedName("ActiveIND")
    private String ActiveIND ;

    @SerializedName("Location")
    private String Location ;

    private String PatientName;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public Integer getPatientAddressID() {
        return PatientAddressID;
    }

    public void setPatientAddressID(Integer patientAddressID) {
        PatientAddressID = patientAddressID;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public long getPinCode() {
        return PinCode;
    }

    public void setPinCode(long pinCode) {
        PinCode = pinCode;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCityTown() {
        return CityTown;
    }

    public void setCityTown(String cityTown) {
        CityTown = cityTown;
    }

    public String getActiveIND() {
        return ActiveIND;
    }

    public void setActiveIND(String activeIND) {
        ActiveIND = activeIND;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }
}
