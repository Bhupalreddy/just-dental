package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Appdest on 10-05-2016.
 */
public class AddUserDevice {
    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("AppType")
    private String AppType = "FAMILYAPP";

    @SerializedName("ClientType")
    private String ClientType = "PATIENT";

    @SerializedName("DeviceID")
    private String DeviceID ;

    @SerializedName("UUID")
    private String UUID ;

    @SerializedName("USRId")
    private long USRId;

    @SerializedName("IDs")
    private long[] IDs;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getClientType() {
        return ClientType;
    }

    public void setClientType(String clientType) {
        ClientType = clientType;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public long[] getIDs() {
        return IDs;
    }

    public void setIDs(long[] IDs) {
        this.IDs = IDs;
    }
}
