package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.Rows;


/**
 * Created by User on 10/13/2016.
 */

public class SearchHospitalResponce implements Serializable
{
    @SerializedName("rows")
    public ArrayList<Rows> rows;

    @SerializedName("count")
    public int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Rows> getRows() {
        return rows;
    }

    public void setRows(ArrayList<Rows> rows) {
        this.rows = rows;
    }
}
