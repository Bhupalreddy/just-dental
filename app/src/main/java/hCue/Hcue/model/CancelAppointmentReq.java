package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 29-06-2016.
 */
public class CancelAppointmentReq implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "DOCTOR";

    @SerializedName("AppointmentID")
    private long AppointmentID;

    @SerializedName("AppointmentStatus")
    private char AppointmentStatus = 'C';

    @SerializedName("USRId")
    private long USRId = 0;

    @SerializedName("Reason")
    private String Reason ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public char getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(char appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }
}
