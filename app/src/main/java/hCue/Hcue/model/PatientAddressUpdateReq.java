package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.PatientAddress;


/**
 * Created by Appdest on 10-06-2016.
 */
public class PatientAddressUpdateReq implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("patientDetails")
    private patientDetails patientdetails ;

    @SerializedName("patientAddress")
    private ArrayList<PatientAddress> patientAddressArrayList ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public patientDetails getPatientdetails() {
        return patientdetails;
    }

    public void setPatientdetails(patientDetails patientdetails) {
        this.patientdetails = patientdetails;
    }

    public ArrayList<PatientAddress> getPatientAddressArrayList() {
        return patientAddressArrayList;
    }

    public void setPatientAddressArrayList(ArrayList<PatientAddress> patientAddressArrayList) {
        this.patientAddressArrayList = patientAddressArrayList;
    }

    public class patientDetails implements Serializable
    {
        @SerializedName("TermsAccepted")
        private char TermsAccepted = 'Y';

        @SerializedName("PatientID")
        private long PatientID ;

        public char getTermsAccepted() {
            return TermsAccepted;
        }

        public void setTermsAccepted(char termsAccepted) {
            TermsAccepted = termsAccepted;
        }

        public long getPatientID() {
            return PatientID;
        }

        public void setPatientID(long patientID) {
            PatientID = patientID;
        }
    }
}
