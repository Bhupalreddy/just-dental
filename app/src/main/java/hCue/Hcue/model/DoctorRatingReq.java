package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 29-06-2016.
 */
public class DoctorRatingReq implements Serializable
{
    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("RatingComments")
    private String RatingComments ;

    @SerializedName("AppointmentID")
    private long AppointmentID ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("StarValue")
    private float StarValue ;

    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("FeedbackQuestions")
    private String FeedbackQuestions ;

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getRatingComments() {
        return RatingComments;
    }

    public void setRatingComments(String ratingComments) {
        RatingComments = ratingComments;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public float getStarValue() {
        return StarValue;
    }

    public void setStarValue(float starValue) {
        StarValue = starValue;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public String getFeedbackQuestions() {
        return FeedbackQuestions;
    }

    public void setFeedbackQuestions(String feedbackQuestions) {
        FeedbackQuestions = feedbackQuestions;
    }
}
