package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 10-06-2016.
 */
public class PatientImageUploadReq implements Serializable
{
    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("PatientID")
    private String ImageStr ;

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getImageStr() {
        return ImageStr;
    }

    public void setImageStr(String imageStr) {
        ImageStr = imageStr;
    }
}
