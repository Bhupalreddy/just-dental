package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class GetPastFutureReq implements Serializable
{
    @SerializedName("FamilyHdID")
    private String FamilyHdID ;

    @SerializedName("PageSize")
    private int PageSize = 10;

    @SerializedName("PageNumber")
    private int PageNumber = 0;

    @SerializedName("Sort")
    private String Sort = "asc";

    @SerializedName("BaseDate")
    private String BaseDate ;

    @SerializedName("Count")
    private int Count ;

    @SerializedName("Indicator")
    private char Indicator ;

    public String getFamilyHdID() {
        return FamilyHdID;
    }

    public void setFamilyHdID(String familyHdID) {
        FamilyHdID = familyHdID;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public String getSort() {
        return Sort;
    }

    public void setSort(String sort) {
        Sort = sort;
    }

    public String getBaseDate() {
        return BaseDate;
    }

    public void setBaseDate(String baseDate) {
        BaseDate = baseDate;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public char getIndicator() {
        return Indicator;
    }

    public void setIndicator(char indicator) {
        Indicator = indicator;
    }
}
