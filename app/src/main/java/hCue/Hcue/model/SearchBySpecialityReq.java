package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Appdest on 23-06-2016.
 */
public class SearchBySpecialityReq implements Serializable
{
    @SerializedName("OrderBy")
    private String OrderBy = "ASC";

    @SerializedName("Radius")
    private int Radius = 30000;

    @SerializedName("PageSize")
    private int PageSize = 25;

    @SerializedName("PageNumber")
    private int PageNumber = 0;

    @SerializedName("SpecialityID")
    private String SpecialityID ;

    @SerializedName("Sort")
    private String Sort = "DIST";

    @SerializedName("Latitude")
    private String Latitude = "13.0482419";

    @SerializedName("Count")
    private int Count = 0;

    @SerializedName("Longitude")
    private String Longitude = "80.2446890";

    @SerializedName("DoctorName")
    private String DoctorName ;

    @SerializedName("DoctorIds")
    private String DoctorIds ;

    @SerializedName("Gender")
    private String Gender ;

    @SerializedName("Book")
    private String Book ;

    @SerializedName("Call")
    private String Call ;

    @SerializedName("Available")
    private String Available ;

    @SerializedName("FilterDay")
    private String FilterDay ;

    @SerializedName("FilterDayList")
    private ArrayList<String> FilterDayList;

    @SerializedName("FeeStartRange")
    private Integer FeeStartRange ;

    @SerializedName("FeeEndRange")
    private Integer FeeEndRange ;

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("additionalSearch")
    private HashMap<String, String> additionalSearch = new HashMap<>();

    public String getOrderBy() {
        return OrderBy;
    }

    public void setOrderBy(String orderBy) {
        OrderBy = orderBy;
    }

    public int getRadius() {
        return Radius;
    }

    public void setRadius(int radius) {
        Radius = radius;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public String getSpecialityID() {
        return SpecialityID;
    }

    public void setSpecialityID(String specialityID) {
        SpecialityID = specialityID;
    }

    public String getSort() {
        return Sort;
    }

    public void setSort(String sort) {
        Sort = sort;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String doctorName) {
        DoctorName = doctorName;
    }

    public String getDoctorIds() {
        return DoctorIds;
    }

    public void setDoctorIds(String doctorIds) {
        DoctorIds = doctorIds;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getBook() {
        return Book;
    }

    public void setBook(String book) {
        Book = book;
    }

    public String getCall() {
        return Call;
    }

    public void setCall(String call) {
        Call = call;
    }

    public String getFilterDay() {
        return FilterDay;
    }

    public void setFilterDay(String filterDay) {
        FilterDay = filterDay;
    }

    public Integer getFeeStartRange() {
        return FeeStartRange;
    }

    public void setFeeStartRange(Integer feeStartRange) {
        FeeStartRange = feeStartRange;
    }

    public Integer getFeeEndRange() {
        return FeeEndRange;
    }

    public void setFeeEndRange(Integer feeEndRange) {
        FeeEndRange = feeEndRange;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getAvailable() {
        return Available;
    }

    public void setAvailable(String available) {
        Available = available;
    }

    public ArrayList<String> getFilterDayList() {
        return FilterDayList;
    }

    public void setFilterDayList(ArrayList<String> listSelectedDay) {
        this.FilterDayList = listSelectedDay;
    }

    public HashMap<String, String> getAdditionalSearch() {
        return additionalSearch;
    }

    public void setAdditionalSearch(HashMap<String, String> additionalSearch) {
        this.additionalSearch = additionalSearch;
    }
}
