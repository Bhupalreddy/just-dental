package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 28-06-2016.
 */
public class GetPatientDetailsReq implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "DOCTOR";

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("USRId")
    private int USRId = 0;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public int getUSRId() {
        return USRId;
    }

    public void setUSRId(int USRId) {
        this.USRId = USRId;
    }
}
