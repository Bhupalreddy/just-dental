package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 05-10-2016.
 */

public class HospitalDetailsRequest implements Serializable
{
    @SerializedName("pageNumber")
    private int pageNumber;

    @SerializedName("HospitalID")
    private int HospitalID;

    @SerializedName("pageSize")
    private int pageSize;

    @SerializedName("IncludeDoctors")
    private String IncludeDoctors = "Y";

    @SerializedName("hospitalLocation")
    private String hospitalLocation;

    @SerializedName("IncludeBranch")
    private String IncludeBranch = "N";

    @SerializedName("IncludeServices")
    private String IncludeServices = "Y";


    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(int hospitalID) {
        HospitalID = hospitalID;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getIncludeDoctors() {
        return IncludeDoctors;
    }

    public void setIncludeDoctors(String includeDoctors) {
        IncludeDoctors = includeDoctors;
    }

    public String getHospitalLocation() {
        return hospitalLocation;
    }

    public void setHospitalLocation(String hospitalLocation) {
        this.hospitalLocation = hospitalLocation;
    }

    public String getIncludeBranch() {
        return IncludeBranch;
    }

    public void setIncludeBranch(String includeBranch) {
        IncludeBranch = includeBranch;
    }

    public String getIncludeServices() {
        return IncludeServices;
    }

    public void setIncludeServices(String includeServices) {
        IncludeServices = includeServices;
    }
}
