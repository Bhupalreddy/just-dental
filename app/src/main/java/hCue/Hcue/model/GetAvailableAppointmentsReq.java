package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 23-06-2016.
 */
public class GetAvailableAppointmentsReq implements Serializable
{
    @SerializedName("DoctorID")
    private int DoctorID;

    @SerializedName("filterByDate")
    private String filterByDate;

    @SerializedName("AddressID")
    private int AddressID;

    public char getIncludeWalkIns() {
        return IncludeWalkIns;
    }

    public void setIncludeWalkIns(char includeWalkIns) {
        IncludeWalkIns = includeWalkIns;
    }
    @SerializedName("IncludeWalkIns")
    private char IncludeWalkIns = 'N';

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getFilterByDate() {
        return filterByDate;
    }

    public void setFilterByDate(String filterByDate) {
        this.filterByDate = filterByDate;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int addressID) {
        AddressID = addressID;
    }
}
