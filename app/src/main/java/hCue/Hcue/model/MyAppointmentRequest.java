package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 15/06/16.
 */
public class MyAppointmentRequest implements Serializable
{
    @SerializedName("PageNumber")
    private int PageNumber = 0;

    @SerializedName("BaseDate")
    private String BaseDate ;

    @SerializedName("Indicator")
    private String Indicator = "F";

    @SerializedName("FamilyHdID")
    private long FamilyHdID ;

    @SerializedName("Count")
    private int Count = 0;

    @SerializedName("PageSize")
    private int PageSize = 10;

    @SerializedName("Sort")
    private String Sort = "asc";

    @SerializedName("HospitalCode")
    private String HospitalCode = "RVUYMISZZF";

    //dev  - RVUYMISZZF

    public String getHospitalCode() {
        return HospitalCode;
    }

    public void setHospitalCode(String HospitalCode) {
        HospitalCode = HospitalCode;
    }


    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public String getBaseDate() {
        return BaseDate;
    }

    public void setBaseDate(String baseDate) {
        BaseDate = baseDate;
    }

    public String getIndicator() {
        return Indicator;
    }

    public void setIndicator(String indicator) {
        Indicator = indicator;
    }

    public long getFamilyHdID() {
        return FamilyHdID;
    }

    public void setFamilyHdID(long familyHdID) {
        FamilyHdID = familyHdID;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public String getSort() {
        return Sort;
    }

    public void setSort(String sort) {
        Sort = sort;
    }
}

