package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Appdest on 10-05-2016.
 */
public class getAddUserReponse {

    @SerializedName("ID")
    private int ID ;

    @SerializedName("CrtUSR")
    private int CrtUSR ;

    @SerializedName("DeviceID")
    private String DeviceID ;

    @SerializedName("UUID")
    private String UUID ;

    @SerializedName("ClientType")
    private String ClientType ;

    @SerializedName("CrtUSRType")
    private String CrtUSRType ;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCrtUSR() {
        return CrtUSR;
    }

    public void setCrtUSR(int crtUSR) {
        CrtUSR = crtUSR;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getClientType() {
        return ClientType;
    }

    public void setClientType(String clientType) {
        ClientType = clientType;
    }

    public String getCrtUSRType() {
        return CrtUSRType;
    }

    public void setCrtUSRType(String crtUSRType) {
        CrtUSRType = crtUSRType;
    }
}
