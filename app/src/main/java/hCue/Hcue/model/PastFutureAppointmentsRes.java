package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.PastFutureRow;


/**
 * Created by Appdest on 11-06-2016.
 */
public class PastFutureAppointmentsRes implements Serializable
{
    @SerializedName("rows")
    private ArrayList<PastFutureRow> arrayRows ;

    @SerializedName("count")
    private int count ;

    public ArrayList<PastFutureRow> getArrayRows() {
        return arrayRows;
    }

    public void setArrayRows(ArrayList<PastFutureRow> arrayRows) {
        this.arrayRows = arrayRows;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
