package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.OrderImageDocuments;
import hCue.Hcue.DAO.OrderTextDocuments;
import hCue.Hcue.DAO.PharmaDetails;

/**
 * Created by User on 11/17/2016.
 */

public class AddUpdatePatientOrderRequest implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("PartnerType")
    private String PartnerType ="PHARMA";

    @SerializedName("OrderType")
    private String OrderType = "NORMAL";

    @SerializedName("OrderDate")
    private long OrderDate ;

    @SerializedName("OrderNotes")
    private String OrderNotes ;

    @SerializedName("OrderStatusID")
    private String OrderStatusID ;

    @SerializedName("PrescriptionChioce")
    private String PrescriptionChioce ;

    @SerializedName("DeliveryAddressID")
    private int DeliveryAddressID ;

    @SerializedName("Latitude")
    private String Latitude ;

    @SerializedName("Longitude")
    private String Longitude ;

    @SerializedName("PatientDOB")
    private String PatientDOB ;

    @SerializedName("PatientName")
    private String PatientName ;

    @SerializedName("PatientGender")
    private String PatientGender ;

    @SerializedName("OrderImageDocuments")
    private ArrayList<OrderImageDocuments> orderImageDocuments = new ArrayList<>();

    @SerializedName("OrderTextDocuments")
    private ArrayList<OrderTextDocuments> orderTextDocuments = new ArrayList<>();

    @SerializedName("PharmaID")
    private Integer PharmaID ;

    @SerializedName("PharmaDetails")
    private PharmaDetails PharmaDetails ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getPartnerType() {
        return PartnerType;
    }

    public void setPartnerType(String partnerType) {
        PartnerType = partnerType;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public long getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(long orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderStatusID() {
        return OrderStatusID;
    }

    public void setOrderStatusID(String orderStatusID) {
        OrderStatusID = orderStatusID;
    }

    public String getPrescriptionChioce() {
        return PrescriptionChioce;
    }

    public void setPrescriptionChioce(String prescriptionChioce) {
        PrescriptionChioce = prescriptionChioce;
    }

    public int getDeliveryAddressID() {
        return DeliveryAddressID;
    }

    public void setDeliveryAddressID(int deliveryAddressID) {
        DeliveryAddressID = deliveryAddressID;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public ArrayList<OrderImageDocuments> getOrderImageDocuments() {
        return orderImageDocuments;
    }

    public void setOrderImageDocuments(ArrayList<OrderImageDocuments> orderImageDocuments) {
        this.orderImageDocuments = orderImageDocuments;
    }

    public ArrayList<OrderTextDocuments> getOrderTextDocuments() {
        return orderTextDocuments;
    }

    public void setOrderTextDocuments(ArrayList<OrderTextDocuments> orderTextDocuments) {
        this.orderTextDocuments = orderTextDocuments;
    }

    public String getOrderNotes() {
        return OrderNotes;
    }

    public void setOrderNotes(String orderNotes) {
        OrderNotes = orderNotes;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPatientDOB() {
        return PatientDOB;
    }

    public void setPatientDOB(String patientDOB) {
        PatientDOB = patientDOB;
    }

    public String getPatientGender() {
        return PatientGender;
    }

    public void setPatientGender(String patientGender) {
        PatientGender = patientGender;
    }

    public Integer getPharmaID() {
        return PharmaID;
    }

    public void setPharmaID(Integer pharmaID) {
        PharmaID = pharmaID;
    }

    public hCue.Hcue.DAO.PharmaDetails getPharmaDetails() {
        return PharmaDetails;
    }

    public void setPharmaDetails(hCue.Hcue.DAO.PharmaDetails pharmaDetails) {
        PharmaDetails = pharmaDetails;
    }
}
