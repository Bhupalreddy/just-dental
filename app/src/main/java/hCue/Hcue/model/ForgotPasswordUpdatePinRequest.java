package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 3/15/2016.
 */
public class ForgotPasswordUpdatePinRequest implements Serializable{

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getPatientLoginID() {
        return PatientLoginID;
    }

    public void setPatientLoginID(String patientLoginID) {
        PatientLoginID = patientLoginID;
    }

    public int getUSRId() {
        return USRId;
    }

    public void setUSRId(int USRId) {
        this.USRId = USRId;
    }

    public String getPinPassCode() {
        return PinPassCode;
    }

    public void setPinPassCode(String pinPassCode) {
        PinPassCode = pinPassCode;
    }


    @SerializedName("PinPassCode")
    private String PinPassCode;

    @SerializedName("PatientLoginID")
    private String PatientLoginID;

    @SerializedName("USRId")
    private int USRId;



   }
