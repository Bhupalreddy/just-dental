package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.PatientCaseRow;

/**
 * Created by Appdest on 29-07-2016.
 */
public class PatientVisitRespone implements Serializable
{
    @SerializedName("rows")
    private ArrayList<PatientCaseRow> listPatientCaserows;

    @SerializedName("count")
    private int count ;

    public ArrayList<PatientCaseRow> getListPatientCaserows() {
        return listPatientCaserows;
    }

    public void setListPatientCaserows(ArrayList<PatientCaseRow> listPatientCaserows) {
        this.listPatientCaserows = listPatientCaserows;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
