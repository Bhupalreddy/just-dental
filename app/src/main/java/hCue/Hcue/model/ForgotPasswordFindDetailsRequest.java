package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 3/15/2016.
 */
public class ForgotPasswordFindDetailsRequest {

    @SerializedName("MobileNumber")
    private long MobileNumber;

    @SerializedName("EmailID")
    private String EmailID;

    public long getMobileNumber() {
        return MobileNumber;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public void setMobileNumber(long mobileNumber) {
        MobileNumber = mobileNumber;
    }
}
