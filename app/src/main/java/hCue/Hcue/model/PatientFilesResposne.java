package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.PatientFileRow;

/**
 * Created by Appdest on 04-08-2016.
 */
public class PatientFilesResposne implements Serializable
{
    @SerializedName("rows")
    private ArrayList<PatientFileRow> rows ;

    @SerializedName("count")
    private int count ;

    public ArrayList<PatientFileRow> getRows() {
        return rows;
    }

    public void setRows(ArrayList<PatientFileRow> rows) {
        this.rows = rows;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
