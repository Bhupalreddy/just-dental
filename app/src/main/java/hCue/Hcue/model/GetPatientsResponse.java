package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.PatientRow;


/**
 * Created by Appdest on 04-06-2016.
 */
public class GetPatientsResponse implements Serializable
{

    @SerializedName("rows")
    private ArrayList<PatientRow> patientRows;

    @SerializedName("count")
    private int count ;

    public ArrayList<PatientRow> getPatientRows() {
        return patientRows;
    }

    public void setPatientRows(ArrayList<PatientRow> patientRows) {
        this.patientRows = patientRows;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
