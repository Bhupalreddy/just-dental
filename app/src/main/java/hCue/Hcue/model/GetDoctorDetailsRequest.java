package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 28-06-2016.
 */
public class GetDoctorDetailsRequest implements Serializable
{
    @SerializedName("DoctorID")
    private int DoctorID;

    @SerializedName("USRId")
    private long USRId;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }
}
