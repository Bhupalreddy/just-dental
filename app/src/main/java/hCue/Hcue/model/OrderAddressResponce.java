package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 11/17/2016.
 */

public class OrderAddressResponce implements Serializable
{
    @SerializedName("status")
    private String status ;

    @SerializedName("response")
    private String response ;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
