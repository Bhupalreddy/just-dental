package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 14-05-2016.
 */
public class PatientLookupResponse {

    @SerializedName("doctorSpecialityType")
    private ArrayList<DoctorSpecialityType> specialityTypeArrayList ;

    public ArrayList<DoctorSpecialityType> getSpecialityTypeArrayList() {
        return specialityTypeArrayList;
    }

    public void setSpecialityTypeArrayList(ArrayList<DoctorSpecialityType> specialityTypeArrayList) {
        this.specialityTypeArrayList = specialityTypeArrayList;
    }

    public class DoctorSpecialityType implements Serializable
    {
        @SerializedName("DoctorSpecialityID")
        private String DoctorSpecialityID ;

        @SerializedName("DoctorSpecialityDesc")
        private String DoctorSpecialityDesc ;

        public String getDoctorSpecialityID() {
            return DoctorSpecialityID;
        }

        public void setDoctorSpecialityID(String doctorSpecialityID) {
            DoctorSpecialityID = doctorSpecialityID;
        }

        public String getDoctorSpecialityDesc() {
            return DoctorSpecialityDesc;
        }

        public void setDoctorSpecialityDesc(String doctorSpecialityDesc) {
            DoctorSpecialityDesc = doctorSpecialityDesc;
        }
    }
}
