package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Appdest on 04-06-2016.
 */
public class LoginRequest {

    @SerializedName("PatientLoginID")
    private String PatientLoginID ;

    @SerializedName("PatientPassword")
    private String PatientPassword ;

    @SerializedName("SocialIDType")
    private String SocialIDType ;

    @SerializedName("SocialID")
    private String SocialID ;

    public String getPatientLoginID() {
        return PatientLoginID;
    }

    public void setPatientLoginID(String patientLoginID) {
        PatientLoginID = patientLoginID;
    }

    public String getPatientPassword() {
        return PatientPassword;
    }

    public void setPatientPassword(String patientPassword) {
        PatientPassword = patientPassword;
    }

    public String getSocialIDType() {
        return SocialIDType;
    }

    public void setSocialIDType(String socialIDType) {
        SocialIDType = socialIDType;
    }

    public String getSocialID() {
        return SocialID;
    }

    public void setSocialID(String socialID) {
        SocialID = socialID;
    }
}
