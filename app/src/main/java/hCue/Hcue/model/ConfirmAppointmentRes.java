package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 24-06-2016.
 */
public class ConfirmAppointmentRes implements Serializable
{
    @SerializedName("AddressConsultID")
    private int AddressConsultID ;

    @SerializedName("AppointmentID")
    private long AppointmentID ;

    @SerializedName("AppointmentStatus")
    private char AppointmentStatus ;

    @SerializedName("ConsultationDt")
    private long ConsultationDt ;

    @SerializedName("DayCD")
    private String DayCD ;

    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("DoctorVisitRsnID")
    private String DoctorVisitRsnID ;

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("FirstTimeVisit")
    private char FirstTimeVisit ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("SeqNo")
    private int SeqNo ;

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("TokenNumber")
    private int TokenNumber ;

    @SerializedName("VisitUserTypeID")
    private String VisitUserTypeID ;

    public int getAddressConsultID() {
        return AddressConsultID;
    }

    public void setAddressConsultID(int addressConsultID) {
        AddressConsultID = addressConsultID;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public char getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(char appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public long getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getDoctorVisitRsnID() {
        return DoctorVisitRsnID;
    }

    public void setDoctorVisitRsnID(String doctorVisitRsnID) {
        DoctorVisitRsnID = doctorVisitRsnID;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public char getFirstTimeVisit() {
        return FirstTimeVisit;
    }

    public void setFirstTimeVisit(char firstTimeVisit) {
        FirstTimeVisit = firstTimeVisit;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public int getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(int seqNo) {
        SeqNo = seqNo;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public int getTokenNumber() {
        return TokenNumber;
    }

    public void setTokenNumber(int tokenNumber) {
        TokenNumber = tokenNumber;
    }

    public String getVisitUserTypeID() {
        return VisitUserTypeID;
    }

    public void setVisitUserTypeID(String visitUserTypeID) {
        VisitUserTypeID = visitUserTypeID;
    }
}
