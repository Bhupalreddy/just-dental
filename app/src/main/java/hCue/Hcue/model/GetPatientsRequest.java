package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-06-2016.
 */
public class GetPatientsRequest implements Serializable
{
    @SerializedName("PageSize")
    private int PageSize = 30;

    @SerializedName("PageNumber")
    private int PageNumber = 1;

/*    @SerializedName("AgeOffSet")
    private int AgeOffSet = 10;

    @SerializedName("Sort")
    private String Sort = "asc";*/

    @SerializedName("PhoneNumber")
    private String PhoneNumber ;

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

/*    public int getAgeOffSet() {
        return AgeOffSet;
    }

    public void setAgeOffSet(int ageOffSet) {
        AgeOffSet = ageOffSet;
    }

    public String getSort() {
        return Sort;
    }

    public void setSort(String sort) {
        Sort = sort;
    }*/

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }
}
