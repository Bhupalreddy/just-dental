package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.AppointmentRows;

/**
 * Created by Appdest on 23-06-2016.
 */
public class GetAvailableAppointmentsRes implements Serializable
{
    @SerializedName("count")
    private int count ;

    @SerializedName("rows")
    private ArrayList<AppointmentRows> arrappointmentRows ;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<AppointmentRows> getArrappointmentRows() {
        return arrappointmentRows;
    }

    public void setArrappointmentRows(ArrayList<AppointmentRows> arrappointmentRows) {
        this.arrappointmentRows = arrappointmentRows;
    }
}
