package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.DeliveryAddressDetails;
import hCue.Hcue.DAO.OrderImageDocuments;
import hCue.Hcue.DAO.OrderTextDocuments;
import hCue.Hcue.DAO.PatientPhone;

/**
 * Created by User on 11/17/2016.
 */

public class AddUpdateOrderResponce implements Serializable
{
    @SerializedName("PatientOrderID")
    private int PatientOrderID ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("PartnerType")
    private String PartnerType ;

    @SerializedName("OrderType")
    private String OrderType ;

    @SerializedName("OrderDate")
    private long OrderDate ;

    @SerializedName("OrderStatusID")
    private String OrderStatusID ;

    @SerializedName("PrescriptionChioce")
    private String PrescriptionChioce ;

    @SerializedName("OrderImageDocuments")
    private ArrayList<OrderImageDocuments> orderImageDocuments ;

    @SerializedName("OrderTextDocuments")
    private ArrayList<OrderTextDocuments> orderTextDocuments ;

    @SerializedName("DeliveryAddressDetails")
    private DeliveryAddressDetails deliveryAddressDetails ;

    @SerializedName("Latitude")
    private String Latitude ;

    @SerializedName("Longitude")
    private String Longitude ;

    @SerializedName("PatientName")
    private String PatientName ;

    @SerializedName("OrderNotes")
    private String OrderNotes ;

    @SerializedName("PatientPhone")
    private PatientPhone PatientPhone;

    public int getPatientOrderID() {
        return PatientOrderID;
    }

    public void setPatientOrderID(int patientOrderID) {
        PatientOrderID = patientOrderID;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getPartnerType() {
        return PartnerType;
    }

    public void setPartnerType(String partnerType) {
        PartnerType = partnerType;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public long getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(long orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderStatusID() {
        return OrderStatusID;
    }

    public void setOrderStatusID(String orderStatusID) {
        OrderStatusID = orderStatusID;
    }

    public String getPrescriptionChioce() {
        return PrescriptionChioce;
    }

    public void setPrescriptionChioce(String prescriptionChioce) {
        PrescriptionChioce = prescriptionChioce;
    }

    public ArrayList<OrderImageDocuments> getOrderImageDocuments() {
        return orderImageDocuments;
    }

    public void setOrderImageDocuments(ArrayList<OrderImageDocuments> orderImageDocuments) {
        this.orderImageDocuments = orderImageDocuments;
    }

    public ArrayList<OrderTextDocuments> getOrderTextDocuments() {
        return orderTextDocuments;
    }

    public void setOrderTextDocuments(ArrayList<OrderTextDocuments> orderTextDocuments) {
        this.orderTextDocuments = orderTextDocuments;
    }

    public DeliveryAddressDetails getDeliveryAddressDetails() {
        return deliveryAddressDetails;
    }

    public void setDeliveryAddressDetails(DeliveryAddressDetails deliveryAddressDetails) {
        this.deliveryAddressDetails = deliveryAddressDetails;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getOrderNotes() {
        return OrderNotes;
    }

    public void setOrderNotes(String orderNotes) {
        OrderNotes = orderNotes;
    }
}
