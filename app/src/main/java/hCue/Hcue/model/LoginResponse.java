package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.FamilyMembers;
import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.PatientAddress;
import hCue.Hcue.DAO.PatientEmail;
import hCue.Hcue.DAO.PatientOtherIds;
import hCue.Hcue.DAO.PatientPhone;
import hCue.Hcue.DAO.PatientVitals;


/**
 * Created by Appdest on 04-06-2016.
 */
public class LoginResponse implements Serializable
{
    @SerializedName("Patient")
    private ArrayList<Patient> arrPatients ;

    @SerializedName("FamilyMenbers")
    private  ArrayList<FamilyMembers> listFamilyMembers;

    @SerializedName("PatientPhone")
    private ArrayList<PatientPhone> listPatientPhone ;

    @SerializedName("PatientAddress")
    private  ArrayList<PatientAddress> listPatientAddress;

    @SerializedName("PatientEmail")
    private ArrayList<PatientEmail> listPatientEmail ;

    @SerializedName("PatientOtherIds")
    private  ArrayList<PatientOtherIds> listPatientOtherIds;

    @SerializedName("PatientVitals")
    private PatientVitals patientVitals;

    @SerializedName("PatientImage")
    private String PatientImage ;

    @SerializedName("Identity")
    private String Identity ;

    public ArrayList<Patient> getArrPatients() {
        return arrPatients;
    }

    public void setArrPatients(ArrayList<Patient> arrPatients) {
        this.arrPatients = arrPatients;
    }

    public ArrayList<FamilyMembers> getListFamilyMembers() {
        return listFamilyMembers;
    }

    public void setListFamilyMembers(ArrayList<FamilyMembers> listFamilyMembers) {
        this.listFamilyMembers = listFamilyMembers;
    }

    public ArrayList<PatientPhone> getListPatientPhone() {
        return listPatientPhone;
    }

    public void setListPatientPhone(ArrayList<PatientPhone> listPatientPhone) {
        this.listPatientPhone = listPatientPhone;
    }

    public ArrayList<PatientAddress> getListPatientAddress() {
        return listPatientAddress;
    }

    public void setListPatientAddress(ArrayList<PatientAddress> listPatientAddress) {
        this.listPatientAddress = listPatientAddress;
    }

    public ArrayList<PatientEmail> getListPatientEmail() {
        return listPatientEmail;
    }

    public void setListPatientEmail(ArrayList<PatientEmail> listPatientEmail) {
        this.listPatientEmail = listPatientEmail;
    }

    public ArrayList<PatientOtherIds> getListPatientOtherIds() {
        return listPatientOtherIds;
    }

    public void setListPatientOtherIds(ArrayList<PatientOtherIds> listPatientOtherIds) {
        this.listPatientOtherIds = listPatientOtherIds;
    }

    public PatientVitals getPatientVitals() {
        return patientVitals;
    }

    public void setPatientVitals(PatientVitals patientVitals) {
        this.patientVitals = patientVitals;
    }

    public String getPatientImage() {
        return PatientImage;
    }

    public void setPatientImage(String patientImage) {
        PatientImage = patientImage;
    }

    public String getIdentity() {
        return Identity;
    }

    public void setIdentity(String identity) {
        Identity = identity;
    }
}
