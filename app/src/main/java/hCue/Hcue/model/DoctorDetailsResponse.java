package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.Doctor;
import hCue.Hcue.DAO.DoctorAchievements;
import hCue.Hcue.DAO.DoctorAddress;
import hCue.Hcue.DAO.DoctorCareerImages;
import hCue.Hcue.DAO.DoctorConsultation;
import hCue.Hcue.DAO.DoctorEducation;
import hCue.Hcue.DAO.DoctorEmail;
import hCue.Hcue.DAO.DoctorPhone;
import hCue.Hcue.DAO.DoctorPublishing;
import hCue.Hcue.DAO.DoctorSetting;
import hCue.Hcue.DAO.SpecilityDetailsDO;

/**
 * Created by Appdest on 28-06-2016.
 */
public class DoctorDetailsResponse implements Serializable
{
    @SerializedName("doctor")
    private ArrayList<Doctor> arrdoctors ;
    @SerializedName("doctorPhone")
    private ArrayList<DoctorPhone> arrdoctorPhones ;
    @SerializedName("doctorAddress")
    private ArrayList<DoctorAddress> arrdoctorAddress ;
    @SerializedName("doctorEmail")
    private ArrayList<DoctorEmail> arrdoctorEmail ;
    @SerializedName("doctorConsultation")
    private ArrayList<DoctorConsultation> arrdoctorConsultation;
    @SerializedName("doctorEducation")
    private ArrayList<DoctorEducation> arrdoctorEducation;
    @SerializedName("DoctorPublishing")
    private ArrayList<DoctorPublishing> arrdoctorPublishing;
    @SerializedName("DoctorSetting")
    private DoctorSetting doctorSettings;
    @SerializedName("DoctorAchievements")
    private ArrayList<DoctorAchievements> arrdoctorAchievements;
    @SerializedName("CareerImages")
    private DoctorCareerImages doctorCareerImages;
    /*@SerializedName("SpecilityDetails")
    private SpecilityDetailsDO specilityDetailsDO;
    @SerializedName("Searchable")
    private String Searchable;*/

    public ArrayList<Doctor> getArrdoctors() {
        return arrdoctors;
    }

    public void setArrdoctors(ArrayList<Doctor> arrdoctors) {
        this.arrdoctors = arrdoctors;
    }

    public ArrayList<DoctorPhone> getArrdoctorPhones() {
        return arrdoctorPhones;
    }

    public void setArrdoctorPhones(ArrayList<DoctorPhone> arrdoctorPhones) {
        this.arrdoctorPhones = arrdoctorPhones;
    }

    public ArrayList<DoctorAddress> getArrdoctorAddress() {
        return arrdoctorAddress;
    }

    public void setArrdoctorAddress(ArrayList<DoctorAddress> arrdoctorAddress) {
        this.arrdoctorAddress = arrdoctorAddress;
    }

    public ArrayList<DoctorEmail> getArrdoctorEmail() {
        return arrdoctorEmail;
    }

    public void setArrdoctorEmail(ArrayList<DoctorEmail> arrdoctorEmail) {
        this.arrdoctorEmail = arrdoctorEmail;
    }

    public ArrayList<DoctorConsultation> getArrdoctorConsultation() {
        return arrdoctorConsultation;
    }

    public void setArrdoctorConsultation(ArrayList<DoctorConsultation> arrdoctorConsultation) {
        this.arrdoctorConsultation = arrdoctorConsultation;
    }

    public ArrayList<DoctorEducation> getArrdoctorEducation() {
        return arrdoctorEducation;
    }

    public void setArrdoctorEducation(ArrayList<DoctorEducation> arrdoctorEducation) {
        this.arrdoctorEducation = arrdoctorEducation;
    }

    public ArrayList<DoctorPublishing> getArrdoctorPublishing() {
        return arrdoctorPublishing;
    }

    public void setArrdoctorPublishing(ArrayList<DoctorPublishing> arrdoctorPublishing) {
        this.arrdoctorPublishing = arrdoctorPublishing;
    }

    public DoctorSetting getDoctorSettings() {
        return doctorSettings;
    }

    public void setDoctorSettings(DoctorSetting doctorSettings) {
        this.doctorSettings = doctorSettings;
    }

    public ArrayList<DoctorAchievements> getArrdoctorAchievements() {
        return arrdoctorAchievements;
    }

    public void setArrdoctorAchievements(ArrayList<DoctorAchievements> arrdoctorAchievements) {
        this.arrdoctorAchievements = arrdoctorAchievements;
    }

    public DoctorCareerImages getDoctorCareerImages() {
        return doctorCareerImages;
    }

    public void setDoctorCareerImages(DoctorCareerImages doctorCareerImages) {
        this.doctorCareerImages = doctorCareerImages;
    }

   /* public SpecilityDetailsDO getSpecilityDetailsDO() {
        return specilityDetailsDO;
    }

    public void setSpecilityDetailsDO(SpecilityDetailsDO specilityDetailsDO) {
        this.specilityDetailsDO = specilityDetailsDO;
    }

    public String getSearchable() {
        return Searchable;
    }

    public void setSearchable(String searchable) {
        Searchable = searchable;
    }*/
}
