package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 29-07-2016.
 */
public class PatientsVisitsRequest implements Serializable
{
    @SerializedName("PageSize")
    private int PageSize = 100;

    @SerializedName("PageNumber")
    private int PageNumber = 1;

    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("ScreenType")
    private String ScreenType = "VISIT";

    @SerializedName("MedicineName")
    private String MedicineName;

    @SerializedName("HospitalCD")
    private String HospitalCD="RVUYMISZZF";

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getScreenType() {
        return ScreenType;
    }

    public void setScreenType(String screenType) {
        ScreenType = screenType;
    }

    public String getHospitalCD() {
        return HospitalCD;
    }

    public void setHospitalCD(String HospitalCD) {
        HospitalCD = HospitalCD;
    }

    public String getMedicineName() {
        return MedicineName;
    }

    public void setMedicineName(String medicineName) {
        MedicineName = medicineName;
    }
}
