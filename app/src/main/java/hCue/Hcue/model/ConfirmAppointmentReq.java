package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import hCue.Hcue.DAO.HospitalDetailsDO;

/**
 * Created by Appdest on 24-06-2016.
 */
public class ConfirmAppointmentReq implements Serializable
{
    @SerializedName("AddressConsultID")
    private int AddressConsultID ;

    @SerializedName("AppointmentStatus")
    private char AppointmentStatus = 'B';

    @SerializedName("ConsultationDt")
    private String ConsultationDt ;

    @SerializedName("DayCD")
    private String DayCD ;

    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("DoctorVisitRsnID")
    private String DoctorVisitRsnID = "OTHERS" ;

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("VisitUserTypeID")
    private String VisitUserTypeID = "OPATIENT" ;

    @SerializedName("OtherVisitRsn")
    private String OtherVisitRsn;

    @SerializedName("HospitalDetails")
    private HospitalDetailsDO HospitalDetails ;

    public int getAddressConsultID() {
        return AddressConsultID;
    }

    public void setAddressConsultID(int addressConsultID) {
        AddressConsultID = addressConsultID;
    }

    public char getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(char appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public String getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(String consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getDoctorVisitRsnID() {
        return DoctorVisitRsnID;
    }

    public void setDoctorVisitRsnID(String doctorVisitRsnID) {
        DoctorVisitRsnID = doctorVisitRsnID;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getVisitUserTypeID() {
        return VisitUserTypeID;
    }

    public void setVisitUserTypeID(String visitUserTypeID) {
        VisitUserTypeID = visitUserTypeID;
    }

    public String getOtherVisitRsn() {
        return OtherVisitRsn;
    }

    public void setOtherVisitRsn(String otherVisitRsn) {
        OtherVisitRsn = otherVisitRsn;
    }

    public HospitalDetailsDO getHospitalDetails() {
        return HospitalDetails;
    }

    public void setHospitalDetails(HospitalDetailsDO hospitalDetails) {
        HospitalDetails = hospitalDetails;
    }
}
