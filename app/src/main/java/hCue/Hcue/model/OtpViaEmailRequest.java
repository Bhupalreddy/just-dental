package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CVLHYD-161 on 20-03-2016.
 */
public class OtpViaEmailRequest implements Serializable{
    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public int getUSRId() {
        return USRId;
    }

    public void setUSRId(int USRId) {
        this.USRId = USRId;
    }

    public String getToAddress() {
        return ToAddress;
    }

    public void setToAddress(String toAddress) {
        ToAddress = toAddress;
    }

    public String getMailContent() {
        return MailContent;
    }

    public void setMailContent(String mailContent) {
        MailContent = mailContent;
    }

    @SerializedName("USRType")

    private String USRType = "PATIENT";

    @SerializedName("ToAddress")
    private String ToAddress ;

    @SerializedName("Subject")
    private String Subject = "One Time Password";

    @SerializedName("USRId")
    private int USRId ;

    @SerializedName("MailContent")
    private String MailContent ;

}
