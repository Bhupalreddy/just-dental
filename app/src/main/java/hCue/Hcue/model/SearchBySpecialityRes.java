package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.SpecialityResRow;

/**
 * Created by Appdest on 23-06-2016.
 */
public class SearchBySpecialityRes implements Serializable
{

    @SerializedName("rows")
    private ArrayList<SpecialityResRow> specialityResRowArrayList ;

    @SerializedName("count")
    private int count ;

    public ArrayList<SpecialityResRow> getSpecialityResRowArrayList() {
        return specialityResRowArrayList;
    }

    public void setSpecialityResRowArrayList(ArrayList<SpecialityResRow> specialityResRowArrayList) {
        this.specialityResRowArrayList = specialityResRowArrayList;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
