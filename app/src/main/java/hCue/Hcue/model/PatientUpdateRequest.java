package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.PatientAddress;
import hCue.Hcue.DAO.PatientDetails3;
import hCue.Hcue.DAO.PatientEmail;
import hCue.Hcue.DAO.PatientPhone;
import hCue.Hcue.DAO.PatientVitals;
import hCue.Hcue.DAO.patientLogin;


/**
 * Created by Appdest on 09-06-2016.
 */
public class PatientUpdateRequest implements Serializable
{
    @SerializedName("profileImages")
    private String profileImages ;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("USRId")
    private long USRId ;

/*    @SerializedName("PatientID")
    private long PatientID ;*/

    @SerializedName("patientDetails")
    private PatientDetails3 patientDetails3 ;

    @SerializedName("patientPhone")
    private ArrayList<PatientPhone> listPatientPhone ;

    @SerializedName("patientAddress")
    private  ArrayList<PatientAddress> listPatientAddress;

    @SerializedName("patientEmail")
    private ArrayList<PatientEmail> listPatientEmail ;

    @SerializedName("patientVitals")
    private PatientVitals patientVitals;

    @SerializedName("patientLogin")
    private patientLogin PatientLogin ;

    @SerializedName("sendWelcomeMail")
    private char sendWelcomeMail = 'Y';

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

/*    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }*/

    public PatientDetails3 getPatientDetails3() {
        return patientDetails3;
    }

    public void setPatientDetails3(PatientDetails3 patientDetails3) {
        this.patientDetails3 = patientDetails3;
    }

    public ArrayList<PatientPhone> getListPatientPhone() {
        return listPatientPhone;
    }

    public void setListPatientPhone(ArrayList<PatientPhone> listPatientPhone) {
        this.listPatientPhone = listPatientPhone;
    }

    public ArrayList<PatientAddress> getListPatientAddress() {
        return listPatientAddress;
    }

    public void setListPatientAddress(ArrayList<PatientAddress> listPatientAddress) {
        this.listPatientAddress = listPatientAddress;
    }

    public ArrayList<PatientEmail> getListPatientEmail() {
        return listPatientEmail;
    }

    public void setListPatientEmail(ArrayList<PatientEmail> listPatientEmail) {
        this.listPatientEmail = listPatientEmail;
    }

    public String getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(String profileImages) {
        this.profileImages = profileImages;
    }

        public PatientVitals getPatientVitals() {
        return patientVitals;
    }

    public void setPatientVitals(PatientVitals patientVitals) {
        this.patientVitals = patientVitals;
    }

    public patientLogin getPatientLogin() {
        return PatientLogin;
    }

    public void setPatientLogin(patientLogin patientLogin) {
        this.PatientLogin = patientLogin;
    }
}
