package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Appdest on 10-05-2016.
 */
public class AdditionalFiltersDO implements Serializable{
    private String Gender;

    private String Call = "Y";

    private String Book = "Y";

    private String Availability = "";

    private String MinFee = "";

    private String MaxFee= "";

    private String StartTime= "";

    private String EndTime= "";

    private String selectedDay= "";

    private boolean isBothSelected;

    private String sortType= "ASC";

    private String sortCategory= "DIST";

    private ArrayList<String> listSelectedDay;

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCall() {
        return Call;
    }

    public void setCall(String call) {
        Call = call;
    }

    public String getBook() {
        return Book;
    }

    public void setBook(String book) {
        Book = book;
    }

    public String getAvailability() {
        return Availability;
    }

    public void setAvailability(String availability) {
        Availability = availability;
    }

    public String getMinFee() {
        return MinFee;
    }

    public void setMinFee(String minFee) {
        MinFee = minFee;
    }

    public String getMaxFee() {
        return MaxFee;
    }

    public void setMaxFee(String maxFee) {
        MaxFee = maxFee;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getSelectedDay() {
        return selectedDay;
    }

    public void setSelectedDay(String selectedDay) {
        this.selectedDay = selectedDay;
    }

    public boolean isBothSelected() {
        return isBothSelected;
    }

    public void setBothSelected(boolean bothSelected) {
        isBothSelected = bothSelected;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public String getSortCategory() {
        return sortCategory;
    }

    public void setSortCategory(String sortCategory) {
        this.sortCategory = sortCategory;
    }

    public ArrayList<String> getListSelectedDay() {
        return listSelectedDay;
    }

    public void setListSelectedDay(ArrayList<String> listSelectedDay) {
        this.listSelectedDay = listSelectedDay;
    }
}
