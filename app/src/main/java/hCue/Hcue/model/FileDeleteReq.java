package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-08-2016.
 */
public class FileDeleteReq implements Serializable
{

    @SerializedName("USRType")
    private String USRType = "PATIENT" ;

    @SerializedName("UpLoadType")
    private String UpLoadType = "PatientRecords" ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("FileExtn")
    private String FileExtn = "PATIENT" ;

    @SerializedName("DocumentNumber")
    private String DocumentNumber ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getUpLoadType() {
        return UpLoadType;
    }

    public void setUpLoadType(String upLoadType) {
        UpLoadType = upLoadType;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getFileExtn() {
        return FileExtn;
    }

    public void setFileExtn(String fileExtn) {
        FileExtn = fileExtn;
    }

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }
}
