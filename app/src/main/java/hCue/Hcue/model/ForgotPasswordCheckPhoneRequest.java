package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 3/15/2016.
 */
public class ForgotPasswordCheckPhoneRequest {

    public String getPinPassCode() {
        return PinPassCode;
    }

    public void setPinPassCode(String pinPassCode) {
        PinPassCode = pinPassCode;
    }

    public String getPatientLoginID() {
        return PatientLoginID;
    }

    public void setPatientLoginID(String patientLoginID) {
        PatientLoginID = patientLoginID;
    }

    @SerializedName("PinPassCode")
    private String PinPassCode  ;

    @SerializedName("PatientLoginID")
    private String PatientLoginID ;

}
