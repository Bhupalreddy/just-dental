package hCue.Hcue.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-06-2016.
 */
public class OTPRequest implements Serializable
{

    @SerializedName("USRId")
    private String USRId ;

    @SerializedName("to")
    private String to ;

    @SerializedName("text")
    private String text ;

    @SerializedName("ToAddress")
    private String ToAddress ;

    @SerializedName("MailContent")
    private String MailContent ;

    public String getUSRId() {
        return USRId;
    }

    public void setUSRId(String USRId) {
        this.USRId = USRId;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getToAddress() {
        return ToAddress;
    }

    public void setToAddress(String toAddress) {
        ToAddress = toAddress;
    }

    public String getMailContent() {
        return MailContent;
    }

    public void setMailContent(String mailContent) {
        MailContent = mailContent;
    }
}
