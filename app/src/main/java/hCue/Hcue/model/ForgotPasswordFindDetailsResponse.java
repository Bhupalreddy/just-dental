package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 3/15/2016.
 */
public class ForgotPasswordFindDetailsResponse {

    @SerializedName("DoctorID")
    private long DoctorID;

    public long getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(long doctorID) {
        DoctorID = doctorID;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public long getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getMessage() {

        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    @SerializedName("EmailID")
    private String EmailID;

    @SerializedName("MobileNumber")
    private long MobileNumber;

    @SerializedName("Message")
    private String Message;


}
