package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 10/13/2016.
 */

public class SearchHospitalRequest
{
    @SerializedName("pageNumber")
    private int pageNumber =1;

    @SerializedName("pageSize")
    private int pageSize=50 ;

    @SerializedName("HospitalID")
    private int HospitalID;

    /*@SerializedName("hospitalLocation")
    private String hospitalLocation="MAS";*/

    @SerializedName("IncludeDoctors")
    private String IncludeDoctors="Y";

    @SerializedName("IncludeBranch")
    private String IncludeBranch="N";

    @SerializedName("IncludeServices")
    private String IncludeServices="Y";

    @SerializedName("doctorProfile")
    private String doctorProfile="DOCTOR";

    public String getDoctorProfile() {
        return doctorProfile;
    }

    public void setDoctorProfile(String doctorProfile) {
        this.doctorProfile = doctorProfile;
    }


    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(int hospitalID) {
        HospitalID = hospitalID;
    }

    /*public String getHospitalLocation() {
        return hospitalLocation;
    }

    public void setHospitalLocation(String hospitalLocation) {
        this.hospitalLocation = hospitalLocation;
    }*/

    public String getIncludeDoctors() {
        return IncludeDoctors;
    }

    public void setIncludeDoctors(String includeDoctors) {
        IncludeDoctors = includeDoctors;
    }

    public String getIncludeBranch() {
        return IncludeBranch;
    }

    public void setIncludeBranch(String includeBranch) {
        IncludeBranch = includeBranch;
    }

    public String getIncludeServices() {
        return IncludeServices;
    }

    public void setIncludeServices(String includeServices) {
        IncludeServices = includeServices;
    }
}
