package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hCue.Hcue.DAO.PatientAddress;
import hCue.Hcue.DAO.PatientDetails2;
import hCue.Hcue.DAO.PatientEmail;
import hCue.Hcue.DAO.PatientPhone;
import hCue.Hcue.DAO.PatientVitals;


/**
 * Created by Appdest on 04-06-2016.
 */
public class RegisterRequest implements Serializable
{
    @SerializedName("USRId")
    private long USRId ;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("patientDetails")
    private PatientDetails2 patientDetails2;

    @SerializedName("patientEmail")
    private ArrayList<PatientEmail> patientEmails ;

    @SerializedName("patientAddress")
    private ArrayList<PatientAddress> patientAddresses ;

    @SerializedName("patientPhone")
    private ArrayList<PatientPhone> patientPhones ;

    @SerializedName("patientVitals")
    private PatientVitals patientVitals ;

    @SerializedName("sendWelcomeMail")
    private char sendWelcomeMail = 'Y';

    @SerializedName("profileImages")
    private String profileImages;

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public PatientDetails2 getPatientDetails2() {
        return patientDetails2;
    }

    public void setPatientDetails2(PatientDetails2 patientDetails2) {
        this.patientDetails2 = patientDetails2;
    }

    public ArrayList<PatientEmail> getPatientEmails() {
        return patientEmails;
    }

    public void setPatientEmails(ArrayList<PatientEmail> patientEmails) {
        this.patientEmails = patientEmails;
    }

    public ArrayList<PatientAddress> getPatientAddresses() {
        return patientAddresses;
    }

    public void setPatientAddresses(ArrayList<PatientAddress> patientAddresses) {
        this.patientAddresses = patientAddresses;
    }

    public ArrayList<PatientPhone> getPatientPhones() {
        return patientPhones;
    }

    public void setPatientPhones(ArrayList<PatientPhone> patientPhones) {
        this.patientPhones = patientPhones;
    }

    public PatientVitals getPatientVitals() {
        return patientVitals;
    }

    public void setPatientVitals(PatientVitals patientVitals) {
        this.patientVitals = patientVitals;
    }

    public char getSendWelcomeMail() {
        return sendWelcomeMail;
    }

    public void setSendWelcomeMail(char sendWelcomeMail) {
        this.sendWelcomeMail = sendWelcomeMail;
    }

    public String getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(String profileImages) {
        this.profileImages = profileImages;
    }
}
