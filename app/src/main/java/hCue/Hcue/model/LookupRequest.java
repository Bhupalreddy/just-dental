package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 05-10-2016.
 */

public class LookupRequest implements Serializable
{
    @SerializedName("DoctorID")
    private int DoctorID;

    @SerializedName("ActiveIND")
    private String ActiveIND = "Y";

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getActiveIND() {
        return ActiveIND;
    }

    public void setActiveIND(String activeIND) {
        ActiveIND = activeIND;
    }
}
