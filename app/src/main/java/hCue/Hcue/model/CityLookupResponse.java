package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 05-10-2016.
 */

public class CityLookupResponse implements Serializable
{
    @SerializedName("CityID")
    private String CityID;

    @SerializedName("CityIDDesc")
    private String CityIDDesc;

    @SerializedName("StateID")
    private String StateID;

    public String getCityID() {
        return CityID;
    }

    public void setCityID(String cityID) {
        CityID = cityID;
    }

    public String getCityIDDesc() {
        return CityIDDesc;
    }

    public void setCityIDDesc(String cityIDDesc) {
        CityIDDesc = cityIDDesc;
    }

    public String getStateID() {
        return StateID;
    }

    public void setStateID(String stateID) {
        StateID = stateID;
    }

    @Override
    public String toString() {
        return this.CityIDDesc;
    }

    @Override
    public int hashCode() {
        return StateID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        else if((obj instanceof StateLookupResponse))
        {
            StateLookupResponse response = (StateLookupResponse) obj;
            if(response.getStateID().equals(this.StateID))
                return true;
            else
                return false;
        }

        else
        {
            CityLookupResponse response = (CityLookupResponse) obj;
            if(response.CityID.equals(this.CityID) && response.getCityIDDesc() == this.CityIDDesc)
                return  true;
        }
        return false;

    }
}
