package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 11/17/2016.
 */

public class AddUpdatePatientCaseDocumentImageRequest
{
    @SerializedName("RequestImageURL")
    private String RequestImageURL = "Y";

    @SerializedName("FileExtn")
    private String FileExtn ;

    @SerializedName("UpLoadType")
    private String UpLoadType = "PatientPrescription";

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("ImageStr")
    private String ImageStr ;

    public String getRequestImageURL() {
        return RequestImageURL;
    }

    public void setRequestImageURL(String requestImageURL) {
        RequestImageURL = requestImageURL;
    }

    public String getFileExtn() {
        return FileExtn;
    }

    public void setFileExtn(String fileExtn) {
        FileExtn = fileExtn;
    }

    public String getUpLoadType() {
        return UpLoadType;
    }

    public void setUpLoadType(String upLoadType) {
        UpLoadType = upLoadType;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getImageStr() {
        return ImageStr;
    }

    public void setImageStr(String imageStr) {
        ImageStr = imageStr;
    }
}
