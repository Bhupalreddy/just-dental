package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 14-06-2016.
 */
public class getLabAppointmentsReq implements Serializable
{
    @SerializedName("ConsultationDt")
    private String ConsultationDt ;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("PartnersID")
    private int []PartnersID ;

    @SerializedName("PartnersType")
    private String PartnersType = "LAB";

    @SerializedName("USRId")
    private long USRId = Long.parseLong("9884201159001");

    public String getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(String consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public int[] getPartnersID() {
        return PartnersID;
    }

    public void setPartnersID(int[] partnersID) {
        PartnersID = partnersID;
    }

    public String getPartnersType() {
        return PartnersType;
    }

    public void setPartnersType(String partnersType) {
        PartnersType = partnersType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }
}
