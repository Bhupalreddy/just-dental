package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 3/15/2016.
 */
public class ResetPasswordRequest {

    @SerializedName("PatientCurrentPassword")
    private String PatientCurrentPassword;

    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("PatientNewPassword")
    private String PatientNewPassword ;

    @SerializedName("PinPassCode")
    private String PinPassCode;

    @SerializedName("PatientLoginID")
    private String PatientLoginID ;

    @SerializedName("USRId")
    private int USRId ;

    public String getPatientCurrentPassword() {
        return PatientCurrentPassword;
    }

    public void setPatientCurrentPassword(String patientCurrentPassword) {
        PatientCurrentPassword = patientCurrentPassword;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getPatientNewPassword() {
        return PatientNewPassword;
    }

    public void setPatientNewPassword(String patientNewPassword) {
        PatientNewPassword = patientNewPassword;
    }

    public String getPinPassCode() {
        return PinPassCode;
    }

    public void setPinPassCode(String pinPassCode) {
        PinPassCode = pinPassCode;
    }

    public String getPatientLoginID() {
        return PatientLoginID;
    }

    public void setPatientLoginID(String patientLoginID) {
        PatientLoginID = patientLoginID;
    }

    public int getUSRId() {
        return USRId;
    }

    public void setUSRId(int USRId) {
        this.USRId = USRId;
    }
}
