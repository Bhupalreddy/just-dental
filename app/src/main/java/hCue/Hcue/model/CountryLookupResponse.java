package hCue.Hcue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 05-10-2016.
 */

public class CountryLookupResponse implements Serializable
{
    @SerializedName("CountryID")
    private String CountryID;

    @SerializedName("CountryDesc")
    private String CountryDesc;

    @SerializedName("CountryCode")
    private String CountryCode;

    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }

    public String getCountryDesc() {
        return CountryDesc;
    }

    public void setCountryDesc(String countryDesc) {
        CountryDesc = countryDesc;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    @Override
    public String toString() {
        return this.CountryDesc;
    }

    @Override
    public int hashCode() {
        return CountryID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        else if(!(obj instanceof CountryLookupResponse))
        return false;
        else
        {
            CountryLookupResponse response = (CountryLookupResponse) obj;
            if(response.CountryID.equals(this.CountryID) && response.getCountryCode() == this.CountryCode)
                return  true;
        }
        return false;

    }
}
