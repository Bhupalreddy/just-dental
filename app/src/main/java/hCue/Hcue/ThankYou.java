package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by shyamprasadg on 06/08/16.
 */
public class ThankYou extends BaseActivity
{
    private LinearLayout llThankyou,llContinue;
    AudioManager mode;
    private Vibrator vibrator;
    @Override
    public void initialize()
    {

        mode = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        llThankyou  =   (LinearLayout) getLayoutInflater().inflate(R.layout.thanks_feedback,null,false);
        llBody.addView(llThankyou,baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);

        llContinue = (LinearLayout) llThankyou.findViewById(R.id.llContinue);

        setSpecificTypeFace(llThankyou, ApplicationConstants.WALSHEIM_MEDIUM);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#28465a"));
        }

        llTop.setVisibility(View.GONE);
        flBottomBar.setVisibility(View.GONE);

        llContinue.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent slideactivity = new Intent(ThankYou.this, Specialities.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
                finish();
            }
        });
    }

}
