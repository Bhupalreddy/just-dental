package hCue.Hcue.CallBackInterfaces;

import android.view.View;

import java.util.Calendar;

/**
 * Created by User on 2/18/2017.
 */

public interface ProfileDataInterface
{
    void GeneralUpdate(View v, String gender, String landmark, String address, String stateUpdate, String DOB, Calendar patientDOB);
    void VitalUpdate(View v, String height, String weight, String bloodGroup);
    void ivHelp();
    void OtherUpdate(View v, String contcatperson, String contactPersonPhoNO, String contactPersonEmail, String relation);
}
