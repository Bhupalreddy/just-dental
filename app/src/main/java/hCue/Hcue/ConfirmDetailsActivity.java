package hCue.Hcue;

import android.app.ActivityOptions;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import hCue.Hcue.DAO.RegisterReqSingleton;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.RegisterRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 03/06/16.
 */
public class ConfirmDetailsActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener
{
    private LinearLayout llConfirmDetails,llName,llDob;
    private EditText edtName, edtDob, edtMobileNo;
    private Button btnConfirm;
    private RippleView rvConfirm;
    private Spinner genderspinner;
    private RegisterRequest registerRequest ;
    private boolean isRegisterSucess = false;

    @Override
    public void initialize()
    {
        llConfirmDetails = (LinearLayout) getLayoutInflater().inflate(R.layout.confirm_details,null,false);
        llBody.addView(llConfirmDetails,baseLayoutParams);

        edtName         = (EditText)        llConfirmDetails.findViewById(R.id.edtName);
        edtDob          = (EditText)        llConfirmDetails.findViewById(R.id.edtDob);
        edtMobileNo     = (EditText)        llConfirmDetails.findViewById(R.id.edtMobileNo);

        btnConfirm      = (Button)          llConfirmDetails.findViewById(R.id.btnConfirm);
        genderspinner   = (Spinner)         llConfirmDetails.findViewById(R.id.genderspinner);
        llDob           = (LinearLayout)    llConfirmDetails.findViewById(R.id.llDob);
        llName          = (LinearLayout)    llConfirmDetails.findViewById(R.id.llName);
        llDob           = (LinearLayout)    llConfirmDetails.findViewById(R.id.llDob);

        rvConfirm       = (RippleView)      llConfirmDetails.findViewById(R.id.rvConfirm);

        setSpecificTypeFace(llConfirmDetails,ApplicationConstants.WALSHEIM_MEDIUM);
        btnConfirm.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        tvTopTitle.setText("CONFIRM DETAILS");
        llLeftMenu.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        registerRequest = RegisterReqSingleton.getSingleton();

        edtName.setText(registerRequest.getPatientDetails2().getFullName());
        edtMobileNo.setText(registerRequest.getPatientPhones().get(0).getPhNumber()+"");

        if(registerRequest.getPatientDetails2().getGender() == 'M')
            genderspinner.setSelection(0);
        else
            genderspinner.setSelection(1);

        edtMobileNo.setClickable(false);
        edtMobileNo.setEnabled(false);
        edtMobileNo.setFocusable(false);

        rvConfirm.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView)
            {
                if(edtName.getText().toString().trim().isEmpty())
                {
                    ShowAlertDialog("Please Enter Your Name.");
                }
                else if(edtDob.getTag() == null)
                {
                    ShowAlertDialog("Please select the date of birth.");
                }
                else {
                    registerRequest.getPatientDetails2().setDOB((String) edtDob.getTag());
                    registerRequest.getPatientDetails2().setFullName(edtName.getText().toString());
                    if (genderspinner.getSelectedItemPosition() == 0)
                        registerRequest.getPatientDetails2().setGender('M');
                    else
                        registerRequest.getPatientDetails2().setGender('F');
                    if (Connectivity.isConnected(ConfirmDetailsActivity.this)) {
                        callRegisterService();
                    } else {
                        ShowAlertDialog("Please check internet connection.");
                    }
                }
            }
        });

//        btnConfirm.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                if(edtName.getText().toString().trim().isEmpty())
//                {
//                    ShowAlertDialog("Please Enter Your Name.");
//                }
//                else if(edtDob.getTag() == null)
//                {
//                    ShowAlertDialog("Please select the date of birth.");
//                }
//                else
//                {
//                    registerRequest.getPatientDetails2().setDOB((String) edtDob.getTag());
//                    registerRequest.getPatientDetails2().setFullName(edtName.getText().toString());
//                    if(genderspinner.getSelectedItemPosition()==0)
//                        registerRequest.getPatientDetails2().setGender('M');
//                    else
//                        registerRequest.getPatientDetails2().setGender('F');
//                    if (Connectivity.isConnected(ConfirmDetailsActivity.this))
//                    {
//                        callRegisterService();
//                    }else
//                    {
//                        ShowAlertDialog("Please check internet connection.");
//                    }
//
//
//                }
//            }
//        });

        llDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction())
                {
                    case MotionEvent.ACTION_UP :
                        final Calendar calendar = Calendar.getInstance();
                        int yy = calendar.get(Calendar.YEAR);
                        int mm = calendar.get(Calendar.MONTH);
                        int dd = calendar.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog datePickerDialog = new DatePickerDialog(ConfirmDetailsActivity.this, ConfirmDetailsActivity.this,  yy, mm, dd);
                        //date is dateSetListener as per your code in question

                        //   SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                        datePickerDialog.show();
                        break;
                }
                return true;
            }
        });
    }

    private void callRegisterService()
    {
        showLoader("");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).patientRegister(registerRequest, new RestCallback<LoginResponse>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
                Log.e("ERROR",restError.getErrorMessage());
                ShowAlertDialog("Registration failed.");
            }

            @Override
            public void success(LoginResponse loginResponse, Response response)
            {
                hideLoader();
                LoginResponseSingleton.getSingleton(loginResponse, ConfirmDetailsActivity.this);
                isRegisterSucess = true;
                ShowAlertDialog("Register success.");

            }
        });


    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        populateSetDate(year, monthOfYear+1, dayOfMonth);
    }

    public void populateSetDate(int year, int month, int day) {
        try {


            //  tt=boo.request;
            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat newDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
            Date MyDate = newDateFormat.parse(day + "/" + month + "/" + year);
            newDateFormat.applyPattern("d MMM yyyy");
            String MyDate1 = newDateFormat.format(MyDate);
            newDateFormat1.applyPattern("EEEE");

            String mon="";
            if(month>9){
                mon=String.valueOf(month);
            }else{
                mon="0"+String.valueOf(month);
            }
            edtDob.setTag(year + "-" + mon + "-" + day);
            edtDob.setText(MyDate1);


        }catch (Exception e){
            Log.e("ERROR",e.toString());}
    }

    public void ShowAlertDialog(String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ConfirmDetailsActivity.this);
        View dialogView = LayoutInflater.from(ConfirmDetailsActivity.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        tvInfo.setTextSize(18);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.GONE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
                if(isRegisterSucess) {
                    Preference preference = new Preference(ConfirmDetailsActivity.this);
                    preference.saveBooleanInPreference("ISLOGOUT", false);
                    preference.commitPreference();

                    Intent slideactivity = new Intent(ConfirmDetailsActivity.this, Specialities.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(slideactivity, bndlanimation);
                    finish();
                }
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }
}
