package hCue.Hcue;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.CaseDoctorDetails;
import hCue.Hcue.DAO.DoctorSpecialisation;
import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.DateUtils;

import static android.R.attr.button;

/**
 * Created by shyamprasadg on 30/07/16.
 */
public class MedicationSummary extends BaseActivity
{
    private ScrollView svMedications;
    private TextView tvDoctorname,tvDate,tvTime,tvspeciality,tvClinic,tvMedicineName,tvDuration,tvDosage,tvWhen,tvDisease,tvDescription;
    private PatientCaseRow patientCaseRow ;
    private TextView tvEditeminders, tvContacts;
    private static final int PICK_CONTACT = 11;
    private boolean isTodayRecord;
    @Override
    public void initialize()
    {

        svMedications    =   (ScrollView) getLayoutInflater().inflate(R.layout.medications_summary,null,false);
        llBody.addView(svMedications,baseLayoutParams);

        patientCaseRow      = (PatientCaseRow) getIntent().getSerializableExtra("SELECTEDCASE");
        isTodayRecord       =  getIntent().getBooleanExtra("isTodayRecord", false);

        tvDoctorname        =   (TextView)  svMedications.findViewById(R.id.tvDoctorname);
        tvDate              =   (TextView)  svMedications.findViewById(R.id.tvDate);
        tvTime              =   (TextView)  svMedications.findViewById(R.id.tvTime);
        tvspeciality        =   (TextView)  svMedications.findViewById(R.id.tvspeciality);
        tvClinic            =   (TextView)  svMedications.findViewById(R.id.tvClinic);
        tvMedicineName      =   (TextView)  svMedications.findViewById(R.id.tvMedicineName);
        tvDuration          =   (TextView)  svMedications.findViewById(R.id.tvDuration);
        tvDosage            =   (TextView)  svMedications.findViewById(R.id.tvDosage);
        tvWhen              =   (TextView)  svMedications.findViewById(R.id.tvWhen);
        tvDisease           =   (TextView)  svMedications.findViewById(R.id.tvDisease);
        tvDescription       =   (TextView)  svMedications.findViewById(R.id.tvDescription);
        tvEditeminders      =   (TextView)  svMedications.findViewById(R.id.tvEditeminders);
        tvContacts          =   (TextView)  svMedications.findViewById(R.id.tvContacts);

        setSpecificTypeFace(svMedications, ApplicationConstants.WALSHEIM_MEDIUM);

        tvDuration.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvDosage.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        tvTopTitle.setText("MY MEDICATIONS");
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyy");
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(patientCaseRow.getConsultationDt());
        tvDate.setText(simpleDateFormat.format(calendar.getTime()));
        tvTime.setText(patientCaseRow.getStartTime()+" Hrs");

        CaseDoctorDetails caseDoctorDetails = patientCaseRow.getListDoctorDetails1();
        if(caseDoctorDetails != null)
        {
            if(caseDoctorDetails.getDoctorName().startsWith("Dr"))
                tvDoctorname.setText(caseDoctorDetails.getDoctorName());
            else
                tvDoctorname.setText("Dr."+caseDoctorDetails.getDoctorName());

            tvClinic.setText(caseDoctorDetails.getClinicName());
            StringBuilder specialitybuilder = new StringBuilder();
            for(DoctorSpecialisation doctorSpecialisation : caseDoctorDetails.getListDoctorSpecolisations())
                specialitybuilder.append(doctorSpecialisation.getSepcialityDesc()+", ");

            if(!specialitybuilder.toString().isEmpty())
                tvspeciality.setText(specialitybuilder.toString().substring(0,specialitybuilder.toString().length()-2));
        }

        PatientCasePrescribObj patientCasePrescribObj = patientCaseRow.getPatientprHistory();
        if(patientCasePrescribObj != null)
        {
            tvMedicineName.setText(patientCasePrescribObj.getMedicine());
            tvDuration.setText(patientCasePrescribObj.getNumberofDays()+" Days");
            tvDosage.setText((patientCasePrescribObj.getDosage1().isEmpty()?"0":patientCasePrescribObj.getDosage1()) + "-" + (patientCasePrescribObj.getDosage2().isEmpty()?"0":patientCasePrescribObj.getDosage2()) + "-" + (patientCasePrescribObj.getDosage4().isEmpty()?"0":patientCasePrescribObj.getDosage4()));
            if(checkIsMedicineType(patientCasePrescribObj.getMedicineType()))
            {
                if (!patientCasePrescribObj.isBeforeAfter())
                    tvWhen.setText("After Meal");
                else
                    tvWhen.setText("Before Meal");
            }else {
                tvWhen.setText("--");
            }
            if(patientCasePrescribObj.getDiagnostic() != null)
                tvDescription.setText(""+patientCasePrescribObj.getDiagnostic());
        }
        if (!isTodayRecord) {
            tvEditeminders.setVisibility(View.GONE);
        }
        tvEditeminders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MedicationSummary.this, ReminderUpdateActivity.class);
                in.putExtra("SELECTEDCASE", patientCaseRow);
                startActivity(in);
            }
        });
        tvContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_PICK,  ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT) :
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c =  managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    }
                }
                break;
        }
    }

    public static boolean checkIsMedicineType(String medicineType)
    {
        switch (medicineType.toUpperCase())
        {
            case  "CRE" :
            case  "GEL" :
            case  "INJ" :
            case  "KIT" :
            case  "INH" :
            case  "LOT" :
            case  "OIN" :
            case  "RES" :
            case  "SPR" :
                return false;
            default: return true;
        }
    }
}
