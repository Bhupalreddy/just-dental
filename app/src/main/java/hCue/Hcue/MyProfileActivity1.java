package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hCue.Hcue.CallBackInterfaces.ProfileDataInterface;
import hCue.Hcue.DAO.OrderImageDocuments;
import hCue.Hcue.DAO.PatientAddress;
import hCue.Hcue.DAO.PatientDetails3;
import hCue.Hcue.DAO.PatientVitals;
import hCue.Hcue.DAO.Vitals;
import hCue.Hcue.Fragments.GeneralFragment;
import hCue.Hcue.Fragments.OthersFragment;
import hCue.Hcue.Fragments.VitalsFragment;
import hCue.Hcue.adapters.ExpAdapter;
import hCue.Hcue.model.AddUpdatePatientCaseDocumentImageRequest;
import hCue.Hcue.model.AddUpdatePatientCaseDocumentImageResponce;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.PatientImageUploadReq;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.FileDownloader;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static hCue.Hcue.R.id.publish;
import static hCue.Hcue.R.id.tvGender;


/**
 * Created by shyamprasadg on 06/06/16.
 */
public class MyProfileActivity1 extends BaseActivity implements ProfileDataInterface
{
    private TabLayout tabLayout;
    private RelativeLayout rlbmi,rlProfilePic;
    private ViewPager viewPager;
    private LinearLayout llmyprofile ,llNameUpdate,llBmi,llLogout,llMale,llFemale,llRelation;
    private ImageView ivCamera,ivProfilePic,ivClose,ivbmiClose,ivProfilerPicEdit,ivMale,ivFemale;
    private TextView  tvProfileName, tvTitle;
    private Button btnOplus,btnOminus,btnAplus,btnAminus,btnBplus,btnBminus,btnABplus,btnABminus,btnDont,btnIntrested,btnNotNow;
    private Animation slide_up, slide_down;
    private int selecteid ;
    private LoginResponse loginResponse ;
    private int PICK_IMAGE_REQUEST = 1;
    private int REQUEST_CAMERA = 2, SELECT_FILE = 1;
    private Uri uri;
    String path = Constants.SDCARD ;
    private  LinearLayout  llGeneralpopup,llVitalsPopup,llOthersPopup,llProfilePic;
    private EditText etusername,etDOB,etAddress,etLandmark,etHeight,etWeight,etEContactNo,etEContactName,etEContactEmail,etState;
    private PopupWindow popupWindow;
    private TextView  tvRelationship;
    private TextInputLayout tilDOB,tilAddress,tilState,tilLandmark,tilName,tilHeight,tilWeight,tilEContactNo,tilEContactName,tilEContactEmail;
    private PatientUpdateRequest patientUpdateRequest ;
    private Spinner Realtionspinner;
    private String selectedbloodgrp;
    public  static  final int PICK_CONTACT = 11;
    public String encodedString="";
    public String latitude, longitude;
    public String updatedLatitude = "", updatedLongitude = "";
    String mCurrentPhotoPath;
    private int screenWidth, screenHeight;
    private ViewPagerAdapter adapter;
    @Override
    public void initialize()
    {
        setUpDisplayScreenResolution(MyProfileActivity1.this);
        llmyprofile =   (LinearLayout)  getLayoutInflater().inflate(R.layout.my_profile,null,false);
        llBody.addView(llmyprofile);
        llProfile.setBackgroundResource(R.drawable.menu_selected_bg);
        tvProfile1Highlight.setVisibility(View.VISIBLE);
//        ivProfile1.setImageResource(R.drawable.profile_hov);
//        tvProfile1.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvTopTitle.setText("MY PROFILE");

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        slide_up      = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slide_down    = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);

        tabLayout               = (TabLayout)       llmyprofile.findViewById(R.id.tabs);

        ivProfilePic            = (ImageView)       llmyprofile.findViewById(R.id.ivProfilePic);
        ivbmiClose              = (ImageView)       llmyprofile.findViewById(R.id.ivbmiClose);
        ivProfilerPicEdit       = (ImageView)       llmyprofile.findViewById(R.id.ivProfilerPicEdit);
        ivMale                  = (ImageView)       llmyprofile.findViewById(R.id.ivMale);
        ivFemale                = (ImageView)       llmyprofile.findViewById(R.id.ivFemale);
        ivClose                 = (ImageView)       llmyprofile.findViewById(R.id.ivClose);

        tvProfileName           = (TextView)        llmyprofile.findViewById(R.id.tvProfileName);
        tvTitle                 = (TextView)        llmyprofile.findViewById(R.id.tvTitle);

        llGeneralpopup          = (LinearLayout)    llmyprofile.findViewById(R.id.llGeneralpopup);
        llVitalsPopup           = (LinearLayout)    llmyprofile.findViewById(R.id.llVitalsPopup);
        llOthersPopup           = (LinearLayout)    llmyprofile.findViewById(R.id.llOthersPopup);
        llProfilePic            = (LinearLayout)    llmyprofile.findViewById(R.id.llProfilePic);
        llNameUpdate            = (LinearLayout)    llmyprofile.findViewById(R.id.llNameUpdate);
        llBmi                   = (LinearLayout)    llmyprofile.findViewById(R.id.llBmi);
        llLogout                = (LinearLayout)    llmyprofile.findViewById(R.id.llLogout);
        llMale                  = (LinearLayout)    llmyprofile.findViewById(R.id.llMale);
        llFemale                = (LinearLayout)    llmyprofile.findViewById(R.id.llFemale);
        llRelation              = (LinearLayout)    llmyprofile.findViewById(R.id.llRelation);

        etusername              = (EditText)        llmyprofile.findViewById(R.id.etusername);
        etDOB                   = (EditText)        llmyprofile.findViewById(R.id.etDOB);
        etAddress               = (EditText)        llmyprofile.findViewById(R.id.etAddress);
        etLandmark              = (EditText)        llmyprofile.findViewById(R.id.etLandmark);
        etHeight                = (EditText)        llmyprofile.findViewById(R.id.etHeight);
        etWeight                = (EditText)        llmyprofile.findViewById(R.id.etWeight);
        etEContactNo            = (EditText)        llmyprofile.findViewById(R.id.etEContactNo);
        etEContactName          = (EditText)        llmyprofile.findViewById(R.id.etEContactName);
        etEContactEmail         = (EditText)        llmyprofile.findViewById(R.id.etEContactEmail);
        etState                 = (EditText)        llmyprofile.findViewById(R.id.etState);
        tvRelationship          = (TextView)        llmyprofile.findViewById(R.id.tvRelationship);

        btnOplus                = (Button)          llmyprofile.findViewById(R.id.btnOplus);
        btnOminus               = (Button)          llmyprofile.findViewById(R.id.btnOminus);
        btnAplus                = (Button)          llmyprofile.findViewById(R.id.btnAplus);
        btnAminus               = (Button)          llmyprofile.findViewById(R.id.btnAminus);
        btnBplus                = (Button)          llmyprofile.findViewById(R.id.btnBplus);
        btnBminus               = (Button)          llmyprofile.findViewById(R.id.btnBminus);
        btnABplus               = (Button)          llmyprofile.findViewById(R.id.btnABplus);
        btnABminus              = (Button)          llmyprofile.findViewById(R.id.btnABminus);
        btnDont                 = (Button)          llmyprofile.findViewById(R.id.btnDont);

        rlbmi                   = (RelativeLayout)  llmyprofile.findViewById(R.id.rlbmi);
        rlProfilePic            = (RelativeLayout)  llmyprofile.findViewById(R.id.rlProfilePic);

        tilDOB                  = (TextInputLayout) llmyprofile.findViewById(R.id.tilDOB);
        tilAddress              = (TextInputLayout) llmyprofile.findViewById(R.id.tilAddress);
        tilState                = (TextInputLayout) llmyprofile.findViewById(R.id.tilState);
        tilLandmark             = (TextInputLayout) llmyprofile.findViewById(R.id.tilLandmark);
        tilName                 = (TextInputLayout) llmyprofile.findViewById(R.id.tilName);
        tilHeight               = (TextInputLayout) llmyprofile.findViewById(R.id.tilHeight);
        tilWeight               = (TextInputLayout) llmyprofile.findViewById(R.id.tilWeight);
        tilEContactNo           = (TextInputLayout) llmyprofile.findViewById(R.id.tilEContactNo);
        tilEContactName         = (TextInputLayout) llmyprofile.findViewById(R.id.tilEContactName);
        tilEContactEmail        = (TextInputLayout) llmyprofile.findViewById(R.id.tilEContactEmail);



        llTop.setVisibility(View.GONE);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.GONE);
        setSpecificTypeFace(llmyprofile,ApplicationConstants.WALSHEIM_MEDIUM);
        tvTitle.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etusername.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etDOB.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etAddress.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etState.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etLandmark.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etHeight.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etWeight.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etEContactNo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etEContactName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        etEContactEmail.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        tilDOB.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilAddress.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilState.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilLandmark.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilHeight.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilWeight.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilEContactNo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilEContactName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tilEContactEmail.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        btnOplus.setTag("0");
        btnOminus.setTag("0");
        btnAplus.setTag("0");
        btnAminus.setTag("0");
        btnBplus.setTag("0");
        btnBminus.setTag("0");
        btnABplus.setTag("0");
        btnABminus.setTag("0");
        btnDont.setTag("0");

        loginResponse           = LoginResponseSingleton.getSingleton();
        tvProfileName.setText(loginResponse.getArrPatients().get(0).getFullName());

        prepareRequest();
        Preference preference = new Preference(MyProfileActivity1.this);
        if(loginResponse!= null)
            if(loginResponse.getPatientImage() != null && !loginResponse.getPatientImage().isEmpty())
            {
                Glide.with(MyProfileActivity1.this).load(loginResponse.getPatientImage()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivProfilePic);
                Glide.with(MyProfileActivity1.this).load(loginResponse.getPatientImage()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivProfilerPicEdit);
                preference.saveStringInPreference("PhotoUrl", "");
                preference.commitPreference();
            }
            else
            {   final String PhotoUrl = preference.getStringFromPreference("PhotoUrl", "");
                if (!TextUtils.isEmpty(PhotoUrl))
                    Glide.with(MyProfileActivity1.this).load(PhotoUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivProfilePic);
                else
                {
                    ivProfilePic.setBackgroundDrawable(null);
                    ivProfilerPicEdit.setBackgroundDrawable(null);
                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.profile_picture);
                    ivProfilePic.setImageBitmap(largeIcon);
                    ivProfilerPicEdit.setImageBitmap(largeIcon);
                }
            }
        llLogout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                isFromLogout = true;
                ShowAlertDialog("Alert","Are you sure you want to logout?",R.drawable.alert);
            }
        });
        llNameUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selecteid = v.getId();
                tvTitle.setText("Please enter your name:");
                llTop.setVisibility(View.VISIBLE);
                llSave.setVisibility(View.VISIBLE);
                tvTopTitle.setText("Basic Details");
                flBottomBar.setVisibility(View.GONE);
                llClose.setVisibility(View.VISIBLE);
                llProfilePic.setVisibility(View.VISIBLE);
                llProfilePic.startAnimation(slide_up);
                etusername.setLines(1);
                int maxLength = 32;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(maxLength);
                etusername.setFilters(fArray);
                if(tvProfileName.getText().toString().isEmpty())
                {
                    etusername.setText("");
                }
                else
                    etusername.setText(tvProfileName.getText());

            }
        });
        llClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                hideKeyBoard(v);
                llProfilePic.startAnimation(slide_down);
                llProfilePic.setVisibility(View.GONE);
                llTop.setVisibility(View.GONE);
                llSave.setVisibility(View.VISIBLE);
                flBottomBar.setVisibility(View.VISIBLE);
                llGeneralpopup.setVisibility(View.GONE);
                llVitalsPopup.setVisibility(View.GONE);
                llOthersPopup.setVisibility(View.GONE);
            }
        });
        llSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyBoard(v);
                llGeneralpopup.startAnimation(slide_down);
                llGeneralpopup.setVisibility(View.GONE);
                llVitalsPopup.startAnimation(slide_down);
                llVitalsPopup.setVisibility(View.GONE);
                llOthersPopup.startAnimation(slide_down);
                llOthersPopup.setVisibility(View.GONE);
                llProfilePic.startAnimation(slide_down);
                llProfilePic.setVisibility(View.GONE);
                llTop.setVisibility(View.GONE);
                flBottomBar.setVisibility(View.VISIBLE);
                switch (selecteid)
                {
                    case R.id.llNameUpdate :
                        patientUpdateRequest.getPatientDetails3().setFullName(etusername.getText().toString());
                        patientUpdateRequest.getPatientDetails3().setFirstName(etusername.getText().toString());
                        if(!encodedString.isEmpty())
                        {
                            PatientImageUploadReq imageUploadReq = new PatientImageUploadReq();
                            imageUploadReq.setImageStr(encodedString);
                            patientUpdateRequest.setProfileImages(encodedString);


                        }
                        if (Connectivity.isConnected(MyProfileActivity1.this))
                        {
                            callPatientUpdateService();

                        }else
                        {
                            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                        }
                        break ;

                    case R.id.llGeneralUpdate :
                        if (patientUpdateRequest.getListPatientAddress() == null || patientUpdateRequest.getListPatientAddress().size() == 0){
                            ArrayList<PatientAddress> listPatientAddress = new ArrayList<PatientAddress>();
                            PatientAddress address = new PatientAddress();
                            listPatientAddress.add(address);
                            patientUpdateRequest.setListPatientAddress(listPatientAddress);
                            loginResponse.setListPatientAddress(listPatientAddress);
                        }
                        patientUpdateRequest.getListPatientAddress().get(0).setAddress2(etState.getText().toString());
                        patientUpdateRequest.getListPatientAddress().get(0).setCityTown(checkCityExistOrNot(etState.getTag(R.string.city_tag) != null ? etState.getTag(R.string.city_tag).toString() : ""));
                        patientUpdateRequest.getListPatientAddress().get(0).setState(checkStateExistOrNot(etState.getTag(R.string.state_tag) != null ? etState.getTag(R.string.state_tag).toString() : ""));
                        patientUpdateRequest.getListPatientAddress().get(0).setAddress1(etAddress.getText().toString());
                        patientUpdateRequest.getListPatientAddress().get(0).setLandMark(etLandmark.getText().toString());
                        if (Connectivity.isConnected(MyProfileActivity1.this))
                        {
                            callPatientUpdateService();
                        }else
                        {
                            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                        }
                        break ;

                    case R.id.llVitalUpdate :
                        patientUpdateRequest.getPatientVitals().getVitals().setHGT(etHeight.getText().toString());
                        patientUpdateRequest.getPatientVitals().getVitals().setWGT(etWeight.getText().toString());
                        if (Connectivity.isConnected(MyProfileActivity1.this))
                        {
                            callPatientUpdateService();
                        }else
                        {
                            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                        }
                        break ;

                    case R.id.llOthersUpdate :
                        patientUpdateRequest.getPatientDetails3().getEmergencyInfo().setName(etEContactName.getText().toString());
                        patientUpdateRequest.getPatientDetails3().getEmergencyInfo().setMobileNumber(etEContactNo.getText().toString());
                        patientUpdateRequest.getPatientDetails3().getEmergencyInfo().setRelationship(tvRelationship.getText().toString());

                        if (!etEContactEmail.getText().toString().equalsIgnoreCase("") && !isValidEmail(etEContactEmail.getText().toString())) {

                            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(MyProfileActivity1.this);
                            View dialogView = LayoutInflater.from(MyProfileActivity1.this).inflate(R.layout.home_pickup_dialog, null);
                            dialogBuilder.setView(dialogView);

                            TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
                            final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                            TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                            TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
                            ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

                            tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                            tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                            tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                            tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

                            final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
                            alertDialog.show();

                            tvInfo.setText("Please enter a valid emailid.");
                            tvInfo.setTextSize(18);
                            tvNo.setVisibility(View.GONE);
                            tvYes.setText("OK");

                            tvHeading.setVisibility(View.GONE);
                            ivLogo.setVisibility(View.GONE);

                            tvYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                }
                            });

                            tvNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                }
                            });
                        }else
                        {
                            llOthersPopup.startAnimation(slide_down);
                            llOthersPopup.setVisibility(View.GONE);
                            patientUpdateRequest.getPatientDetails3().getEmergencyInfo().setEmailID(etEContactEmail.getText().toString());
                            if (Connectivity.isConnected(MyProfileActivity1.this))
                            {
                                callPatientUpdateService();
                            } else
                            {
                                ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                            }
                        }
                        break ;
                }
            }

        });

        ivProfilerPicEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String[] items = new String[]{"Take from camera",
                        "Select from gallery"};

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        MyProfileActivity1.this);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        MyProfileActivity1.this, R.layout.select_dialog_item,
                        R.id.text, items);

                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int item)
                            {
                                if (item == 0)
                                {
                                    dispatchTakePictureIntent();
                                } else
                                {
                                    galleryIntent();
                                }
                            }
                        });

                final AlertDialog dialog = builder.show();
            }
        });

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
        changeTabsFont();

        rlbmi.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                switch (event.getAction())
                {
                    case MotionEvent.ACTION_UP:

                        llBmi.startAnimation(slide_down);
                        llBmi.setVisibility(View.GONE);
                        break;
                }

                return true;
            }
        });


    }

    private void prepareRequest()
    {
        patientUpdateRequest = new PatientUpdateRequest();
        patientUpdateRequest.setListPatientAddress(loginResponse.getListPatientAddress());
        patientUpdateRequest.setListPatientEmail(loginResponse.getListPatientEmail());
        patientUpdateRequest.setListPatientPhone(loginResponse.getListPatientPhone());
        patientUpdateRequest.setUSRId(loginResponse.getArrPatients().get(0).getPatientID());

        PatientDetails3 patientDetails3 = new PatientDetails3();
        patientDetails3.setPatientID(loginResponse.getArrPatients().get(0).getPatientID());
        //patientDetails3.setFirstName(loginResponse.getArrPatients().get(0).getFullName());
        //patientDetails3.setAge(loginResponse.getArrPatients().get(0).getAge());
        patientDetails3.setGender(loginResponse.getArrPatients().get(0).getGender()+"");
        Calendar calendar1  = Calendar.getInstance(Locale.getDefault());
        calendar1.setTimeInMillis(loginResponse.getArrPatients().get(0).getDOB());
        patientDetails3.setDOB(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()));

        if(loginResponse.getArrPatients().get(0).getArrPatientOtherDetails() != null && !loginResponse.getArrPatients().get(0).getArrPatientOtherDetails().isEmpty())
            patientDetails3.setOtherDetails(loginResponse.getArrPatients().get(0).getArrPatientOtherDetails().get(0));

        if(loginResponse.getArrPatients().get(0).getEmergencyInfo() != null)
            patientDetails3.setEmergencyInfo(loginResponse.getArrPatients().get(0).getEmergencyInfo());

        if(loginResponse.getPatientVitals() != null)
            patientUpdateRequest.setPatientVitals(loginResponse.getPatientVitals());
        else
        {
            Vitals vitals = new Vitals();
            PatientVitals patientVitals = new PatientVitals();
            patientVitals.setVitals(vitals);
            patientUpdateRequest.setPatientVitals(patientVitals);
        }

        //EmergencyInfo emergencyInfo = new EmergencyInfo();

        patientUpdateRequest.setPatientDetails3(patientDetails3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE && resultCode == RESULT_OK)
        {
            /*beginCrop(data.getData());*/
            CropImage.activity(data.getData())
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setMultiTouchEnabled(true)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setAspectRatio(5,5)
                    .start(MyProfileActivity1.this);
        }
        else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK)
        {
            Bitmap originalImg = null;
            BitmapFactory.Options options=new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            originalImg = setPic();
            if (originalImg != null){
                galleryAddPic();

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                originalImg.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                bytes1 = bytes ;
                File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                uri = Uri.fromFile(destination);
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                CropImage.activity(uri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setAspectRatio(5,5)
                        .start(MyProfileActivity1.this);
            }else{
                Toast.makeText(MyProfileActivity1.this, "Capturing Image failed.", Toast.LENGTH_SHORT).show();
            }

        }else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                LoginResponseSingleton.getSingleton().setPatientImage(getPath(resultUri));

                prepareUploadPrescriptionRequest(resultUri);

                /*try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    ivProfilePic.setImageBitmap(bitmap);
                    ivProfilerPicEdit.setImageBitmap(bitmap);
                    ivMenuProfilePic.setImageBitmap(bitmap);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();


                    encodedString= Base64.encodeToString(b, Base64.DEFAULT);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if (requestCode == PICK_CONTACT && resultCode == RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c =  managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String phone = null, email = null;
                // TODO Fetch other Contact details as you want to use

                String id =c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                String hasPhone =c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (hasPhone.equalsIgnoreCase("1")) {
                    Cursor phones = getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,
                            null, null);
                    phones.moveToFirst();
                    phone = phones.getString(phones.getColumnIndex("data1"));
                    System.out.println("number is:"+phone);
                }

                Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,  null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?", new String[] { id }, null);
                int emailIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);
                if (cursor.moveToFirst()) {
                    email = cursor.getString(emailIdx);
                }


                etEContactNo.setText("");
                etEContactName.setText("");
                etEContactEmail.setText("");

                if (phone != null)
                    etEContactNo.setText(phone.replace("+91",""));
                if (name != null)
                    etEContactName.setText(name);
                if (email != null)
                    etEContactEmail.setText(email);
            }
        }

    }
    File file = null;
    AddUpdatePatientCaseDocumentImageRequest uploadfileRequest = null;
    public void prepareUploadPrescriptionRequest(final Uri tempUri) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                uploadfileRequest = new AddUpdatePatientCaseDocumentImageRequest();
                try {
                    String encodedString="";
                    long fileSizeInMB = 0;
                    final Uri uri = tempUri;
                    String picturePath = getPath(uri);

                    Log.d("PATH", picturePath);
                    file = new File(picturePath);
                    long fileSizeInBytes = file.length();
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    fileSizeInMB = fileSizeInKB / 1024;
                    byte[] bytes1 = new byte[(int) fileSizeInBytes];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes1, 0, bytes1.length);
                        buf.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int index = picturePath.lastIndexOf(".");
                    Log.e("EXTENSION", picturePath.substring(index, picturePath.length()));
                    encodedString = Base64.encodeToString(bytes1, Base64.DEFAULT);

                    Log.e("EXTENSION", picturePath.substring(index, picturePath.length()));
                    uploadfileRequest.setFileExtn(picturePath.substring(index, picturePath.length()));
//                    }

                    uploadfileRequest.setPatientID(loginResponse.getArrPatients().get(0).getPatientID());
                    uploadfileRequest.setImageStr(encodedString);

                    if (fileSizeInMB < 5) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                file.deleteOnExit();
                                hideLoader();

                                Uri uri2 = tempUri;
                                String path = getPath(uri2);
                                callUploadProfilePic(uploadfileRequest, uri2);
                            }
                        });


                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                file.deleteOnExit();
                                hideLoader();
                                uploadfileRequest = null;
                                ShowAlertDialog("Alert!", "Sorry, The file could not be uploaded as it exceeds the size of 5MB.", R.drawable.alert);
                            }
                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    @SuppressLint("NewApi")
    private String getPath(Uri uri)
    {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (isMediaDocument(uri)) {
            final String docId = DocumentsContract.getDocumentId(uri);
            final String[] split = docId.split(":");
            final String type = split[0];
            if ("image".equals(type)) {
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(type)) {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            selection = "_id=?";
            selectionArgs = new String[] {
                    split[1]
            };
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }

    }

    public Uri getImageUri(Bitmap inImage)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(GeneralFragment.getInstance(patientUpdateRequest), "GENERAL");
        adapter.addFragment(VitalsFragment.getInstance(patientUpdateRequest), "VITALS");
        adapter.addFragment(OthersFragment.getInstance(patientUpdateRequest), "OTHERS");
        viewPager.setAdapter(adapter);
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private boolean isValidEmail(String s) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

    private void callPatientUpdateService()
    {
        showLoader("Updating");
        if(patientUpdateRequest.getListPatientAddress() == null || patientUpdateRequest.getListPatientAddress().isEmpty())
        {
            PatientAddress patientAddress = new PatientAddress();
            patientAddress.setPrimaryIND("Y");
            patientAddress.setLandMark("");
            patientAddress.setAddress2("");
            patientAddress.setAddress1("");
            patientAddress.setAddressType("H");
            patientAddress.setCountry("IN");
            patientAddress.setLatitude(updatedLatitude);
            patientAddress.setLongitude(updatedLongitude);
            ArrayList<PatientAddress> arrayAddress = new ArrayList<>(1);
            arrayAddress.add(patientAddress);
            patientUpdateRequest.setListPatientAddress(arrayAddress);
        }else {
            if ((updatedLatitude != null && !updatedLatitude.equalsIgnoreCase("")) && (updatedLongitude != null && !updatedLongitude.equalsIgnoreCase("")))
            patientUpdateRequest.getListPatientAddress().get(0).setLatitude(updatedLatitude);
            patientUpdateRequest.getListPatientAddress().get(0).setLongitude(updatedLongitude);
        }
        if (patientUpdateRequest.getListPatientAddress().get(0).getAddress2() == null) {
            patientUpdateRequest.getListPatientAddress().get(0).setAddress2(" ");
        }
        if (patientUpdateRequest.getListPatientAddress().get(0).getAddress1() == null) {
            patientUpdateRequest.getListPatientAddress().get(0).setAddress1(" ");
        }
        if (patientUpdateRequest.getListPatientAddress().get(0).getPrimaryIND() == null) {
            patientUpdateRequest.getListPatientAddress().get(0).setPrimaryIND("Y");
        }
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).updatePatient(patientUpdateRequest, new RestCallback<LoginResponse>()
        {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
                ShowAlertDialog("Whoops!","Failed to update please try again.",R.drawable.worng);
                encodedString="";
//                Toast.makeText(MyProfileActivity.this,"Failed to update please try again.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(LoginResponse loginResponse1, Response response)
            {
                hideLoader();
                encodedString="";
                if(loginResponse1 !=null && loginResponse1.getArrPatients()!=null && !loginResponse1.getArrPatients().isEmpty())
                {
                    loginResponse = LoginResponseSingleton.getSingleton(loginResponse1, MyProfileActivity1.this);
                    updateProfileUI();
                    Toast.makeText(MyProfileActivity1.this,"Profile updated successfully.", Toast.LENGTH_SHORT).show();
                    //ShowAlertDialog(head,msg,resource);
                }
                else
                {
                    ShowAlertDialog("Whoops!","Failed to update.",R.drawable.worng);
                }
            }
        });
    }

    private void updateProfileUI()
    {
        try {
            switch (selecteid)
            {
                case R.id.llNameUpdate :
                    tvProfileName.setText(loginResponse.getArrPatients().get(0).getFullName());
                    Preference preference = new Preference(MyProfileActivity1.this);
                    if(LoginResponseSingleton.getSingleton() != null)
                        if(LoginResponseSingleton.getSingleton() != null)
                            if(LoginResponseSingleton.getSingleton().getPatientImage() != null && !LoginResponseSingleton.getSingleton().getPatientImage().isEmpty())
                            {
//                                Picasso.with(MyProfileActivity1.this).load(LoginResponseSingleton.getSingleton().getPatientImage()).into(ivProfilePic);
//                                preference.saveStringInPreference("PhotoUrl", "");
//                                preference.commitPreference();
                            }
                            else
                            {   final String PhotoUrl = preference.getStringFromPreference("PhotoUrl", "");
                                if (!TextUtils.isEmpty(PhotoUrl)) {
//                                    Picasso.with(MyProfileActivity1.this).load(PhotoUrl).into(ivProfilerPicEdit);
//                                    Picasso.with(MyProfileActivity1.this).load(PhotoUrl).into(ivProfilePic);
                                }
                                else
                                {
                                    ivProfilePic.setBackgroundDrawable(null);
                                    ivProfilerPicEdit.setBackgroundDrawable(null);
                                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.male_selected);
                                    ivProfilerPicEdit.setImageBitmap(largeIcon);
                                    ivProfilePic.setImageBitmap(largeIcon);
                                }
                            }
                    break ;
                case R.id.llGeneralUpdate :

                    Fragment  fragment2 =  adapter.getItem(0);
                    ((GeneralFragment) fragment2).setdata(loginResponse);
                    break;
                case R.id.llVitalUpdate :
                    Fragment  fragment1 =  adapter.getItem(1);
                    ((VitalsFragment) fragment1).setdata(loginResponse.getPatientVitals().getVitals().getHGT(),loginResponse.getPatientVitals().getVitals().getWGT(),loginResponse.getPatientVitals().getVitals().getBLG(),loginResponse.getPatientVitals());
                    break;
                case R.id.llOthersUpdate:
                   Fragment  fragment =  adapter.getItem(2);
                    ((OthersFragment) fragment).setdata(loginResponse.getArrPatients().get(0).getEmergencyInfo());
                         break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Preference preference = new Preference(mContext);
        String address = preference.getStringFromPreference("PROFILE_ADDRESS","");
        String latLng = preference.getStringFromPreference("PROFILE_LATLONG","");
        String cityName = preference.getStringFromPreference("CITY_NAME","");
        String stateName = preference.getStringFromPreference("STATE","");
        if (address != null && !address.equalsIgnoreCase("")){
            etState.setText(address);
            etState.setTag(R.string.city_tag, cityName);
            etState.setTag(R.string.state_tag, stateName);
        }

        if (latLng != null && !latLng.equalsIgnoreCase("")){
            updatedLatitude = latLng.split(",")[0];
            updatedLongitude = latLng.split(",")[1];
        }

        preference.saveStringInPreference("PROFILE_ADDRESS","");
        preference.saveStringInPreference("PROFILE_LATLONG","");

    }

        @Override
        public void onBackPressed()
        {
            Intent slideactivity = new Intent(MyProfileActivity1.this, Specialities.class);
            Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
            slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(slideactivity, bndlanimation);
            finish();
        }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,"com.example.android.fileprovider",photoFile);

                List<ResolveInfo> resolvedIntentActivities = getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    //add photo to gallery
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
    private Bitmap setPic() {
        // Get the dimensions of the View
        /*int targetW = *//*imageView.getWidth()*//*500;
        int targetH = *//*imageView.getHeight()*//*500;*/

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor;

        // Determine how much to scale down the image
        try {
            scaleFactor = Math.min(photoW/screenWidth, photoH/screenHeight);
        }catch (Exception e){
            e.printStackTrace();
            scaleFactor = 1;
        }


        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        Log.d(mCurrentPhotoPath,"Resolution : "+photoW+"x"+photoH);
        return bitmap;
    }

    public void setUpDisplayScreenResolution(Context context) {
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        screenWidth = display.widthPixels;
        screenHeight = display.heightPixels;
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }


    @Override
    public void OtherUpdate(View v, String contcatperson, String contactPersonPhoNO, String contactPersonEmail, String relation)
    {
        selecteid = v.getId();
        llOthersPopup.setVisibility(View.VISIBLE);
        llOthersPopup.startAnimation(slide_up);
        llTop.setVisibility(View.VISIBLE);
        llSave.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);
        tvTopTitle.setText("Other Details");
        llClose.setVisibility(View.VISIBLE);
        etEContactName.setText(contcatperson);
        etEContactNo.setText(contactPersonPhoNO);
        etEContactEmail.setText(contactPersonEmail);
        tvRelationship.setText(relation);
        llRelation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String[] items = {"Father", "Mother","Brother","Sister","GrandFather","GrandMother","Husband","Wife","Friend" ,"Others"};
                new AlertDialog.Builder(MyProfileActivity1.this)
                        .setTitle("Select Relationship")
                        .setSingleChoiceItems(items, 0, null)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                                tvRelationship.setText(items[selectedPosition]);

                                patientUpdateRequest.getPatientDetails3().getEmergencyInfo().setRelationship(items[selectedPosition]);
                                            /*if (Connectivity.isConnected(MyProfileActivity.this))
                                            {
                                                callPatientUpdateService();
                                            }else
                                            {
                                                ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                            }*/
                            }
                        })
                        .show();



            }
        });

        etEContactNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_PICK,  ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
    }

    @Override
    public void GeneralUpdate(View v, String gender, String landmark, String address, String stateUpdate, String DOB, final Calendar patientDOB)
    {
        selecteid = v.getId();
        llGeneralpopup.setVisibility(View.VISIBLE);
        llGeneralpopup.startAnimation(slide_up);
        llTop.setVisibility(View.VISIBLE);
        llSave.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);
        tvTopTitle.setText("General Details");
        llClose.setVisibility(View.VISIBLE);

        if(gender.equalsIgnoreCase("Male"))
        {
            ivMale.setImageResource(R.drawable.male_selected);
            ivFemale.setImageResource(R.drawable.female_unselected);
        }
        else
        {
            ivMale.setImageResource(R.drawable.male_unselected);
            ivFemale.setImageResource(R.drawable.female_selected);
        }
        etLandmark.setText(landmark);
        etAddress.setText(address);
        etState.setText(stateUpdate);
        etDOB.setText(DOB);


        llMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ivMale.setImageResource(R.drawable.male_selected);
                ivFemale.setImageResource(R.drawable.female_unselected);
                patientUpdateRequest.getPatientDetails3().setGender("M");
            }
        });
        llFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ivFemale.setImageResource(R.drawable.female_selected);
                ivMale.setImageResource(R.drawable.male_unselected);
                patientUpdateRequest.getPatientDetails3().setGender("F");
            }
        });
        etState.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent in = new Intent(MyProfileActivity1.this, LocationSearchActivity.class);
                in.putExtra("isFromProfile", true);
                startActivity(in);

            }
        });

        etDOB.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction())
                {
                    case MotionEvent.ACTION_UP :
                        final Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        calendar.setTime(patientDOB.getTime());
                        int yy = calendar.get(Calendar.YEAR);
                        int mm = calendar.get(Calendar.MONTH);
                        int dd = calendar.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {

                                SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat newDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                                try {
                                    Date MyDate = newDateFormat.parse(day + "/" + (month+1) + "/" + year);
                                    patientDOB.setTime(MyDate);
                                    newDateFormat.applyPattern("yyy-MM-dd");
                                    patientUpdateRequest.getPatientDetails3().setDOB(newDateFormat.format(MyDate));
                                    etDOB.setText(newDateFormat1.format(MyDate));

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        };
                        DatePickerDialog datePickerDialog = new DatePickerDialog(MyProfileActivity1.this, onDateSetListener,  yy, mm, dd);
                        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                dialog.dismiss();
                            }
                        });
                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                        datePickerDialog.show();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void VitalUpdate(View v, String height, String weight, String bloodGroup) {
        selecteid = v.getId();
        llVitalsPopup.setVisibility(View.VISIBLE);
        llVitalsPopup.startAnimation(slide_up);
        llTop.setVisibility(View.VISIBLE);
        llSave.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);
        tvTopTitle.setText("Vital Details");
        llClose.setVisibility(View.VISIBLE);

        etHeight.setText(height);
        etWeight.setText(weight);
        if(btnOplus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnOplus.getText().toString();
            btnOplus.setTag("Selected");
            btnOplus.setTextColor(getResources().getColor(R.color.white));
            btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("O+");
        }
        else if(btnOminus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnOminus.getText().toString();
            btnOminus.setTag("Selected");
            btnOminus.setTextColor(getResources().getColor(R.color.white));
            btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("O-");
        }
        else if(btnABplus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnABplus.getText().toString();
            btnABplus.setTag("Selected");
            btnABplus.setTextColor(getResources().getColor(R.color.white));
            btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("AB+");
        }
        else if(btnABminus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnABminus.getText().toString();
            btnABminus.setTag("Selected");
            btnABminus.setTextColor(getResources().getColor(R.color.white));
            btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("AB-");
        }
        else if(btnAplus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnAplus.getText().toString();
            btnAplus.setTag("Selected");
            btnAplus.setTextColor(getResources().getColor(R.color.white));
            btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("A+");
        }
        else if(btnAminus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnOplus.getText().toString();
            btnAminus.setTag("Selected");
            btnAminus.setTextColor(getResources().getColor(R.color.white));
            btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("A-");
        }
        else if(btnBplus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnOplus.getText().toString();
            btnBplus.setTag("Selected");
            btnBplus.setTextColor(getResources().getColor(R.color.white));
            btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("B+");
        }
        else if(btnBminus.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnOplus.getText().toString();
            btnBminus.setTag("Selected");
            btnBminus.setTextColor(getResources().getColor(R.color.white));
            btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("B-");
        }
        else if(btnDont.getText().toString().equalsIgnoreCase(bloodGroup))
        {
            selectedbloodgrp = btnOplus.getText().toString();
            btnDont.setTag("Selected");
            btnDont.setTextColor(getResources().getColor(R.color.white));
            btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
            patientUpdateRequest.getPatientVitals().getVitals().setBLG("N/A");
        }

        btnOplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                selectedbloodgrp = btnOplus.getText().toString();
                btnOplus.setTag("Selected");
                btnOplus.setTextColor(getResources().getColor(R.color.white));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("O+");


                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));





            }
        });


        btnOminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnOminus.getText().toString();

                btnOminus.setTag("Selected");
                btnOminus.setTextColor(getResources().getColor(R.color.white));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("O-");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));





            }
        });

        btnAplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnAplus.getText().toString();

                btnAplus.setTag("Selected");
                btnAplus.setTextColor(getResources().getColor(R.color.white));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("A+");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnAminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnAminus.getText().toString();
                btnAminus.setTag("Selected");
                btnAminus.setTextColor(getResources().getColor(R.color.white));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("A-");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnBplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnBplus.getText().toString();
                btnBplus.setTag("Selected");
                btnBplus.setTextColor(getResources().getColor(R.color.white));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("B+");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnBminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnBminus.getText().toString();
                btnBminus.setTag("Selected");
                btnBminus.setTextColor(getResources().getColor(R.color.white));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("B-");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });


        btnABplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnABplus.getText().toString();
                btnABplus.setTag("Selected");
                btnABplus.setTextColor(getResources().getColor(R.color.white));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("AB+");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnABminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnABminus.getText().toString();
                btnABminus.setTag("Selected");
                btnABminus.setTextColor(getResources().getColor(R.color.white));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("AB-");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });
        btnDont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnDont.getText().toString();
                btnDont.setTag("Selected");
                btnDont.setTextColor(getResources().getColor(R.color.white));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));
                patientUpdateRequest.getPatientVitals().getVitals().setBLG("N/A");

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });
    }

    @Override
    public void ivHelp() {
        tvTitle.setText("What is BMI?");
        llBmi.setVisibility(View.VISIBLE);
        llBmi.startAnimation(slide_up);
    }
    public String checkCityExistOrNot(String cityName){
        boolean isCityExist = false;
        if (cityName != null && !cityName.equalsIgnoreCase("")){
            for (int i = 0; i < ApplicationConstants.listCityDO.size(); i++){
                if (cityName.equalsIgnoreCase(ApplicationConstants.listCityDO.get(i).getCityIDDesc())){
                    cityName = ApplicationConstants.listCityDO.get(i).getCityID();
                    isCityExist = true;
                }
                if (isCityExist){
                    break;
                }
            }
        }else{
            cityName = patientUpdateRequest.getListPatientAddress().get(0).getCityTown();
        }
        return cityName;
    }

    public String checkStateExistOrNot(String stateName){
        boolean isStateExist = false;
        if (stateName != null && !stateName.equalsIgnoreCase("")){
            for (int i = 0; i < ApplicationConstants.listStatesDO.size(); i++){
                if (stateName.equalsIgnoreCase(ApplicationConstants.listStatesDO.get(i).getStateDesc())){
                    stateName = ApplicationConstants.listStatesDO.get(i).getStateID();
                    isStateExist = true;
                }
                if (isStateExist){
                    break;
                }
            }
        }else{
            stateName = patientUpdateRequest.getListPatientAddress().get(0).getState();
        }
        return stateName;
    }
    public void callUploadProfilePic(final AddUpdatePatientCaseDocumentImageRequest request, final Uri uri) {

        showLoader("");
        final File photoFile = new File(uri.getPath());
        TypedFile image = new TypedFile("image/*", photoFile);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).uploadProfilePic(image, request.getFileExtn(), request.getPatientID()+"", new RestCallback<AddUpdatePatientCaseDocumentImageResponce>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(final AddUpdatePatientCaseDocumentImageResponce responce1, Response response) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Preference preference = new Preference(MyProfileActivity1.this);
                        new DownloadFile().execute(responce1.getImageURL(), System.currentTimeMillis()+"profilePic.png");
                        preference.saveStringInPreference("PhotoUrl", responce1.getImageURL());
                        preference.commitPreference();
                    }
                }, 5000);
            }
        });

    }
    public class DownloadFile extends AsyncTask<String, Void, Void> {
        private ProgressDialog dialog;
        String filename_;
        File pdfFile = null;

        @Override
        protected void onPreExecute() {}

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];
            filename_ = fileName;
            // -> maven.pdf

            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ProfilePic");
            if (!folder.exists()) {
                folder.mkdirs();
            }
            pdfFile = new File(folder, fileName);

            try {
                if(pdfFile.createNewFile()){
                    Log.e("", "File created");
                }else {
                    Log.e("", "File doesn't created");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            filename_ = pdfFile.getPath();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideLoader();
            Glide.with(MyProfileActivity1.this).load(pdfFile)
                    .signature(new StringSignature(UUID.randomUUID().toString())).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivProfilePic);
            Glide.with(MyProfileActivity1.this).load(pdfFile).signature(new StringSignature(UUID.randomUUID().toString())).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivProfilerPicEdit);


        }

        }
}
