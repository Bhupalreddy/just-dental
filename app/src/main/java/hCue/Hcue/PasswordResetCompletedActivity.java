package hCue.Hcue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import hCue.Hcue.utils.ApplicationConstants;


/**
 * Created by CVLHYD-161 on 13-03-2016.
 */
public class PasswordResetCompletedActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView tvContinue;
    BaseActivity baseActivity;
    private LinearLayout llResetPasswordCompleted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myaccnt_passwd_success);

        llResetPasswordCompleted = (LinearLayout) findViewById(R.id.llResetPasswordCompleted);
        tvContinue   = (TextView)findViewById(R.id.tvContinue);
        tvContinue.setOnClickListener(this);

        BaseActivity.setSpecificTypeFace(llResetPasswordCompleted, ApplicationConstants.WALSHEIM_MEDIUM);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvContinue){
            Intent in = new Intent(PasswordResetCompletedActivity.this, Login.class);
            startActivity(in);
            finish();

        }
    }

}
