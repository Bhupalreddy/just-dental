package hCue.Hcue.Fragments;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.DAO.CaseDoctorDetails;
import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.MedicationSummary;
import hCue.Hcue.R;
import hCue.Hcue.ReminderManager;
import hCue.Hcue.model.PatientVisitRespone;
import hCue.Hcue.model.PatientVisitdetailsReq;
import hCue.Hcue.model.PatientsVisitsRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LoginResponseSingleton;
import retrofit.client.Response;

/**
 * Created by User on 2/17/2017.
 */

public class MedicationsFragment extends BaseFragment {
    private ArrayList<PatientCaseRow> listPatientCaseRow;
    private MedicationsAdapter medicationsAdapter;
    private ListView lvMedications;
    private LinearLayout llMedications;
    private TextView tvMedications;
    private long patinetid;
    private ImageView ivMedications;
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String TIME_FORMAT = "hh:mm a";
    //    private RemindersDbAdapter mDbHelper;
    int count = 1;
    public static MedicationsFragment getInstance(long patinetid)
    {
        MedicationsFragment medicationsFragment1 = new MedicationsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PATIENTID", patinetid);
        medicationsFragment1.setArguments(bundle);
        return medicationsFragment1;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listPatientCaseRow = new ArrayList<>();
        patinetid = (long) getArguments().getSerializable("PATIENTID");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Medications = inflater.inflate(R.layout.common_list, container, false);
        lvMedications = (ListView) Medications.findViewById(R.id.lvCommon);
        llMedications = (LinearLayout) Medications.findViewById(R.id.llAvailability);
        tvMedications = (TextView) Medications.findViewById(R.id.tvNotAvailable);
        tvMedications.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        ivMedications = (ImageView) Medications.findViewById(R.id.ivIcon);
        final EditText edtfilter = (EditText) Medications.findViewById(R.id.edtfilter);
        LinearLayout llclose = (LinearLayout) Medications.findViewById(R.id.llclose);
        LinearLayout llSearchLocation = (LinearLayout) Medications.findViewById(R.id.llSearchLocation);

        mcontext.setSpecificTypeFace((ViewGroup) Medications, ApplicationConstants.WALSHEIM_MEDIUM);

        medicationsAdapter = new MedicationsAdapter(getActivity(), listPatientCaseRow);
        lvMedications.setAdapter(medicationsAdapter);
        if (Connectivity.isConnected(getActivity())) {
            callgetUservisits(medicationsAdapter, llSearchLocation);
        } else {
            mcontext.ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
        }

        edtfilter.setText("");

        edtfilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase();
                if (medicationsAdapter != null && medicationsAdapter.getListPatientCaserows() != null) {
                    medicationsAdapter.getListPatientCaserows().clear();
                    medicationsAdapter.getListPatientCaserows().addAll(listPatientCaseRow);
                    Iterator<PatientCaseRow> iterator = medicationsAdapter.getListPatientCaserows().iterator();
                    while (iterator.hasNext()) {
                        PatientCaseRow patientCaseRow = iterator.next();
                        if (patientCaseRow.getMedicine().toLowerCase().contains(str) || patientCaseRow.getListDoctorDetails1().getDoctorName().toLowerCase().contains(str)) {
                        } else {
                            iterator.remove();
                        }
                    }

                    if (medicationsAdapter.getListPatientCaserows().size() == 0){
                        llMedications.setVisibility(View.VISIBLE);
                        tvMedications.setText("No medications to display.");
                        ivMedications.setBackgroundResource(R.drawable.tablet);
                    }else{
                        llMedications.setVisibility(View.GONE);
                        medicationsAdapter.notifyDataSetChanged();
                    }


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        llclose.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    edtfilter.setText("");
                    edtfilter.setText("");
                    if (medicationsAdapter != null && medicationsAdapter.getListPatientCaserows() != null) {
                        medicationsAdapter.getListPatientCaserows().clear();
                        medicationsAdapter.getListPatientCaserows().addAll(listPatientCaseRow);
                        medicationsAdapter.notifyDataSetChanged();
                    }
                    return true;
                }
                return false;
            }
        });

        return Medications;
    }
    private void callgetUservisits(final MedicationsAdapter medicationsAdapter, final LinearLayout llSearchLocation) {
        //showLoader("");
        PatientsVisitsRequest visitsRequest = new PatientsVisitsRequest();
        visitsRequest.setPatientID(patinetid);
        visitsRequest.setHospitalCD("RVUYMISZZF");
        visitsRequest.setScreenType("MEDICINE");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientsVisits(visitsRequest, new RestCallback<PatientVisitRespone>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                mcontext.ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
            }

            @Override
            public void success(PatientVisitRespone patientVisitRespone, Response response) {
                hideLoader();
                if (patientVisitRespone != null && patientVisitRespone.getListPatientCaserows() != null && !patientVisitRespone.getListPatientCaserows().isEmpty()) {
                    llMedications.setVisibility(View.GONE);
                    medicationsAdapter.refreshAdapter(patientVisitRespone.getListPatientCaserows());
                    if (patientVisitRespone.getListPatientCaserows().size() > 7)
                        llSearchLocation.setVisibility(View.VISIBLE);
                } else {
                    llMedications.setVisibility(View.VISIBLE);
                    tvMedications.setText("No medications to display.");
                    ivMedications.setBackgroundResource(R.drawable.tablet);
                }
            }
        });
    }

    @Override
    public void setupListeners() {

    }

    public class MedicationsAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<PatientCaseRow> listPatientCaserows;
        private ArrayList<PatientCaseRow> originallistPatientCaserows;

        public MedicationsAdapter(Context context, ArrayList<PatientCaseRow> listPatientCaseRow) {
            this.context = context;
            this.originallistPatientCaserows = listPatientCaseRow;
        }


        @Override
        public int getCount() {
            if (listPatientCaserows == null || listPatientCaserows.isEmpty())
                return 0;
            else
                return listPatientCaserows.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(context).inflate(R.layout.medications_cell, parent, false);

            TextView tvMedicineName = (TextView) convertView.findViewById(R.id.tvMedicineName);
            TextView tvDoctorname = (TextView) convertView.findViewById(R.id.tvDoctorname);
            TextView tvSession = (TextView) convertView.findViewById(R.id.tvSession);
            TextView tvTimePeriod = (TextView) convertView.findViewById(R.id.tvTimePeriod);
            TextView tvStatus   = (TextView) convertView.findViewById(R.id.tvStatus);
            View vCurrent = (View) convertView.findViewById(R.id.vCurrent);
            mcontext.setSpecificTypeFace((ViewGroup) convertView, ApplicationConstants.WALSHEIM_MEDIUM);

            LinearLayout llMedications = (LinearLayout) convertView.findViewById(R.id.llMedications);

            BaseActivity.setSpecificTypeFace(llMedications, ApplicationConstants.WALSHEIM_MEDIUM);

            final PatientCaseRow patientCaseRow = listPatientCaserows.get(position);

            tvMedicineName.setText(patientCaseRow.getMedicine());
            CaseDoctorDetails caseDoctorDetails = patientCaseRow.getListDoctorDetails1();
            if (caseDoctorDetails != null) {
                if (caseDoctorDetails.getDoctorName().startsWith("Dr."))
                    tvDoctorname.setText(caseDoctorDetails.getDoctorName());
                else
                    tvDoctorname.setText("Dr." + caseDoctorDetails.getDoctorName());
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeInMillis(patientCaseRow.getConsultationDt());


            PatientCasePrescribObj patientCasePrescribObj = patientCaseRow.getPatientprHistory();
            if (patientCasePrescribObj != null) {

                Calendar calendar1 = Calendar.getInstance(Locale.getDefault());
                calendar1.setTimeInMillis(patientCaseRow.getConsultationDt());
                int noofdays = patientCasePrescribObj.getNumberofDays() ;
                calendar1.add(Calendar.DATE, noofdays);

                if (DateUtils.isToday(calendar1) || DateUtils.isBeforeDay(Calendar.getInstance(Locale.getDefault()), calendar1)) {
                    tvStatus.setVisibility(View.VISIBLE);
                    vCurrent.setVisibility(View.VISIBLE);
                    patientCaseRow.setTodayRecord(true);
                    Calendar cal = Calendar.getInstance(Locale.getDefault());
                    cal.setTimeInMillis(patientCaseRow.getConsultationDt());
                    int pastDaysCount = 0;
                    String patientName = LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFullName();
                    String medicineName = patientCaseRow.getMedicine();

                    for (int i = 0; i <= noofdays; i++) {

                        if (DateUtils.isToday(cal) || (!DateUtils.isBeforeDay((cal), Calendar.getInstance()))) {
                            if (!patientCasePrescribObj.getDosage1().equalsIgnoreCase("0")) {

                                cal.set(Calendar.HOUR_OF_DAY, 8);
                                cal.set(Calendar.MINUTE, 0);
                                cal.set(Calendar.SECOND, 0);
                                if (Calendar.getInstance(Locale.getDefault()).before(cal))
                                    saveState(patientCaseRow.getCaseID()+"" ,patientCaseRow.getRowID(), medicineName, cal, patientName, 1);
                            }
                            if (!patientCasePrescribObj.getDosage2().equalsIgnoreCase("0")) {
                                cal.set(Calendar.HOUR_OF_DAY, 13);
                                cal.set(Calendar.MINUTE, 0);
                                cal.set(Calendar.SECOND, 0);
                                if (Calendar.getInstance(Locale.getDefault()).before(cal))
                                    saveState(patientCaseRow.getCaseID()+"" ,patientCaseRow.getRowID(), medicineName, cal, patientName, 2);
                            }
                            if (!patientCasePrescribObj.getDosage4().equalsIgnoreCase("0")) {
                                cal.set(Calendar.HOUR_OF_DAY, 20);
                                cal.set(Calendar.MINUTE, 0);
                                cal.set(Calendar.SECOND, 0);
                                if (Calendar.getInstance(Locale.getDefault()).before(cal))
                                    saveState(patientCaseRow.getCaseID()+"" ,patientCaseRow.getRowID(), medicineName, cal, patientName, 3);
                            }
                        }
                        cal.add(Calendar.DATE, 1);
                    }

                    /*String patientName = LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFullName();
                    String medicineName = patientCaseRow.getMedicine();
                    for (int i = 0; i < noofdays; i++){
                        if (!patientCasePrescribObj.getDosage1().isEmpty()){
                            calendar.set(Calendar.HOUR_OF_DAY, 8);
                            saveState(patientCaseRow.getRowID()+"", medicineName, calendar, patientName);
                        }
                    }*/

                } else {
                    tvStatus.setVisibility(View.INVISIBLE);
                    vCurrent.setVisibility(View.INVISIBLE);
                }

                tvTimePeriod.setText(simpleDateFormat.format(calendar.getTime()) + " - " + simpleDateFormat.format(calendar1.getTime()));
                tvSession.setText((patientCasePrescribObj.getDosage1().isEmpty() ? "0" : patientCasePrescribObj.getDosage1()) + "-" + (patientCasePrescribObj.getDosage2().isEmpty() ? "0" : patientCasePrescribObj.getDosage2()) + "-" + (patientCasePrescribObj.getDosage4().isEmpty() ? "0" : patientCasePrescribObj.getDosage4()));
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showLoader("");
                    PatientVisitdetailsReq visitsRequest = new PatientVisitdetailsReq();
                    visitsRequest.setScreenType("MEDICINE");
                    visitsRequest.setPatientID(patinetid);
                    visitsRequest.setPatientCaseID(patientCaseRow.getCaseID());
                    visitsRequest.setRowID(patientCaseRow.getRowID());
                    final boolean isTodayRecord = patientCaseRow.isTodayRecord();
                    RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientsVisitsDetails(visitsRequest, new RestCallback<PatientVisitRespone>() {
                        @Override
                        public void failure(RestError restError) {
                            hideLoader();
                            mcontext.ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
                        }

                        @Override
                        public void success(PatientVisitRespone patientVisitRespone, Response response) {
                            hideLoader();
                            if (patientVisitRespone != null && patientVisitRespone.getListPatientCaserows() != null && !patientVisitRespone.getListPatientCaserows().isEmpty()) {
                                Intent slideactivity = new Intent(context, MedicationSummary.class);
                                slideactivity.putExtra("SELECTEDCASE", patientVisitRespone.getListPatientCaserows().get(0));
                                slideactivity.putExtra("isTodayRecord", isTodayRecord);
                                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                                startActivity(slideactivity, bndlanimation);
                            }

                        }
                    });
                }
            });
            if (!patientCaseRow.isShow())
                convertView.setVisibility(View.GONE);

            return convertView;
        }

        public void refreshAdapter(ArrayList<PatientCaseRow> listPatientCaserows) {
            if (originallistPatientCaserows != null)
                originallistPatientCaserows.clear();
            originallistPatientCaserows.addAll(listPatientCaserows);
            this.listPatientCaserows = listPatientCaserows;
            notifyDataSetChanged();
        }

        public ArrayList<PatientCaseRow> getListPatientCaserows() {
            return listPatientCaserows;
        }
    }
    private boolean saveState(String caseID, long rowID, String medicineName, Calendar dateTime, String patientName, int dayID) {
        long recordID = -1l;
//        int count = 1;
        try {
//            if (count <= 2) {
            count++;
            SimpleDateFormat dateTimeFormat     = new SimpleDateFormat(DATE_FORMAT);
            SimpleDateFormat TimeFormat         = new SimpleDateFormat(TIME_FORMAT);
            String reminderDateTime             = dateTimeFormat.format(dateTime.getTime());
            String reminderTime                 = TimeFormat.format(dateTime.getTime());
            String hour                         = (reminderTime.split(":")[0]);
            String minutes                      = (reminderTime.split(":")[1].split(" ")[0]);
            String dayMode                      = (reminderTime.split(":")[1].split(" ")[1]);

            if (dayMode.contains("p") || dayMode.contains("P")){
                reminderTime = hour + ":"+minutes+ " PM";
            }else{
                reminderTime = hour + ":"+minutes+ " AM";
            }

            Cursor c = ApplicationConstants.dbAdapter.checkIfRecordExist(Long.parseLong(caseID), rowID, dayID, reminderDateTime);
            if (c == null || c.getCount() == 0) {
                recordID = ApplicationConstants.dbAdapter.createReminder(medicineName, reminderDateTime, rowID, patientName, dayID, reminderTime, Long.parseLong(caseID));
                new ReminderManager(getActivity()).setReminder(recordID, dayID, dateTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Operation Succeeded.
            return recordID != -1l;
        }
    }

}
