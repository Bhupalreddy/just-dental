package hCue.Hcue.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.CallBackInterfaces.ProfileDataInterface;
import hCue.Hcue.DAO.EmergencyInfo;
import hCue.Hcue.R;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by User on 2/18/2017.
 */

public class OthersFragment extends BaseFragment
{
    private TextView   tvContactPersonEmail, tvContactPerson, tvContactPersonPhoNO,tvRelation;
    private PatientUpdateRequest patientUpdateRequest ;
    private ProfileDataInterface otherFragmentOthersUpdate;
    public OthersFragment()
    {
        super();
    }
    public static Fragment getInstance(PatientUpdateRequest patientUpdateRequest)
    {
        OthersFragment  othersFragment = new OthersFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PatientUpdateRequest",patientUpdateRequest);
        othersFragment.setArguments(bundle);
        return othersFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        patientUpdateRequest = (PatientUpdateRequest) getArguments().getSerializable("PatientUpdateRequest");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View            Others                  =   inflater.inflate(R.layout.fragment_others, container, false);

        tvContactPerson         =   (TextView)     Others.findViewById(R.id.tvContactPerson);
        tvContactPersonPhoNO    =   (TextView)     Others.findViewById(R.id.tvContactPersonPhoNO);
        tvContactPersonEmail    =   (TextView)     Others.findViewById(R.id.tvContactPersonEmail);
        tvRelation              =   (TextView)     Others.findViewById(R.id.tvRelation);

        LinearLayout llOthers                =   (LinearLayout) Others.findViewById(R.id.llOthers);
        LinearLayout    llOthersUpdate          =   (LinearLayout) Others.findViewById(R.id.llOthersUpdate);

        if(patientUpdateRequest.getPatientDetails3().getEmergencyInfo() != null)
        {
            EmergencyInfo emergencyInfo = patientUpdateRequest.getPatientDetails3().getEmergencyInfo();
            if(emergencyInfo.getName() !=null )
                tvContactPerson.setText(emergencyInfo.getName());
            if(emergencyInfo.getMobileNumber() !=null )
                tvContactPersonPhoNO.setText(emergencyInfo.getMobileNumber());
            if(emergencyInfo.getEmailID() !=null )
                tvContactPersonEmail.setText(emergencyInfo.getEmailID());
            if(emergencyInfo.getRelationship() !=null )
                tvRelation.setText(emergencyInfo.getRelationship());
        }else
        {
            EmergencyInfo  emergencyInfo = new EmergencyInfo();
            patientUpdateRequest.getPatientDetails3().setEmergencyInfo(emergencyInfo);
        }
        otherFragmentOthersUpdate =(ProfileDataInterface) getActivity();

        llOthersUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Contcatperson,ContactPersonPhoNO,ContactPersonEmail,Relation;

                if(tvContactPerson.getText().toString().isEmpty()) {
                    Contcatperson = "";
                }
                else {
                    Contcatperson = tvContactPerson.getText().toString();
                }
                if(tvContactPersonPhoNO.getText().toString().isEmpty()) {
                    ContactPersonPhoNO = "";
                }
                else {
                    ContactPersonPhoNO = tvContactPersonPhoNO.getText().toString();
                }
                if(tvContactPersonEmail.getText().toString().isEmpty()) {
                    ContactPersonEmail = "";
                }
                else {
                    ContactPersonEmail = tvContactPersonEmail.getText().toString();
                }
                if(tvRelation.getText().toString().isEmpty())
                {
                    Relation = "";
                }
                else
                {
                    Relation = tvRelation.getText().toString();
                }

                otherFragmentOthersUpdate.OtherUpdate(v,Contcatperson,ContactPersonPhoNO,ContactPersonEmail,Relation);
            }

        });


        BaseActivity.setSpecificTypeFace(llOthers, ApplicationConstants.WALSHEIM_MEDIUM);

        return Others;
    }

    @Override
    public void setupListeners() {

    }
    public void setdata(EmergencyInfo emergencyInfo){
        tvContactPerson.setText(emergencyInfo.getName());
        tvContactPersonPhoNO.setText(emergencyInfo.getMobileNumber());
        tvContactPersonEmail.setText(emergencyInfo.getEmailID());
        tvRelation.setText(emergencyInfo.getRelationship());

    }
}
