package hCue.Hcue.Fragments;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.DAO.CaseDoctorDetails;
import hCue.Hcue.DAO.DoctorSpecialisation;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.R;
import hCue.Hcue.VisitsSummary;
import hCue.Hcue.model.PatientVisitRespone;
import hCue.Hcue.model.PatientVisitdetailsReq;
import hCue.Hcue.model.PatientsVisitsRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import retrofit.client.Response;

/**
 * Created by User on 2/17/2017.
 */

public class MyVisitsFragment extends BaseFragment
{
    private ArrayList<PatientCaseRow> listPatientCaseRow;
    private ListView lvVisits;
    private LinearLayout llVisits;
    private TextView tvVisits;
    private ImageView ivVisits;
    private MyVisitsAdapter myVisitsAdapter;
    private long patinetid;
    public static MyVisitsFragment getInstance(long patinetid)
    {
        MyVisitsFragment myVisitsFragment1 = new MyVisitsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PATIENTID", patinetid);
        myVisitsFragment1.setArguments(bundle);
        return myVisitsFragment1;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listPatientCaseRow = new ArrayList<>();
        patinetid = (long) getArguments().getSerializable("PATIENTID");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Myvisits = inflater.inflate(R.layout.common_list, container, false);
        lvVisits = (ListView) Myvisits.findViewById(R.id.lvCommon);
        llVisits = (LinearLayout) Myvisits.findViewById(R.id.llAvailability);
        LinearLayout llSearchLocation = (LinearLayout) Myvisits.findViewById(R.id.llSearchLocation);
        tvVisits = (TextView) Myvisits.findViewById(R.id.tvNotAvailable);
        ivVisits = (ImageView) Myvisits.findViewById(R.id.ivIcon);
        final EditText edtfilter = (EditText) Myvisits.findViewById(R.id.edtfilter);
        LinearLayout llclose = (LinearLayout) Myvisits.findViewById(R.id.llclose);
        mcontext.setSpecificTypeFace((ViewGroup) Myvisits, ApplicationConstants.WALSHEIM_MEDIUM);
        tvVisits.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        myVisitsAdapter = new MyVisitsAdapter(getActivity(), listPatientCaseRow);
        lvVisits.setAdapter(myVisitsAdapter);
        if (Connectivity.isConnected(getActivity())) {
            callgetUservisits(myVisitsAdapter, llSearchLocation);
        } else {
            mcontext.ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
        }

        //edtfilter.setText("");
        edtfilter.setHint("Enter doctor or clinic name");
        edtfilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase();
                if (myVisitsAdapter != null && myVisitsAdapter.getListPatientCaserows() != null) {
                    myVisitsAdapter.getListPatientCaserows().clear();
                    myVisitsAdapter.getListPatientCaserows().addAll(listPatientCaseRow);
                    Iterator<PatientCaseRow> iterator = myVisitsAdapter.getListPatientCaserows().iterator();
                    while (iterator.hasNext()) {
                        PatientCaseRow patientCaseRow = iterator.next();
                        if (patientCaseRow.getListDoctorDetails().getClinicName().toLowerCase().contains(str) || patientCaseRow.getListDoctorDetails().getDoctorName().toLowerCase().contains(str)) {
                            //patientCaseRow.setShow(true);
                        } else {
                            iterator.remove();
                        }
                    }

                    if (myVisitsAdapter.getListPatientCaserows().size() == 0){
                        llVisits.setVisibility(View.VISIBLE);
                        tvVisits.setText("No visits to display.");
                        ivVisits.setBackgroundResource(R.drawable.steth);
                    }else{
                        llVisits.setVisibility(View.GONE);
                        myVisitsAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        llclose.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    edtfilter.setText("");
                    if (myVisitsAdapter != null && myVisitsAdapter.getListPatientCaserows() != null) {
                        myVisitsAdapter.getListPatientCaserows().clear();
                        myVisitsAdapter.getListPatientCaserows().addAll(listPatientCaseRow);
                        myVisitsAdapter.notifyDataSetChanged();
                    }
                    return true;
                }
                return false;
            }
        });

        return Myvisits;
    }

    @Override
    public void setupListeners() {

    }
    private void callgetUservisits(final MyVisitsAdapter myVisitsAdapter, final LinearLayout llSearchLocation) {
        showLoader("");
        PatientsVisitsRequest visitsRequest = new PatientsVisitsRequest();
        visitsRequest.setPatientID(patinetid);
        visitsRequest.setHospitalCD("RVUYMISZZF");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientsVisits(visitsRequest, new RestCallback<PatientVisitRespone>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                mcontext.ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
            }

            @Override
            public void success(PatientVisitRespone patientVisitRespone, Response response) {
                hideLoader();
                if (patientVisitRespone != null && patientVisitRespone.getListPatientCaserows() != null && !patientVisitRespone.getListPatientCaserows().isEmpty()) {
                    llVisits.setVisibility(View.GONE);
                    myVisitsAdapter.refreshAdapter(patientVisitRespone.getListPatientCaserows());
                    if (patientVisitRespone.getListPatientCaserows().size() > 7)
                        llSearchLocation.setVisibility(View.VISIBLE);
                } else {
                    llVisits.setVisibility(View.VISIBLE);
                    tvVisits.setText("No visits to display");
                    ivVisits.setBackgroundResource(R.drawable.steth);
                }

            }
        });

    }

    public class MyVisitsAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<PatientCaseRow> listPatientCaserows;
        private ArrayList<PatientCaseRow> originallistPatientCaserows;

        public ArrayList<PatientCaseRow> getListPatientCaserows() {
            return listPatientCaserows;
        }

        public MyVisitsAdapter(Context context, ArrayList<PatientCaseRow> listPatientCaserows) {
            this.context = context;
            this.originallistPatientCaserows = listPatientCaserows;
        }

        @Override
        public int getCount() {
            if (listPatientCaserows == null)
                return 0;
            else
                return listPatientCaserows.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(context).inflate(R.layout.my_visits_cell, parent, false);

            TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            TextView tvTime = (TextView) convertView.findViewById(R.id.tvTime);
            TextView tvDoctorname = (TextView) convertView.findViewById(R.id.tvDoctorname);
            TextView tvspeciality = (TextView) convertView.findViewById(R.id.tvspeciality);
            TextView tvClinic = (TextView) convertView.findViewById(R.id.tvClinic);
            mcontext.setSpecificTypeFace((ViewGroup) convertView, ApplicationConstants.WALSHEIM_MEDIUM);

            LinearLayout llMyVitals = (LinearLayout) convertView.findViewById(R.id.llMyVitals);
            final PatientCaseRow patientCaseRow = listPatientCaserows.get(position);
            try {

                CaseDoctorDetails caseDoctorDetails = patientCaseRow.getListDoctorDetails();
                if (caseDoctorDetails.getDoctorName().startsWith("Dr."))
                    tvDoctorname.setText(caseDoctorDetails.getDoctorName());
                else
                    tvDoctorname.setText("Dr." + caseDoctorDetails.getDoctorName());
                tvClinic.setText(caseDoctorDetails.getClinicName());
                StringBuilder specialityBuilder = new StringBuilder();
                if (caseDoctorDetails.getListDoctorSpecolisations() != null && !caseDoctorDetails.getListDoctorSpecolisations().isEmpty())
                    for (DoctorSpecialisation doctorSpecialisation : caseDoctorDetails.getListDoctorSpecolisations())
                        specialityBuilder.append(doctorSpecialisation.getSepcialityDesc() + " ,");

                if (!specialityBuilder.toString().isEmpty())
                    tvspeciality.setText(specialityBuilder.toString().substring(0, specialityBuilder.toString().length() - 2));

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yy");
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setTimeInMillis(patientCaseRow.getConsultationDt());
                tvDate.setText(simpleDateFormat.format(calendar.getTime()));

                tvTime.setText(patientCaseRow.getStartTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

            BaseActivity.setSpecificTypeFace(llMyVitals, ApplicationConstants.WALSHEIM_MEDIUM);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showLoader("");
                    PatientVisitdetailsReq visitsRequest = new PatientVisitdetailsReq();
                    visitsRequest.setPatientID(patinetid);
                    visitsRequest.setPatientCaseID(patientCaseRow.getCaseID());
                    RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientsVisitsDetails(visitsRequest, new RestCallback<PatientVisitRespone>() {
                        @Override
                        public void failure(RestError restError) {
                            hideLoader();
                            mcontext.ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
                        }

                        @Override
                        public void success(PatientVisitRespone patientVisitRespone, Response response) {
                            hideLoader();
                            if (patientVisitRespone != null && patientVisitRespone.getListPatientCaserows() != null && !patientVisitRespone.getListPatientCaserows().isEmpty()) {
                                Intent slideactivity = new Intent(context, VisitsSummary.class);
                                slideactivity.putExtra("SELECTEDCASE", patientVisitRespone.getListPatientCaserows().get(0));
                                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                                startActivity(slideactivity, bndlanimation);
                            }
                        }
                    });

                }
            });
            return convertView;
        }

        public void refreshAdapter(ArrayList<PatientCaseRow> listPatientCaserows) {
            if (originallistPatientCaserows != null)
                originallistPatientCaserows.clear();
            originallistPatientCaserows.addAll(listPatientCaserows);
            this.listPatientCaserows = listPatientCaserows;
            notifyDataSetChanged();
        }
    }
}
