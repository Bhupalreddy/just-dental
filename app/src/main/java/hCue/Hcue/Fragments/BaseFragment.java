package hCue.Hcue.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import hCue.Hcue.BaseActivity;

/**
 * Created by Appdest on 24-10-2016.
 */

public abstract class BaseFragment extends Fragment
{
    protected static BaseActivity mcontext;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mcontext = (BaseActivity) activity;
        setupListeners();
    }

    public abstract void setupListeners() ;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void showLoader(String msg)
    {
        mcontext.showLoader(msg);
    }

    public void hideLoader()
    {
        mcontext.hideLoader();
    }

}
