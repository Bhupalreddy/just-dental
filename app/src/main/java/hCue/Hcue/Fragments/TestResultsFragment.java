package hCue.Hcue.Fragments;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.DAO.CaseLabHistory;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.R;
import hCue.Hcue.TestResultsSummary;
import hCue.Hcue.model.PatientVisitRespone;
import hCue.Hcue.model.PatientVisitdetailsReq;
import hCue.Hcue.model.PatientsVisitsRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import retrofit.client.Response;

/**
 * Created by User on 2/17/2017.
 */

public class TestResultsFragment extends BaseFragment {
    private ArrayList<PatientCaseRow> listPatientCaseRow;
    private ListView lvTest;
    private LinearLayout llTest;
    private TextView tvTest;
    private ImageView ivTest;
    private TestResultsAdapter testResultsAdapter;
    private long patinetid;

    public static TestResultsFragment getInstance(long patinetid) {
        TestResultsFragment testResultsFragment1 = new TestResultsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PATIENTID", patinetid);
        testResultsFragment1.setArguments(bundle);
        return testResultsFragment1;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listPatientCaseRow = new ArrayList<>();
        patinetid = (long) getArguments().getSerializable("PATIENTID");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View TestResults = inflater.inflate(R.layout.common_list, container, false);
        lvTest = (ListView) TestResults.findViewById(R.id.lvCommon);
        llTest = (LinearLayout) TestResults.findViewById(R.id.llAvailability);
        tvTest = (TextView) TestResults.findViewById(R.id.tvNotAvailable);
        tvTest.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        ivTest = (ImageView) TestResults.findViewById(R.id.ivIcon);
        final EditText edtfilter = (EditText) TestResults.findViewById(R.id.edtfilter);
        LinearLayout llclose = (LinearLayout) TestResults.findViewById(R.id.llclose);
        LinearLayout llSearchLocation = (LinearLayout) TestResults.findViewById(R.id.llSearchLocation);

        mcontext.setSpecificTypeFace((ViewGroup) TestResults, ApplicationConstants.WALSHEIM_MEDIUM);

        testResultsAdapter = new TestResultsAdapter(getActivity(), listPatientCaseRow);
        lvTest.setAdapter(testResultsAdapter);
        if (Connectivity.isConnected(getActivity())) {
            callgetUservisits(testResultsAdapter, llSearchLocation);
        } else {
            mcontext.ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
        }

        edtfilter.setHint("Enter test or lab name");

        edtfilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase();
                if (testResultsAdapter != null && testResultsAdapter.getListPatientCaserows() != null) {
                    testResultsAdapter.getListPatientCaserows().clear();
                    testResultsAdapter.getListPatientCaserows().addAll(listPatientCaseRow);
                    Iterator<PatientCaseRow> iterator = testResultsAdapter.getListPatientCaserows().iterator();
                    while (iterator.hasNext()) {
                        PatientCaseRow patientCaseRow = iterator.next();
                        Log.e("LAB TEST, LAB NAME --", patientCaseRow.getLabTest() + "," + patientCaseRow.getLabName());
                        if (patientCaseRow.getLabName() != null) {
                            if (patientCaseRow.getLabTest().toLowerCase().contains(str) || patientCaseRow.getLabName().toLowerCase().contains(str)) {
                                //patientCaseRow.setShow(true);
                            } else {
                                iterator.remove();
                            }
                        } else {
                            iterator.remove();
                        }
                    }
                    testResultsAdapter.notifyDataSetChanged();


                    if (testResultsAdapter.getListPatientCaserows().size() == 0) {
                        llTest.setVisibility(View.VISIBLE);
                        tvTest.setText("No tests to display.");
                        ivTest.setBackgroundResource(R.drawable.lab);
                    } else {
                        llTest.setVisibility(View.GONE);
                        testResultsAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        llclose.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    edtfilter.setText("");
                    edtfilter.setText("");
                    if (testResultsAdapter != null && testResultsAdapter.getListPatientCaserows() != null) {
                        testResultsAdapter.getListPatientCaserows().clear();
                        testResultsAdapter.getListPatientCaserows().addAll(listPatientCaseRow);
                        testResultsAdapter.notifyDataSetChanged();
                    }
                    return true;
                }
                return false;
            }
        });
        return TestResults;
    }

    private void callgetUservisits(final TestResultsAdapter testResultsAdapter, final LinearLayout llSearchLocation) {
        PatientsVisitsRequest visitsRequest = new PatientsVisitsRequest();
        visitsRequest.setPatientID(patinetid);
        visitsRequest.setHospitalCD("RVUYMISZZF");
        visitsRequest.setScreenType("LAB");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientsVisits(visitsRequest, new RestCallback<PatientVisitRespone>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                mcontext.ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
            }

            @Override
            public void success(PatientVisitRespone patientVisitRespone, Response response) {
                hideLoader();
                if (patientVisitRespone != null && patientVisitRespone.getListPatientCaserows() != null && !patientVisitRespone.getListPatientCaserows().isEmpty()) {
                    llTest.setVisibility(View.GONE);
                    testResultsAdapter.refreshAdapter(patientVisitRespone.getListPatientCaserows());
                    if (patientVisitRespone.getListPatientCaserows().size() > 7)
                        llSearchLocation.setVisibility(View.VISIBLE);
                } else {
                    llTest.setVisibility(View.VISIBLE);
                    tvTest.setText("No tests to display.");
                    ivTest.setBackgroundResource(R.drawable.lab);
                }

            }
        });
    }

    @Override
    public void setupListeners() {

    }

    public class TestResultsAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<PatientCaseRow> listPatientCaserows;
        private ArrayList<PatientCaseRow> originallistPatientCaserows;

        public TestResultsAdapter(Context context, ArrayList<PatientCaseRow> listPatientCaserows) {
            this.context = context;
            this.originallistPatientCaserows = listPatientCaserows;
        }

        @Override
        public int getCount() {
            if (listPatientCaserows == null || listPatientCaserows.isEmpty())
                return 0;
            else
                return listPatientCaserows.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(context).inflate(R.layout.test_results_cell, parent, false);

            TextView tvTestType = (TextView) convertView.findViewById(R.id.tvTestType);
            TextView tvLabName = (TextView) convertView.findViewById(R.id.tvLabName);
            TextView tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
            TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            TextView tvTime = (TextView) convertView.findViewById(R.id.tvTime);

            LinearLayout llTestResults = (LinearLayout) convertView.findViewById(R.id.llTestResults);

            BaseActivity.setSpecificTypeFace(llTestResults, ApplicationConstants.WALSHEIM_MEDIUM);

            final PatientCaseRow patientCaseRow = listPatientCaserows.get(position);
            CaseLabHistory caseLabHistory = patientCaseRow.getCaseLabHistory();
            tvTestType.setText(patientCaseRow.getLabTest());
            if (patientCaseRow.getLabName() != null && !patientCaseRow.getLabName().equalsIgnoreCase("")){
                tvLabName.setText(patientCaseRow.getLabName());
                tvLabName.setVisibility(View.VISIBLE);
            }else{
                tvLabName.setVisibility(View.GONE);
            }

            tvTime.setText(patientCaseRow.getStartTime1());

            if (!patientCaseRow.getStatus().equalsIgnoreCase("LCOM")) {
                tvStatus.setText("Results Pending");
            } else {
                tvStatus.setText("Completed");
                tvStatus.setTextColor(Color.parseColor("#4ebb50"));
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeInMillis(patientCaseRow.getConsultationDt());
            tvDate.setText(simpleDateFormat.format(calendar.getTime()));

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showLoader("");
                    PatientVisitdetailsReq visitsRequest = new PatientVisitdetailsReq();
                    visitsRequest.setPatientID(patinetid);
                    visitsRequest.setScreenType("LAB");
                    visitsRequest.setPatientCaseID(patientCaseRow.getCaseID());
                    visitsRequest.setRowID(patientCaseRow.getRowID());
                    RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientsVisitsDetails(visitsRequest, new RestCallback<PatientVisitRespone>() {
                        @Override
                        public void failure(RestError restError) {
                            hideLoader();
                            mcontext.ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
                        }

                        @Override
                        public void success(PatientVisitRespone patientVisitRespone, Response response) {
                            hideLoader();
                            if (patientVisitRespone != null && patientVisitRespone.getListPatientCaserows() != null && !patientVisitRespone.getListPatientCaserows().isEmpty()) {
                                Intent slideactivity = new Intent(context, TestResultsSummary.class);
                                slideactivity.putExtra("SELECTEDCASE", patientVisitRespone.getListPatientCaserows().get(0));
                                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                                startActivity(slideactivity, bndlanimation);
                            }

                        }
                    });
                }
            });

            return convertView;
        }

        public void refreshAdapter(ArrayList<PatientCaseRow> listPatientCaserows) {
            if (originallistPatientCaserows != null)
                originallistPatientCaserows.clear();
            originallistPatientCaserows.addAll(listPatientCaserows);
            this.listPatientCaserows = listPatientCaserows;
            notifyDataSetChanged();
        }

        public ArrayList<PatientCaseRow> getListPatientCaserows() {
            return listPatientCaserows;
        }
    }
}
