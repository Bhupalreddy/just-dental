package hCue.Hcue.Fragments;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.ChooseDateTime;
import hCue.Hcue.DAO.DoctorAddress;
import hCue.Hcue.DAO.PastFutureRow;
import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.R;
import hCue.Hcue.model.CancelAppointmentReq;
import hCue.Hcue.model.MyAppointmentRequest;
import hCue.Hcue.model.PastFutureAppointmentsRes;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;

/**
 * Created by User on 2/17/2017.
 */

public class UpcomingFragment extends BaseFragment
{
    private Button btnAddMore ;
    private UpcomingAdapter upcomingAdapter;
    private ListView   lvCommon1;
    private LinearLayout llAvailability1;
    private TextView tvNotAvailable1;
    private int upcomingpagesize =10;
    private boolean hasmultipleusers ;
    private Patient selectedPatienRow ;
    private ArrayList<PastFutureRow> arrayRowsFuture ;
    private Double currentlat, currentlong ;

    public UpcomingFragment()
    {
        super();
    }

    public static Fragment getInstance()
    {
        UpcomingFragment upcomingFragment = new UpcomingFragment();
        return upcomingFragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View  Upcoming     =   inflater.inflate(R.layout.common_list, container, false);
        lvCommon1          =   (ListView)      Upcoming.findViewById(R.id.lvCommon);
        llAvailability1   =   (LinearLayout)  Upcoming.findViewById(R.id.llAvailability);
        tvNotAvailable1   =   (TextView)      Upcoming.findViewById(R.id.tvNotAvailable);

        selectedPatienRow = LoginResponseSingleton.getSingleton().getArrPatients().get(0);
        Preference preference = new Preference(getActivity());
        String latlong = preference.getStringFromPreference("CURRENTLATLONG","");
        if(latlong.contains(","))
        {
            currentlat  = Double.parseDouble(latlong.split(",")[0]);
            currentlong = Double.parseDouble(latlong.split(",")[1]);
        }

        upcomingAdapter = new UpcomingAdapter(getActivity());
        btnAddMore = new Button(getActivity());
        //btnAddMore.setLayoutParams(myLayoutParams);
        btnAddMore.setText("Load More");
        btnAddMore.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        btnAddMore.setBackgroundResource(R.color.white);
        btnAddMore.setTextColor(getResources().getColor(R.color.gray));
        btnAddMore.setVisibility(View.GONE);
        lvCommon1.addFooterView(btnAddMore);
        lvCommon1.setAdapter(upcomingAdapter);
        lvCommon1.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if(upcomingpagesize != 0)
                    if(lastInScreen == upcomingpagesize-1||lastInScreen == upcomingpagesize-2)
                    {
                        btnAddMore.setVisibility(View.VISIBLE);
                        btnAddMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                upcomingpagesize = upcomingpagesize+10;
                                callgetPastFutureAppointmentsService(upcomingAdapter,upcomingpagesize);
                                btnAddMore.setVisibility(View.GONE);
                            }
                        });
                    }
            }
        });

        return Upcoming;
    }
    @Override
    public void setupListeners()
    {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Connectivity.isConnected(getActivity()))
        {
            callgetPastFutureAppointmentsService(upcomingAdapter,upcomingpagesize);
        }else
        {
            mcontext.ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
        }
    }
    private void callgetPastFutureAppointmentsService(final UpcomingAdapter upcomingAdapter , int pagesize)
    {

        System.out.println("Enter callgetPastFutureAppointmentsService");
        showLoader("");
        MyAppointmentRequest myAppointmentRequest = new MyAppointmentRequest();
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.add(Calendar.DATE,-1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        myAppointmentRequest.setBaseDate(simpleDateFormat.format(calendar.getTime()));
        myAppointmentRequest.setIndicator("F");
        myAppointmentRequest.setPageSize(pagesize);
        myAppointmentRequest.setHospitalCode("RVUYMISZZF");

        if(hasmultipleusers)
            myAppointmentRequest.setFamilyHdID(selectedPatienRow.getFamilyHdID());
        else
            myAppointmentRequest.setFamilyHdID(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFamilyHdID());
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPastFutureAppointments(myAppointmentRequest, new RestCallback<PastFutureAppointmentsRes>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
                mcontext.ShowAlertDialog("Whoops!","Failed to get appointments. Please try again later.",R.drawable.worng);
            }

            @Override
            public void success(PastFutureAppointmentsRes pastFutureAppointmentsRes, Response response)
            {
                hideLoader();
                lvCommon1.setVisibility(View.VISIBLE);
                llAvailability1.setVisibility(View.GONE);
                if(pastFutureAppointmentsRes != null && pastFutureAppointmentsRes.getArrayRows()!= null && !pastFutureAppointmentsRes.getArrayRows().isEmpty())
                {
                    Iterator<PastFutureRow> iterator =  pastFutureAppointmentsRes.getArrayRows().iterator();
                    while(iterator.hasNext())
                    {
                        if(iterator.next().getAppointmentDetails().getAppointmentStatus()=='C')
                        {
                            iterator.remove();

                        }
                    }
                    if(pastFutureAppointmentsRes.getArrayRows().isEmpty())
                    {
                        lvCommon1.setVisibility(View.GONE);
                        llAvailability1.setVisibility(View.VISIBLE);
                        tvNotAvailable1.setText("You do not have any upcoming \n appointments");
                        tvNotAvailable1.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                    }
                    else {
                        upcomingAdapter.refreshAdapter(pastFutureAppointmentsRes.getArrayRows());
                    }
                }
                else
                {
//                    if(pastFutureAppointmentsRes != null && pastFutureAppointmentsRes.getArrayRows()!= null)
//                        upcomingAdapter.refreshAdapter(pastFutureAppointmentsRes.getArrayRows());
                    lvCommon1.setVisibility(View.GONE);
                    llAvailability1.setVisibility(View.VISIBLE);
                    tvNotAvailable1.setText("You do not have any upcoming \n appointments");
                    tvNotAvailable1.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                }
            }
        });
    }
    public class UpcomingAdapter extends BaseAdapter
    {
        private Context context_ ;


        public UpcomingAdapter(Context context)
        {
            this.context_ = context ;
        }

        @Override
        public int getCount() {

            if(arrayRowsFuture == null)
                return 0;
            else
                return arrayRowsFuture.size();
        }

        @Override
        public Object getItem(int position)
        {

            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            convertView                         =   LayoutInflater.from(context_).inflate(R.layout.my_appointment_cell,parent,false);

            TextView        tvDoctorname        =   (TextView)      convertView.findViewById(R.id.tvDoctorname);
            TextView        tvSpeciality        =   (TextView)      convertView.findViewById(R.id.tvSpeciality);
            TextView        tvDateTime          =   (TextView)      convertView.findViewById(R.id.tvDateTime);
            TextView        tvHospitalName      =   (TextView)      convertView.findViewById(R.id.tvHospitalName);
            TextView        tvChange            =   (TextView)      convertView.findViewById(R.id.tvChange);
            TextView        tvCallNow           =   (TextView)      convertView.findViewById(R.id.tvCallNow);
            TextView        tvSchedule          =   (TextView)      convertView.findViewById(R.id.tvSchedule);
           // final TextView  tvShowMap           =   (TextView)      convertView.findViewById(R.id.tvShowMap);

            ImageView ivChange            =   (ImageView)     convertView.findViewById(R.id.ivChange);
            ImageView       ivCallNow           =   (ImageView)     convertView.findViewById(R.id.ivCallNow);
            ImageView       ivSchedule          =   (ImageView)     convertView.findViewById(R.id.ivSchedule);

            ImageView       ivDoctorPic         =   (ImageView)     convertView.findViewById(R.id.ivDoctorPic);

            LinearLayout    llMyAppointments    =   (LinearLayout)  convertView.findViewById(R.id.llMyAppointments);
            LinearLayout    llChange            =   (LinearLayout)  convertView.findViewById(R.id.llChange);
            LinearLayout    llCallNow           =   (LinearLayout)  convertView.findViewById(R.id.llCallNow);
            LinearLayout    llSchedule          =   (LinearLayout)  convertView.findViewById(R.id.llSchedule);

            BaseActivity.setSpecificTypeFace(llMyAppointments,ApplicationConstants.WALSHEIM_MEDIUM);
            tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvCallNow.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvChange.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvSchedule.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

            final PastFutureRow pastFutureRow = arrayRowsFuture.get(position);

            if(pastFutureRow.getDoctorDetails().getProfileImage() != null && !pastFutureRow.getDoctorDetails().getProfileImage().isEmpty()) {
                Picasso.with(context_).load(Uri.parse(pastFutureRow.getDoctorDetails().getProfileImage())).into(ivDoctorPic);
            }
            else
            {
                if(pastFutureRow.getDoctorDetails().getGender() == 'M')
                {
                    Picasso.with(context_).load(R.drawable.male).into(ivDoctorPic);
                }
                else {
                    Picasso.with(context_).load(R.drawable.female).into(ivDoctorPic);
                }
            }
            if(pastFutureRow.getDoctorDetails().getDoctorFullName().startsWith("Dr."))
                tvDoctorname.setText(pastFutureRow.getDoctorDetails().getDoctorFullName());
            else
                tvDoctorname.setText("Dr. "+pastFutureRow.getDoctorDetails().getDoctorFullName());
            tvHospitalName.setText(pastFutureRow.getAppointmentDetails().getClinicName());
            StringBuilder specialitybuilder = new StringBuilder();
            if(pastFutureRow.getDoctorDetails().getDoctorSpecialization() != null) if(pastFutureRow.getDoctorDetails().getDoctorSpecialization() != null)
                for(String specialityid : pastFutureRow.getDoctorDetails().getDoctorSpecialization().values())
                {
                    specialitybuilder.append(Constants.specialitiesMap.get(specialityid)+", ");
                }
            try {
                tvSpeciality.setText(specialitybuilder.toString().substring(0, (specialitybuilder.toString().length()-2)));
            }catch (Exception e){
                e.printStackTrace();
            }

            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeInMillis(pastFutureRow.getAppointmentDetails().getConsultationDt());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM dd");
            tvDateTime.setText(simpleDateFormat.format(calendar.getTime())+" @ "+pastFutureRow.getAppointmentDetails().getStartTime()+" Hrs");
            tvChange.setText("CANCEL");
            ivChange.setImageResource(R.drawable.appoint_cancel);
            tvSchedule.setText("RESCHEDULE");
            ivSchedule.setImageResource(R.drawable.appointment_reschedule);

            if(pastFutureRow.getAppointmentDetails().getAppointmentStatus()!='B')
            {
                ivChange.setImageResource(R.drawable.cancel_disable);
                tvChange.setTextColor(Color.parseColor("#b1b1b1"));
                llChange.setClickable(false);
                llChange.setEnabled(false);
                ivSchedule.setImageResource(R.drawable.reschedule_disable);
                tvSchedule.setTextColor(Color.parseColor("#b1b1b1"));
                llSchedule.setClickable(false);
                llSchedule.setEnabled(false);
            }
            llCallNow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+pastFutureRow.getDoctorDetails().getArraydoctorPhone().get(0).getPhNumber()+""));
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", pastFutureRow.getDoctorDetails().getArraydoctorPhone().get(0).getPhNumber()+"", null));
                    context_.startActivity(intent);
                }
            });

            llChange.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.home_pickup_dialog, null);
                    dialogBuilder.setView(dialogView);

                    TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
                    final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                    TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                    TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
                    ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);


                    tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                    tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                    tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                    tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.setCanceledOnTouchOutside(false);

                    tvInfo.setText("Are you sure you want to cancel \n this appointment?");
                    tvInfo.setTextSize(18);
                    tvHeading.setText("Confirmation");
                    tvHeading.setVisibility(View.GONE);
                    ivLogo.setVisibility(View.VISIBLE);
                    ivLogo.setBackgroundResource(R.drawable.alert);

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                        @Override
                        public void onClick(View v)
                        {
                            alertDialog.dismiss();
                            final String[] items = {"Travel distance", "Date/Time not convenient","Started feeling better","Doctor unavailable","Other personal reasons"};
                            new android.app.AlertDialog.Builder(getActivity())
                                    .setTitle("Reason for cancellation")
                                    .setSingleChoiceItems(items, 0, null)
                                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                            dialog.dismiss();
//                                            if (Connectivity.isConnected(getActivity())) {
//                                                callCancelAppointmentService(pastFutureRow.getAppointmentDetails().getAppointmentID(), "");
//                                            } else {
//                                                ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
//                                            }
                                        }
                                    })
                                    .setPositiveButton("Submit", new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            dialog.dismiss();
                                            int selectedPosition = ((android.app.AlertDialog)dialog).getListView().getCheckedItemPosition();

                                            if (Connectivity.isConnected(getActivity()))
                                            {
                                                callCancelAppointmentService(pastFutureRow.getAppointmentDetails().getAppointmentID(), items[selectedPosition]);
                                            }else
                                            {
                                                mcontext.ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                            }
                                        }

                                    })
                                    .setNegativeButton("Skip",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    dialog.dismiss();
                                                    if (Connectivity.isConnected(getActivity())) {
                                                        callCancelAppointmentService(pastFutureRow.getAppointmentDetails().getAppointmentID(), "");
                                                    } else {
                                                       mcontext.ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
                                                    }
                                                }
                                            }

                                    ).show();


                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                }
            });

            /*tvShowMap.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(pastFutureRow.getDoctorDetails().getArraydoctorAddress() != null && !pastFutureRow.getDoctorDetails().getArraydoctorAddress().isEmpty())
                    {
                        DoctorAddress doctorAddress = pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0);
                        if(doctorAddress.getExtdetails() != null)
                        {
                            String latitude = doctorAddress.getExtdetails().getLatitude();
                            String longitude = doctorAddress.getExtdetails().getLongitude();

                            Uri location = Uri.parse(String.format("http://maps.google.com/?daddr="+latitude+","+longitude));
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

                            PackageManager packageManager = getActivity().getPackageManager();
                            List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                            boolean isIntentSafe = activities.size() > 0;

                            if (isIntentSafe) {
                                startActivity(mapIntent);
                            }
                            else
                            {
                                mcontext.ShowAlertDialog("Whoops!","Latlongs not found.",R.drawable.worng);
                            }
                        }
                    }

                    else
                    {
                        mcontext.ShowAlertDialog("Whoops!","Latlongs not found.",R.drawable.worng);
                    }
                }
            });*/

            llSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DoctorAddress doctorAddress = pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0);

                    SpecialityResRow specialityResRow = new SpecialityResRow();
                    specialityResRow.setProfileImage(pastFutureRow.getDoctorDetails().getProfileImage());
                    specialityResRow.setFullName(pastFutureRow.getDoctorDetails().getDoctorFullName());
                    specialityResRow.setClinicName(pastFutureRow.getAppointmentDetails().getClinicName());
                    specialityResRow.setSpecialityID(pastFutureRow.getDoctorDetails().getDoctorSpecialization().toString());
                    specialityResRow.setAddressID(pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0).getAddressID());
                    specialityResRow.setDoctorID(pastFutureRow.getAppointmentDetails().getDoctorID());
                    specialityResRow.setLatitude(doctorAddress.getExtdetails().getLatitude());
                    specialityResRow.setLongitude(doctorAddress.getExtdetails().getLongitude());
                    specialityResRow.setEmailID(pastFutureRow.getDoctorDetails().getArraydoctorEmail().get(0).getEmailID());
                    specialityResRow.setPhoneNumber(pastFutureRow.getDoctorDetails().getArraydoctorPhone().get(0).getPhNumber()+"");
                    specialityResRow.setAddress(pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0).getAddress1() + pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0).getAddress2());
                    specialityResRow.setGender(pastFutureRow.getDoctorDetails().getGender());
                    specialityResRow.setDistance(getDistance(doctorAddress.getExtdetails().getLatitude(), doctorAddress.getExtdetails().getLongitude()));
                    specialityResRow.setOtherVisitReason(pastFutureRow.getAppointmentDetails().getOtherVisitRsn() != null ? pastFutureRow.getAppointmentDetails().getOtherVisitRsn() : "");

                    Intent intent = new Intent(context_, ChooseDateTime.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(context_.getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    intent.putExtra("SELECTED_DATAILS",specialityResRow);
                    intent.putExtra("ISFROMRESCHEDULE", true);
                    intent.putExtra("AppointmentID", pastFutureRow.getAppointmentDetails().getAppointmentID());
                    intent.putExtra("AppointmentStatus", pastFutureRow.getAppointmentDetails().getAppointmentStatus());

                    context_.startActivity(intent, bndlanimation);
                }
            });

            return convertView;
        }

        public void refreshAdapter(ArrayList<PastFutureRow> arrayRows1)
        {
            if(arrayRowsFuture != null)
                arrayRowsFuture.clear();
            arrayRowsFuture = arrayRows1;
            notifyDataSetChanged();
        }
    }
    private void callCancelAppointmentService(long appointmentID, String reason)
    {
        showLoader("");
        CancelAppointmentReq cancelAppointmentReq = new CancelAppointmentReq();
        cancelAppointmentReq.setUSRType("PATIENT");
        cancelAppointmentReq.setUSRId(selectedPatienRow.getPatientID());
        cancelAppointmentReq.setAppointmentID(appointmentID);
        cancelAppointmentReq.setReason(reason);
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).cancelDocotAppointment(cancelAppointmentReq, new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                mcontext.ShowAlertDialog("Whoops!","Failed to cancel appointment. Please try again later.",R.drawable.worng);
            }

            @Override
            public void success(String s, Response response)
            {
                hideLoader();
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.home_pickup_dialog, null);
                dialogBuilder.setView(dialogView);

                TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
                final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
                ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

                tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                tvNo.setVisibility(View.GONE);
                tvYes.setText("OK");

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();

                tvInfo.setText("Your appointment cancelled successfully.");
                tvInfo.setTextSize(18);
                tvHeading.setText("Confirmation");
                tvHeading.setVisibility(View.GONE);
                ivLogo.setVisibility(View.VISIBLE);
                ivLogo.setBackgroundResource(R.drawable.sucess);

                tvYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        callgetPastFutureAppointmentsService(upcomingAdapter,upcomingpagesize);

                    }
                });


            }
        });
    }
    private String getDistance(String eLatitude, String eLongitude){

        double latitude = getDouble(eLatitude);
        double longitude = getDouble(eLongitude);

        String distance = "NA";
        if(currentlat == null || currentlong == null || (latitude == -1d || longitude == -1d))
            return distance;
        Location locationA = new Location("point A");

        locationA.setLatitude(currentlat);
        locationA.setLongitude(currentlong);

        Location locationB = new Location("point B");

        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);

        float distanceValue = locationA.distanceTo(locationB);
        Float obj = distanceValue/1000;
        if(obj.floatValue() < 100.0f)
        {
            distance = "~" + String.valueOf(distanceValue / 1000).substring(0, 4) + " kms";
        }
        else
        {
            distance = "";
        }
        return distance;
    }

    private double getDouble(String doubleString)
    {
        double doubleValue = -1d;
        try{
            doubleValue = Double.parseDouble(doubleString);
        }
        catch (Exception e){}
        return doubleValue;
    }

}
