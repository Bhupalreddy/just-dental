package hCue.Hcue.Fragments;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.ChooseDateTime;
import hCue.Hcue.DAO.DoctorAddress;
import hCue.Hcue.DAO.PastFutureRow;
import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.FeedBackActivity;
import hCue.Hcue.R;
import hCue.Hcue.model.MyAppointmentRequest;
import hCue.Hcue.model.PastFutureAppointmentsRes;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;

/**
 * Created by User on 2/17/2017.
 */

public class PastFragment extends BaseFragment
{
    private Button btnAddMore ;
    private ListView lvCommon2;
    private LinearLayout llAvailability2;
    private TextView tvNotAvailable2;
    private PastAdapter pastAdapter ;
    private  int pastpagesize=10;
    private ArrayList<PastFutureRow> arrayRowsPast ;
    private Double currentlat, currentlong ;
    private boolean hasmultipleusers ;
    private Patient selectedPatienRow ;
    public PastFragment()
    {
        super();
    }

    public static Fragment getInstance() {
        PastFragment pastFragment = new PastFragment();
        return pastFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View  Past=   inflater.inflate(R.layout.common_list, container, false);
        lvCommon2        =   (ListView) Past.findViewById(R.id.lvCommon);
        llAvailability2  =   (LinearLayout)  Past.findViewById(R.id.llAvailability);
        tvNotAvailable2  =   (TextView)      Past.findViewById(R.id.tvNotAvailable);

        pastAdapter = new PastAdapter(getActivity());
        btnAddMore = new Button(getActivity());
        selectedPatienRow = LoginResponseSingleton.getSingleton().getArrPatients().get(0);
        Preference preference = new Preference(getActivity());
        String latlong = preference.getStringFromPreference("CURRENTLATLONG","");
        if(latlong.contains(","))
        {
            currentlat  = Double.parseDouble(latlong.split(",")[0]);
            currentlong = Double.parseDouble(latlong.split(",")[1]);
        }
        //btnAddMore.setLayoutParams(myLayoutParams);
        btnAddMore.setText("Load More");
        btnAddMore.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        btnAddMore.setBackgroundResource(R.color.white);
        btnAddMore.setTextColor(getResources().getColor(R.color.gray));
        btnAddMore.setVisibility(View.GONE);
        lvCommon2.addFooterView(btnAddMore);
        lvCommon2.setAdapter(pastAdapter);

        lvCommon2.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if(pastpagesize != 0)
                    if(lastInScreen == pastpagesize-1||lastInScreen == pastpagesize-2)
                    {

                        btnAddMore.setVisibility(View.VISIBLE);
                        btnAddMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pastpagesize = pastpagesize +10;
                                callgetPastFutureAppointmentsService(pastAdapter,pastpagesize);
                                btnAddMore.setVisibility(View.GONE);
                            }
                        });

                    }
            }
        });


        return Past;
    }

    @Override
    public void setupListeners() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if(Connectivity.isConnected(getActivity()))
        {
            callgetPastFutureAppointmentsService(pastAdapter,pastpagesize);
        }else
        {
            mcontext.ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
        }
    }
    public class PastAdapter extends BaseAdapter
    {
        private Context context ;


        public PastAdapter(Context context)
        {
            this.context = context ;
        }

        @Override
        public int getCount() {

            if(arrayRowsPast == null)
                return 0;
            else
                return arrayRowsPast.size();
        }

        @Override
        public Object getItem(int position)
        {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            convertView                         =   LayoutInflater.from(context).inflate(R.layout.my_appointment_cell,parent,false);

            TextView        tvDoctorname        =   (TextView)       convertView.findViewById(R.id.tvDoctorname);
            TextView        tvSpeciality        =   (TextView)       convertView.findViewById(R.id.tvSpeciality);
            TextView        tvDateTime          =   (TextView)       convertView.findViewById(R.id.tvDateTime);
            TextView        tvHospitalName      =   (TextView)       convertView.findViewById(R.id.tvHospitalName);
            TextView        tvChange            =   (TextView)       convertView.findViewById(R.id.tvChange);
            TextView        tvCallNow           =   (TextView)       convertView.findViewById(R.id.tvCallNow);
            TextView        tvSchedule          =   (TextView)       convertView.findViewById(R.id.tvSchedule);

            ImageView ivChange            =   (ImageView)       convertView.findViewById(R.id.ivChange);
            ImageView       ivCallNow           =   (ImageView)       convertView.findViewById(R.id.ivCallNow);
            ImageView       ivSchedule          =   (ImageView)       convertView.findViewById(R.id.ivSchedule);
            //final TextView  tvShowMap           =   (TextView)       convertView.findViewById(R.id.tvShowMap);

            ImageView       ivDoctorPic         =   (ImageView)      convertView.findViewById(R.id.ivDoctorPic);

            LinearLayout    llMyAppointments    =   (LinearLayout)   convertView.findViewById(R.id.llMyAppointments);
            LinearLayout    llChange            =   (LinearLayout)  convertView.findViewById(R.id.llChange);
            LinearLayout    llCallNow           =   (LinearLayout)  convertView.findViewById(R.id.llCallNow);
            LinearLayout    llSchedule          =   (LinearLayout)  convertView.findViewById(R.id.llSchedule);

            BaseActivity.setSpecificTypeFace(llMyAppointments,ApplicationConstants.WALSHEIM_MEDIUM);
            tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvCallNow.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvChange.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvSchedule.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

            tvChange.setText("RATE");
            ivChange.setImageResource(R.drawable.appointment_rating);
            tvSchedule.setText("REBOOK");
            ivSchedule.setImageResource(R.drawable.appointment_rebook);

            final PastFutureRow pastFutureRow = arrayRowsPast.get(position);
            if(pastFutureRow.isRatingEntered())
            {
                ivChange.setImageResource(R.drawable.rating_disable);
                tvChange.setTextColor(Color.parseColor("#b1b1b1"));
                llChange.setClickable(false);
                llChange.setEnabled(false);
            }

            if(pastFutureRow.getDoctorDetails().getProfileImage() != null && !pastFutureRow.getDoctorDetails().getProfileImage().isEmpty()) {
                Picasso.with(context).load(Uri.parse(pastFutureRow.getDoctorDetails().getProfileImage())).into(ivDoctorPic);
            }
            else
            {
                if(pastFutureRow.getDoctorDetails().getGender() == 'M')
                {
                    Picasso.with(context).load(R.drawable.male).into(ivDoctorPic);
                }
                else {
                    Picasso.with(context).load(R.drawable.female).into(ivDoctorPic);
                }
            }
            if(pastFutureRow.getDoctorDetails().getDoctorFullName().startsWith("Dr."))
                tvDoctorname.setText(pastFutureRow.getDoctorDetails().getDoctorFullName());
            else
                tvDoctorname.setText("Dr. "+pastFutureRow.getDoctorDetails().getDoctorFullName());
            tvHospitalName.setText(pastFutureRow.getAppointmentDetails().getClinicName());
            StringBuilder specialitybuilder = new StringBuilder();
            if(pastFutureRow.getDoctorDetails().getDoctorSpecialization() != null)
                for(String specialityid : pastFutureRow.getDoctorDetails().getDoctorSpecialization().values())
                {
                    specialitybuilder.append(Constants.specialitiesMap.get(specialityid)+", ");
                }
            try {
                tvSpeciality.setText(specialitybuilder.toString().substring(0, (specialitybuilder.toString().length()-2)));
            }catch (Exception e){
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeInMillis(pastFutureRow.getAppointmentDetails().getConsultationDt());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM dd");
            tvDateTime.setText(simpleDateFormat.format(calendar.getTime())+" @ "+pastFutureRow.getAppointmentDetails().getStartTime()+" Hrs");

            llSchedule.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    DoctorAddress doctorAddress = pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0);

                    SpecialityResRow specialityResRow = new SpecialityResRow();
                    specialityResRow.setProfileImage(pastFutureRow.getDoctorDetails().getProfileImage());
                    specialityResRow.setFullName(pastFutureRow.getDoctorDetails().getDoctorFullName());
                    specialityResRow.setClinicName(pastFutureRow.getAppointmentDetails().getClinicName());
                    specialityResRow.setSpecialityID(pastFutureRow.getDoctorDetails().getDoctorSpecialization().toString());
                    specialityResRow.setAddressID(pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0).getAddressID());
                    specialityResRow.setDoctorID(pastFutureRow.getAppointmentDetails().getDoctorID());
                    specialityResRow.setLatitude(doctorAddress.getExtdetails().getLatitude());
                    specialityResRow.setLongitude(doctorAddress.getExtdetails().getLongitude());
                    specialityResRow.setEmailID(pastFutureRow.getDoctorDetails().getArraydoctorEmail().get(0).getEmailID());
                    specialityResRow.setPhoneNumber(pastFutureRow.getDoctorDetails().getArraydoctorPhone().get(0).getPhNumber()+"");
                    specialityResRow.setAddress(pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0).getAddress1() + pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0).getAddress2());
                    specialityResRow.setGender(pastFutureRow.getDoctorDetails().getGender());
                    specialityResRow.setDistance(getDistance(doctorAddress.getExtdetails().getLatitude(), doctorAddress.getExtdetails().getLongitude()));

                    Intent intent = new Intent(context, ChooseDateTime.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(context.getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    intent.putExtra("SELECTED_DATAILS",specialityResRow);
                    intent.putExtra("ISFROMRESCHEDULE", false);
                    intent.putExtra("AppointmentID", pastFutureRow.getAppointmentDetails().getAppointmentID());
                    intent.putExtra("AppointmentStatus", pastFutureRow.getAppointmentDetails().getAppointmentStatus());

                    context.startActivity(intent, bndlanimation);
                }
            });
            llCallNow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+pastFutureRow.getDoctorDetails().getArraydoctorPhone().get(0).getPhNumber()+""));
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", pastFutureRow.getDoctorDetails().getArraydoctorPhone().get(0).getPhNumber()+"", null));
                    context.startActivity(intent);
                }
            });

            llChange.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(getActivity(), FeedBackActivity.class);
                    intent.putExtra("SELECTEDDETAILS",pastFutureRow);
                    startActivity(intent);
                }
            });

           /* tvShowMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    if(pastFutureRow.getDoctorDetails().getArraydoctorAddress() != null && !pastFutureRow.getDoctorDetails().getArraydoctorAddress().isEmpty())
                    {
                        DoctorAddress doctorAddress = pastFutureRow.getDoctorDetails().getArraydoctorAddress().get(0);
                        if(doctorAddress.getExtdetails() != null)
                        {
                            String latitude = doctorAddress.getExtdetails().getLatitude();
                            String longitude = doctorAddress.getExtdetails().getLongitude();

                            Uri location = Uri.parse(String.format("http://maps.google.com/?daddr="+latitude+","+longitude));
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

                            PackageManager packageManager = getActivity().getPackageManager();
                            List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                            boolean isIntentSafe = activities.size() > 0;

                            if (isIntentSafe) {
                                startActivity(mapIntent);
                            }
                            else
                            {
                                mcontext.ShowAlertDialog("Whoops!","Latlongs not found.",R.drawable.worng);
                            }
                        }
                    }

                    else
                    {
                        mcontext.ShowAlertDialog("Whoops!","Latlongs not found.",R.drawable.worng);
                    }
                }
            });*/
            return convertView;
        }

        public void refreshAdapter(ArrayList<PastFutureRow> arrayRows)
        {
            if(arrayRowsPast != null)
                arrayRowsPast.clear();
            arrayRowsPast = arrayRows;
            notifyDataSetChanged();
        }
    }
    private void callgetPastFutureAppointmentsService(final PastAdapter pastAdapter,int pagesize)
    {

        System.out.print("past records "+hasmultipleusers);
        MyAppointmentRequest myAppointmentRequest = new MyAppointmentRequest();
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.add(Calendar.DATE,1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        myAppointmentRequest.setBaseDate(simpleDateFormat.format(calendar.getTime()));
        myAppointmentRequest.setIndicator("P");
        myAppointmentRequest.setPageSize(pagesize);
        myAppointmentRequest.setHospitalCode("RVUYMISZZF");

        if(hasmultipleusers)
            myAppointmentRequest.setFamilyHdID(selectedPatienRow.getFamilyHdID());
        else
            myAppointmentRequest.setFamilyHdID(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFamilyHdID());
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPastFutureAppointments(myAppointmentRequest, new RestCallback<PastFutureAppointmentsRes>() {
            @Override
            public void failure(RestError restError)
            {

            }

            @Override
            public void success(PastFutureAppointmentsRes pastFutureAppointmentsRes, Response response) {
                lvCommon2.setVisibility(View.VISIBLE);
                llAvailability2.setVisibility(View.GONE);
                if(pastFutureAppointmentsRes != null && pastFutureAppointmentsRes.getArrayRows()!= null && !pastFutureAppointmentsRes.getArrayRows().isEmpty())
                {
                    pastAdapter.refreshAdapter(pastFutureAppointmentsRes.getArrayRows());
                }
                else
                {
                    if(pastFutureAppointmentsRes != null && pastFutureAppointmentsRes.getArrayRows()!= null)
                        pastAdapter.refreshAdapter(pastFutureAppointmentsRes.getArrayRows());
                    lvCommon2.setVisibility(View.GONE);
                    llAvailability2.setVisibility(View.VISIBLE);
                    tvNotAvailable2.setText("You do not have any past \n appointments");
                    tvNotAvailable2.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                }
            }
        });
    }
    private String getDistance(String eLatitude, String eLongitude){

        double latitude = getDouble(eLatitude);
        double longitude = getDouble(eLongitude);

        String distance = "NA";
        if(currentlat == null || currentlong == null || (latitude == -1d || longitude == -1d))
            return distance;
        Location locationA = new Location("point A");

        locationA.setLatitude(currentlat);
        locationA.setLongitude(currentlong);

        Location locationB = new Location("point B");

        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);

        float distanceValue = locationA.distanceTo(locationB);
        Float obj = distanceValue/1000;
        if(obj.floatValue() < 100.0f)
        {
            distance = "~" + String.valueOf(distanceValue / 1000).substring(0, 4) + " kms";
        }
        else
        {
            distance = "";
        }
        return distance;
    }

    private double getDouble(String doubleString)
    {
        double doubleValue = -1d;
        try{
            doubleValue = Double.parseDouble(doubleString);
        }
        catch (Exception e){}
        return doubleValue;
    }

}
