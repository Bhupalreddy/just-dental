package hCue.Hcue.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.DAO.ListQuestionsRequestDO;
import hCue.Hcue.DAO.ListQuestionsResponseDO;
import hCue.Hcue.DAO.PastFutureRow;
import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.QuestionsResponsDO;
import hCue.Hcue.FullScreenSlideActivity;
import hCue.Hcue.MyQuestions;
import hCue.Hcue.QuestionDetailsActivity;
import hCue.Hcue.R;
import hCue.Hcue.model.AddUpdatePatientOrderRequest;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.MySpannable;
import hCue.Hcue.utils.Preference;
import retrofit.client.Response;

/**
 * Created by User on 2/17/2017.
 */

public class MyQuestionsFragment extends BaseFragment
{
    private MyQuestionsAdapter myQuestionAdapter;
    private RecyclerView rvCommon;
    private LinearLayout llAvailability;
    private TextView tvNotAvailable;

    public MyQuestionsFragment()
    {
        super();
    }

    public static Fragment getInstance()
    {
        MyQuestionsFragment upcomingFragment = new MyQuestionsFragment();
        return upcomingFragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View  myQuestions     =   inflater.inflate(R.layout.questions_list, container, false);
        rvCommon              =   (RecyclerView)      myQuestions.findViewById(R.id.rvCommon);
        llAvailability        =   (LinearLayout)  myQuestions.findViewById(R.id.llAvailability);
        tvNotAvailable        =   (TextView)      myQuestions.findViewById(R.id.tvNotAvailable);

        tvNotAvailable.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        myQuestionAdapter = new MyQuestionsAdapter(getActivity(), new ArrayList<ListQuestionsResponseDO>());
//        btnAddMore = new Button(getActivity());
//        //btnAddMore.setLayoutParams(myLayoutParams);
//        btnAddMore.setText("Load More");
//        btnAddMore.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
//        btnAddMore.setBackgroundResource(R.color.white);
//        btnAddMore.setTextColor(getResources().getColor(R.color.gray));
//        btnAddMore.setVisibility(View.GONE);

        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvCommon.setLayoutManager(layoutManager);
        rvCommon.setAdapter(myQuestionAdapter);
        /*lvCommon1.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if(upcomingpagesize != 0)
                    if(lastInScreen == upcomingpagesize-1||lastInScreen == upcomingpagesize-2)
                    {
                        btnAddMore.setVisibility(View.VISIBLE);
                        btnAddMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                upcomingpagesize = upcomingpagesize+10;
                                callgetPastFutureAppointmentsService(upcomingAdapter,upcomingpagesize);
                                btnAddMore.setVisibility(View.GONE);
                            }
                        });
                    }
            }
        });*/

        return myQuestions;
    }
    @Override
    public void setupListeners()
    {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Connectivity.isConnected(getActivity()))
        {
            callMyQuestionsService();
        }else
        {
            mcontext.ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
        }
    }
    public class MyQuestionsAdapter extends RecyclerView.Adapter<MyQuestionsAdapter.MyQuestionsViewHolder> {
        private Context context;
        ArrayList<ListQuestionsResponseDO> listQuestions = new ArrayList<>();
        TextView tv;

        public MyQuestionsAdapter(Context context, ArrayList<ListQuestionsResponseDO> listQuestions) {
            this.context = context;
            this.listQuestions = listQuestions;
        }

        @Override
        public MyQuestionsAdapter.MyQuestionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.questions_cell, parent, false);
            return new MyQuestionsAdapter.MyQuestionsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyQuestionsViewHolder holder, int position) {

            tv = holder.tvQuestion;

            ListQuestionsResponseDO myQuestionDO = listQuestions.get(position);
            holder.llContainer.setTag(myQuestionDO);
            holder.tvQuestion.setText(myQuestionDO.getQuestion());
            calculateDaysBeforeAnswerGiven(holder.tvTime, myQuestionDO);
            if (myQuestionDO.getAnswerCount() == 0)
                holder.tvNoOfAnswers.setText("No Answers");
            else if (myQuestionDO.getAnswerCount() == 1)
                holder.tvNoOfAnswers.setText("1 Answer");
            else
                holder.tvNoOfAnswers.setText(myQuestionDO.getAnswerCount()+" Answers");

            holder.tvQuestion.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
            holder.tvNoOfAnswers.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            holder.tvTime.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            /*holder.tvQuestion.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount = tv.getLineCount();
                    if ((lineCount > 3))
                        makeTextViewResizable(tv, 3, "View More", true);
                }
            });*/

        }

        @Override
        public int getItemCount() {
            if (listQuestions != null && listQuestions.size() > 0)
                return listQuestions.size();
            else
                return 0;
        }

        public class MyQuestionsViewHolder extends RecyclerView.ViewHolder {
            private TextView tvQuestion, tvTime, tvNoOfAnswers;
            private LinearLayout llContainer;

            public MyQuestionsViewHolder(View itemView) {
                super(itemView);
                tvQuestion = (TextView) itemView.findViewById(R.id.tvQuestion);
                tvTime = (TextView) itemView.findViewById(R.id.tvTime);
                tvNoOfAnswers = (TextView) itemView.findViewById(R.id.tvNoOfAnswers);
                llContainer             = (LinearLayout) itemView.findViewById(R.id.llContainer);
                llContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ListQuestionsResponseDO questionDO = (ListQuestionsResponseDO)v.getTag();

                        Intent in = new Intent(getActivity(), QuestionDetailsActivity.class);
                        in.putExtra("isFromPrivateQuestions", true);
                        in.putExtra("questionDO", questionDO);
                        startActivity(in);
                    }
                });
            }
        }

        public void refreshAdapters(ArrayList<ListQuestionsResponseDO> listQuestionsResponseDOs) {
            listQuestions = listQuestionsResponseDOs;
            notifyDataSetChanged();
        }

        private void calculateDaysBeforeAnswerGiven(TextView tv, ListQuestionsResponseDO myQuestionDO) {

            Calendar cal = Calendar.getInstance();
            Calendar calQuestionDate = Calendar.getInstance();
            calQuestionDate.setTimeInMillis(myQuestionDO.getQuestionDate());
            int noOfDays = (int) TimeUnit.MILLISECONDS.toDays(Math.abs(cal.getTimeInMillis() - calQuestionDate.getTimeInMillis()));

            if (myQuestionDO.getQuestionDate() != 0){
                if (noOfDays > 6){
                    String date = DateUtils.getQuestionDate(myQuestionDO.getQuestionDate());
                        if (myQuestionDO.getQuestionSpeciality().getSpecialityDesc() != null && !myQuestionDO.getQuestionSpeciality().getSpecialityDesc().equalsIgnoreCase(""))
                            tv.setText(date +" - "+ myQuestionDO.getQuestionSpeciality().getSpecialityDesc() );
                        else
                            tv.setText(date);

                }else{
                    if (noOfDays == 0){
                        if (myQuestionDO.getQuestionSpeciality().getSpecialityDesc() != null && !myQuestionDO.getQuestionSpeciality().getSpecialityDesc().equalsIgnoreCase(""))
                            tv.setText("Today" +" - "+ myQuestionDO.getQuestionSpeciality().getSpecialityDesc() );
                        else
                            tv.setText("Today");
                    }
                    else{
                        if (myQuestionDO.getQuestionSpeciality().getSpecialityDesc() != null && !myQuestionDO.getQuestionSpeciality().getSpecialityDesc().equalsIgnoreCase(""))
                            tv.setText(noOfDays +" day(s) ago " +" - "+ myQuestionDO.getQuestionSpeciality().getSpecialityDesc() );
                        else
                            tv.setText(noOfDays +" day(s) ago ");
                    }
                }
            }
        }
    }

    private void callMyQuestionsService()
    {
        LoginResponse loginResponse  = LoginResponseSingleton.getSingleton();
        long patientID = loginResponse.getArrPatients().get(0).getPatientID()/*9952417722001l*/;


        ListQuestionsRequestDO requestDO = new ListQuestionsRequestDO();
        requestDO.setPatientID(patientID);
        requestDO.setIsPrivate("N");
        requestDO.setPageNumber(0);
        requestDO.setPageSize(50);
        requestDO.setIsPublicQuestion("N");

        showLoader("");
        RestClient.getAPI(Constants.DEVICE_REGISTRATION).listQuestionaries(requestDO, new RestCallback<QuestionsResponsDO>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
//                ((BaseActivity)getActivity()).ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(QuestionsResponsDO questionsResponsDO, Response response)
            {
                hideLoader();
                if (questionsResponsDO.getQuestionAnswersList() != null && questionsResponsDO.getQuestionAnswersList().size() > 0) {
                    myQuestionAdapter.refreshAdapters(questionsResponsDO.getQuestionAnswersList());

                    llAvailability.setVisibility(View.GONE);
                    tvNotAvailable.setText("No Private Questions Found");
                    rvCommon.setVisibility(View.VISIBLE);
                }else {
                    llAvailability.setVisibility(View.VISIBLE);
                    tvNotAvailable.setText("No Private Questions Found");
                    rvCommon.setVisibility(View.GONE);
                }
            }
        });
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = ((String)tv.getTag()).subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = ((String)tv.getTag()).subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = ((String)tv.getTag()).subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(true) {

                @Override
                public void onClick(View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        if (tv.getLineCount() > 3)
                            makeTextViewResizable(tv, -1, "View Less", false);
                        else
                            makeTextViewResizable(tv, -1, "", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        if (tv.getLineCount() > 3)
                            makeTextViewResizable(tv, 3, "View More", true);
                        else
                            makeTextViewResizable(tv, -1, "", false);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);
        }
        return ssb;

    }

}
