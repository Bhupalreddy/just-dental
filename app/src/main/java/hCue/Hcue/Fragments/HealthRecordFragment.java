package hCue.Hcue.Fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.DAO.PatientDocuments;
import hCue.Hcue.DAO.PatientFileRow;
import hCue.Hcue.DAO.UpdatePatientRecordRequestDO;
import hCue.Hcue.DAO.UploadfileRequest;
import hCue.Hcue.R;
import hCue.Hcue.SplashActivityOne;
import hCue.Hcue.model.FileDeleteReq;
import hCue.Hcue.model.PatientFilesResposne;
import hCue.Hcue.model.PatientsVisitsRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LargeImageLoader;
import hCue.Hcue.utils.MarshMallowPermission;
import hCue.Hcue.widget.FABToolbarLayout;
import retrofit.client.Response;
import retrofit.mime.TypedFile;


/**
 * Created by User on 2/17/2017.
 */

public class HealthRecordFragment extends BaseFragment {
    private Calendar selecteddate;
    ArrayList<PatientFileRow> documents;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    private ListView lvHealth;
    private LinearLayout llHealth, llCamera, llGallery, llFile, llAttachementPreview,llSearchLocation;
    private HealthRecordAdapter healthRecordAdapter;
    private TextView tvHealth, tvPicture, tvAlbum, tvFile, tvCancel, tvAdd, edtDOB;
    private FABToolbarLayout fabtoolbar;
    private RelativeLayout rlFab;
    private FloatingActionButton fabtoolbar_fab;
    private ImageView ivUploPic, ivHealth;
    private EditText edtDescription;
    private int REQUEST_CAMERA = 2;
    private int PICK_IMAGE_REQUEST = 1;
    private int PICK_FILE_REQUEST = 3;
    private TypedFile typedFile;
    private UploadfileRequest uploadfileRequest;
    private Animation slide_up, slide_down;
    private long patinetid;
    private Calendar updateRecordTime;
    long mLastClickTime = 0;
    private boolean isUpdatePatientRecord;
    private PatientDocuments updateDocumentDO;
    private MarshMallowPermission marshMallowPermission;
    private ArrayList<File> files = new ArrayList<File>();

    public static HealthRecordFragment getInstance(long patinetid) {
        HealthRecordFragment healthRecordFragment1 = new HealthRecordFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PATIENTID", patinetid);
        healthRecordFragment1.setArguments(bundle);
        return healthRecordFragment1;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        patinetid = (long) getArguments().getSerializable("PATIENTID");
        documents = new ArrayList<PatientFileRow>();
        selecteddate = Calendar.getInstance(Locale.getDefault());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Health = inflater.inflate(R.layout.health_records, container, false);
        lvHealth = (ListView) Health.findViewById(R.id.lvCommon);
        llHealth = (LinearLayout) Health.findViewById(R.id.llAvailability);
        llCamera = (LinearLayout) Health.findViewById(R.id.llCamera);
        llGallery = (LinearLayout) Health.findViewById(R.id.llGallery);
        llFile = (LinearLayout) Health.findViewById(R.id.llFile);
        rlFab = (RelativeLayout) Health.findViewById(R.id.rlFab);
        fabtoolbar = (FABToolbarLayout) Health.findViewById(R.id.fabtoolbar);
        fabtoolbar_fab = (FloatingActionButton) Health.findViewById(R.id.fabtoolbar_fab);
        tvHealth = (TextView) Health.findViewById(R.id.tvNotAvailable);
        tvPicture = (TextView) Health.findViewById(R.id.tvPicture);
        tvAlbum = (TextView) Health.findViewById(R.id.tvAlbum);
        tvFile = (TextView) Health.findViewById(R.id.tvFile);
        tvCancel = (TextView) Health.findViewById(R.id.tvCancel);
        tvAdd = (TextView) Health.findViewById(R.id.tvAdd);
        ivUploPic = (ImageView) Health.findViewById(R.id.ivUploPic);
        ivHealth = (ImageView) Health.findViewById(R.id.ivIcon);
        edtDescription = (EditText) Health.findViewById(R.id.edtDescription);
        edtDOB = (TextView) Health.findViewById(R.id.edtDOB);
        final EditText edtfilter = (EditText) Health.findViewById(R.id.edtfilter);
        LinearLayout llclose = (LinearLayout) Health.findViewById(R.id.llclose);
        llSearchLocation = (LinearLayout) Health.findViewById(R.id.llSearchLocation);
        llAttachementPreview = (LinearLayout) Health.findViewById(R.id.llAttachementPreview);

        mcontext.setSpecificTypeFace((ViewGroup) Health, ApplicationConstants.WALSHEIM_MEDIUM);

        tvPicture.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvAlbum.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvFile.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvHealth.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        marshMallowPermission  = new MarshMallowPermission(getActivity());

        slide_up = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);

        updateRecordTime = Calendar.getInstance(Locale.getDefault());

        healthRecordAdapter = new HealthRecordAdapter(getActivity(), documents, llSearchLocation);
        lvHealth.setAdapter(healthRecordAdapter);

        if (Connectivity.isConnected(getActivity())) {
            callgetUservisits(healthRecordAdapter, llSearchLocation);
        } else {
            mcontext.ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
        }
        rlFab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fabtoolbar.hide();
                if (rlFab.getVisibility() == View.VISIBLE)
                    rlFab.setVisibility(View.INVISIBLE);
                return false;
            }
        });

        fabtoolbar_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabtoolbar.show();
                if (rlFab.getVisibility() == View.INVISIBLE)
                    rlFab.setVisibility(View.VISIBLE);
            }
        });

        edtDOB.setText(simpleDateFormat.format(selecteddate.getTime()));
        edtDOB.setTag(selecteddate);
        edtDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance(Locale.getDefault());
//                    calendar.setTime(selecteddate.getTime());
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {


                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date MyDate = dateFormat.parse(day + "/" + (month + 1) + "/" + year);
                            selecteddate.setTime(MyDate);
                            edtDOB.setTag(selecteddate);
                            SimpleDateFormat newDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                            edtDOB.setText(newDateFormat1.format(MyDate));
                            updateRecordTime.setTime(MyDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), onDateSetListener, yy, mm, dd);
                datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (edtDescription.getText().toString().trim().isEmpty()) {
                    mcontext.ShowAlertDialog("Alert!", "Description should not be empty!", R.drawable.alert);
                } else {
                    if (isUpdatePatientRecord) {
                        isUpdatePatientRecord = false;
                        tvAdd.setText("ADD");
                        updatePatientRecord();
                    } else {
                        showLoader("");
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        uploadfileRequest.setUpLoadedDate(simpleDateFormat1.format(selecteddate.getTime()));
                        uploadfileRequest.setFileID(edtDescription.getText().toString());
                        Random rand = new Random();

                        int n = rand.nextInt(100) + 1;
                        uploadfileRequest.setDocumentNumber((selecteddate.getTimeInMillis() + n) + "");
                        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).uploadFile(uploadfileRequest, new RestCallback<String>() {
                            @Override
                            public void failure(RestError restError) {
                                Log.e("Whoops", restError.getErrorMessage());
                                hideLoader();
                                llAttachementPreview.setVisibility(View.GONE);
                                llAttachementPreview.startAnimation(slide_down);
                                mcontext.ShowAlertDialog("Whoops!", "" + restError.getErrorMessage(), R.drawable.worng);
                            }

                            @Override
                            public void success(String patientVisitRespone, Response response) {


                                llAttachementPreview.setVisibility(View.GONE);
                                llAttachementPreview.startAnimation(slide_down);
                                if (Connectivity.isConnected(getActivity())) {
                                    callgetUservisits(healthRecordAdapter, llSearchLocation);
                                } else {
                                    mcontext.ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
                                }

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideLoader();
                                        mcontext.ShowAlertDialog("Alert", "Your file is being verified and will be uploaded shortly.", R.drawable.alert);
                                    }
                                }, 1000);

                            }
                        });
                    }

                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAttachementPreview.setVisibility(View.GONE);
                llAttachementPreview.startAnimation(slide_down);
                tvAdd.setText("ADD");
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                SimpleDateFormat newDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                edtDOB.setText(newDateFormat1.format(calendar.getTime()));


            }
        });

        llCamera.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                edtDescription.setText("");
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    llCamera.setBackgroundColor(Color.parseColor("#33000000"));
                } else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    rlFab.setVisibility(View.INVISIBLE);
                    llCamera.setBackgroundColor(Color.parseColor("#00000000"));
                int currentAPIVersion = Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M)
                {
                   boolean result =  marshMallowPermission.checkPermissionForCamera();
                    if(result){
                        boolean result2 =  marshMallowPermission.checkPermissionForExternalStorage();
                        if(result2)
                        {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            getActivity().startActivityForResult(intent, REQUEST_CAMERA);
                        }
                        else
                        {
                            marshMallowPermission.requestPermissionForExternalStorage();
                        }

                    }
                    else{
                        marshMallowPermission.requestPermissionForCamera();
                    }
                }
                else
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    getActivity().startActivityForResult(intent, REQUEST_CAMERA);
                }
                }
                return true;
            }
        });
        llGallery.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                edtDescription.setText("");
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    llGallery.setBackgroundColor(Color.parseColor("#33000000"));

                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    rlFab.setVisibility(View.INVISIBLE);
                    llGallery.setBackgroundColor(Color.parseColor("#00000000"));
                    int currentAPIVersion = Build.VERSION.SDK_INT;
                    if (currentAPIVersion >= Build.VERSION_CODES.M)
                    {
                        boolean result =  marshMallowPermission.checkPermissionForExternalStorage();
                        if(result){

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

                        }
                        else{
                            marshMallowPermission.requestPermissionForExternalStorage();
                        }
                    }
                    else
                    {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

                    }
                        }
                return true;
            }
        });
        llFile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    llFile.setBackgroundColor(Color.parseColor("#33000000"));

                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    rlFab.setVisibility(View.INVISIBLE);
                    llFile.setBackgroundColor(Color.parseColor("#00000000"));
                    Intent intent = new Intent();
                    intent.setType("*/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    getActivity().startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FILE_REQUEST);
                }
                return true;
            }
        });

        return Health;
    }



    private String getPath(Uri uri) {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (isMediaDocument(uri)) {
            final String docId ;
                docId = DocumentsContract.getDocumentId(uri);
            final String[] split = docId.split(":");
            final String type = split[0];
            if ("image".equals(type)) {
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(type)) {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            selection = "_id=?";
            selectionArgs = new String[]{
                    split[1]
            };
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private void callgetUservisits(final HealthRecordAdapter healthRecordAdapter, LinearLayout llSearchLocation) {
        PatientsVisitsRequest visitsRequest = new PatientsVisitsRequest();
        visitsRequest.setPatientID(patinetid);
        visitsRequest.setScreenType("FILE");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientsFiles(visitsRequest, new RestCallback<PatientFilesResposne>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                mcontext.ShowAlertDialog("Whoops!", "Something went wrong. please try again.", R.drawable.worng);
            }

            @Override
            public void success(PatientFilesResposne patientFilesResposne, Response response) {
                hideLoader();
                if (patientFilesResposne != null && patientFilesResposne.getRows() != null && !patientFilesResposne.getRows().isEmpty()) {
                    llHealth.setVisibility(View.GONE);
                } else {
                    llHealth.setVisibility(View.VISIBLE);
                    tvHealth.setText("Safely store your health records & access any time you want.");
                    ivHealth.setBackgroundResource(R.drawable.health_records);
                }
                if (patientFilesResposne.getRows() != null)
                    healthRecordAdapter.refreshAdapter(patientFilesResposne.getRows());

            }
        });
    }

    @Override
    public void setupListeners() {

    }

    public class HealthRecordAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<PatientFileRow> documents;
        private ArrayList<PatientFileRow> originaldocuments;
        private LinearLayout llSearchLocation, llEdit, llShare;

        public HealthRecordAdapter(Context context, ArrayList<PatientFileRow> documents, LinearLayout llSearchLocation) {
            this.context = context;
            this.originaldocuments = documents;
            this.llSearchLocation = llSearchLocation;
        }

        @Override
        public int getCount() {
            if (documents == null || documents.isEmpty())
                return 0;
            else
                return documents.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(context).inflate(R.layout.health_records_cell, parent, false);

            TextView tvHeading = (TextView) convertView.findViewById(R.id.tvHeading);
            TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            TextView tvBy = (TextView) convertView.findViewById(R.id.tvBy);

            final ImageView ivRecordsPic = (ImageView) convertView.findViewById(R.id.ivRecordsPic);
            LinearLayout llDelete = (LinearLayout) convertView.findViewById(R.id.llDelete);
            LinearLayout llBg = (LinearLayout) convertView.findViewById(R.id.llBg);
            llEdit = (LinearLayout) convertView.findViewById(R.id.llEdit);
            llShare = (LinearLayout) convertView.findViewById(R.id.llShare);
            ImageView ivEdit = (ImageView) convertView.findViewById(R.id.ivEdit);
            ImageView ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);

            LinearLayout llHealth = (LinearLayout) convertView.findViewById(R.id.llHealth);
            mcontext.setSpecificTypeFace((ViewGroup) convertView, ApplicationConstants.WALSHEIM_MEDIUM);

            BaseActivity.setSpecificTypeFace(llHealth, ApplicationConstants.WALSHEIM_MEDIUM);

            PatientFileRow patientFileRow = documents.get(position);
            final PatientDocuments documentDAO = patientFileRow.getPatientDocuments();
            tvHeading.setText(documentDAO.getFileName());
            llEdit.setTag(documentDAO);

            llShare.setTag(documentDAO);
            Picasso.with(getActivity()).load(Uri.parse(documentDAO.getFileURL())).resize(150, 150).into(ivRecordsPic);


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeInMillis(documentDAO.getUpLoadedDate());
            tvDate.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
            tvDate.setText(simpleDateFormat.format(calendar.getTime()));
            if (documentDAO.getUpLoadedBy().equalsIgnoreCase("PATIENT")) {
                tvBy.setText("Added By you");
                ivEdit.setEnabled(false);
                ivDelete.setEnabled(false);

            } else {
                tvBy.setText("Added By " + documentDAO.getUpLoadedBy());
                ivEdit.setImageResource(R.drawable.edit_disabled);
                ivDelete.setImageResource(R.drawable.delete_disabled);
                llEdit.setEnabled(false);
                llDelete.setEnabled(false);

//                llShare.setTag(documentDAO.getFileURL().toString());
//                Picasso.with(getActivity()).load(documentDAO.getFileURL()).resize(150, 150).into(ivRecordsPic);
            }


            ivRecordsPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

                    // Include dialog.xml file
                    dialog.setContentView(R.layout.photo);
                    // Set dialog title
                    dialog.setTitle("");


                    // set values for custom dialog components - text, image and button
                    //TextView text = (TextView) dialog.findViewById(R.id.textDialog);
                    //text.setText("Custom dialog Android example.");
                    ImageView image = (ImageView) dialog.findViewById(R.id.imageDialog);
                    ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClose);

                    LargeImageLoader largeimageLoader = new LargeImageLoader(context.getApplicationContext());
                    ImageView image1 = image;
                    largeimageLoader.DisplayImage(documentDAO.getFileURL(), image1);

                    dialog.show();


                    // if decline button is clicked, close the custom dialog
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Close dialog
                            dialog.dismiss();
                        }
                    });
                    ivClose.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            dialog.dismiss();
                            return false;
                        }
                    });
                }
            });

            llDelete.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.home_pickup_dialog, null);
                        dialogBuilder.setView(dialogView);

                        TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
                        final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                        TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
                        ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

                        ivLogo.setVisibility(View.VISIBLE);

                        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
                        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

                        final AlertDialog alertDialog = dialogBuilder.create();
                        alertDialog.show();

                        tvInfo.setText("Do you want to delete this health record?");
                        tvInfo.setTextSize(18);
                        tvHeading.setText("Confirmation");

                        tvYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                showLoader("");
                                FileDeleteReq fileDeleteReq = new FileDeleteReq();
                                fileDeleteReq.setPatientID(patinetid);
                                fileDeleteReq.setFileExtn(documentDAO.getFileExtn());
                                fileDeleteReq.setDocumentNumber(documentDAO.getDocumentNumber());
                                RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).deleteFile(fileDeleteReq, new RestCallback<String>() {
                                    @Override
                                    public void failure(RestError restError) {
                                        Log.e("Whoops", restError.getErrorMessage());
                                        hideLoader();
                                        mcontext.ShowAlertDialog("Whoops!", "" + restError.getErrorMessage(), R.drawable.worng);
                                    }

                                    @Override
                                    public void success(String patientVisitRespone, Response response) {

                                        hideLoader();
                                        mcontext.ShowAlertDialog("Alert", "Deleted succesfully!", R.drawable.alert);
                                        callgetUservisits(HealthRecordAdapter.this, llSearchLocation);

                                    }
                                });
                            }
                        });

                        tvNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });

                        return true;
                    }
                    return true;
                }
            });

            llEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isUpdatePatientRecord = true;
                    llAttachementPreview.setVisibility(View.VISIBLE);
                    tvAdd.setText("UPDATE");
                    llAttachementPreview.startAnimation(slide_up);
                    PatientDocuments documentDO = (PatientDocuments) v.getTag();

                    updateDocumentDO = documentDO;

                    Picasso.with(getActivity()).load(documentDO.getFileURL()).resize(150, 150).into(ivUploPic);
                    edtDescription.setText(documentDO.getFileName());


                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
//                    SimpleDateFormat newDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");

                    // Create a calendar object that will convert the date and time value in milliseconds to date.
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(documentDO.getUpLoadedDate());
                    edtDOB.setText(formatter.format(calendar.getTime()));

                }
            });

            llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PatientDocuments documentDO = (PatientDocuments) v.getTag();
                    shareImg(documentDO.getFileURL());
                }
            });

            return convertView;
        }

        public void refreshAdapter(ArrayList<PatientFileRow> listPatientCaserows) {
            if (originaldocuments != null)
                originaldocuments.clear();
            ArrayList<PatientFileRow> listDocumentDAO = new ArrayList<>();

            originaldocuments.addAll(listPatientCaserows);
            documents = listPatientCaserows;
            notifyDataSetChanged();

        }
    }


    public void updatePatientRecord() {
        showLoader("");
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

        UpdatePatientRecordRequestDO requestDO = new UpdatePatientRecordRequestDO();
        requestDO.setUSRId(patinetid);
        requestDO.setPatientID(patinetid);
        requestDO.getPatientDetails().setPatientID(patinetid);
        requestDO.getPatientDocuments().setDocumentNumber(updateDocumentDO.getDocumentNumber());
        requestDO.getPatientDocuments().setFileID(edtDescription.getText().toString());
        requestDO.getPatientDocuments().setUpLoadedDate(simpleDateFormat1.format(updateRecordTime.getTime()));


        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).updatePatientRecord(requestDO, new RestCallback<Object>() {
            @Override
            public void failure(RestError restError) {
                Log.e("Whoops", restError.getErrorMessage());
                hideLoader();
                llAttachementPreview.setVisibility(View.GONE);
                llAttachementPreview.startAnimation(slide_down);
                mcontext.ShowAlertDialog("Whoops!", "" + restError.getErrorMessage(), R.drawable.worng);
            }

            @Override
            public void success(Object objResponse, Response response) {

                if (objResponse != null) {
                    tvAdd.setText("ADD");
                    llAttachementPreview.setVisibility(View.GONE);
                    llAttachementPreview.startAnimation(slide_down);
                    if (Connectivity.isConnected(getActivity())) {
                        callgetUservisits(healthRecordAdapter, llSearchLocation);
                    } else {
                        mcontext.ShowAlertDialog("Whoops!", "No Internet connection found. Check your connection or try again.", R.drawable.no_internet);
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideLoader();
                            mcontext. ShowAlertDialog("Alert", "Your file is being verified and will be uploaded shortly.", R.drawable.alert);
                        }
                    }, 1000);
                }
            }
        });
    }
    public void shareImg(String arrUrl)
    {

//        delete = true;
        ArrayList<Uri> imageUris = new ArrayList<Uri>();
//		for(String path : arrUrl /* List of the files you want to send */)
//        {
//            showLoader("");

        String uri=    convertURLtoBitmapShare(arrUrl);
        File file = new File(uri);
        files.add(file);
        Uri uri2 = Uri.fromFile(file);
        imageUris.add(uri2);
//            hideLoader();
//		}
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
        shareIntent.setType("image/*");
//        shareIntent.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(shareIntent, "Share images to.."));
        hideLoader();
    }
    public String convertURLtoBitmapShare(String src)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {

            URL url = new URL(src);

            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();

            connection.setDoInput(true);
            connection.connect();

            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            File f = new File(Environment.getExternalStorageDirectory() + File.separator + System.currentTimeMillis()+".jpg");
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return f.toString();

        } catch (IOException e) {

            e.printStackTrace();
            return null;

        }
    }
    public void PICK_FILE_REQUEST(final Intent data){
        new Thread(new Runnable() {
            @Override
            public void run() {

                uploadfileRequest = new UploadfileRequest();
                try {
                    final Uri uri = data.getData();
                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(projection[0]);
                    String picturePath = cursor.getString(columnIndex); // returns null
                    cursor.close();
                    Log.d("PATH", picturePath);
                    final File file = new File(picturePath);
                    long fileSizeInBytes = file.length();
// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;
                    int index = picturePath.lastIndexOf(".");
                    byte[] bFile = new byte[(int) file.length()];
                    FileInputStream fileInputStream = new FileInputStream(file);
                    fileInputStream.read(bFile);
                    fileInputStream.close();
                    String encodedString = Base64.encodeToString(bFile, Base64.DEFAULT);

                    Log.e("EXTENSION", picturePath.substring(index, picturePath.length()));
                    uploadfileRequest.setFileExtn(picturePath.substring(index, picturePath.length()));
                    uploadfileRequest.setPatientID(patinetid);
                    uploadfileRequest.setImageStr(encodedString);

                    if (fileSizeInMB < 5) {
                        //typedFile = new TypedFile("multipart/form-data", destination);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                file.deleteOnExit();
                                hideLoader();
                                Picasso.with(getActivity()).load(uri).resize(150, 150).into(ivUploPic);
                                fabtoolbar.hide();
                                llAttachementPreview.setVisibility(View.VISIBLE);
                                Calendar cal = Calendar.getInstance(Locale.getDefault());
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                                edtDOB.setText(sdf.format(cal.getTime()));
                                selecteddate = cal;
                                llAttachementPreview.startAnimation(slide_up);

                            }
                        });


                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                file.deleteOnExit();
                                hideLoader();
                                mcontext.ShowAlertDialog("Alert!", "Sorry, The file could not be uploaded as it exceeds the size of 5MB.", R.drawable.alert);
                            }
                        });

                    }


                    // Log.d(TAG, String.valueOf(bitmap));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void PICK_IMAGE_REQUEST(final Intent data){
        showLoader("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                uploadfileRequest = new UploadfileRequest();
                try {
                    final Uri uri = data.getData();
                    String picturePath = getPath(uri);
                    Log.d("PATH", picturePath);
                    final File file = new File(picturePath);
                    long fileSizeInBytes = file.length();
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    long fileSizeInMB = fileSizeInKB / 1024;
                    byte[] bytes1 = new byte[(int) fileSizeInBytes];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes1, 0, bytes1.length);
                        buf.close();
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    int index = picturePath.lastIndexOf(".");
                    Log.e("EXTENSION", picturePath.substring(index, picturePath.length()));
                    String encodedString = Base64.encodeToString(bytes1, Base64.DEFAULT);
                    uploadfileRequest.setFileExtn(picturePath.substring(index, picturePath.length()));
                    uploadfileRequest.setPatientID(patinetid);
                    uploadfileRequest.setImageStr(encodedString);

                    if (fileSizeInMB < 5) {
                        //typedFile = new TypedFile("multipart/form-data", destination);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Picasso.with(getActivity()).load(uri).resize(150, 150).into(ivUploPic);
                                file.deleteOnExit();
                                hideLoader();
                                fabtoolbar.hide();
                                llAttachementPreview.setVisibility(View.VISIBLE);
                                Calendar cal = Calendar.getInstance(Locale.getDefault());
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                                edtDOB.setText(sdf.format(cal.getTime()));
                                selecteddate = cal;
                                llAttachementPreview.startAnimation(slide_up);

                            }
                        });


                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                file.deleteOnExit();
                                hideLoader();
                                mcontext.ShowAlertDialog("Alert!", "Sorry, The file could not be uploaded as it exceeds the size of 5MB.", R.drawable.alert);
                            }
                        });

                    }
                    // Log.d(TAG, String.valueOf(bitmap));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void REQUEST_CAMERA(final Intent data)
    {
        showLoader("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                uploadfileRequest = new UploadfileRequest();
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".png");
                final Uri uri = Uri.fromFile(destination);

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();

                    typedFile = new TypedFile("multipart/form-data", destination);
                    DisplayMetrics metrics = new DisplayMetrics();
                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    Log.e("DENSITY", "" + metrics.widthPixels + " , " + metrics.heightPixels);
                    Log.e("DENSITY", "" + ((metrics.widthPixels * 3) / 4) + " , " + ((metrics.heightPixels * 3) / 4));
                    //Bitmap bitmap = Picasso.with(getActivity()).load(uri).centerInside().resize((metrics.heightPixels / 2), (metrics.heightPixels / 2)).get();
                    long fileSizeInBytes = destination.length();
// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;

                    byte[] bytes1 = new byte[(int) fileSizeInBytes];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(destination));
                        buf.read(bytes1, 0, bytes1.length);
                        buf.close();
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    String encodedString = Base64.encodeToString(bytes1, Base64.DEFAULT);
                    uploadfileRequest.setFileExtn(".png");
                    uploadfileRequest.setPatientID(patinetid);
                    uploadfileRequest.setImageStr(encodedString);
                    if (fileSizeInMB < 5) {
                        //typedFile = new TypedFile("multipart/form-data", destination);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoader();
                                Picasso.with(getActivity()).load(uri).resize(150, 150).into(ivUploPic);
                                fabtoolbar.hide();
                                llAttachementPreview.setVisibility(View.VISIBLE);
                                Calendar cal = Calendar.getInstance(Locale.getDefault());
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                                edtDOB.setText(sdf.format(cal.getTime()));
                                selecteddate = cal;
                                llAttachementPreview.startAnimation(slide_up);

                            }
                        });


                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoader();
                                mcontext.ShowAlertDialog("Alert!", "Sorry, The file could not be uploaded as it exceeds the size of 5MB.", R.drawable.alert);
                            }
                        });

                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}