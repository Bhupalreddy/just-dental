package hCue.Hcue.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.CallBackInterfaces.ProfileDataInterface;
import hCue.Hcue.DAO.PatientAddress;
import hCue.Hcue.R;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.LoginResponseSingleton;


/**
 * Created by User on 2/18/2017.
 */

public class GeneralFragment extends BaseFragment
{
    private TextView emailVerifyHint;
    private Calendar patientDOB ;
    private LoginResponse loginResponse ;
    private TextView  tvGender,  tvDOB, tvAddress,tvStateUpdate, tvLandmark;
    private ProfileDataInterface profileDataInterface;

    public GeneralFragment()
    {
        super();
    }

    public static Fragment getInstance(PatientUpdateRequest patientUpdateRequest)
    {
        GeneralFragment  generalFragment = new GeneralFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PatientUpdateRequest",patientUpdateRequest);
        generalFragment.setArguments(bundle);
        return generalFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View            General         =   inflater.inflate(R.layout.fragment_general, container, false);
        tvGender                        =   (TextView)     General.findViewById(R.id.tvGender);
        tvDOB                           =   (TextView)     General.findViewById(R.id.tvDOB);
        TextView        tvMobile        =   (TextView)     General.findViewById(R.id.tvMobile);
        TextView        tvEmail         =   (TextView)     General.findViewById(R.id.tvEmail);
        tvAddress                       =   (TextView)     General.findViewById(R.id.tvAddress);
        tvStateUpdate                   =   (TextView)     General.findViewById(R.id.tvStateUpdate);
        tvLandmark                      =   (TextView)     General.findViewById(R.id.tvLandmark);
        emailVerifyHint                 =   (TextView)     General.findViewById(R.id.emailVerifyHint);


        final LinearLayout llGeneralUpdate     =   (LinearLayout)      General.findViewById(R.id.llGeneralUpdate);
        final LinearLayout  llGeneral       =   (LinearLayout)      General.findViewById(R.id.llGeneral);


        BaseActivity.setSpecificTypeFace(llGeneral, ApplicationConstants.WALSHEIM_MEDIUM);
        profileDataInterface = (ProfileDataInterface) getActivity();

        loginResponse           = LoginResponseSingleton.getSingleton();
        patientDOB = Calendar.getInstance(Locale.getDefault());
        patientDOB.setTimeInMillis(loginResponse.getArrPatients().get(0).getDOB());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        tvDOB.setText(newDateFormat.format(patientDOB.getTime()));

        if(loginResponse.getArrPatients().get(0).getGender() == 'M')
            tvGender.setText("Male");
        else
            tvGender.setText("Female");


        if(loginResponse.getListPatientPhone() != null && !loginResponse.getListPatientPhone().isEmpty())
            tvMobile.setText(loginResponse.getListPatientPhone().get(0).getPhNumber()+"");
        if(loginResponse.getListPatientEmail() != null && !loginResponse.getListPatientEmail().isEmpty()){
            tvEmail.setText(loginResponse.getListPatientEmail().get(0).getEmailID());
            emailVerifyHint.setVisibility(View.VISIBLE);
        }
        else
            emailVerifyHint.setVisibility(View.GONE);

        if(loginResponse.getListPatientAddress() != null && !loginResponse.getListPatientAddress().isEmpty())
        {
            PatientAddress patientAddress = loginResponse.getListPatientAddress().get(0);
            tvAddress.setText(patientAddress.getAddress1());
            PatientAddress patientState = loginResponse.getListPatientAddress().get(0);
            tvStateUpdate.setText(patientAddress.getAddress2());
            if(patientAddress.getLandMark() != null && !patientAddress.getLandMark().isEmpty())
                tvLandmark.setText(patientAddress.getLandMark());
        }

        llGeneralUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Gender,Landmark,Address,StateUpdate,DOB;

                Gender = tvGender.getText().toString();

                if(tvLandmark.getText().toString().isEmpty())
                {
                    Landmark = "";
                }
                else {
                    Landmark = tvLandmark.getText().toString();
                }

                if(tvAddress.getText().toString().isEmpty())
                {
                    Address = "";
                }
                else {
                    Address = tvAddress.getText().toString();
                }
                if(tvStateUpdate.getText().toString().isEmpty())
                {
                    StateUpdate = "";
                }
                else
                {
                    StateUpdate = tvStateUpdate.getText().toString();
                }

                if(tvDOB.getText().toString().isEmpty())
                {
                    DOB = "";
                }
                else {
                    DOB = tvDOB.getText().toString();
                }
                profileDataInterface.GeneralUpdate(v,Gender,Landmark,Address,StateUpdate,DOB,patientDOB);
            }
        });


        return General;
    }


    @Override
    public void setupListeners() {

    }

    public void setdata(LoginResponse loginResponse) {
        if (loginResponse.getArrPatients().get(0).getGender() == 'M')
            tvGender.setText("Male");
        else
            tvGender.setText("Female");
        patientDOB = Calendar.getInstance(Locale.getDefault());
        patientDOB.setTimeInMillis(loginResponse.getArrPatients().get(0).getDOB());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        tvDOB.setText(newDateFormat.format(patientDOB.getTime()));
        if (loginResponse.getListPatientAddress() != null && loginResponse.getListPatientAddress().size() > 0) {
            tvAddress.setText(loginResponse.getListPatientAddress().get(0).getAddress1());
            tvStateUpdate.setText(loginResponse.getListPatientAddress().get(0).getAddress2());
            tvLandmark.setText(loginResponse.getListPatientAddress().get(0).getLandMark());
        }
    }
}