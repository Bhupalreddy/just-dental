package hCue.Hcue.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.CallBackInterfaces.ProfileDataInterface;
import hCue.Hcue.DAO.PatientVitals;
import hCue.Hcue.DAO.Vitals;
import hCue.Hcue.R;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.LoginResponseSingleton;

/**
 * Created by User on 2/18/2017.
 */

public class VitalsFragment extends BaseFragment
{
    private TextView tvBloodGroup, tvWeight, tvHeight,tvTemp;
    private LoginResponse loginResponse ;
    private ProfileDataInterface profileDataInterface;
    public VitalsFragment()
    {
        super();
    }

    public static Fragment getInstance(PatientUpdateRequest patientUpdateRequest)
    {
        VitalsFragment  vitalsFragment = new VitalsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PatientUpdateRequest",patientUpdateRequest);
        vitalsFragment.setArguments(bundle);
        return vitalsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View            Vitals          =   inflater.inflate(R.layout.fragment_vitals, container, false);
        tvHeight                        =   (TextView)       Vitals.findViewById(R.id.tvHeight);
        tvWeight                        =   (TextView)       Vitals.findViewById(R.id.tvWeight);
        tvTemp                          =   (TextView)       Vitals.findViewById(R.id.tvTemp);
        tvBloodGroup                    =   (TextView)       Vitals.findViewById(R.id.tvBloodGroup);
        ImageView ivHelp          =   (ImageView)      Vitals.findViewById(R.id.ivHelp);
        final LinearLayout llVitals        =   (LinearLayout)   Vitals.findViewById(R.id.llVitals);
        LinearLayout    llVitalUpdate   =   (LinearLayout)   Vitals.findViewById(R.id.llVitalUpdate);
        LinearLayout    llHeight        =   (LinearLayout)   Vitals.findViewById(R.id.llHeight);
        LinearLayout    llweight        =   (LinearLayout)   Vitals.findViewById(R.id.llweight);
        LinearLayout    llbloudbroup    =   (LinearLayout)   Vitals.findViewById(R.id.llbloudbroup);

        loginResponse           = LoginResponseSingleton.getSingleton();
        profileDataInterface = (ProfileDataInterface) getActivity();
        if(loginResponse.getPatientVitals() != null)
        {
            if(loginResponse.getPatientVitals().getVitals() != null)
            {
                hCue.Hcue.DAO.Vitals vitals = loginResponse.getPatientVitals().getVitals();
                if(vitals.getBLG() != null)
                    tvBloodGroup.setText(vitals.getBLG());
                if(vitals.getHGT() != null)
                    tvHeight.setText(vitals.getHGT());
                if(vitals.getWGT() != null)
                    tvWeight.setText(vitals.getWGT());
                if(vitals.getHGT() != null && vitals.getWGT()!= null && !vitals.getHGT().isEmpty() && !vitals.getWGT().isEmpty())
                {
                    try {
                        double height = Float.parseFloat(vitals.getHGT())/100;
                        float weight = Float.parseFloat(vitals.getWGT());
                        String bmi = (weight/(height * height))+"";
                        if(bmi.length()>7)
                        {
                            tvTemp.setText(bmi.substring(0,7));
                        }else
                        {
                            tvTemp.setText(bmi);
                        }
                    }catch (Exception  e)
                    {
                        Log.e("Arithmethic exception",""+e.getMessage());
                    }
                }

            }
        }
        llVitalUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Height,Weight,BloodGroup;

                if(tvHeight.getText().toString().isEmpty()) {
                    Height = "";
                }
                else {
                    Height =tvHeight.getText().toString();
                }
                if(tvWeight.getText().toString().isEmpty())
                {
                    Weight="";
                }
                else {
                    Weight =tvWeight.getText().toString();
                }
                if(tvBloodGroup.getText().toString().isEmpty())
                {
                    BloodGroup="";
                }
                else
                {
                    BloodGroup =tvBloodGroup.getText().toString();
                }
                profileDataInterface.VitalUpdate(v,Height,Weight,BloodGroup);
            }
        });



        ivHelp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                switch (event.getAction())
                {
                    case MotionEvent.ACTION_UP:
                        profileDataInterface.ivHelp();
                        break;
                }

                return true;
            }
        });

        BaseActivity.setSpecificTypeFace(llVitals, ApplicationConstants.WALSHEIM_MEDIUM);

        return Vitals;
    }

    @Override
    public void setupListeners() {

    }
    public void setdata(String hgt, String wgt, String blg, PatientVitals patientVitals)
    {
        tvHeight.setText(hgt);

        tvWeight.setText(wgt);

        tvBloodGroup.setText(blg);
        updateBMI(patientVitals);
    }
    private void updateBMI(PatientVitals patientVitals)
    {
        if(patientVitals != null)
        {
            if(patientVitals.getVitals() != null)
            {
                Vitals vitals = patientVitals.getVitals();
                if(vitals.getBLG() != null)
                    tvBloodGroup.setText(vitals.getBLG());
                if(vitals.getHGT() != null)
                    tvHeight.setText(vitals.getHGT());
                if(vitals.getWGT() != null)
                    tvWeight.setText(vitals.getWGT());
                if(vitals.getHGT() != null && vitals.getWGT()!= null && !vitals.getHGT().isEmpty() && !vitals.getWGT().isEmpty())
                {
                    try {
                        double height = Float.parseFloat(vitals.getHGT())/100;
                        float weight = Float.parseFloat(vitals.getWGT());
                        String bmi = (weight/(height * height))+"";
                        if(bmi.length()>5)
                        {
                            tvTemp.setText(bmi.substring(0,5));
                        }else
                        {
                            tvTemp.setText(bmi);
                        }
                    }catch (Exception  e)
                    {
                        Log.e("Arithmethic exception",""+e.getMessage());
                    }
                }

            }
        }
    }

}
