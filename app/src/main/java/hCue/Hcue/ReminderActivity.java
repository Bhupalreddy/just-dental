package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.DAO.ReminderDetailsDO;
import hCue.Hcue.adapters.RemindersDbAdapter;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.ExitActivity;
import hCue.Hcue.utils.Preference;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by shyamprasadg on 06/08/16.
 */
public class ReminderActivity extends AppCompatActivity
{
//    private GifImageView gifImage;
    private  FrameLayout llRemainder, llContainer;
    AudioManager mode;
    private Vibrator vibrator;
    MediaPlayer mMediaPlayer;
    private TextView tvMedicine, tvTime, tvSwipe;
    private ListView lvMedicines;
    private MedicineReminderAdapter adapter;
    private ArrayList<ReminderDetailsDO> listReminders;
    long rowID;
    Preference preference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reminder_activity);
        preference = new Preference(ReminderActivity.this);

        /*if (preference.getbooleanFromPreference("reminderDismissed", false)) {
            finish();
            Log.i("Reminder Activity", "Inside listReminder empty condtion");
        }else{*/
            Log.i("Reminder Activity", "Started Reminder Activity");

            mode            = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            vibrator        = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            rowID           = getIntent().getLongExtra(RemindersDbAdapter.KEY_ROWID, 0);
            listReminders   = (ArrayList<ReminderDetailsDO>) getIntent().getSerializableExtra("listReminders");

            tvMedicine      = (TextView) findViewById(R.id.tvMedicine);
            tvTime          = (TextView) findViewById(R.id.tvTime);
            tvSwipe          = (TextView) findViewById(R.id.tvSwipe);
            lvMedicines     = (ListView) findViewById(R.id.lvMedicines);
            llContainer     = (FrameLayout) findViewById(R.id.llContainer);

            tvMedicine.setText("Hey, "+listReminders.get(0).getPatientName() + " Its time to take your medicine");
            tvTime.setText(listReminders.get(0).getTime());

            adapter = new MedicineReminderAdapter(ReminderActivity.this, listReminders);
            lvMedicines.setAdapter(adapter);

            makePhoneRinger();
            Log.i("Reminder Activity", "After Reminder Activity Ringing");
//        setSpecificTypeFace(llRemainder, ApplicationConstants.WALSHEIM_MEDIUM);

            llContainer.setOnTouchListener(new SwipeGestureListener(ReminderActivity.this) {

                public void onSwipeTop() {}
                public void onSwipeRight() {
                    cancelAlaram();
                }
                public void onSwipeLeft() {
                    cancelAlaram();
                }
                public void onSwipeBottom() {}

                @Override
                public void onNothing() {}
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor("#28465a"));
            }
//        }


//        llTop.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {

        Log.i("Reminder Activity", "OnResume Reminder Activity");


        /*if (preference.getbooleanFromPreference("reminderDismissed", false)) {
            finish();
            Log.i("Reminder Activity", "Inside listReminder empty condtion");
        }*/
        super.onResume();
    }



    public void makePhoneRinger() {

        Log.i("Reminder Activity", "Started Reminder Activity Ringing");

        // Set ringer mode to ringer normal
        mode.setRingerMode(AudioManager.FLAG_ALLOW_RINGER_MODES);
        if(vibrator.hasVibrator()){
            vibrator.vibrate(15000);
        }

        try {
            Uri alert =  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(this, alert);
            final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        // Change silent and vibrate buttons' bgcolor to red
        // Change ringer normal button's bgcolor to yellow
    }

    //Swipe Gesture Listener class

    public class SwipeGestureListener implements View.OnTouchListener {

        private final GestureDetector gestureDetector;

        public SwipeGestureListener(Context ctx) {
            gestureDetector = new GestureDetector(ctx, new GestureListener());
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }

        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                        }else{
                            onNothing();
                        }
                        result = true;
                    } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                    }else {
                        onNothing();
                    }
                    result = true;

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }

        public void onSwipeRight() {
        }

        public void onSwipeLeft() {
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }
        public void onNothing() {
        }
    }

    public void cancelAlaram() {
        mMediaPlayer.pause();
        vibrator.cancel();
        /*if (isTaskRoot()){
            preference.saveBooleanInPreference("reminderDismissed",true);
        }else{
            preference.saveBooleanInPreference("reminderDismissed",false);
        }
        preference.commitPreference();*/
        finishAffinity();
        ExitActivity.exitApplication(ReminderActivity.this);
    }

    public class MedicineReminderAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<ReminderDetailsDO> listReminders;

        public MedicineReminderAdapter(Context context,  ArrayList<ReminderDetailsDO> listReminders) {
            this.listReminders = listReminders;
            this.context = context;
        }


        @Override
        public int getCount() {
            if (listReminders == null || listReminders.isEmpty())
                return 0;
            else
                return listReminders.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(context).inflate(R.layout.remainder_activity_cell, parent, false);

            TextView tvMedicineName = (TextView) convertView.findViewById(R.id.tvMedicineName);
//            setSpecificTypeFace((ViewGroup) convertView, ApplicationConstants.WALSHEIM_MEDIUM);

            LinearLayout llMedicine = (LinearLayout) convertView.findViewById(R.id.llMedicine);

            BaseActivity.setSpecificTypeFace(llMedicine, ApplicationConstants.WALSHEIM_MEDIUM);

            final ReminderDetailsDO reminderDetailsDO = listReminders.get(position);

            tvMedicineName.setText(reminderDetailsDO.getMedicineName());

            return convertView;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
