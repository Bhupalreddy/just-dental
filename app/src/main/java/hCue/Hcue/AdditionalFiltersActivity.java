package hCue.Hcue;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.edmodo.rangebar.RangeBar;
import com.google.android.gms.ads.formats.NativeAd;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import hCue.Hcue.model.AdditionalFiltersDO;
import hCue.Hcue.utils.ApplicationConstants;


/**
 * Created by shyamprasadg on 03/06/16.
 */
public class AdditionalFiltersActivity extends BaseActivity implements View.OnClickListener
{
    private LinearLayout llMale, llFemale,llAdditionalFilters;
    private TextView tvMinFee, tvMaxFee, tvAny, tvMon, tvTue, tvWed, tvThu, tvFri, tvSat, tvSun, tvStartTime, tvEndTime, tvApply;
    private RangeBar rBarFee, rBarTime;
    private CheckBox cbBook, cbCall, cbAvailability;
    private RadioButton rbAscending, rbDescending;
    private TextView tvExperience, tvDistance;
    private ImageView ivMale, ivFemale;
    private String selectedDay = "", call="Y", book = "Y", gender, availability = "N", sortType = "ASC", sortCategory = "DIST", startTime, endTime, startFee, endFee;
    private ArrayList<Integer> listFee = null;
    private ArrayList<String> listConsultationTime = null;
    private ArrayList<String> listConsultationTimeService = null;
    private ArrayList<String> listSelectedDay = new ArrayList<>();
    private int tickCount = 0;
    private int consultationTickCount = 0;
    AdditionalFiltersDO filtersDO = new AdditionalFiltersDO();
    private boolean isResetActive = false, isGenderBothSelected;
    @Override
    public void initialize() {

        filtersDO = (AdditionalFiltersDO) getIntent().getSerializableExtra("FiltersDO");

        llAdditionalFilters = (LinearLayout) getLayoutInflater().inflate(R.layout.additional_filters, null, false);
        llBody.addView(llAdditionalFilters, baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);

        tvTopTitle.setVisibility(View.VISIBLE);
        llClose.setVisibility(View.VISIBLE);
        tvTopTitle.setText("Filters");
        llSave.setVisibility(View.VISIBLE);
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.GONE);
        tvSave.setText("Reset");

        llMale = (LinearLayout) llAdditionalFilters.findViewById(R.id.llMale);
        llFemale = (LinearLayout) llAdditionalFilters.findViewById(R.id.llFemale);
        tvMinFee = (TextView) llAdditionalFilters.findViewById(R.id.tvMinFee);
        tvMaxFee = (TextView) llAdditionalFilters.findViewById(R.id.tvMaxFee);
        tvAny = (TextView) llAdditionalFilters.findViewById(R.id.tvAny);
        tvMon = (TextView) llAdditionalFilters.findViewById(R.id.tvMon);
        tvTue = (TextView) llAdditionalFilters.findViewById(R.id.tvTue);
        tvWed = (TextView) llAdditionalFilters.findViewById(R.id.tvWed);
        tvThu = (TextView) llAdditionalFilters.findViewById(R.id.tvThu);
        tvFri = (TextView) llAdditionalFilters.findViewById(R.id.tvFri);
        tvSat = (TextView) llAdditionalFilters.findViewById(R.id.tvSat);
        tvSun = (TextView) llAdditionalFilters.findViewById(R.id.tvSun);
        tvStartTime = (TextView) llAdditionalFilters.findViewById(R.id.tvStartTime);
        tvEndTime = (TextView) llAdditionalFilters.findViewById(R.id.tvEndTime);
        tvApply = (TextView) llAdditionalFilters.findViewById(R.id.tvApply);
        ivMale = (ImageView) llAdditionalFilters.findViewById(R.id.ivMale);
        ivFemale = (ImageView) llAdditionalFilters.findViewById(R.id.ivFemale);

        tvExperience = (TextView) llAdditionalFilters.findViewById(R.id.tvExperience);
        tvDistance = (TextView) llAdditionalFilters.findViewById(R.id.tvDistance);
        rbAscending = (RadioButton) llAdditionalFilters.findViewById(R.id.rbAscending);
        rbDescending = (RadioButton) llAdditionalFilters.findViewById(R.id.rbDescending);

        rBarFee  = (RangeBar) llAdditionalFilters.findViewById(R.id.rBarFee);
        rBarTime  = (RangeBar) llAdditionalFilters.findViewById(R.id.rBarTime);

        cbBook  = (CheckBox) llAdditionalFilters.findViewById(R.id.cbBook);
        cbCall  = (CheckBox) llAdditionalFilters.findViewById(R.id.cbCall);
        cbAvailability  = (CheckBox) llAdditionalFilters.findViewById(R.id.cbAvailability);

        setupFeeRangeValues();
        setupConsultationTimeRangeValues();
        rbAscending.setChecked(true);
        bindControls();
    }

    public void bindControls(){

        tvStartTime.setTag(listConsultationTimeService.get(0));
        tvEndTime.setTag(listConsultationTimeService.get(consultationTickCount - 1));
        tvAny.performClick();
        setSpecificTypeFace(llAdditionalFilters, ApplicationConstants.WALSHEIM_MEDIUM);
        tvApply.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        tvAny.setOnClickListener(this);
        tvMon.setOnClickListener(this);
        tvTue.setOnClickListener(this);
        tvWed.setOnClickListener(this);
        tvThu.setOnClickListener(this);
        tvFri.setOnClickListener(this);
        tvSat.setOnClickListener(this);
        tvSun.setOnClickListener(this);
        tvApply.setOnClickListener(this);
        llMale.setOnClickListener(this);
        llFemale.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        tvDistance.setOnClickListener(this);
        tvExperience.setOnClickListener(this);

        rBarFee.setTickCount(tickCount);
        rBarTime.setTickCount(consultationTickCount);
        rBarFee.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int minValue, int maxValue) {
                try{
                    tvMinFee.setText(listFee.get(minValue)+"");
                    tvMaxFee.setText(listFee.get(maxValue)+"");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        rBarTime.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int minValue, int maxValue) {
                try {
                    tvStartTime.setText(listConsultationTime.get(minValue));
                    tvEndTime.setText(listConsultationTime.get(maxValue));

                    tvStartTime.setTag(listConsultationTimeService.get(minValue));
                    tvEndTime.setTag(listConsultationTimeService.get(maxValue));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        cbBook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    book = "Y";
                else
                    book = "N";
            }
        });

        cbCall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    call = "Y";
                else
                    call = "N";
            }
        });

        cbAvailability.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    availability = "Y";
                else
                    availability = "N";
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rbAscending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbDescending.setChecked(false);
                    sortType = "ASC";
                }
            }
        });

        rbDescending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbAscending.setChecked(false);
                    sortType = "DESC";
                }
            }
        });

        updateFilterUI();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.llMale:
                isResetActive = false;
                genderConditions(ivMale.getTag() != null ? (boolean)ivMale.getTag() : false, true);
                genderBgConditions(ivMale, (((ivMale.getTag() != null) ? (boolean)ivMale.getTag() : false)? R.drawable.male_unsel : R.drawable.male_sel), (ivMale.getTag() != null) ? (boolean)ivMale.getTag() : false);
                break;
            case R.id.llFemale:
                isResetActive = false;
                genderConditions(ivFemale.getTag() != null ? (boolean)ivFemale.getTag() : false, false);
                genderBgConditions(ivFemale, (((ivFemale.getTag() != null) ? (boolean)ivFemale.getTag() : false)? R.drawable.female_unselect : R.drawable.female_sel), (ivFemale.getTag() != null) ? (boolean)ivFemale.getTag() : false);
                break;
            case R.id.tvAny:
                isResetActive = false;
                selectedDay = "";
                generateFilterDayString("", tvAny, tvAny.getTag() != null ? (boolean)tvAny.getTag() : false);
                break;
            case R.id.tvMon:
                isResetActive = false;
                generateFilterDayString("MON", tvMon, tvMon.getTag() != null ? (boolean)tvMon.getTag() : false);
                break;
            case R.id.tvTue:
                isResetActive = false;
                generateFilterDayString("TUE", tvTue, tvTue.getTag() != null ? (boolean)tvTue.getTag() : false);
                break;
            case R.id.tvWed:
                isResetActive = false;
                generateFilterDayString("WED", tvWed, tvWed.getTag() != null ? (boolean)tvWed.getTag() : false);
                break;
            case R.id.tvThu:
                isResetActive = false;
                generateFilterDayString("THU", tvThu, tvThu.getTag() != null ? (boolean)tvThu.getTag() : false);
                break;
            case R.id.tvFri:
                isResetActive = false;
                generateFilterDayString("FRI", tvFri, tvFri.getTag() != null ? (boolean)tvFri.getTag() : false);
                break;
            case R.id.tvSat:
                isResetActive = false;
                generateFilterDayString("SAT", tvSat, tvSat.getTag() != null ? (boolean)tvSat.getTag() : false);
                break;
            case R.id.tvSun:
                isResetActive = false;
                generateFilterDayString("SUN", tvSun, tvSun.getTag() != null ? (boolean)tvSun.getTag() : false);
                break;
            case R.id.tvApply:
                submitFilterValues();
                break;
            case R.id.tvSave:
                resetFilterValues();
                break;
            case R.id.tvExperience:
                sortCategory = "EXP";
                clearSortByBgs();
                tvExperience.setBackgroundResource(R.drawable.green);
                tvExperience.setTextColor(getResources().getColor(R.color.white));
                break;
            case R.id.tvDistance:
                sortCategory = "DIST";
                clearSortByBgs();
                tvDistance.setBackgroundResource(R.drawable.green);
                tvDistance.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    public void clearGenderBg()
    {
        ivMale.setImageResource(R.drawable.male_unsel);
        ivFemale.setImageResource(R.drawable.female_unselect);
    }
    public void clearSortByBgs()
    {
        tvExperience.setBackgroundResource(R.drawable.grey_exp);
        tvDistance.setBackgroundResource(R.drawable.grey_distance);
        tvExperience.setTextColor(getResources().getColor(R.color.light_black));
        tvDistance.setTextColor(getResources().getColor(R.color.light_black));
    }

    public void clearWeekBg()
    {
        tvAny.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvMon.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvTue.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvWed.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvThu.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvFri.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvSat.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvSun.setBackgroundColor(getResources().getColor(R.color.transparent));

        tvAny.setTextColor(getResources().getColor(R.color.light_black));tvAny.setTag(false);
        tvMon.setTextColor(getResources().getColor(R.color.light_black));tvMon.setTag(false);
        tvTue.setTextColor(getResources().getColor(R.color.light_black));tvTue.setTag(false);
        tvWed.setTextColor(getResources().getColor(R.color.light_black));tvWed.setTag(false);
        tvThu.setTextColor(getResources().getColor(R.color.light_black));tvThu.setTag(false);
        tvFri.setTextColor(getResources().getColor(R.color.light_black));tvFri.setTag(false);
        tvSat.setTextColor(getResources().getColor(R.color.light_black));tvSat.setTag(false);
        tvSun.setTextColor(getResources().getColor(R.color.light_black));tvSun.setTag(false);


    }

    public void selectWeekBgs()
    {
        tvAny.setBackgroundColor(getResources().getColor(R.color.bg_color));
        tvMon.setBackgroundColor(getResources().getColor(R.color.bg_color));
        tvTue.setBackgroundColor(getResources().getColor(R.color.bg_color));
        tvWed.setBackgroundColor(getResources().getColor(R.color.bg_color));
        tvThu.setBackgroundColor(getResources().getColor(R.color.bg_color));
        tvFri.setBackgroundColor(getResources().getColor(R.color.bg_color));
        tvSat.setBackgroundColor(getResources().getColor(R.color.bg_color));
        tvSun.setBackgroundColor(getResources().getColor(R.color.bg_color));

        tvAny.setTextColor(getResources().getColor(R.color.white));tvAny.setTag(true);
        tvMon.setTextColor(getResources().getColor(R.color.white));tvMon.setTag(true);
        tvTue.setTextColor(getResources().getColor(R.color.white));tvTue.setTag(true);
        tvWed.setTextColor(getResources().getColor(R.color.white));tvWed.setTag(true);
        tvThu.setTextColor(getResources().getColor(R.color.white));tvThu.setTag(true);
        tvFri.setTextColor(getResources().getColor(R.color.white));tvFri.setTag(true);
        tvSat.setTextColor(getResources().getColor(R.color.white));tvSat.setTag(true);
        tvSun.setTextColor(getResources().getColor(R.color.white));tvSun.setTag(true);


    }

    public void setupFeeRangeValues() {
        listFee = new ArrayList<>();
        for (int i  = 0; i <= 1000;){
            listFee.add(i);
            i += 50;
        }
        tickCount = (1000/50)+1;
    }

    public void setupConsultationTimeRangeValues() {
        listConsultationTime = new ArrayList<>();
        listConsultationTimeService = new ArrayList<>();

        listConsultationTime.add("12:00 AM");
        listConsultationTime.add("12:30 AM");
        listConsultationTime.add("01:00 AM");
        listConsultationTime.add("01:30 AM");
        listConsultationTime.add("02:00 AM");
        listConsultationTime.add("02:30 AM");
        listConsultationTime.add("03:00 AM");
        listConsultationTime.add("03:30 AM");
        listConsultationTime.add("04:00 AM");
        listConsultationTime.add("04:30 AM");
        listConsultationTime.add("05:00 AM");
        listConsultationTime.add("05:30 AM");
        listConsultationTime.add("06:00 AM");
        listConsultationTime.add("06:30 AM");
        listConsultationTime.add("07:00 AM");
        listConsultationTime.add("07:30 AM");
        listConsultationTime.add("08:00 AM");
        listConsultationTime.add("08:30 AM");
        listConsultationTime.add("09:00 AM");
        listConsultationTime.add("09:30 AM");
        listConsultationTime.add("10:00 AM");
        listConsultationTime.add("10:30 AM");
        listConsultationTime.add("11:00 AM");
        listConsultationTime.add("11:30 AM");
        listConsultationTime.add("12:00 PM");
        listConsultationTime.add("12:30 PM");
        listConsultationTime.add("01:00 PM");
        listConsultationTime.add("01:30 PM");
        listConsultationTime.add("02:00 PM");
        listConsultationTime.add("02:30 PM");
        listConsultationTime.add("03:00 PM");
        listConsultationTime.add("03:30 PM");
        listConsultationTime.add("04:00 PM");
        listConsultationTime.add("04:30 PM");
        listConsultationTime.add("05:00 PM");
        listConsultationTime.add("05:30 PM");
        listConsultationTime.add("06:00 PM");
        listConsultationTime.add("06:30 PM");
        listConsultationTime.add("07:00 PM");
        listConsultationTime.add("07:30 PM");
        listConsultationTime.add("08:00 PM");
        listConsultationTime.add("08:30 PM");
        listConsultationTime.add("09:00 PM");
        listConsultationTime.add("09:30 PM");
        listConsultationTime.add("10:00 PM");
        listConsultationTime.add("10:30 PM");
        listConsultationTime.add("11:00 PM");
        listConsultationTime.add("11:30 PM");

        listConsultationTimeService.add("00:00");
        listConsultationTimeService.add("00:30");
        listConsultationTimeService.add("01:00");
        listConsultationTimeService.add("01:30");
        listConsultationTimeService.add("02:00");
        listConsultationTimeService.add("02:30");
        listConsultationTimeService.add("03:00");
        listConsultationTimeService.add("03:30");
        listConsultationTimeService.add("04:00");
        listConsultationTimeService.add("04:30");
        listConsultationTimeService.add("05:00");
        listConsultationTimeService.add("05:30");
        listConsultationTimeService.add("06:00");
        listConsultationTimeService.add("06:30");
        listConsultationTimeService.add("07:00");
        listConsultationTimeService.add("07:30");
        listConsultationTimeService.add("08:00");
        listConsultationTimeService.add("08:30");
        listConsultationTimeService.add("09:00");
        listConsultationTimeService.add("09:30");
        listConsultationTimeService.add("10:00");
        listConsultationTimeService.add("10:30");
        listConsultationTimeService.add("11:00");
        listConsultationTimeService.add("11:30");
        listConsultationTimeService.add("12:00");
        listConsultationTimeService.add("12:30");
        listConsultationTimeService.add("13:00");
        listConsultationTimeService.add("13:30");
        listConsultationTimeService.add("14:00");
        listConsultationTimeService.add("14:30");
        listConsultationTimeService.add("15:00");
        listConsultationTimeService.add("15:30");
        listConsultationTimeService.add("16:00");
        listConsultationTimeService.add("16:30");
        listConsultationTimeService.add("17:00");
        listConsultationTimeService.add("17:30");
        listConsultationTimeService.add("18:00");
        listConsultationTimeService.add("18:30");
        listConsultationTimeService.add("19:00");
        listConsultationTimeService.add("19:30");
        listConsultationTimeService.add("20:00");
        listConsultationTimeService.add("20:30");
        listConsultationTimeService.add("21:00");
        listConsultationTimeService.add("21:30");
        listConsultationTimeService.add("22:00");
        listConsultationTimeService.add("22:30");
        listConsultationTimeService.add("23:00");
        listConsultationTimeService.add("23:30");

        consultationTickCount = 48;
    }

    public void resetFilterValues(){

        gender = null;
        isGenderBothSelected = false;
        availability = null;
        clearGenderBg();
        ivMale.setTag(false);
        ivFemale.setTag(false);
        clearWeekBg();
        tvAny.performClick();
        cbCall.setChecked(true);
        cbBook.setChecked(true);
        cbAvailability.setChecked(false);
        rBarFee.setThumbIndices(0, tickCount-1);
        rBarTime.setThumbIndices(0, consultationTickCount-1);
        tvStartTime.setTag(listConsultationTimeService.get(0));
        tvEndTime.setTag(listConsultationTimeService.get(consultationTickCount - 1));
        tvDistance.performClick();
        rbAscending.performClick();

        isResetActive = true;
    }

    public void submitFilterValues() {
        AdditionalFiltersDO filtersDO = new AdditionalFiltersDO();
        filtersDO.setGender(gender);
        filtersDO.setBook(/*book*/"Y");
        filtersDO.setCall(/*call*/"N");
        filtersDO.setAvailability(availability);
        filtersDO.setBothSelected(isGenderBothSelected);
        filtersDO.setSortType(sortType);
        filtersDO.setSortCategory(sortCategory);
        filtersDO.setListSelectedDay(listSelectedDay);
        if (isResetActive){
            filtersDO.setMaxFee("1000");
            filtersDO.setMinFee("0");
            filtersDO.setStartTime("00:00");
            filtersDO.setEndTime("23:30");
            filtersDO.setSelectedDay(null);
        }else{
            filtersDO.setMaxFee(tvMaxFee.getText().toString().trim());
            filtersDO.setMinFee(tvMinFee.getText().toString().trim());
            filtersDO.setStartTime(tvStartTime.getTag().toString());
            filtersDO.setEndTime(tvEndTime.getTag().toString().trim());
            filtersDO.setSelectedDay(selectedDay);
        }



        Intent returnIntent = new Intent();
        returnIntent.putExtra("FiltersDO",filtersDO);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
    public void updateFilterUI() {

        try{
            if (filtersDO != null && filtersDO.getBook() != null && filtersDO.getBook().equalsIgnoreCase("Y"))
                cbBook.setChecked(true);
            else
                cbBook.setChecked(false);

            if (filtersDO != null && filtersDO.getCall() != null && filtersDO.getCall().equalsIgnoreCase("Y"))
                cbCall.setChecked(true);
            else
                cbCall.setChecked(false);

            if (filtersDO != null && filtersDO.getAvailability() != null && filtersDO.getAvailability().equalsIgnoreCase("Y"))
                cbAvailability.setChecked(true);
            else
                cbAvailability.setChecked(false);

            if (filtersDO != null && filtersDO.getGender() != null && filtersDO.getGender().equalsIgnoreCase("F")){
                llFemale.performClick();
            }else if(filtersDO != null && filtersDO.getGender() != null && filtersDO.getGender().equalsIgnoreCase("M")){
                llMale.performClick();
            }else if (filtersDO.isBothSelected()){
                llMale.performClick();
                llFemale.performClick();
            }

            for (int i = 0; i < filtersDO.getListSelectedDay().size(); i++){
                if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("ANY")){
                    tvAny.performClick();
                }else if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("MON")){
                    tvMon.performClick();
                }else if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("TUE")){
                    tvTue.performClick();
                }else if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("WED")){
                    tvWed.performClick();
                }else if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("THU")){
                    tvThu.performClick();
                }else if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("FRI")){
                    tvFri.performClick();
                }else if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("SAT")){
                    tvSat.performClick();
                }else if (filtersDO != null && filtersDO.getListSelectedDay().get(i) != null && filtersDO.getListSelectedDay().get(i).equalsIgnoreCase("SUN")){
                    tvSun.performClick();
                }
            }

            int feeStartIndex = 0;
            int feeEndIndex   = tickCount - 1;

            if (filtersDO.getMinFee() != null && !filtersDO.getMinFee().equalsIgnoreCase("")){
                tvMinFee.setText(filtersDO.getMinFee());
                feeStartIndex = listFee.indexOf(Integer.parseInt(filtersDO.getMinFee()));
            }

            if (filtersDO.getMaxFee() != null && !filtersDO.getMaxFee().equalsIgnoreCase("")){
                tvMaxFee.setText(filtersDO.getMaxFee());
                feeEndIndex = listFee.indexOf(Integer.parseInt(filtersDO.getMaxFee()));
            }
            rBarFee.setThumbIndices(feeStartIndex, feeEndIndex);

            int startIndex = 0;
            int endIndex   = consultationTickCount-1;

            if (listConsultationTimeService.contains(filtersDO.getStartTime())){
                tvStartTime.setText(listConsultationTime.get(listConsultationTimeService.indexOf(filtersDO.getStartTime())));
                startIndex = listConsultationTimeService.indexOf(filtersDO.getStartTime());
                tvStartTime.setTag(filtersDO.getStartTime());
            }
            if (listConsultationTimeService.contains(filtersDO.getEndTime())){
                tvEndTime.setText(filtersDO.getEndTime());
                tvEndTime.setTag(listConsultationTimeService.indexOf(filtersDO.getEndTime()));
                endIndex   = listConsultationTimeService.indexOf(filtersDO.getEndTime());
            }
            rBarTime.setThumbIndices(startIndex, endIndex);

            if (filtersDO.getSortCategory().equalsIgnoreCase("EXP")){
                tvExperience.performClick();
            }else{
                tvDistance.performClick();
            }

            if (filtersDO.getSortType().equalsIgnoreCase("DESC")){
                rbDescending.performClick();
            }else{
                rbAscending.performClick();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void genderConditions(boolean status, boolean isFromMale){
        if (gender == null && !status){
            gender = isFromMale ?  "M" :  "F";
        }
        else if(gender == null && status){
            gender = isFromMale ?  "F" :  "M";
        }
        else{
            gender = null;
            isGenderBothSelected = !status;
        }

    }

    public void genderBgConditions(ImageView v, int resID, boolean status){
        v.setImageResource(resID);
        if (status)
            v.setTag(false);
        else
            v.setTag(true);
    }
    public void generateFilterDayString( String day, TextView tv, boolean status) {
        if (day.equalsIgnoreCase("")) {
            listSelectedDay.clear();
        } else if (listSelectedDay.contains(day)) {
            listSelectedDay.remove(day);
        } else {
            listSelectedDay.add(day);
        }
        changeBgAndStatus(tv, status);
        if (day.equalsIgnoreCase("")) {
            if ((tvAny.getTag() != null ? (boolean)tvAny.getTag() : false)){
                clearWeekBg();
            }else{
                selectWeekBgs();
            }
        }else{
            tvAny.setTag(false);
            tvAny.setBackgroundColor(getResources().getColor(R.color.transparent));
            tvAny.setTextColor(getResources().getColor(R.color.light_black));
        }

        if (tvMon.getTag() != null ? (boolean)tvMon.getTag() : false || tvTue.getTag() != null ? (boolean)tvTue.getTag() : false  ||
                tvWed.getTag() != null ? (boolean)tvWed.getTag() : false || tvThu.getTag() != null ? (boolean)tvThu.getTag() : false ||
                tvFri.getTag() != null ? (boolean)tvFri.getTag() : false || tvSat.getTag() != null ? (boolean)tvSat.getTag() : false ||
                tvSun.getTag() != null ? (boolean)tvSun.getTag() : false) {

            changeBgAndStatus(tvAny, true);

        } else if (!(tvMon.getTag() != null ? (boolean)tvMon.getTag() : false) && !(tvTue.getTag() != null ? (boolean)tvTue.getTag() : false)  &&
                !(tvWed.getTag() != null ? (boolean)tvWed.getTag() : false) && !(tvThu.getTag() != null ? (boolean)tvThu.getTag() : false) &&
                !(tvFri.getTag() != null ? (boolean)tvFri.getTag() : false) && !(tvSat.getTag() != null ? (boolean)tvSat.getTag() : false) &&
                !(tvSun.getTag() != null ? (boolean)tvSun.getTag() : false)) {
            changeBgAndStatus(tvAny, false);
        }
    }
    public void changeBgAndStatus(TextView tv, boolean status) {
        if (status){
            tv.setTag(false);
            tv.setBackgroundColor(getResources().getColor(R.color.transparent));
            tv.setTextColor(getResources().getColor(R.color.light_black));
        }else {
            tv.setTag(true);
            tv.setBackgroundColor(getResources().getColor(R.color.bg_color));
            tv.setTextColor(getResources().getColor(R.color.white));
        }
    }
}
