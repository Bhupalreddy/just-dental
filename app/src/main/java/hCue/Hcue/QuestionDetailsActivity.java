package hCue.Hcue;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import hCue.Hcue.DAO.AnswerDO;
import hCue.Hcue.DAO.LikeDisLikeAnswerRequestDO;
import hCue.Hcue.DAO.ListQuestionsRequestDO;
import hCue.Hcue.DAO.ListQuestionsResponseDO;
import hCue.Hcue.DAO.PatientLikeDO;
import hCue.Hcue.DAO.QuestionsResponsDO;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateHelper;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.MySpannable;
import retrofit.client.Response;

import static hCue.Hcue.R.id.llAvailability;
import static hCue.Hcue.R.id.tvNotAvailable;
import static hCue.Hcue.R.id.tvUpCount;


/**
 * Created by shyamprasadg on 03/06/16.
 */
public class QuestionDetailsActivity extends BaseActivity implements View.OnClickListener
{

    private ScrollView llAskPublicQuestion;
    private TextView tvQuestion, tvTime, tvNoOfAnswers, etDummy;
    private RecyclerView rvAnswers;
    private AnswersAdapter answersAdapter;
    private boolean isFromPrivateQuestions;
    private ListQuestionsResponseDO questionDO;
    private int screenWidth;
    private ListView lvCommon1;
    private LinearLayout llAvailability1;
    private TextView tvNotAvailable1;

    @Override
    public void initialize() {

        if(getIntent().hasExtra("isFromPrivateQuestions")){
            isFromPrivateQuestions = getIntent().getBooleanExtra("isFromPrivateQuestions", false);
        }

        if(getIntent().hasExtra("questionDO")){
            questionDO = (ListQuestionsResponseDO)getIntent().getSerializableExtra("questionDO");
        }
        setUpDisplayScreenResolution(QuestionDetailsActivity.this);
        llAskPublicQuestion = (ScrollView) getLayoutInflater().inflate(R.layout.question_details, null, false);
        llBody.addView(llAskPublicQuestion, baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);
        if (isFromPrivateQuestions)
            tvTopTitle.setText("Your Question");
        else
            tvTopTitle.setText("Public Question");

        tvQuestion                              = (TextView)        findViewById(R.id.tvQuestion);
        tvTime                                  = (TextView)        findViewById(R.id.tvTime);
        tvNoOfAnswers                           = (TextView)        findViewById(R.id.tvNoOfAnswers);
        rvAnswers                               = (RecyclerView)    findViewById(R.id.rvAnswers);
        llAvailability1                         = (LinearLayout)    findViewById(R.id.llAvailability);
        tvNotAvailable1                         = (TextView)        findViewById(R.id.tvNotAvailable);
        etDummy                                 = (TextView)        findViewById(R.id.etDummy);
        answersAdapter = new AnswersAdapter(QuestionDetailsActivity.this, new ArrayList<AnswerDO>());
        LinearLayoutManager layoutManager1= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvAnswers.setLayoutManager(layoutManager1);
        ViewGroup.LayoutParams params = rvAnswers.getLayoutParams();
        params.width  = screenWidth;
        rvAnswers.setLayoutParams(params);
        rvAnswers.setAdapter(answersAdapter);

        tvQuestion.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvTime.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvNoOfAnswers.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvNotAvailable1.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        llLocation.setVisibility(View.GONE);
        tvTopTitle.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);
        updateDataOnUI();
        etDummy.requestFocus();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
        }
    }

    public class AnswersAdapter extends RecyclerView.Adapter<AnswersAdapter.MyQuestionsViewHolder> {

        private Context context;
        ArrayList<AnswerDO> listAnswers = new ArrayList<>();

        public AnswersAdapter(Context context, ArrayList<AnswerDO> listAnswers) {
            this.context = context;
            this.listAnswers = listAnswers;
        }

        @Override
        public MyQuestionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.answer_cell, parent, false);
            return new AnswersAdapter.MyQuestionsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final AnswersAdapter.MyQuestionsViewHolder holder, int position) {

            AnswerDO answerDO = listAnswers.get(position);

            holder.tvAnswer.setTag(answerDO.getAnswer());
            holder.tvUpCount.setTag(answerDO);
            holder.tvDownCount.setTag(answerDO);

            if (answerDO.getDoctorDetails().getDoctorName() != null &&
                    !answerDO.getDoctorDetails().getDoctorName().equalsIgnoreCase(""))
                holder.tvDoctorName.setText("Dr. "+answerDO.getDoctorDetails().getDoctorName());
            else{
                holder.tvDoctorName.setText("");
            }
            holder.tvSpeciality.setText(answerDO.getDoctorDetails().getDoctorSpeciality());
            calculateDaysBeforeAnswerGiven(holder.tvTime, answerDO.getAnswerDate(), true);
            holder.tvAnswer.setText(answerDO.getAnswer());
            holder.tvNoOfDoctorsAgreed.setText(answerDO.getCountDO().getAgreeCount()+ " Doctor(s) Agreed");
            holder.tvUpCount.setText(answerDO.getCountDO().getLikeCount()+"");
            holder.tvDownCount.setText(answerDO.getCountDO().getUnLikeCount()+"");
            if (answerDO.getDoctorDetails().getProfileImageURL() != null && !answerDO.getDoctorDetails().getProfileImageURL().equalsIgnoreCase("")){
                Glide.with(context).load(answerDO.getDoctorDetails().getProfileImageURL()).skipMemoryCache(true).into(holder.ivDoctorPic);
            }else{
                if (answerDO.getDoctorDetails() != null && answerDO.getDoctorDetails().getDoctorGender() != null
                        && answerDO.getDoctorDetails().getDoctorGender().equalsIgnoreCase("F"))
                    Glide.with(context).load(R.drawable.question_female).skipMemoryCache(true).into(holder.ivDoctorPic);
                else
                    Glide.with(context).load(R.drawable.question_male).skipMemoryCache(true).into(holder.ivDoctorPic);
            }
            if(answerDO.getAnswerLiked() != null && answerDO.getAnswerLiked().equalsIgnoreCase("LIKE")){
                holder.tvUpCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_up_green, 0, 0, 0);
                holder.tvDownCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_down_grey, 0, 0, 0);

                holder.tvUpCount.setTextColor(getResources().getColor(R.color.bg_color));
                holder.tvDownCount.setTextColor(getResources().getColor(R.color.gray));
                answerDO.setLikeStatus(1);
            }else if(answerDO.getAnswerLiked() != null && answerDO.getAnswerLiked().equalsIgnoreCase("DISLIKE")){
                holder.tvUpCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_up_grey, 0, 0, 0);
                holder.tvDownCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_down_green, 0, 0, 0);

                holder.tvUpCount.setTextColor(getResources().getColor(R.color.gray));
                holder.tvDownCount.setTextColor(getResources().getColor(R.color.bg_color));
                answerDO.setLikeStatus(2);
            }else{
                holder.tvUpCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_up_grey, 0, 0, 0);
                holder.tvDownCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_down_grey, 0, 0, 0);

                holder.tvUpCount.setTextColor(getResources().getColor(R.color.gray));
                holder.tvDownCount.setTextColor(getResources().getColor(R.color.gray));
            }

            holder.tvAnswer.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount = holder.tvAnswer.getLineCount();
                    if ((lineCount > 3))
                        makeTextViewResizable(holder.tvAnswer, 3, "View More", true);
                }
            });

//            if ((holder.tvAnswer.getText().getLineCount() > 3))
//                makeTextViewResizable(holder.tvAnswer, 3, "View More", true);

            holder.tvDoctorName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            holder.tvSpeciality.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            holder.tvAnswer.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            holder.tvNoOfDoctorsAgreed.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            holder.tvUpCount.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            holder.tvDownCount.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        }

        @Override
        public int getItemCount() {
            if (listAnswers != null && listAnswers.size() > 0)
                return listAnswers.size();
            else
                return 0;
        }

        public class MyQuestionsViewHolder extends RecyclerView.ViewHolder {
            private TextView tvDoctorName, tvSpeciality, tvTime, tvAnswer, tvNoOfDoctorsAgreed, tvUpCount, tvDownCount;
            private CircleImageView ivDoctorPic;

            public MyQuestionsViewHolder(View itemView) {
                super(itemView);
                tvDoctorName            = (TextView) itemView.findViewById(R.id.tvDoctorName);
                tvSpeciality            = (TextView) itemView.findViewById(R.id.tvSpeciality);
                tvTime                  = (TextView) itemView.findViewById(R.id.tvTime);
                tvAnswer                = (TextView) itemView.findViewById(R.id.tvAnswer);
                tvNoOfDoctorsAgreed     = (TextView) itemView.findViewById(R.id.tvNoOfDoctorsAgreed);
                tvUpCount               = (TextView) itemView.findViewById(R.id.tvUpCount);
                tvDownCount             = (TextView) itemView.findViewById(R.id.tvDownCount);
                ivDoctorPic             = (CircleImageView) itemView.findViewById(R.id.ivDoctorPic);

                tvUpCount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AnswerDO selectedAnswerDO = (AnswerDO)v.getTag();

                        if (selectedAnswerDO.getAnswerLiked() == null && selectedAnswerDO.getLikeStatus() == 0){
                            selectedAnswerDO.setLikeStatus(1);
                            selectedAnswerDO.getCountDO().setLikeCount(selectedAnswerDO.getCountDO().getLikeCount() + 1);
                            updateLikeDisLikeService(selectedAnswerDO, true);
                        }else if(selectedAnswerDO.getLikeStatus() == 2){
                            selectedAnswerDO.setLikeStatus(1);
                            selectedAnswerDO.getCountDO().setLikeCount(selectedAnswerDO.getCountDO().getLikeCount() + 1);
                            selectedAnswerDO.getCountDO().setUnLikeCount(selectedAnswerDO.getCountDO().getUnLikeCount() - 1);
                            updateLikeDisLikeService(selectedAnswerDO, true);
                        }
                        tvUpCount.setText((selectedAnswerDO.getCountDO().getLikeCount())+"");
                        tvDownCount.setText((selectedAnswerDO.getCountDO().getUnLikeCount())+"");
                        tvUpCount.setCompoundDrawablesWithIntrinsicBounds( R.drawable.thumbs_up_green, 0, 0, 0);
                        tvDownCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_down_grey, 0, 0, 0);
                        tvUpCount.setTextColor(getResources().getColor(R.color.bg_color));
                        tvDownCount.setTextColor(getResources().getColor(R.color.gray));

                    }
                });

                tvDownCount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AnswerDO selectedAnswerDO = (AnswerDO)v.getTag();
                        if (selectedAnswerDO.getAnswerLiked() == null  && selectedAnswerDO.getLikeStatus() == 0){
                            selectedAnswerDO.setLikeStatus(2);
                            selectedAnswerDO.getCountDO().setUnLikeCount(selectedAnswerDO.getCountDO().getUnLikeCount() + 1);
                            updateLikeDisLikeService(selectedAnswerDO, false);
                        }else if(selectedAnswerDO.getLikeStatus() == 1){
                            selectedAnswerDO.setLikeStatus(2);
                            selectedAnswerDO.getCountDO().setUnLikeCount(selectedAnswerDO.getCountDO().getUnLikeCount() + 1);
                            selectedAnswerDO.getCountDO().setLikeCount(selectedAnswerDO.getCountDO().getLikeCount() - 1);
                            updateLikeDisLikeService(selectedAnswerDO, false);
                        }
                        tvUpCount.setText((selectedAnswerDO.getCountDO().getLikeCount())+"");
                        tvDownCount.setText((selectedAnswerDO.getCountDO().getUnLikeCount())+"");
                        tvUpCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_up_grey, 0, 0, 0);
                        tvDownCount.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thumbs_down_green, 0, 0, 0);
                        tvUpCount.setTextColor(getResources().getColor(R.color.gray));
                        tvDownCount.setTextColor(getResources().getColor(R.color.bg_color));
                    }
                });
            }
        }

        public void refreshAdapters(ArrayList<AnswerDO> tempListAnswers) {
            listAnswers = tempListAnswers;
            notifyDataSetChanged();
        }
    }

    public void setUpDisplayScreenResolution(Context context) {
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        screenWidth = display.widthPixels;
    }

    private void updateDataOnUI(){

        tvQuestion.setText(questionDO.getQuestion());
        tvNoOfAnswers.setText(questionDO.getAnswerCount()+ " Answer(s)");
        if (isFromPrivateQuestions)
            calculateDaysBeforeAnswerGiven(tvTime, questionDO.getQuestionDate(), false);
        else{
            Date date = new Date(questionDO.getQuentionaireDetails().getDOB());
            String age = DateHelper.getAgeAndMonth(date)[0]+"";
            String gender = questionDO.getQuentionaireDetails().getGender().equalsIgnoreCase("M") ? "Male" : "Female";
            if (questionDO.getQuentionaireDetails().getCity() != null &&
                    !questionDO.getQuentionaireDetails().getCity().equalsIgnoreCase(""))
                tvTime.setText(gender + " - "+ age + "yrs - " + questionDO.getQuentionaireDetails().getCity());
            else
                tvTime.setText(gender + " - "+ age + "yrs");
        }
        if (questionDO.getAnswerCount() > 0){
            answersAdapter.refreshAdapters(questionDO.getAnswers());
            llAvailability1.setVisibility(View.GONE);
            rvAnswers.setVisibility(View.VISIBLE);
        }else {
            llAvailability1.setVisibility(View.VISIBLE);
            rvAnswers.setVisibility(View.GONE);
        }

        tvQuestion.post(new Runnable() {
            @Override
            public void run() {
                int lineCount = tvQuestion.getLineCount();
                if ((lineCount > 3))
                    makeTextViewResizable(tvQuestion, 3, "View More", true);
            }
        });
    }

    private void calculateDaysBeforeAnswerGiven(TextView tv, long timeInMillis, boolean isFromAdapter) {

        Calendar cal = Calendar.getInstance();
        Calendar calQuestionDate = Calendar.getInstance();
        calQuestionDate.setTimeInMillis(timeInMillis);
        int noOfDays = (int) TimeUnit.MILLISECONDS.toDays(Math.abs(cal.getTimeInMillis() - calQuestionDate.getTimeInMillis()));

        if (timeInMillis != 0){
            if (noOfDays > 6){
                String date = DateUtils.getQuestionDate(timeInMillis);
                if (!isFromAdapter)
                    tv.setText(date +" - "+ questionDO.getQuestionSpeciality().getSpecialityDesc() );
                else {
                    if (questionDO.getQuestionSpeciality().getSpecialityDesc() != null && !questionDO.getQuestionSpeciality().getSpecialityDesc().equalsIgnoreCase(""))
                        tv.setText(date);
                }

            }else{
                tv.setText(noOfDays +" day(s) ago ");
            }
        }
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = ((String)tv.getTag()).subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = ((String)tv.getTag()).subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = ((String)tv.getTag()).subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(true) {

                @Override
                public void onClick(View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        if (tv.getLineCount() > 3)
                            makeTextViewResizable(tv, -1, "View Less", false);
                        else
                            makeTextViewResizable(tv, -1, "", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        if (tv.getLineCount() > 3)
                            makeTextViewResizable(tv, 3, "View More", true);
                        else
                            makeTextViewResizable(tv, -1, "", false);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);
        }
        return ssb;

    }
    private void updateLikeDisLikeService(AnswerDO selectedAnswerDO, final boolean isLike)
    {
        LoginResponse loginResponse  = LoginResponseSingleton.getSingleton();
        long patientID = loginResponse.getArrPatients().get(0).getPatientID();

        HashMap<String, String> tempLikeInfo = new HashMap<>();
        tempLikeInfo.put(patientID+"", isLike ? "LIKE" : "DISLIKE");

        PatientLikeDO likeDO = new PatientLikeDO();
        likeDO.setLikeCount(tempLikeInfo);

        LikeDisLikeAnswerRequestDO requestDO = new LikeDisLikeAnswerRequestDO();
        requestDO.setPatientID(patientID);
        requestDO.setUsrID(selectedAnswerDO.getDoctorDetails().getDoctorID());
        requestDO.setUsrType("DOCTOR");
        requestDO.setMessageID(questionDO.getMessageID());
        requestDO.setParentMessageID(questionDO.getParentMessageID());
        requestDO.setAnswerID(selectedAnswerDO.getAnswerID());
        requestDO.setLikeCount(selectedAnswerDO.getCountDO().getLikeCount());
        requestDO.setUnLikeCount(selectedAnswerDO.getCountDO().getUnLikeCount());
        requestDO.setLikeCount(tempLikeInfo);

        showLoader("");
        RestClient.getAPI(Constants.DEVICE_REGISTRATION).updateLikeDislikeAnswer(requestDO, new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
//                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(String resp, Response response)
            {
                hideLoader();
            }
        });
    }
}
