package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import hCue.Hcue.DAO.ClinicDocument;
import hCue.Hcue.DAO.ClinicImagesDO;
import hCue.Hcue.DAO.Doctor;
import hCue.Hcue.DAO.DoctorAddress;
import hCue.Hcue.DAO.DoctorConsultation;
import hCue.Hcue.DAO.DoctorEducation;
import hCue.Hcue.DAO.DoctorPublishing;
import hCue.Hcue.it.sephiroth.android.library.imagezoom.ImageViewTouch;
import hCue.Hcue.model.DoctorDetailsResponse;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.CustomSimpleImageLoadingListener;
import hCue.Hcue.utils.LargeImageLoader;



/**
 * Created by shyamprasadg on 06/06/16.
 */
public class DoctorProfileActivity extends BaseActivity implements OnMapReadyCallback
{
    private TabLayout     tabLayout;
    private ViewPager     viewPager;
    private LinearLayout  llmyprofile, lltimeslots;
    private ImageView     ivDoctorPic;
    private TextView      tvDoctorname,tvDoctorQualification,tvspeciality,tvDoctorDes,tvExperiance;
    private DoctorDetailsResponse doctorDetailsResponse ;
    private String TODAY_CD ;
    private int addressid ;
    private TextView tvClinicHours,tvDoctorName;
    private ListView lvtimeslots ;
    private Animation slide_up, slide_down ;
    public LargeImageLoader largeimageLoader;
    private ImageLoader bubbleimageLoader;
    private DisplayImageOptions bubbleDisplayImageOptions;

    private ArrayList<String> imgList;

    private DisplayImageOptions getBubbleDIO()
    {
        if(bubbleDisplayImageOptions == null)
            bubbleDisplayImageOptions = getDisplayOptions(R.drawable.pics_samp);
        return bubbleDisplayImageOptions;
    }

    @Override
    public void initialize()
    {
        imgList = new ArrayList<>();
//        imgList.add("http://i-cdn.phonearena.com/images/articles/144983-image/Android-Lollipop-wallpapers.jpg");
//        imgList.add("http://www.spyderonlines.com/images/wallpapers/marshmallow-wallpaper/marshmallow-wallpaper-7.png");
//        imgList.add("http://samsung-updates.com/wp-content/uploads/2016/01/mmallows.jpg");
//        imgList.add("http://www.teacher-chef.com/wp-content/uploads/2012/05/5-12-chocolate-n-graham-cracker-covered-marshmallow1.jpg");
//        imgList.add("http://i302.photobucket.com/albums/nn98/AllThingsGD/Blog%20Pics/2012/009-1.jpg");

        llmyprofile =   (LinearLayout)  getLayoutInflater().inflate(R.layout.doctor_profile,null,false);
        llBody.addView(llmyprofile);
        BaseActivity.setSpecificTypeFace(llmyprofile, ApplicationConstants.WALSHEIM_MEDIUM);

        slide_up      = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slide_down    = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        bubbleimageLoader = initImageLoader(this, R.drawable.pics_samp);

        tvTopTitle.setText("DOCTOR PROFILE");
        if(getIntent().hasExtra(Constants.DOCTOR_DETAILS))
            doctorDetailsResponse = (DoctorDetailsResponse) getIntent().getSerializableExtra(Constants.DOCTOR_DETAILS);

        addressid = getIntent().getIntExtra("ADDRESSID",0);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE");
        TODAY_CD = simpleDateFormat.format(Calendar.getInstance().getTime());
        Log.e("TODAY_CD",TODAY_CD);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout               = (TabLayout) llmyprofile.findViewById(R.id.tabs);
        lltimeslots             = (LinearLayout) llmyprofile.findViewById(R.id.lltimeslots);

        ivDoctorPic             = (ImageView) llmyprofile.findViewById(R.id.ivDoctorPic);

        tvDoctorname            = (TextView)  llmyprofile.findViewById(R.id.tvDoctorname);
        tvDoctorQualification   = (TextView)  llmyprofile.findViewById(R.id.tvDoctorQualification);
        tvspeciality            = (TextView)  llmyprofile.findViewById(R.id.tvspeciality);
        tvDoctorDes             = (TextView)  llmyprofile.findViewById(R.id.tvDoctorDes);
        tvExperiance            = (TextView)  llmyprofile.findViewById(R.id.tvExperiance);

        tvClinicHours           = (TextView)  llmyprofile.findViewById(R.id.tvClinicHours);
        tvDoctorName            = (TextView)  llmyprofile.findViewById(R.id.tvDoctorName);

        lvtimeslots             = (ListView)  llmyprofile.findViewById(R.id.lvtimeslots);

        setSpecificTypeFace(llmyprofile,ApplicationConstants.WALSHEIM_MEDIUM);
        tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        flBottomBar.setVisibility(View.GONE);


        if(doctorDetailsResponse != null)
        {

            tvDoctorDes.setVisibility(View.GONE);
            if(doctorDetailsResponse.getArrdoctors().get(0).getProfileImage()!= null) {
                Picasso.with(this).load(Uri.parse(doctorDetailsResponse.getArrdoctors().get(0).getProfileImage())).into(ivDoctorPic);
            }
            else
            {
                if(doctorDetailsResponse.getArrdoctors().get(0).getGender().equalsIgnoreCase("M"))
                {
                    Picasso.with(this).load(R.drawable.male).into(ivDoctorPic);
                }
                else {
                    Picasso.with(this).load(R.drawable.female).into(ivDoctorPic);
                }
            }

            tvClinicHours.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvDoctorName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            if(doctorDetailsResponse.getArrdoctors().get(0).getFullName().startsWith("Dr.")) {
                tvDoctorName.setText(doctorDetailsResponse.getArrdoctors().get(0).getFullName());
                tvDoctorname.setText(doctorDetailsResponse.getArrdoctors().get(0).getFullName());
            }
            else {
                tvDoctorName.setText("Dr. " + doctorDetailsResponse.getArrdoctors().get(0).getFullName());
                tvDoctorname.setText("Dr. " + doctorDetailsResponse.getArrdoctors().get(0).getFullName());
            }

            if(doctorDetailsResponse.getArrdoctors().get(0).getExp() != 0)
                tvExperiance.setText(doctorDetailsResponse.getArrdoctors().get(0).getExp()+" Years Experience");
            else
                tvExperiance.setText("N/A");
            if(doctorDetailsResponse.getArrdoctors().get(0).SpecialityCD!= null && !doctorDetailsResponse.getArrdoctors().get(0).SpecialityCD.isEmpty())
            {
                StringBuilder specialty = new StringBuilder();
                for (String speialityid :doctorDetailsResponse.getArrdoctors().get(0).SpecialityCD.values())
                {
                    specialty.append(Constants.specialitiesMap.get(speialityid)+",");
                }
                tvspeciality.setText(specialty.toString().substring(0,(specialty.toString().length()-1)));
            }
            else
                tvspeciality.setText("");
            if(doctorDetailsResponse.getArrdoctors().get(0).Qualification!= null && !doctorDetailsResponse.getArrdoctors().get(0).Qualification.isEmpty())
                tvDoctorQualification.setText(doctorDetailsResponse.getArrdoctors().get(0).Qualification.values().toString().replace("[", "").replace("]",""));
            else
                tvDoctorQualification.setText("");
        }

//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.VISIBLE);
        llBack.setVisibility(View.VISIBLE);

        ivDoctorPic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Create custom dialog object
                final Context context = DoctorProfileActivity.this;
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


                // Include dialog.xml file
                dialog.setContentView(R.layout.photo);
                // Set dialog title
                dialog.setTitle("");


                // set values for custom dialog components - text, image and button
                //TextView text = (TextView) dialog.findViewById(R.id.textDialog);
                //text.setText("Custom dialog Android example.");
                ImageViewTouch image = (ImageViewTouch) dialog.findViewById(R.id.imageDialog);

                largeimageLoader = new LargeImageLoader(context.getApplicationContext());
                largeimageLoader.DisplayImage(doctorDetailsResponse.getArrdoctors().get(0).getProfileImage(), image);

                dialog.show();


                // if decline button is clicked, close the custom dialog
//                image.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Close dialog
//                        dialog.dismiss();
//                    }
//                });


            }
        });



        llRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent slideactivity = new Intent(DoctorProfileActivity.this, DoctorSearch.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
        changeTabsFont();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PracticeFragment(), "PRACTICE");
        adapter.addFragment(new AboutFragment(), "ABOUT");
        // adapter.addFragment(new OthersFragment(), "OTHERS");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @SuppressLint("ValidFragment")
    public class PracticeFragment extends Fragment
    {
        MapView mapView;
        GoogleMap map;
        DoctorAddress doctorAddress ;
        ImageView ivMenuOne,ivMenuTwo,ivMenuThree;
        ProgressBar pBarMenuOne, pBarMenuTwo, pBarMenuThree,pBarLogo;
        TextView tvMenuPicsCount,tvImagesHeading;
        ImageView ivHospitalPic;
        LinearLayout llClinicThumbnails;

        public PracticeFragment()
        {
            super();
        }

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
        }

        private void loadClinicImages(ArrayList<ClinicDocument> clinicDocs)
        {    String imagePath = "";
             int count = 0;
             boolean isLogo = false;
            for(int i = 0; i < clinicDocs.size(); i++)
            {
                if (!clinicDocs.get(i).getFileName().equalsIgnoreCase("Logo")) {
                    count++;
                    imagePath = clinicDocs.get(i).getFileURL();
                    prepareMenuView(imagePath, count);
                    imgList.add(imagePath);
                }else
                {
                    isLogo = true;
                    Picasso.with(DoctorProfileActivity.this).load(clinicDocs.get(i).getFileURL()).into(ivHospitalPic);
                }
            if(!isLogo)
            {
                ivHospitalPic.setVisibility(View.GONE);
            }

            }

        }

        private void prepareMenuView(String iPath, int menuPhotoCount)
        {
            if (menuPhotoCount == 1)
                bubbleimageLoader.displayImage(iPath, ivMenuOne, getBubbleDIO(), new CustomSimpleImageLoadingListener(pBarMenuOne));
            else if (menuPhotoCount == 2)
                bubbleimageLoader.displayImage(iPath, ivMenuTwo, getBubbleDIO(), new CustomSimpleImageLoadingListener(pBarMenuTwo));
            else if (menuPhotoCount == 3)
                bubbleimageLoader.displayImage(iPath, ivMenuThree, getBubbleDIO(), new CustomSimpleImageLoadingListener(pBarMenuThree));

            if (menuPhotoCount > 3) {
                tvMenuPicsCount.setVisibility(View.VISIBLE);
                tvMenuPicsCount.setText("+" + String.valueOf(menuPhotoCount - 3));
            }
            else
            {
                tvMenuPicsCount.setVisibility(View.GONE);
            }
        }

        public void setLayoutParams(View v, int drawableId)
        {
            int [] dDimens = getDrawableDimensions(v.getContext(), drawableId);
            v.getLayoutParams().width = dDimens[0];
            v.getLayoutParams().height = dDimens[1];
        }

        public int[] getDrawableDimensions(Context context, int id)
        {
            int[] dimensions = new int[2];
            if(context == null)
                return dimensions;
            Drawable d = getResources().getDrawable(id);
            if(d != null)
            {
                dimensions[0] = d.getIntrinsicWidth();
                dimensions[1] = d.getIntrinsicHeight();
            }
            return dimensions;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {


            View                    Practice            =   inflater.inflate(R.layout.fragment_practice, container, false);

            TextView                tvHospitalName      =   (TextView)          Practice.findViewById(R.id.tvHospitalName);
            TextView                tvHospitalAddress   =   (TextView)          Practice.findViewById(R.id.tvHospitalAddress);
            TextView                tvHospitalNo        =   (TextView)          Practice.findViewById(R.id.tvHospitalNo);
            TextView                tvHospitalFee       =   (TextView)          Practice.findViewById(R.id.tvHospitalFee);
            TextView                tvHospitalTime      =   (TextView)          Practice.findViewById(R.id.tvHospitalTime);
            TextView                tvImagesHeading     =   (TextView)          Practice.findViewById(R.id.tvImagesHeading);
            TextView                hospitaltimingmore  =   (TextView)          Practice.findViewById(R.id.hospitaltimingmore);
            final TextView          tvShowAllTimings    =   (TextView)          Practice.findViewById(R.id.tvShowAllTimings);
            TextView                tvshowtimings       =   (TextView)          Practice.findViewById(R.id.tvshowtimings);
            tvMenuPicsCount                             =   (TextView)          Practice.findViewById(R.id.tvMenuPicsCount);
            llClinicThumbnails                          =   (LinearLayout)          Practice.findViewById(R.id.llClinicThumbnails);

            ivMenuOne           =   (ImageView)         Practice.findViewById(R.id.ivMenuOne);
            ivMenuTwo           =   (ImageView)         Practice.findViewById(R.id.ivMenuTwo);
            ivMenuThree         =   (ImageView)         Practice.findViewById(R.id.ivMenuThree);
            ivHospitalPic         = (ImageView)         Practice.findViewById(R.id.ivHospitalPic);

            pBarMenuOne         =   (ProgressBar)       Practice.findViewById(R.id.pBarMenuOne);
            pBarMenuTwo         =   (ProgressBar)       Practice.findViewById(R.id.pBarMenuTwo);
            pBarMenuThree       =   (ProgressBar)       Practice.findViewById(R.id.pBarMenuThree);
            pBarLogo            =   (ProgressBar)       Practice.findViewById(R.id.pBarLogo);

            setLayoutParams(ivMenuOne, R.drawable.pics_samp);
            setLayoutParams(ivMenuTwo, R.drawable.pics_samp);
            setLayoutParams(ivMenuThree, R.drawable.pics_samp);


            LinearLayout            llCallNow           =   (LinearLayout)      Practice.findViewById(R.id.llCallNow);
            final RelativeLayout    rlalltimings        =   (RelativeLayout)    Practice.findViewById(R.id.rlalltimings);

            ivMenuOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if(getMenuImageCount() > 0)
                        startSlideActivity(0,ApplicationConstants.IMG_MENU_TYPE);
                }
            });
            ivMenuTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if(getMenuImageCount() > 1)
                        startSlideActivity(1,ApplicationConstants.IMG_MENU_TYPE);
                }
            });
            ivMenuThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if(getMenuImageCount() > 2)
                        startSlideActivity(2,ApplicationConstants.IMG_MENU_TYPE);
                }
            });
            tvMenuPicsCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if(getMenuImageCount() > 3)
                        startSlideActivity(0,ApplicationConstants.IMG_MENU_TYPE);
                }
            });

            if (doctorDetailsResponse.getArrdoctorAddress().get(0).getExtdetails() != null &&
                    doctorDetailsResponse.getArrdoctorAddress().get(0).getExtdetails().getClinicImages() != null &&
                    doctorDetailsResponse.getArrdoctorAddress().get(0).getExtdetails().getClinicImages().getClinicDocuments() != null)
                loadClinicImages(doctorDetailsResponse.getArrdoctorAddress().get(0).getExtdetails().getClinicImages().getClinicDocuments());

            if (imgList.size() == 0){
                llClinicThumbnails.setVisibility(View.GONE);
                tvImagesHeading.setVisibility(View.GONE);
            }

            if(doctorDetailsResponse.getArrdoctorAddress() != null && !doctorDetailsResponse.getArrdoctorAddress().isEmpty())
            {
                for (DoctorAddress doctorAddress1 : doctorDetailsResponse.getArrdoctorAddress())
                {
                    if(doctorAddress1.getAddressID() == addressid)
                    {
                        doctorAddress = doctorAddress1;
                        break;
                    }
                }
            }
            if(doctorAddress == null)
                doctorAddress = doctorDetailsResponse.getArrdoctorAddress().get(0);
            // Gets the MapView from the XML layout and creates it
            mapView = (MapView) Practice.findViewById(R.id.mapview);
            mapView.onCreate(savedInstanceState);
            // Gets to GoogleMap from the MapView and does initialization stuff
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                   map = googleMap;
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    // map.setMyLocationEnabled(true);
                    // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                    MapsInitializer.initialize(getActivity());
                    // Updates the location and zoom of the MapView
                    if(doctorAddress != null && doctorAddress.getExtdetails() != null)
                    {
                        String latitude = doctorAddress.getExtdetails().getLatitude();
                        String longitude = doctorAddress.getExtdetails().getLongitude() ;
                        LatLng sydney = new LatLng(Float.valueOf(latitude), Float.valueOf(longitude));
                        map.addMarker(new MarkerOptions().position(sydney).title(doctorAddress.getClinicName()));
                        //map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(sydney, 17);
                        map.animateCamera(cameraUpdate);
                    }
                }
            });


            LinearLayout    llPractice          =   (LinearLayout) Practice.findViewById(R.id.llPractice);
            BaseActivity.setSpecificTypeFace(llPractice, ApplicationConstants.WALSHEIM_MEDIUM);
            tvHospitalName.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            if(doctorAddress != null) {
                if (doctorAddress.getActive().equalsIgnoreCase("Y"))
                    tvHospitalName.setText(doctorAddress.getClinicName() + "");
                else
                    tvHospitalName.setText(doctorAddress.getClinicName() + "");
                tvHospitalName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                if (doctorDetailsResponse.getArrdoctorPhones() != null && !doctorDetailsResponse.getArrdoctorPhones().isEmpty())
                    tvHospitalNo.setText(doctorDetailsResponse.getArrdoctorPhones().get(0).getPhNumber() + "");
                llCallNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            Intent intent = null;
                            if (doctorDetailsResponse.getArrdoctorPhones() != null && doctorDetailsResponse.getArrdoctorPhones().get(0).getPhNumber() != null){
                                 intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", doctorDetailsResponse.getArrdoctorPhones().get(0).getPhNumber() + "", ""));
                            }else{
                                intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "", ""));
                            }
//                            callIntent.setData(Uri.parse("tel:" + () ? doctorDetailsResponse.getArrdoctorPhones().get(0).getPhNumber() : ""));
//                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", doctorDetailsResponse.getArrdoctorPhones().get(0).getPhNumber() + "", null));
                            startActivity(intent);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                if (doctorAddress.getAddress1() != null)
                    tvHospitalAddress.setText(doctorAddress.getAddress2());
                else
                    tvHospitalAddress.setText(doctorAddress.getAddress2());
/*                StringBuilder timings = new StringBuilder();
                StringBuilder timings2 = new StringBuilder();
                for (DoctorConsultation doctorconsulation : doctorDetailsResponse.getArrdoctorConsultation()) {
                    if (doctorAddress.getAddressID() == doctorconsulation.getAddressID())
                        tvHospitalFee.setText(doctorconsulation.getFees() + "");
                        if(TODAY_CD.equalsIgnoreCase(doctorconsulation.getDayCD()))
                        {
                            timings2.append(doctorconsulation.getDayCD()+"   "+doctorconsulation.getStartTime() + "-" + doctorconsulation.getEndTime() + "");
                        }else
                        {
                            timings.append(doctorconsulation.getDayCD()+"   "+doctorconsulation.getStartTime() + "-" + doctorconsulation.getEndTime() + ",\n");
                        }
                }
                if(!timings2.toString().isEmpty())
                    hospitaltimingmore.setText(timings2.toString());
                else
                    hospitaltimingmore.setText("Doctor not available today.");
                if(!timings.toString().isEmpty())
                tvshowtimings.setText(timings.toString().substring(0,(timings.toString().length()-2)));
                else
                    tvshowtimings.setText("No more time slots available.");*/
                HashMap<String, StringBuilder> slotsmap = new HashMap<>(7);
                slotsmap.put("MON", new StringBuilder());
                slotsmap.put("TUE", new StringBuilder());
                slotsmap.put("WED", new StringBuilder());
                slotsmap.put("THU", new StringBuilder());
                slotsmap.put("FRI", new StringBuilder());
                slotsmap.put("SAT", new StringBuilder());
                slotsmap.put("SUN", new StringBuilder());

                for (DoctorConsultation doctorconsulation : doctorDetailsResponse.getArrdoctorConsultation()) {
                    if (doctorAddress.getAddressID() == doctorconsulation.getAddressID()){
                        if (doctorconsulation.getFees() != 0) {
                            tvHospitalFee.setText(doctorconsulation.getFees() + "");
                        } else {
                            tvHospitalFee.setText("N/A");
                        }
                    if (doctorconsulation.getActive().equalsIgnoreCase("Y")) {
                        int starthour = 00, endhour = 00;
                        String startmin = "00", endmin = "00";
                        try {
                            starthour = Integer.parseInt(doctorconsulation.getStartTime().split(":")[0]);
                            startmin = doctorconsulation.getStartTime().split(":")[1];
                            endhour = Integer.parseInt(doctorconsulation.getEndTime().split(":")[0]);
                            endmin = doctorconsulation.getEndTime().split(":")[1];
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (starthour > 12 || endhour > 12) {
                            String starttime = "";
                            String endtime = "";
                            if (starthour > 12) {
                                starthour = starthour - 12;
                                if (starthour < 10)
                                    starttime = starttime + "0" + starthour + ":" + startmin + "PM";
                                else
                                    starttime = starttime + starthour + ":" + startmin + "PM";
                            } else {
                                if (starthour < 10)
                                    starttime = starttime + "0" + starthour + ":" + startmin + "AM";
                                else
                                    starttime = starttime + starthour + ":" + startmin + "AM";
                            }
                            if (endhour > 12) {
                                endhour = endhour - 12;
                                if (endhour < 10)
                                    endtime = endtime + "0" + endhour + ":" + endmin + "PM";
                                else
                                    endtime = endtime + endhour + ":" + endmin + "PM";
                            } else {
                                if (endhour < 10)
                                    endtime = endtime + "0" + endhour + ":" + endmin + "AM";
                                else
                                    endtime = endtime + endhour + ":" + endmin + "AM";
                            }
                            slotsmap.get(doctorconsulation.getDayCD()).append(starttime + " - " + endtime + "\n");
                        } else {
                            slotsmap.get(doctorconsulation.getDayCD()).append(doctorconsulation.getStartTime() + "AM - " + doctorconsulation.getEndTime() + "AM\n");
                        }

                    }
                }
            }
                if(slotsmap.get(TODAY_CD.toUpperCase()) != null && !slotsmap.get(TODAY_CD.toUpperCase()).toString().isEmpty())
                {
                    tvHospitalTime.setText(TODAY_CD.toUpperCase() + " ");
                    hospitaltimingmore.setText(slotsmap.get(TODAY_CD.toUpperCase()).toString().substring(0, (slotsmap.get(TODAY_CD.toUpperCase()).toString().length() - 1)));

                }
                else
                {
                    hospitaltimingmore.setText("Doctor not available today.");
                }
                WeekSlotsAdapter weekSlotsAdapter = new WeekSlotsAdapter(slotsmap, TODAY_CD);
                lvtimeslots.setAdapter(weekSlotsAdapter);

                tvShowAllTimings.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        switch (event.getAction())
                        {
                            case MotionEvent.ACTION_UP :
                            {
                                llBack.setVisibility(View.GONE);
                                llClose.setVisibility(View.VISIBLE);
                                ivSearch.setVisibility(View.INVISIBLE);
                                lltimeslots.setVisibility(View.VISIBLE);
                                lltimeslots.startAnimation(slide_up);
                                flBottomBar.setVisibility(View.GONE);
                                tvTopTitle.setText("CLINIC HOURS");
                            }
                            break ;
                        }
                        return true;
                    }
                });

                llClose.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        switch (event.getAction())
                        {
                            case MotionEvent.ACTION_UP :
                            {
                                ivSearch.setVisibility(View.VISIBLE);
                                lltimeslots.setVisibility(View.GONE);
                                lltimeslots.startAnimation(slide_down);
                                llClose.setVisibility(View.GONE);
                                llBack.setVisibility(View.VISIBLE);
                                flBottomBar.setVisibility(View.GONE);
                                tvTopTitle.setText("DOCTOR PROFILE");

                            }
                            break ;
                        }
                        return true;
                    }
                });


            }



            return Practice;
        }

        @Override
        public void onResume() {
            mapView.onResume();
            super.onResume();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mapView.onLowMemory();
        }
    }

    public class WeekSlotsAdapter extends BaseAdapter
    {
        private HashMap<String, StringBuilder> slotsmap ;
        private String today_cd ;

        public WeekSlotsAdapter(HashMap<String, StringBuilder> slotsmap, String today_cd)
        {
            this.slotsmap = slotsmap;
            this.today_cd = today_cd ;
        }

        @Override
        public int getCount() {
            return 7;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if(convertView == null)
                convertView =  LayoutInflater.from(DoctorProfileActivity.this).inflate(R.layout.doctortimescell,null);

            TextView tvdaycd = (TextView) convertView.findViewById(R.id.tvdaycd);
            TextView tvtimes = (TextView) convertView.findViewById(R.id.tvtimes);

            tvdaycd.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvtimes.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            switch (position)
            {
                case 0 :
                    if(today_cd.equalsIgnoreCase("MON"))
                        applyboldiftoday(tvdaycd, tvtimes);
                    tvdaycd.setText("MON");
                    if(!slotsmap.get("MON").toString().isEmpty())
                        tvtimes.setText(slotsmap.get("MON").toString().substring(0,(slotsmap.get("MON").toString().length()-1)));
                    else
                        tvtimes.setText("CLOSED");
                    break;
                case 1 :
                    if(today_cd.equalsIgnoreCase("TUE"))
                        applyboldiftoday(tvdaycd, tvtimes);
                    tvdaycd.setText("TUE");
                    if(!slotsmap.get("TUE").toString().isEmpty())
                        tvtimes.setText(slotsmap.get("TUE").toString().substring(0,(slotsmap.get("TUE").toString().length()-1)));
                    else
                        tvtimes.setText("CLOSED");
                    break;
                case 2 :
                    if(today_cd.equalsIgnoreCase("WED"))
                        applyboldiftoday(tvdaycd, tvtimes);
                    tvdaycd.setText("WED");
                    if(!slotsmap.get("WED").toString().isEmpty())
                        tvtimes.setText(slotsmap.get("WED").toString().substring(0,(slotsmap.get("WED").toString().length()-1)));
                    else
                        tvtimes.setText("CLOSED");
                    break;
                case 3 :
                    if(today_cd.equalsIgnoreCase("THU"))
                        applyboldiftoday(tvdaycd, tvtimes);
                    tvdaycd.setText("THU");
                    if(!slotsmap.get("THU").toString().isEmpty())
                        tvtimes.setText(slotsmap.get("THU").toString().substring(0,(slotsmap.get("THU").toString().length()-1)));
                    else
                        tvtimes.setText("CLOSED");
                    break;
                case 4 :
                    if(today_cd.equalsIgnoreCase("FRI"))
                        applyboldiftoday(tvdaycd, tvtimes);
                    tvdaycd.setText("FRI");
                    if(!slotsmap.get("FRI").toString().isEmpty())
                        tvtimes.setText(slotsmap.get("FRI").toString().substring(0,(slotsmap.get("FRI").toString().length()-1)));
                    else
                        tvtimes.setText("CLOSED");
                    break;
                case 5 :
                    if(today_cd.equalsIgnoreCase("SAT"))
                        applyboldiftoday(tvdaycd, tvtimes);
                    tvdaycd.setText("SAT");
                    if(!slotsmap.get("SAT").toString().isEmpty())
                        tvtimes.setText(slotsmap.get("SAT").toString().substring(0,(slotsmap.get("SAT").toString().length()-1)));
                    else
                        tvtimes.setText("CLOSED");
                    break;
                case 6 :
                    if(today_cd.equalsIgnoreCase("SUN"))
                        applyboldiftoday(tvdaycd, tvtimes);
                    tvdaycd.setText("SUN");
                    if(!slotsmap.get("SUN").toString().isEmpty())
                        tvtimes.setText(slotsmap.get("SUN").toString().substring(0,(slotsmap.get("SUN").toString().length()-1)));
                    else
                        tvtimes.setText("CLOSED");
                    break;
            }
            return convertView;
        }

        private void applyboldiftoday(TextView tvdaycd, TextView tvtimes)
        {
            tvdaycd.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvtimes.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        }
    }

    public class PracticeAdapter extends BaseAdapter
    {
        private Context context ;

        public PracticeAdapter(Context context)
        {
            this.context = context ;
        }

        @Override
        public int getCount() {

            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            convertView =  LayoutInflater.from(context).inflate(R.layout.fragment_practice,parent,false);

            return convertView;
        }
    }
    @SuppressLint("ValidFragment")
    public class AboutFragment extends Fragment
    {

        public AboutFragment()
        {
            super();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View            About              =   inflater.inflate(R.layout.fragment_about, container, false);

            TextView        tvAbout             =   (TextView)       About.findViewById(R.id.tvAbout);
            TextView        tvSpecializations   =   (TextView)       About.findViewById(R.id.tvSpecializations);
            TextView        tvServices          =   (TextView)       About.findViewById(R.id.tvServices);
            TextView        tvAchievements      =   (TextView)       About.findViewById(R.id.tvAchievements);
            TextView        tvNotAvailable      =   (TextView)       About.findViewById(R.id.tvNotAvailable);
            TextView        tvEducation         =   (TextView)       About.findViewById(R.id.tvEducation);

            LinearLayout    llAbout            =   (LinearLayout)   About.findViewById(R.id.llAbout);

            Doctor doctor = doctorDetailsResponse.getArrdoctors().get(0);

            if(doctor.Services != null && !doctor.Services.isEmpty())
            {
                StringBuilder services = new StringBuilder();
                for(String service : doctor.Services.values())
                    services.append(service.trim()+"\n");
                tvServices.setText(services.toString());
            }

            if(doctor.SpecialityCD != null && !doctor.SpecialityCD.isEmpty())
            {
                StringBuilder specialitiesstr = new StringBuilder();
                for(String speciality : doctor.SpecialityCD.values())
                    specialitiesstr.append(Constants.specialitiesMap.get(speciality)+"\n");
                tvSpecializations.setText(specialitiesstr.toString());
            }

            if(doctor.About != null && !doctor.About.isEmpty())
            {
                StringBuilder specialities = new StringBuilder();
                for(String speciality : doctor.About.values())
                    specialities.append(speciality+"\n");
                tvAbout.setText(specialities.toString());
            }

           /* if(doctorDetailsResponse.getArrdoctorAchievements() != null && !doctorDetailsResponse.getArrdoctorAchievements().isEmpty())
            {
                StringBuilder specialities = new StringBuilder();
                for(DoctorAchievements doctorAchievements : doctorDetailsResponse.getArrdoctorAchievements())
                    specialities.append(doctorAchievements.getName()+"\n");
                tvAchievements.setText(specialities.toString());
            }*/
            if(doctorDetailsResponse.getArrdoctorEducation()!= null && !doctorDetailsResponse.getArrdoctorEducation().isEmpty())
            {
                StringBuilder publishes = new StringBuilder();
                for(DoctorEducation doctorEducation : doctorDetailsResponse.getArrdoctorEducation())
                    publishes.append(doctorEducation.getEducationName()+"\n");
                tvEducation.setText(publishes.toString());
            }

            BaseActivity.setSpecificTypeFace(llAbout,ApplicationConstants.WALSHEIM_MEDIUM);

            return About;
        }
    }
    public class AboutAdapter extends BaseAdapter
    {
        private Context context ;

        public AboutAdapter(Context context)
        {
            this.context = context ;
        }

        @Override
        public int getCount() {

            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            convertView =  LayoutInflater.from(context).inflate(R.layout.fragment_about,parent,false);

            return convertView;
        }
    }
    @SuppressLint("ValidFragment")
    public class OthersFragment extends Fragment
    {

        public OthersFragment()
        {
            super();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View            Others                  =   inflater.inflate(R.layout.fragment_doctor_others, container, false);

            TextView        tvAwards        =   (TextView)     Others.findViewById(R.id.tvAwards);
            TextView        tvPublications  =   (TextView)     Others.findViewById(R.id.tvPublications);
            TextView        tvMembership    =   (TextView)     Others.findViewById(R.id.tvMembership);
            TextView        tvNotAvailable  =   (TextView)     Others.findViewById(R.id.tvNotAvailable);

            LinearLayout    llOthers        =   (LinearLayout) Others.findViewById(R.id.llOthers);
            BaseActivity.setSpecificTypeFace(llOthers,ApplicationConstants.WALSHEIM_MEDIUM);

            Doctor doctor = doctorDetailsResponse.getArrdoctors().get(0);
            if(doctor.Awards != null && !doctor.Awards.isEmpty())
            {
                StringBuilder awards = new StringBuilder();
                for(String award : doctor.Awards.values())
                    awards.append(award+"\n");
                tvAwards.setText(awards.toString());
            }

            if(doctor.Membership != null && !doctor.Membership.isEmpty())
            {
                StringBuilder memberships = new StringBuilder();
                for(String membership : doctor.Membership.values())
                    memberships.append(membership+"\n");
                tvMembership.setText(memberships.toString());
            }
            //tv_awards2.setVisibility(View.GONE);

            if(doctorDetailsResponse.getArrdoctorPublishing()!= null && !doctorDetailsResponse.getArrdoctorPublishing().isEmpty())
            {
                StringBuilder publishes = new StringBuilder();
                for(DoctorPublishing doctorPublish : doctorDetailsResponse.getArrdoctorPublishing())
                    publishes.append(doctorPublish.getName()+"\n");
                tvPublications.setText(publishes.toString());
            }

            return Others;
        }
    }
    public class OthersAdapter extends BaseAdapter
    {
        private Context context ;

        public OthersAdapter(Context context)
        {
            this.context = context ;
        }

        @Override
        public int getCount() {

            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            convertView =  LayoutInflater.from(context).inflate(R.layout.fragment_doctor_others,parent,false);

            return convertView;
        }
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                }
            }
        }
    }


    private int getMenuImageCount()
    {
        return imgList.size();
    }

    private void startSlideActivity(int position, int imgTypeId)
    {
        if(position == -1)
            return;
        Intent SlideActivity = new Intent(DoctorProfileActivity.this, FullScreenSlideActivity.class);
        SlideActivity.putExtra("position", position);
        SlideActivity.putExtra("ImageTypeId", imgTypeId);
        SlideActivity.putStringArrayListExtra("imageData", imgList);

        startActivity(SlideActivity);
    }

}
