package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import hCue.Hcue.DAO.DoctorSpecialityDO;
import hCue.Hcue.DAO.PatientAddress;
import hCue.Hcue.DAO.PatientDetails3;
import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.SpecialityDAO;
import hCue.Hcue.DAO.SpecialityDO;
import hCue.Hcue.adapters.CoverFlowAdapter;
import hCue.Hcue.adapters.SpecialityAdapter;
import hCue.Hcue.model.AddUpdatePatientOrderRequest;
import hCue.Hcue.model.CityLookupResponse;
import hCue.Hcue.model.CountryLookupResponse;
import hCue.Hcue.model.GetPatientDetailsReq;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.model.LocationLookupResponse;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.LookupRequest;
import hCue.Hcue.model.PatientImageUploadReq;
import hCue.Hcue.model.PatientLookupResponse;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.model.SearchBySpecialityReq;
import hCue.Hcue.model.SearchBySpecialityRes;
import hCue.Hcue.model.StateLookupResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.CircleImageView;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.GetPatientsResSingleton;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.utils.SelectedConsSingleton;
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 22/06/16.
 */
public class Specialities extends BaseActivity
{
    private LinearLayout llSpecialities, lldropdownpatients, llPatients,llLocationOne,llSearchBox;
    private SpecialityAdapter specialityAdapter;
    private TextView    tvLocationHeaderOne,tvLocationOne;

   /* private String[]    specialityNames  = {"Dentist","General Practitioner","Physiotherapist","Dermatologist","Homeopathy","Cardiologist","Neurologist","Gastroenterologist","Gynaecologist",
            "ENT","Paediatrician","Ophthalmologist","Diabetologist","Urologist","Orthopedist","Nephrologist","Plastic Surgeon","Acupuncture",
            "Ayurveda","Anesthesist", "Veterinary","Psychiatrist"};*/

    /*private String[]    specialityNames  = {"Acupuncture","Anesthesist", "Ayurveda","Cardiologist","Dentist", "Dermatologist", "Diabetologist","ENT",
            "Gastroenterologist","General Practitioner","Gynaecologist","Homeopathy","Nephrologist","Neurologist","Ophthalmologist","Orthopedist","Paediatrician","Physiotherapist",
            "Plastic Surgeon", "Psychiatrist", "Urologist", "Veterinary"};*/

    private String[]    specialityNames  = {"Dentist"};


    /*private String[]    specialityCodes  = {"DNT","GEP","PST","DER","HYM","CAO","NEU","GAS","GYN",
            "ENT","PAE","OPT","DBT","URO","ORT","NEP","PLS","ACP",
            "AYU","ANE", "VTY", "PCT"};*/

    //private String[]    specialityCodes  = {"ACP","ANE","AYU","CAO","DNT","DER","DBT","ENT","GAS","GEP","GYN","HYM","NEP","NEU","OPT","ORT","PAE","PST","PLS","PCT","URO","VTY"};

    private String[]    specialityCodes  = {"DNT"};

    private ArrayList<SpecialityDAO> listSpecialityDAOs ;
    private Preference preference ;
    private LoginResponse loginResponse ;
    private PatientUpdateRequest patientUpdateRequest ;

    private FeatureCoverFlow mCoverFlow;
    private CoverFlowAdapter mAdapter;
    private ArrayList<SpecialityDO> mData = new ArrayList<>(0);
    private int selectedposition = 0;
    private PatientListAdapter patientListAdapter ;
    private ListView lvPatients;
    private boolean isPatientShown = false;
    private CircleImageView ivMenuProfilePic;
    private TextView tvPatientName;
    private ImageView ivDownArrow;
    LookupRequest lookupRequest;

    @Override
    public void initialize()
    {
        mContext = Specialities.this;
        selTabPos = 1;
        llSpecialities  =   (LinearLayout)  getLayoutInflater().inflate(R.layout.specialities_grid,null,false);
        llBody.addView(llSpecialities,baseLayoutParams);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        llHome.setBackgroundResource(R.drawable.menu_selected_bg);
        tvHomeHighlight.setVisibility(View.VISIBLE);
        preference          =  new Preference(Specialities.this);
        lvPatients          = (ListView)    llSpecialities.findViewById(R.id.lvPatients);
        lldropdownpatients  = (LinearLayout) llSpecialities.findViewById(R.id.lldropdownpatients);
        llPatients          = (LinearLayout) llSpecialities.findViewById(R.id.llPatients);
        llLocationOne       = (LinearLayout) llSpecialities.findViewById(R.id.llLocationOne);
        //llSearchBox         = (LinearLayout) llSpecialities.findViewById(R.id.llSearchBox);
        tvLocationHeaderOne = (TextView) llSpecialities.findViewById(R.id.tvLocationHeaderOne);
        tvLocationOne       = (TextView) llSpecialities.findViewById(R.id.tvLocationOne);
        ivMenuProfilePic    = (CircleImageView) llSpecialities.findViewById(R.id.ivMenuProfilePic);
        tvPatientName       = (TextView) llSpecialities.findViewById(R.id.tvPatientName);
        ivDownArrow         = (ImageView) llSpecialities.findViewById(R.id.ivDownArrow);

        setSpecificTypeFace(llSpecialities,ApplicationConstants.WALSHEIM_MEDIUM);
        tvLocationHeaderOne.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvLocationOne.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvPatientName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        llLeftMenu.setVisibility(View.GONE);
        llTop.setVisibility(View.GONE);
        LoginResponseSingleton.setContext(Specialities.this);
        loginResponse = LoginResponseSingleton.getSingleton();
        callLookupTableService();
        initialiseSpecialities();

        final String PhotoUrl = preference.getStringFromPreference("PhotoUrl","");
        if(loginResponse.getListPatientAddress() == null || loginResponse.getListPatientAddress().isEmpty())
        {
            initialisePatientUpdateRequest();
            initialiseProfile();
        }else if(!PhotoUrl.isEmpty() &&  (loginResponse.getPatientImage() == null || loginResponse.getPatientImage().isEmpty()))
        {
            initialisePatientUpdateRequest();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        String encodedString = "";
                        final Bitmap bitmap = Picasso.with(Specialities.this).load(PhotoUrl).get();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                        byte[] b = baos.toByteArray();
                        encodedString= Base64.encodeToString(b, Base64.DEFAULT);
                        if(!encodedString.isEmpty())
                        {
                            PatientImageUploadReq imageUploadReq = new PatientImageUploadReq();
                            imageUploadReq.setImageStr(encodedString);
                            patientUpdateRequest.setProfileImages(encodedString);
                            if (Connectivity.isConnected(Specialities.this))
                            {
                                initialiseProfile();
                                preference.saveStringInPreference("PhotoUrl", "");
                                preference.commitPreference();
                            }else
                            {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                    }
                                });
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        }
        specialityAdapter   =   new SpecialityAdapter(Specialities.this, listSpecialityDAOs);
        prepareSpecailitiesData();
        mAdapter = new CoverFlowAdapter(this);
        mAdapter.setData(mData);
        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCoverFlow.setReflectionOpacity(0);
        mCoverFlow.setAdapter(mAdapter);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectedConsSingleton.getSingleton().setSpecialityDAO(listSpecialityDAOs.get(position));
                Intent slideactivity = new Intent(Specialities.this, BookAppointment.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

       /* mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {}

            @Override
            public void onScrolling() {}
        });*/

       /* llSearchBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Specialities.this,DoctorSearch.class);
                startActivity(intent);
            }
        });*/

        llLocationOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Specialities.this,LocationSearchActivity.class);
                intent.putExtra("isFromSpecialities", true);
                ApplicationConstants.isFromLocationSearch = true;
                startActivity(intent);
            }
        });

        lldropdownpatients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (isPatientShown){
                    try {
                        hideAddLayout(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    isPatientShown = false;
                }else{
                    llPatients.bringToFront();
                    llPatients.setVisibility(View.VISIBLE);
                    try {
                        showAddLayout();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    isPatientShown = true;
                }
            }
        });

        callCountryLookupService();
    }

    private void initialiseProfile()
    {
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).updatePatient(patientUpdateRequest, new RestCallback<LoginResponse>()
        {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
                ShowAlertDialog("Whoops!","Failed to update please try again.",R.drawable.worng);
            }

            @Override
            public void success(final LoginResponse loginResponse1, Response response)
            {
                hideLoader();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(loginResponse1 != null && loginResponse1.getArrPatients()!= null && !loginResponse1.getArrPatients().isEmpty() && loginResponse1.getListPatientAddress()!=null)
                            loginResponse = LoginResponseSingleton.getSingleton(loginResponse1, Specialities.this);
                        final String PhotoUrl = preference.getStringFromPreference("PhotoUrl","");
                        if(!PhotoUrl.isEmpty() &&  (loginResponse.getPatientImage() == null || loginResponse.getPatientImage().isEmpty()))
                        {
                            String encodedString = "";
                            try {
                                final Bitmap bitmap = Picasso.with(Specialities.this).load(PhotoUrl).get();

                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                                byte[] b = baos.toByteArray();

                                encodedString= Base64.encodeToString(b, Base64.DEFAULT);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if(!encodedString.isEmpty())
                            {
                                PatientImageUploadReq imageUploadReq = new PatientImageUploadReq();
                                //imageUploadReq.setPatientID(patientUpdateRequest.getPatientID());
                                imageUploadReq.setImageStr(encodedString);
                                patientUpdateRequest.setProfileImages(encodedString);
                                if (Connectivity.isConnected(Specialities.this))
                                {
                                    initialiseProfile();
                                    preference.saveStringInPreference("PhotoUrl", "");
                                    preference.commitPreference();
                                }else
                                {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }).start();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        callgetPatientsService();
        ApplicationConstants.listPresciptions = new ArrayList<>();
        ApplicationConstants.request = new AddUpdatePatientOrderRequest();
        ApplicationConstants.mapDeliveryAddr = new HashMap<>();
        preference.getStringFromPreference("LATLONG","");
        String temp = preference.getStringFromPreference("LOCALITY","");
        if (temp.split(",")[1].equalsIgnoreCase(" null")){
            tvLocation.setText(temp.split(",")[0]);
        }else{
            tvLocation.setText(temp.split(",")[0] + temp.split(",")[1]);
        }
        tvLocationOne.setText(tvLocation.getText().toString());
        if(LoginResponseSingleton.getSingleton() != null)
            if(LoginResponseSingleton.getSingleton() != null)
                if(LoginResponseSingleton.getSingleton().getPatientImage() != null && !LoginResponseSingleton.getSingleton().getPatientImage().isEmpty())
                {
                    Glide.with(Specialities.this).load(LoginResponseSingleton.getSingleton().getPatientImage()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                }
                else
                {   final String PhotoUrl = preference.getStringFromPreference("PhotoUrl", "");
                    if (!TextUtils.isEmpty(PhotoUrl))
                        Glide.with(Specialities.this).load(PhotoUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                    else
                    {
                        if(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getGender() == 'M')
                        {
                            Glide.with(Specialities.this).load(R.drawable.male_main).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                        }
                        else
                        {
                            Glide.with(Specialities.this).load(R.drawable.female_main).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                        }
                    }
                }

        if(LoginResponseSingleton.getSingleton() != null && LoginResponseSingleton.getSingleton().getArrPatients()!= null && !LoginResponseSingleton.getSingleton().getArrPatients().isEmpty())
        {
            tvPatientName.setText(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getFullName());
            if(LoginResponseSingleton.getSingleton().getListPatientEmail() != null && !LoginResponseSingleton.getSingleton().getListPatientEmail().isEmpty()) {
                tvEmail.setVisibility(View.VISIBLE);
                tvEmail.setText(LoginResponseSingleton.getSingleton().getListPatientEmail().get(0).getEmailID());
            }
            else
            {
                tvEmail.setVisibility(View.GONE);
            }
            if(LoginResponseSingleton.getSingleton().getListPatientPhone() != null && !LoginResponseSingleton.getSingleton().getListPatientPhone().isEmpty()) {
                tvPhone.setVisibility(View.VISIBLE);
                tvPhone.setText(LoginResponseSingleton.getSingleton().getListPatientPhone().get(0).getPhNumber() + "");
            }
            else
            {
                tvPhone.setVisibility(View.GONE);
            }
        }
    }

    private void initialisePatientUpdateRequest()
    {
        patientUpdateRequest = new PatientUpdateRequest();
        patientUpdateRequest.setListPatientAddress(loginResponse.getListPatientAddress());
        patientUpdateRequest.setListPatientEmail(loginResponse.getListPatientEmail());
        patientUpdateRequest.setListPatientPhone(loginResponse.getListPatientPhone());

        patientUpdateRequest.setUSRId(loginResponse.getArrPatients().get(0).getPatientID());

        PatientDetails3 patientDetails3 = new PatientDetails3();
        patientDetails3.setPatientID(loginResponse.getArrPatients().get(0).getPatientID());

        patientDetails3.setGender(loginResponse.getArrPatients().get(0).getGender()+"");
        Calendar calendar1  = Calendar.getInstance();
        calendar1.setTimeInMillis(loginResponse.getArrPatients().get(0).getDOB());
        patientDetails3.setDOB(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()));

        if(loginResponse.getArrPatients().get(0).getArrPatientOtherDetails() != null && !loginResponse.getArrPatients().get(0).getArrPatientOtherDetails().isEmpty())
            patientDetails3.setOtherDetails(loginResponse.getArrPatients().get(0).getArrPatientOtherDetails().get(0));

        if(loginResponse.getArrPatients().get(0).getEmergencyInfo() != null)
            patientDetails3.setEmergencyInfo(loginResponse.getArrPatients().get(0).getEmergencyInfo());

        if(loginResponse.getPatientVitals() != null)
            patientUpdateRequest.setPatientVitals(loginResponse.getPatientVitals());

        //EmergencyInfo emergencyInfo = new EmergencyInfo();
        String latlong = preference.getStringFromPreference("LATLONG", "");
        Log.e("LATLONG VALUE", latlong);
        String[] values = latlong.split(",");
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(Float.valueOf(values[0]),Float.valueOf(values[1]), 1);
            if (addresses != null)
            {
                android.location.Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                final String cityName = addresses.get(0).getLocality();
                final String stateName = addresses.get(0).getSubLocality();

                PatientAddress patientAddress = new PatientAddress();
                patientAddress.setPrimaryIND("Y");
                patientAddress.setLandMark("");
                patientAddress.setAddress2(strReturnedAddress.toString());
                patientAddress.setAddress1("");
                patientAddress.setAddressType("H");
                patientAddress.setCountry(addresses.get(0).getCountryCode());
                patientAddress.setCityTown(cityName);
                patientAddress.setLatitude(values[0]);
                patientAddress.setLongitude(values[1]);
                if(addresses.get(0).getPostalCode() != null)
                    patientAddress.setPinCode(Integer.valueOf(addresses.get(0).getPostalCode()));
                else
                    patientAddress.setPinCode(600024);
                patientAddress.setState("TN");
                patientAddress.setStreet(stateName);
                ArrayList<PatientAddress> arrayAddress = new ArrayList<>(1);
                arrayAddress.add(patientAddress);
                patientUpdateRequest.setListPatientAddress(arrayAddress);
            } else
            {
                PatientAddress patientAddress = new PatientAddress();
                patientAddress.setPrimaryIND("Y");
                patientAddress.setLandMark("");
                patientAddress.setAddress2("");
                patientAddress.setAddress1("");
                patientAddress.setAddressType("H");
                patientAddress.setCountry("IN");
                patientAddress.setCityTown("Chennai");
                patientAddress.setPinCode(600024);
                patientAddress.setState("TN");
                patientAddress.setStreet("");
                ArrayList<PatientAddress> arrayAddress = new ArrayList<>(1);
                arrayAddress.add(patientAddress);
                patientUpdateRequest.setListPatientAddress(arrayAddress);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            PatientAddress patientAddress = new PatientAddress();
            patientAddress.setPrimaryIND("Y");
            patientAddress.setLandMark("");
            patientAddress.setAddress2("");
            patientAddress.setAddress1("");
            patientAddress.setAddressType("H");
            patientAddress.setCountry("IN");
            patientAddress.setCityTown("Chennai");
            patientAddress.setPinCode(600024);
            patientAddress.setState("TN");
            patientAddress.setStreet("");
            ArrayList<PatientAddress> arrayAddress = new ArrayList<>(1);
            arrayAddress.add(patientAddress);
            patientUpdateRequest.setListPatientAddress(arrayAddress);
        }
        patientUpdateRequest.setPatientDetails3(patientDetails3);
    }

    private void initializeSpecialityMap()
    {
        String specialitiesmapString = preference.getStringFromPreference(Constants.DOCTOR_LOOKUP,"");
        Gson gson = new Gson();
        PatientLookupResponse patientLookupResponse = gson.fromJson(specialitiesmapString, PatientLookupResponse.class);
        for (PatientLookupResponse.DoctorSpecialityType doctorSpecialityType : patientLookupResponse.getSpecialityTypeArrayList())
        {
            Constants.specialitiesMap.put(doctorSpecialityType.getDoctorSpecialityID(),doctorSpecialityType.getDoctorSpecialityDesc());
        }
    }

    private void initialiseSpecialities()
    {
        listSpecialityDAOs = new ArrayList<>(specialityCodes.length);
        for(int i=0;i<specialityCodes.length;i++)
        {
            SpecialityDAO specialityDAO = new SpecialityDAO();
            specialityDAO.setSpeciality_code(specialityCodes[i]);
            specialityDAO.setSpecialityname(specialityNames[i]);

            DoctorSpecialityDO doctorSpecialityDO = new DoctorSpecialityDO();
            doctorSpecialityDO.setCategory("Speciality");
            doctorSpecialityDO.setSpecialityID(specialityCodes[i]);
            doctorSpecialityDO.setName(specialityNames[i]);

            listSpecialityDAOs.add(specialityDAO);
            ApplicationConstants.listSpecialityDO.add(doctorSpecialityDO);
        }
    }

    @Override
    public void onBackPressed()
    {
        isFromExit = true;
        super.onBackPressed();
    }

    public void prepareSpecailitiesData() {

        /*mData.add(new SpecialityDO(R.drawable.accupuncture, R.string.acupuncture, getResources().getString(R.string.acupuncture_des),R.drawable.btn_one,"#F38164"));
        mData.add(new SpecialityDO(R.drawable.anesthetist, R.string.anesthesist, getResources().getString(R.string.anesthesist_des),R.drawable.btn_three,"#EA4E52"));
        mData.add(new SpecialityDO(R.drawable.ayurveda, R.string.ayurveda, getResources().getString(R.string.ayurveda_des),R.drawable.btn_two,"#8E85BD"));
        mData.add(new SpecialityDO(R.drawable.cardiologist, R.string.cardiologist, getResources().getString(R.string.cardiologist_des),R.drawable.btn_one,"#F38164"));
        */
        mData.add(new SpecialityDO(R.drawable.justdentalhome, getResources().getString(R.string.justdent_des),R.drawable.home_btn,"#F38164"));

       /* mData.add(new SpecialityDO(R.drawable.dermatologist, R.string.dermatologist, getResources().getString(R.string.dermatologist_des),R.drawable.btn_four,"#31BA99"));
        mData.add(new SpecialityDO(R.drawable.diabetologist, R.string.diabetologist, getResources().getString(R.string.diabetologist_des),R.drawable.btn_one,"#F38164"));
        mData.add(new SpecialityDO(R.drawable.ent, R.string.ent, getResources().getString(R.string.ent_des),R.drawable.btn_five,"#A3CA5A"));
        mData.add(new SpecialityDO(R.drawable.gastrologist, R.string.gastroenterologist, getResources().getString(R.string.gastroenterologist_des),R.drawable.btn_three,"#EA4E52"));
        mData.add(new SpecialityDO(R.drawable.general_prac, R.string.general_practitioner, getResources().getString(R.string.general_practitioner_des),R.drawable.btn_two,"#8E85BD"));
        mData.add(new SpecialityDO(R.drawable.gynecologist, R.string.gynecologist, getResources().getString(R.string.gynecologist_des),R.drawable.btn_four,"#31BA99"));
        mData.add(new SpecialityDO(R.drawable.homeo, R.string.homeopathy, getResources().getString(R.string.homeopathy_des),R.drawable.btn_five,"#A3CA5A"));
        mData.add(new SpecialityDO(R.drawable.nephrologist, R.string.nephrologist, getResources().getString(R.string.nephrologist_des),R.drawable.btn_four,"#31BA99"));
        mData.add(new SpecialityDO(R.drawable.neuroogist, R.string.neurologist, getResources().getString(R.string.neurologist_des),R.drawable.btn_two,"#8E85BD"));
        mData.add(new SpecialityDO(R.drawable.ophtahlmologist, R.string.orhthalmologist, getResources().getString(R.string.orhthalmologist_des),R.drawable.btn_two,"#8E85BD"));
        mData.add(new SpecialityDO(R.drawable.orthopedist, R.string.orthopedist, getResources().getString(R.string.orthopedist_des),R.drawable.btn_three,"#EA4E52"));
        mData.add(new SpecialityDO(R.drawable.paediatrician, R.string.paediatrician, getResources().getString(R.string.paediatrician_des),R.drawable.btn_one,"#F38164"));
        mData.add(new SpecialityDO(R.drawable.phyiotherapist, R.string.physiotherapist, getResources().getString(R.string.physiotherapist_des),R.drawable.btn_three,"#EA4E52"));
        mData.add(new SpecialityDO(R.drawable.pastic_surgeon, R.string.plastic_surgeon, getResources().getString(R.string.plastic_surgeon_des),R.drawable.btn_five,"#A3CA5A"));
        mData.add(new SpecialityDO(R.drawable.psychiatrist, R.string.psychiatrist, getResources().getString(R.string.psychiatrist_des),R.drawable.btn_four,"#31BA99"));
        mData.add(new SpecialityDO(R.drawable.urologist, R.string.urologist, getResources().getString(R.string.urologist_des),R.drawable.btn_two,"#8E85BD"));
        mData.add(new SpecialityDO(R.drawable.veternary, R.string.veterinary, getResources().getString(R.string.veternary_des),R.drawable.btn_three,"#EA4E52"));
    */}

    private void callgetPatientsService()
    {
        LoginResponseSingleton.setContext(Specialities.this);
        GetPatientsRequest getPatientsRequest = new GetPatientsRequest();
        if(LoginResponseSingleton.getSingleton().getListPatientPhone() != null && !LoginResponseSingleton.getSingleton().getListPatientPhone().isEmpty())
            getPatientsRequest.setPhoneNumber(LoginResponseSingleton.getSingleton().getListPatientPhone().get(0).getPhNumber()+"");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(getPatientsRequest, new RestCallback<GetPatientsResponse>() {
            @Override
            public void failure(RestError restError)
            {
                isSinglePatient = true ;
                if (ApplicationConstants.isFromLocationSearch){
                    ApplicationConstants.isFromLocationSearch = false;
                    callServiceGetDoctors();
                }
            }

            @Override
            public void success(GetPatientsResponse getPatientsResponse, Response response) {
                if(getPatientsResponse != null)
                {
                    Preference preference = new Preference(Specialities.this);

                    if(getPatientsResponse.getCount()==1)
                    {
                        isSinglePatient = true ;
                        preference.saveBooleanInPreference(Preference.ISSINGLEPATIENT,true);
                    }else
                    {
                        isSinglePatient = false ;
                        preference.saveBooleanInPreference(Preference.ISSINGLEPATIENT,false);
                    }
                    preference.commitPreference();
                    {
                        GetPatientsResSingleton.getSingleton(getPatientsResponse, Specialities.this);
                        if(!isSinglePatient)
                        {
                            updatePatientsList();
                            ivDownArrow.setVisibility(View.VISIBLE);
                        }else
                        {
                            ivDownArrow.setVisibility(View.GONE);
                        }
                    }
                }
                if (ApplicationConstants.isFromLocationSearch){
                    ApplicationConstants.isFromLocationSearch = false;
                    callServiceGetDoctors();
                }
            }
        });
    }

    private void updatePatientsList()
    {
        patientListAdapter = new PatientListAdapter(GetPatientsResSingleton.getSingleton().getPatientRows());
        lvPatients.setAdapter(patientListAdapter);
    }

    class PatientListAdapter extends BaseAdapter
    {
        private ArrayList<PatientRow> patientRows ;

        public PatientListAdapter(ArrayList<PatientRow> patientRows)
        {
            this.patientRows = patientRows ;
        }

        @Override
        public int getCount() {
            return patientRows.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(Specialities.this).inflate(R.layout.drawer_list_cell, null);
            TextView tvDrawerOperation = (TextView) convertView.findViewById(R.id.tvDrawerOperation);
            CircleImageView ivPatientPic       = (CircleImageView) convertView.findViewById(R.id.ivPatientPic);
            final RadioButton tvCount        = (RadioButton) convertView.findViewById(R.id.tvCount);
            final PatientRow patientRow = patientRows.get(position);
            tvDrawerOperation.setText(patientRow.getArrPatients().get(0).getFullName());
            if (patientRow.getPatientImage() != null ) {
                Glide.with(Specialities.this).load(patientRow.getPatientImage()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivPatientPic);
            }
            else
            {
                if(patientRow.getArrPatients().get(0).getGender() == 'M')
                {
                    Glide.with(Specialities.this).load(R.drawable.male_dropdown).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivPatientPic);
                }
                else {
                    Glide.with(Specialities.this).load(R.drawable.female_drodown).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivPatientPic);
                }
            }
            tvDrawerOperation.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            if(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID()== patientRow.getArrPatients().get(0).getPatientID())
            {
                tvCount.setChecked(true);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvCount.setChecked(true);
                    selectedposition = position ;
                    try {
                        hideAddLayout(patientRow.getArrPatients().get(0).getPatientID());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    isPatientShown = false;
                }
            });
            return convertView;
        }
    }

    private void callgetPatientDataService(long patientID)
    {
        showLoader("Loading");
        GetPatientDetailsReq patientDetailsReq = new GetPatientDetailsReq();
        patientDetailsReq.setPatientID(patientID);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatientDetails(patientDetailsReq, new RestCallback<LoginResponse>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(final LoginResponse loginResponse, Response response) {
                llGroup.setVisibility(View.VISIBLE);
                ivArrow.setBackgroundResource(R.drawable.up);
                hideLoader();
                if(loginResponse != null) {
                    LoginResponseSingleton.getSingleton(loginResponse, Specialities.this);
                    tvPatientName.setText(loginResponse.getArrPatients().get(0).getFullName());
                    if(loginResponse.getListPatientEmail() != null && !loginResponse.getListPatientEmail().isEmpty())
                    {
                        tvEmail.setVisibility(View.VISIBLE);
                        tvEmail.setText(loginResponse.getListPatientEmail().get(0).getEmailID());
                    }else
                    {
                        tvEmail.setVisibility(View.GONE);
                    }
                    if(loginResponse.getListPatientPhone() != null && !loginResponse.getListPatientPhone().isEmpty())
                    {
                        tvPhone.setVisibility(View.VISIBLE);
                        tvPhone.setText(loginResponse.getListPatientPhone().get(0).getPhNumber() + "");
                    }
                    else
                    {
                        tvPhone.setVisibility(View.GONE);
                    }
                    if(LoginResponseSingleton.getSingleton() != null)
                        if(LoginResponseSingleton.getSingleton() != null)
                            if(LoginResponseSingleton.getSingleton().getPatientImage() != null && !LoginResponseSingleton.getSingleton().getPatientImage().isEmpty())
                            {
                                Glide.with(Specialities.this).load(LoginResponseSingleton.getSingleton().getPatientImage()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                                preference.saveStringInPreference("PhotoUrl", "");
                                preference.commitPreference();
                            }
                            else
                            {   final String PhotoUrl = preference.getStringFromPreference("PhotoUrl", "");
                                if (!TextUtils.isEmpty(PhotoUrl))
                                    Glide.with(Specialities.this).load(PhotoUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                                else
                                {
                                    if(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getGender() == 'M')
                                    {
                                        Glide.with(Specialities.this).load(R.drawable.male_main).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                                    }
                                    else
                                    {
                                        Glide.with(Specialities.this).load(R.drawable.female_main).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(ivMenuProfilePic);
                                    }
                                }
                            }
                }
                patientListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void hideAddLayout(final long patientID) throws Exception {
        TranslateAnimation trans = new TranslateAnimation(0, 0,TranslateAnimation.START_ON_FIRST_FRAME, 0 , 0, 0,
                TranslateAnimation.START_ON_FIRST_FRAME, -(GetPatientsResSingleton.getSingleton().getPatientRows().size()*150));
        trans.setDuration(1000);
        llPatients.startAnimation(trans);
        trans.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                llPatients.setVisibility(View.GONE);
                if (patientID != 0){
                    callgetPatientDataService(patientID);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
    }

    private void showAddLayout() throws Exception {

        TranslateAnimation trans = new TranslateAnimation(0, 0,TranslateAnimation.START_ON_FIRST_FRAME, 0 , 0, -GetPatientsResSingleton.getSingleton().getPatientRows().size()*150,
                TranslateAnimation.START_ON_FIRST_FRAME, 0);
        trans.setDuration(1000);
        AnimationSet slideInAnimation = new AnimationSet(false);
        llPatients.startAnimation(trans);
        trans.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                llPatients.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
    }

    private void callServiceGetDoctors()
    {
        showLoader("");
        String latLng = preference.getStringFromPreference("CURRENTLATLONG","");
        SearchBySpecialityReq req = new SearchBySpecialityReq();
        req.setLatitude(latLng.split(",")[0]);
        req.setLongitude(latLng.split(",")[1]);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).searchBySpeciality(req, new RestCallback<SearchBySpecialityRes>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.coming_soon);
            }

            @Override
            public void success(SearchBySpecialityRes resp, Response response) {
                hideLoader();
                if (resp.getCount() == 0){
                    ShowAlertDialog("Whoops!","hCue will be coming soon in your location.",R.drawable.coming_soon);
                }
            }
        });
    }

    private void callCountryLookupService()
    {
        lookupRequest = new LookupRequest();
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getCountryLookup(lookupRequest, new RestCallback<ArrayList<CountryLookupResponse>>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(ArrayList<CountryLookupResponse> countryLookupResponses, Response response) {
                hideLoader();
                if(countryLookupResponses != null && !countryLookupResponses.isEmpty())
                {
                    ApplicationConstants.listCountryDO = countryLookupResponses;
                }
                callStateLookUpResponse();
            }
        });
    }

    private void callCityLookUpResponse() {
        lookupRequest = new LookupRequest();
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getCityLookup(lookupRequest, new RestCallback<ArrayList<CityLookupResponse>>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(ArrayList<CityLookupResponse> cityLookupResponses, Response response) {
                hideLoader();
                if(cityLookupResponses != null && !cityLookupResponses.isEmpty())
                {
                    ApplicationConstants.listCityDO = cityLookupResponses;
                    callLocationLookUpResponse();
                }
            }
        });
    }

    private void callStateLookUpResponse() {
        lookupRequest = new LookupRequest();
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getStateLookup(lookupRequest, new RestCallback<ArrayList<StateLookupResponse>>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(ArrayList<StateLookupResponse> stateLookupResponses, Response response) {
                hideLoader();
                if(stateLookupResponses != null && !stateLookupResponses.isEmpty())
                {
                    ApplicationConstants.listStatesDO = stateLookupResponses;
                }
                callCityLookUpResponse();
            }
        });
    }

    private void callLocationLookUpResponse() {
        lookupRequest = new LookupRequest();
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getLocationLookup(lookupRequest, new RestCallback<ArrayList<LocationLookupResponse>>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
            }

            @Override
            public void success(ArrayList<LocationLookupResponse> locationLookupResponses, Response response) {
                hideLoader();
                if(locationLookupResponses != null && !locationLookupResponses.isEmpty())
                {
                    ApplicationConstants.listLocationDO = locationLookupResponses;
                }

            }
        });
    }


    private void callLookupTableService()
    {
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getdoctorLookup(new RestCallback<PatientLookupResponse>() {
            @Override
            public void failure(RestError restError) {

                callLookupTableService();
            }

            @Override
            public void success(PatientLookupResponse patientLookupResponse, Response response) {
                Gson gson = new Gson();
                String json = gson.toJson(patientLookupResponse);
                preference.saveStringInPreference(Constants.DOCTOR_LOOKUP, json);
                preference.commitPreference();

                if(Constants.specialitiesMap == null || Constants.specialitiesMap.isEmpty())
                    initializeSpecialityMap();
            }
        });
    }
}
