package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-06-2016.
 */
public class PatientDetails2 implements Serializable
{
    @SerializedName("PatientLoginID")
    private String PatientLoginID;

    @SerializedName("PatientPassword")
    private String PatientPassword;

    @SerializedName("FirstName")
    private String FirstName;

    @SerializedName("DOB")
    private String DOB;

    @SerializedName("TermsAccepted")
    private char TermsAccepted = 'Y';

    @SerializedName("FullName")
    private String FullName;

    @SerializedName("Gender")
    private char Gender;

    @SerializedName("MobileID")
    private long MobileID;

    @SerializedName("Title")
    private String Title;

    @SerializedName("PatientOtherDetails")
    private PatientOtherDetails otherDetails;

    @SerializedName("PatientLoginDetails")
    private PatientLoginDetails patientLoginDetails ;

    public String getPatientLoginID() {
        return PatientLoginID;
    }

    public void setPatientLoginID(String patientLoginID) {
        PatientLoginID = patientLoginID;
    }

    public String getPatientPassword() {
        return PatientPassword;
    }

    public void setPatientPassword(String patientPassword) {
        PatientPassword = patientPassword;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public char getTermsAccepted() {
        return TermsAccepted;
    }

    public void setTermsAccepted(char termsAccepted) {
        TermsAccepted = termsAccepted;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public char getGender() {
        return Gender;
    }

    public void setGender(char gender) {
        Gender = gender;
    }

    public long getMobileID() {
        return MobileID;
    }

    public void setMobileID(long mobileID) {
        MobileID = mobileID;
    }

    public PatientOtherDetails getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(PatientOtherDetails otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public PatientLoginDetails getPatientLoginDetails() {
        return patientLoginDetails;
    }

    public void setPatientLoginDetails(PatientLoginDetails patientLoginDetails) {
        this.patientLoginDetails = patientLoginDetails;
    }
}
