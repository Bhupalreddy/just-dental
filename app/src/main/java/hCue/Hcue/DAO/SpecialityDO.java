package hCue.Hcue.DAO;

/**
 * Created by marco.granatiero on 03/02/2015.
 */
public class SpecialityDO {
    private int imageResId;
    private int btnImage;
    private int titleResId;
    private String specialityDesc;
    private String titleColor;

    public SpecialityDO(int imageResId, String specialityDesc, int btnImage, String titleColor){
        this.imageResId = imageResId;
        //this.titleResId = titleResId;
        this.specialityDesc = specialityDesc;
        this.btnImage   =   btnImage;
        this.titleColor =   titleColor;
    }

    public int getBtnImage() {
        return btnImage;
    }

    public void setBtnImage(int btnImage) {
        this.btnImage = btnImage;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public int getTitleResId() {
        return titleResId;
    }

    public void setTitleResId(int titleResId) {
        this.titleResId = titleResId;
    }

    public String getSpecialityDesc() {
        return specialityDesc;
    }

    public void setSpecialityDesc(String specialityDesc) {
        this.specialityDesc = specialityDesc;
    }

    public String getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }
}
