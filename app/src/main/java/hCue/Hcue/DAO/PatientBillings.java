package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientBillings implements Serializable
{
    @SerializedName("patientCaseBillingInfo")
    private ArrayList<PatientCaseBillingInfo> listpatientCaseBillingInfo;

    @SerializedName("patientCaseBillingAmtInfo")
    private ArrayList<PatientCaseBillingAmtInfo> listpatientCaseBillingAmtInfo ;

}
