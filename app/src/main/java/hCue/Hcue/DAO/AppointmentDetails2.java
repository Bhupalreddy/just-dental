package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 17-06-2016.
 */
public class AppointmentDetails2 implements Serializable
{
    @SerializedName("AppointmentID")
    private long AppointmentID ;

    @SerializedName("AppointmentStatusID")
    private char AppointmentStatusID ;

    @SerializedName("ConsultationDt")
    private long ConsultationDt ;

    @SerializedName("DayCD")
    private String DayCD ;

    @SerializedName("FirstTimeVisit")
    private char FirstTimeVisit ;

    @SerializedName("ParentAppointmentID")
    private int ParentAppointmentID ;

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public char getAppointmentStatusID() {
        return AppointmentStatusID;
    }

    public void setAppointmentStatusID(char appointmentStatusID) {
        AppointmentStatusID = appointmentStatusID;
    }

    public long getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public char getFirstTimeVisit() {
        return FirstTimeVisit;
    }

    public void setFirstTimeVisit(char firstTimeVisit) {
        FirstTimeVisit = firstTimeVisit;
    }

    public int getParentAppointmentID() {
        return ParentAppointmentID;
    }

    public void setParentAppointmentID(int parentAppointmentID) {
        ParentAppointmentID = parentAppointmentID;
    }


}
