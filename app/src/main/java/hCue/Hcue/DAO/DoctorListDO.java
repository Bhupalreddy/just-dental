package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class DoctorListDO implements Serializable
{
    @SerializedName("count")
    private int count;
    @SerializedName("doctorsList")
    private ArrayList<GlobalSearchDoctorsDO> doctorsList ;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<GlobalSearchDoctorsDO> getDoctorsList() {
        return doctorsList;
    }

    public void setDoctorsList(ArrayList<GlobalSearchDoctorsDO> doctorsList) {
        this.doctorsList = doctorsList;
    }
}
