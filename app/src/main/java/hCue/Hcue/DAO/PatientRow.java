package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 04-06-2016.
 */
public class PatientRow implements Serializable
{
    @SerializedName("Patient")
    private ArrayList<Patient> arrPatients ;

    @SerializedName("FamilyMenbers")
    private  ArrayList<FamilyMembers> listFamilyMembers;

    @SerializedName("PatientPhone")
    private ArrayList<PatientPhone> listPatientPhone ;

    @SerializedName("PatientAddress")
    private  ArrayList<PatientAddress> listPatientAddress;

    @SerializedName("PatientEmail")
    private ArrayList<PatientEmail> listPatientEmail ;

    @SerializedName("PatientOtherIds")
    private  ArrayList<PatientOtherIds> listPatientOtherIds;

    @SerializedName("PatientImage")
    private  String PatientImage;

    private boolean isselected = false ;

    public ArrayList<Patient> getArrPatients() {
        return arrPatients;
    }

    public void setArrPatients(ArrayList<Patient> arrPatients) {
        this.arrPatients = arrPatients;
    }

    public ArrayList<FamilyMembers> getListFamilyMembers() {
        return listFamilyMembers;
    }

    public void setListFamilyMembers(ArrayList<FamilyMembers> listFamilyMembers) {
        this.listFamilyMembers = listFamilyMembers;
    }

    public ArrayList<PatientPhone> getListPatientPhone() {
        return listPatientPhone;
    }

    public void setListPatientPhone(ArrayList<PatientPhone> listPatientPhone) {
        this.listPatientPhone = listPatientPhone;
    }

    public ArrayList<PatientAddress> getListPatientAddress() {
        return listPatientAddress;
    }

    public void setListPatientAddress(ArrayList<PatientAddress> listPatientAddress) {
        this.listPatientAddress = listPatientAddress;
    }

    public ArrayList<PatientEmail> getListPatientEmail() {
        return listPatientEmail;
    }

    public void setListPatientEmail(ArrayList<PatientEmail> listPatientEmail) {
        this.listPatientEmail = listPatientEmail;
    }

    public ArrayList<PatientOtherIds> getListPatientOtherIds() {
        return listPatientOtherIds;
    }

    public void setListPatientOtherIds(ArrayList<PatientOtherIds> listPatientOtherIds) {
        this.listPatientOtherIds = listPatientOtherIds;
    }

    public boolean isselected() {
        return isselected;
    }

    public void setIsselected(boolean isselected) {
        this.isselected = isselected;
    }

    public String getPatientImage() {
        return PatientImage;
    }

    public void setPatientImage(String patientImage) {
        PatientImage = patientImage;
    }
}
