package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class DoctorAddress implements Serializable
{
    @SerializedName("Active")
    private String Active ;

    @SerializedName("AddressID")
    private int AddressID ;

    @SerializedName("Address1")
    private String Address1 ;

    @SerializedName("Address2")
    private String Address2 ;

    @SerializedName("Street")
    private String Street ;

    @SerializedName("Location")
    private String Location ;

    @SerializedName("CityTown")
    private String CityTown ;

    @SerializedName("DistrictRegion")
    private String DistrictRegion ;

    @SerializedName("State")
    private String State ;

    @SerializedName("PinCode")
    private int PinCode ;

    @SerializedName("Country")
    private String Country ;

    @SerializedName("PrimaryIND")
    private String PrimaryIND ;

    @SerializedName("ClinicName")
    private String ClinicName ;

    @SerializedName("AddressType")
    private String AddressType = "H" ;

    @SerializedName("ExtDetails")
    private Extdetails extdetails ;

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int addressID) {
        AddressID = addressID;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getCityTown() {
        return CityTown;
    }

    public void setCityTown(String cityTown) {
        CityTown = cityTown;
    }

    public String getDistrictRegion() {
        return DistrictRegion;
    }

    public void setDistrictRegion(String districtRegion) {
        DistrictRegion = districtRegion;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public int getPinCode() {
        return PinCode;
    }

    public void setPinCode(int pinCode) {
        PinCode = pinCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPrimaryIND() {
        return PrimaryIND;
    }

    public void setPrimaryIND(String primaryIND) {
        PrimaryIND = primaryIND;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public void setClinicName(String clinicName) {
        ClinicName = clinicName;
    }

    public String getAddressType() {
        return AddressType;
    }

    public void setAddressType(String addressType) {
        AddressType = addressType;
    }

    public Extdetails getExtdetails() {
        return extdetails;
    }

    public void setExtdetails(Extdetails extdetails) {
        this.extdetails = extdetails;
    }
}
