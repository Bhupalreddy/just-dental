package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 17-06-2016.
 */
public class Rows2 implements Serializable
{
    @SerializedName("patientDetails")
    private PatientDetails4 patientDetails ;

    @SerializedName("partnerDetails")
    private PartnerDetails partnerDetails ;

    @SerializedName("appointmentDetails")
    private AppointmentDetails2 appointmentDetails ;

    @SerializedName("RatingEntered")
    private boolean RatingEntered ;

    public PatientDetails4 getPatientDetails() {
        return patientDetails;
    }

    public void setPatientDetails(PatientDetails4 patientDetails) {
        this.patientDetails = patientDetails;
    }

    public PartnerDetails getPartnerDetails() {
        return partnerDetails;
    }

    public void setPartnerDetails(PartnerDetails partnerDetails) {
        this.partnerDetails = partnerDetails;
    }

    public AppointmentDetails2 getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(AppointmentDetails2 appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public boolean isRatingEntered() {
        return RatingEntered;
    }

    public void setRatingEntered(boolean ratingEntered) {
        RatingEntered = ratingEntered;
    }
}
