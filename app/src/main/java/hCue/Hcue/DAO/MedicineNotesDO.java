package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class MedicineNotesDO implements Serializable {


    @SerializedName("IsPresent")
    private String IsPresent ;

    @SerializedName("Medicine")
    private String Medicine ;

    @SerializedName("MedicineNotes")
    private String MedicineNotes ;

    public String getMedicine() {
        return Medicine;
    }

    public void setMedicine(String medicine) {
        Medicine = medicine;
    }

    public String getMedicineNotes() {
        return MedicineNotes;
    }

    public void setMedicineNotes(String medicineNotes) {
        MedicineNotes = medicineNotes;
    }

    public String getIsPresent() {
        return IsPresent;
    }

    public void setIsPresent(String isPresent) {
        IsPresent = isPresent;
    }
}
