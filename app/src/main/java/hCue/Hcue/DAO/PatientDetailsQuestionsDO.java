package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class PatientDetailsQuestionsDO implements Serializable {

    @SerializedName("PatientName")
    private String PatientName ;

    @SerializedName("DOB")
    private String DOB ;

    @SerializedName("Gender")
    private String Gender ;

    @SerializedName("City")
    private String City ;

    @SerializedName("Location")
    private String location ;

    @SerializedName("PatientVitals")
    private QuestionsProfileVitalsDO PatientVitals ;

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public QuestionsProfileVitalsDO getPatientVitals() {
        return PatientVitals;
    }

    public void setPatientVitals(QuestionsProfileVitalsDO patientVitals) {
        PatientVitals = patientVitals;
    }
}
