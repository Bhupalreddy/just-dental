package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientHospital implements Serializable
{
    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("HospitalWrkFlowStatusID")
    private String HospitalWrkFlowStatusID ;

    @SerializedName("PatientCaseID")
    private long PatientCaseID ;

    @SerializedName("ReferralNotes")
    private String ReferralNotes ;

    @SerializedName("RowID")
    private int RowID;

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getHospitalWrkFlowStatusID() {
        return HospitalWrkFlowStatusID;
    }

    public void setHospitalWrkFlowStatusID(String hospitalWrkFlowStatusID) {
        HospitalWrkFlowStatusID = hospitalWrkFlowStatusID;
    }

    public long getPatientCaseID() {
        return PatientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        PatientCaseID = patientCaseID;
    }

    public String getReferralNotes() {
        return ReferralNotes;
    }

    public void setReferralNotes(String referralNotes) {
        ReferralNotes = referralNotes;
    }

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }
}
