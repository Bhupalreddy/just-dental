package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class GlobalSearchResponseDO implements Serializable
{
    @SerializedName("doctors")
    private DoctorListDO Doctors;
    @SerializedName("specialities")
    private ArrayList<GlobalSpecialityDO> listSpecialities ;
    @SerializedName("hospitals")
    private HospitalListDO Hospitals;
    @SerializedName("Symptoms")
    private ArrayList<GlobalSymptomsDO> listSymptoms;
    @SerializedName("Services")
    private ArrayList<GlobalServicesDO> listServices;

    public DoctorListDO getDoctors() {
        return Doctors;
    }

    public void setDoctors(DoctorListDO doctors) {
        Doctors = doctors;
    }

    public ArrayList<GlobalSpecialityDO> getListSpecialities() {
        return listSpecialities;
    }

    public void setListSpecialities(ArrayList<GlobalSpecialityDO> listSpecialities) {
        this.listSpecialities = listSpecialities;
    }

    public HospitalListDO getHospitals() {
        return Hospitals;
    }

    public void setHospitals(HospitalListDO hospitals) {
        Hospitals = hospitals;
    }

    public ArrayList<GlobalSymptomsDO> getListSymptoms() {
        return listSymptoms;
    }

    public void setListSymptoms(ArrayList<GlobalSymptomsDO> listSymptoms) {
        this.listSymptoms = listSymptoms;
    }

    public ArrayList<GlobalServicesDO> getListServices() {
        return listServices;
    }

    public void setListServices(ArrayList<GlobalServicesDO> listServices) {
        this.listServices = listServices;
    }
}
