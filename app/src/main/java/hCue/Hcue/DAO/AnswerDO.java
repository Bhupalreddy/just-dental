package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class AnswerDO implements Serializable {

    @SerializedName("Answer")
    private String Answer ;

    @SerializedName("AnswerID")
    private Integer AnswerID ;

    @SerializedName("AnswerDate")
    private long AnswerDate ;

    @SerializedName("AnswerLiked")
    private String AnswerLiked ;

    @SerializedName("Count")
    private QuestionViewCountDO countDO ;

    @SerializedName("DoctorDetails")
    private DoctorDetailsDO DoctorDetails ;

    private int likeStatus = 0;

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public long getAnswerDate() {
        return AnswerDate;
    }

    public void setAnswerDate(long answerDate) {
        AnswerDate = answerDate;
    }

    public QuestionViewCountDO getCountDO() {
        return countDO;
    }

    public void setCountDO(QuestionViewCountDO countDO) {
        this.countDO = countDO;
    }

    public DoctorDetailsDO getDoctorDetails() {
        return DoctorDetails;
    }

    public void setDoctorDetails(DoctorDetailsDO doctorDetails) {
        DoctorDetails = doctorDetails;
    }

    public Integer getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(Integer answerID) {
        AnswerID = answerID;
    }

    public String getAnswerLiked() {
        return AnswerLiked;
    }

    public void setAnswerLiked(String answerLiked) {
        AnswerLiked = answerLiked;
    }

    public int getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(int likeStatus) {
        this.likeStatus = likeStatus;
    }
}
