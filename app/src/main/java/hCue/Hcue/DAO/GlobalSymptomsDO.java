package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class GlobalSymptomsDO implements Serializable
{
    @SerializedName("SpecialityName")
    private String specialityName;
    @SerializedName("SymptomName")
    private String SymptomName ;
    @SerializedName("SpecialityID")
    private String specialityID ;

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public String getSpecialityID() {
        return specialityID;
    }

    public void setSpecialityID(String specialityID) {
        this.specialityID = specialityID;
    }

    public String getSymptomName() {
        return SymptomName;
    }

    public void setSymptomName(String symptomName) {
        SymptomName = symptomName;
    }
}
