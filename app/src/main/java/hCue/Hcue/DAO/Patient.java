package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 3/18/2016.
 */
public class Patient implements Serializable {

    @SerializedName("Title")
    private String Title ;
    @SerializedName("FirstName")
    private String FirstName ;
    @SerializedName("FullName")
    private String FullName ;
    @SerializedName("PatientID")
    private long PatientID ;
    @SerializedName("Gender")
    private char Gender ;
    @SerializedName("DOB")
    private long DOB ;
    @SerializedName("Age")
    private double Age ;
    @SerializedName("FamilyHdID")
    private long FamilyHdID ;
    @SerializedName("CurrentAge")
    private CurrentAge currentAge ;
    @SerializedName("PatientOtherDetails")
    private ArrayList<PatientOtherDetails> arrPatientOtherDetails ;
    @SerializedName("EmergencyInfo")
    private EmergencyInfo emergencyInfo ;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public char getGender() {
        return Gender;
    }

    public void setGender(char gender) {
        Gender = gender;
    }

    public long getDOB() {
        return DOB;
    }

    public void setDOB(long DOB) {
        this.DOB = DOB;
    }

    public double getAge() {
        return Age;
    }

    public void setAge(double age) {
        Age = age;
    }

    public long getFamilyHdID() {
        return FamilyHdID;
    }

    public void setFamilyHdID(long familyHdID) {
        FamilyHdID = familyHdID;
    }

    public CurrentAge getCurrentAge() {
        return currentAge;
    }

    public void setCurrentAge(CurrentAge currentAge) {
        this.currentAge = currentAge;
    }

    public ArrayList<PatientOtherDetails> getArrPatientOtherDetails() {
        return arrPatientOtherDetails;
    }

    public void setArrPatientOtherDetails(ArrayList<PatientOtherDetails> arrPatientOtherDetails) {
        this.arrPatientOtherDetails = arrPatientOtherDetails;
    }

    public EmergencyInfo getEmergencyInfo() {
        return emergencyInfo;
    }

    public void setEmergencyInfo(EmergencyInfo emergencyInfo) {
        this.emergencyInfo = emergencyInfo;
    }
}
