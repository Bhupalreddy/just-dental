package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 30/11/16.
 */

public class CancelOrderByPatientRequest implements Serializable
{
    @SerializedName("patientId")
    private String  patientId;

    @SerializedName("orderId")
    private int  orderId;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
