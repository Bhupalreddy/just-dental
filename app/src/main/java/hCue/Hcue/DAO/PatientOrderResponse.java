package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by shyamprasadg on 28/11/16.
 */

public class PatientOrderResponse implements Serializable
{
    @SerializedName("rows")
    ArrayList<PharmaInfo> rows;

    @SerializedName("count")
    private int count;

    public ArrayList<PharmaInfo> getRows() {
        return rows;
    }

    public void setRows(ArrayList<PharmaInfo> rows) {
        this.rows = rows;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
