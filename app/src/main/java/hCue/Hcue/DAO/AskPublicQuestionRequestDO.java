package hCue.Hcue.DAO;

import com.google.android.gms.drive.query.Filters;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 10/14/2016.
 */

public class AskPublicQuestionRequestDO implements Serializable
{

    @SerializedName("Question")
    private String Question ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("UsrID")
    private long UsrID ;

    @SerializedName("UsrType")
    private String UsrType = "PATIENT";

    @SerializedName("isPrivate")
    private String isPrivate = "N";

    @SerializedName("ViewCount")
    private int ViewCount ;

    @SerializedName("AdditionalDetails")
    private AdditionalDetailsDO additionalDetails ;

    @SerializedName("Filters")
    private FiltersDO filters ;

    @SerializedName("PatientDetails")
    private PatientDetailsQuestionsDO PatientDetails ;

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public long getUsrID() {
        return UsrID;
    }

    public void setUsrID(long usrID) {
        UsrID = usrID;
    }

    public String getUsrType() {
        return UsrType;
    }

    public void setUsrType(String usrType) {
        UsrType = usrType;
    }

    public String getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        this.isPrivate = isPrivate;
    }

    public int getViewCount() {
        return ViewCount;
    }

    public void setViewCount(int viewCount) {
        ViewCount = viewCount;
    }

    public AdditionalDetailsDO getAdditionalDetails() {
        return additionalDetails;
    }

    public void setAdditionalDetails(AdditionalDetailsDO additionalDetails) {
        this.additionalDetails = additionalDetails;
    }

    public FiltersDO getFilters() {
        return filters;
    }

    public void setFilters(FiltersDO filters) {
        this.filters = filters;
    }

    public PatientDetailsQuestionsDO getPatientDetails() {
        return PatientDetails;
    }

    public void setPatientDetails(PatientDetailsQuestionsDO patientDetails) {
        PatientDetails = patientDetails;
    }
}
