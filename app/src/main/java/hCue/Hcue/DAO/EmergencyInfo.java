package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 09-06-2016.
 */
public class EmergencyInfo implements Serializable
{
    @SerializedName("EmailID")
    private String EmailID ;

    @SerializedName("MobileNumber")
    private String MobileNumber ;

    @SerializedName("Name")
    private String Name ;

    @SerializedName("Relationship")
    private String Relationship ;

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getRelationship() {
        return Relationship;
    }

    public void setRelationship(String relationship) {
        Relationship = relationship;
    }
}
