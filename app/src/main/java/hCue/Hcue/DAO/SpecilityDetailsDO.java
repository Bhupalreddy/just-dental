package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 3/15/2016.
 */
public class SpecilityDetailsDO implements Serializable {

    @SerializedName("SerialNumber")
    private String SerialNumber ;
    @SerializedName("SepcialityCode")
    private int SepcialityCode ;
    @SerializedName("SepcialityDesc")
    private int SepcialityDesc ;
    @SerializedName("CustomSpecialityDesc")
    private String CustomSpecialityDesc ;
    @SerializedName("SpecialityOESDesc")
    private int SpecialityOESDesc ;

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public int getSepcialityCode() {
        return SepcialityCode;
    }

    public void setSepcialityCode(int sepcialityCode) {
        SepcialityCode = sepcialityCode;
    }

    public int getSepcialityDesc() {
        return SepcialityDesc;
    }

    public void setSepcialityDesc(int sepcialityDesc) {
        SepcialityDesc = sepcialityDesc;
    }

    public String getCustomSpecialityDesc() {
        return CustomSpecialityDesc;
    }

    public void setCustomSpecialityDesc(String customSpecialityDesc) {
        CustomSpecialityDesc = customSpecialityDesc;
    }

    public int getSpecialityOESDesc() {
        return SpecialityOESDesc;
    }

    public void setSpecialityOESDesc(int specialityOESDesc) {
        SpecialityOESDesc = specialityOESDesc;
    }
}
