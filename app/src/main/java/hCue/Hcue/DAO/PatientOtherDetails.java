package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Created by User on 3/18/2016.
 */
public class PatientOtherDetails implements Serializable {

    @SerializedName("MaritalStatus")
    private String MaritalStatus ;
    @SerializedName("Education")
    private String Education ;
    @SerializedName("Occupation")
    private String Occupation ;
    @SerializedName("PaymentMode")
    private String PaymentMode ;
    @SerializedName("ReferralSource")
    private LinkedHashMap<String,String> hashmapReferralSource ;
    @SerializedName("Preference")
    private Preference preference ;

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public LinkedHashMap<String, String> getHashmapReferralSource() {
        return hashmapReferralSource;
    }

    public void setHashmapReferralSource(LinkedHashMap<String, String> hashmapReferralSource) {
        this.hashmapReferralSource = hashmapReferralSource;
    }

    public Preference getPreference() {
        return preference;
    }

    public void setPreference(Preference preference) {
        this.preference = preference;
    }

    public class Preference implements Serializable
    {

        @SerializedName("BloodDonor")
        private String BloodDonor = "Y";

        public String getBloodDonor() {
            return BloodDonor;
        }

        public void setBloodDonor(String bloodDonor) {
            BloodDonor = bloodDonor;
        }
    }
}
