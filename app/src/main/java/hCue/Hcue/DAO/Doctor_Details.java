package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by User on 10/13/2016.
 */
public class Doctor_Details  implements Serializable
{

    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("FullName")
    private String FullName ;

    @SerializedName("AddressID")
    private int AddressID ;

    @SerializedName("Active")
    private String Active ;

    @SerializedName("Gender")
    private String Gender ;

    @SerializedName("SpecialityCD")
    private LinkedHashMap<Integer,String> SpecialityCD ;

    @SerializedName("RoleID")
    private String RoleID ;

    @SerializedName("GroupID")
    private String GroupID ;

    @SerializedName("Mobile")
    private Number Mobile ;

    @SerializedName("Email")
    private String Email ;

    @SerializedName("Prospect")
    private String Prospect ;

    @SerializedName("ImageURL")
    private String ImageURL ;

    @SerializedName("DoctorSettings")
    private Doctor_Settings doctor_settings;

    @SerializedName("DoctorLoginID")
    private String DoctorLoginID ;

    @SerializedName("AppointmentCount")
    private int AppointmentCount ;

    @SerializedName("AppointmentDetails")
    private ArrayList<Appointment_Details> appointmentDetailses;

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int addressID) {
        AddressID = addressID;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

    public LinkedHashMap<Integer, String> getSpecialityCD() {
        return SpecialityCD;
    }

    public void setSpecialityCD(LinkedHashMap<Integer, String> specialityCD) {
        SpecialityCD = specialityCD;
    }

    public String getRoleID() {
        return RoleID;
    }

    public void setRoleID(String roleID) {
        RoleID = roleID;
    }

    public String getGroupID() {
        return GroupID;
    }

    public void setGroupID(String groupID) {
        GroupID = groupID;
    }

    public Number getMobile() {
        return Mobile;
    }

    public void setMobile(Number mobile) {
        Mobile = mobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getProspect() {
        return Prospect;
    }

    public void setProspect(String prospect) {
        Prospect = prospect;
    }

    public Doctor_Settings getDoctor_settings() {
        return doctor_settings;
    }

    public void setDoctor_settings(Doctor_Settings doctor_settings) {
        this.doctor_settings = doctor_settings;
    }

    public String getDoctorLoginID() {
        return DoctorLoginID;
    }

    public void setDoctorLoginID(String doctorLoginID) {
        DoctorLoginID = doctorLoginID;
    }

    public int getAppointmentCount() {
        return AppointmentCount;
    }

    public void setAppointmentCount(int appointmentCount) {
        AppointmentCount = appointmentCount;
    }

    public ArrayList<Appointment_Details> getAppointmentDetailses() {
        return appointmentDetailses;
    }

    public void setAppointmentDetailses(ArrayList<Appointment_Details> appointmentDetailses) {
        this.appointmentDetailses = appointmentDetailses;
    }
}
