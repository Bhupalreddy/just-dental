package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 14-06-2016.
 */
public class LabpartnerDetails implements Serializable
{
    @SerializedName("partnerID")
    private int partnerID ;

    @SerializedName("partnerName")
    private String partnerName ;

    @SerializedName("partnersType")
    private String partnersType ;

    public int getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(int partnerID) {
        this.partnerID = partnerID;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnersType() {
        return partnersType;
    }

    public void setPartnersType(String partnersType) {
        this.partnersType = partnersType;
    }
}
