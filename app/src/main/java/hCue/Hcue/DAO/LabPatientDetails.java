package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 14-06-2016.
 */
public class LabPatientDetails implements Serializable
{
    @SerializedName("fillName")
    private String fillName ;

    @SerializedName("gender")
    private char gender ;

    @SerializedName("contactNumber")
    private long contactNumber;

    @SerializedName("CurrentAge")
    private CurrentAge currentAge ;

    public String getFillName() {
        return fillName;
    }

    public void setFillName(String fillName) {
        this.fillName = fillName;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public long getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(long contactNumber) {
        this.contactNumber = contactNumber;
    }

    public CurrentAge getCurrentAge() {
        return currentAge;
    }

    public void setCurrentAge(CurrentAge currentAge) {
        this.currentAge = currentAge;
    }
}
