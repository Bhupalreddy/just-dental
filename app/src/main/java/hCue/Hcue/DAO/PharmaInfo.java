package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by shyamprasadg on 28/11/16.
 */

public class PharmaInfo implements Serializable
{
    @SerializedName("PatientOrderID")
    private int PatientOrderID;

    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("PatientName")
    private String PatientName;

    @SerializedName("PartnerType")
    private String PartnerType;

    @SerializedName("OrderType")
    private String OrderType;

    @SerializedName("OrderDate")
    private long OrderDate;

    @SerializedName("DeliveryTime")
    private long DeliveryTime;

    @SerializedName("OrderStatusID")
    private String OrderStatusID;

    @SerializedName("OrderNotes")
    private String OrderNotes;

    @SerializedName("PrescriptionChioce")
    private String PrescriptionChioce;

    @SerializedName("OrderImageDocuments")
    private ArrayList<OrderImageDocuments> OrderImageDocuments;

    @SerializedName("OrderTextDocuments")
    private ArrayList<OrderTextDocuments> OrderTextDocuments;

    @SerializedName("DeliveryAddressDetails")
    private DeliveryAddressDetails DeliveryAddressDetails;

    @SerializedName("PharmaPhone")
    private PatientPhone PharmaPhone;

    @SerializedName("PharmaDetails")
    private PharmaDetails PharmaDetails;


    @SerializedName("PharmaID")
    private int PharmaID;

    @SerializedName("BillAmount")
    private double BillAmount;

    public int getPatientOrderID() {
        return PatientOrderID;
    }

    public void setPatientOrderID(int patientOrderID) {
        PatientOrderID = patientOrderID;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPartnerType() {
        return PartnerType;
    }

    public void setPartnerType(String partnerType) {
        PartnerType = partnerType;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public long getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(long orderDate) {
        OrderDate = orderDate;
    }

    public long getDeliveryTime() {
        return DeliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        DeliveryTime = deliveryTime;
    }

    public String  getOrderStatusID() {
        return OrderStatusID;
    }

    public void setOrderStatusID(String orderStatusID) {
        OrderStatusID = orderStatusID;
    }

    public String getOrderNotes() {
        return OrderNotes;
    }

    public void setOrderNotes(String orderNotes) {
        OrderNotes = orderNotes;
    }

    public String getPrescriptionChioce() {
        return PrescriptionChioce;
    }

    public void setPrescriptionChioce(String prescriptionChioce) {
        PrescriptionChioce = prescriptionChioce;
    }

    public hCue.Hcue.DAO.DeliveryAddressDetails getDeliveryAddressDetails() {
        return DeliveryAddressDetails;
    }

    public void setDeliveryAddressDetails(hCue.Hcue.DAO.DeliveryAddressDetails deliveryAddressDetails) {
        DeliveryAddressDetails = deliveryAddressDetails;
    }

    public int getPharmaID() {
        return PharmaID;
    }

    public void setPharmaID(int pharmaID) {
        PharmaID = pharmaID;
    }

    public double getBillAmount() {
        return BillAmount;
    }

    public void setBillAmount(double billAmount) {
        BillAmount = billAmount;
    }

    public hCue.Hcue.DAO.PharmaDetails getPharmaDetails() {
        return PharmaDetails;
    }

    public void setPharmaDetails(hCue.Hcue.DAO.PharmaDetails pharmaDetails) {
        PharmaDetails = pharmaDetails;
    }

    public ArrayList<hCue.Hcue.DAO.OrderImageDocuments> getOrderImageDocuments() {
        return OrderImageDocuments;
    }

    public void setOrderImageDocuments(ArrayList<hCue.Hcue.DAO.OrderImageDocuments> orderImageDocuments) {
        OrderImageDocuments = orderImageDocuments;
    }

    public ArrayList<hCue.Hcue.DAO.OrderTextDocuments> getOrderTextDocuments() {
        return OrderTextDocuments;
    }

    public void setOrderTextDocuments(ArrayList<hCue.Hcue.DAO.OrderTextDocuments> orderTextDocuments) {
        OrderTextDocuments = orderTextDocuments;
    }

    public PatientPhone getPharmaPhone() {
        return PharmaPhone;
    }

    public void setPharmaPhone(PatientPhone patientPhone) {
        this.PharmaPhone = patientPhone;
    }
}
