package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 24-06-2016.
 */
public class SlotList implements Serializable
{
    @SerializedName("Available")
    private char Available ;

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("sequence")
    private int sequence ;

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("TokenNumber")
    private int TokenNumber ;

    @SerializedName("PatientInfo")
    private PatientInfo patientInfo ;

    private int AddressConsultid ;

    private String HospitalCode ;

    private String BranchCode ;

    private int hospitalID ;

    private int hospitalParentID ;

    public char getAvailable() {
        return Available;
    }

    public void setAvailable(char available) {
        Available = available;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public int getTokenNumber() {
        return TokenNumber;
    }

    public void setTokenNumber(int tokenNumber) {
        TokenNumber = tokenNumber;
    }

    public PatientInfo getPatientInfo() {
        return patientInfo;
    }

    public void setPatientInfo(PatientInfo patientInfo) {
        this.patientInfo = patientInfo;
    }

    public int getAddressConsultid() {
        return AddressConsultid;
    }

    public void setAddressConsultid(int addressConsultid) {
        AddressConsultid = addressConsultid;
    }

    public String getHospitalCode() {
        return HospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        HospitalCode = hospitalCode;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public int getHospitalID() {
        return hospitalID;
    }

    public void setHospitalID(int hospitalID) {
        this.hospitalID = hospitalID;
    }

    public int getHospitalParentID() {
        return hospitalParentID;
    }

    public void setHospitalParentID(int hospitalParentID) {
        this.hospitalParentID = hospitalParentID;
    }
}
