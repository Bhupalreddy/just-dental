package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class LikeDisLikeAnswerRequestDO implements Serializable {

    @SerializedName("MessageID")
    private Integer MessageID ;

    @SerializedName("ParentMessageID")
    private Integer ParentMessageID ;

    @SerializedName("AnswerID")
    private Integer AnswerID ;

    @SerializedName("PatientID")
    private Long PatientID ;

    @SerializedName("UsrID")
    private Long UsrID ;

    @SerializedName("UsrType")
    private String UsrType ;

    @SerializedName("LikeCount")
    private Integer LikeCount ;

    @SerializedName("UnLikeCount")
    private Integer UnLikeCount ;

    @SerializedName("PatientLikeCount")
    private HashMap<String , String> likeCount;

    public Integer getMessageID() {
        return MessageID;
    }

    public void setMessageID(Integer messageID) {
        MessageID = messageID;
    }

    public Integer getParentMessageID() {
        return ParentMessageID;
    }

    public void setParentMessageID(Integer parentMessageID) {
        ParentMessageID = parentMessageID;
    }

    public Integer getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(Integer answerID) {
        AnswerID = answerID;
    }

    public Long getPatientID() {
        return PatientID;
    }

    public void setPatientID(Long patientID) {
        PatientID = patientID;
    }

    public Long getUsrID() {
        return UsrID;
    }

    public void setUsrID(Long usrID) {
        UsrID = usrID;
    }

    public String getUsrType() {
        return UsrType;
    }

    public void setUsrType(String usrType) {
        UsrType = usrType;
    }

    public HashMap<String, String> getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(HashMap<String, String> likeCount) {
        this.likeCount = likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        LikeCount = likeCount;
    }

    public Integer getUnLikeCount() {
        return UnLikeCount;
    }

    public void setUnLikeCount(Integer unLikeCount) {
        UnLikeCount = unLikeCount;
    }
}
