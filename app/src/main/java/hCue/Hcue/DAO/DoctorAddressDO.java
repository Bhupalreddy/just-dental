package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class DoctorAddressDO implements Serializable {

    @SerializedName("DoctorCountry")
    private String DoctorCountry ;

    @SerializedName("DoctorState")
    private String DoctorState ;

    @SerializedName("DoctorLocation")
    private String DoctorLocation ;

    public String getDoctorCountry() {
        return DoctorCountry;
    }

    public void setDoctorCountry(String doctorCountry) {
        DoctorCountry = doctorCountry;
    }

    public String getDoctorState() {
        return DoctorState;
    }

    public void setDoctorState(String doctorState) {
        DoctorState = doctorState;
    }

    public String getDoctorLocation() {
        return DoctorLocation;
    }

    public void setDoctorLocation(String doctorLocation) {
        DoctorLocation = doctorLocation;
    }
}
