package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class DoctorDetailsDO implements Serializable {

    @SerializedName("ActiveIND")
    private String ActiveIND = "Y" ;

    @SerializedName("DoctorID")
    private long DoctorID ;

    @SerializedName("DoctorName")
    private String DoctorName ;

    @SerializedName("DoctorSpeciality")
    private String DoctorSpeciality ;

    @SerializedName("DoctorGender")
    private String DoctorGender ;

    @SerializedName("ProfileImageURL")
    private String ProfileImageURL ;

    @SerializedName("DoctorAddress")
    private DoctorAddressDO DoctorAddress ;

    public String getActiveIND() {
        return ActiveIND;
    }

    public void setActiveIND(String activeIND) {
        ActiveIND = activeIND;
    }

    public long getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(long doctorID) {
        DoctorID = doctorID;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String doctorName) {
        DoctorName = doctorName;
    }

    public String getDoctorSpeciality() {
        return DoctorSpeciality;
    }

    public void setDoctorSpeciality(String doctorSpeciality) {
        DoctorSpeciality = doctorSpeciality;
    }

    public String getProfileImageURL() {
        return ProfileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        ProfileImageURL = profileImageURL;
    }

    public DoctorAddressDO getDoctorAddress() {
        return DoctorAddress;
    }

    public void setDoctorAddress(DoctorAddressDO doctorAddress) {
        DoctorAddress = doctorAddress;
    }

    public String getDoctorGender() {
        return DoctorGender;
    }

    public void setDoctorGender(String doctorGender) {
        DoctorGender = doctorGender;
    }
}
