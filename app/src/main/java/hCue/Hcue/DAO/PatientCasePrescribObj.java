package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientCasePrescribObj implements Serializable
{
    @SerializedName("BeforeAfter")
    private boolean BeforeAfter ;

    @SerializedName("Diagnostic")
    private String Diagnostic;

    @SerializedName("Dosage1")
    private String Dosage1 = "0";

    @SerializedName("Dosage2")
    private String Dosage2 = "0";

    @SerializedName("Dosage3")
    private String Dosage3 = "0";

    @SerializedName("Dosage4")
    private String Dosage4 ;

    @SerializedName("Manufacturer")
    private String Manufacturer;

    @SerializedName("Medicine")
    private String Medicine;

    @SerializedName("MedicineContainer")
    private String MedicineContainer ;

    @SerializedName("MedicineGenericName")
    private String MedicineGenericName ;

    @SerializedName("MedicineType")
    private String MedicineType ;

    @SerializedName("MRP")
    private double MRP ;

    @SerializedName("NumberofDays")
    private int NumberofDays ;

    @SerializedName("Quantity")
    private int Quantity ;

    @SerializedName("RowID")
    private int RowID ;

    private boolean isAdded;

    public boolean isBeforeAfter() {
        return BeforeAfter;
    }

    public void setBeforeAfter(boolean beforeAfter) {
        BeforeAfter = beforeAfter;
    }

    public String getDiagnostic() {
        return Diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        Diagnostic = diagnostic;
    }

    public String getDosage1() {
        return Dosage1;
    }

    public void setDosage1(String dosage1) {
        Dosage1 = dosage1;
    }

    public String getDosage2() {
        return Dosage2;
    }

    public void setDosage2(String dosage2) {
        Dosage2 = dosage2;
    }

    public String getDosage3() {
        return Dosage3;
    }

    public void setDosage3(String dosage3) {
        Dosage3 = dosage3;
    }

    public String getDosage4() {
        return Dosage4;
    }

    public void setDosage4(String dosage4) {
        Dosage4 = dosage4;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        Manufacturer = manufacturer;
    }

    public String getMedicine() {
        return Medicine;
    }

    public void setMedicine(String medicine) {
        Medicine = medicine;
    }

    public String getMedicineContainer() {
        return MedicineContainer;
    }

    public void setMedicineContainer(String medicineContainer) {
        MedicineContainer = medicineContainer;
    }

    public String getMedicineGenericName() {
        return MedicineGenericName;
    }

    public void setMedicineGenericName(String medicineGenericName) {
        MedicineGenericName = medicineGenericName;
    }

    public String getMedicineType() {
        return MedicineType;
    }

    public void setMedicineType(String medicineType) {
        MedicineType = medicineType;
    }

    public double getMRP() {
        return MRP;
    }

    public void setMRP(double MRP) {
        this.MRP = MRP;
    }

    public int getNumberofDays() {
        return NumberofDays;
    }

    public void setNumberofDays(int numberofDays) {
        NumberofDays = numberofDays;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }
}
