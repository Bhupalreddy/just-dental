package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class GlobalServicesDO implements Serializable
{
    @SerializedName("DoctorIDs")
    private String DoctorIDs;
    @SerializedName("ServiceName")
    private String ServiceName ;

    public String getDoctorIDs() {
        return DoctorIDs;
    }

    public void setDoctorIDs(String doctorIDs) {
        DoctorIDs = doctorIDs;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }
}
