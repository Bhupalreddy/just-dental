package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 09-06-2016.
 */
public class PatientDetails3 implements Serializable
{
    @SerializedName("DOB")
    private String DOB ;
    @SerializedName("FirstName")
    private String FirstName ;
    @SerializedName("FullName")
    private String FullName ;
    @SerializedName("Gender")
    private String Gender ;
    @SerializedName("PatientID")
    private long PatientID ;
    @SerializedName("TermsAccepted")
    private char TermsAccepted = 'Y';
    @SerializedName("PatientOtherDetails")
    private PatientOtherDetails otherDetails;
    @SerializedName("EmergencyInfo")
    private EmergencyInfo emergencyInfo;
    @SerializedName("Title")
    private String Title;
    @SerializedName("FamilyHeadIndicator")
    private String FamilyHeadIndicator = "Y";


    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

/*    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }*/

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public char getTermsAccepted() {
        return TermsAccepted;
    }

    public void setTermsAccepted(char termsAccepted) {
        TermsAccepted = termsAccepted;
    }

    public PatientOtherDetails getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(PatientOtherDetails otherDetails) {
        this.otherDetails = otherDetails;
    }

    public EmergencyInfo getEmergencyInfo() {
        return emergencyInfo;
    }

    public void setEmergencyInfo(EmergencyInfo emergencyInfo) {
        this.emergencyInfo = emergencyInfo;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getFamilyHeadIndicator() {
        return FamilyHeadIndicator;
    }

    public void setFamilyHeadIndicator(String familyHeadIndicator) {
        FamilyHeadIndicator = familyHeadIndicator;
    }
}
