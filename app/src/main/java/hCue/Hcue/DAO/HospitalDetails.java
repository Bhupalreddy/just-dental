package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 20-06-2016.
 */
public class HospitalDetails implements Serializable
{

    @SerializedName("ActiveIND")
    private char ActiveIND ;

    @SerializedName("ContactName")
    private String ContactName ;

    @SerializedName("EmailAddress")
    private String EmailAddress ;

    @SerializedName("HospitalID")
    private int HospitalID ;

    @SerializedName("HospitalName")
    private String HospitalName ;

    @SerializedName("MobileNumber")
    private long MobileNumber ;

    @SerializedName("ParentHospitalID")
    private int ParentHospitalID ;

    @SerializedName("TermsAccepted")
    private char TermsAccepted ;

    public char getActiveIND() {
        return ActiveIND;
    }

    public void setActiveIND(char activeIND) {
        ActiveIND = activeIND;
    }

    public String getContactName() {
        return ContactName;
    }

    public void setContactName(String contactName) {
        ContactName = contactName;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public int getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(int hospitalID) {
        HospitalID = hospitalID;
    }

    public String getHospitalName() {
        return HospitalName;
    }

    public void setHospitalName(String hospitalName) {
        HospitalName = hospitalName;
    }

    public long getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public int getParentHospitalID() {
        return ParentHospitalID;
    }

    public void setParentHospitalID(int parentHospitalID) {
        ParentHospitalID = parentHospitalID;
    }

    public char getTermsAccepted() {
        return TermsAccepted;
    }

    public void setTermsAccepted(char termsAccepted) {
        TermsAccepted = termsAccepted;
    }
}
