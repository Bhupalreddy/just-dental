package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Created by Appdest on 02-08-2016.
 */
public class PrintURLs implements Serializable
{
    @SerializedName("PrintType")
    private String PrintType ;

    @SerializedName("Status")
    private String Status ;

    @SerializedName("Url")
    private String Url ;

    public String getPrintType() {
        return PrintType;
    }

    public void setPrintType(String printType) {
        PrintType = printType;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
