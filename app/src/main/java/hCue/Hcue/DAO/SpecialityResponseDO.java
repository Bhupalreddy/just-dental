package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class SpecialityResponseDO implements Serializable {

    @SerializedName("DoctorSpecialityID")
    private String DoctorSpecialityID ;

    @SerializedName("DoctorSpecialityDesc")
    private String DoctorSpecialityDesc ;

    @SerializedName("SpecialityDesc")
    private String SpecialityDesc ;

    @SerializedName("DoctorSpecialitySEODesc")
    private String DoctorSpecialitySEODesc ;

    @SerializedName("ActiveIND")
    private String ActiveIND ;

    public String getDoctorSpecialityID() {
        return DoctorSpecialityID;
    }

    public void setDoctorSpecialityID(String doctorSpecialityID) {
        DoctorSpecialityID = doctorSpecialityID;
    }

    public String getDoctorSpecialityDesc() {
        return DoctorSpecialityDesc;
    }

    public void setDoctorSpecialityDesc(String doctorSpecialityDesc) {
        DoctorSpecialityDesc = doctorSpecialityDesc;
    }

    public String getSpecialityDesc() {
        return SpecialityDesc;
    }

    public void setSpecialityDesc(String specialityDesc) {
        SpecialityDesc = specialityDesc;
    }

    public String getDoctorSpecialitySEODesc() {
        return DoctorSpecialitySEODesc;
    }

    public void setDoctorSpecialitySEODesc(String doctorSpecialitySEODesc) {
        DoctorSpecialitySEODesc = doctorSpecialitySEODesc;
    }

    public String getActiveIND() {
        return ActiveIND;
    }

    public void setActiveIND(String activeIND) {
        ActiveIND = activeIND;
    }
}
