package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class SpecialityRequestDO implements Serializable
{
    @SerializedName("SearchText")
    private String SearchText = "";

    @SerializedName("ActiveIND")
    private String ActiveIND = "Y";

    public String getSearchText() {
        return SearchText;
    }

    public void setSearchText(String searchText) {
        SearchText = searchText;
    }

    public String getActiveIND() {
        return ActiveIND;
    }

    public void setActiveIND(String activeIND) {
        ActiveIND = activeIND;
    }
}
