package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-06-2016.
 */
public class PatientVitals implements Serializable {

    @SerializedName("Vitals")
    private Vitals vitals;

    @SerializedName("History")
    private History history;

    @SerializedName("HistotyNotes")
    private HistotyNotes histotyNotes;

    public Vitals getVitals() {
        return vitals;
    }

    public void setVitals(Vitals vitals) {
        this.vitals = vitals;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public HistotyNotes getHistotyNotes() {
        return histotyNotes;
    }

    public void setHistotyNotes(HistotyNotes histotyNotes) {
        this.histotyNotes = histotyNotes;
    }
}
