package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class AppointmentDetails implements Serializable
{
    @SerializedName("AddressConsultID")
    private int AddressConsultID ;

    @SerializedName("AppointmentID")
    private long AppointmentID ;

    @SerializedName("AppointmentStatus")
    private char AppointmentStatus ;

    @SerializedName("ClinicName")
    private String ClinicName ;

    @SerializedName("ConsultationDt")
    private long ConsultationDt ;

    @SerializedName("DayCD")
    private String DayCD ;

    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("DoctorVisitRsnID")
    private String DoctorVisitRsnID ;

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("FirstTimeVisit")
    private char FirstTimeVisit ;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("TokenNumber")
    private String TokenNumber ;

    @SerializedName("VisitUserTypeID")
    private String VisitUserTypeID ;

    @SerializedName("OtherVisitRsn")
    private String OtherVisitRsn;

    public int getAddressConsultID() {
        return AddressConsultID;
    }

    public void setAddressConsultID(int addressConsultID) {
        AddressConsultID = addressConsultID;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public char getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(char appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public void setClinicName(String clinicName) {
        ClinicName = clinicName;
    }

    public long getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getDoctorVisitRsnID() {
        return DoctorVisitRsnID;
    }

    public void setDoctorVisitRsnID(String doctorVisitRsnID) {
        DoctorVisitRsnID = doctorVisitRsnID;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public char getFirstTimeVisit() {
        return FirstTimeVisit;
    }

    public void setFirstTimeVisit(char firstTimeVisit) {
        FirstTimeVisit = firstTimeVisit;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getTokenNumber() {
        return TokenNumber;
    }

    public void setTokenNumber(String tokenNumber) {
        TokenNumber = tokenNumber;
    }

    public String getVisitUserTypeID() {
        return VisitUserTypeID;
    }

    public void setVisitUserTypeID(String visitUserTypeID) {
        VisitUserTypeID = visitUserTypeID;
    }

    public String getOtherVisitRsn() {
        return OtherVisitRsn;
    }

    public void setOtherVisitRsn(String otherVisitRsn) {
        OtherVisitRsn = otherVisitRsn;
    }
}
