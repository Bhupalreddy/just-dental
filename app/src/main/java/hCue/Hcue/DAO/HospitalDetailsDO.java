package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class HospitalDetailsDO implements Serializable
{
    @SerializedName("BranchCD")
    private String BranchCD;
    @SerializedName("HospitalID")
    private Integer HospitalID ;
    @SerializedName("HospitalCD")
    private String HospitalCD ;
    @SerializedName("ParentHospitalID")
    private Integer ParentHospitalID ;

    public String getBranchCD() {
        return BranchCD;
    }

    public void setBranchCD(String branchCD) {
        BranchCD = branchCD;
    }

    public Integer getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(Integer hospitalID) {
        HospitalID = hospitalID;
    }

    public String getHospitalCD() {
        return HospitalCD;
    }

    public void setHospitalCD(String hospitalCD) {
        HospitalCD = hospitalCD;
    }

    public Integer getParentHospitalID() {
        return ParentHospitalID;
    }

    public void setParentHospitalID(Integer parentHospitalID) {
        ParentHospitalID = parentHospitalID;
    }
}
