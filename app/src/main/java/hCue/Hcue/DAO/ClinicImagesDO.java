package hCue.Hcue.DAO;

import java.util.ArrayList;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import hCue.Hcue.DAO.ClinicDocument;

/**
 * Created by Appdest on 11-06-2016.
 */
public class ClinicImagesDO implements Serializable
{
    @SerializedName("ClinicDocuments")
    private ArrayList<ClinicDocument> ClinicDocuments ;


    public ArrayList<ClinicDocument> getClinicDocuments() {
        return ClinicDocuments;
    }

    public void setClinicDocuments(ArrayList<ClinicDocument> clinicDocuments) {
        ClinicDocuments = clinicDocuments;
    }
}
