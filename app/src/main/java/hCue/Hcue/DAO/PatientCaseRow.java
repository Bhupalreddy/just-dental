package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 29-07-2016.
 */
public class PatientCaseRow implements Serializable
{
    @SerializedName("caseID")
    private long caseID ;

    @SerializedName("RowID")
    private int RowID ;

    @SerializedName("consultationDt")
    private long consultationDt;

    @SerializedName("Medicine")
    private String Medicine ;

    @SerializedName("LabName")
    private String LabName;

    @SerializedName("LabTest")
    private String LabTest ;

    @SerializedName("Status")
    private String Status;

    @SerializedName("OtherVistReason")
    private String OtherVistReason ;

    @SerializedName("startTime")
    private String startTime ;

    @SerializedName("StartTime")
    private String startTime1 ;

    @SerializedName("DoctorDetails")
    private CaseDoctorDetails listDoctorDetails ;

    @SerializedName("doctorDetails")
    private CaseDoctorDetails listDoctorDetails1 ;

    @SerializedName("caseHistroy")
    private ArrayList<CaseHistory> listCaseHistory ;

    @SerializedName("casePreHistroy")
    private PatientCasePrescribObj patientprHistory;

    @SerializedName("caseLabHistroy")
    private CaseLabHistory caseLabHistory ;

    @SerializedName("VoiceDocuments")
    private CaseDocuments VoiceDocuments ;

    @SerializedName("caseDocuments")
    private CaseDocuments caseDocuments ;

    @SerializedName("PrintURLs")
    private ArrayList<PrintURLs> printURLses ;

    public ArrayList<PrintURLs> getPrintURLses() {
        return printURLses;
    }

    public void setPrintURLses(ArrayList<PrintURLs> printURLses) {
        this.printURLses = printURLses;
    }

    private boolean isShow = true ;
    private boolean isTodayRecord = false ;

    public long getCaseID() {
        return caseID;
    }

    public void setCaseID(long caseID) {
        this.caseID = caseID;
    }

    public long getConsultationDt() {
        return consultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        this.consultationDt = consultationDt;
    }

    public String getOtherVistReason() {
        return OtherVistReason;
    }

    public void setOtherVistReason(String otherVistReason) {
        OtherVistReason = otherVistReason;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public CaseDoctorDetails getListDoctorDetails() {
        return listDoctorDetails;
    }

    public void setListDoctorDetails(CaseDoctorDetails listDoctorDetails) {
        this.listDoctorDetails = listDoctorDetails;
    }

    public ArrayList<CaseHistory> getListCaseHistory() {
        return listCaseHistory;
    }

    public void setListCaseHistory(ArrayList<CaseHistory> listCaseHistory) {
        this.listCaseHistory = listCaseHistory;
    }

    public String getMedicine() {
        return Medicine;
    }

    public void setMedicine(String medicine) {
        Medicine = medicine;
    }

    public PatientCasePrescribObj getPatientprHistory() {
        return patientprHistory;
    }

    public void setPatientprHistory(PatientCasePrescribObj patientprHistory) {
        this.patientprHistory = patientprHistory;
    }

    public String getLabName() {
        return LabName;
    }

    public void setLabName(String labName) {
        LabName = labName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public CaseLabHistory getCaseLabHistory() {
        return caseLabHistory;
    }

    public void setCaseLabHistory(CaseLabHistory caseLabHistory) {
        this.caseLabHistory = caseLabHistory;
    }

    public String getLabTest() {
        return LabTest;
    }

    public void setLabTest(String labTest) {
        LabTest = labTest;
    }

    public CaseDoctorDetails getListDoctorDetails1() {
        return listDoctorDetails1;
    }

    public void setListDoctorDetails1(CaseDoctorDetails listDoctorDetails1) {
        this.listDoctorDetails1 = listDoctorDetails1;
    }

    public String getStartTime1() {
        return startTime1;
    }

    public void setStartTime1(String startTime1) {
        this.startTime1 = startTime1;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public CaseDocuments getCaseDocuments() {
        return caseDocuments;
    }

    public void setCaseDocuments(CaseDocuments caseDocuments) {
        this.caseDocuments = caseDocuments;
    }

    public CaseDocuments getVoiceDocuments() {
        return VoiceDocuments;
    }

    public void setVoiceDocuments(CaseDocuments voiceDocuments) {
        VoiceDocuments = voiceDocuments;
    }

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }

    public boolean isTodayRecord() {
        return isTodayRecord;
    }

    public void setTodayRecord(boolean todayRecord) {
        isTodayRecord = todayRecord;
    }

}
