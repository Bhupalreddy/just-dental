package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 10/14/2016.
 */

public class Doctor_Settings implements Serializable
{
    @SerializedName("CountryCode")
    private String CountryCode ;

    @SerializedName("CurrencyCode")
    private String CurrencyCode ;

    @SerializedName("MaxDoc")
    private String MaxDoc ;

    @SerializedName("CLINIC_LEVEL_FLAG")
    private String CLINIC_LEVEL_FLAG ;

    @SerializedName("MaxBranch")
    private String MaxBranch ;

    @SerializedName("MaxRecep")
    private String MaxRecep ;

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        CurrencyCode = currencyCode;
    }

    public String getMaxDoc() {
        return MaxDoc;
    }

    public void setMaxDoc(String maxDoc) {
        MaxDoc = maxDoc;
    }

    public String getCLINIC_LEVEL_FLAG() {
        return CLINIC_LEVEL_FLAG;
    }

    public void setCLINIC_LEVEL_FLAG(String CLINIC_LEVEL_FLAG) {
        this.CLINIC_LEVEL_FLAG = CLINIC_LEVEL_FLAG;
    }

    public String getMaxBranch() {
        return MaxBranch;
    }

    public void setMaxBranch(String maxBranch) {
        MaxBranch = maxBranch;
    }

    public String getMaxRecep() {
        return MaxRecep;
    }

    public void setMaxRecep(String maxRecep) {
        MaxRecep = maxRecep;
    }
}
