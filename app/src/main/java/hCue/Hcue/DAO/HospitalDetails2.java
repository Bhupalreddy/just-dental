package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 20-06-2016.
 */
public class HospitalDetails2 implements Serializable
{

    @SerializedName("HospitalDetails")
    private HospitalDetails hospitalDetails ;

    @SerializedName("HospitalAddress")
    private PatientAddress hospitalAddress ;

    public HospitalDetails getHospitalDetails() {
        return hospitalDetails;
    }

    public void setHospitalDetails(HospitalDetails hospitalDetails) {
        this.hospitalDetails = hospitalDetails;
    }

    public PatientAddress getHospitalAddress() {
        return hospitalAddress;
    }

    public void setHospitalAddress(PatientAddress hospitalAddress) {
        this.hospitalAddress = hospitalAddress;
    }
}
