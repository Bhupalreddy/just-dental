package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class TreatmentDocumentsURLDO implements Serializable {

    @SerializedName("DocumentNumber")
    private String DocumentNumber ;

    @SerializedName("FileExtn")
    private String FileExtn ;

    @SerializedName("FileName")
    private String FileName ;

    @SerializedName("FileURL")
    private String FileURL ;

    @SerializedName("UpLoadedBy")
    private String UpLoadedBy ;

    @SerializedName("UpLoadedDate")
    private String UpLoadedDate ;

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }

    public String getFileExtn() {
        return FileExtn;
    }

    public void setFileExtn(String fileExtn) {
        FileExtn = fileExtn;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileURL() {
        return FileURL;
    }

    public void setFileURL(String fileURL) {
        FileURL = fileURL;
    }

    public String getUpLoadedBy() {
        return UpLoadedBy;
    }

    public void setUpLoadedBy(String upLoadedBy) {
        UpLoadedBy = upLoadedBy;
    }

    public String getUpLoadedDate() {
        return UpLoadedDate;
    }

    public void setUpLoadedDate(String upLoadedDate) {
        UpLoadedDate = upLoadedDate;
    }
}
