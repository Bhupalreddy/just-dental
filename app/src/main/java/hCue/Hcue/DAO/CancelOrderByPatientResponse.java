package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 30/11/16.
 */

public class CancelOrderByPatientResponse implements Serializable
{
    @SerializedName("status")
    private String  status;

    @SerializedName("result")
    private String  result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
