package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 3/18/2016.
 */
public class PatientEmail implements Serializable {

    @SerializedName("EmailID")
    private String EmailID ;
    @SerializedName("EmailIDType")
    private String EmailIDType ;
    @SerializedName("PrimaryIND")
    private String PrimaryIND ;

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getEmailIDType() {
        return EmailIDType;
    }

    public void setEmailIDType(String emailIDType) {
        EmailIDType = emailIDType;
    }

    public String getPrimaryIND() {
        return PrimaryIND;
    }

    public void setPrimaryIND(String primaryIND) {
        PrimaryIND = primaryIND;
    }
}
