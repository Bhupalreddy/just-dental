package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 11/17/2016.
 */

public class OrderImageDocuments implements Serializable
{
    @SerializedName("FileURL")
    private String FileURL ;

    @SerializedName("FileName")
    private String FileName ;

    @SerializedName("FileExtn")
    private String FileExtn ;

    @SerializedName("DocumentNumber")
    private String DocumentNumber ;

    @SerializedName("UpLoadedDate")
    private long UpLoadedDate ;

    @SerializedName("UpLoadedBy")
    private String UpLoadedBy = "PATIENT";

    public String getFileURL() {
        return FileURL;
    }

    public void setFileURL(String fileURL) {
        FileURL = fileURL;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileExtn() {
        return FileExtn;
    }

    public void setFileExtn(String fileExtn) {
        FileExtn = fileExtn;
    }

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }

    public long getUpLoadedDate() {
        return UpLoadedDate;
    }

    public void setUpLoadedDate(long upLoadedDate) {
        UpLoadedDate = upLoadedDate;
    }

    public String getUpLoadedBy() {
        return UpLoadedBy;
    }

    public void setUpLoadedBy(String upLoadedBy) {
        UpLoadedBy = upLoadedBy;
    }
}


