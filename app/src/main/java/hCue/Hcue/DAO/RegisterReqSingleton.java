package hCue.Hcue.DAO;

import java.io.Serializable;

import hCue.Hcue.model.RegisterRequest;


/**
 * Created by Appdest on 06-06-2016.
 */
public class RegisterReqSingleton implements Serializable
{
    private static RegisterRequest registerRequest ;

    private RegisterReqSingleton() {
    }

    public static RegisterRequest getSingleton()
    {
        if(registerRequest == null)
        {
            registerRequest = new RegisterRequest();
        }
        return  registerRequest ;
    }

    public Object clone()
            throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }
}
