package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 10/14/2016.
 */

public class Appointment_Details implements Serializable
{

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("Available")
    private String Available ;

    @SerializedName("TokenNumber")
    private int TokenNumber ;

    @SerializedName("PatientInfo")
    private Patient_Info patientInfo ;

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getAvailable() {
        return Available;
    }

    public void setAvailable(String available) {
        Available = available;
    }

    public int getTokenNumber() {
        return TokenNumber;
    }

    public void setTokenNumber(int tokenNumber) {
        TokenNumber = tokenNumber;
    }

    public Patient_Info getPatientInfo() {
        return patientInfo;
    }

    public void setPatientInfo(Patient_Info patientInfo) {
        this.patientInfo = patientInfo;
    }
}
