package hCue.Hcue.DAO;

import java.io.Serializable;

/**
 * Created by Appdest on 08-06-2016.
 */
public class ClinicdetailsDAO implements Serializable
{

    private String clinicname ;
    private int cliniclogo ;
    private String clinincAddress ;
    private String latlang ;
    private String phone ;
    private int Hospitalid ;
    private int AddressConsultid ;

    public String getClinicname() {
        return clinicname;
    }

    public void setClinicname(String clinicname) {
        this.clinicname = clinicname;
    }

    public int getCliniclogo() {
        return cliniclogo;
    }

    public void setCliniclogo(int cliniclogo) {
        this.cliniclogo = cliniclogo;
    }

    public String getClinincAddress() {
        return clinincAddress;
    }

    public void setClinincAddress(String clinincAddress) {
        this.clinincAddress = clinincAddress;
    }

    public String getLatlang() {
        return latlang;
    }

    public void setLatlang(String latlang) {
        this.latlang = latlang;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getHospitalid() {
        return Hospitalid;
    }

    public void setHospitalid(int hospitalid) {
        Hospitalid = hospitalid;
    }

    public int getAddressConsultid() {
        return AddressConsultid;
    }

    public void setAddressConsultid(int addressConsultid) {
        AddressConsultid = addressConsultid;
    }
}
