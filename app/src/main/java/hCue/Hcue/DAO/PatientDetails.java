package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 3/18/2016.
 */
public class PatientDetails implements Serializable {

    @SerializedName("Title")
    private String Title;

    @SerializedName("FirstName")
    private String FirstName;

    @SerializedName("FullName")
    private String FullName;

    @SerializedName("MobileID")
    private long MobileID;

    @SerializedName("TermsAccepted")
    private char TermsAccepted = '1';

    @SerializedName("Age")
    private double Age;

    @SerializedName("Gender")
    private char Gender;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public long getMobileID() {
        return MobileID;
    }

    public void setMobileID(long mobileID) {
        MobileID = mobileID;
    }

    public char getTermsAccepted() {
        return TermsAccepted;
    }

    public void setTermsAccepted(char termsAccepted) {
        TermsAccepted = termsAccepted;
    }

    public double getAge() {
        return Age;
    }

    public void setAge(double age) {
        Age = age;
    }

    public char getGender() {
        return Gender;
    }

    public void setGender(char gender) {
        Gender = gender;
    }
}
