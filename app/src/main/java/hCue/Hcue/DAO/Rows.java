package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by User on 10/13/2016.
 */
public class Rows implements Serializable
{
    @SerializedName("DoctorDetails")
    public ArrayList<Doctor_Details> doctorInfo;

    @SerializedName("HospitalInfo")
    public Hospital_Info hospitalInfos;


    @SerializedName("Services")
    private ArrayList<String> Services ;

    @SerializedName("DoctorCount")
    public int DoctorCount;

    public ArrayList<String> getServices() {
        return Services;
    }

    public void setServices(ArrayList<String> services) {
        Services = services;
    }

    public ArrayList<Doctor_Details> getDoctorInfo() {
        return doctorInfo;
    }

    public void setDoctorInfo(ArrayList<Doctor_Details> doctorInfo) {
        this.doctorInfo = doctorInfo;
    }

    public Hospital_Info getHospitalInfos() {
        return hospitalInfos;
    }

    public void setHospitalInfos(Hospital_Info hospitalInfos) {
        this.hospitalInfos = hospitalInfos;
    }

    public int getDoctorCount() {
        return DoctorCount;
    }

    public void setDoctorCount(int doctorCount) {
        DoctorCount = doctorCount;
    }
}
