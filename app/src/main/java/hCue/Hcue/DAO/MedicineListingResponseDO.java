package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class MedicineListingResponseDO implements Serializable
{
    @SerializedName("Medicine")
    private String Medicine;

    @SerializedName("Strength")
    private String Strength;

    @SerializedName("MedicineType")
    private String MedicineType ;

    @SerializedName("AvailableStock")
    private int AvailableStock;

    @SerializedName("MedicineItemID")
    private int MedicineItemID ;

    public String getMedicine() {
        return Medicine;
    }

    public void setMedicine(String medicine) {
        Medicine = medicine;
    }

    public String getStrength() {
        return Strength;
    }

    public void setStrength(String strength) {
        Strength = strength;
    }

    public String getMedicineType() {
        return MedicineType;
    }

    public void setMedicineType(String medicineType) {
        MedicineType = medicineType;
    }

    public int getAvailableStock() {
        return AvailableStock;
    }

    public void setAvailableStock(int availableStock) {
        AvailableStock = availableStock;
    }

    public int getMedicineItemID() {
        return MedicineItemID;
    }

    public void setMedicineItemID(int medicineItemID) {
        MedicineItemID = medicineItemID;
    }
}
