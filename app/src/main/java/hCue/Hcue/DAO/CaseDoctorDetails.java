package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 29-07-2016.
 */
public class CaseDoctorDetails implements Serializable
{
    @SerializedName("ClinicName")
    private String ClinicName ;

    @SerializedName("DoctorID")
    private int DoctorID;

    @SerializedName("DoctorName")
    private String DoctorName ;

    @SerializedName("Location")
    private String Location;

    @SerializedName("ProfileImageURL")
    private String ProfileImageURL;

    @SerializedName("Specialization")
    private ArrayList<DoctorSpecialisation> listDoctorSpecolisations ;

    public String getClinicName() {
        return ClinicName;
    }

    public void setClinicName(String clinicName) {
        ClinicName = clinicName;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String doctorName) {
        DoctorName = doctorName;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getProfileImageURL() {
        return ProfileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        ProfileImageURL = profileImageURL;
    }

    public ArrayList<DoctorSpecialisation> getListDoctorSpecolisations() {
        return listDoctorSpecolisations;
    }

    public void setListDoctorSpecolisations(ArrayList<DoctorSpecialisation> listDoctorSpecolisations) {
        this.listDoctorSpecolisations = listDoctorSpecolisations;
    }
}
