package hCue.Hcue.DAO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Appdest on 08-06-2016.
 */
public class SelectedConsultation implements Serializable
{
    private ClinicdetailsDAO clinicdetailsDAO ;
    private SpecialityDAO specialityDAO ;
    private Calendar selected_date ;
    private String starttime ;
    private String endtime ;
    private String address ;
    private String hospitalcode ;
    private String branchcode ;
    private LabTypeDAO labTypeDAO ;
    private int AddressConsultId ;
    private int hospitalID ;
    private int parentHospitalID ;
    private int tokennumber ;

    public ClinicdetailsDAO getClinicdetailsDAO() {
        return clinicdetailsDAO;
    }

    public void setClinicdetailsDAO(ClinicdetailsDAO clinicdetailsDAO) {
        this.clinicdetailsDAO = clinicdetailsDAO;
    }

    public SpecialityDAO getSpecialityDAO() {
        return specialityDAO;
    }

    public void setSpecialityDAO(SpecialityDAO specialityDAO) {
        this.specialityDAO = specialityDAO;
    }

    public Calendar getSelected_date() {
        return selected_date;
    }

    public void setSelected_date(Calendar selected_date) {
        this.selected_date = selected_date;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LabTypeDAO getLabTypeDAO() {
        return labTypeDAO;
    }

    public void setLabTypeDAO(LabTypeDAO labTypeDAO) {
        this.labTypeDAO = labTypeDAO;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public int getAddressConsultId() {
        return AddressConsultId;
    }

    public void setAddressConsultId(int addressConsultId) {
        AddressConsultId = addressConsultId;
    }

    public String getHospitalcode() {
        return hospitalcode;
    }

    public void setHospitalcode(String hospitalcode) {
        this.hospitalcode = hospitalcode;
    }

    public int getTokennumber() {
        return tokennumber;
    }

    public void setTokennumber(int tokennumber) {
        this.tokennumber = tokennumber;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public int getHospitalID() {
        return hospitalID;
    }

    public void setHospitalID(int hospitalID) {
        this.hospitalID = hospitalID;
    }

    public int getParentHospitalID() {
        return parentHospitalID;
    }

    public void setParentHospitalID(int parentHospitalID) {
        this.parentHospitalID = parentHospitalID;
    }
}
