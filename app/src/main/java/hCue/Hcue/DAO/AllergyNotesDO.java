package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class AllergyNotesDO implements Serializable {

    @SerializedName("IsPresent")
    private String IsPresent ;

    @SerializedName("Allergy")
    private String Allergy ;

    @SerializedName("AllergyNotes")
    private String AllergyNotes ;

    public String getAllergy() {
        return Allergy;
    }

    public void setAllergy(String allergy) {
        Allergy = allergy;
    }

    public String getAllergyNotes() {
        return AllergyNotes;
    }

    public void setAllergyNotes(String allergyNotes) {
        AllergyNotes = allergyNotes;
    }

    public String getIsPresent() {
        return IsPresent;
    }

    public void setIsPresent(String isPresent) {
        IsPresent = isPresent;
    }
}
