package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class UpdatePatientRecordRequestDO implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("patientDetails")
    private PatientDetailsDO patientDetails = new PatientDetailsDO();

    @SerializedName("patientDocuments")
    private PatientDocumentsDO patientDocuments = new PatientDocumentsDO();

    @SerializedName("USRId")
    private long USRId ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public PatientDetailsDO getPatientDetails() {
        return patientDetails;
    }

    public void setPatientDetails(PatientDetailsDO patientDetails) {
        this.patientDetails = patientDetails;
    }

    public PatientDocumentsDO getPatientDocuments() {
        return patientDocuments;
    }

    public void setPatientDocuments(PatientDocumentsDO patientDocuments) {
        this.patientDocuments = patientDocuments;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public class PatientDetailsDO implements Serializable{
        @SerializedName("PatientID")
        private long PatientID ;

        @SerializedName("TermsAccepted")
        private String TermsAccepted = "Y";

        public long getPatientID() {
            return PatientID;
        }

        public void setPatientID(long patientID) {
            PatientID = patientID;
        }

        public String getTermsAccepted() {
            return TermsAccepted;
        }

        public void setTermsAccepted(String termsAccepted) {
            TermsAccepted = termsAccepted;
        }
    }

    public class PatientDocumentsDO implements Serializable{
        @SerializedName("DocumentNumber")
        private String DocumentNumber ;

        @SerializedName("FileID")
        private String FileID;

        @SerializedName("UpLoadedDate")
        private String UpLoadedDate;

        public String getDocumentNumber() {
            return DocumentNumber;
        }

        public void setDocumentNumber(String documentNumber) {
            DocumentNumber = documentNumber;
        }

        public String getFileID() {
            return FileID;
        }

        public void setFileID(String fileID) {
            FileID = fileID;
        }

        public String getUpLoadedDate() {
            return UpLoadedDate;
        }

        public void setUpLoadedDate(String upLoadedDate) {
            UpLoadedDate = upLoadedDate;
        }
    }

}
