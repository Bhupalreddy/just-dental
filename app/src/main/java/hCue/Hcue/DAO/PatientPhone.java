package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-06-2016.
 */
public class PatientPhone implements Serializable {

    @SerializedName("PhAreaCD")
    private int PhAreaCD ;

    @SerializedName("PhCntryCD")
    private int PhCntryCD = 91;

    @SerializedName("PhNumber")
    private long PhNumber ;

    @SerializedName("PhStateCD")
    private int PhStateCD ;

    @SerializedName("PhType")
    private char PhType = 'M';

    @SerializedName("PrimaryIND")
    private char PrimaryIND = 'Y';

    public int getPhAreaCD() {
        return PhAreaCD;
    }

    public void setPhAreaCD(int phAreaCD) {
        PhAreaCD = phAreaCD;
    }

    public int getPhCntryCD() {
        return PhCntryCD;
    }

    public void setPhCntryCD(int phCntryCD) {
        PhCntryCD = phCntryCD;
    }

    public long getPhNumber() {
        return PhNumber;
    }

    public void setPhNumber(long phNumber) {
        PhNumber = phNumber;
    }

    public int getPhStateCD() {
        return PhStateCD;
    }

    public void setPhStateCD(int phStateCD) {
        PhStateCD = phStateCD;
    }

    public char getPhType() {
        return PhType;
    }

    public void setPhType(char phType) {
        PhType = phType;
    }

    public char getPrimaryIND() {
        return PrimaryIND;
    }

    public void setPrimaryIND(char primaryIND) {
        PrimaryIND = primaryIND;
    }
}
