package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 17-06-2016.
 */
public class PatientInfo2 implements Serializable
{
    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("FullName")
    private String FullName;

    @SerializedName("Gender")
    private char Gender ;

    @SerializedName("DOB")
    private long DOB ;

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public char getGender() {
        return Gender;
    }

    public void setGender(char gender) {
        Gender = gender;
    }

    public long getDOB() {
        return DOB;
    }

    public void setDOB(long DOB) {
        this.DOB = DOB;
    }
}
