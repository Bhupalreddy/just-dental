package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-06-2016.
 */
public class NumberInfo implements Serializable
{
    @SerializedName("carrier")
    private String carrier ;

    @SerializedName("country_code")
    private int country_code ;

    @SerializedName("country_iso_code")
    private String country_iso_code ;

    @SerializedName("e164_format")
    private String e164_format ;

    @SerializedName("formatting")
    private String formatting ;

    @SerializedName("is_mobile")
    private boolean is_mobile ;

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public int getCountry_code() {
        return country_code;
    }

    public void setCountry_code(int country_code) {
        this.country_code = country_code;
    }

    public String getCountry_iso_code() {
        return country_iso_code;
    }

    public void setCountry_iso_code(String country_iso_code) {
        this.country_iso_code = country_iso_code;
    }

    public String getE164_format() {
        return e164_format;
    }

    public void setE164_format(String e164_format) {
        this.e164_format = e164_format;
    }

    public String getFormatting() {
        return formatting;
    }

    public void setFormatting(String formatting) {
        this.formatting = formatting;
    }

    public boolean is_mobile() {
        return is_mobile;
    }

    public void setIs_mobile(boolean is_mobile) {
        this.is_mobile = is_mobile;
    }
}
