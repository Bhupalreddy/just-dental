package hCue.Hcue.DAO;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 04/06/16.
 */
public class LabTypeDAO implements Serializable
{
    private String labtypename ;

    public String getlabtypename() {
        return labtypename;
    }

    public void setlabtypename(String labtypename) {
        this.labtypename = labtypename;
    }

}
