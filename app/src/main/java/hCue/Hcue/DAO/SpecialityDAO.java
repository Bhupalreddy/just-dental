package hCue.Hcue.DAO;

import java.io.Serializable;

/**
 * Created by Appdest on 03-06-2016.
 */
public class SpecialityDAO implements Serializable
{

    private String specialityname ;
    private int specialityicon ;
    private int specialityimg ;
    private String speciality_code="DNT" ;

    public int getSpecialityimg() {
        return specialityimg;
    }

    public void setSpecialityimg(int specialityimg) {
        this.specialityimg = specialityimg;
    }

    public String getSpecialityname() {
        return specialityname;
    }

    public void setSpecialityname(String specialityname) {
        this.specialityname = specialityname;
    }


    public int getSpecialityicon()
    {
        return specialityicon;
    }

    public void setSpecialityicon(int specialityicon)
    {
        this.specialityicon = specialityicon;
    }

    public String getSpeciality_code() {
        return speciality_code;
    }

    public void setSpeciality_code(String speciality_code) {
        this.speciality_code = speciality_code;
    }
}
