package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class HistoryRecord implements Serializable
{

    @SerializedName("Description")
    private String Description ;

    @SerializedName("HistoryID")
    private  int HistoryID;

    @SerializedName("Notes")
    private String Notes ;

    @SerializedName("Selection")
    private String Selection ;

    @SerializedName("SpecialityCD")
    private String SpecialityCD ;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getHistoryID() {
        return HistoryID;
    }

    public void setHistoryID(int historyID) {
        HistoryID = historyID;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getSelection() {
        return Selection;
    }

    public void setSelection(String selection) {
        Selection = selection;
    }

    public String getSpecialityCD() {
        return SpecialityCD;
    }

    public void setSpecialityCD(String specialityCD) {
        SpecialityCD = specialityCD;
    }
}
