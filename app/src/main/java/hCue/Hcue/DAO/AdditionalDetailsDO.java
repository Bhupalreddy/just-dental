package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class AdditionalDetailsDO implements Serializable {

    @SerializedName("Medication")
    private ArrayList<MedicineNotesDO> listMedications ;

    @SerializedName("Diagonsis")
    private ArrayList<DiagnosisNotesDO> listDiagonsis ;

    @SerializedName("Allergy")
    private ArrayList<AllergyNotesDO> listAllergy ;

    public ArrayList<MedicineNotesDO> getListMedications() {
        return listMedications;
    }

    public void setListMedications(ArrayList<MedicineNotesDO> listMedications) {
        this.listMedications = listMedications;
    }

    public ArrayList<DiagnosisNotesDO> getListDiagonsis() {
        return listDiagonsis;
    }

    public void setListDiagonsis(ArrayList<DiagnosisNotesDO> listDiagonsis) {
        this.listDiagonsis = listDiagonsis;
    }

    public ArrayList<AllergyNotesDO> getListAllergy() {
        return listAllergy;
    }

    public void setListAllergy(ArrayList<AllergyNotesDO> listAllergy) {
        this.listAllergy = listAllergy;
    }
}
