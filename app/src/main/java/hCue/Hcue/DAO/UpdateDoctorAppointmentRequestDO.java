package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class UpdateDoctorAppointmentRequestDO implements Serializable
{
    @SerializedName("AppointmentID")
    private long AppointmentID;
    @SerializedName("AddressConsultID")
    private int AddressConsultID ;
    @SerializedName("DayCD")
    private String DayCD ;
    @SerializedName("ConsultationDt")
    private String ConsultationDt ;
    @SerializedName("StartTime")
    private String StartTime ;
    @SerializedName("EndTime")
    private String EndTime ;
    @SerializedName("PatientID")
    private long PatientID ;
    @SerializedName("AppointmentStatus")
    private char AppointmentStatus ;
    @SerializedName("DoctorID")
    private int DoctorID ;
    @SerializedName("USRType")
    private String USRType ;
    @SerializedName("USRId")
    private long USRId ;
    @SerializedName("DoctorVisitRsnID")
    private String DoctorVisitRsnID = "OTHERS";
    @SerializedName("VisitUserTypeID")
    private String VisitUserTypeID = "OPATIENT";
    @SerializedName("Reason")
    private String Reason ;
    @SerializedName("HospitalDetails")
    private HospitalDetailsDO HospitalDetails ;

    @SerializedName("OtherVisitRsn")
    private String OtherVisitRsn;

    private String latitude;
    private String longitude;

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public int getAddressConsultID() {
        return AddressConsultID;
    }

    public void setAddressConsultID(int addressConsultID) {
        AddressConsultID = addressConsultID;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public String getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(String consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public char getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(char appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public String getDoctorVisitRsnID() {
        return DoctorVisitRsnID;
    }

    public void setDoctorVisitRsnID(String doctorVisitRsnID) {
        DoctorVisitRsnID = doctorVisitRsnID;
    }

    public String getVisitUserTypeID() {
        return VisitUserTypeID;
    }

    public void setVisitUserTypeID(String visitUserTypeID) {
        VisitUserTypeID = visitUserTypeID;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public HospitalDetailsDO getHospitalDetails() {
        return HospitalDetails;
    }

    public void setHospitalDetails(HospitalDetailsDO hospitalDetails) {
        HospitalDetails = hospitalDetails;
    }

    public String getOtherVisitRsn() {
        return OtherVisitRsn;
    }

    public void setOtherVisitRsn(String otherVisitRsn) {
        OtherVisitRsn = otherVisitRsn;
    }
}
