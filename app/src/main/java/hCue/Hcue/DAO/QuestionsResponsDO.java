package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class QuestionsResponsDO implements Serializable {

    @SerializedName("QuestionAnswersList")
    private ArrayList<ListQuestionsResponseDO> QuestionAnswersList ;

    @SerializedName("QuestionCount")
    private int QuestionCount ;

    public ArrayList<ListQuestionsResponseDO> getQuestionAnswersList() {
        return QuestionAnswersList;
    }

    public void setQuestionAnswersList(ArrayList<ListQuestionsResponseDO> questionAnswersList) {
        QuestionAnswersList = questionAnswersList;
    }

    public int getQuestionCount() {
        return QuestionCount;
    }

    public void setQuestionCount(int questionCount) {
        QuestionCount = questionCount;
    }
}
