package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class UpdateDoctorAppointmentResponseDO implements Serializable
{
    @SerializedName("SeqNo")
    private int SeqNo ;
    @SerializedName("AppointmentID")
    private long AppointmentID ;
    @SerializedName("AddressConsultID")
    private int AddressConsultID ;
    @SerializedName("DayCD")
    private String DayCD ;
    @SerializedName("ConsultationDt")
    private long ConsultationDt ;
    @SerializedName("StartTime")
    private String StartTime ;
    @SerializedName("EndTime")
    private String EndTime ;
    @SerializedName("PatientID")
    private long PatientID ;
    @SerializedName("VisitUserTypeID")
    private String VisitUserTypeID ;
    @SerializedName("DoctorID")
    private int DoctorID ;
    @SerializedName("AppointmentStatus")
    private String AppointmentStatus ;
    @SerializedName("DoctorVisitRsnID")
    private String DoctorVisitRsnID ;
    @SerializedName("FirstTimeVisit")
    private String FirstTimeVisit ;
    @SerializedName("TokenNumber")
    private String TokenNumber ;

    public int getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(int seqNo) {
        SeqNo = seqNo;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public int getAddressConsultID() {
        return AddressConsultID;
    }

    public void setAddressConsultID(int addressConsultID) {
        AddressConsultID = addressConsultID;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public long getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getVisitUserTypeID() {
        return VisitUserTypeID;
    }

    public void setVisitUserTypeID(String visitUserTypeID) {
        VisitUserTypeID = visitUserTypeID;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public String getDoctorVisitRsnID() {
        return DoctorVisitRsnID;
    }

    public void setDoctorVisitRsnID(String doctorVisitRsnID) {
        DoctorVisitRsnID = doctorVisitRsnID;
    }

    public String getFirstTimeVisit() {
        return FirstTimeVisit;
    }

    public void setFirstTimeVisit(String firstTimeVisit) {
        FirstTimeVisit = firstTimeVisit;
    }

    public String getTokenNumber() {
        return TokenNumber;
    }

    public void setTokenNumber(String tokenNumber) {
        TokenNumber = tokenNumber;
    }

}
