package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientCaseBillingInfo implements Serializable
{

    @SerializedName("AdvancePaid")
    private double AdvancePaid;

    @SerializedName("Balance")
    private double Balance ;

    @SerializedName("BillDate")
    private long BillDate ;

    @SerializedName("BilledCost")
    private double BilledCost ;

    @SerializedName("BillID")
    private long BillID ;

    @SerializedName("DiscountCost")
    private double DiscountCost ;

    @SerializedName("Multiplyfactor")
    private int Multiplyfactor ;

    @SerializedName("ParentProcedureID")
    private int ParentProcedureID ;

    @SerializedName("PatientCaseID")
    private long PatientCaseID ;

    @SerializedName("ProcedureCost")
    private double ProcedureCost ;

    @SerializedName("ProcedureID")
    private int ProcedureID;

    public double getAdvancePaid() {
        return AdvancePaid;
    }

    public void setAdvancePaid(double advancePaid) {
        AdvancePaid = advancePaid;
    }

    public double getBalance() {
        return Balance;
    }

    public void setBalance(double balance) {
        Balance = balance;
    }

    public long getBillDate() {
        return BillDate;
    }

    public void setBillDate(long billDate) {
        BillDate = billDate;
    }

    public double getBilledCost() {
        return BilledCost;
    }

    public void setBilledCost(double billedCost) {
        BilledCost = billedCost;
    }

    public long getBillID() {
        return BillID;
    }

    public void setBillID(long billID) {
        BillID = billID;
    }

    public double getDiscountCost() {
        return DiscountCost;
    }

    public void setDiscountCost(double discountCost) {
        DiscountCost = discountCost;
    }

    public int getMultiplyfactor() {
        return Multiplyfactor;
    }

    public void setMultiplyfactor(int multiplyfactor) {
        Multiplyfactor = multiplyfactor;
    }

    public int getParentProcedureID() {
        return ParentProcedureID;
    }

    public void setParentProcedureID(int parentProcedureID) {
        ParentProcedureID = parentProcedureID;
    }

    public long getPatientCaseID() {
        return PatientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        PatientCaseID = patientCaseID;
    }

    public double getProcedureCost() {
        return ProcedureCost;
    }

    public void setProcedureCost(double procedureCost) {
        ProcedureCost = procedureCost;
    }

    public int getProcedureID() {
        return ProcedureID;
    }

    public void setProcedureID(int procedureID) {
        ProcedureID = procedureID;
    }
}
