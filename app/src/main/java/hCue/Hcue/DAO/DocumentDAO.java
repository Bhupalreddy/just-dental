package hCue.Hcue.DAO;

import java.io.Serializable;

/**
 * Created by Appdest on 02-08-2016.
 */
public class DocumentDAO implements Serializable
{
    private String imagename;

    private String imageURL ;

    private long ConsultationDt ;

    private long patientCaseID ;

    private String Doctorname ;

    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public long getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        ConsultationDt = consultationDt;
    }

    public long getPatientCaseID() {
        return patientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        this.patientCaseID = patientCaseID;
    }

    public String getDoctorname() {
        return Doctorname;
    }

    public void setDoctorname(String doctorname) {
        Doctorname = doctorname;
    }
}
