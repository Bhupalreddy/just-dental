package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 8/19/2016.
 */
public class PharmaDetailsDo implements Serializable
{
    @SerializedName("hasAccountId")
    private Boolean hasAccountId ;

    @SerializedName("hasInstanceId")
    private Boolean hasInstanceId ;

    @SerializedName("instance")
    private Instance instance ;

    @SerializedName("currentUrl")
    private String currentUrl ;

    @SerializedName("userPermission")
    private String  userPermission;


    @SerializedName("userId")
    private String userId ;

    @SerializedName("password")
    private String password ;

    @SerializedName("userType")
    private int userType ;

    @SerializedName("status")
    private String status ;

    @SerializedName("name")
    private String name ;

    @SerializedName("mobile")
    private String mobile ;

    @SerializedName("address")
    private String  address;

    @SerializedName("area")
    private String area ;

    @SerializedName("city")
    private String  city;

    @SerializedName("state")
    private String  state;

    @SerializedName("pincode")
    private String  pincode;

    @SerializedName("passwordResetKey")
    private String  passwordResetKey;

    @SerializedName("resetRequestTime")
    private String  resetRequestTime;

    @SerializedName("id")
    private String id ;

    @SerializedName("accountId")
    private String accountId ;

    @SerializedName("instanceId")
    private String instanceId ;

    @SerializedName("clientId")
    private String clientId ;

    @SerializedName("createdAt")
    private String createdAt ;

    @SerializedName("updatedAt")
    private String updatedAt ;

    @SerializedName("createdBy")
    private String createdBy ;

    @SerializedName("updatedBy")
    private String updatedBy ;




    public Boolean getHasAccountId() {
        return hasAccountId;
    }

    public void setHasAccountId(Boolean hasAccountId) {
        this.hasAccountId = hasAccountId;
    }

    public Boolean getHasInstanceId() {
        return hasInstanceId;
    }

    public void setHasInstanceId(Boolean hasInstanceId) {
        this.hasInstanceId = hasInstanceId;
    }

    public Instance getInstance() {
        return instance;
    }

    public void setInstance(Instance instance) {
        this.instance = instance;
    }

    public String getCurrentUrl() {
        return currentUrl;
    }

    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String userPermission) {
        this.userPermission = userPermission;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPasswordResetKey() {
        return passwordResetKey;
    }

    public void setPasswordResetKey(String passwordResetKey) {
        this.passwordResetKey = passwordResetKey;
    }

    public String getResetRequestTime() {
        return resetRequestTime;
    }

    public void setResetRequestTime(String resetRequestTime) {
        this.resetRequestTime = resetRequestTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}
