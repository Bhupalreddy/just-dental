package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 11/17/2016.
 */

public class OrderTextDocuments implements Serializable
{
    @SerializedName("MedicineName")
    private String MedicineName ;

    @SerializedName("MedicineStrength")
    private String MedicineStrength ;

    @SerializedName("MedicineGenericName")
    private String MedicineGenericName ;

    @SerializedName("Manufacturer")
    private String Manufacturer ;

    @SerializedName("RequiredQuantity")
    private int RequiredQuantity ;

    public String getMedicineName() {
        return MedicineName;
    }

    public void setMedicineName(String medicineName) {
        MedicineName = medicineName;
    }

    public String getMedicineStrength() {
        return MedicineStrength;
    }

    public void setMedicineStrength(String medicineStrength) {
        MedicineStrength = medicineStrength;
    }

    public String getMedicineGenericName() {
        return MedicineGenericName;
    }

    public void setMedicineGenericName(String medicineGenericName) {
        MedicineGenericName = medicineGenericName;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        Manufacturer = manufacturer;
    }

    public int getRequiredQuantity() {
        return RequiredQuantity;
    }

    public void setRequiredQuantity(int requiredQuantity) {
        RequiredQuantity = requiredQuantity;
    }
}
