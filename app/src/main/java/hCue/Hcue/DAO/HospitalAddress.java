package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 10/13/2016.
 */
public class HospitalAddress implements Serializable
{
    @SerializedName("Address1")
    private  String Address1;

    @SerializedName("Address2")
    private  String Address2;

    @SerializedName("Street")
    private  String Street;

    @SerializedName("Location")
    private  String Location;

    @SerializedName("CityTown")
    private  String CityTown;

    @SerializedName("DistrictRegion")
    private  String DistrictRegion;

    @SerializedName("State")
    private  String State;

    @SerializedName("PinCode")
    private  long PinCode;

    @SerializedName("Country")
    private  String Country;

    @SerializedName("LandMark")
    private  String LandMark;

    @SerializedName("Latitude")
    private  String Latitude;

    @SerializedName("Longitude")
    private  String Longitude;

    @SerializedName("HospitalDocuments")
    public ArrayList<HospitalDocuments> hospitalDocuments;

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getCityTown() {
        return CityTown;
    }

    public void setCityTown(String cityTown) {
        CityTown = cityTown;
    }

    public String getDistrictRegion() {
        return DistrictRegion;
    }

    public void setDistrictRegion(String districtRegion) {
        DistrictRegion = districtRegion;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public long getPinCode() {
        return PinCode;
    }

    public void setPinCode(long pinCode) {
        PinCode = pinCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getLandMark() {
        return LandMark;
    }

    public void setLandMark(String landMark) {
        LandMark = landMark;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public ArrayList<HospitalDocuments> getHospitalDocuments() {
        return hospitalDocuments;
    }

    public void setHospitalDocuments(ArrayList<HospitalDocuments> hospitalDocuments) {
        this.hospitalDocuments = hospitalDocuments;
    }
}
