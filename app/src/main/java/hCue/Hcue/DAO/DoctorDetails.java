package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Appdest on 11-06-2016.
 */
public class DoctorDetails implements Serializable
{
    @SerializedName("doctorFullName")
    private String doctorFullName ;

    @SerializedName("gender")
    private char gender ;

    @SerializedName("ProfileImage")
    private String ProfileImage ;

    @SerializedName("doctorQualification")
    private LinkedHashMap<Integer,String> doctorQualification ;

    @SerializedName("doctorSpecialization")
    private LinkedHashMap<Integer,String> doctorSpecialization ;

    @SerializedName("doctorPhone")
    private ArrayList<PatientPhone> arraydoctorPhone ;

    @SerializedName("doctorEmail")
    private ArrayList<PatientEmail> arraydoctorEmail ;

    @SerializedName("doctorAddress")
    private ArrayList<DoctorAddress> arraydoctorAddress ;

    public String getDoctorFullName() {
        return doctorFullName;
    }

    public void setDoctorFullName(String doctorFullName) {
        this.doctorFullName = doctorFullName;
    }

    public LinkedHashMap<Integer, String> getDoctorQualification() {
        return doctorQualification;
    }

    public void setDoctorQualification(LinkedHashMap<Integer, String> doctorQualification) {
        this.doctorQualification = doctorQualification;
    }

    public LinkedHashMap<Integer, String> getDoctorSpecialization() {
        return doctorSpecialization;
    }

    public void setDoctorSpecialization(LinkedHashMap<Integer, String> doctorSpecialization) {
        this.doctorSpecialization = doctorSpecialization;
    }

    public ArrayList<PatientPhone> getArraydoctorPhone() {
        return arraydoctorPhone;
    }

    public void setArraydoctorPhone(ArrayList<PatientPhone> arraydoctorPhone) {
        this.arraydoctorPhone = arraydoctorPhone;
    }

    public ArrayList<PatientEmail> getArraydoctorEmail() {
        return arraydoctorEmail;
    }

    public void setArraydoctorEmail(ArrayList<PatientEmail> arraydoctorEmail) {
        this.arraydoctorEmail = arraydoctorEmail;
    }

    public ArrayList<DoctorAddress> getArraydoctorAddress() {
        return arraydoctorAddress;
    }

    public void setArraydoctorAddress(ArrayList<DoctorAddress> arraydoctorAddress) {
        this.arraydoctorAddress = arraydoctorAddress;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }
}
