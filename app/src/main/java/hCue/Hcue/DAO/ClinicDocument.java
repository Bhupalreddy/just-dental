package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class ClinicDocument implements Serializable
{
    @SerializedName("FileURL")
    private String FileURL ;

    @SerializedName("FileName")
    private String FileName ;

    @SerializedName("FileExtn")
    private String FileExtn ;

    @SerializedName("DocumentNumber")
    private String DocumentNumber ;

    public String getFileURL() {
        return FileURL;
    }

    public void setFileURL(String fileURL) {
        FileURL = fileURL;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileExtn() {
        return FileExtn;
    }

    public void setFileExtn(String fileExtn) {
        FileExtn = fileExtn;
    }

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }
}
