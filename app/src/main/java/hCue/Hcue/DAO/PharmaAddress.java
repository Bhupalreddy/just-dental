package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 28/11/16.
 */

public class PharmaAddress implements Serializable
{
    @SerializedName("Address2")
    private String Address2;

    @SerializedName("Address1")
    private String Address1;

    @SerializedName("Latitude")
    private String Latitude;

    @SerializedName("Longitude")
    private String Longitude;

    @SerializedName("PinCode")
    private int PinCode;

    @SerializedName("State")
    private String State;

    @SerializedName("Street")
    private String Street;

    @SerializedName("Country")
    private String Country;

    @SerializedName("CityTown")
    private String CityTown;

    @SerializedName("PrimaryIND")
    private String PrimaryIND;

    @SerializedName("Location")
    private int Location;

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public int getPinCode() {
        return PinCode;
    }

    public void setPinCode(int pinCode) {
        PinCode = pinCode;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCityTown() {
        return CityTown;
    }

    public void setCityTown(String cityTown) {
        CityTown = cityTown;
    }

    public String getPrimaryIND() {
        return PrimaryIND;
    }

    public void setPrimaryIND(String primaryIND) {
        PrimaryIND = primaryIND;
    }

    public int getLocation() {
        return Location;
    }

    public void setLocation(int location) {
        Location = location;
    }
}
