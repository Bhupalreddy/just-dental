package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 29-07-2016.
 */
public class DoctorSpecialisation implements Serializable
{
    @SerializedName("SepcialityCode")
    private String SepcialityCode ;

    @SerializedName("SepcialityDesc")
    private String SepcialityDesc ;

    @SerializedName("SerialNumber")
    private String SerialNumber ;

    @SerializedName("SpecialityOESDesc")
    private String SpecialityOESDesc ;

    public String getSepcialityCode() {
        return SepcialityCode;
    }

    public void setSepcialityCode(String sepcialityCode) {
        SepcialityCode = sepcialityCode;
    }

    public String getSepcialityDesc() {
        return SepcialityDesc;
    }

    public void setSepcialityDesc(String sepcialityDesc) {
        SepcialityDesc = sepcialityDesc;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getSpecialityOESDesc() {
        return SpecialityOESDesc;
    }

    public void setSpecialityOESDesc(String specialityOESDesc) {
        SpecialityOESDesc = specialityOESDesc;
    }
}
