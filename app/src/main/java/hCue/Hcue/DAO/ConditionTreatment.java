package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class ConditionTreatment implements Serializable
{
    @SerializedName("TreatmentID")
    private int TreatmentID ;

    @SerializedName("TreatmentIDDesc")
    private String TreatmentIDDesc ;

    @SerializedName("TreatmentNotes")
    private String TreatmentNotes ;

    @SerializedName("TreatMentStatusCD")
    private int TreatMentStatusCD ;

    @SerializedName("TreatMentStatusDesc")
    private String TreatMentStatusDesc;

    public int getTreatmentID() {
        return TreatmentID;
    }

    public void setTreatmentID(int treatmentID) {
        TreatmentID = treatmentID;
    }

    public String getTreatmentIDDesc() {
        return TreatmentIDDesc;
    }

    public void setTreatmentIDDesc(String treatmentIDDesc) {
        TreatmentIDDesc = treatmentIDDesc;
    }

    public String getTreatmentNotes() {
        return TreatmentNotes;
    }

    public void setTreatmentNotes(String treatmentNotes) {
        TreatmentNotes = treatmentNotes;
    }

    public int getTreatMentStatusCD() {
        return TreatMentStatusCD;
    }

    public void setTreatMentStatusCD(int treatMentStatusCD) {
        TreatMentStatusCD = treatMentStatusCD;
    }

    public String getTreatMentStatusDesc() {
        return TreatMentStatusDesc;
    }

    public void setTreatMentStatusDesc(String treatMentStatusDesc) {
        TreatMentStatusDesc = treatMentStatusDesc;
    }
}
