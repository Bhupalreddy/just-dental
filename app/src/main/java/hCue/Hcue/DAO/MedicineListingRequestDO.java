package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class MedicineListingRequestDO implements Serializable
{
    @SerializedName("DoctorID")
    private int DoctorID = 0;

    @SerializedName("PageSize")
    private int PageSize = 30;

    @SerializedName("StartsWith")
    private String StartsWith ;

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public String getStartsWith() {
        return StartsWith;
    }

    public void setStartsWith(String startsWith) {
        StartsWith = startsWith;
    }
}
