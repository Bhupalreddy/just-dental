package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class CaseHistory implements Serializable
{
    @SerializedName("patientCase")
    private PatientCase patientCase ;

    @SerializedName("DentalCasePrescription")
    private ArrayList<DentalCasePrescription> listDentalcaseDescrption ;

    @SerializedName("patientCasePrescribObj")
    private ArrayList<PatientCasePrescribObj> listPatientCasePrescribObj;

    @SerializedName("patientLabTest")
    private ArrayList<PatientLabTest> listpatientLabTest ;

    @SerializedName("patientCaseDocuments")
    private PatientCaseDocuments patientCaseDocuments;

    @SerializedName("patientHospital")
    private ArrayList<PatientHospital> listpatientHospital;

    @SerializedName("patientBillings")
    private PatientBillings listpatientBillings;

    @SerializedName("CaseTreatment")
    private ArrayList<CaseTreatmentDO> listCaseTreatment;

    public PatientCase getPatientCase() {
        return patientCase;
    }

    public void setPatientCase(PatientCase patientCase) {
        this.patientCase = patientCase;
    }

    public ArrayList<DentalCasePrescription> getListDentalcaseDescrption() {
        return listDentalcaseDescrption;
    }

    public void setListDentalcaseDescrption(ArrayList<DentalCasePrescription> listDentalcaseDescrption) {
        this.listDentalcaseDescrption = listDentalcaseDescrption;
    }

    public ArrayList<PatientCasePrescribObj> getListPatientCasePrescribObj() {
        return listPatientCasePrescribObj;
    }

    public void setListPatientCasePrescribObj(ArrayList<PatientCasePrescribObj> listPatientCasePrescribObj) {
        this.listPatientCasePrescribObj = listPatientCasePrescribObj;
    }

    public ArrayList<PatientLabTest> getListpatientLabTest() {
        return listpatientLabTest;
    }

    public void setListpatientLabTest(ArrayList<PatientLabTest> listpatientLabTest) {
        this.listpatientLabTest = listpatientLabTest;
    }

    public PatientCaseDocuments getPatientCaseDocuments() {
        return patientCaseDocuments;
    }

    public void setPatientCaseDocuments(PatientCaseDocuments patientCaseDocuments) {
        this.patientCaseDocuments = patientCaseDocuments;
    }

    public ArrayList<PatientHospital> getListpatientHospital() {
        return listpatientHospital;
    }

    public void setListpatientHospital(ArrayList<PatientHospital> listpatientHospital) {
        this.listpatientHospital = listpatientHospital;
    }

    public PatientBillings getListpatientBillings() {
        return listpatientBillings;
    }

    public void setListpatientBillings(PatientBillings listpatientBillings) {
        this.listpatientBillings = listpatientBillings;
    }

    public ArrayList<CaseTreatmentDO> getListCaseTreatment() {
        return listCaseTreatment;
    }

    public void setListCaseTreatment(ArrayList<CaseTreatmentDO> listCaseTreatment) {
        this.listCaseTreatment = listCaseTreatment;
    }
}
