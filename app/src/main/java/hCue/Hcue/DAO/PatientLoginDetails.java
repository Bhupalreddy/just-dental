package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 06-08-2016.
 */
public class PatientLoginDetails implements Serializable
{
    @SerializedName("SocialID")
    private String SocialID ;

    public String getSocialID() {
        return SocialID;
    }

    public void setSocialID(String socialID) {
        SocialID = socialID;
    }
}
