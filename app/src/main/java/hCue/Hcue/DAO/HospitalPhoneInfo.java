package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 10/13/2016.
 */
public class HospitalPhoneInfo implements Serializable
{
    @SerializedName("HospitalID")
    private int HospitalID;

    @SerializedName("PhCntryCD")
    private int PhCntryCD;

    @SerializedName("PhStateCD")
    private int PhStateCD;

    @SerializedName("PhAreaCD")
    private int PhAreaCD;

    @SerializedName("PhNumber")
    private Number PhNumber;

    @SerializedName("PhType")
    private String PhType;

    @SerializedName("RowID")
    private int RowID;

    @SerializedName("PrimaryIND")
    private String PrimaryIND;

    @SerializedName("PublicDisplay")
    private String PublicDisplay;

    public int getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(int hospitalID) {
        HospitalID = hospitalID;
    }

    public int getPhCntryCD() {
        return PhCntryCD;
    }

    public void setPhCntryCD(int phCntryCD) {
        PhCntryCD = phCntryCD;
    }

    public int getPhStateCD() {
        return PhStateCD;
    }

    public void setPhStateCD(int phStateCD) {
        PhStateCD = phStateCD;
    }

    public int getPhAreaCD() {
        return PhAreaCD;
    }

    public void setPhAreaCD(int phAreaCD) {
        PhAreaCD = phAreaCD;
    }

    public Number getPhNumber() {
        return PhNumber;
    }

    public void setPhNumber(Number phNumber) {
        PhNumber = phNumber;
    }

    public String getPhType() {
        return PhType;
    }

    public void setPhType(String phType) {
        PhType = phType;
    }

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }

    public String getPrimaryIND() {
        return PrimaryIND;
    }

    public void setPrimaryIND(String primaryIND) {
        PrimaryIND = primaryIND;
    }

    public String getPublicDisplay() {
        return PublicDisplay;
    }

    public void setPublicDisplay(String publicDisplay) {
        PublicDisplay = publicDisplay;
    }
}
