package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 23-06-2016.
 */
public class AppointmentRows implements Serializable
{
    @SerializedName("AddressConsultID")
    private int AddressConsultID ;

    @SerializedName("AddressID")
    private int AddressID ;

    @SerializedName("ClinicName")
    private String ClinicName ;

    @SerializedName("ConsultationDate")
    private long ConsultationDate ;

    @SerializedName("DayCD")
    private String DayCD ;

    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("EndTime")
    private String EndTime ;

    @SerializedName("Fees")
    private int Fees ;

    @SerializedName("MinPerCase")
    private int MinPerCase ;

    @SerializedName("StartTime")
    private String StartTime ;

    @SerializedName("HospitalCD")
    private String HospitalCode ;

    @SerializedName("BranchCD")
    private String BranchCode ;

    @SerializedName("HospitalInfo")
    private HospitalInfo hospitalInfo;

    @SerializedName("SlotList")
    private ArrayList<SlotList> listslotLists ;

    public int getAddressConsultID() {
        return AddressConsultID;
    }

    public void setAddressConsultID(int addressConsultID) {
        AddressConsultID = addressConsultID;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int addressID) {
        AddressID = addressID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public void setClinicName(String clinicName) {
        ClinicName = clinicName;
    }

    public long getConsultationDate() {
        return ConsultationDate;
    }

    public void setConsultationDate(long consultationDate) {
        ConsultationDate = consultationDate;
    }

    public String getDayCD() {
        return DayCD;
    }

    public void setDayCD(String dayCD) {
        DayCD = dayCD;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public int getFees() {
        return Fees;
    }

    public void setFees(int fees) {
        Fees = fees;
    }

    public int getMinPerCase() {
        return MinPerCase;
    }

    public void setMinPerCase(int minPerCase) {
        MinPerCase = minPerCase;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getHospitalCode() {
        return HospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        HospitalCode = hospitalCode;
    }

    public ArrayList<SlotList> getListslotLists() {
        return listslotLists;
    }

    public void setListslotLists(ArrayList<SlotList> listslotLists) {
        this.listslotLists = listslotLists;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public HospitalInfo getHospitalInfo() {
        return hospitalInfo;
    }

    public void setHospitalInfo(HospitalInfo hospitalInfo) {
        this.hospitalInfo = hospitalInfo;
    }
}
