package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class GlobalSearchDoctorsDO implements Serializable
{
    @SerializedName("doctorID")
    private int doctorID;
    @SerializedName("doctorName")
    private String doctorName ;
    @SerializedName("doctorLocation")
    private String doctorLocation ;
    @SerializedName("ProfileImageURL")
    private String ProfileImageURL ;
    @SerializedName("doctorSpeciality")
    private ArrayList<String> doctorSpeciality ;

    public int getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(int doctorID) {
        this.doctorID = doctorID;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorLocation() {
        return doctorLocation;
    }

    public void setDoctorLocation(String doctorLocation) {
        this.doctorLocation = doctorLocation;
    }

    public String getProfileImageURL() {
        return ProfileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        ProfileImageURL = profileImageURL;
    }

    public ArrayList<String> getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(ArrayList<String> doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
    }
}
