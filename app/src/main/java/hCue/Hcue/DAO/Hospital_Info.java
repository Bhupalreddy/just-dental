package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 10/13/2016.
 */
public class Hospital_Info implements Serializable
{
    @SerializedName("HospitalDetails")
    private HospitalDetails hospitalDetails;

    @SerializedName("HospitalAddress")
    private  HospitalAddress hospitalAddress;

    @SerializedName("HospitalPhone")
    private ArrayList<HospitalPhoneInfo> hospitalPhoneInfo;

    public HospitalDetails getHospitalDetails() {
        return hospitalDetails;
    }

    public void setHospitalDetails(HospitalDetails hospitalDetails) {
        this.hospitalDetails = hospitalDetails;
    }

    public HospitalAddress getHospitalAddress() {
        return hospitalAddress;
    }

    public void setHospitalAddress(HospitalAddress hospitalAddress) {
        this.hospitalAddress = hospitalAddress;
    }

    public ArrayList<HospitalPhoneInfo> getHospitalPhoneInfo() {
        return hospitalPhoneInfo;
    }

    public void setHospitalPhoneInfo(ArrayList<HospitalPhoneInfo> hospitalPhoneInfo) {
        this.hospitalPhoneInfo = hospitalPhoneInfo;
    }
}
