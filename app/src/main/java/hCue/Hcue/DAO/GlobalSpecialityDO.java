package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class GlobalSpecialityDO implements Serializable
{
    @SerializedName("specialityName")
    private String specialityName;
    @SerializedName("count")
    private int count ;
    @SerializedName("specialityID")
    private String specialityID ;

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSpecialityID() {
        return specialityID;
    }

    public void setSpecialityID(String specialityID) {
        this.specialityID = specialityID;
    }
}
