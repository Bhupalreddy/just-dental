package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class PatientLikeDO implements Serializable {

    private HashMap<String , String> likeCount;

    public HashMap<String, String> getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(HashMap<String, String> likeCount) {
        this.likeCount = likeCount;
    }
}
