package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 3/17/2016.
 */
public class PatientInfo implements Serializable {

    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("Title")
    private String Title;

    @SerializedName("FullName")
    private String FullName;

    @SerializedName("AppointmentID")
    private long AppointmentID;

    @SerializedName("AppointmentStatus")
    private String AppointmentStatus;

    @SerializedName("Age")
    private double Age;

    @SerializedName("Sex")
    private char Sex;

    @SerializedName("CurrentAge")
    private CurrentAge currentAge;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public CurrentAge getCurrentAge() {
        return currentAge;
    }

    public void setCurrentAge(CurrentAge currentAge) {
        this.currentAge = currentAge;
    }

    public long getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        MobileNumber = mobileNumber;
    }

    @SerializedName("MobileNumber")
    private long MobileNumber ;

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public String getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public double getAge() {
        return Age;
    }

    public void setAge(double age) {
        Age = age;
    }

    public char getSex() {
        return Sex;
    }

    public void setSex(char sex) {
        Sex = sex;
    }
}
