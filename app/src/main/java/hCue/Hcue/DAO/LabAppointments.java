package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 14-06-2016.
 */
public class LabAppointments implements Serializable
{

    @SerializedName("appointmentDetails")
    private LabappointmentDetails appointmentDetails ;

    @SerializedName("partnerDetails")
    private LabpartnerDetails partnerDetails ;

    @SerializedName("patientDetails")
    private LabPatientDetails labPatientDetails ;

    public LabappointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(LabappointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public LabpartnerDetails getPartnerDetails() {
        return partnerDetails;
    }

    public void setPartnerDetails(LabpartnerDetails partnerDetails) {
        this.partnerDetails = partnerDetails;
    }

    public LabPatientDetails getLabPatientDetails() {
        return labPatientDetails;
    }

    public void setLabPatientDetails(LabPatientDetails labPatientDetails) {
        this.labPatientDetails = labPatientDetails;
    }
}
