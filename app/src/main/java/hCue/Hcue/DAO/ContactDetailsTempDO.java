package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 10/14/2016.
 */

public class ContactDetailsTempDO implements Serializable
{

    private String PatientName ;

    private String PatientAddressID ;

    private String age ;

    private char gender ;

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPatientAddressID() {
        return PatientAddressID;
    }

    public void setPatientAddressID(String patientAddressID) {
        PatientAddressID = patientAddressID;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }
}
