package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 3/18/2016.
 */
public class PatientAddress implements Serializable
{

    @SerializedName("Address1")
    private String Address1 ;

    @SerializedName("Address2")
    private String Address2 ;

    @SerializedName("Street")
    private String Street ;

/*    @SerializedName("Location")
    private String Location ;*/

    @SerializedName("CityTown")
    private String CityTown ;

    @SerializedName("DistrictRegion")
    private String DistrictRegion ;

    @SerializedName("State")
    private String State = "AP";

    @SerializedName("PinCode")
    private int PinCode ;

    @SerializedName("Country")
    private String Country = "IN";

    @SerializedName("PrimaryIND")
    private String PrimaryIND ;

    @SerializedName("Latitude")
    private String Latitude ;

    @SerializedName("Longitude")
    private String Longitude ;

    @SerializedName("LandMark")
    private String LandMark ;

    @SerializedName("AddressType")
    private String AddressType = "H" ;

    public String getAddressType() {
        return AddressType;
    }

    public void setAddressType(String addressType) {
        AddressType = addressType;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

/*    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }*/

    public String getCityTown() {
        return CityTown;
    }

    public void setCityTown(String cityTown) {
        CityTown = cityTown;
    }

    public String getDistrictRegion() {
        return DistrictRegion;
    }

    public void setDistrictRegion(String districtRegion) {
        DistrictRegion = districtRegion;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public int getPinCode() {
        return PinCode;
    }

    public void setPinCode(int pinCode) {
        PinCode = pinCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPrimaryIND() {
        return PrimaryIND;
    }

    public void setPrimaryIND(String primaryIND) {
        PrimaryIND = primaryIND;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLandMark() {
        return LandMark;
    }

    public void setLandMark(String landMark) {
        LandMark = landMark;
    }
}
