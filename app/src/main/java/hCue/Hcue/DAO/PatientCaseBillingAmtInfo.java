package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientCaseBillingAmtInfo implements Serializable
{

    @SerializedName("BilledCost")
    private double BilledCost ;

    @SerializedName("DiscountCost")
    private double DiscountCost ;

    @SerializedName("ProcedureCost")
    private double ProcedureCost ;

    @SerializedName("TaxCost")
    private double TaxCost ;

    @SerializedName("totalBalanceCost")
    private double totalBalanceCost;

    public double getBilledCost() {
        return BilledCost;
    }

    public void setBilledCost(double billedCost) {
        BilledCost = billedCost;
    }

    public double getDiscountCost() {
        return DiscountCost;
    }

    public void setDiscountCost(double discountCost) {
        DiscountCost = discountCost;
    }

    public double getProcedureCost() {
        return ProcedureCost;
    }

    public void setProcedureCost(double procedureCost) {
        ProcedureCost = procedureCost;
    }

    public double getTaxCost() {
        return TaxCost;
    }

    public void setTaxCost(double taxCost) {
        TaxCost = taxCost;
    }

    public double getTotalBalanceCost() {
        return totalBalanceCost;
    }

    public void setTotalBalanceCost(double totalBalanceCost) {
        this.totalBalanceCost = totalBalanceCost;
    }
}
