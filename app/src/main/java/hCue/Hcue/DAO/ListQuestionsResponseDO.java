package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class ListQuestionsResponseDO implements Serializable {

    @SerializedName("AnswerCount")
    private int AnswerCount ;

    @SerializedName("IsPrivate")
    private String IsPrivate ;

    @SerializedName("MessageID")
    private int MessageID ;

    @SerializedName("ParentMessageID")
    private int ParentMessageID ;

    @SerializedName("Question")
    private String Question ;

    @SerializedName("QuestionDate")
    private long QuestionDate ;

    @SerializedName("Answers")
    private ArrayList<AnswerDO> answers ;

    @SerializedName("QuentionaireDetails")
    private QuestionaireDetailsDO QuentionaireDetails ;

    @SerializedName("QuestionSpeciality")
    private QuestionaireSpecialityDO questionSpeciality ;

    public int getAnswerCount() {
        return AnswerCount;
    }

    public void setAnswerCount(int answerCount) {
        AnswerCount = answerCount;
    }

    public String getIsPrivate() {
        return IsPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        IsPrivate = isPrivate;
    }

    public int getMessageID() {
        return MessageID;
    }

    public void setMessageID(int messageID) {
        MessageID = messageID;
    }

    public int getParentMessageID() {
        return ParentMessageID;
    }

    public void setParentMessageID(int parentMessageID) {
        ParentMessageID = parentMessageID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public long getQuestionDate() {
        return QuestionDate;
    }

    public void setQuestionDate(long questionDate) {
        QuestionDate = questionDate;
    }

    public ArrayList<AnswerDO> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AnswerDO> answers) {
        this.answers = answers;
    }

    public QuestionaireDetailsDO getQuentionaireDetails() {
        return QuentionaireDetails;
    }

    public void setQuentionaireDetails(QuestionaireDetailsDO quentionaireDetails) {
        QuentionaireDetails = quentionaireDetails;
    }

    public QuestionaireSpecialityDO getQuestionSpeciality() {
        return questionSpeciality;
    }

    public void setQuestionSpeciality(QuestionaireSpecialityDO questionSpeciality) {
        this.questionSpeciality = questionSpeciality;
    }
}
