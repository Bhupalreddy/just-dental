package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 29-06-2016.
 */
public class PastFutureRow implements Serializable
{
    @SerializedName("PatientInfo")
    private PatientInfo2 patientInfo ;

    @SerializedName("AppointmentDetails")
    private AppointmentDetails appointmentDetails ;

    @SerializedName("DoctorDetails")
    private DoctorDetails doctorDetails ;

    @SerializedName("RatingEntered")
    private boolean RatingEntered;

    public PatientInfo2 getPatientInfo() {
        return patientInfo;
    }

    public void setPatientInfo(PatientInfo2 patientInfo) {
        this.patientInfo = patientInfo;
    }

    public AppointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(AppointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public DoctorDetails getDoctorDetails() {
        return doctorDetails;
    }

    public void setDoctorDetails(DoctorDetails doctorDetails) {
        this.doctorDetails = doctorDetails;
    }

    public boolean isRatingEntered() {
        return RatingEntered;
    }

    public void setRatingEntered(boolean ratingEntered) {
        RatingEntered = ratingEntered;
    }
}
