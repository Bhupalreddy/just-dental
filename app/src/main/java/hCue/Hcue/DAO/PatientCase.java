package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientCase implements Serializable
{

    @SerializedName("ActualFees")
    private int ActualFees ;

    @SerializedName("AppointmentID")
    private long AppointmentID ;

    @SerializedName("ConsultationDt")
    private long ConsultationDt ;

    @SerializedName("DiagonsticNotes")
    private String DiagonsticNotes;

    @SerializedName("DoctorNotes")
    private String DoctorNotes ;

    @SerializedName("StartTime")
    private String StartTime;

    @SerializedName("DoctorVisitRsnID")
    private String DoctorVisitRsnID ;

    @SerializedName("FollowUpDetails")
    private FollowUpDetails followUpDetails ;

    @SerializedName("HistoryRecord")
    private ArrayList<HistoryRecord> historyRecord;

    @SerializedName("OtherVistReason")
    private String OtherVistReason;

    @SerializedName("PatientCaseID")
    private long PatientCaseID ;

    @SerializedName("PatientCaseVitals")
    private Vitals vitals;

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("DoctorDetails")
    private CaseDoctorDetails doctorDetails;

    @SerializedName("Notes")
    private NotesDO Notes;


    public int getActualFees() {
        return ActualFees;
    }

    public void setActualFees(int actualFees) {
        ActualFees = actualFees;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public long getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        ConsultationDt = consultationDt;
    }

    public String getDoctorVisitRsnID() {
        return DoctorVisitRsnID;
    }

    public void setDoctorVisitRsnID(String doctorVisitRsnID) {
        DoctorVisitRsnID = doctorVisitRsnID;
    }

    public long getPatientCaseID() {
        return PatientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        PatientCaseID = patientCaseID;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public FollowUpDetails getFollowUpDetails() {
        return followUpDetails;
    }

    public void setFollowUpDetails(FollowUpDetails followUpDetails) {
        this.followUpDetails = followUpDetails;
    }

    public String getDiagonsticNotes() {
        return DiagonsticNotes;
    }

    public void setDiagonsticNotes(String diagonsticNotes) {
        DiagonsticNotes = diagonsticNotes;
    }

    public String getDoctorNotes() {
        return DoctorNotes;
    }

    public void setDoctorNotes(String doctorNotes) {
        DoctorNotes = doctorNotes;
    }

    public ArrayList<HistoryRecord> getHistoryRecord() {
        return historyRecord;
    }

    public void setHistoryRecord(ArrayList<HistoryRecord> historyRecord) {
        this.historyRecord = historyRecord;
    }

    public String getOtherVistReason() {
        return OtherVistReason;
    }

    public void setOtherVistReason(String otherVistReason) {
        OtherVistReason = otherVistReason;
    }

    public Vitals getVitals() {
        return vitals;
    }

    public void setVitals(Vitals vitals) {
        this.vitals = vitals;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public CaseDoctorDetails getDoctorDetails() {
        return doctorDetails;
    }

    public void setDoctorDetails(CaseDoctorDetails doctorDetails) {
        this.doctorDetails = doctorDetails;
    }

    public NotesDO getNotes() {
        return Notes;
    }

    public void setNotes(NotesDO notes) {
        Notes = notes;
    }
}