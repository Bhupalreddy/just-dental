package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class DiagnosisNotesDO implements Serializable {

    @SerializedName("IsPresent")
    private String IsPresent ;

    @SerializedName("Condition")
    private String Condition ;

    @SerializedName("ConditionNotes")
    private String ConditionNotes ;

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    public String getConditionNotes() {
        return ConditionNotes;
    }

    public void setConditionNotes(String conditionNotes) {
        ConditionNotes = conditionNotes;
    }

    public String getIsPresent() {
        return IsPresent;
    }

    public void setIsPresent(String isPresent) {
        IsPresent = isPresent;
    }
}
