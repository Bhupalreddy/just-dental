package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 11-06-2016.
 */
public class CaseTreatmentDO implements Serializable
{
    @SerializedName("RowID")
    private String RowID;

    @SerializedName("TreatmentDesctription")
    private String TreatmentDesctription;

    @SerializedName("TreatMentID")
    private String TreatMentID;

    @SerializedName("TreatmentNotes")
    private String TreatmentNotes;

    @SerializedName("TreatmentDocuments")
    private ArrayList<String> treatmentDocuments;

    @SerializedName("TreatmentDocumentsURL")
    private ArrayList<TreatmentDocumentsURLDO> listTreatmentDocumentsURL;

    public String getRowID() {
        return RowID;
    }

    public void setRowID(String rowID) {
        RowID = rowID;
    }

    public String getTreatmentDesctription() {
        return TreatmentDesctription;
    }

    public void setTreatmentDesctription(String treatmentDesctription) {
        TreatmentDesctription = treatmentDesctription;
    }

    public String getTreatMentID() {
        return TreatMentID;
    }

    public void setTreatMentID(String treatMentID) {
        TreatMentID = treatMentID;
    }

    public String getTreatmentNotes() {
        return TreatmentNotes;
    }

    public void setTreatmentNotes(String treatmentNotes) {
        TreatmentNotes = treatmentNotes;
    }

    public ArrayList<String> getTreatmentDocuments() {
        return treatmentDocuments;
    }

    public void setTreatmentDocuments(ArrayList<String> treatmentDocuments) {
        this.treatmentDocuments = treatmentDocuments;
    }

    public ArrayList<TreatmentDocumentsURLDO> getListTreatmentDocumentsURL() {
        return listTreatmentDocumentsURL;
    }

    public void setListTreatmentDocumentsURL(ArrayList<TreatmentDocumentsURLDO> listTreatmentDocumentsURL) {
        this.listTreatmentDocumentsURL = listTreatmentDocumentsURL;
    }
}
