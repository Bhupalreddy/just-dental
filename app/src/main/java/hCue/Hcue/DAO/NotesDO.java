package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class NotesDO implements Serializable
{

    @SerializedName("VisitRsn")
    private ArrayList<String> listVisitRsn ;

    @SerializedName("Diagnostic")
    private ArrayList<String> listDiagnostic;

    @SerializedName("Investigation")
    private ArrayList<String> listInvestigation ;

    public ArrayList<String> getListVisitRsn() {
        return listVisitRsn;
    }

    public void setListVisitRsn(ArrayList<String> listVisitRsn) {
        this.listVisitRsn = listVisitRsn;
    }

    public ArrayList<String> getListDiagnostic() {
        return listDiagnostic;
    }

    public void setListDiagnostic(ArrayList<String> listDiagnostic) {
        this.listDiagnostic = listDiagnostic;
    }

    public ArrayList<String> getListInvestigation() {
        return listInvestigation;
    }

    public void setListInvestigation(ArrayList<String> listInvestigation) {
        this.listInvestigation = listInvestigation;
    }
}
