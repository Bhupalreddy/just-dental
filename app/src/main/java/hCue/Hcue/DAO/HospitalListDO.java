package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class HospitalListDO implements Serializable
{
    @SerializedName("count")
    private int count;
    @SerializedName("hospitalsList")
    private ArrayList<HospitalDO> hospitalsList ;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<HospitalDO> getHospitalsList() {
        return hospitalsList;
    }

    public void setHospitalsList(ArrayList<HospitalDO> hospitalsList) {
        this.hospitalsList = hospitalsList;
    }
}
