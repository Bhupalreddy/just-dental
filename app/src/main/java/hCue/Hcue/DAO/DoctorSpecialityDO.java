package hCue.Hcue.DAO;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class DoctorSpecialityDO implements Serializable
{
    private String name;
    private String category ;
    private String specialityID;
    private int hospitalid;
    private String hospitalloacation;
    private String symptomName;
    private String specialityName;

    public String getHospitalloacation() {
        return hospitalloacation;
    }

    public void setHospitalloacation(String hospitalloacation) {
        this.hospitalloacation = hospitalloacation;
    }

    public int getHospitalid() {
        return hospitalid;
    }

    public void setHospitalid(int hospitalid) {
        this.hospitalid = hospitalid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSpecialityID() {
        return specialityID;
    }

    public void setSpecialityID(String specialityID) {
        this.specialityID = specialityID;
    }

    public String getSymptomName() {
        return symptomName;
    }

    public void setSymptomName(String symptomName) {
        this.symptomName = symptomName;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }
}
