package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 11-06-2016.
 */
public class Extdetails implements Serializable
{
    @SerializedName("Fees")
    private int Fees ;

    @SerializedName("HospitalID")
    private int HospitalID ;

    @SerializedName("timePerPatient")
    private int timePerPatient ;

    @SerializedName("PrimaryIND")
    private char PrimaryIND ;

    @SerializedName("Latitude")
    private String Latitude ;

    @SerializedName("Longitude")
    private String Longitude ;

    @SerializedName("ClinicImages")
    private ClinicImagesDO ClinicImages ;



    public int getFees() {
        return Fees;
    }

    public void setFees(int fees) {
        Fees = fees;
    }

    public int getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(int hospitalID) {
        HospitalID = hospitalID;
    }

    public int getTimePerPatient() {
        return timePerPatient;
    }

    public void setTimePerPatient(int timePerPatient) {
        this.timePerPatient = timePerPatient;
    }

    public char getPrimaryIND() {
        return PrimaryIND;
    }

    public void setPrimaryIND(char primaryIND) {
        PrimaryIND = primaryIND;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public ClinicImagesDO getClinicImages() {
        return ClinicImages;
    }

    public void setClinicImages(ClinicImagesDO clinicImages) {
        ClinicImages = clinicImages;
    }
}
