package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User on 10/14/2016.
 */

public class Patient_Info implements Serializable
{
    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("Title")
    private String Title;

    @SerializedName("FullName")
    private String FullName;

    @SerializedName("AppointmentID")
    private long AppointmentID;

    @SerializedName("AppointmentStatus")
    private String AppointmentStatus;

    @SerializedName("MobileNumber")
    private Number MobileNumber;

    @SerializedName("Sex")
    private String Sex;

    @SerializedName("CurrentAge")
    private CurrentAge currentAge;

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public long getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(long appointmentID) {
        AppointmentID = appointmentID;
    }

    public String getAppointmentStatus() {
        return AppointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        AppointmentStatus = appointmentStatus;
    }

    public Number getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(Number mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public CurrentAge getCurrentAge() {
        return currentAge;
    }

    public void setCurrentAge(CurrentAge currentAge) {
        this.currentAge = currentAge;
    }
}
