package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 17-06-2016.
 */
public class PatientDetails4 implements Serializable
{
    @SerializedName("fullName")
    private String fullName ;

    @SerializedName("gender")
    private char gender ;

    @SerializedName("CurrentAge")
    private CurrentAge currentAge ;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public CurrentAge getCurrentAge() {
        return currentAge;
    }

    public void setCurrentAge(CurrentAge currentAge) {
        this.currentAge = currentAge;
    }
}
