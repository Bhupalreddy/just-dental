package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-08-2016.
 */
public class UploadfileRequest implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("UpLoadType")
    private String UpLoadType = "PatientRecords";

    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("FileExtn")
    private String FileExtn;

    @SerializedName("UpLoadedDate")
    private String UpLoadedDate;

    @SerializedName("DocumentNumber")
    private String DocumentNumber;

    @SerializedName("ImageStr")
    private String ImageStr;

    @SerializedName("FileID")
    private String FileID ;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public String getUpLoadType() {
        return UpLoadType;
    }

    public void setUpLoadType(String upLoadType) {
        UpLoadType = upLoadType;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getFileExtn() {
        return FileExtn;
    }

    public void setFileExtn(String fileExtn) {
        FileExtn = fileExtn;
    }

    public String getUpLoadedDate() {
        return UpLoadedDate;
    }

    public void setUpLoadedDate(String upLoadedDate) {
        UpLoadedDate = upLoadedDate;
    }

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }

    public String getImageStr() {
        return ImageStr;
    }

    public void setImageStr(String imageStr) {
        ImageStr = imageStr;
    }

    public String getFileID() {
        return FileID;
    }

    public void setFileID(String fileID) {
        FileID = fileID;
    }
}
