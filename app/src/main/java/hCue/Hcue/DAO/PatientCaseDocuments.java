package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientCaseDocuments implements Serializable
{
    @SerializedName("ConsultationDt")
    private long ConsultationDt ;

    @SerializedName("patientCaseID")
    private long patientCaseID ;

    public long getConsultationDt() {
        return ConsultationDt;
    }

    public void setConsultationDt(long consultationDt) {
        ConsultationDt = consultationDt;
    }

    public long getPatientCaseID() {
        return patientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        this.patientCaseID = patientCaseID;
    }
}
