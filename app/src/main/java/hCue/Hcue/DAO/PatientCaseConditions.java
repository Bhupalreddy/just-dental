package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientCaseConditions implements Serializable
{
    @SerializedName("ConditionID")
    private int ConditionID ;

    @SerializedName("ConditionDesc")
    private String ConditionDesc;

    @SerializedName("ConditionNotes")
    private String ConditionNotes ;

    @SerializedName("ConditionTreatment")
    private ArrayList<ConditionTreatment> conditionTreatment;

    public int getConditionID() {
        return ConditionID;
    }

    public void setConditionID(int conditionID) {
        ConditionID = conditionID;
    }

    public String getConditionDesc() {
        return ConditionDesc;
    }

    public void setConditionDesc(String conditionDesc) {
        ConditionDesc = conditionDesc;
    }

    public String getConditionNotes() {
        return ConditionNotes;
    }

    public void setConditionNotes(String conditionNotes) {
        ConditionNotes = conditionNotes;
    }

    public ArrayList<ConditionTreatment> getConditionTreatment() {
        return conditionTreatment;
    }

    public void setConditionTreatment(ArrayList<ConditionTreatment> conditionTreatment) {
        this.conditionTreatment = conditionTreatment;
    }
}
