package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 01-08-2016.
 */
public class CaseLabHistory implements Serializable
{
    @SerializedName("RowID")
    private int RowID ;

    @SerializedName("PatientCaseID")
    private long PatientCaseID ;

    @SerializedName("LabWrkFlowStatusID")
    private String LabWrkFlowStatusID ;

    @SerializedName("LabTestTypeID")
    private String LabTestTypeID ;

    @SerializedName("LabTestName")
    private String LabTestName ;

    @SerializedName("LabNotes")
    private String LabNotes ;

    @SerializedName("LabName")
    private String LabName ;

    @SerializedName("LabTestCode")
    private String LabTestCode ;

    @SerializedName("LabGroupTag")
    private String LabGroupTag;

    @SerializedName("doctorDetails")
    private CaseDoctorDetails listDoctorDetails ;

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }

    public long getPatientCaseID() {
        return PatientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        PatientCaseID = patientCaseID;
    }

    public String getLabWrkFlowStatusID() {
        return LabWrkFlowStatusID;
    }

    public void setLabWrkFlowStatusID(String labWrkFlowStatusID) {
        LabWrkFlowStatusID = labWrkFlowStatusID;
    }

    public String getLabTestTypeID() {
        return LabTestTypeID;
    }

    public void setLabTestTypeID(String labTestTypeID) {
        LabTestTypeID = labTestTypeID;
    }

    public String getLabTestName() {
        return LabTestName;
    }

    public void setLabTestName(String labTestName) {
        LabTestName = labTestName;
    }

    public String getLabNotes() {
        return LabNotes;
    }

    public void setLabNotes(String labNotes) {
        LabNotes = labNotes;
    }

    public String getLabName() {
        return LabName;
    }

    public void setLabName(String labName) {
        LabName = labName;
    }

    public String getLabTestCode() {
        return LabTestCode;
    }

    public void setLabTestCode(String labTestCode) {
        LabTestCode = labTestCode;
    }

    public String getLabGroupTag() {
        return LabGroupTag;
    }

    public void setLabGroupTag(String labGroupTag) {
        LabGroupTag = labGroupTag;
    }

    public CaseDoctorDetails getListDoctorDetails() {
        return listDoctorDetails;
    }

    public void setListDoctorDetails(CaseDoctorDetails listDoctorDetails) {
        this.listDoctorDetails = listDoctorDetails;
    }
}
