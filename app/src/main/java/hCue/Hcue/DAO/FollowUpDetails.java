package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class FollowUpDetails implements Serializable
{
    @SerializedName("FollowUpDay")
    private int FollowUpDay ;

    @SerializedName("DoctorNotes")
    private String DoctorNotes;

    @SerializedName("FollowUpDate")
    private long FollowUpDate ;

    @SerializedName("FollowUpNotes")
    private String FollowUpNotes;

    public int getFollowUpDay() {
        return FollowUpDay;
    }

    public void setFollowUpDay(int followUpDay) {
        FollowUpDay = followUpDay;
    }

    public String getDoctorNotes() {
        return DoctorNotes;
    }

    public void setDoctorNotes(String doctorNotes) {
        DoctorNotes = doctorNotes;
    }

    public long getFollowUpDate() {
        return FollowUpDate;
    }

    public void setFollowUpDate(long followUpDate) {
        FollowUpDate = followUpDate;
    }

    public String getFollowUpNotes() {
        return FollowUpNotes;
    }

    public void setFollowUpNotes(String followUpNotes) {
        FollowUpNotes = followUpNotes;
    }
}
