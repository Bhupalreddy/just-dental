package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 28/11/16.
 */

public class PharmaDetails implements Serializable
{
    @SerializedName("PharmaName")
    private String PharmaName;

    @SerializedName("PharmaAddress")
    private PharmaAddress PharmaAddress;

    @SerializedName("PharmaPhone")
    private PatientPhone PharmaPhone;

    public String getPharmaName() {
        return PharmaName;
    }

    public void setPharmaName(String pharmaName) {
        PharmaName = pharmaName;
    }

    public hCue.Hcue.DAO.PharmaAddress getPharmaAddress() {
        return PharmaAddress;
    }

    public void setPharmaAddress(hCue.Hcue.DAO.PharmaAddress pharmaAddress) {
        PharmaAddress = pharmaAddress;
    }

    public PatientPhone getPharmaPhone() {
        return PharmaPhone;
    }

    public void setPharmaPhone(PatientPhone pharmaPhone) {
        PharmaPhone = pharmaPhone;
    }
}
