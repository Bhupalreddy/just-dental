package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 17-08-2016.
 */
public class HospitalInfo implements Serializable
{
    @SerializedName("BranchCD")
    private String BranchCD ;

    @SerializedName("HospitalCD")
    private String HospitalCD ;

    @SerializedName("HospitalID")
    private int HospitalID = 0;

    @SerializedName("HospitalName")
    private String HospitalName ;

    @SerializedName("ParentHospitalID")
    private int ParentHospitalID = 0;

    public String getBranchCD() {
        return BranchCD;
    }

    public void setBranchCD(String branchCD) {
        BranchCD = branchCD;
    }

    public String getHospitalCD() {
        return HospitalCD;
    }

    public void setHospitalCD(String hospitalCD) {
        HospitalCD = hospitalCD;
    }

    public int getHospitalID() {
        return HospitalID;
    }

    public void setHospitalID(int hospitalID) {
        HospitalID = hospitalID;
    }

    public String getHospitalName() {
        return HospitalName;
    }

    public void setHospitalName(String hospitalName) {
        HospitalName = hospitalName;
    }

    public int getParentHospitalID() {
        return ParentHospitalID;
    }

    public void setParentHospitalID(int parentHospitalID) {
        ParentHospitalID = parentHospitalID;
    }
}
