package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Appdest on 30-07-2016.
 */
public class DentalCasePrescription implements Serializable
{
    @SerializedName("RowID")
    private int RowID ;

    @SerializedName("ShowPrimary")
    private String ShowPrimary;

    @SerializedName("BodyParts")
    private BodyParts bodyParts ;

    @SerializedName("PatientCaseConditions")
    private ArrayList<PatientCaseConditions> listPatientCaseCondition;

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }

    public String getShowPrimary() {
        return ShowPrimary;
    }

    public void setShowPrimary(String showPrimary) {
        ShowPrimary = showPrimary;
    }

    public BodyParts getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(BodyParts bodyParts) {
        this.bodyParts = bodyParts;
    }

    public ArrayList<PatientCaseConditions> getListPatientCaseCondition() {
        return listPatientCaseCondition;
    }

    public void setListPatientCaseCondition(ArrayList<PatientCaseConditions> listPatientCaseCondition) {
        this.listPatientCaseCondition = listPatientCaseCondition;
    }
}
