package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 28/11/16.
 */

public class PatientOrderRequest implements Serializable
{
    @SerializedName("USRType")
    private String USRType = "PATIENT";

    @SerializedName("USRId")
    private long USRId;

    @SerializedName("PatientID")
    private long PatientID;

    @SerializedName("PageNumber")
    private int PageNumber = 1;

    @SerializedName("PageSize")
    private int PageSize = 30;

    @SerializedName("DeliveredOrders")
    private String DeliveredOrders;

    public String getUSRType() {
        return USRType;
    }

    public void setUSRType(String USRType) {
        this.USRType = USRType;
    }

    public long getUSRId() {
        return USRId;
    }

    public void setUSRId(long USRId) {
        this.USRId = USRId;
    }

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public String getDeliveredOrders() {
        return DeliveredOrders;
    }

    public void setDeliveredOrders(String deliveredOrders) {
        DeliveredOrders = deliveredOrders;
    }
}
