package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-08-2016.
 */
public class PatientFileRow implements Serializable
{
    @SerializedName("PatientDocuments")
    private PatientDocuments patientDocuments;

    public PatientDocuments getPatientDocuments() {
        return patientDocuments;
    }

    public void setPatientDocuments(PatientDocuments patientDocuments) {
        this.patientDocuments = patientDocuments;
    }
}
