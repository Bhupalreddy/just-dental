package hCue.Hcue.DAO;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.LinkedHashMap;

import hCue.Hcue.utils.Constants;

/**
 * Created by Appdest on 23-06-2016.
 */
public class SpecialityResRow implements Serializable
{
    @SerializedName("Address")
    private String Address ;

    @SerializedName("AddressID")
    private int AddressID ;

    @SerializedName("ClinicName")
    private String ClinicName ;

    @SerializedName("Distance")
    private String Distance ;

    @SerializedName("DoctorID")
    private int DoctorID ;

    @SerializedName("EmailID")
    private String EmailID ;

    @SerializedName("Exp")
    private int Exp ;

    @SerializedName("Fees")
    private int Fees ;

    @SerializedName("FullName")
    private String FullName ;

    @SerializedName("Gender")
    private char Gender ;

    @SerializedName("Latitude")
    private String Latitude ;

    @SerializedName("Location")
    private String Location ;

    @SerializedName("Longitude")
    private String Longitude ;

    @SerializedName("PhoneCountryCD")
    private String PhoneCountryCD ;

    @SerializedName("PhoneNumber")
    private String PhoneNumber ;

    @SerializedName("ProfileImage")
    private String ProfileImage ;

    @SerializedName("Prospect")
    private String Prospect ;

    @SerializedName("Qualification")
    private String Qualification ;

    @SerializedName("SpecialityID")
    private String SpecialityID ;

    private String specialityDesc ;

    private boolean isDoctormatched ;

    private String locale ;

    private String otherVisitReason ;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int addressID) {
        AddressID = addressID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public void setClinicName(String clinicName) {
        ClinicName = clinicName;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public int getExp() {
        return Exp;
    }

    public void setExp(int exp) {
        Exp = exp;
    }

    public int getFees() {
        return Fees;
    }

    public void setFees(int fees) {
        Fees = fees;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public char getGender() {
        return Gender;
    }

    public void setGender(char gender) {
        Gender = gender;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getPhoneCountryCD() {
        return PhoneCountryCD;
    }

    public void setPhoneCountryCD(String phoneCountryCD) {
        PhoneCountryCD = phoneCountryCD;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getProspect() {
        return Prospect;
    }

    public void setProspect(String prospect) {
        Prospect = prospect;
    }

    public String getQualification() {
        return Qualification;
    }

    public void setQualification(String qualification) {
        Qualification = qualification;
    }

    public String getSpecialityID() {
        return SpecialityID;
    }

    public void setSpecialityID(String specialityID) {
        SpecialityID = specialityID;
    }

    public boolean isDoctormatched() {
        return isDoctormatched;
    }

    public void setDoctormatched(boolean doctormatched) {
        isDoctormatched = doctormatched;
    }

    public String getSpecialityDesc() {
        if(specialityDesc == null)
        {
            Gson gson = new Gson();
            LinkedHashMap<String, String> speciality = gson.fromJson(getSpecialityID(), new TypeToken<LinkedHashMap<String, String>>(){}.getType());
            StringBuilder specialitybuilder = new StringBuilder();
            for(String specialityid : speciality.values())
            {
                specialitybuilder.append(Constants.specialitiesMap.get(specialityid));
                setSpecialityDesc(specialitybuilder.toString());
                break;
            }
        }
        return specialityDesc;
    }

    public void setSpecialityDesc(String specialityDesc) {
        this.specialityDesc = specialityDesc;
    }

    @Override
    public String toString() {
        if(isDoctormatched)
            return this.getFullName();
        else
            return this.getSpecialityDesc();
    }

    public String getOtherVisitReason() {
        return otherVisitReason;
    }

    public void setOtherVisitReason(String otherVisitReason) {
        this.otherVisitReason = otherVisitReason;
    }
}
