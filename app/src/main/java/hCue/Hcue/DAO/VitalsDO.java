package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class VitalsDO implements Serializable {

    @SerializedName("HGT")
    private String HGT ;

    @SerializedName("WGT")
    private String WGT ;

    public String getHGT() {
        return HGT;
    }

    public void setHGT(String HGT) {
        this.HGT = HGT;
    }

    public String getWGT() {
        return WGT;
    }

    public void setWGT(String WGT) {
        this.WGT = WGT;
    }
}
