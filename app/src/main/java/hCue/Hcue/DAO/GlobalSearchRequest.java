package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class GlobalSearchRequest implements Serializable
{
    @SerializedName("search_text")
    private String search_text;
    @SerializedName("latitude")
    private String latitude ;
    @SerializedName("radius")
    private int radius ;
    @SerializedName("longitude")
    private String longitude ;

    public String getSearch_text() {
        return search_text;
    }

    public void setSearch_text(String search_text) {
        this.search_text = search_text;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
