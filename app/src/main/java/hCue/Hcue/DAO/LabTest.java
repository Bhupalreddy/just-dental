package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 14-06-2016.
 */
public class LabTest implements Serializable
{
    @SerializedName("LabGroupTag")
    private String LabGroupTag = "TGroupTag";

    @SerializedName("LabNotes")
    private String LabNotes = "2";

    @SerializedName("LabTestCode")
    private String LabTestCode = "TCode";

    @SerializedName("LabTestName")
    private String LabTestName ;

    @SerializedName("LabTestTypeID")
    private String LabTestTypeID = "TEST";

    @SerializedName("LabWrkFlowStatusID")
    private String LabWrkFlowStatusID = "INIT";

    @SerializedName("RowID")
    private int RowID = 1;

    public String getLabGroupTag() {
        return LabGroupTag;
    }

    public void setLabGroupTag(String labGroupTag) {
        LabGroupTag = labGroupTag;
    }

    public String getLabNotes() {
        return LabNotes;
    }

    public void setLabNotes(String labNotes) {
        LabNotes = labNotes;
    }

    public String getLabTestCode() {
        return LabTestCode;
    }

    public void setLabTestCode(String labTestCode) {
        LabTestCode = labTestCode;
    }

    public String getLabTestName() {
        return LabTestName;
    }

    public void setLabTestName(String labTestName) {
        LabTestName = labTestName;
    }

    public String getLabTestTypeID() {
        return LabTestTypeID;
    }

    public void setLabTestTypeID(String labTestTypeID) {
        LabTestTypeID = labTestTypeID;
    }

    public String getLabWrkFlowStatusID() {
        return LabWrkFlowStatusID;
    }

    public void setLabWrkFlowStatusID(String labWrkFlowStatusID) {
        LabWrkFlowStatusID = labWrkFlowStatusID;
    }

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }
}
