package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class QuestionaireSpecialityDO implements Serializable {

    @SerializedName("SpecialityCD")
    private String SpecialityCD ;

    @SerializedName("SpecialityDesc")
    private String SpecialityDesc ;

    public String getSpecialityCD() {
        return SpecialityCD;
    }

    public void setSpecialityCD(String specialityCD) {
        SpecialityCD = specialityCD;
    }

    public String getSpecialityDesc() {
        return SpecialityDesc;
    }

    public void setSpecialityDesc(String specialityDesc) {
        SpecialityDesc = specialityDesc;
    }
}
