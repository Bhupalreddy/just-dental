package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Created by Appdest on 02-08-2016.
 */
public class CaseDocuments implements Serializable
{
    @SerializedName("Images")
    private LinkedHashMap<Integer, String> imagename ;

    @SerializedName("URL")
    private LinkedHashMap<Integer, String> imageurls ;

    public LinkedHashMap<Integer, String> getImagename() {
        return imagename;
    }

    public void setImagename(LinkedHashMap<Integer, String> imagename) {
        this.imagename = imagename;
    }

    public LinkedHashMap<Integer, String> getImageurls() {
        return imageurls;
    }

    public void setImageurls(LinkedHashMap<Integer, String> imageurls) {
        this.imageurls = imageurls;
    }
}
