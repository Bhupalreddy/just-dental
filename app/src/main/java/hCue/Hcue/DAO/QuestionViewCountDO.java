package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class QuestionViewCountDO implements Serializable {

    @SerializedName("AgreeCount")
    private int AgreeCount;

    @SerializedName("LikeCount")
    private int LikeCount ;

    @SerializedName("UnLikeCount")
    private int UnLikeCount ;

    @SerializedName("ViewCount")
    private int ViewCount ;

    public int getAgreeCount() {
        return AgreeCount;
    }

    public void setAgreeCount(int agreeCount) {
        AgreeCount = agreeCount;
    }

    public int getLikeCount() {
        return LikeCount;
    }

    public void setLikeCount(int likeCount) {
        LikeCount = likeCount;
    }

    public int getUnLikeCount() {
        return UnLikeCount;
    }

    public void setUnLikeCount(int unLikeCount) {
        UnLikeCount = unLikeCount;
    }

    public int getViewCount() {
        return ViewCount;
    }

    public void setViewCount(int viewCount) {
        ViewCount = viewCount;
    }
}
