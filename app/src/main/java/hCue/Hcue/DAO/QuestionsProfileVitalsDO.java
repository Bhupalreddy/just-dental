package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class QuestionsProfileVitalsDO implements Serializable {

    @SerializedName("Vitals")
    private VitalsDO Vitals ;

    public VitalsDO getVitals() {
        return Vitals;
    }

    public void setVitals(VitalsDO vitals) {
        Vitals = vitals;
    }
}
