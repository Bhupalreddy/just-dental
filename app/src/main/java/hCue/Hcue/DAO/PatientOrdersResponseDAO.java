package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shyamprasadg on 28/11/16.
 */

public class PatientOrdersResponseDAO implements Serializable
{
    @SerializedName("status")
    private String status;

    @SerializedName("response")
    private PatientOrderResponse response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PatientOrderResponse getResponse() {
        return response;
    }

    public void setResponse(PatientOrderResponse response) {
        this.response = response;
    }
}
