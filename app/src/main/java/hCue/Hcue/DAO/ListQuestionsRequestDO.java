package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bhupalreddy on 15/03/17.
 */

public class ListQuestionsRequestDO implements Serializable {

    @SerializedName("PatientID")
    private long PatientID ;

    @SerializedName("IsPrivate")
    private String IsPrivate ;

    @SerializedName("PageNumber")
    private int PageNumber ;

    @SerializedName("PageSize")
    private int PageSize ;

    @SerializedName("IsPublicQuestion")
    private String IsPublicQuestion = "Y" ;

    public long getPatientID() {
        return PatientID;
    }

    public void setPatientID(long patientID) {
        PatientID = patientID;
    }

    public String getIsPrivate() {
        return IsPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        IsPrivate = isPrivate;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public String getIsPublicQuestion() {
        return IsPublicQuestion;
    }

    public void setIsPublicQuestion(String isPublicQuestion) {
        IsPublicQuestion = isPublicQuestion;
    }
}
