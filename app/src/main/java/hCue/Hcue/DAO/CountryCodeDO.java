package hCue.Hcue.DAO;

/**
 * Created by shyamprasadg on 27/09/16.
 */

public class CountryCodeDO
{
    private int Countrycode;

    private String CountryName;

    public int getCountrycode() {
        return Countrycode;
    }

    public void setCountrycode(int countrycode) {
        Countrycode = countrycode;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }


}
