package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 04-06-2016.
 */
public class Vitals implements Serializable {

    @SerializedName("BloodGroup")
    private String BloodGroup ;

    @SerializedName("PLS")
    private String PLS;

    @SerializedName("LPP")
    private String LPP;

    @SerializedName("SCT")
    private String SCT;

    @SerializedName("SRM")
    private  String SRM;

    @SerializedName("Height")
    private String Height ;

    @SerializedName("Weight")
    private String Weight ;

    @SerializedName("BLG")
    private String BLG;

    @SerializedName("BPH")
    private String BPH;

    @SerializedName("BPL")
    private String BPL;

    @SerializedName("HGT")
    private String HGT;

    @SerializedName("SFT")
    private String SFT;

    @SerializedName("SPP")
    private String SPP;

    @SerializedName("TMP")
    private String TMP;

    @SerializedName("WGT")
    private String WGT;

    @SerializedName("HCF")
    private String HCF;

    public String getPLS() {
        return PLS;
    }

    public void setPLS(String PLS) {
        this.PLS = PLS;
    }

    public String getLPP() {
        return LPP;
    }

    public void setLPP(String LPP) {
        this.LPP = LPP;
    }

    public String getSCT() {
        return SCT;
    }

    public void setSCT(String SCT) {
        this.SCT = SCT;
    }

    public String getSRM() {
        return SRM;
    }

    public void setSRM(String SRM) {
        this.SRM = SRM;
    }

    public String getBLG() {
        return BLG;
    }

    public void setBLG(String BLG) {
        this.BLG = BLG;
    }

    public String getBPH() {
        return BPH;
    }

    public void setBPH(String BPH) {
        this.BPH = BPH;
    }

    public String getBPL() {
        return BPL;
    }

    public void setBPL(String BPL) {
        this.BPL = BPL;
    }

    public String getHGT() {
        return HGT;
    }

    public void setHGT(String HGT) {
        this.HGT = HGT;
    }

    public String getSFT() {
        return SFT;
    }

    public void setSFT(String SFT) {
        this.SFT = SFT;
    }

    public String getSPP() {
        return SPP;
    }

    public void setSPP(String SPP) {
        this.SPP = SPP;
    }

    public String getTMP() {
        return TMP;
    }

    public void setTMP(String TMP) {
        this.TMP = TMP;
    }

    public String getWGT() {
        return WGT;
    }

    public void setWGT(String WGT) {
        this.WGT = WGT;
    }

    public String getBloodGroup() {
        return BloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        BloodGroup = bloodGroup;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getHCF() {
        return HCF;
    }

    public void setHCF(String HCF) {
        this.HCF = HCF;
    }
}
