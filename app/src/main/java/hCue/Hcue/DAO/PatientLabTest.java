package hCue.Hcue.DAO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Appdest on 30-07-2016.
 */
public class PatientLabTest implements Serializable
{
    @SerializedName("LabGroupTag")
    private String LabGroupTag ;

    @SerializedName("LabName")
    private String LabName;

    @SerializedName("LabNotes")
    private String LabNotes ;

    @SerializedName("LabTestCode")
    private String LabTestCode;

    @SerializedName("LabTestName")
    private String LabTestName ;

    @SerializedName("LabTestTypeID")
    private String LabTestTypeID;

    @SerializedName("LabWrkFlowStatusID")
    private String LabWrkFlowStatusID ;

    @SerializedName("PatientCaseID")
    private long PatientCaseID;

    @SerializedName("requestSubmitDate")
    private long requestSubmitDate ;

    @SerializedName("RowID")
    private int RowID;

    public String getLabGroupTag() {
        return LabGroupTag;
    }

    public void setLabGroupTag(String labGroupTag) {
        LabGroupTag = labGroupTag;
    }

    public String getLabName() {
        return LabName;
    }

    public void setLabName(String labName) {
        LabName = labName;
    }

    public String getLabNotes() {
        return LabNotes;
    }

    public void setLabNotes(String labNotes) {
        LabNotes = labNotes;
    }

    public String getLabTestCode() {
        return LabTestCode;
    }

    public void setLabTestCode(String labTestCode) {
        LabTestCode = labTestCode;
    }

    public String getLabTestName() {
        return LabTestName;
    }

    public void setLabTestName(String labTestName) {
        LabTestName = labTestName;
    }

    public String getLabTestTypeID() {
        return LabTestTypeID;
    }

    public void setLabTestTypeID(String labTestTypeID) {
        LabTestTypeID = labTestTypeID;
    }

    public String getLabWrkFlowStatusID() {
        return LabWrkFlowStatusID;
    }

    public void setLabWrkFlowStatusID(String labWrkFlowStatusID) {
        LabWrkFlowStatusID = labWrkFlowStatusID;
    }

    public long getPatientCaseID() {
        return PatientCaseID;
    }

    public void setPatientCaseID(long patientCaseID) {
        PatientCaseID = patientCaseID;
    }

    public long getRequestSubmitDate() {
        return requestSubmitDate;
    }

    public void setRequestSubmitDate(long requestSubmitDate) {
        this.requestSubmitDate = requestSubmitDate;
    }

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }
}
