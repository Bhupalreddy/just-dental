package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.PastFutureRow;
import hCue.Hcue.model.DoctorRatingReq;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 23/06/16.
 */
public class FeedBackActivity extends BaseActivity implements View.OnClickListener
{
    private LinearLayout llFeedBack;
    private ImageView ivDoctorPic;
    private TextView tvDoctorName,tvSpecality,tvFeedBackDate,tvFeedBackTime,tvSubmit,tvAdditionalComments;
    private float StarValue = 0f;
    private PastFutureRow pastFutureRow ;
    private EditText edtData ;
    private String additioncomments = "";
    private RatingBar rating;
    private CheckBox cbReception,cbStaff,cbService,cbCare,cbClean,cbAll;

    @Override
    public void initialize()
    {
        llFeedBack  =   (LinearLayout) getLayoutInflater().inflate(R.layout.feedback,null,false);
        llBody.addView(llFeedBack);
        flBottomBar.setVisibility(View.GONE);
        setSpecificTypeFace(llFeedBack, ApplicationConstants.WALSHEIM_MEDIUM);

        if(getIntent().hasExtra("SELECTEDDETAILS"))
        {
            pastFutureRow = (PastFutureRow) getIntent().getSerializableExtra("SELECTEDDETAILS");
        }

        ivDoctorPic 	        = (ImageView)       llFeedBack.findViewById(R.id.ivDoctorPic);

        tvDoctorName 			= (TextView)        llFeedBack.findViewById(R.id.tvDoctorName);
        tvSpecality 			= (TextView)        llFeedBack.findViewById(R.id.tvSpecality);
        tvAdditionalComments    = (TextView)        llFeedBack.findViewById(R.id.tvAdditionalComments);
        tvFeedBackDate 	        = (TextView)        llFeedBack.findViewById(R.id.tvFeedBackDate);
        tvFeedBackTime 	        = (TextView)        llFeedBack.findViewById(R.id.tvFeedBackTime);
        tvSubmit 	            = (TextView)        llFeedBack.findViewById(R.id.tvSubmit);

        ivClose 		        = (ImageView)       llFeedBack.findViewById(R.id.ivClose);
        edtData 		        = (EditText)        llFeedBack.findViewById(R.id.edtData);

        rating                  = (RatingBar)       llFeedBack.findViewById(R.id.rating);
        cbReception             = (CheckBox)        llFeedBack.findViewById(R.id.cbReception);
        cbStaff                 = (CheckBox)        llFeedBack.findViewById(R.id.cbStaff);
        cbService               = (CheckBox)        llFeedBack.findViewById(R.id.cbService);
        cbCare                  = (CheckBox)        llFeedBack.findViewById(R.id.cbCare);
        cbClean                 = (CheckBox)        llFeedBack.findViewById(R.id.cbClean);
        cbAll                   = (CheckBox)        llFeedBack.findViewById(R.id.cbAll);


        tvTopTitle.setText("PLEASE GIVE RATING");
        edtData.setText("OK");
        tvAdditionalComments.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

                edtData.setText("");
                edtData.setSingleLine(false);
                edtData.setLines(5);
                int maxLength = 150;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(maxLength);
                edtData.setFilters(fArray);

        if(pastFutureRow != null)
        {
            if(pastFutureRow.getDoctorDetails().getProfileImage() != null && !pastFutureRow.getDoctorDetails().getProfileImage().isEmpty()) {
                Picasso.with(this).load(Uri.parse(pastFutureRow.getDoctorDetails().getProfileImage())).into(ivDoctorPic);
            }
            else
            {
                if(pastFutureRow.getDoctorDetails().getGender() == 'M')
                {
                    Picasso.with(this).load(R.drawable.male).into(ivDoctorPic);
                }
                else
                {
                    Picasso.with(this).load(R.drawable.female).into(ivDoctorPic);
                }
            }
            if(pastFutureRow.getDoctorDetails().getDoctorFullName().startsWith("Dr."))
                tvDoctorName.setText(pastFutureRow.getDoctorDetails().getDoctorFullName());
            else
                tvDoctorName.setText("Dr."+pastFutureRow.getDoctorDetails().getDoctorFullName());
            StringBuilder specialitybuilder = new StringBuilder();
            if(pastFutureRow.getDoctorDetails().getDoctorSpecialization() != null)
                for(String specialityid : pastFutureRow.getDoctorDetails().getDoctorSpecialization().values())
                {
                    specialitybuilder.append(Constants.specialitiesMap.get(specialityid));
                }
            tvSpecality.setText(pastFutureRow.getAppointmentDetails().getClinicName());
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeInMillis(pastFutureRow.getAppointmentDetails().getConsultationDt());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
            tvFeedBackDate.setText(simpleDateFormat.format(calendar.getTime()));
            tvFeedBackTime.setText(pastFutureRow.getAppointmentDetails().getStartTime()+" Hrs");
        }

        cbAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbAll.isChecked())
                {
                    cbCare.setChecked(true);
                    cbClean.setChecked(true);
                    cbReception.setChecked(true);
                    cbService.setChecked(true);
                    cbStaff.setChecked(true);
                }
                else
                {
                    cbCare.setChecked(false);
                    cbClean.setChecked(false);
                    cbReception.setChecked(false);
                    cbService.setChecked(false);
                    cbStaff.setChecked(false);
                }
            }
        });

        /*cbAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    cbCare.setChecked(true);
                    cbClean.setChecked(true);
                    cbReception.setChecked(true);
                    cbService.setChecked(true);
                    cbStaff.setChecked(true);
                }
                else
                {
                    cbCare.setChecked(false);
                    cbClean.setChecked(false);
                    cbReception.setChecked(false);
                    cbService.setChecked(false);
                    cbStaff.setChecked(false);
                }
            }
        });*/

        cbCare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbAll.isChecked() && !cbClean.isChecked())
                    cbAll.setChecked(false);
            }
        });
        cbClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbAll.isChecked() && !cbClean.isChecked())
                    cbAll.setChecked(false);
//                else
//                    cbAll.setChecked(true);
            }
        });
        cbReception.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbAll.isChecked() && !cbReception.isChecked())
                    cbAll.setChecked(false);
            }
        });
        cbService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbAll.isChecked()  && !cbService.isChecked())
                    cbAll.setChecked(false);

            }
        });
        cbStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbAll.isChecked() && !cbStaff.isChecked())
                    cbAll.setChecked(false);
            }
        });


        tvSubmit.setOnClickListener(this);

        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener()
        {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
            {
                tvAdditionalComments.setVisibility(View.VISIBLE);

                if(rating == 1)
                {
                    tvAdditionalComments.setText(R.string.one_stars);
                }
                else if(rating == 2)
                {
                    tvAdditionalComments.setText(R.string.two_stars);
                }
                else if(rating == 3)
                {
                    tvAdditionalComments.setText(R.string.three_stars);
                }
                else if(rating == 4)
                {
                    tvAdditionalComments.setText(R.string.four_stars);
                }
                else if(rating == 5)
                {
                    tvAdditionalComments.setText(R.string.five_stars);
                }

                StarValue = rating*2;


            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tvSubmit :
                if(StarValue == 0f)
                {
                    ShowAlertDialog("Alert","Please select a rating to give feedback.",R.drawable.alert);
                }else
                {
                    additioncomments = edtData.getText().toString();
                    callDoctorRatingService();
                }
                break;
        }
    }

    private void callDoctorRatingService()
    {
        showLoader("");
        DoctorRatingReq doctorRatingReq = new DoctorRatingReq();
        doctorRatingReq.setStarValue(StarValue);
        doctorRatingReq.setAppointmentID(pastFutureRow.getAppointmentDetails().getAppointmentID());
        doctorRatingReq.setDoctorID(pastFutureRow.getAppointmentDetails().getDoctorID());
        doctorRatingReq.setPatientID(pastFutureRow.getPatientInfo().getPatientID());
        doctorRatingReq.setUSRId(pastFutureRow.getPatientInfo().getPatientID());
        doctorRatingReq.setFeedbackQuestions(buildSelectedFeedbackQuestions());
        if(additioncomments != null && !additioncomments.isEmpty())
            doctorRatingReq.setRatingComments(additioncomments);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).giveDocotorRating(doctorRatingReq, new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Failed to update Doctor rating. Please try again later.",R.drawable.worng);
            }

            @Override
            public void success(String s, Response response)
            {
                hideLoader();
                Intent slideactivity = new Intent(FeedBackActivity.this, ThankYou.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
                finish();
            }
        });
    }

    private String buildSelectedFeedbackQuestions(){

        StringBuilder resultStr = new StringBuilder();
        boolean isFirst = true;

        if (cbAll.isChecked()){

            resultStr.append("{").append(cbReception.getText().toString())
                    .append(",").append(cbStaff.getText().toString())
                    .append(",").append(cbService.getText().toString())
                    .append(",").append(cbCare.getText().toString())
                    .append(",").append(cbClean.getText().toString())
                    .append("}");
        }else {
            resultStr.append("{");
            if (cbReception.isChecked()){
                    isFirst = false;
                    resultStr.append(cbReception.getText().toString());
            }
            if (cbStaff.isChecked()){
                if (isFirst){
                    isFirst = false;
                    resultStr.append(cbStaff.getText().toString());
                }else{
                    resultStr.append(",").append(cbStaff.getText().toString());
                }

            }
            if (cbService.isChecked()){
                if (isFirst){
                    isFirst = false;
                    resultStr.append(cbService.getText().toString());
                }else{
                    resultStr.append(",").append(cbService.getText().toString());
                }
            }
            if (cbCare.isChecked()){
                if (isFirst){
                    isFirst = false;
                    resultStr.append(cbCare.getText().toString());
                }else{
                    resultStr.append(",").append(cbCare.getText().toString());
                }
            }
            if (cbClean.isChecked()){
                if (isFirst){
                    isFirst = false;
                    resultStr.append(cbClean.getText().toString());
                }else{
                    resultStr.append(",").append(cbClean.getText().toString());
                }
            }
            resultStr.append("}");
        }

        if (isFirst){
            resultStr.append("");
        }
        return resultStr.toString();
    }
}
