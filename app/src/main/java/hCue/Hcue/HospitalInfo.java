package hCue.Hcue;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hCue.Hcue.DAO.DoctorSpecialityDO;
import hCue.Hcue.DAO.Doctor_Details;
import hCue.Hcue.DAO.HospitalAddress;
import hCue.Hcue.DAO.HospitalDocuments;
import hCue.Hcue.adapters.DoctorsAdapter;
import hCue.Hcue.adapters.ServicesAdapter;
import hCue.Hcue.model.SearchHospitalRequest;
import hCue.Hcue.model.SearchHospitalResponce;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.CustomSimpleImageLoadingListener;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.utils.Utils;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 13/10/16.
 */

public class HospitalInfo extends BaseActivity
{
    private LinearLayout llDoctor ,llClinicThumbnails,llCommon,llSearch,llHospital,llServices,llClinicPhotos,llAvailability;
    private TextView tvHospitalAddress,tvDistance,tvSeeMoreDoctors,tvSeeMoreServices,tvMenuPicsCount,tvNoresult;
    private FrameLayout fMapView;
    private GoogleMap map;
    private MapView mapView;
    private LatLng latLng;
    private ImageView mapPreview,ivHospitalPic,ivMenuOne,ivMenuTwo,ivMenuThree;
    private ProgressBar pBarMenuOne, pBarMenuTwo, pBarMenuThree,pBarLogo;
    private EditText etSearch;
    private ListView llDoctors,lvServices,lvDoctors,lvCommon;
    private Animation slide_up, slide_down;
    private DoctorsAdapter  doctorsAdapter;
    private ServicesAdapter servicesAdapter;
    private DoctorSpecialityDO doctorSpecialityDO;
    SearchHospitalResponce searchHospitalResponce;
    private ImageLoader bubbleimageLoader;
    private DisplayImageOptions bubbleDisplayImageOptions;
    private ArrayList<String> imgList;
    public ArrayList<Doctor_Details> doctorInfo;
    private Double currentlat, currentlong ;
    private DisplayImageOptions getBubbleDIO()
    {
        if(bubbleDisplayImageOptions == null)
            bubbleDisplayImageOptions = getDisplayOptions(R.drawable.pics_samp);
        return bubbleDisplayImageOptions;
    }
    @Override
    public void initialize()
    {
        llHospital = (LinearLayout) getLayoutInflater().inflate(R.layout.hospital_clinic_profiles, null, false);
        llBody.addView(llHospital,baseLayoutParams);
        imgList = new ArrayList<>();
        slide_up            =   AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slide_down          =   AnimationUtils.loadAnimation(mContext, R.anim.slide_down);

        llCommon            =   (LinearLayout)  llHospital.findViewById(R.id.llCommon);
        llClinicThumbnails  =   (LinearLayout)  llHospital.findViewById(R.id.llClinicThumbnails);
        llSearch            =   (LinearLayout)  llHospital.findViewById(R.id.llSearch);
        llDoctor            =   (LinearLayout)  llHospital.findViewById(R.id.llDoctor);
        llServices          =   (LinearLayout)  llHospital.findViewById(R.id.llServices);
        llClinicPhotos      =   (LinearLayout)  llHospital.findViewById(R.id.llClinicPhotos);
        llAvailability      =   (LinearLayout)  llHospital.findViewById(R.id.llAvailability);

        tvHospitalAddress   =   (TextView)      llHospital.findViewById(R.id.tvHospitalAddress);
        tvDistance          =   (TextView)      llHospital.findViewById(R.id.tvDistance);
        tvSeeMoreDoctors    =   (TextView)      llHospital.findViewById(R.id.tvSeeMoreDoctors);
        tvSeeMoreServices   =   (TextView)      llHospital.findViewById(R.id.tvSeeMoreServices);
        tvMenuPicsCount     =   (TextView)      llHospital.findViewById(R.id.tvMenuPicsCount);
        tvNoresult          =   (TextView)      llHospital.findViewById(R.id.tvNoresult);

        ivHospitalPic       =   (ImageView)     llHospital.findViewById(R.id.ivHospitalPic);
        ivMenuOne           =   (ImageView)     llHospital.findViewById(R.id.ivMenuOne);
        ivMenuTwo           =   (ImageView)     llHospital.findViewById(R.id.ivMenuTwo);
        ivMenuThree         =   (ImageView)     llHospital.findViewById(R.id.ivMenuThree);
        mapPreview          =   (ImageView)     llHospital.findViewById(R.id.mapPreview);

        pBarMenuOne         =   (ProgressBar)   llHospital.findViewById(R.id.pBarMenuOne);
        pBarMenuTwo         =   (ProgressBar)   llHospital.findViewById(R.id.pBarMenuTwo);
        pBarMenuThree       =   (ProgressBar)   llHospital.findViewById(R.id.pBarMenuThree);
        pBarLogo            =   (ProgressBar)   llHospital.findViewById(R.id.pBarLogo);

        fMapView            =   (FrameLayout)   llHospital.findViewById(R.id.flMaps);
        mapView             =   (MapView)       llHospital.findViewById(R.id.mapview);


        etSearch            =   (EditText)      llHospital.findViewById(R.id.etSearch);

        lvCommon            =   (ListView)      llHospital.findViewById(R.id.lvCommon);
        lvDoctors           =   (ListView)      llHospital.findViewById(R.id.lvDoctors);
        lvServices          =   (ListView)      llHospital.findViewById(R.id.lvServices);

        setSpecificTypeFace(llHospital, ApplicationConstants.WALSHEIM_MEDIUM);
        tvNoresult.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        mapView.onCreate(savedInstanceState);
        flBottomBar.setVisibility(View.GONE);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        llLocation.setVisibility(View.VISIBLE);
        tvTopTitle.setVisibility(View.GONE);
        llDownarrow.setVisibility(View.GONE);
        tvLocationHeader.setTextSize(16);
        tvLocation.setTextSize(14);
        bubbleimageLoader = initImageLoader(this, R.drawable.pics_samp);

        Preference preference = new Preference(HospitalInfo.this);
        String latlong = preference.getStringFromPreference("CURRENTLATLONG","");
        if(latlong.contains(","))
        {
            currentlat  = Double.parseDouble(latlong.split(",")[0]);
            currentlong = Double.parseDouble(latlong.split(",")[1]);
        }

        doctorInfo = new ArrayList<Doctor_Details>();

        setLayoutParams(ivMenuOne, R.drawable.pics_samp);
        setLayoutParams(ivMenuTwo, R.drawable.pics_samp);
        setLayoutParams(ivMenuThree, R.drawable.pics_samp);
        setLayoutParams(ivHospitalPic, R.drawable.pics_samp);
        Intent in = getIntent();

        if(in.hasExtra("HospitalDetails"))
        {
            doctorSpecialityDO = (DoctorSpecialityDO) in.getSerializableExtra("HospitalDetails");
        }else
        finish();
    /*    mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.getUiSettings().setMyLocationButtonEnabled(false);
                // map.setMyLocationEnabled(true);
                // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                MapsInitializer.initialize(HospitalInfo.this);
            }
        });*/



        callservice(doctorSpecialityDO);




        tvSeeMoreDoctors.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tvLocationHeader.setText("DOCTORS"+"("+doctorInfo.size()+")");
                tvLocation.setText(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalDetails().getHospitalName());
                flBottomBar.setVisibility(View.GONE);
                llClose.setVisibility(View.VISIBLE);
                llBack.setVisibility(View.GONE);
                llCommon.setVisibility(View.VISIBLE);
                llSearch.setVisibility(View.VISIBLE);
                llCommon.startAnimation(slide_up);
                doctorsAdapter = new DoctorsAdapter(HospitalInfo.this,true,doctorInfo,searchHospitalResponce);
                lvCommon.setAdapter(doctorsAdapter);

            }
        });

        tvSeeMoreServices.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tvLocationHeader.setText("SERVICES"+"("+searchHospitalResponce.getRows().get(0).getServices().size()+")");
                tvLocation.setText(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalDetails().getHospitalName());
                flBottomBar.setVisibility(View.GONE);
                llClose.setVisibility(View.VISIBLE);
                llBack.setVisibility(View.GONE);
                llCommon.setVisibility(View.VISIBLE);
                llCommon.startAnimation(slide_up);
                llSearch.setVisibility(View.GONE);
                servicesAdapter = new ServicesAdapter(HospitalInfo.this,true,searchHospitalResponce.getRows().get(0).getServices());
                lvCommon.setAdapter(servicesAdapter);

            }
        });
        ivMenuOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(getMenuImageCount() > 0)
                    startSlideActivity(0,ApplicationConstants.IMG_MENU_TYPE);
            }
        });

        ivMenuOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(getMenuImageCount() > 0)
                    startSlideActivity(0,ApplicationConstants.IMG_MENU_TYPE);
            }
        });
        ivMenuTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(getMenuImageCount() > 1)
                    startSlideActivity(1,ApplicationConstants.IMG_MENU_TYPE);
            }
        });
        ivMenuThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(getMenuImageCount() > 2)
                    startSlideActivity(2,ApplicationConstants.IMG_MENU_TYPE);
            }
        });
        tvMenuPicsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(getMenuImageCount() > 3)
                    startSlideActivity(0,ApplicationConstants.IMG_MENU_TYPE);
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayList<Doctor_Details> filteredPatients = filterListByName(s.toString());
                   doctorsAdapter.refreshAdapterFilteredApplied(filteredPatients);
            }
        });



        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                flBottomBar.setVisibility(View.GONE);
                llClose.setVisibility(View.GONE);
                llBack.setVisibility(View.VISIBLE);
                llCommon.setVisibility(View.GONE);
                llCommon.startAnimation(slide_down);
                tvLocationHeader.setText(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalDetails().getHospitalName());
                tvLocation.setText("");
                //tvLocation.setText(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLocation());
            }
        });

        tvDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Uri location = Uri.parse(String.format("http://maps.google.com/?daddr=%1s,%2s",searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLatitude(),searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLongitude()));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                boolean isIntentSafe = activities.size() > 0;

                if (isIntentSafe) {
                    startActivity(mapIntent);
                }
                else
                {
                   ShowAlertDialog("Whoops!","Doctor Latlongs not available",R.drawable.alert);
                }
            }
        });

    }

    private void callservice(final DoctorSpecialityDO doctorSpecialityDO)
    {
        showLoader("");
        SearchHospitalRequest searchHospitalRequest  = new SearchHospitalRequest();
       // searchHospitalRequest.setHospitalID(546);
       searchHospitalRequest.setHospitalID(doctorSpecialityDO.getHospitalid());
//       searchHospitalRequest.setHospitalLocation(doctorSpecialityDO.getHospitalloacation());
        RestClient.getAPI(Constants.DEVICE_REGISTRATION).getHospitalDetails(searchHospitalRequest, new RestCallback<SearchHospitalResponce>() {
            @Override
            public void success(SearchHospitalResponce searchHospitalResponce1, Response response) {
                hideLoader();
                if (searchHospitalResponce1 != null && searchHospitalResponce1.getRows().size() != 0)
                {
                    searchHospitalResponce = searchHospitalResponce1;
                    if(searchHospitalResponce.getRows().get(0).getHospitalInfos()!=null && searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalDetails()!=null )
                    tvLocationHeader.setText(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalDetails().getHospitalName());
                    tvLocation.setText("");

                    // tvLocation.setText(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLocation());
                    mapView.getMapAsync(new OnMapReadyCallback()
                    {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            map = googleMap;
                            map.getUiSettings().setMyLocationButtonEnabled(false);
                            // map.setMyLocationEnabled(true);
                            // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                            MapsInitializer.initialize(HospitalInfo.this);

                            // Updates the location and zoom of the MapView
                            if (searchHospitalResponce != null && searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress() != null) {
                                String latitude = searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLatitude();
                                String longitude = searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLongitude();
                                LatLng sydney = new LatLng(Float.valueOf(latitude), Float.valueOf(longitude));
                                googleMap.addMarker(new MarkerOptions().position(sydney).title(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalDetails().getHospitalName()));
                                //map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(sydney, 17);
                                googleMap.animateCamera(cameraUpdate);
                            }
                        }
                    });


                    if(searchHospitalResponce.getRows().get(0).getDoctorInfo()!=null && searchHospitalResponce.getRows().get(0).getDoctorInfo().size()!=0)
                    {
                        doctorInfo = searchHospitalResponce.getRows().get(0).getDoctorInfo();
                        if (doctorInfo.size() > 1)
                        {
                            tvSeeMoreDoctors.setVisibility(View.VISIBLE);
                        } else
                        {
                            tvSeeMoreDoctors.setVisibility(View.GONE);
                        }

                        doctorsAdapter = new DoctorsAdapter(HospitalInfo.this, false, doctorInfo, searchHospitalResponce);
                        lvDoctors.setAdapter(doctorsAdapter);
                        Utils.setListViewHeightBasedOnChildren(lvDoctors);
                    }
                    else
                    {
                        llDoctor.setVisibility(View.GONE);
                    }
                    if(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getHospitalDocuments()!=null && searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getHospitalDocuments().size()!=0)
                    loadClinicImages( searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getHospitalDocuments());
                    else
                        llClinicPhotos.setVisibility(View.GONE);

                    if(searchHospitalResponce.getRows().get(0).getHospitalInfos()!=null && searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress()!=null)
                    {
                        HospitalAddress  hospitalAddress = searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress();
                        tvHospitalAddress.setText(hospitalAddress.getAddress2());
                        String Distance;

                       if (hospitalAddress.getLatitude()!=null && hospitalAddress.getLongitude()!=null)
                       {
                           Distance = getDistance(searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLatitude(), searchHospitalResponce.getRows().get(0).getHospitalInfos().getHospitalAddress().getLongitude());

                           if (Distance != null && Distance.length() != 0) {
                               if (Distance.length() >= 4)
                                   tvDistance.setText(Distance.substring(0, 4) + " kms" + " - from you");
                               else
                                   tvDistance.setText(Distance + " kms" + " - from you");
                           } else
                               tvDistance.setVisibility(View.GONE);
                       }
                    }

                    if(searchHospitalResponce.getRows().get(0).getServices()!=null && searchHospitalResponce.getRows().get(0).getServices().size()!=0)
                    {
                        servicesAdapter = new ServicesAdapter(HospitalInfo.this,false,searchHospitalResponce.getRows().get(0).getServices());
                        lvServices.setAdapter(servicesAdapter);
                        Utils.setListViewHeightBasedOnChildren(lvServices);
                    }
                    else
                        llServices.setVisibility(View.GONE);


                }
                else
                    llAvailability.setVisibility(View.VISIBLE);
            }
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }
        });
    }
    private void loadClinicImages(ArrayList<HospitalDocuments> clinicDocs)
    {    String imagePath = "";
        int count = 0;
        boolean isLogo = false;
        for(int i = 0; i < clinicDocs.size(); i++)
        {
            if (!clinicDocs.get(i).getFileName().equalsIgnoreCase("Logo")) {
                count++;
                imagePath = clinicDocs.get(i).getFileURL();
                prepareMenuView(imagePath, count);
                imgList.add(imagePath);
            }else
            {
                isLogo = true;
                Picasso.with(HospitalInfo.this).load(clinicDocs.get(i).getFileURL()).into(ivHospitalPic);
            }
            if(!isLogo)
            {
                ivHospitalPic.setVisibility(View.GONE);
            }

        }

    }
    private void prepareMenuView(String iPath, int menuPhotoCount)
    {
        if (menuPhotoCount == 1)
            bubbleimageLoader.displayImage(iPath, ivMenuOne, getBubbleDIO(), new CustomSimpleImageLoadingListener(pBarMenuOne));
        else if (menuPhotoCount == 2)
            bubbleimageLoader.displayImage(iPath, ivMenuTwo, getBubbleDIO(), new CustomSimpleImageLoadingListener(pBarMenuTwo));
        else if (menuPhotoCount == 3)
            bubbleimageLoader.displayImage(iPath, ivMenuThree, getBubbleDIO(), new CustomSimpleImageLoadingListener(pBarMenuThree));

        if (menuPhotoCount > 3) {
            tvMenuPicsCount.setVisibility(View.VISIBLE);
            tvMenuPicsCount.setText("+" + String.valueOf(menuPhotoCount - 3));
        }
        else
        {
            tvMenuPicsCount.setVisibility(View.GONE);
        }
    }
    public void setLayoutParams(View v, int drawableId)
    {
        int [] dDimens = getDrawableDimensions(v.getContext(), drawableId);
        v.getLayoutParams().width = dDimens[0];
        v.getLayoutParams().height = dDimens[1];
    }
    public int[] getDrawableDimensions(Context context, int id)
    {
        int[] dimensions = new int[2];
        if(context == null)
            return dimensions;
        Drawable d = getResources().getDrawable(id);
        if(d != null)
        {
            dimensions[0] = d.getIntrinsicWidth();
            dimensions[1] = d.getIntrinsicHeight();
        }
        return dimensions;
    }
    private int getMenuImageCount()
    {
        return imgList.size();
    }

    private void startSlideActivity(int position, int imgTypeId)
    {
        if(position == -1)
            return;
        Intent SlideActivity = new Intent(HospitalInfo.this, FullScreenSlideActivity.class);
        SlideActivity.putExtra("position", position);
        SlideActivity.putExtra("ImageTypeId", imgTypeId);
        SlideActivity.putStringArrayListExtra("imageData", imgList);
        startActivity(SlideActivity);
    }
    private String getDistance(String eLatitude, String eLongitude){

        double latitude = getDouble(eLatitude);
        double longitude = getDouble(eLongitude);

        String distance = "NA";
        if(currentlat == null || currentlong == null || (latitude == -1d || longitude == -1d))
            return distance;
        Location locationA = new Location("point A");

        locationA.setLatitude(currentlat);
        locationA.setLongitude(currentlong);

        Location locationB = new Location("point B");

        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);

        float distanceValue = locationA.distanceTo(locationB);
        Float obj = distanceValue/1000;
        if(obj.floatValue() < 100.0f)
        {
            distance = "~" + String.valueOf(distanceValue / 1000).substring(0, 4) + " kms";
        }
        else
        {
            distance = "";
        }
        return distance;
    }
    private double getDouble(String doubleString)
    {
        double doubleValue = -1d;
        try{
            doubleValue = Double.parseDouble(doubleString);
        }
        catch (Exception e){}
        return doubleValue;
    }
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    public ArrayList<Doctor_Details> filterListByName(String filterStr)
    {
        ArrayList<Doctor_Details> tempSlotList = new ArrayList<>();
        for (int i = 0; i < doctorInfo.size(); i ++){
            Doctor_Details temp = new Doctor_Details();
            temp = doctorInfo.get(i);
            String fullName = temp.getFullName().toLowerCase();
            if (fullName.contains(filterStr.toLowerCase())){
                tempSlotList.add(temp);
            }
        }
        return tempSlotList;
    }
}
