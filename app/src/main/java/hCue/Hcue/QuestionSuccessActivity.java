package hCue.Hcue;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import hCue.Hcue.utils.ApplicationConstants;


/**
 * Created by shyamprasadg on 03/06/16.
 */
public class QuestionSuccessActivity extends BaseActivity implements View.OnClickListener
{

    private LinearLayout llQuestionSuccess;
    private ImageView ivOk;
    private TextView tvTnq, tvSuccess, tvPlsWait;

    @Override
    public void initialize() {

        llQuestionSuccess = (LinearLayout) getLayoutInflater().inflate(R.layout.question_success, null, false);
        llBody.addView(llQuestionSuccess, baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);

        llTop.setVisibility(View.GONE);
        initViewControls(llQuestionSuccess);
        bindControls();

    }

    public void initViewControls(View view){

        ivOk                                = (ImageView) view.findViewById(R.id.ivOk);
        tvTnq                               = (TextView) view.findViewById(R.id.tvTnq);
        tvSuccess                           = (TextView) view.findViewById(R.id.tvSuccess);
        tvPlsWait                           = (TextView) view.findViewById(R.id.tvPlsWait);

    }

    public void bindControls(){
        ivOk.setOnClickListener(this);

        tvTnq.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvSuccess.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvPlsWait.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ivOk:
                Intent in = new Intent(QuestionSuccessActivity.this, MyQuestions.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
                break;
        }
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finishActivity();
    }

    public void finishActivity(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
    }
}
