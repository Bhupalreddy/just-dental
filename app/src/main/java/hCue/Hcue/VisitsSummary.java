package hCue.Hcue;

import android.support.v4.widget.DrawerLayout;
import android.view.Display;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.DoctorSpecialisation;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.adapters.ExpAdapter;


/**
 * Created by Bharath on 10/6/2016.
 */

public class VisitsSummary extends BaseActivity
{
    private LinearLayout svVisits;
    private PatientCaseRow patientCaseRow ;
    private TextView tvDoctorname,tvDate,tvTime,tvspeciality,tvClinic;
    private ExpandableListView elvCaseHistory;


    @Override
    public void initialize() {
        svVisits    =   (LinearLayout) getLayoutInflater().inflate(R.layout.vitalsummary,null,false);
        llBody.addView(svVisits);

        if(getIntent().hasExtra("SELECTEDCASE"))
            patientCaseRow = (PatientCaseRow) getIntent().getSerializableExtra("SELECTEDCASE");

        tvDoctorname        =   (TextView)  svVisits.findViewById(R.id.tvDoctorname);
        tvDate              =   (TextView)  svVisits.findViewById(R.id.tvDate);
        tvTime              =   (TextView)  svVisits.findViewById(R.id.tvTime);
        tvspeciality        =   (TextView)  svVisits.findViewById(R.id.tvspeciality);
        tvClinic            =   (TextView)  svVisits.findViewById(R.id.tvClinic);
        elvCaseHistory      =    (ExpandableListView) svVisits.findViewById(R.id.elvCaseHistory);




        //.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        llBack.setVisibility(View.VISIBLE);
        tvTopTitle.setText("VISITS");
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(patientCaseRow != null)
        {
            if(patientCaseRow.getListDoctorDetails().getDoctorName().startsWith("Dr"))
                tvDoctorname.setText(patientCaseRow.getListDoctorDetails().getDoctorName());
            else
                tvDoctorname.setText("Dr."+patientCaseRow.getListDoctorDetails().getDoctorName());
            tvClinic.setText(patientCaseRow.getListDoctorDetails().getClinicName());
            StringBuilder specialitybuilder = new StringBuilder();
            for(DoctorSpecialisation doctorSpecialisation : patientCaseRow.getListDoctorDetails().getListDoctorSpecolisations())
                specialitybuilder.append(doctorSpecialisation.getSepcialityDesc()+", ");

            if(!specialitybuilder.toString().isEmpty())
                tvspeciality.setText(specialitybuilder.toString().substring(0,specialitybuilder.toString().length()-2));
            if(patientCaseRow.getConsultationDt()!=0L){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyy");
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setTimeInMillis(patientCaseRow.getConsultationDt());
                tvDate.setText(simpleDateFormat.format(calendar.getTime()));
                tvTime.setText(patientCaseRow.getStartTime() + " Hrs");
            }
        }

        ExpAdapter expAdapter = new ExpAdapter(VisitsSummary.this,patientCaseRow.getListCaseHistory(), patientCaseRow.getPrintURLses());
        elvCaseHistory.setAdapter(expAdapter);

        if(patientCaseRow.getListCaseHistory().size()==1)
            elvCaseHistory.expandGroup(0);

        Display newDisplay = getWindowManager().getDefaultDisplay();
        int width = newDisplay.getWidth();
        elvCaseHistory.setIndicatorBounds(width-50, width);


    }
}
