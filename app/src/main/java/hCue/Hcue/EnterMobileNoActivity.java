package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Random;

import hCue.Hcue.DAO.PatientPhone;
import hCue.Hcue.DAO.RegisterReqSingleton;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.model.OTPRequest;
import hCue.Hcue.model.OTPResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 03/06/16.
 */
public class EnterMobileNoActivity extends BaseActivity
{
    private LinearLayout llMobileNo ;
    private EditText edtMobileNo;
    private TextView tvContinue;
    private RippleView rvContinue;

    @Override
    public void initialize()
    {
        llMobileNo  =   (LinearLayout) getLayoutInflater().inflate(R.layout.enter_mobile_no,null,false);
        llBody.addView(llMobileNo,baseLayoutParams);

        edtMobileNo             = (EditText)    llMobileNo.findViewById(R.id.edtMobileNo);

        tvContinue              = (TextView)    llMobileNo.findViewById(R.id.tvContinue);

        rvContinue              = (RippleView)  llMobileNo.findViewById(R.id.rvContinue);

        tvTopTitle.setText("MOBILE NUMBER");

        llLeftMenu.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        setSpecificTypeFace(llMobileNo,ApplicationConstants.WALSHEIM_MEDIUM);

        tvContinue.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView)
            {
                validateMobileNumber(edtMobileNo.getText().toString().trim());
            }
        });

//        tvContinue.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//
//
//            }
//        });
    }

    private void validateMobileNumber(String mobilenumber)
    {
        if(mobilenumber.isEmpty())
        {
            ShowAlertDialog("Alert","Mobile number should not be empty.",R.drawable.alert);
        }else if(mobilenumber.length() !=10)
        {
            ShowAlertDialog("Alert","Mobile number should be 10 digits.",R.drawable.alert);
        }else
        {
            callGetPatientData(mobilenumber);
        }
    }

    private void callGetPatientData(final String mobilenumber)
    {

        showLoader("");
        GetPatientsRequest patientsRequest = new GetPatientsRequest();

        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(patientsRequest, new RestCallback<GetPatientsResponse>() {
            @Override
            public void failure(RestError restError)
            {
               hideLoader();
            }

            @Override
            public void success(GetPatientsResponse getPatientsResponse, Response response) {
                hideLoader();
                if(getPatientsResponse != null)
                {
                    if(getPatientsResponse.getCount()==0)
                    {
                        int otp = generateRandom(6);
                        OTPRequest otpRequest = new OTPRequest();
                        otpRequest.setMailContent(""+otp);
                        otpRequest.setText("Your hcue OTP is "+otp);
                        otpRequest.setUSRId("0");
                        otpRequest.setTo("91"+mobilenumber);
                        otpRequest.setToAddress("");
                        callSendOTP(otpRequest,mobilenumber);
                    }else
                    {
                        int otp = generateRandom(6);
                        OTPRequest otpRequest = new OTPRequest();
                        otpRequest.setMailContent(""+otp);
                        otpRequest.setText("Your hcue OTP is "+otp);
                        otpRequest.setUSRId(""+getPatientsResponse.getPatientRows().get(0).getArrPatients().get(0).getPatientID());
                        otpRequest.setTo("91"+mobilenumber);
                        otpRequest.setToAddress("");
                        callSendOTP(otpRequest,mobilenumber);
                    }
                }
            }
        });
    }

    private void callSendOTP(final OTPRequest otpRequest, final String mobilenumber)
    {
        RestClient.getAPI("https://api.checkmobi.com").sendOPT(otpRequest, new RestCallback<OTPResponse>() {
            @Override
            public void failure(RestError restError)
            {
                ShowAlertDialog("Whoops!","Failed to send OTP please try again.",R.drawable.worng);
            }

            @Override
            public void success(OTPResponse otpResponse, Response response) {

                PatientPhone patientPhone = new PatientPhone();
                patientPhone.setPhNumber(Long.valueOf(mobilenumber));
                patientPhone.setPhType('M');
                patientPhone.setPrimaryIND('Y');
                patientPhone.setPhAreaCD(Integer.parseInt(mobilenumber.substring(4,10)));
                patientPhone.setPhCntryCD(91);
                patientPhone.setPhStateCD(Integer.parseInt(mobilenumber.substring(0,4)));

                ArrayList<PatientPhone> patientPhones = new ArrayList<PatientPhone>();
                patientPhones.add(patientPhone);
                RegisterReqSingleton.getSingleton().setPatientPhones(patientPhones);
                RegisterReqSingleton.getSingleton().getPatientDetails2().setMobileID(Long.valueOf(mobilenumber));

                Intent slideactivity = new Intent(EnterMobileNoActivity.this, EnterOtpActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                slideactivity.putExtra("OTPREQUEST",otpRequest);
                slideactivity.putExtra("OTPVALUE",otpRequest.getMailContent());
                startActivity(slideactivity, bndlanimation);

            }
        });
    }

    public int generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Integer.parseInt(new String(digits));
    }

}
