package hCue.Hcue.gps.lib;


import hCue.Hcue.gps.common.GPSErrorCode;

public interface GPSCallback
{
    public abstract void gotGpsValidationResponse(Object response, GPSErrorCode code);
}
