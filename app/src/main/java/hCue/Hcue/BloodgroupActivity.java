package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.PatientDetails3;
import hCue.Hcue.DAO.PatientOtherDetails;
import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.PatientVitals;
import hCue.Hcue.DAO.RegisterReqSingleton;
import hCue.Hcue.DAO.Vitals;
import hCue.Hcue.DAO.patientLogin;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.model.RegisterRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;


/**
 * Created by shyamprasadg on 03/06/16.
 */
public class BloodgroupActivity extends BaseActivity
{
    private LinearLayout llBloodgroup ;
    private Button btnOplus,btnOminus,btnAplus,btnAminus,btnBplus,btnBminus,btnABplus,btnABminus,btnDont,btnIntrested,btnNotNow;
    private TextView tvContinue;
    private RippleView rvContinue;
    private String selectedbloodgrp ;
    private String isintrested ;
    private boolean isRegisterSucess = false;
    private boolean ispatinetupdate ;
    private PatientUpdateRequest patientUpdateRequest ;
    private PatientRow selectedpatinet ;
    @Override
    public void initialize()
    {
        llBloodgroup    =   (LinearLayout) getLayoutInflater().inflate(R.layout.blood_group,null,false);
        llBody.addView(llBloodgroup,baseLayoutParams);

        ispatinetupdate = getIntent().getBooleanExtra("UPDATEPATINET",false);
        selectedpatinet = (PatientRow) getIntent().getSerializableExtra("SELECTEDPATIENT");

        btnOplus        = (Button)      llBloodgroup.findViewById(R.id.btnOplus);
        btnOminus       = (Button)      llBloodgroup.findViewById(R.id.btnOminus);
        btnAplus        = (Button)      llBloodgroup.findViewById(R.id.btnAplus);
        btnAminus       = (Button)      llBloodgroup.findViewById(R.id.btnAminus);
        btnBplus        = (Button)      llBloodgroup.findViewById(R.id.btnBplus);
        btnBminus       = (Button)      llBloodgroup.findViewById(R.id.btnBminus);
        btnABplus       = (Button)      llBloodgroup.findViewById(R.id.btnABplus);
        btnABminus      = (Button)      llBloodgroup.findViewById(R.id.btnABminus);
        btnDont         = (Button)      llBloodgroup.findViewById(R.id.btnDont);
        btnIntrested    = (Button)      llBloodgroup.findViewById(R.id.btnIntrested);
        btnNotNow       = (Button)      llBloodgroup.findViewById(R.id.btnNotNow);

        rvContinue      = (RippleView)  llBloodgroup.findViewById(R.id.rvContinue);

        tvContinue      = (TextView)    llBloodgroup.findViewById(R.id.tvContinue);

        setSpecificTypeFace(llBloodgroup,ApplicationConstants.WALSHEIM_MEDIUM);

        tvContinue.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        tvTopTitle.setText("YOUR BLOOD GROUP");
        llLeftMenu.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        btnOplus.setTag("0");
        btnOminus.setTag("0");
        btnAplus.setTag("0");
        btnAminus.setTag("0");
        btnBplus.setTag("0");
        btnBminus.setTag("0");
        btnABplus.setTag("0");
        btnABminus.setTag("0");
        btnDont.setTag("0");
        btnIntrested.setTag("0");
        btnNotNow.setTag("0");

        if(ispatinetupdate)
        {
            prepareRequest(RegisterReqSingleton.getSingleton());
        }

        final String bloodgroup;
        btnOplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnOplus.getText().toString();
                btnOplus.setTag("Selected");
                btnOplus.setTextColor(getResources().getColor(R.color.white));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));





            }
        });


        btnOminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnOminus.getText().toString();

                btnOminus.setTag("Selected");
                btnOminus.setTextColor(getResources().getColor(R.color.white));

                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));





            }
        });

        btnAplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnAplus.getText().toString();

                btnAplus.setTag("Selected");
                btnAplus.setTextColor(getResources().getColor(R.color.white));

                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnAminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnAminus.getText().toString();

                btnAminus.setTag("Selected");
                btnAminus.setTextColor(getResources().getColor(R.color.white));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnBplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnBplus.getText().toString();

                btnBplus.setTag("Selected");
                btnBplus.setTextColor(getResources().getColor(R.color.white));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnBminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnBminus.getText().toString();

                btnBminus.setTag("Selected");
                btnBminus.setTextColor(getResources().getColor(R.color.white));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });


        btnABplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnABplus.getText().toString();

                btnABplus.setTag("Selected");
                btnABplus.setTextColor(getResources().getColor(R.color.white));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        btnABminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnABminus.getText().toString();

                btnABminus.setTag("Selected");
                btnABminus.setTextColor(getResources().getColor(R.color.white));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnDont.setTag("0");
                btnDont.setTextColor(getResources().getColor(R.color.black));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });
        btnDont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedbloodgrp = btnDont.getText().toString();
                btnDont.setTag("Selected");
                btnDont.setTextColor(getResources().getColor(R.color.white));
                btnDont.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnOplus.setTag("0");
                btnOplus.setTextColor(getResources().getColor(R.color.black));
                btnOplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnOminus.setTag("0");
                btnOminus.setTextColor(getResources().getColor(R.color.black));
                btnOminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAplus.setTag("0");
                btnAplus.setTextColor(getResources().getColor(R.color.black));
                btnAplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnAminus.setTag("0");
                btnAminus.setTextColor(getResources().getColor(R.color.black));
                btnAminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBplus.setTag("0");
                btnBplus.setTextColor(getResources().getColor(R.color.black));
                btnBplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnBminus.setTag("0");
                btnBminus.setTextColor(getResources().getColor(R.color.black));
                btnBminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABplus.setTag("0");
                btnABplus.setTextColor(getResources().getColor(R.color.black));
                btnABplus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));

                btnABminus.setTag("0");
                btnABminus.setTextColor(getResources().getColor(R.color.black));
                btnABminus.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });


        btnIntrested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isintrested = "Y";
                btnIntrested.setTag("Selected");
                btnIntrested.setTextColor(getResources().getColor(R.color.white));
                btnIntrested.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnNotNow.setTag("0");
                btnNotNow.setTextColor(getResources().getColor(R.color.black));
                btnNotNow.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });


        btnNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isintrested = "N";
                btnNotNow.setTag("Selected");
                btnNotNow.setTextColor(getResources().getColor(R.color.white));
                btnNotNow.setBackground(getResources().getDrawable(R.drawable.hcue_greenroundbutton));

                btnIntrested.setTag("0");
                btnIntrested.setTextColor(getResources().getColor(R.color.black));
                btnIntrested.setBackground(getResources().getDrawable(R.drawable.hcue_button_borderinput));


            }
        });

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView)
            {
                if(selectedbloodgrp == null)
                {
                    ShowAlertDialog("Please select blood group.");
                }else if(isintrested == null)
                {
                    ShowAlertDialog("Please select want to be blood Donor.");
                }else
                {
                    PatientVitals patientVitals = new PatientVitals();
                    Vitals vitals = new Vitals();
                    vitals.setBLG(selectedbloodgrp);
                    patientVitals.setVitals(vitals);

                    PatientOtherDetails patientOtherDetails = new PatientOtherDetails();
                    PatientOtherDetails.Preference preference = patientOtherDetails. new Preference();
                    preference.setBloodDonor(isintrested);
                    patientOtherDetails.setPreference(preference);
                    if(!ispatinetupdate) {
                        RegisterRequest registerRequest = RegisterReqSingleton.getSingleton();
                        registerRequest.setUSRId(registerRequest.getPatientDetails2().getMobileID());
                        registerRequest.getPatientDetails2().setOtherDetails(patientOtherDetails);
                        registerRequest.setPatientVitals(patientVitals);
                        showLoader("");
                        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).patientRegister(registerRequest, new RestCallback<LoginResponse>() {
                            @Override
                            public void failure(RestError restError) {
                                hideLoader();
                                Log.e("ERROR", restError.getErrorMessage());
                                ShowAlertDialog("Registration failed.");
                            }

                            @Override
                            public void success(LoginResponse loginResponse, Response response) {
                                hideLoader();
                                if(loginResponse !=null && loginResponse.getArrPatients()!=null && !loginResponse.getArrPatients().isEmpty()) {
                                    LoginResponseSingleton.getSingleton(loginResponse, BloodgroupActivity.this);
                                    isRegisterSucess = true;
                                    ShowAlertDialog("Successfully Registered.");
                                }else
                                {
                                    ShowAlertDialog("Registration failed.");
                                }

                            }
                        });
                    }else
                    {
                        patientUpdateRequest.setPatientVitals(patientVitals);
                        patientUpdateRequest.getPatientDetails3().setOtherDetails(patientOtherDetails);
                        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).updatePatient(patientUpdateRequest, new RestCallback<LoginResponse>()
                        {
                            @Override
                            public void failure(RestError restError)
                            {
                                hideLoader();
                                ShowAlertDialog("Failed to update please try again.");
//                Toast.makeText(MyProfileActivity.this,"Failed to update please try again.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void success(LoginResponse loginResponse, Response response)
                            {
                                hideLoader();
                                if(loginResponse !=null && loginResponse.getArrPatients()!=null && !loginResponse.getArrPatients().isEmpty()) {
                                    LoginResponseSingleton.getSingleton(loginResponse, BloodgroupActivity.this);
                                    isRegisterSucess = true;
                                    ShowAlertDialog("Register success.");
                                }else
                                {
                                    ShowAlertDialog("Registration failed.");
                                }
                            }
                        });
                    }

                }
            }
        });

//        tvContinue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                if(selectedbloodgrp == null)
//                {
//                    ShowAlertDialog("Please select blood group.");
//                }else if(isintrested == null)
//                {
//                    ShowAlertDialog("Please select want to be blood Donor.");
//                }else
//                {
//                    PatientVitals patientVitals = new PatientVitals();
//                    Vitals vitals = new Vitals();
//                    vitals.setBLG(selectedbloodgrp);
//                    patientVitals.setVitals(vitals);
//                    RegisterReqSingleton.getSingleton().setPatientVitals(patientVitals);
//
//                    Intent slideactivity = new Intent(BloodgroupActivity.this, BloodgroupActivity.class);
//                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
//                    startActivity(slideactivity, bndlanimation);
//
//                }
//
//            }
//        });
    }
    public void ShowAlertDialog(String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BloodgroupActivity.this);
        View dialogView = LayoutInflater.from(BloodgroupActivity.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        tvInfo.setTextSize(18);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.GONE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
                if(isRegisterSucess) {
                    Preference preference = new Preference(BloodgroupActivity.this);
                    preference.saveBooleanInPreference("ISLOGOUT", false);
                    preference.commitPreference();

                    Intent slideactivity = new Intent(BloodgroupActivity.this, Specialities.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(slideactivity, bndlanimation);
                    finishAffinity();
                }
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }

    private void prepareRequest(RegisterRequest singleton)
    {
        patientUpdateRequest = new PatientUpdateRequest();
        patientUpdateRequest.setListPatientAddress(singleton.getPatientAddresses());
        patientUpdateRequest.setListPatientEmail(singleton.getPatientEmails());
        patientUpdateRequest.setListPatientPhone(singleton.getPatientPhones());
        patientUpdateRequest.setUSRId(selectedpatinet.getArrPatients().get(0).getPatientID());

        patientLogin patientLogin= new patientLogin();
        patientLogin.setLoginID(singleton.getPatientDetails2().getPatientLoginID());
        patientLogin.setPassword(singleton.getPatientDetails2().getPatientPassword());

        PatientDetails3 patientDetails3 = new PatientDetails3();
        patientDetails3.setPatientID(selectedpatinet.getArrPatients().get(0).getPatientID());
        patientDetails3.setFirstName(selectedpatinet.getArrPatients().get(0).getFullName());
        patientDetails3.setFullName(selectedpatinet.getArrPatients().get(0).getFullName());
        //patientDetails3.setAge(loginResponse.getArrPatients().get(0).getAge());
        patientDetails3.setGender(singleton.getPatientDetails2().getGender()+"");
        Calendar calendar1  = Calendar.getInstance(Locale.getDefault());
        calendar1.setTimeInMillis(selectedpatinet.getArrPatients().get(0).getDOB());
        patientDetails3.setDOB(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()));

        if(selectedpatinet.getArrPatients().get(0).getArrPatientOtherDetails() != null && !selectedpatinet.getArrPatients().get(0).getArrPatientOtherDetails().isEmpty())
            patientDetails3.setOtherDetails(selectedpatinet.getArrPatients().get(0).getArrPatientOtherDetails().get(0));

        if(selectedpatinet.getArrPatients().get(0).getEmergencyInfo() != null)
            patientDetails3.setEmergencyInfo(selectedpatinet.getArrPatients().get(0).getEmergencyInfo());

        //EmergencyInfo emergencyInfo = new EmergencyInfo();
        patientUpdateRequest.setPatientLogin(patientLogin);
        patientUpdateRequest.setPatientDetails3(patientDetails3);
    }

}
