package hCue.Hcue;

import android.app.ActivityOptions;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hCue.Hcue.DAO.PatientDetails2;
import hCue.Hcue.DAO.PatientEmail;
import hCue.Hcue.DAO.PatientPhone;
import hCue.Hcue.DAO.RegisterReqSingleton;
import hCue.Hcue.adapters.Genderadapter;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.model.OTPRequest;
import hCue.Hcue.model.OTPResponse;
import hCue.Hcue.model.RegisterRequest;
import hCue.Hcue.model.VerifyPatientReq;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateHelper;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;


/**
 * Created by Appdest on 02-06-2016.
 */
public class RegisterActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener
{

    private LinearLayout llRegister, llTitle, llDob, llPassword;
    private EditText     edtusername, edtemailid, edtpassword, edtconfirmpassword,edtPhone;
    private Button       btnContinue;
    private Spinner spinner;
    private RippleView rvRegister;
    private TextView tvTitle, edtDob;
    private PopupWindow popupWindow ;
    private ArrayList<String> gender;
    private boolean isGplusLogin = false ;
    private String GOOGLEID ;
    private TextInputLayout tvName,tvEmail,tvMobileNo,tvPassword,tvConfirmPassword;

    @Override
    public void initialize()
    {
        llRegister  =   (LinearLayout) getLayoutInflater().inflate(R.layout.register,null,false);
        llBody.addView(llRegister,baseLayoutParams);

        edtDob                  =   (TextView)          llRegister.findViewById(R.id.edtDob);
        edtPhone                =   (EditText)          llRegister.findViewById(R.id.edtPhone);
        edtusername             =   (EditText)          llRegister.findViewById(R.id.edtusername);
        edtemailid              =   (EditText)          llRegister.findViewById(R.id.edtemailid);
        edtpassword             =   (EditText)          llRegister.findViewById(R.id.edtpassword);
        edtconfirmpassword      =   (EditText)          llRegister.findViewById(R.id.edtconfirmpassword);

        spinner                 =   (Spinner)           llRegister.findViewById(R.id.spinner);

        btnContinue             =   (Button)            llRegister.findViewById(R.id.btnContinue);

        rvRegister              =   (RippleView)        llRegister.findViewById(R.id.rvRegister);

        llTitle                 =   (LinearLayout)      llRegister.findViewById(R.id.llTitle);
        llDob                   =   (LinearLayout)      llRegister.findViewById(R.id.llDob);
        llPassword              =   (LinearLayout)      llRegister.findViewById(R.id.llPassword);

        tvTitle                 =   (TextView)          llRegister.findViewById(R.id.tvTitle);
        tvName                  =   (TextInputLayout)   llRegister.findViewById(R.id.tvName);
        tvEmail                 =   (TextInputLayout)   llRegister.findViewById(R.id.tvEmail);
        tvMobileNo              =   (TextInputLayout)   llRegister.findViewById(R.id.tvMobileNo);
        tvPassword              =   (TextInputLayout)   llRegister.findViewById(R.id.tvPassword);
        tvConfirmPassword       =   (TextInputLayout)   llRegister.findViewById(R.id.tvConfirmPassword);

        setSpecificTypeFace(llRegister,ApplicationConstants.WALSHEIM_MEDIUM);

        tvName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvPassword.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvConfirmPassword.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvMobileNo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvEmail.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        btnContinue.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        Intent in = getIntent();
        if(in!=null)
        {
            edtusername.setText(in.getStringExtra("Name"));
            edtemailid.setText(in.getStringExtra("Email"));
            if(in.getStringExtra("Gender")!=null) {
                if (in.getStringExtra("Gender").equalsIgnoreCase("0"))
                    spinner.setSelection(0);
                else
                    spinner.setSelection(1);
            }
            GOOGLEID = in.getStringExtra("GOOGLEID");
        }
        if(in.hasExtra("isGplusLogin"))
        {
            isGplusLogin = true;
        }else
        {
            llPassword.setVisibility(View.VISIBLE);
        }

        tvTopTitle.setText("REGISTER");

        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.GONE);
        llClose.setVisibility(View.VISIBLE);
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        gender = new ArrayList<>();
        gender.add("Male");
        gender.add("Female");
        Genderadapter genderadapter  = new Genderadapter(RegisterActivity.this,gender);
        spinner.setAdapter(genderadapter);
        spinner.setSelection(0);


        llClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent slideactivity = new Intent(RegisterActivity.this, LaunchActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_down_in,R.anim.push_down_out).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

        edtDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, RegisterActivity.this,  yy, mm, dd);
                //date is dateSetListener as per your code in question

                //   SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });



        llTitle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showDropdownTitles();
            }
        });

rvRegister.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
    @Override
    public void onComplete(RippleView rippleView)
    {
        validateFields();
    }
});

//        btnContinue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//
//
//            }
//        });
    }

    private void validateFields() {
       /* if(tvTitle.getTag() == null)
        {
//            ShowAlertDialog("Alert","Please select title.",R.drawable.alret);
        }else*/
        if (edtusername.getText().toString().trim().isEmpty())
        {
            ShowAlertDialog("Alert","Username should not be empty.",R.drawable.alert);
        } else if(edtDob.getTag() == null)
        {
            ShowAlertDialog("Alert","Please select the date of birth.",R.drawable.alert);
        }else  if(edtPhone.getText().toString().isEmpty())
        {
            ShowAlertDialog("Alert","Mobile number should not be empty.",R.drawable.alert);
        }else if(edtPhone.getText().toString().length() !=10)
        {
            ShowAlertDialog("Alert","Mobile number should be 10 digits.",R.drawable.alert);
        }
        else if (edtemailid.getText().toString().trim().isEmpty())
        {
            ShowAlertDialog("Alert","Email should not be empty.",R.drawable.alert);
        } else if (!isValidEmail(edtemailid.getText().toString().trim()))
        {
            ShowAlertDialog("Alert","Please enter a valid emailid.",R.drawable.alert);
        } else
        if(!isGplusLogin)
        {
            if (edtpassword.getText().toString().trim().isEmpty())

            {
                ShowAlertDialog("Alert", "Enter password should not be empty.", R.drawable.alert);
            } else if (edtconfirmpassword.getText().toString().trim().isEmpty()) {
                ShowAlertDialog("Alert", "Confirm password should not be empty.", R.drawable.alert);
            } else if (edtconfirmpassword.getText().toString().trim().length() < 6) {
                ShowAlertDialog("Alert", "Password must Contain atleast 6 Characters", R.drawable.alert);
            } else if (!edtconfirmpassword.getText().toString().trim().equals(edtpassword.getText().toString().trim())) {
                ShowAlertDialog("Alert", "Password, Confirm password should be same.", R.drawable.alert);
            }else
            {
                callRegisterPatient();
            }
        }
        else
        {
            callRegisterPatient();
        }
    }

    private void callRegisterPatient() {
        VerifyPatientReq verifyPatientReq = new VerifyPatientReq();
        verifyPatientReq.setPatientLoginID(edtemailid.getText().toString().trim());
        verifyPatientReq.setPhoneNumber(Long.valueOf(edtPhone.getText().toString()));
        showLoader("");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).verifyPatientLogin(verifyPatientReq, new RestCallback<JsonObject>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
            }
            @Override
            public void success(JsonObject jsonObject, Response response) {
                hideLoader();
                if(jsonObject != null)
                {
                    if(jsonObject.has("Message") && jsonObject.has("EmailID") )
                    {
                        isFromRegisterfail = true;
                        ShowAlertDialog("Alert","The given mobile number is already registered with "+jsonObject.get("EmailID").getAsString(),R.drawable.alert);
                    }else if(jsonObject.has("Message"))
                    {
                        String value = jsonObject.get("Message").getAsString();
                        String[] items = {"Mr", "Miss" , "Baby"};
                        if (value.equalsIgnoreCase("New User"))
                        {
                            PatientDetails2 patientDetails2 = new PatientDetails2();
                            patientDetails2.setFirstName(edtusername.getText().toString());
                            patientDetails2.setFullName(edtusername.getText().toString());
                            patientDetails2.setPatientLoginID(edtemailid.getText().toString().trim());
                            if(!isGplusLogin)
                            patientDetails2.setPatientPassword(edtpassword.getText().toString().trim());
                            patientDetails2.setMobileID(Long.valueOf(edtPhone.getText().toString()));
                            patientDetails2.setDOB((String) edtDob.getTag());

                            int age = DateHelper.getAge((Date) llDob.getTag());
                            if(age >3)
                            {
                                if (spinner.getSelectedItemPosition() == 0) {
                                    patientDetails2.setTitle(items[0]);
                                    patientDetails2.setGender('M');
                                } else {
                                    patientDetails2.setTitle(items[1]);
                                    patientDetails2.setGender('F');
                                }
                            }else
                            {
                                if (spinner.getSelectedItemPosition() == 0)
                                {
                                    patientDetails2.setGender('M');
                                } else
                                {
                                    patientDetails2.setGender('F');
                                }
                                patientDetails2.setTitle(items[2]);
                            }

                            PatientEmail patientEmail = new PatientEmail();
                            patientEmail.setEmailID(edtemailid.getText().toString().trim());
                            patientEmail.setEmailIDType("P");
                            patientEmail.setPrimaryIND("Y");

                            ArrayList<PatientEmail> patientEmails = new ArrayList<>();
                            patientEmails.add(patientEmail);

                            PatientPhone patientPhone = new PatientPhone();
                            patientPhone.setPhNumber(Long.valueOf(edtPhone.getText().toString()));
                            patientPhone.setPhType('M');
                            patientPhone.setPrimaryIND('Y');
                            patientPhone.setPhAreaCD(Integer.parseInt(edtPhone.getText().toString().substring(4, 10)));
                            patientPhone.setPhCntryCD(91);
                            patientPhone.setPhStateCD(Integer.parseInt(edtPhone.getText().toString().substring(0, 4)));

                            ArrayList<PatientPhone> patientPhones = new ArrayList<PatientPhone>();
                            patientPhones.add(patientPhone);

                            RegisterRequest registerRequest = RegisterReqSingleton.getSingleton();
                            registerRequest.setPatientDetails2(patientDetails2);
                            registerRequest.setPatientEmails(patientEmails);
                            registerRequest.setPatientPhones(patientPhones);

                            GetPatientsRequest patientsRequest = new GetPatientsRequest();
                            showLoader("");
                            RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(patientsRequest, new RestCallback<GetPatientsResponse>() {
                                @Override
                                public void failure(RestError restError) {
                                    hideLoader();
                                }

                                @Override
                                public void success(GetPatientsResponse getPatientsResponse, Response response) {
                                    hideLoader();
                                    if (getPatientsResponse != null) {
                                        if (getPatientsResponse.getCount() == 0) {
                                            int otp = generateRandom(6);
                                            OTPRequest otpRequest = new OTPRequest();
                                            otpRequest.setMailContent("" + otp);
                                            otpRequest.setText("Your hcue OTP is " + otp);
                                            otpRequest.setUSRId("0");
                                            otpRequest.setTo("91" + edtPhone.getText().toString());
                                            otpRequest.setToAddress("");
                                            callSendOTP(otpRequest, edtPhone.getText().toString());
                                        } else {
                                            int otp = generateRandom(6);
                                            OTPRequest otpRequest = new OTPRequest();
                                            otpRequest.setMailContent("" + otp);
                                            otpRequest.setText("Your hcue OTP is " + otp);
                                            otpRequest.setUSRId("" + getPatientsResponse.getPatientRows().get(0).getArrPatients().get(0).getPatientID());
                                            otpRequest.setTo("91" + edtPhone.getText().toString());
                                            otpRequest.setToAddress("");
                                            callSendOTP(otpRequest, edtPhone.getText().toString());
                                        }
                                    }
                                }
                            });
                        }else
                        {
                            isFromRegisterfail = true;
                            ShowAlertDialog("Alert","Entered email is already registered.",R.drawable.alert);
                        }
                    }

                }
            }
        });
    }

    private void callSendOTP(final OTPRequest otpRequest, final String mobilenumber)
    {
        RestClient.getAPI("https://api.checkmobi.com").sendOPT(otpRequest, new RestCallback<OTPResponse>() {
            @Override
            public void failure(RestError restError)
            {
                ShowAlertDialog("Whoops!","Failed to send OTP please try again.",R.drawable.worng);
            }

            @Override
            public void success(OTPResponse otpResponse, Response response) {

/*                PatientPhone patientPhone = new PatientPhone();
                patientPhone.setPhNumber(Long.valueOf(mobilenumber));
                patientPhone.setPhType('M');
                patientPhone.setPrimaryIND('Y');
                patientPhone.setPhAreaCD(Integer.valueOf(mobilenumber.substring(4,10)));
                patientPhone.setPhCntryCD(91);
                patientPhone.setPhStateCD(Integer.valueOf(mobilenumber.substring(0,4)));

                ArrayList<PatientPhone> patientPhones = new ArrayList<PatientPhone>();
                patientPhones.add(patientPhone);
                RegisterReqSingleton.getSingleton().setPatientPhones(patientPhones);
                RegisterReqSingleton.getSingleton().getPatientDetails2().setMobileID(Long.valueOf(mobilenumber));*/

                Intent slideactivity = new Intent(RegisterActivity.this, EnterOtpActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                slideactivity.putExtra("OTPREQUEST",otpRequest);
                slideactivity.putExtra("OTPVALUE",otpRequest.getMailContent());
                if(isGplusLogin) {
                    slideactivity.putExtra("isGplusLogin", true);
                    slideactivity.putExtra("GOOGLEID",GOOGLEID);
                }
                startActivity(slideactivity, bndlanimation);

            }
        });
    }

    public int generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Integer.parseInt(new String(digits));
    }


    private boolean isValidEmail(String et_Username) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(et_Username);
        return matcher.matches();
    }

    @Override
    public void onBackPressed()
    {
        Intent slideactivity = new Intent(RegisterActivity.this, LaunchActivity.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_down_in,R.anim.push_down_out).toBundle();
        startActivity(slideactivity, bndlanimation);
//        super.onBackPressed();
    }

    private void showDropdownTitles()
    {
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.custom_dialog,null);
        if(popupWindow == null)
        {
            popupWindow = new PopupWindow(view, llTitle.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
            ListView lv_source_names = (ListView) view.findViewById(R.id.lv_source_names);

            String[] items = {"Mr", "Ms" , "Miss" , "Master" , "Baby"};
           // ArrayAdapter<String> referral_names_adapter = new ArrayAdapter(this, R.layout.popup_cell, R.id.tv_pop_cell, items);
            TitleArrayAdapter arrayAdapter = new TitleArrayAdapter(this, items);
            lv_source_names.setAdapter(arrayAdapter);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            popupWindow.setOutsideTouchable(true);
            tvTitle.setTag(items[0]);
            tvTitle.setText(items[0]);
            setSpecificTypeFace(lv_source_names, ApplicationConstants.WALSHEIM_MEDIUM);
            popupWindow.showAsDropDown(llTitle);
/*            lv_source_names.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));
                    tvTitle.setTag((String) parent.getItemAtPosition(position));
                    tvTitle.setText((String) parent.getItemAtPosition(position));
                    popupWindow.dismiss();
                    popupWindow = null;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });*/
           /* lv_source_names.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));
                    tvTitle.setTag((String) parent.getItemAtPosition(position));
                    tvTitle.setText((String) parent.getItemAtPosition(position));
                    popupWindow.dismiss();
                }
            });*/
        }else
        {

            popupWindow.dismiss();
            popupWindow = null;
        }
    }


    public class TitleArrayAdapter extends ArrayAdapter<String>
    {

        private String[] items ;
        public TitleArrayAdapter(RegisterActivity context, String[] items) {

            super(context, 0, items);
            this.items = items ;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.popup_cell, parent, false);
            }
            // Lookup view for data population
            TextView tv_pop_cell = (TextView) convertView.findViewById(R.id.tv_pop_cell);
            // Populate the data into the template view using the data object
            tv_pop_cell.setText(items[position]);
            // Return the completed view to render on screen

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvTitle.setTag(items[position]);
                    tvTitle.setText(items[position]);
                    popupWindow.dismiss();
                    popupWindow = null;
                }
            });
            return convertView;
        }
    }
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        populateSetDate(year, monthOfYear+1, dayOfMonth);
    }

    public void populateSetDate(int year, int month, int day) {
        try {


            //  tt=boo.request;
            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat newDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
            Date MyDate = newDateFormat.parse(day + "/" + month + "/" + year);
            newDateFormat.applyPattern("dd MMM yyyy");
            String MyDate1 = newDateFormat.format(MyDate);
            newDateFormat1.applyPattern("EEEE");

            String mon="";
            if(month>9){
                mon=String.valueOf(month);
            }else{
                mon="0"+String.valueOf(month);
            }
            edtDob.setTag(year + "-" + mon + "-" + day);
            edtDob.setText(MyDate1);
            llDob.setTag(MyDate);


        }catch (Exception e){
            Log.e("ERROR",e.toString());}
    }
}
