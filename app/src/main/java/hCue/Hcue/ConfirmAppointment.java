package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import hCue.Hcue.DAO.HospitalDetails;
import hCue.Hcue.DAO.HospitalDetailsDO;
import hCue.Hcue.DAO.Patient;
import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.DAO.SelectedConsultation;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.DAO.UpdateDoctorAppointmentRequestDO;
import hCue.Hcue.DAO.UpdateDoctorAppointmentResponseDO;
import hCue.Hcue.model.ConfirmAppointmentReq;
import hCue.Hcue.model.ConfirmAppointmentRes;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.SelectedConsSingleton;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 07/06/16.
 */
public class ConfirmAppointment extends BaseActivity implements OnMapReadyCallback {
    private RelativeLayout rlConfirmAppointment;
    private TextView tvDoctorname, tvDoctorQualification, tvspeciality, tvClinic, tvDateTime, tvPatientName, tvDoctorEmail,
            tvDoctorNo, tvDoctorAddress, tvVisitReason;
    private Button btnConfirm, btnEmail, btnCallNow;
    private FrameLayout fMapView;
    private GoogleMap mapView;
    private LatLng latLng;
    private ImageView mapPreview, ivDoctorPic;
    private SpecialityResRow specialityResRow;
    private PatientRow selectedPatienRow;
    private Patient patient;
    private String reasontovisit;
    private LinearLayout llVisitReason;
    private boolean isFromReschedule = false;
    private long appointmentID;
    private char appointmentStatus;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void initialize() {
        rlConfirmAppointment = (RelativeLayout) getLayoutInflater().inflate(R.layout.confirm_appointment, null, false);
        llBody.addView(rlConfirmAppointment);

        specialityResRow = (SpecialityResRow) getIntent().getSerializableExtra("SELECTED_DATAILS");
        selectedPatienRow = (PatientRow) getIntent().getSerializableExtra("SELECTEDPATIENT");
        reasontovisit = getIntent().getStringExtra("REASONTOVISIT");
        if (getIntent().hasExtra("ISFROMRESCHEDULE")) {
            isFromReschedule = getIntent().getBooleanExtra("ISFROMRESCHEDULE", false);
            appointmentID = getIntent().getLongExtra("AppointmentID", 0);
            appointmentStatus = getIntent().getCharExtra("AppointmentStatus", '0');
        }

        tvPatientName           = (TextView) rlConfirmAppointment.findViewById(R.id.tvPatientName);
        tvDoctorname            = (TextView) rlConfirmAppointment.findViewById(R.id.tvDoctorname);
        tvDoctorQualification   = (TextView) rlConfirmAppointment.findViewById(R.id.tvDoctorQualification);
        tvspeciality            = (TextView) rlConfirmAppointment.findViewById(R.id.tvspeciality);
        tvClinic                = (TextView) rlConfirmAppointment.findViewById(R.id.tvClinic);
        tvDateTime              = (TextView) rlConfirmAppointment.findViewById(R.id.tvDateTime);
        tvDoctorEmail           = (TextView) rlConfirmAppointment.findViewById(R.id.tvDoctorEmail);
        tvDoctorNo              = (TextView) rlConfirmAppointment.findViewById(R.id.tvDoctorNo);
        tvDoctorAddress         = (TextView) rlConfirmAppointment.findViewById(R.id.tvDoctorAddress);
        tvVisitReason           = (TextView) rlConfirmAppointment.findViewById(R.id.tvVisitReason);

        btnConfirm              = (Button) rlConfirmAppointment.findViewById(R.id.btnConfirm);
        btnCallNow              = (Button) rlConfirmAppointment.findViewById(R.id.btnCallNow);
        btnEmail                = (Button) rlConfirmAppointment.findViewById(R.id.btnEmail);

        ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mapView = googleMap;
                //mapView.setOnMarkerClickListener(ConfirmAppointment.this);
                mapView.setMyLocationEnabled(true);
                if (specialityResRow != null)
                {
                    initData();
                    if (specialityResRow.getProfileImage() != null && !specialityResRow.getProfileImage().isEmpty()) {
                        Picasso.with(ConfirmAppointment.this).load(Uri.parse(specialityResRow.getProfileImage())).into(ivDoctorPic);
                    }
                    else
                    {
                        if(specialityResRow.getGender() == 'M')
                        {
                            Picasso.with(ConfirmAppointment.this).load(R.drawable.male).into(ivDoctorPic);
                        }
                        else {
                            Picasso.with(ConfirmAppointment.this).load(R.drawable.female).into(ivDoctorPic);
                        }
                    }
                    if(specialityResRow.getFullName().startsWith("Dr."))
                        tvDoctorname.setText(specialityResRow.getFullName());
                    else
                        tvDoctorname.setText("Dr. "+specialityResRow.getFullName());
                    //tvDoctorQualification.setText(specialityResRow.getQualification());
                    tvDoctorQualification.setVisibility(View.GONE);
                    Gson gson = new Gson();
                    LinkedHashMap<String, String> speciality = gson.fromJson(specialityResRow.getSpecialityID(), new TypeToken<LinkedHashMap<String, String>>() {
                    }.getType());
                    StringBuilder specialitybuilder = new StringBuilder();
                    for(String specialityid : speciality.values())
                    {
                        specialitybuilder.append(Constants.specialitiesMap.get(specialityid)+", ");
                    }
                    tvspeciality.setText(specialitybuilder.toString().substring(0, (specialitybuilder.toString().length()-2)));
                    //tvspeciality.setText(specialityResRow.getSpecialityID()+"");
                    tvClinic.setText(specialityResRow.getClinicName());
                    tvDoctorEmail.setText(specialityResRow.getEmailID());
                    tvDoctorNo.setText(specialityResRow.getPhoneNumber());
                    tvDoctorAddress.setText(specialityResRow.getAddress());
                }
            }
        });
        mapPreview              = (ImageView) rlConfirmAppointment.findViewById(R.id.mapPreview);
        ivDoctorPic             = (ImageView) rlConfirmAppointment.findViewById(R.id.ivDoctorPic);
        fMapView                = (FrameLayout) rlConfirmAppointment.findViewById(R.id.flMaps);
        llVisitReason           = (LinearLayout) rlConfirmAppointment.findViewById(R.id.llVisitReason);

        tvTopTitle.setText("CONFIRM APPOINTMENT");
        if (reasontovisit == null || reasontovisit.isEmpty() || reasontovisit.trim().isEmpty()) {
            llVisitReason.setVisibility(View.GONE);
        } else {
            tvVisitReason.setText(reasontovisit);
        }
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy, EEEE");
        Calendar calendar = SelectedConsSingleton.getSingleton().getSelected_date();
        int year = calendar.get(Calendar.YEAR);;
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int week = calendar.get(Calendar.DAY_OF_WEEK);

        String strMonth = DateUtils.getMonthInShortForm(month);
        String strDay = day + DateUtils.getExtension(day);
        String strWeek = DateUtils.getWeekInShortForm(week);

        tvDateTime.setText(strDay.toLowerCase()+" "+strMonth+" "+year +" "+strWeek+ " - " + SelectedConsSingleton.getSingleton().getStarttime()+" Hrs");
        if (selectedPatienRow == null)
            patient = LoginResponseSingleton.getSingleton().getArrPatients().get(0);
        else
            patient = selectedPatienRow.getArrPatients().get(0);
        tvPatientName.setText(patient.getFullName());



//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        setSpecificTypeFace(rlConfirmAppointment, ApplicationConstants.WALSHEIM_MEDIUM);


        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{tvDoctorEmail.getText().toString()});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Enquiry");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        btnCallNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + specialityResRow.getPhoneNumber()));
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", specialityResRow.getPhoneNumber(), null));
                startActivity(intent);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Connectivity.isConnected(ConfirmAppointment.this)) {
                    if (isFromReschedule){
                        callUpdateAppointmentService();
                    }else
                        callConfirmAppointmentService();
                } else {
                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                }
                /*Intent intent = new Intent(ConfirmAppointment.this,FeedBackActivity.class);
                startActivity(intent);*/
            }
        });


    }

    private void callConfirmAppointmentService() {
        showLoader("");
        ConfirmAppointmentReq confirmAppointmentReq = new ConfirmAppointmentReq();
        HospitalDetailsDO hospitalDetails = new HospitalDetailsDO();
        confirmAppointmentReq.setDoctorID(specialityResRow.getDoctorID());
        //confirmAppointmentReq.setAddressConsultID();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE");
        SelectedConsultation selectedConsultation = SelectedConsSingleton.getSingleton();
        Date date = selectedConsultation.getSelected_date().getTime();
        confirmAppointmentReq.setConsultationDt(simpleDateFormat.format(date));
        confirmAppointmentReq.setDayCD(simpleDateFormat2.format(date).substring(0, 3).toUpperCase());
        confirmAppointmentReq.setEndTime(selectedConsultation.getEndtime());
        confirmAppointmentReq.setStartTime(selectedConsultation.getStarttime());
        confirmAppointmentReq.setUSRId(patient.getPatientID());
        confirmAppointmentReq.setPatientID(patient.getPatientID());
        confirmAppointmentReq.setAddressConsultID(selectedConsultation.getAddressConsultId());
        if(selectedConsultation.getHospitalcode() != null)
            hospitalDetails.setHospitalCD(selectedConsultation.getHospitalcode());
        if(selectedConsultation.getBranchcode() != null)
            hospitalDetails.setBranchCD(selectedConsultation.getBranchcode());
        if(selectedConsultation.getHospitalID() != 0)
            hospitalDetails.setHospitalID(selectedConsultation.getHospitalID());
        else
            hospitalDetails.setHospitalID(null);
        if(selectedConsultation.getParentHospitalID() != 0)
            hospitalDetails.setParentHospitalID(selectedConsultation.getParentHospitalID());
        else
            hospitalDetails.setParentHospitalID(null);
        confirmAppointmentReq.setHospitalDetails(hospitalDetails);

        if (reasontovisit != null && !reasontovisit.isEmpty() && !reasontovisit.trim().isEmpty())
            confirmAppointmentReq.setOtherVisitRsn(reasontovisit);

        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).confirAppointment(confirmAppointmentReq, new RestCallback<ConfirmAppointmentRes>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong try again.",R.drawable.worng);
                /*ShowAlertDialog("Failed to book please try again.");
                SelectedConsSingleton.resetInstance();
                Intent slideactivity = new Intent(ConfirmAppointment.this, BookingSucess.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);*/
            }

            @Override
            public void success(ConfirmAppointmentRes confirmAppointmentRes, Response response) {
                try {
                    Calendar calendar = Calendar.getInstance(Locale.getDefault());
                    Calendar calendar2 = Calendar.getInstance(Locale.getDefault());
                    calendar.setTimeInMillis(confirmAppointmentRes.getConsultationDt());
                    calendar2.setTimeInMillis(confirmAppointmentRes.getConsultationDt());
                    String[] vals = confirmAppointmentRes.getStartTime().split(":");
                    calendar.set(Calendar.HOUR, Integer.parseInt(vals[0]));
                    calendar.set(Calendar.MINUTE, Integer.parseInt(vals[1]));
                    long calID = 3;
                    ContentResolver cr = getContentResolver();
                    ContentValues values = new ContentValues();
                    values.put(CalendarContract.Events.DTSTART, calendar.getTimeInMillis());
                    String[] vals1 = confirmAppointmentRes.getEndTime().split(":");
                    calendar2.set(Calendar.HOUR, Integer.parseInt(vals1[0]));
                    calendar2.set(Calendar.MINUTE, Integer.parseInt(vals1[1]));
                    values.put(CalendarContract.Events.DTEND, calendar2.getTimeInMillis());
                    values.put(CalendarContract.Events.TITLE, "Appointment");
                    values.put(CalendarContract.Events.DESCRIPTION, "Your have an appointment with Dr. " + specialityResRow.getFullName() + " at " + confirmAppointmentRes.getStartTime() + " Hrs");
                    values.put(CalendarContract.Events.CALENDAR_ID, calID);
                    values.put(CalendarContract.Events.EVENT_TIMEZONE, calendar.getTimeZone().getID());
                    Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

                    String eventID = uri.getLastPathSegment();
                    Log.d("EVENT ID : ", eventID);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }


                hideLoader();
                SelectedConsSingleton.getSingleton().setTokennumber(confirmAppointmentRes.getTokenNumber());
                //SelectedConsSingleton.resetInstance();
                Intent slideactivity = new Intent(ConfirmAppointment.this, BookingSucess.class);
                slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                slideactivity.putExtra("SELECTED_DATAILS",specialityResRow);
                slideactivity.putExtra("SELECTEDPATIENT",selectedPatienRow);
                slideactivity.putExtra("REASONTOVISIT",reasontovisit);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
                finishAffinity();
                finish();
            }
        });
    }

    private void callUpdateAppointmentService() {
        showLoader("");
        UpdateDoctorAppointmentRequestDO confirmAppointmentReq = new UpdateDoctorAppointmentRequestDO();
        HospitalDetailsDO hospitalDetails = new HospitalDetailsDO();
        confirmAppointmentReq.setDoctorID(specialityResRow.getDoctorID());
        //confirmAppointmentReq.setAddressConsultID();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE");
        SelectedConsultation selectedConsultation = SelectedConsSingleton.getSingleton();
        Date date = selectedConsultation.getSelected_date().getTime();
        confirmAppointmentReq.setConsultationDt(simpleDateFormat.format(date));
        confirmAppointmentReq.setDayCD(simpleDateFormat2.format(date).substring(0, 3).toUpperCase());
        confirmAppointmentReq.setEndTime(selectedConsultation.getEndtime());
        confirmAppointmentReq.setStartTime(selectedConsultation.getStarttime());
        confirmAppointmentReq.setUSRId(patient.getPatientID());
        confirmAppointmentReq.setUSRType("PATIENT");
        confirmAppointmentReq.setPatientID(patient.getPatientID());
        confirmAppointmentReq.setAddressConsultID(selectedConsultation.getAddressConsultId());
        confirmAppointmentReq.setAppointmentStatus(appointmentStatus);
        confirmAppointmentReq.setAppointmentID(appointmentID);
//        if(selectedConsultation.getHospitalcode() != null)
//            confirmAppointmentReq.setHospitalCD(selectedConsultation.getHospitalcode());
//        if(selectedConsultation.getBranchcode() != null)
//            confirmAppointmentReq.setBranchCD(selectedConsultation.getBranchcode());
//        if (reasontovisit != null && !reasontovisit.isEmpty() && !reasontovisit.trim().isEmpty())
//            confirmAppointmentReq.setOtherVisitRsn(reasontovisit);

        if(selectedConsultation.getHospitalcode() != null)
            hospitalDetails.setHospitalCD(selectedConsultation.getHospitalcode());
        if(selectedConsultation.getBranchcode() != null)
            hospitalDetails.setBranchCD(selectedConsultation.getBranchcode());
        if(selectedConsultation.getHospitalID() != 0)
            hospitalDetails.setHospitalID(selectedConsultation.getHospitalID());
        if(selectedConsultation.getParentHospitalID() != 0)
            hospitalDetails.setParentHospitalID(selectedConsultation.getParentHospitalID());
        confirmAppointmentReq.setHospitalDetails(hospitalDetails);

        confirmAppointmentReq.setDoctorVisitRsnID("OPATIENT");
        if (reasontovisit != null && !reasontovisit.isEmpty() && !reasontovisit.trim().isEmpty())
            confirmAppointmentReq.setOtherVisitRsn(reasontovisit);


        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).updateDoctorAppointments(confirmAppointmentReq, new RestCallback<UpdateDoctorAppointmentResponseDO>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong try again.",R.drawable.worng);
                /*ShowAlertDialog("Failed to book please try again.");
                SelectedConsSingleton.resetInstance();
                Intent slideactivity = new Intent(ConfirmAppointment.this, BookingSucess.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);*/
            }

            @Override
            public void success(UpdateDoctorAppointmentResponseDO confirmAppointmentRes, Response response) {
                try {
                    Calendar calendar = Calendar.getInstance(Locale.getDefault());
                    Calendar calendar2 = Calendar.getInstance(Locale.getDefault());
                    calendar.setTimeInMillis(confirmAppointmentRes.getConsultationDt());
                    calendar2.setTimeInMillis(confirmAppointmentRes.getConsultationDt());
                    String[] vals = confirmAppointmentRes.getStartTime().split(":");
                    calendar.set(Calendar.HOUR, Integer.parseInt(vals[0]));
                    calendar.set(Calendar.MINUTE, Integer.parseInt(vals[1]));
                    long calID = 3;
                    ContentResolver cr = getContentResolver();
                    ContentValues values = new ContentValues();
                    values.put(CalendarContract.Events.DTSTART, calendar.getTimeInMillis());
                    String[] vals1 = confirmAppointmentRes.getEndTime().split(":");
                    calendar2.set(Calendar.HOUR, Integer.parseInt(vals1[0]));
                    calendar2.set(Calendar.MINUTE, Integer.parseInt(vals1[1]));
                    values.put(CalendarContract.Events.DTEND, calendar2.getTimeInMillis());
                    values.put(CalendarContract.Events.TITLE, "Appointment");
                    values.put(CalendarContract.Events.DESCRIPTION, "Your have an appointment with Dr. " + specialityResRow.getFullName() + " at " + confirmAppointmentRes.getStartTime() + " Hrs");
                    values.put(CalendarContract.Events.CALENDAR_ID, calID);
                    values.put(CalendarContract.Events.EVENT_TIMEZONE, calendar.getTimeZone().getID());
                    Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

                    String eventID = uri.getLastPathSegment();
                    Log.d("EVENT ID : ", eventID);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }


                hideLoader();
                SelectedConsSingleton.getSingleton().setTokennumber(Integer.parseInt(confirmAppointmentRes.getTokenNumber()));
                //SelectedConsSingleton.resetInstance();
                Intent slideactivity = new Intent(ConfirmAppointment.this, BookingSucess.class);
                slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                slideactivity.putExtra("SELECTED_DATAILS",specialityResRow);
                slideactivity.putExtra("SELECTEDPATIENT",selectedPatienRow);
                slideactivity.putExtra("REASONTOVISIT",reasontovisit);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                startActivity(slideactivity, bndlanimation);
                finishAffinity();
                finish();
            }
        });
    }

    private void setUpMap(LatLng doctorLatLng) {
        try {
            if (doctorLatLng == null) {
                if (specialityResRow.getLatitude() == null || specialityResRow.getLongitude() == null)
                    return;
                else
                    latLng = new LatLng(Double.parseDouble(specialityResRow.getLatitude()), Double.parseDouble(specialityResRow.getLongitude()));
            } else {
                latLng = doctorLatLng;
            }
        }catch (Exception e){

        }
        mapView.addMarker(new MarkerOptions().position(latLng));
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 14.0f);
        mapView.moveCamera(yourLocation);


        mapPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri location = Uri.parse(String.format("http://maps.google.com/?daddr=" + latLng.latitude + "," + latLng.longitude));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                boolean isIntentSafe = activities.size() > 0;

                if (isIntentSafe) {
                    startActivity(mapIntent);
                } else {
                    //showCustomDialog(RestaurantFullViewActivity.this, "Alert!", "No map applications installed on phone", "Ok", "", "");
                }
            }
        });

        mapView.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mapView.snapshot(new GoogleMap.SnapshotReadyCallback() {

                    @Override
                    public void onSnapshotReady(Bitmap bitmap) {
                        fMapView.removeViewAt(0);
                        mapPreview.setLayoutParams(new FrameLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        mapPreview.setImageBitmap(bitmap);
                        //						llLoader.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    private void initData() {
        setUpMap(null);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ConfirmAppointment Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://hCue.Hcue/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ConfirmAppointment Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://hCue.Hcue/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
