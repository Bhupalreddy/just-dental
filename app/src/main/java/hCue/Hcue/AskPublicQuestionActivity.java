package hCue.Hcue;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import hCue.Hcue.DAO.AdditionalDetailsDO;
import hCue.Hcue.DAO.AllergyNotesDO;
import hCue.Hcue.DAO.AskPublicQuestionRequestDO;
import hCue.Hcue.DAO.DiagnosisNotesDO;
import hCue.Hcue.DAO.FiltersDO;
import hCue.Hcue.DAO.ListQuestionsRequestDO;
import hCue.Hcue.DAO.MedicineNotesDO;
import hCue.Hcue.DAO.PatientDetailsQuestionsDO;
import hCue.Hcue.DAO.QuestionsProfileVitalsDO;
import hCue.Hcue.DAO.QuestionsResponsDO;
import hCue.Hcue.DAO.SpecialityResponseDO;
import hCue.Hcue.DAO.VitalsDO;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;


/**
 * Created by shyamprasadg on 03/06/16.
 */
public class AskPublicQuestionActivity extends BaseActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener
{

    private LinearLayout llAskPublicQuestion, llPreviousDiagnosed, llAnyMedications, llAllergies;
    private Button btnNext;
    private TextInputLayout tvPatientName, tvDOB, tvCity, tvArea, tvHeight, tvWeight, tvDoctorSpeciality;
    private TextView tvMinCharacters, tvAddNewPreviousDiagnosed, tvAddNewAnyMedications, tvAddNewAllergies, tvAdditionalDetails;
    private EditText etPatientName, etDOB, etCity, etArea, etHeight, etWeight, etQuestion, etDoctorSpeciality;
    private RadioButton rbMale, rbFeMale;
    private CheckBox cbPreviousDiagnosed, cbAnyMedications, cbAllergy;
    private RippleView rvNext;
    private static final int ADD_NEW_ALLERGY = 1000, ADD_NEW_CONDITION = 1001, ADD_NEW_MEDICATION = 1002, SEARCH_SPECIALITY_REQUEST_CODE = 1003;
    private ArrayList<MedicineNotesDO> listMedications;
    private ArrayList<DiagnosisNotesDO> listDiagonsis;
    private ArrayList<AllergyNotesDO> listAllergy;
    private boolean isMaleSelected = true;


    @Override
    public void initialize() {


        llAskPublicQuestion = (LinearLayout) getLayoutInflater().inflate(R.layout.ask_public_question, null, false);
        llBody.addView(llAskPublicQuestion, baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);

        btnNext = (Button) findViewById(R.id.btnNext);

        tvTopTitle.setVisibility(View.VISIBLE);
        llClose.setVisibility(View.GONE);
        tvTopTitle.setText("Ask a Question");
        llSave.setVisibility(View.GONE);
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);

        initViewControls();
        bindControls();
        loadDefaultProfileData();
    }

    private void initViewControls() {
        llPreviousDiagnosed                     = (LinearLayout) llAskPublicQuestion.findViewById(R.id.llPreviousDiagnosed);
        llAnyMedications                        = (LinearLayout) llAskPublicQuestion.findViewById(R.id.llAnyMedications);
        llAllergies                             = (LinearLayout) llAskPublicQuestion.findViewById(R.id.llAllergies);
        btnNext                                 = (Button) findViewById(R.id.btnNext);
        tvPatientName                           = (TextInputLayout) findViewById(R.id.tvPatientName);
        tvDOB                                   = (TextInputLayout) findViewById(R.id.tvDOB);
        tvCity                                  = (TextInputLayout) findViewById(R.id.tvCity);
        tvArea                                  = (TextInputLayout) findViewById(R.id.tvArea);
        tvHeight                                = (TextInputLayout) findViewById(R.id.tvHeight);
        tvWeight                                = (TextInputLayout) findViewById(R.id.tvWeight);
        tvDoctorSpeciality                      = (TextInputLayout) findViewById(R.id.tvDoctorSpeciality);
        tvMinCharacters                         = (TextView) findViewById(R.id.tvMinCharacters);
        tvAddNewPreviousDiagnosed               = (TextView) findViewById(R.id.tvAddNewPreviousDiagnosed);
        tvAddNewAnyMedications                  = (TextView) findViewById(R.id.tvAddNewAnyMedications);
        tvAddNewAllergies                       = (TextView) findViewById(R.id.tvAddNewAllergies);
        tvAdditionalDetails                     = (TextView) findViewById(R.id.tvAdditionalDetails);
        etPatientName                           = (EditText) findViewById(R.id.etPatientName);
        etDOB                                   = (EditText) findViewById(R.id.etDOB);
        etCity                                  = (EditText) findViewById(R.id.etCity);
        etArea                                  = (EditText) findViewById(R.id.etArea);
        etDoctorSpeciality                      = (EditText) findViewById(R.id.etDoctorSpeciality);
        etHeight                                = (EditText) findViewById(R.id.etHeight);
        etWeight                                = (EditText) findViewById(R.id.etWeight);
        etQuestion                              = (EditText) findViewById(R.id.etQuestion);
        rbMale                                  = (RadioButton) findViewById(R.id.rbMale);
        rbFeMale                                = (RadioButton) findViewById(R.id.rbFeMale);
        cbPreviousDiagnosed                     = (CheckBox) findViewById(R.id.cbPreviousDiagnosed);
        cbAnyMedications                        = (CheckBox) findViewById(R.id.cbAnyMedications);
        cbAllergy                               = (CheckBox) findViewById(R.id.cbAllergy);
        rvNext                                  = (RippleView) findViewById(R.id.rvNext);

    }

    public void bindControls(){

        listDiagonsis       = new ArrayList<>();
        listMedications     = new ArrayList<>();
        listAllergy         = new ArrayList<>();

        btnNext.setOnClickListener(this);
        etDOB.setOnClickListener(this);
        rbMale.setOnClickListener(this);
        rbFeMale.setOnClickListener(this);
        tvAddNewPreviousDiagnosed.setOnClickListener(this);
        tvAddNewAnyMedications.setOnClickListener(this);
        tvAddNewAllergies.setOnClickListener(this);
        cbPreviousDiagnosed.setOnClickListener(this);
        cbAnyMedications.setOnClickListener(this);
        cbAllergy.setOnClickListener(this);
        etDoctorSpeciality.setOnClickListener(this);

        setSpecificTypeFace(llAskPublicQuestion, ApplicationConstants.WALSHEIM_MEDIUM);
        tvPatientName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvDOB.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvCity.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvArea.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvHeight.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvWeight.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvDoctorSpeciality.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvMinCharacters.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvAddNewPreviousDiagnosed.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvAddNewAnyMedications.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvAddNewAllergies.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvAdditionalDetails.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        etQuestion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (60 - s.toString().trim().length() > 0){
                    tvMinCharacters.setVisibility(View.VISIBLE);
                    tvMinCharacters.setText((60 - s.toString().trim().length())+" characters remaining");
                }else if (60 - s.toString().trim().length() == 60){
                    tvMinCharacters.setVisibility(View.VISIBLE);
                    tvMinCharacters.setText("Minimum " +(60 - s.toString().trim().length())+" characters");
                }
                else
                    tvMinCharacters.setVisibility(View.INVISIBLE);
            }
        });

        etPatientName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0)
                    etPatientName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                else
                    etPatientName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_field_clear, 0);
            }
        });

        etPatientName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    try{
                        if(event.getRawX() >= (etPatientName.getRight() - etPatientName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            etPatientName.setText("");
                            return true;
                        }
                    }catch (Exception e){
                    }

                }
                return false;
            }
        });

        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0)
                    etCity.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                else
                    etCity.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_field_clear, 0);
            }
        });

        etCity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    try{
                        if(event.getRawX() >= (etCity.getRight() - etCity.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            etCity.setText("");

                            return true;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
                return false;
            }
        });

        etArea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0)
                    etArea.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                else
                    etArea.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_field_clear, 0);
            }
        });

        etArea.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    try{
                        if(event.getRawX() >= (etArea.getRight() - etArea.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            etArea.setText("");
                            return true;
                        }
                    }catch (Exception e){}
                }
                return false;
            }
        });

        etHeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0)
                    etHeight.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                else
                    etHeight.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_field_clear, 0);
            }
        });

        etHeight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    try{
                        if(event.getRawX() >= (etHeight.getRight() - etHeight.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            etHeight.setText("");

                            return true;
                        }
                    }catch (Exception e){}
                }
                return false;
            }
        });

        etWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0)
                    etWeight.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                else
                    etWeight.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_field_clear, 0);
            }
        });

        etWeight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    try{
                        if(event.getRawX() >= (etWeight.getRight() - etWeight.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            etWeight.setText("");

                            return true;
                        }
                    }catch (Exception e){}
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnNext:
                prepareSubmitRequest();
                break;
            case R.id.etDOB:
                displayDatePicker();
                break;
            case R.id.rbMale:
                rbFeMale.setChecked(false);
                isMaleSelected = true;
                break;
            case R.id.rbFeMale:
                rbMale.setChecked(false);
                isMaleSelected = false;
                break;
            case R.id.tvAddNewPreviousDiagnosed:
                Intent inPreviousDiagnosed = new Intent(AskPublicQuestionActivity.this, AddConditionActivity.class);
                startActivityForResult(inPreviousDiagnosed, ADD_NEW_CONDITION);
                break;
            case R.id.tvAddNewAnyMedications:
                Intent inAddNewMedication  = new Intent(AskPublicQuestionActivity.this, AddMedicineActivity.class);
                startActivityForResult(inAddNewMedication, ADD_NEW_MEDICATION);
                break;
            case R.id.tvAddNewAllergies:
                Intent inAddNewAllergies = new Intent(AskPublicQuestionActivity.this, AddAllergyActivity.class);
                startActivityForResult(inAddNewAllergies, ADD_NEW_ALLERGY);
                break;
            case R.id.cbPreviousDiagnosed:
                if (cbPreviousDiagnosed.isChecked()){
                    llPreviousDiagnosed.setVisibility(View.VISIBLE);
                    tvAddNewPreviousDiagnosed.setVisibility(View.VISIBLE);
                    if (listDiagonsis.size() == 0){
                        Intent diagnosis = new Intent(AskPublicQuestionActivity.this, AddConditionActivity.class);
                        startActivityForResult(diagnosis, ADD_NEW_CONDITION);
                    }
                }else{
                    llPreviousDiagnosed.setVisibility(View.GONE);
                    tvAddNewPreviousDiagnosed.setVisibility(View.GONE);
                }
                break;
            case R.id.cbAllergy:
                if (cbAllergy.isChecked()){
                    llAllergies.setVisibility(View.VISIBLE);
                    tvAddNewAllergies.setVisibility(View.VISIBLE);
                    if (listAllergy.size() == 0) {
                        Intent diagnosis = new Intent(AskPublicQuestionActivity.this, AddAllergyActivity.class);
                        startActivityForResult(diagnosis, ADD_NEW_ALLERGY);
                    }
                }else{
                    llAllergies.setVisibility(View.GONE);
                    tvAddNewAllergies.setVisibility(View.GONE);
                }
                break;
            case R.id.cbAnyMedications:
                if (cbAnyMedications.isChecked()){
                    llAnyMedications.setVisibility(View.VISIBLE);
                    tvAddNewAnyMedications.setVisibility(View.VISIBLE);
                    if (listMedications.size() == 0) {
                        Intent diagnosis = new Intent(AskPublicQuestionActivity.this, AddMedicineActivity.class);
                        startActivityForResult(diagnosis, ADD_NEW_MEDICATION);
                    }
                }else{
                    llAnyMedications.setVisibility(View.GONE);
                    tvAddNewAnyMedications.setVisibility(View.GONE);
                }
                break;
            case R.id.etDoctorSpeciality:
                Intent in = new Intent(AskPublicQuestionActivity.this, SearchSpecialityActivity.class);
                startActivityForResult(in, SEARCH_SPECIALITY_REQUEST_CODE);
                break;
        }
    }

    private void loadDefaultProfileData(){

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat sdfService = new SimpleDateFormat("yyyy-MM-dd");
        String height = "", weight = "";
        LoginResponse loginResponse  = LoginResponseSingleton.getSingleton();
        String patientName = loginResponse.getArrPatients().get(0).getFullName();
        char gender        = loginResponse.getArrPatients().get(0).getGender();
        long dob           = loginResponse.getArrPatients().get(0).getDOB();
        if (loginResponse.getPatientVitals() != null && loginResponse.getPatientVitals().getVitals() != null
                && loginResponse.getPatientVitals().getVitals().getHeight() != null)
            height           = loginResponse.getPatientVitals().getVitals().getHeight();
        if (loginResponse.getPatientVitals() != null && loginResponse.getPatientVitals().getVitals() != null
                && loginResponse.getPatientVitals().getVitals().getHeight() != null)
            weight            = loginResponse.getPatientVitals().getVitals().getWeight();

        etPatientName.setText(patientName);
        etHeight.setText(height);
        etWeight.setText(weight);
        etDOB.setText(sdf.format(dob));
        etDOB.setTag(sdfService.format(dob));
        if (gender == 'F'){
            rbMale.setChecked(false);
            rbFeMale.setChecked(true);
            isMaleSelected = false;
        }
        else{
            rbMale.setChecked(true);
            rbFeMale.setChecked(false);
            isMaleSelected = true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String condition = "", notes = "", medicine = "", allergy = "";
        boolean isHaving;

        if (requestCode == ADD_NEW_CONDITION && resultCode == RESULT_OK){
            if(data.getExtras().containsKey("condition")){

                condition         = data.getExtras().getString("condition");
                isHaving    = data.getExtras().getBoolean("isHaving");
                notes       = data.getExtras().getString("Notes");

                DiagnosisNotesDO diagnosisNotesDO = new DiagnosisNotesDO();
                diagnosisNotesDO.setCondition(condition);
                diagnosisNotesDO.setIsPresent(isHaving? "Y":"N");
                diagnosisNotesDO.setConditionNotes(notes);

                listDiagonsis.add(diagnosisNotesDO);
                String temp = "";
                if (isHaving){
                    if(notes != null && !notes.equalsIgnoreCase(""))
                        temp = condition + " (Present) - "+ (notes);
                    else
                        temp = condition + " (Present) ";
                }else{
                    if(notes != null && !notes.equalsIgnoreCase(""))
                        temp = condition + " (Absent) - "+ (notes);
                    else
                        temp = condition + " (Absent) ";
                }

                addtoContainer(temp, llPreviousDiagnosed, diagnosisNotesDO);
            }
        }else if (requestCode == ADD_NEW_MEDICATION && resultCode == RESULT_OK){
            if(data.getExtras().containsKey("medicine")){

                medicine         = data.getExtras().getString("medicine");
                isHaving    = data.getExtras().getBoolean("isHaving");
                notes       = data.getExtras().getString("Notes");

                MedicineNotesDO medicineNotesDO = new MedicineNotesDO();
                medicineNotesDO.setMedicine(medicine);
                medicineNotesDO.setIsPresent(isHaving? "Y":"N");
                medicineNotesDO.setMedicineNotes(notes);

                listMedications.add(medicineNotesDO);
                String temp = "";
                if (isHaving){
                    if(notes != null && !notes.equalsIgnoreCase(""))
                        temp = medicine + " (Taking) - "+ (notes);
                    else
                        temp = medicine + " (Taking) ";
                }else{
                    if(notes != null && !notes.equalsIgnoreCase(""))
                        temp = medicine + " (Not Taking) - "+ (notes);
                    else
                        temp = medicine + " (Not Taking) ";
                }

                addtoContainer(temp, llAnyMedications, medicineNotesDO);
            }
        }else if (requestCode == ADD_NEW_ALLERGY && resultCode == RESULT_OK){
            if(data.getExtras().containsKey("allergy")){

                allergy         = data.getExtras().getString("allergy");
                isHaving        = data.getExtras().getBoolean("isHaving");
                notes           = data.getExtras().getString("Notes");

                AllergyNotesDO allergyNotesDO = new AllergyNotesDO();
                allergyNotesDO.setAllergy(allergy);
                allergyNotesDO.setIsPresent(isHaving? "Y":"N");
                allergyNotesDO.setAllergyNotes(notes);

                listAllergy.add(allergyNotesDO);
                String temp = "";
                if (isHaving){
                    if(notes != null && !notes.equalsIgnoreCase(""))
                        temp = allergy + " (Present) - "+ (notes);
                    else
                        temp = allergy + " (Present) ";
                }else{
                    if(notes != null && !notes.equalsIgnoreCase(""))
                        temp = allergy + " (Not Present) - "+ (notes);
                    else
                        temp = allergy + " (Not Present) ";
                }

                addtoContainer(temp, llAllergies, allergyNotesDO);
            }
        }else if (requestCode == SEARCH_SPECIALITY_REQUEST_CODE && resultCode == RESULT_OK)
        {
            if(data.getExtras().containsKey("specialityDO")) {
                SpecialityResponseDO specialityDO = (SpecialityResponseDO) data.getSerializableExtra("specialityDO");
                etDoctorSpeciality.setText(specialityDO.getDoctorSpecialityDesc());
                etDoctorSpeciality.setTag(specialityDO.getDoctorSpecialityID());
            }
        }else if (requestCode == ADD_NEW_CONDITION && resultCode == RESULT_CANCELED){
            if (listDiagonsis.size() == 0){
                llPreviousDiagnosed.setVisibility(View.GONE);
                tvAddNewPreviousDiagnosed.setVisibility(View.GONE);
                cbPreviousDiagnosed.setChecked(false);
            }
        }else if (requestCode == ADD_NEW_MEDICATION && resultCode == RESULT_CANCELED){
            if (listMedications.size() == 0){
                llAnyMedications.setVisibility(View.GONE);
                tvAddNewAnyMedications.setVisibility(View.GONE);
                cbAnyMedications.setChecked(false);
            }
        }else if (requestCode == ADD_NEW_ALLERGY && resultCode == RESULT_CANCELED){
            if (listAllergy.size() == 0){
                llAllergies.setVisibility(View.GONE);
                tvAddNewAllergies.setVisibility(View.GONE);
                cbAllergy.setChecked(false);
            }
        }
    }

    private void addtoContainer(String medicine, final LinearLayout llContainer, final Object obj)
    {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.ask_public_question_additional_details_cell, null);
        final TextView tvName = (TextView) addView.findViewById(R.id.tvName);
        tvName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        final LinearLayout llDelete = (LinearLayout) addView.findViewById(R.id.llDelete);
        llDelete.setTag(obj);

        tvName.setText(medicine);
        llContainer.addView(addView, llContainer.getChildCount() -1);

        llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llContainer.removeView((ViewGroup) llDelete.getParent().getParent());

                Object object = llDelete.getTag();

                if (object instanceof DiagnosisNotesDO){
                    listDiagonsis.remove(object);
                    if (llContainer.getChildCount() == 0){
                        llPreviousDiagnosed.setVisibility(View.GONE);
                        tvAddNewPreviousDiagnosed.setVisibility(View.GONE);
                        cbPreviousDiagnosed.setChecked(false);
                    }
                }else if (object instanceof MedicineNotesDO){
                    listMedications.remove(object);
                    if (llContainer.getChildCount() == 0){
                        llAnyMedications.setVisibility(View.GONE);
                        tvAddNewAnyMedications.setVisibility(View.GONE);
                        cbAnyMedications.setChecked(false);
                    }
                }else if (object instanceof AllergyNotesDO){
                    listAllergy.remove(object);
                    if (llContainer.getChildCount() == 0){
                        llAllergies.setVisibility(View.GONE);
                        cbAllergy.setChecked(false);
                    }
                }
            }
        });
    }

    private void prepareSubmitRequest() {

        if (TextUtils.isEmpty(etPatientName.getText().toString().trim())) {
            ShowAlertDialog("Alert","Patient name should not be empty.",R.drawable.alert, false);
            etPatientName.requestFocus();
        } else if (TextUtils.isEmpty(etDOB.getText().toString().trim())) {
            ShowAlertDialog("Alert","Please enter a date of birth.",R.drawable.alert, false);
            etDOB.requestFocus();
        } else if (TextUtils.isEmpty(etCity.getText().toString().trim())) {
            etCity.requestFocus();
            ShowAlertDialog("Alert","Please enter city.",R.drawable.alert, false);
        }else if (TextUtils.isEmpty(etHeight.getText().toString().trim())) {
            etHeight.requestFocus();
            ShowAlertDialog("Alert","Please enter height.",R.drawable.alert, false);
        }else if (etHeight != null && Double.parseDouble(etHeight.getText().toString().trim()) == 0) {
            etHeight.requestFocus();
            ShowAlertDialog("Alert","Height should not be Zero.",R.drawable.alert, false);
        }else if (TextUtils.isEmpty(etWeight.getText().toString().trim())) {
            etWeight.requestFocus();
            ShowAlertDialog("Alert","Please enter weight.",R.drawable.alert, false);
        }else if (etWeight != null && Double.parseDouble(etWeight.getText().toString().trim()) == 0) {
            etWeight.requestFocus();
            ShowAlertDialog("Alert","Weight should not be Zero.",R.drawable.alert, false);
        }else if (TextUtils.isEmpty(etQuestion.getText().toString().trim())) {
            etQuestion.requestFocus();
            ShowAlertDialog("Alert","Please enter question.",R.drawable.alert, false);
        }else if (etQuestion.getText().toString().trim().length() < 60) {
            etQuestion.requestFocus();
            ShowAlertDialog("Alert","Question should have at least 60 characters length.",R.drawable.alert, false);
        }else {
            ShowAlertDialog();
        }
    }

    private void callMyQuestionsService(AskPublicQuestionRequestDO requestDO)
    {

        showLoader("");
        RestClient.getAPI(Constants.DEVICE_REGISTRATION).askPublicQuestion(requestDO, new RestCallback<String>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
//                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(String responseStr, Response response)
            {
                hideLoader();
                if (responseStr.equalsIgnoreCase("success")){
//                    Toast.makeText(AskPublicQuestionActivity.this, "Sending Question Success.", Toast.LENGTH_LONG).show();
                    Intent in = new Intent(AskPublicQuestionActivity.this, QuestionSuccessActivity.class);
                    startActivity(in);
                }else{
                    Toast.makeText(AskPublicQuestionActivity.this, "Sending Question failed.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void ShowAlertDialog()
    {

        try{
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AskPublicQuestionActivity.this, R.style.MyQuestionDialogTheme);
            View dialogView = LayoutInflater.from(AskPublicQuestionActivity.this).inflate(R.layout.question_submit_dialog, null);
            dialogBuilder.setView(dialogView);
            final TextView tvAlert = (TextView) dialogView.findViewById(R.id.tvAlert);
            RippleView rvNo = (RippleView) dialogView.findViewById(R.id.rvNo);
            RippleView rvYes = (RippleView) dialogView.findViewById(R.id.rvYes);
            TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
            TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);

            tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
            tvNo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            tvAlert.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();

            rvYes.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                @Override
                public void onComplete(RippleView rippleView) {

                    alertDialog.dismiss();

                    LoginResponse loginResponse  = LoginResponseSingleton.getSingleton();
                    long patientID = loginResponse.getArrPatients().get(0).getPatientID();

                    AskPublicQuestionRequestDO requestDO = new AskPublicQuestionRequestDO();
                    PatientDetailsQuestionsDO patientDetails = new PatientDetailsQuestionsDO();
                    QuestionsProfileVitalsDO vitalsContainerDO = new QuestionsProfileVitalsDO();
                    VitalsDO vitalsDO = new VitalsDO();
                    AdditionalDetailsDO additionalDetailsDO = new AdditionalDetailsDO();
                    FiltersDO filtersDO = new FiltersDO();

                    patientDetails.setPatientName(etPatientName.getText().toString().trim());
                    patientDetails.setDOB((String) etDOB.getTag());
                    patientDetails.setCity(etCity.getText().toString().trim());
                    patientDetails.setLocation(etArea.getText().toString().trim());
                    patientDetails.setGender(isMaleSelected ? "M":"F");

                    vitalsDO.setHGT(etHeight.getText().toString().trim());
                    vitalsDO.setWGT(etWeight.getText().toString().trim());
                    vitalsContainerDO.setVitals(vitalsDO);
                    patientDetails.setPatientVitals(vitalsContainerDO);

                    if (etDoctorSpeciality.getTag() != null)
                        filtersDO.setSpeciality((String)etDoctorSpeciality.getTag());

                    additionalDetailsDO.setListDiagonsis(listDiagonsis);
                    additionalDetailsDO.setListMedications(listMedications);
                    additionalDetailsDO.setListAllergy(listAllergy);

                    requestDO.setAdditionalDetails(additionalDetailsDO);
                    requestDO.setPatientDetails(patientDetails);
                    requestDO.setFilters(filtersDO);
                    requestDO.setPatientID(patientID);
                    requestDO.setUsrID(patientID);
                    requestDO.setQuestion(etQuestion.getText().toString().trim());

                    callMyQuestionsService(requestDO);
                }
            });

            rvNo.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                @Override
                public void onComplete(RippleView rippleView) {
                    alertDialog.dismiss();
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void displayDatePicker(){
        final Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        if(!etDOB.getText().toString().equalsIgnoreCase("")){
            String dob = etDOB.getText().toString().trim();
            try {
                Date tempDate = sdf.parse(dob);
                calendar.setTimeInMillis(tempDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AskPublicQuestionActivity.this, AskPublicQuestionActivity.this,  yy, mm, dd);
        //date is dateSetListener as per your code in question

        //   SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        populateSetDate(year, monthOfYear+1, dayOfMonth);
    }

    public void populateSetDate(int year, int month, int day) {
        try {
            //  tt=boo.request;
            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat newDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat newDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");
            Date MyDate = newDateFormat.parse(day + "/" + month + "/" + year);
            newDateFormat.applyPattern("dd MMM yyyy");
            String MyDate1 = newDateFormat.format(MyDate);
            newDateFormat1.applyPattern("EEEE");

            String mon="";
            if(month>9){
                mon= String.valueOf(month);
            }else{
                mon="0"+ String.valueOf(month);
            }
            etDOB.setText(newDateFormat2.format(MyDate));
            etDOB.setTag(year + "-" + mon + "-" + day);

        }catch (Exception e){
            Log.e("ERROR",e.toString());}
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        ShowAlertDialog("Alert","Are you sure you want to exit?",R.drawable.alert, true);

    }

    public void ShowAlertDialog(String header, String message, int resourceid, final boolean isFromBackButton)
    {

        try{
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AskPublicQuestionActivity.this, R.style.MyDialogTheme);
            View dialogView = LayoutInflater.from(AskPublicQuestionActivity.this).inflate(R.layout.home_pickup_dialog, null);
            dialogBuilder.setView(dialogView);
            TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
            final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
            final TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
            TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
            ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

            tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
            tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
            tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();

            tvInfo.setText(message);
            if (!isFromBackButton){
                tvYes.setText("OK");
                tvNo.setVisibility(View.GONE);
            }
            ivLogo.setBackgroundResource(resourceid);
            ivLogo.setVisibility(View.VISIBLE);
            tvInfo.setTextSize(18);

            tvNo.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction()==MotionEvent.ACTION_DOWN)
                    {
                        v.setBackgroundColor(Color.parseColor("#33110000"));

                    }
                    else if(event.getAction()==MotionEvent.ACTION_UP)
                    {
                        v.setBackgroundColor(Color.parseColor("#33000000"));
                        alertDialog.dismiss();
                    }
                    return true;
                }
            });
            tvYes.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction()==MotionEvent.ACTION_DOWN)
                    {
                        v.setBackgroundColor(Color.parseColor("#33110000"));

                    }
                    else if(event.getAction()==MotionEvent.ACTION_UP)
                    {
                        v.setBackgroundColor(Color.parseColor("#33000000"));
                        alertDialog.dismiss();
                        if (isFromBackButton)
                            finish();
                    }
                    return true;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
