package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import hCue.Hcue.CallBackInterfaces.OnRegistrationStepOneCallBack;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.widget.RippleView;

/**
 * Created by Appdest on 16-09-2016.
 */
@SuppressLint("ValidFragment")
public class RegisterStepThree extends Fragment implements DatePickerDialog.OnDateSetListener{

    public OnRegistrationStepOneCallBack mCallBack;
    public Context context;
    private RelativeLayout rlDOB;
    private LinearLayout llMale, llFemale;
    private TextView tvTitle,tvMale,tvFemale,tvDOB,tvDOBTilte;
    private boolean isMaleSelected = true;
    private Button btnContinue;
    private RippleView rvContinue;
    private char genderSelected;
    private ImageView ivMale, ivFemale;

    public RegisterStepThree() {
    }

    public RegisterStepThree(Context context, OnRegistrationStepOneCallBack onRegistrationStepOneCallBack) {
        this.context = context;
        this.mCallBack = onRegistrationStepOneCallBack;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register_step_three, container, false);

        rlDOB           =   (RelativeLayout)    v.findViewById(R.id.rlDOB);
        llMale          =   (LinearLayout)      v.findViewById(R.id.llMale);
        llFemale        =   (LinearLayout)      v.findViewById(R.id.llFemale);
        ivMale          =   (ImageView)         v.findViewById(R.id.ivMale);
        ivFemale        =   (ImageView)         v.findViewById(R.id.ivFemale);
        rvContinue      =   (RippleView)        v.findViewById(R.id.rvContinue);
        btnContinue     =   (Button)            v.findViewById(R.id.btnContinue);
        tvDOB           =   (TextView)          v.findViewById(R.id.tvDOB);
        tvTitle         =   (TextView)          v.findViewById(R.id.tvTitle);
        tvMale          =   (TextView)          v.findViewById(R.id.tvMale);
        tvFemale        =   (TextView)          v.findViewById(R.id.tvFemale);
        tvDOBTilte      =   (TextView)          v.findViewById(R.id.tvDOBTilte);

        btnContinue.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvDOB.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvTitle.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvMale.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvFemale.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvDOBTilte.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        if (RegistrationActivity.registrationDO.gender != ' ')
            if (RegistrationActivity.registrationDO.gender == 'M'){
                clearGenderBg();
                ivMale.setImageResource(R.drawable.male_selected);
                genderSelected = 'M';
            }else{
                clearGenderBg();
                ivFemale.setImageResource(R.drawable.female_selected);
                genderSelected = 'F';
            }


        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView)
            {
                validateInputFields();
            }
        });

        llMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearGenderBg();
                ivMale.setImageResource(R.drawable.male_selected);
                genderSelected = 'M';
            }
        });

        llFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearGenderBg();
                ivFemale.setImageResource(R.drawable.female_selected);
                genderSelected = 'F';
            }
        });


        rlDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), RegisterStepThree.this,  yy, mm, dd);
                //date is dateSetListener as per your code in question

                //   SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });


        return v;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        populateSetDate(year, monthOfYear+1, dayOfMonth);
    }

    public void populateSetDate(int year, int month, int day) {
        try {


            //  tt=boo.request;
            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat newDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat newDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");
            Date MyDate = newDateFormat.parse(day + "/" + month + "/" + year);
            newDateFormat.applyPattern("dd MMM yyyy");
            String MyDate1 = newDateFormat.format(MyDate);
            newDateFormat1.applyPattern("EEEE");

            String mon="";
            if(month>9){
                mon= String.valueOf(month);
            }else{
                mon="0"+ String.valueOf(month);
            }
            tvDOB.setText(newDateFormat2.format(MyDate));
            tvDOB.setTag(year + "-" + mon + "-" + day);

            RegistrationActivity.registrationDO.DOB = year + "-" + mon + "-" + day;
            RegistrationActivity.registrationDO.date1 = MyDate;

        }catch (Exception e){
            Log.e("ERROR",e.toString());}
    }

    public void validateInputFields() {
        String dob = tvDOB.getText().toString().trim();
        if (TextUtils.isEmpty(dob)){
            ShowAlertDialog("Alert","DOB should not be empty.",R.drawable.alert);
        }else {
            RegistrationActivity.registrationDO.DOB = (String) tvDOB.getTag();
            RegistrationActivity.registrationDO.gender = genderSelected;
            mCallBack.onContinueClickListener(3);
        }
    }
    public void clearGenderBg()
    {
        ivMale.setImageResource(R.drawable.male_unselected);
        ivFemale.setImageResource(R.drawable.female_unselected);
    }

    public void ShowAlertDialog(String header, String message, int resourceid)
    {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
        final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        final TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        ivLogo.setBackgroundResource(resourceid);
        tvInfo.setTextSize(18);

        tvYes.setText("OK");
        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.VISIBLE);
        tvNo.setVisibility(View.GONE);

        tvNo.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        tvYes.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });


    }
}