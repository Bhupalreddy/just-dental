package hCue.Hcue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hCue.Hcue.CallBackInterfaces.OnRegistrationStepOneCallBack;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.MarshMallowPermission;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;

import static android.R.attr.data;
import static android.app.Activity.RESULT_OK;

/**
 * Created by Appdest on 16-09-2016.
 */
@SuppressLint("ValidFragment")
public class RegisterStepTwo extends Fragment {

    public OnRegistrationStepOneCallBack mCallBack;
    public Context context;
    public static CircleImageView profileImage;
    private Button btnContinue;
    private EditText etName;
    private LinearLayout llCamera;
    private RippleView rvContinue;
    private int selecteid ;
    private int REQUEST_CAMERA = 2, SELECT_FILE = 1;
    private Uri uri;
    private TextInputLayout tvName;
    private Preference preference ;
    String mCurrentPhotoPath;
    private int screenWidth, screenHeight;
    private MarshMallowPermission marshMallowPermission;

    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 3;
    private File file = null;

//    private PatientUpdateRequest patientUpdateRequest ;

    public RegisterStepTwo(Context context, OnRegistrationStepOneCallBack onRegistrationStepOneCallBack) {
        this.context = context;
        this.mCallBack = onRegistrationStepOneCallBack;
    }

    public RegisterStepTwo() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register_step_two, container, false);

        preference = new Preference(getActivity());

        profileImage    =   (CircleImageView)   v.findViewById(R.id.profileImage);
        btnContinue     =   (Button)            v.findViewById(R.id.btnContinue);
        rvContinue      =   (RippleView)        v.findViewById(R.id.rvContinue);
        etName          =   (EditText)          v.findViewById(R.id.etName);
        tvName          =   (TextInputLayout)   v.findViewById(R.id.tvName);
        setUpDisplayScreenResolution(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());

        etName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        btnContinue.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        if (RegistrationActivity.registrationDO.fullName != null && !RegistrationActivity.registrationDO.equals("")) {
            etName.setText(RegistrationActivity.registrationDO.fullName);
            String url = preference.getStringFromPreference("PhotoUrl", "");
            preference.commitPreference();
            setLayoutParams(profileImage,R.drawable.default_photo);
            if (url != null){
                Glide.with(context).load(url).into(profileImage);
                new UrlToBitmap().execute(url);
            }
        }

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView) {
                validateInputFields();
            }
        });


        profileImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selecteid = v.getId();
                final String[] items = new String[]{"Take from camera",
                        "Select from gallery"};

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        getActivity(), R.layout.select_dialog_item,
                        R.id.text, items);

                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int item)
                            {

                                if (item == 0)
                                {
                                    ApplicationConstants.isFromCamera = true;
                                    int currentAPIVersion = Build.VERSION.SDK_INT;
                                    if (currentAPIVersion >= Build.VERSION_CODES.M)
                                    {
                                        boolean result =  marshMallowPermission.checkPermissionForCamera();
                                        if(result){
                                            boolean result2 =  marshMallowPermission.checkPermissionForExternalStorage();
                                            if(result2)
                                            {
                                                dispatchTakePictureIntent();
                                            }
                                            else
                                            {
                                                marshMallowPermission.requestPermissionForExternalStorage();
                                            }
                                        }
                                        else{
                                            marshMallowPermission.requestPermissionForCamera();
                                        }
                                    }
                                    else
                                    {
                                        dispatchTakePictureIntent();
                                    }

                                } else
                                {
                                    ApplicationConstants.isFromCamera = false;
                                    int currentAPIVersion = Build.VERSION.SDK_INT;
                                    if (currentAPIVersion >= Build.VERSION_CODES.M)
                                    {
                                        boolean result =  marshMallowPermission.checkPermissionForExternalStorage();
                                        if(result){
                                            galleryIntent();
                                        }
                                        else{
                                            marshMallowPermission.requestPermissionForExternalStorage();
                                        }
                                    }
                                    else
                                    {
                                        galleryIntent();
                                    }
                                }

//                                ((BaseActivity)context).showLoader("");
                            }
                        });

                final AlertDialog dialog = builder.show();
            }
        });

        return v;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),"com.example.android.fileprovider",photoFile);

                List<ResolveInfo> resolvedIntentActivities = getActivity().getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            }
        }
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void validateInputFields() {
        String name = etName.getText().toString().trim();
        if (TextUtils.isEmpty(name)){
            ShowAlertDialog("Alert","Name should not be empty.",R.drawable.alert);
        }else {
            RegistrationActivity.registrationDO.fullName = name;
//            RegistrationActivity.registrationDO.password = pwd;
            mCallBack.onContinueClickListener(2);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallBack = (OnRegistrationStepOneCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == getActivity().RESULT_OK)
        {
            ((BaseActivity)context).hideLoader();
            /*try {
                if (data != null) {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();

                    RegistrationActivity.registrationDO.profilePic = Base64.encodeToString(b, Base64.DEFAULT);
                    RegistrationActivity.registrationDO.profilePicUri = data.getData().toString();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            Intent intent = CropImage.activity(data.getData())
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setMultiTouchEnabled(true)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setAspectRatio(5,5)
                    .getIntent(getContext());
            startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

            setLayoutParams(profileImage, R.drawable.default_photo);

//            Glide.with(context).load(data.getData()).into(profileImage);
        }
        else if (requestCode == REQUEST_CAMERA && resultCode == getActivity().RESULT_OK ) {
            ((BaseActivity) context).hideLoader();

            Bitmap originalImg = null;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            originalImg = setPic();
            if (originalImg != null) {
                galleryAddPic();

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                originalImg.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                uri = Uri.fromFile(destination);
                RegistrationActivity.registrationDO.profilePic = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                RegistrationActivity.registrationDO.profilePicUri = uri.toString();

                Intent intent = CropImage.activity(uri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setAspectRatio(5,5).setOutputCompressFormat(Bitmap.CompressFormat.PNG).getIntent(getContext());
                startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

                setLayoutParams(profileImage, R.drawable.default_photo);
//                Glide.with(context).load(uri).into(profileImage);
            }
        }else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                prepareUploadPrescriptionRequest(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    //add photo to gallery
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }
    private Bitmap setPic() {
        // Get the dimensions of the View
        /*int targetW = *//*imageView.getWidth()*//*500;
        int targetH = *//*imageView.getHeight()*//*500;*/

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor;

        // Determine how much to scale down the image
        try {
            scaleFactor = Math.min(photoW/screenWidth, photoH/screenHeight);
        }catch (Exception e){
            e.printStackTrace();
            scaleFactor = 1;
        }


        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        Log.d(mCurrentPhotoPath,"Resolution : "+photoW+"x"+photoH);
        return bitmap;
    }


    @SuppressLint("NewApi")
    private String getPath(Uri uri)
    {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (isMediaDocument(uri)) {
            final String docId = DocumentsContract.getDocumentId(uri);
            final String[] split = docId.split(":");
            final String type = split[0];
            if ("image".equals(type)) {
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(type)) {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            selection = "_id=?";
            selectionArgs = new String[] {
                    split[1]
            };
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    public void ShowAlertDialog(String header, String message, int resourceid)
    {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView tvHeading = (TextView) dialogView.findViewById(R.id.tvHeading);
        final TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        final TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        TextView tvInfo = (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo = (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        ivLogo.setBackgroundResource(resourceid);
        tvInfo.setTextSize(18);

        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.VISIBLE);

        tvNo.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        tvYes.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#33110000"));

                }
                else if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    v.setBackgroundColor(Color.parseColor("#33000000"));
                    alertDialog.dismiss();
                }
                return true;
            }
        });
    }

    public void setLayoutParams(View v, int drawableId)
    {
        int [] dDimens = getDrawableDimensions(v.getContext(), drawableId);
        v.getLayoutParams().width = dDimens[0];
        v.getLayoutParams().height = dDimens[1];
    }
    public int[] getDrawableDimensions(Context context, int id)
    {
        int[] dimensions = new int[2];
        if(context == null)
            return dimensions;
        Drawable d = getResources().getDrawable(id);
        if(d != null)
        {
            dimensions[0] = d.getIntrinsicWidth();
            dimensions[1] = d.getIntrinsicHeight();
        }
        return dimensions;
    }

    class UrlToBitmap extends AsyncTask<String, Void, byte[]> {

        private Exception exception;
        String url = "";

        protected byte[] doInBackground(String... urls) {
            try {
                url = urls[0];
                URL url1 = new URL(urls[0]);
                Bitmap image = BitmapFactory.decodeStream(url1.openConnection().getInputStream());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                return b;
            } catch (Exception e) {
                this.exception = e;

                return null;
            }
        }

        protected void onPostExecute(byte[] b) {
            // TODO: check this.exception
            // TODO: do something with the feed
            RegistrationActivity.registrationDO.profilePic = Base64.encodeToString(b, Base64.DEFAULT);
            RegistrationActivity.registrationDO.profilePicUri = url;
        }
    }

    public void setUpDisplayScreenResolution(Context context) {
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        screenWidth = display.widthPixels;
        screenHeight = display.heightPixels;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                boolean result2 = marshMallowPermission.checkPermissionForExternalStorage();
                if (result2) {
                    dispatchTakePictureIntent();
                } else {
                    marshMallowPermission.requestPermissionForExternalStorage();
                }
            } else {
                marshMallowPermission.requestPermissionForCamera();
            }
        }
        //Checking the request code of our request
        else if (requestCode == EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ApplicationConstants.isFromCamera)

                    dispatchTakePictureIntent();
                else
                    galleryIntent();

            } else {
                marshMallowPermission.requestPermissionForExternalStorage();
            }
        }
    }

    public void prepareUploadPrescriptionRequest(final Uri tempUri) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    long fileSizeInMB = 0;
                    final Uri uri = tempUri;
                    String picturePath = getPath(uri);

//                    Cursor returnCursor = getActivity().getContentResolver().query(tempUri, null, null, null, null);
//    /*
//     * Get the column indexes of the data in the Cursor,
//     * move to the first row in the Cursor, get the data,
//     * and display it.
//     */
//                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
//                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);

                    Log.d("PATH", picturePath);
                    file = new File(picturePath);
                    long fileSizeInBytes = file.length();
//                    long fileSizeInBytes = sizeIndex;
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    fileSizeInMB = fileSizeInKB / 1024;
                    byte[] bytes1 = new byte[(int) fileSizeInBytes];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes1, 0, bytes1.length);
                        buf.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int index = picturePath.lastIndexOf(".");
                    ApplicationConstants.imageFile = file;
                    ApplicationConstants.fileExtension = picturePath.substring(index, picturePath.length());
                    Log.e("EXTENSION", picturePath.substring(index, picturePath.length()));
                    RegistrationActivity.registrationDO.profilePic = Base64.encodeToString(bytes1, Base64.DEFAULT);
                    RegistrationActivity.registrationDO.profilePicUri = uri.toString();
                    Log.e("EXTENSION", picturePath.substring(index, picturePath.length()));

                    if (fileSizeInMB < 5) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

//                                new UrlToBitmap().execute(uri.toString());


                                Glide.with(getActivity()).load(uri).skipMemoryCache(true).into(profileImage);

//                                Bitmap temp = ((BitmapDrawable)profileImage.getDrawable()).getBitmap();
//                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                                temp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
//                                byte[] b = baos.toByteArray();

//                                RegistrationActivity.registrationDO.profilePic = Base64.encodeToString(b, Base64.DEFAULT);
//                                RegistrationActivity.registrationDO.profilePicUri = uri.toString();
//                                file.deleteOnExit();
                            }
                        });


                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                file.deleteOnExit();
                                Toast.makeText(getActivity(), "Profile Picture size should not be exceed 5 MB.", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}