package hCue.Hcue;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

import hCue.Hcue.model.ForgotPasswordFindDetailsResponse;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import retrofit.client.Response;

/**
 * Created by CVLHYD-161 on 13-03-2016.
 */
public class ForgotPassowordVerificationActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etVerificationDetails;
    private TextView tvSearch,tvTitle;
    BaseActivity baseActivity;
    public ProgressDialog progressdialog;
    HashMap<Object , Object> mobileMap = new HashMap<>();
    private LinearLayout llBack ;
    private static AlertDialog alertDialog;
    private TextInputLayout tilVerificationDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_phone_no);

        LinearLayout llforgotPassword = (LinearLayout)findViewById(R.id.llforgotPassword);
        llBack = (LinearLayout)findViewById(R.id.llBack);
        etVerificationDetails   =   (EditText)          findViewById(R.id.etVerificationDetails);
        tilVerificationDetails  =   (TextInputLayout)   findViewById(R.id.tilVerificationDetails);
        tvSearch                =   (TextView)          findViewById(R.id.tvSearch);
        tvTitle                 =   (TextView)          findViewById(R.id.tvTitle);

        tvSearch.setOnClickListener(this);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });
        tvTitle.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        tilVerificationDetails.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        BaseActivity.setSpecificTypeFace(llforgotPassword, ApplicationConstants.WALSHEIM_MEDIUM);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvSearch){

            if (etVerificationDetails.getText().toString().isEmpty())
            {
                ShowAlertDialog("Please enter Email or Phone number.");
//                etVerificationDetails.setError("Please enter Email or Phone number.");
                return;
            }

            String verificationDetails = etVerificationDetails.getText().toString().trim();
            if(verificationDetails.contains(("@"))){
                try{
                    mobileMap.put("EmailID", verificationDetails);
                }catch (Exception e){

                }
                if (Connectivity.isConnected(ForgotPassowordVerificationActivity.this))
                {
                    callService(mobileMap);
                }else
                {
                    ShowAlertDialog("Please check internet connection.");
                }

            }else{
                try{
                    mobileMap.put("MobileNumber", Long.parseLong(verificationDetails));
                }catch (Exception e){

                }
                if (Connectivity.isConnected(ForgotPassowordVerificationActivity.this))
                {
                    callService(mobileMap);
                }else
                {
                    ShowAlertDialog("Please check internet connection.");
                }
            }
        }
    }

    private void callService(HashMap<Object , Object> request) {

        showLoader("Loading...");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).forgotPasswordFindDetails(request, new RestCallback<ForgotPasswordFindDetailsResponse>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("We couldn't find a hCue account with this email ID or Phone number. Please try searching for your email, phone number again.");
//                Toast.makeText(ForgotPassowordVerificationActivity.this, "Invalid Phone or Email ID.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(ForgotPasswordFindDetailsResponse forgotPasswordFindDetailsResponse, Response response) {

                hideLoader();
                if(forgotPasswordFindDetailsResponse != null && forgotPasswordFindDetailsResponse.getMessage().equalsIgnoreCase("Invalid_Login_ID"))
                {
                    ShowAlertDialog("We couldn't find a hCue account with this email ID or Phone number. Please try searching for your email, phone number again.");
//                    Toast.makeText(ForgotPassowordVerificationActivity.this, "Please enter valid email id or phone number.", Toast.LENGTH_SHORT).show();
                }else if(forgotPasswordFindDetailsResponse.getEmailID()==null || forgotPasswordFindDetailsResponse.getEmailID().isEmpty())
                {
                    ShowAlertDialog("We couldn't find a hCue account with this email ID or Phone number. Please try searching for your email, phone number again.");
                }else
                {
                    mobileMap.clear();
                    ApplicationConstants.forgotPasswordFindDetailsResponse = forgotPasswordFindDetailsResponse;
                    Intent slideactivity = new Intent(ForgotPassowordVerificationActivity.this, ChooseVerificationMode.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    slideactivity.putExtra("EmailID", forgotPasswordFindDetailsResponse.getEmailID());
                    slideactivity.putExtra("Mobile", forgotPasswordFindDetailsResponse.getMobileNumber() + "");
                    startActivity(slideactivity, bndlanimation);
                }

            }
        });
    }
    public void showLoader(String str) {
        runOnUiThread(new RunShowLoader(str));
    }

    public void hideLoader()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressdialog != null && progressdialog.isShowing()) {
                    progressdialog.dismiss();
                }
            }
        });
    }

    /**
     * Name:         RunShowLoader
     Description:  This is to show the loading progress dialog when some other functionality is taking place.**/
    class RunShowLoader implements Runnable {
        private String strMsg;

        public RunShowLoader(String strMsg) {
            this.strMsg = strMsg;
        }

        @Override
        public void run()
        {
            try {
                if(progressdialog == null ||(progressdialog != null && !progressdialog.isShowing())) {
                    progressdialog = ProgressDialog.show(ForgotPassowordVerificationActivity.this, "", strMsg);
//					progressdialog.setContentView(R.layout.progress_dialog);
                    progressdialog.setCancelable(false);
                } else {
                }
            } catch(Exception e) {
                e.printStackTrace();
                progressdialog = null;
            }
        }
    }

    public void ShowAlertDialog(String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ForgotPassowordVerificationActivity.this);
        View dialogView = LayoutInflater.from(ForgotPassowordVerificationActivity.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        tvInfo.setTextSize(18);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.GONE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }
}
