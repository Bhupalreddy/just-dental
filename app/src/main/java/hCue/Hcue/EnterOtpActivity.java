package hCue.Hcue;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.telephony.gsm.SmsManager;
import android.telephony.gsm.SmsMessage;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;

import hCue.Hcue.DAO.PatientDetails3;
import hCue.Hcue.DAO.PatientLoginDetails;
import hCue.Hcue.DAO.RegisterReqSingleton;
import hCue.Hcue.DAO.patientLogin;
import hCue.Hcue.model.AddUpdatePatientCaseDocumentImageRequest;
import hCue.Hcue.model.AddUpdatePatientCaseDocumentImageResponce;
import hCue.Hcue.model.GetPatientsRequest;
import hCue.Hcue.model.GetPatientsResponse;
import hCue.Hcue.model.LoginRequest;
import hCue.Hcue.model.LoginResponse;
import hCue.Hcue.model.OTPRequest;
import hCue.Hcue.model.OTPResponse;
import hCue.Hcue.model.PatientUpdateRequest;
import hCue.Hcue.model.RegisterRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;
import retrofit.mime.TypedFile;


/**
 * Created by shyamprasadg on 03/06/16.
 */
public class EnterOtpActivity extends BaseActivity implements View.OnFocusChangeListener
{
    private RelativeLayout llOtp ;
    private LinearLayout llVal1, llVal2, llVal3, llVal4, llVal5, llVal6;
    private static EditText etVal1, etVal2, etVal3, etVal4, etVal5, etVal6;
    private TextView tvContinue,tvTitle,tvResendOtp;
    private RippleView rvContinue,rvResendOtp;
    private static AlertDialog alertDialog;
    private boolean isRegisterSucess = false;
    private String GOOGLEID ;
    private TextView tvMobileNum;
    long mLastClickTime = 0;
    OTPRequest otpRequest;
    @Override
    public void initialize()
    {
        llOtp   =   (RelativeLayout) getLayoutInflater().inflate(R.layout.enter_otp,null,false);
        llBody.addView(llOtp,baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);

        final String otpvalue = getIntent().getStringExtra("OTPVALUE");
        final String mobile = getIntent().getStringExtra("Mobile");

        llVal1          = (LinearLayout) llOtp.findViewById(R.id.llVal1);
        llVal2          = (LinearLayout) llOtp.findViewById(R.id.llVal2);
        llVal3          = (LinearLayout) llOtp.findViewById(R.id.llVal3);
        llVal4          = (LinearLayout) llOtp.findViewById(R.id.llVal4);
        llVal5          = (LinearLayout) llOtp.findViewById(R.id.llVal5);
        llVal6          = (LinearLayout) llOtp.findViewById(R.id.llVal6);

        etVal1          = (EditText) llOtp.findViewById(R.id.etVal1);
        etVal2          = (EditText) llOtp.findViewById(R.id.etVal2);
        etVal3          = (EditText) llOtp.findViewById(R.id.etVal3);
        etVal4          = (EditText) llOtp.findViewById(R.id.etVal4);
        etVal5          = (EditText) llOtp.findViewById(R.id.etVal5);
        etVal6          = (EditText) llOtp.findViewById(R.id.etVal6);

        tvMobileNum     = (TextView)   llOtp.findViewById(R.id.tvMobileNum);
        tvContinue      = (TextView)    llOtp.findViewById(R.id.tvContinue);
        tvTitle         = (TextView)    llOtp.findViewById(R.id.tvTitle);
        tvResendOtp     = (TextView)    llOtp.findViewById(R.id.tvResendOtp);

        rvContinue      = (RippleView)  llOtp.findViewById(R.id.rvContinue);
        rvResendOtp     = (RippleView)  llOtp.findViewById(R.id.rvResendOtp);

         otpRequest = (OTPRequest) getIntent().getSerializableExtra("OTPREQUEST");
        if(getIntent().hasExtra("GOOGLEID"))
        {
            GOOGLEID = getIntent().getStringExtra("GOOGLEID");
        }

        setSpecificTypeFace(llOtp,ApplicationConstants.WALSHEIM_MEDIUM);
        tvContinue.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvTopTitle.setText("OTP PASSWORD");
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        tvMobileNum.setText(mobile);

        etVal1.setOnFocusChangeListener(this);
        etVal2.setOnFocusChangeListener(this);
        etVal3.setOnFocusChangeListener(this);
        etVal4.setOnFocusChangeListener(this);
        etVal5.setOnFocusChangeListener(this);
        etVal6.setOnFocusChangeListener(this);

        etVal1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1){
                    etVal2.requestFocus();
                    clearOtpBgs();
                    llVal2.setBackgroundResource(R.drawable.box_selected);
                }else {
                    llVal1.setBackgroundResource(R.drawable.box_selected);
                }
            }
        });

        etVal2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1){
                    etVal3.requestFocus();
                    clearOtpBgs();
                    llVal3.setBackgroundResource(R.drawable.box_selected);
                }else {
                    llVal2.setBackgroundResource(R.drawable.box_selected);
                }
            }
        });

        etVal3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1){
                    etVal4.requestFocus();
                    clearOtpBgs();
                    llVal4.setBackgroundResource(R.drawable.box_selected);
                }else{
                    llVal3.setBackgroundResource(R.drawable.box_selected);
                }
            }
        });

        etVal4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1){
                    etVal5.requestFocus();
                    clearOtpBgs();
                    llVal5.setBackgroundResource(R.drawable.box_selected);
                }else
                    llVal4.setBackgroundResource(R.drawable.box_selected);
            }
        });

        etVal5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1){
                    etVal6.requestFocus();
                    clearOtpBgs();
                    llVal6.setBackgroundResource(R.drawable.box_selected);
                }else
                    llVal5.setBackgroundResource(R.drawable.box_selected);
            }
        });

        etVal6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1){
                    hideKeyBoard(etVal6);
                }
            }
        });


        ShowAlertDialog("Alert","Automatically detecting OTP sent by SMS.Please wait...",R.drawable.alert);

        rvContinue.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView)
            {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 10000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                disableContinueButton();

                String val1 = etVal1.getText().toString().trim();
                String val2 = etVal2.getText().toString().trim();
                String val3 = etVal3.getText().toString().trim();
                String val4 = etVal4.getText().toString().trim();
                String val5 = etVal5.getText().toString().trim();
                String val6 = etVal6.getText().toString().trim();

                String finalOtp = val1+val2+val3+val4+val5+val6;


                if(TextUtils.isEmpty(val1)|| TextUtils.isEmpty(val2) ||
                    TextUtils.isEmpty(val3) || TextUtils.isEmpty(val4) ||
                        TextUtils.isEmpty(val5) || TextUtils.isEmpty(val6))
                {
                    ShowAlertDialog("Alert","Plase enter 6 digit OTP number.",R.drawable.alert);
                    enableContinueButton();
                }else if(!finalOtp.equals(otpvalue))
                {
                    ShowAlertDialog("Whoops!","Incorrect OTP. Please enter OTP send to your mobile.",R.drawable.worng);
                    enableContinueButton();
                }else
                {
                    callgetPatientsService();
                }
            }
        });

        rvResendOtp.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView)
            {
                if (Connectivity.isConnected(EnterOtpActivity.this))
                {
                    RestClient.getAPI("https://api.checkmobi.com").sendOPT(otpRequest, new RestCallback<OTPResponse>() {
                        @Override
                        public void failure(RestError restError)
                        {
                            enableContinueButton();
                            ShowAlertDialog("Whoops!","Failed to send OTP try again.",R.drawable.worng);
                        }

                        @Override
                        public void success(OTPResponse otpResponse, Response response) {
                            ShowAlertDialog("Alert","Automatically detecting OTP sent by SMS.Please wait...",R.drawable.alert);
                        }
                    });
                }else
                {
                    enableContinueButton();
                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                }
            }
        });
    }

    private void callgetPatientsService()
    {
        showLoader("");
        GetPatientsRequest getPatientsRequest = new GetPatientsRequest();
        getPatientsRequest.setPhoneNumber(RegisterReqSingleton.getSingleton().getPatientPhones().get(0).getPhNumber()+"");
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).getPatients(getPatientsRequest, new RestCallback<GetPatientsResponse>() {
            @Override
            public void failure(RestError restError)
            {
                hideLoader();
            }

            @Override
            public void success(GetPatientsResponse getPatientsResponse, Response response) {
                hideLoader();
                if(getPatientsResponse != null)
                {
                    Preference preference = new Preference(EnterOtpActivity.this);

                    if(getPatientsResponse.getCount()==0)
                    {
                        preference.saveBooleanInPreference(Preference.ISSINGLEPATIENT,true);
                    }else
                    {
                        preference.saveBooleanInPreference(Preference.ISSINGLEPATIENT,false);
                    }
                    preference.commitPreference();
                    {
                        final RegisterRequest registerRequest = RegisterReqSingleton.getSingleton();
                        registerRequest.setUSRId(registerRequest.getPatientDetails2().getMobileID());
                        if(GOOGLEID != null)
                        {
                            PatientLoginDetails patientLoginDetails = new PatientLoginDetails();
                            patientLoginDetails.setSocialID(GOOGLEID);
                             registerRequest.getPatientDetails2().setPatientLoginDetails(patientLoginDetails);
                        }
                        showLoader("");

                        if (otpRequest.getUSRId().equalsIgnoreCase("0"))
                        {
                            RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).patientRegister(registerRequest, new RestCallback<LoginResponse>() {
                                @Override
                                public void failure(RestError restError) {
                                    hideLoader();
                                    Log.e("ERROR", restError.getErrorMessage());
                                    ShowAlertDialog("Registration failed.");
                                }

                                @Override
                                public void success(LoginResponse loginResponse, Response response) {
                                    hideLoader();
//                                    if(loginResponse !=null && loginResponse.getArrPatients()!=null && !loginResponse.getArrPatients().isEmpty()) {
//                                        LoginResponseSingleton.getSingleton(loginResponse, EnterOtpActivity.this);
//                                        isRegisterSucess = true;
//                                        ShowAlertDialog("Successfully Registered.");
                                    if (ApplicationConstants.imageFile != null)
                                        callUploadProfilePic(loginResponse.getArrPatients().get(0).getPatientID(), registerRequest.getPatientDetails2().getPatientLoginID(), registerRequest.getPatientDetails2().getPatientPassword());
                                    else
                                        callLoginService(registerRequest.getPatientDetails2().getPatientLoginID(), registerRequest.getPatientDetails2().getPatientPassword());
//                                    }else
//                                    {
//                                        ShowAlertDialog("Registration failed.");
//                                    }

                                }
                            });
                        }else {

                            PatientUpdateRequest updateRequest = new PatientUpdateRequest();
                            updateRequest.setUSRId(Long.parseLong(otpRequest.getUSRId()));
                            updateRequest.setProfileImages(registerRequest.getProfileImages());
                            updateRequest.setPatientVitals(registerRequest.getPatientVitals());
                            updateRequest.setListPatientAddress(registerRequest.getPatientAddresses());
                            updateRequest.setListPatientPhone(registerRequest.getPatientPhones());
                            updateRequest.setListPatientEmail(registerRequest.getPatientEmails());

                            PatientDetails3 patientDetails3 = new PatientDetails3();
                            patientDetails3.setTitle(registerRequest.getPatientDetails2().getTitle());
                            patientDetails3.setFirstName(registerRequest.getPatientDetails2().getFirstName());
                            patientDetails3.setFullName(registerRequest.getPatientDetails2().getFullName());
                            patientDetails3.setDOB(registerRequest.getPatientDetails2().getDOB());
                            patientDetails3.setGender(registerRequest.getPatientDetails2().getGender()+"");
                            patientDetails3.setPatientID(Long.parseLong(otpRequest.getUSRId()));

                            final patientLogin loginDO = new patientLogin();
                            loginDO.setLoginID(registerRequest.getPatientDetails2().getPatientLoginID());
                            loginDO.setPassword(registerRequest.getPatientDetails2().getPatientPassword());

                            updateRequest.setPatientDetails3(patientDetails3);
                            updateRequest.setPatientLogin(loginDO);

                            RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).updatePatient(updateRequest, new RestCallback<LoginResponse>() {
                                @Override
                                public void failure(RestError restError) {
                                    hideLoader();
                                    Log.e("ERROR", restError.getErrorMessage());
                                    ShowAlertDialog("Registration failed.");
                                }

                                @Override
                                public void success(LoginResponse loginResponse, Response response) {
                                    hideLoader();
                                    if (ApplicationConstants.imageFile != null)
                                        callUploadProfilePic(loginResponse.getArrPatients().get(0).getPatientID(), loginDO.getLoginID(), loginDO.getPassword());
                                    else
                                        callLoginService(loginDO.getLoginID(), loginDO.getPassword());
                                }
                            });
                        }

                    }
                }
            }
        });
    }

    public void ShowAlertDialog(String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EnterOtpActivity.this);
        View dialogView = LayoutInflater.from(EnterOtpActivity.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        tvInfo.setText(message);
        tvInfo.setTextSize(18);

        tvYes.setText("OK");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.GONE);
        ivLogo.setVisibility(View.GONE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
                if(isRegisterSucess) {
                    Preference preference = new Preference(EnterOtpActivity.this);
                    preference.saveBooleanInPreference("ISLOGOUT", false);
                    preference.commitPreference();
                    ApplicationConstants.fileExtension = "";
                    ApplicationConstants.imageFile = null;

                    Intent slideactivity = new Intent(EnterOtpActivity.this, Specialities.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    slideactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(slideactivity, bndlanimation);
                    finishAffinity();
                }
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }

    public void ShowAlertDialog(String header,String message, int resourceid)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EnterOtpActivity.this);
        View dialogView = LayoutInflater.from(EnterOtpActivity.this).inflate(R.layout.home_pickup_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView tvHeading  =   (TextView) dialogView.findViewById(R.id.tvHeading);
        TextView  tvNo       =   (TextView) dialogView.findViewById(R.id.tvNo);
        TextView  tvYes      =   (TextView) dialogView.findViewById(R.id.tvYes);
        TextView  tvInfo     =   (TextView) dialogView.findViewById(R.id.tvInfo);
        ImageView ivLogo     =   (ImageView) dialogView.findViewById(R.id.ivLogo);

        tvNo.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvYes.setTypeface(ApplicationConstants.WALSHEIM_BOLD);
        tvHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvInfo.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        alertDialog = dialogBuilder.create();
        alertDialog.show();

        tvInfo.setText(message);
        tvInfo.setTextSize(18);
        tvHeading.setText(header);
        tvHeading.setTextSize(20);
        ivLogo.setBackgroundResource(resourceid);

        tvYes.setText("got it!");
        tvNo.setVisibility(View.GONE);
        tvHeading.setVisibility(View.VISIBLE);
        ivLogo.setVisibility(View.VISIBLE);

        tvYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.etVal1:
                if (hasFocus)
                    llVal1.setBackgroundResource(R.drawable.box_selected);
                else
                    llVal1.setBackgroundResource(R.drawable.box_unselected);
                break;
            case R.id.etVal2:
                if (hasFocus)
                    etVal2.setBackgroundResource(R.drawable.box_selected);
                else
                    etVal2.setBackgroundResource(R.drawable.box_unselected);
                break;
            case R.id.etVal3:
                if (hasFocus)
                    etVal3.setBackgroundResource(R.drawable.box_selected);
                else
                    etVal3.setBackgroundResource(R.drawable.box_unselected);
                break;
            case R.id.etVal4:
                if (hasFocus)
                    etVal4.setBackgroundResource(R.drawable.box_selected);
                else
                    etVal4.setBackgroundResource(R.drawable.box_unselected);
                break;
            case R.id.etVal5:
                if (hasFocus)
                    etVal5.setBackgroundResource(R.drawable.box_selected);
                else
                    etVal5.setBackgroundResource(R.drawable.box_unselected);
                break;
            case R.id.etVal6:
                if (hasFocus)
                    etVal6.setBackgroundResource(R.drawable.box_selected);
                else
                    etVal6.setBackgroundResource(R.drawable.box_unselected);
                break;
        }
    }

    public static class GRegisterSms extends BroadcastReceiver {
        // Get the object of SmsManager
        final SmsManager sms = SmsManager.getDefault();
        public String tmessage;

        public void onReceive(Context context, Intent intent) {

            // Retrieves a map of extended data from the intent.
            final Bundle bundle = intent.getExtras();
            try {
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    for (int i = 0; i < pdusObj.length; i++) {

                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                        String senderNum = phoneNumber;
                        String message = currentMessage.getDisplayMessageBody();

                        Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);
                        tmessage=message.substring(message.length()-6);

                        alertDialog.dismiss();
                        if (tmessage != null && tmessage.length() == 6) {
                            etVal1.setText(tmessage.charAt(0)+"");
                            etVal2.setText(tmessage.charAt(1)+"");
                            etVal3.setText(tmessage.charAt(2)+"");
                            etVal4.setText(tmessage.charAt(3)+"");
                            etVal5.setText(tmessage.charAt(4)+"");
                            etVal6.setText(tmessage.charAt(5)+"");
                        }
                    } // end for loop
                } // bundle is null
            } catch (Exception e) {
                Log.e("SmsReceiverhg", "Exception smsReceiver" +e);
            }
        }
    }
    public void clearOtpBgs() {
        llVal1.setBackgroundResource(R.drawable.box_unselected);
        llVal2.setBackgroundResource(R.drawable.box_unselected);
        llVal3.setBackgroundResource(R.drawable.box_unselected);
        llVal4.setBackgroundResource(R.drawable.box_unselected);
        llVal5.setBackgroundResource(R.drawable.box_unselected);
        llVal6.setBackgroundResource(R.drawable.box_unselected);
    }

    public void disableContinueButton() {
        rvContinue.setEnabled(false);
        rvContinue.setClickable(false);
        rvContinue.setFocusable(false);
        rvContinue.setFocusableInTouchMode(false);
    }

    public void enableContinueButton() {
        rvContinue.setEnabled(true);
        rvContinue.setClickable(true);
        rvContinue.setFocusable(true);
        rvContinue.setFocusableInTouchMode(true);
    }

    public void callLoginService(String userName, String Pwd) {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPatientLoginID(userName);
        if(GOOGLEID != null)
        {
            loginRequest.setSocialIDType("GOOGLE");
            loginRequest.setSocialID(GOOGLEID);
        }
        else
            loginRequest.setPatientPassword(Pwd);

        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).validateLogin(loginRequest, new RestCallback<LoginResponse>()
        {
            @Override
            public void failure(RestError restError)
            {
                Log.e("Error", restError.getErrorMessage());
                ShowAlertDialog("Registration failed.");
            }

            @Override
            public void success(LoginResponse loginResponse, Response response)
            {
                if (loginResponse != null)
                {
                    LoginResponseSingleton.getSingleton(loginResponse, EnterOtpActivity.this);
                    isRegisterSucess = true;

                    MyApplication myApplication = MyApplication.getIntance();
                    myApplication.setIdentity(loginResponse.getIdentity());
                    ShowAlertDialog("Successfully Registered.");
                }
            }
        });
    }
    public void callUploadProfilePic(long patientID, final String userName, final String password) {

        showLoader("");
//        final File photoFile = new File(uri.getPath());
        TypedFile image = new TypedFile("image/*",  ApplicationConstants.imageFile);
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).uploadProfilePic(image, ApplicationConstants.fileExtension, patientID+"", new RestCallback<AddUpdatePatientCaseDocumentImageResponce>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(final AddUpdatePatientCaseDocumentImageResponce responce1, Response response) {

               callLoginService(userName, password);
            }
        });

    }
}
