package hCue.Hcue;

import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.CaseDoctorDetails;
import hCue.Hcue.DAO.CaseHistory;
import hCue.Hcue.DAO.DoctorSpecialisation;
import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.DAO.PatientCaseRow;
import hCue.Hcue.DAO.PatientLabTest;
import hCue.Hcue.DAO.Vitals;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by shyamprasadg on 30/07/16.
 */
public class VisitSummary extends BaseActivity
{
    private ScrollView svVisits;
    private TextView   tvDoctorname,tvDate,tvTime,tvspeciality,tvClinic,tvTemperature,tvBloodPressure,tvFasting,tvWeight,
            tvHeight,tvBloodGroup,tvDiagnosisNotes, tvVisitReason, tvfollowupdays, tvfollowupnotes,tvVitalHeading,
            tvFollowUpDetails,tvLabTestHeading, tvNotesHeader ,tvMedicinePrescribed,tvDiagnosis,tvVisitReasonHeading;
    private PatientCaseRow patientCaseRow ;
    private LinearLayout llVisitReson, llDiagnosisNotes, llMedicinePrescribed, llLabTestPrescribed,
                         llLabTestAdd, llMedicineAdd, llFollowupdetails, llFollowUpAdd  ;
    @Override
    public void initialize()
    {
        svVisits    =   (ScrollView) getLayoutInflater().inflate(R.layout.visits_summary,null,false);
        llBody.addView(svVisits,baseLayoutParams);

        if(getIntent().hasExtra("SELECTEDCASE"))
        patientCaseRow = (PatientCaseRow) getIntent().getSerializableExtra("SELECTEDCASE");

        /*final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);*/

        llVisitReson            =   (LinearLayout) findViewById(R.id.llVisitReson);
        llDiagnosisNotes        =   (LinearLayout) findViewById(R.id.llDiagnosisNotes);
        llMedicinePrescribed    =   (LinearLayout) findViewById(R.id.llMedicinePrescribed);
        llLabTestPrescribed     =   (LinearLayout) findViewById(R.id.llLabTestPrescribed);
        llMedicineAdd           =   (LinearLayout) findViewById(R.id.llMedicineAdd);
        llLabTestAdd            =   (LinearLayout) findViewById(R.id.llLabTestAdd);
        llFollowupdetails       =   (LinearLayout) findViewById(R.id.llFollowupdetails);
        llFollowUpAdd           =   (LinearLayout) findViewById(R.id.llFollowUpAdd);

        tvfollowupdays      =   (TextView)  svVisits.findViewById(R.id.tvfollowupdays);
        tvfollowupnotes     =   (TextView)  svVisits.findViewById(R.id.tvfollowupnotes);
        tvVisitReason       =   (TextView)  svVisits.findViewById(R.id.tvVisitReason);
        tvDoctorname        =   (TextView)  svVisits.findViewById(R.id.tvDoctorname);
        tvDate              =   (TextView)  svVisits.findViewById(R.id.tvDate);
        tvTime              =   (TextView)  svVisits.findViewById(R.id.tvTime);
        tvspeciality        =   (TextView)  svVisits.findViewById(R.id.tvspeciality);
        tvClinic            =   (TextView)  svVisits.findViewById(R.id.tvClinic);
        tvTemperature       =   (TextView)  svVisits.findViewById(R.id.tvTemperature);
        tvBloodPressure     =   (TextView)  svVisits.findViewById(R.id.tvBloodPressure);
        tvFasting           =   (TextView)  svVisits.findViewById(R.id.tvFasting);
        tvWeight            =   (TextView)  svVisits.findViewById(R.id.tvWeight);
        tvHeight            =   (TextView)  svVisits.findViewById(R.id.tvHeight);
        tvBloodGroup        =   (TextView)  svVisits.findViewById(R.id.tvBloodGroup);
        tvDiagnosisNotes    =   (TextView)  svVisits.findViewById(R.id.tvDiagnosisNotes);
        tvVitalHeading      =   (TextView)  svVisits.findViewById(R.id.tvVitalHeading);
        tvFollowUpDetails   =   (TextView)  svVisits.findViewById(R.id.tvFollowUpDetails);
        tvLabTestHeading    =   (TextView)  svVisits.findViewById(R.id.tvLabTestHeading);
        tvMedicinePrescribed=   (TextView)  svVisits.findViewById(R.id.tvMedicinePrescribed);
        tvDiagnosis         =   (TextView)  svVisits.findViewById(R.id.tvDiagnosis);
        tvVisitReasonHeading=   (TextView)  svVisits.findViewById(R.id.tvVisitReasonHeading);
        tvNotesHeader       =   (TextView)  svVisits.findViewById(R.id.tvNotesHeader);

        setSpecificTypeFace(svVisits, ApplicationConstants.WALSHEIM_MEDIUM);
        tvTemperature.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvBloodPressure.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvFasting.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvWeight.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvHeight.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvBloodGroup.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvVitalHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvFollowUpDetails.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvLabTestHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvMedicinePrescribed.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvDiagnosis.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvVisitReasonHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        tvTopTitle.setText("VISITS");
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);

        CaseDoctorDetails caseDoctorDetails = patientCaseRow.getListCaseHistory().get(0).getPatientCase().getDoctorDetails();
        if(caseDoctorDetails != null)
        {
            if(caseDoctorDetails.getDoctorName().startsWith("Dr"))
                tvDoctorname.setText(caseDoctorDetails.getDoctorName());
            else
                tvDoctorname.setText("Dr."+caseDoctorDetails.getDoctorName());
            tvClinic.setText(caseDoctorDetails.getClinicName());
            StringBuilder specialitybuilder = new StringBuilder();
            for(DoctorSpecialisation doctorSpecialisation : caseDoctorDetails.getListDoctorSpecolisations())
                specialitybuilder.append(doctorSpecialisation.getSepcialityDesc()+", ");

            if(!specialitybuilder.toString().isEmpty())
                tvspeciality.setText(specialitybuilder.toString().substring(0,specialitybuilder.toString().length()-2));
        }

        if(patientCaseRow.getListCaseHistory() != null && !patientCaseRow.getListCaseHistory().isEmpty())
        {
           CaseHistory caseHistory =  patientCaseRow.getListCaseHistory().get(0);
            if(caseHistory.getPatientCase() != null)
            {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyy");
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setTimeInMillis(caseHistory.getPatientCase().getConsultationDt());
                tvDate.setText(simpleDateFormat.format(calendar.getTime()));
                tvTime.setText(caseHistory.getPatientCase().getStartTime() + " Hrs");
            }

            if(caseHistory.getPatientCase().getOtherVistReason() != null && !caseHistory.getPatientCase().getOtherVistReason().trim().isEmpty())
            {
                llVisitReson.setVisibility(View.VISIBLE);
                tvVisitReason.setText(caseHistory.getPatientCase().getOtherVistReason());
            }else
            {
                llVisitReson.setVisibility(View.GONE);
            }
            Vitals vitals = caseHistory.getPatientCase().getVitals();
            if(vitals != null)
            {
                if(vitals.getTMP() != null && !vitals.getTMP().isEmpty())
                tvTemperature.setText("" + vitals.getTMP() + (char) 0x00B0+"F");
                if(vitals.getBPL() != null && !vitals.getBPL().isEmpty())
                tvBloodPressure.setText("" + vitals.getBPL()+"/"+vitals.getBPH());
                if(vitals.getSFT() != null && !vitals.getSFT().isEmpty())
                tvFasting.setText(vitals.getSFT()+"/" + vitals.getSPP());
                if(vitals.getWGT() != null && !vitals.getWGT().isEmpty())
                tvWeight.setText("" + vitals.getWGT() + " Kgs");
                if(vitals.getHGT() != null && !vitals.getHGT().isEmpty())
                tvHeight.setText("" + vitals.getHGT()+" cms");
                if(vitals.getBLG() != null && !vitals.getBLG().isEmpty())
                tvBloodGroup.setText("" + vitals.getBLG());
            }
            if(caseHistory.getPatientCase() != null && caseHistory.getPatientCase().getDiagonsticNotes()!= null
                    && !caseHistory.getPatientCase().getDiagonsticNotes().trim().isEmpty())
            {
                llDiagnosisNotes.setVisibility(View.VISIBLE);
                tvDiagnosisNotes.setText(caseHistory.getPatientCase().getDiagonsticNotes());
            }else
            {
                llDiagnosisNotes.setVisibility(View.GONE);
            }
            if(caseHistory.getListPatientCasePrescribObj() != null && !caseHistory.getListPatientCasePrescribObj().isEmpty())
            {
                llMedicinePrescribed.setVisibility(View.VISIBLE);
                ArrayList<PatientCasePrescribObj> listPatientCasePrescribObj = caseHistory.getListPatientCasePrescribObj();
                for(PatientCasePrescribObj patientCasePrescribObj : listPatientCasePrescribObj)
                {
                    View view =  LayoutInflater.from(VisitSummary.this).inflate(R.layout.medicinecell,null);
                    TextView tvMedicineName = (TextView) view.findViewById(R.id.tvMedicineName);
                    TextView tvDuration     = (TextView) view.findViewById(R.id.tvDuration);
                    TextView tvDosage       = (TextView) view.findViewById(R.id.tvDosage);
                    TextView tvWhen         = (TextView) view.findViewById(R.id.tvWhen);
                    TextView tvNotes        = (TextView) view.findViewById(R.id.tvNotes);
                    tvMedicineName.setText(patientCasePrescribObj.getMedicine());
                    setSpecificTypeFace((ViewGroup) view,ApplicationConstants.WALSHEIM_MEDIUM);
                    tvMedicineName.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                    tvDuration.setText(patientCasePrescribObj.getNumberofDays()+" Days");
                    tvDosage.setText(patientCasePrescribObj.getDosage1()+"-"+patientCasePrescribObj.getDosage2()+"-"+patientCasePrescribObj.getDosage4());
                    if(MedicationSummary.checkIsMedicineType(patientCasePrescribObj.getMedicineType()))
                    {
                        if (patientCasePrescribObj.isBeforeAfter())
                            tvWhen.setText("After Meal");
                        else
                            tvWhen.setText("Before Meal");
                    }else
                    {
                        tvWhen.setText("--");
                    }
                    llMedicineAdd.addView(view);
                    if(patientCasePrescribObj.getDiagnostic() != null)
                    tvNotes.setText("Dosage Notes : "+patientCasePrescribObj.getDiagnostic());

                }

            }else
            {
                llMedicinePrescribed.setVisibility(View.GONE);
            }


            if(caseHistory.getListpatientLabTest() != null && !caseHistory.getListpatientLabTest().isEmpty() && caseHistory.getListpatientLabTest().size() !=0)
            {
                llLabTestPrescribed.setVisibility(View.VISIBLE);
                ArrayList<PatientLabTest> listPatientLabTest  = caseHistory.getListpatientLabTest();
                for (PatientLabTest patientLabTest : listPatientLabTest)
                {
                    View view =  LayoutInflater.from(VisitSummary.this).inflate(R.layout.labcell,null);
                    TextView tvDisease = (TextView) view.findViewById(R.id.tvDisease);
                    TextView tvDescription     = (TextView) view.findViewById(R.id.tvDescription);
                    tvDisease.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                    tvDescription.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                    tvDisease.setText(patientLabTest.getLabTestName());
                    tvDescription.setText(patientLabTest.getLabNotes());
                    llLabTestAdd.addView(view);
                }

            }else
            {
                llLabTestPrescribed.setVisibility(View.GONE);
            }

            if(caseHistory.getPatientCase().getFollowUpDetails() != null)
            {
                tvfollowupdays.setText(caseHistory.getPatientCase().getFollowUpDetails().getFollowUpDay() != 0 ?caseHistory.getPatientCase().getFollowUpDetails().getFollowUpDay()+" days" : "- days");
                if(caseHistory.getPatientCase().getFollowUpDetails().getFollowUpNotes() != null && !caseHistory.getPatientCase().getFollowUpDetails().getFollowUpNotes().equalsIgnoreCase("") ){
                    tvNotesHeader.setVisibility(View.VISIBLE);
                    tvfollowupnotes.setVisibility(View.VISIBLE);
                    tvfollowupnotes.setText(caseHistory.getPatientCase().getFollowUpDetails().getFollowUpNotes());
                }

                else{
                    tvNotesHeader.setVisibility(View.GONE);
                    tvfollowupnotes.setVisibility(View.GONE);
                }

                llFollowupdetails.setVisibility(View.VISIBLE);
            }else
            {
                llFollowupdetails.setVisibility(View.GONE);
            }
        }

    }
}
