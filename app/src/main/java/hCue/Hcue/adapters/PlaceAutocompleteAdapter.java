package hCue.Hcue.adapters;

/**
 * Created by Bharath on 8/1/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.RecyclerView;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hCue.Hcue.BaseActivity;
import hCue.Hcue.LocationSearchActivity;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Locationtext;
import hCue.Hcue.utils.Preference;

import static hCue.Hcue.R.string.address;

/**
 * Created by anuj.sharma on 4/6/2016.
 */
public class PlaceAutocompleteAdapter extends RecyclerView.Adapter<PlaceAutocompleteAdapter.PlaceViewHolder> implements Filterable{

    public interface PlaceAutoCompleteInterface
    {
        public void onPlaceClick(ArrayList<PlaceAutocomplete> mResultList, int position);
    }
    Locationtext locationtext;
    Context mContext;
    PlaceAutoCompleteInterface mListener;
    private static final String TAG = "PlaceAutocompleteAdapter";
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    ArrayList<PlaceAutocomplete> mResultList;

    private GoogleApiClient mGoogleApiClient;

    private LatLngBounds mBounds;

    private int layout;

    private AutocompleteFilter mPlaceFilter;
    private TextView textView;


    public PlaceAutocompleteAdapter(Context context, int resource, GoogleApiClient googleApiClient,
                                    LatLngBounds bounds, AutocompleteFilter filter, TextView tvLocation){
        locationtext = (Locationtext) context;
        this.mContext = context;
        layout = resource;
        mGoogleApiClient = googleApiClient;
        mBounds = bounds;
        mPlaceFilter = filter;
       // this.mListener = (PlaceAutoCompleteInterface)mContext;
        this.textView = tvLocation;
    }

    /*
    Clear List items
     */
    public void clearList(){
        if(mResultList!=null && mResultList.size()>0){
            mResultList.clear();
        }
    }


    /**
     * Sets the bounds for all subsequent queries.
     */
    public void setBounds(LatLngBounds bounds) {
        mBounds = bounds;
    }

    @Override
    public Filter getFilter()
    {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    mResultList = getAutocomplete(constraint);
                    if (mResultList != null) {
                        // The API successfully returned results.
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    notifyDataSetChanged();
                } else {
                    // The API did not return any results, invalidate the data set.
                    //notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private ArrayList<PlaceAutocomplete> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            Log.i("", "Starting autocomplete query for: " + constraint);

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    mBounds, mPlaceFilter);

            // This method should have been called off the main UI thread. Block and wait for at most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
//                Toast.makeText(mContext, "Error contacting API: " + status.toString(),
//                        Toast.LENGTH_SHORT).show();
                Log.e("", "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.i("", "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");

            // Copy the results into our own data structure, because we can't hold onto the buffer.
            // AutocompletePrediction objects encapsulate the API response (place ID and description).

            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                // Get the details of this prediction and copy it into a new PlaceAutocomplete object.
                resultList.add(new PlaceAutocomplete(prediction.getPlaceId(),
                        prediction.getFullText( STYLE_BOLD)));
            }

            // Release the buffer now that all data has been copied.
            autocompletePredictions.release();

            return resultList;
        }
        Log.e("", "Google API client is not connected for autocomplete query.");
        return null;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(layout, viewGroup, false);
        PlaceViewHolder mPredictionHolder = new PlaceViewHolder(convertView);
        return mPredictionHolder;
    }


    @Override
    public void onBindViewHolder(PlaceViewHolder mPredictionHolder, final int i) {

        String address_ = mResultList.get(i).description.toString();
        if(address_.contains(",")) {
            String address1[] = address_.split(",");
            if (address1.length == 2)
            {
                address_ = address1[0] +" , "+ address1[1];
                mPredictionHolder.tvAddress.setText(address1[0]);
                mPredictionHolder.tvAddress2.setText(address1[1]);
            } else if (address1.length == 3)
            {
                address_ = address1[0] + " , " + address1[1];
                mPredictionHolder.tvAddress.setText(address1[0]);
                mPredictionHolder.tvAddress2.setText(address1[1]+","+address1[2]);
            }
            else if (address1.length == 4)
            {
                address_ = address1[0] + " , " + address1[1];
                mPredictionHolder.tvAddress.setText(address1[0]);
                mPredictionHolder.tvAddress2.setText(address1[1]+","+address1[2]+","+address1[3]);
            }
            else if (address1.length == 5) {
                address_ = address1[1] + " , " + address1[2];
                mPredictionHolder.tvAddress.setText(address1[0]);
                mPredictionHolder.tvAddress2.setText(address1[1]+","+address1[2]+","+address1[3]+","+address1[4]);
            } else if (address1.length == 6)
            {
                address_ = address1[2] + " , " + address1[3];
                mPredictionHolder.tvAddress.setText(address1[0]);
                mPredictionHolder.tvAddress2.setText(address1[1]+","+address1[2]+","+address1[3]+","+address1[4]+","+address1[5]);
            } else if (address1.length == 7) {
                address_ = address1[3] + " , " + address1[4];
                mPredictionHolder.tvAddress.setText(address1[0]);
                mPredictionHolder.tvAddress2.setText(address1[1] + "," + address1[2] + "," + address1[3] + "," + address1[4] + "," + address1[5]+","+address1[6]);
            }
        }
        else{
            mPredictionHolder.tvAddress.setText(address_);
        }

        mPredictionHolder.mParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String addressFull, address = null;
                String address2 = addressFull = mResultList.get(i).description.toString();
                /*if(address.contains(",")) {
                    String address1[] = address.split(",");
                    if (address1.length == 2) {
                        address = address1[0] +" , "+ address1[1];
                    } else if (address1.length == 3)
                        address = address1[0] +" , "+ address1[1];
                    else if (address1.length == 4)
                        address =address1[0] +" , "+ address1[1];
                    else if (address1.length == 5)
                        address =address1[1] +" , "+ address1[2];
                    else if (address1.length == 6)
                        address = address1[2] +" , "+ address1[3];
                    else if (address1.length == 6)
                        address = address1[3] +" , "+ address1[4];

                }*/

                String area = address2;
                String city = address2;
                String state = address2;
                if (address2.contains(",")) {
                    String address1[] = address2.split(",");
                    if (address1.length == 2) {
                        address = address1[0] + " , " + address1[1];
                        area = address1[0];
                    } else if (address1.length == 3) {
                        address = address1[0] + " , " + address1[1];
                        area = address1[0];
                        city = address1[0];
                        state = address1[1];
                    }
                    else if (address1.length == 4) {
                        address = address1[0] + " , " + address1[1];
                        area = address1[0];
                        city = address1[1];
                        state = address1[2];
                    }
                    else if (address1.length == 5) {
                        address = address1[1] + " , " + address1[2];
                        area = address1[1];
                        city = address1[2];
                        state = address1[3];
                    }
                    else if (address1.length == 6) {
                        address = address1[2] + " , " + address1[3];
                        area = address1[2];
                        city = address1[3];
                        state = address1[4];
                    }
                    else if (address1.length == 6) {
                        address = address1[3] + " , " + address1[4];
                        area = address1[3];
                        city = address1[4];
                        state = address1[5];
                    }

                }

                if (!((LocationSearchActivity)mContext).isFromProfile && !((LocationSearchActivity)mContext).isFromContactDetails){
                    locationtext.setlocation(address);
                }
            getLocationFromAddress(addressFull, address2, area, city, state);
            }
        });

    }

    @Override
    public int getItemCount() {
        if(mResultList != null)
            return mResultList.size();
        else
            return 0;
    }

    public PlaceAutocomplete getItem(int position) {
        return mResultList.get(position);
    }

    /*
    View Holder For Trip History
     */
    public class PlaceViewHolder extends RecyclerView.ViewHolder {
        //        CardView mCardView;
        public LinearLayout mParentLayout;
        public TextView tvAddress,tvAddress2;

        public PlaceViewHolder(View itemView) {
            super(itemView);
            mParentLayout = (LinearLayout)itemView.findViewById(R.id.predictedRow);
            tvAddress = (TextView)itemView.findViewById(R.id.tvAddress);
            tvAddress2 = (TextView)itemView.findViewById(R.id.tvAddress2);

            tvAddress.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvAddress2.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        }

    }

    /**
     * Holder for Places Geo Data Autocomplete API results.
     */
    public class PlaceAutocomplete {

        public CharSequence placeId;
        public CharSequence description;

        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }

    public void getLocationFromAddress(String strAddress, String profileAddress, String area, String city, String state){

        Geocoder coder = new Geocoder(mContext);
        String cityName = null, countryCode = null, countryName = null, postalCode = null,fullAddress, stateName = null;
        List<Address> address;
        Barcode.GeoPoint p1 = null;

        try {
            address = coder.getFromLocationName(strAddress,1);
            if (address==null)
            {
            }
            Address location=address.get(0);
            double lat = location.getLatitude();
            double lon = location.getLongitude();

            android.location.Address returnedAddress = address.get(0);
            StringBuilder strReturnedAddress = new StringBuilder("Address:\n");
            for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
            }
            cityName = location.getLocality();
            stateName = location.getAdminArea();
            location.getCountryCode();
            location.getCountryName();
            location.getPostalCode();


            ((BaseActivity)mContext).tvLocation.setTag(R.string.latlng, location.getLatitude()+", "+location.getLongitude());

            Preference preference = new Preference(mContext);
            if (((LocationSearchActivity)mContext).isFromProfile){
                preference.saveStringInPreference("CURRENTLATLONG",location.getLatitude()+", "+location.getLongitude());
                preference.saveStringInPreference("PROFILE_LATLONG",location.getLatitude()+", "+location.getLongitude());
                preference.saveStringInPreference("PROFILE_ADDRESS", profileAddress);
                preference.saveStringInPreference("CITY_NAME", cityName);
                preference.saveStringInPreference("STATE", stateName);
                preference.commitPreference();
            }else if(((LocationSearchActivity)mContext).isFromContactDetails){
                preference.saveStringInPreference("CONTACT_LATLONG",lat+", "+lon);
                preference.saveStringInPreference("CONTACT_ADDRESS", strReturnedAddress.toString());
                preference.saveStringInPreference("CONTACT_COUNTRY_CODE", countryCode);
                preference.saveStringInPreference("CONTACT_COUNTRY_NAME", countryName);
                preference.saveStringInPreference("CONTACT_POST_CODE", postalCode);
                preference.saveStringInPreference("CONTACT_CITY_NAME", cityName);
                preference.saveStringInPreference("CONTACT_STATE", stateName);
                preference.saveStringInPreference("AREA", area);
                preference.saveStringInPreference("CITY", city);
                preference.saveStringInPreference("STATE", state);
                preference.commitPreference();
            }
            else {
                preference.saveStringInPreference("CURRENTLATLONG",location.getLatitude()+", "+location.getLongitude());
                preference.saveStringInPreference("LOCALITY",stateName+", "+cityName);
                ((BaseActivity)mContext).tvLocation.setText(stateName+", "+cityName);
                preference.commitPreference();
            }

            ((Activity)mContext).finish();

        }catch (Exception e)
        {

        }
    }

}
