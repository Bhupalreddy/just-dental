package hCue.Hcue.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import hCue.Hcue.DAO.SelectedConsultation;
import hCue.Hcue.DAO.SlotList;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.DateUtils;
import hCue.Hcue.utils.SelectedConsSingleton;

/**
 * Created by shyamprasadg on 23/06/16.
 */
public class TimeSlotAdapter extends BaseAdapter
{
    private Context context;
    private View selectedview ;
    private ArrayList<SlotList> eveningslotLists ;
    private int AddressConsultId ;
    private TextView tvSubmit;
    int SelectedEveningpoition;
    public TimeSlotAdapter(Context context, ArrayList<SlotList> eveningslotLists, TextView tvSubmit)
    {
        this.context = context;
        this.eveningslotLists = eveningslotLists ;
        this.tvSubmit   =   tvSubmit;
    }
    @Override
    public int getCount()
    {
        if(eveningslotLists == null)
            return 0;
        else
            return eveningslotLists.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        convertView = LayoutInflater.from(context).inflate(R.layout.time_slot_cell, null);
        final TextView tvSlots = (TextView) convertView.findViewById(R.id.tvSlots);
        tvSlots.setBackgroundResource(R.drawable.time_slot_selection_white);
        tvSlots.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvSlots.setText(eveningslotLists.get(position).getStartTime());
        final View finalConvertView = convertView;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String str = sdf.format(new Date());
        String currenthour = str.split(":")[0];
        String currentmin = str.split(":")[1];
        String hour = eveningslotLists.get(position).getStartTime().split(":")[0];
        String min = eveningslotLists.get(position).getStartTime().split(":")[1];
        if(DateUtils.isToday(SelectedConsSingleton.getSingleton().getSelected_date().getTime()))
            if(Integer.parseInt(hour)<Integer.parseInt(currenthour))
            {
//                convertView.setBackgroundResource(R.drawable.hcue_graybutton);
                convertView.setClickable(false);
                convertView.setEnabled(false);
                tvSlots.setTextColor(Color.GRAY);
//                tvSlots.setPaintFlags(tvSlots.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
            }else if(Integer.parseInt(hour)== Integer.parseInt(currenthour))
            {
                if(Integer.parseInt(min)<Integer.parseInt(currentmin))
                {
//                    convertView.setBackgroundResource(R.drawable.hcue_graybutton);
                    convertView.setClickable(false);
                    convertView.setEnabled(false);
                    tvSlots.setTextColor(Color.GRAY);
//                    tvSlots.setPaintFlags(tvSlots.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                }

            }
        if(selectedview!=null&& SelectedEveningpoition>=0)
            if(position ==SelectedEveningpoition) {
                tvSlots.setTextColor(Color.WHITE);
                tvSlots.setBackgroundResource(R.drawable.time_slot_selection);
            }

        SlotList slotList = eveningslotLists.get(position);
        if(slotList.getAvailable()=='N')
        {
//            convertView.setBackgroundResource(R.drawable.hcue_redroundbutton);
            convertView.setClickable(false);
            convertView.setEnabled(false);
            tvSlots.setTextColor(Color.parseColor("#B5171C"));
        }

        convertView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SelectedEveningpoition = position;
                tvSubmit.setVisibility(View.VISIBLE);

                SelectedConsultation selectedConsultation =  SelectedConsSingleton.getSingleton();
                selectedConsultation.setStarttime(eveningslotLists.get(position).getStartTime());
                selectedConsultation.setEndtime(eveningslotLists.get(position).getEndTime());
                selectedConsultation.setAddressConsultId(eveningslotLists.get(position).getAddressConsultid());
                if(eveningslotLists.get(position).getHospitalCode()!= null)
                selectedConsultation.setHospitalcode(eveningslotLists.get(position).getHospitalCode());
                if(eveningslotLists.get(position).getBranchCode()!= null)
                selectedConsultation.setBranchcode(eveningslotLists.get(position).getBranchCode());

                if(eveningslotLists.get(position).getHospitalID()!= 0)
                    selectedConsultation.setHospitalID(eveningslotLists.get(position).getHospitalID());
                if(eveningslotLists.get(position).getHospitalParentID()!= 0)
                    selectedConsultation.setParentHospitalID(eveningslotLists.get(position).getHospitalParentID());

                if(selectedview == null)
                {
                    tvSlots.setTextColor(Color.WHITE);
                    tvSlots.setBackgroundResource(R.drawable.time_slot_selection);
                    selectedview = tvSlots;
                }else
                {
                    selectedview.setBackgroundResource(R.drawable.time_slot_selection_white);
                    ((TextView)selectedview.findViewById(R.id.tvSlots)).setTextColor(Color.parseColor("#4ebb50"));
                    tvSlots.setTextColor(Color.WHITE);
                    tvSlots.setBackgroundResource(R.drawable.time_slot_selection);
                    selectedview = tvSlots;
                }
                notifyDataSetChanged();
            }
        });

        return convertView;


    }

    public void refreshAdapter(ArrayList<SlotList> listslotLists, int addressConsultID)
    {
        AddressConsultId = addressConsultID ;
        eveningslotLists = listslotLists;
        selectedview = null;
        notifyDataSetChanged();
    }

    public void removeviewAdapter()
    {
        selectedview = null;
        notifyDataSetChanged();
        SelectedConsSingleton.getSingleton().setStarttime(null);
    }
}
