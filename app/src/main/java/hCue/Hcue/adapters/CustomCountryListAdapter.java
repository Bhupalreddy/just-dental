package hCue.Hcue.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hCue.Hcue.DAO.CountryCodeDO;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;

public class CustomCountryListAdapter extends BaseAdapter {

    private ArrayList<CountryCodeDO> listData;

    private LayoutInflater layoutInflater;

    public CustomCountryListAdapter(Context context, ArrayList<CountryCodeDO> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.coutry_list_row, null);
            holder = new ViewHolder();
            holder.tvCountry = (TextView) convertView.findViewById(R.id.tvCountry);
            holder.tvCode = (TextView) convertView.findViewById(R.id.tvCode);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvCountry.setText(listData.get(position).getCountryName().toString());
        holder.tvCode.setText("+"+listData.get(position).getCountrycode());
        holder.tvCode.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        holder.tvCountry.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        return convertView;
    }

    static class ViewHolder {
        TextView tvCountry;
        TextView tvCode;
    }
}