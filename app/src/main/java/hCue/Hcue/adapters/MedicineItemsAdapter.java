package hCue.Hcue.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import hCue.Hcue.DAO.OrderImageDocuments;
import hCue.Hcue.DAO.OrderTextDocuments;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by shyamprasadg on 17/11/16.
 */

public class MedicineItemsAdapter extends RecyclerView.Adapter<MedicineItemsAdapter.MedicineItemsViewHolder>
{
    private Context context;
    private ArrayList<OrderTextDocuments> listMedicines = new ArrayList<>();

    public MedicineItemsAdapter(Context context, ArrayList<OrderTextDocuments> listMedicines)
    {
        this.context    =   context;
        this.listMedicines = listMedicines;
    }

    @Override
    public MedicineItemsAdapter.MedicineItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.medicine_items_cell, parent, false);
        return new MedicineItemsAdapter.MedicineItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MedicineItemsViewHolder holder, int position)
    {

        OrderTextDocuments orderTextDocuments = listMedicines.get(position);

        if (orderTextDocuments.getMedicineStrength() != null && !orderTextDocuments.getMedicineStrength().equalsIgnoreCase(""))
            holder.tvMedicineName.setText(orderTextDocuments.getMedicineName() +" - "+ orderTextDocuments.getMedicineStrength());
        else
            holder.tvMedicineName.setText(orderTextDocuments.getMedicineName());

        holder.tvMedicineQuantity.setText("Qty("+orderTextDocuments.getRequiredQuantity()+")");

        holder.tvMedicineName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        holder.tvMedicineQuantity.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
//        holder.tvDummy.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
    }

    @Override
    public int getItemCount()
    {
        if (listMedicines != null && listMedicines.size() > 0)
            return listMedicines.size();
        else
            return 0;
    }

    public class MedicineItemsViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvMedicineName,tvMedicineQuantity,tvDummy;

        public MedicineItemsViewHolder(View itemView)
        {
            super(itemView);
            tvMedicineName      =   (TextView)      itemView.findViewById(R.id.tvMedicineName);
            tvMedicineQuantity  =   (TextView)      itemView.findViewById(R.id.tvMedicineQuantity);
//            tvDummy             =   (TextView)      itemView.findViewById(R.id.tvDummy);


        }

    }
}
