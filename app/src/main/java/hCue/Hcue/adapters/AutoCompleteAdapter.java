package hCue.Hcue.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

import hCue.Hcue.BookAppointment;
import hCue.Hcue.DAO.DoctorSpecialityDO;
import hCue.Hcue.DoctorSearch;
import hCue.Hcue.HospitalInfo;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.MyautoScroolView;

/**
 * Created by Appdest on 25-06-2016.
 */
public class AutoCompleteAdapter extends ArrayAdapter<DoctorSpecialityDO>
{
/*    private ArrayList<SpecialityResRow> specialityResRowArrayList ;
    public ArrayList<SpecialityResRow> filteredspecialityResRowArrayList = new ArrayList<>();*/
    private ArrayList<DoctorSpecialityDO> originalArrayList = new ArrayList<>();
//    public ArrayList<DoctorSpecialityDO> filteredArrayList = new ArrayList<>();
    private MyautoScroolView actDoctors;
    private int count = 0;
 /*   public AutoCompleteAdapter(Context context, int resource,  ArrayList<SpecialityResRow> specialityResRowArrayList)
    {
        super(context, resource, specialityResRowArrayList);
        this.specialityResRowArrayList = specialityResRowArrayList ;
    }*/

    public AutoCompleteAdapter(Context context, int resource, ArrayList<DoctorSpecialityDO> strings, MyautoScroolView actDoctors) {
        super(context, resource, strings);
        //originalArrayList.addAll(strings);
        this.actDoctors = actDoctors ;
    }

    @Override
    public int getCount() {
        return originalArrayList.size();
    }

    @Override
    public Filter getFilter() {
        return new SpecialtyDoctorFilter(this, originalArrayList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item from filtered list.
        DoctorSpecialityDO specialityResRow = originalArrayList.get(position);

        // Inflate your custom row layout as usual.
        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.search_list_item, parent, false);

        TextView tvSearchname         =   (TextView)  convertView.findViewById(R.id.tvSearchname);
        TextView tvSpeciality         =   (TextView)  convertView.findViewById(R.id.tvSpeciality);
        TextView tvCaption         =   (TextView)  convertView.findViewById(R.id.tvCaption);

        if (specialityResRow.getCategory().equalsIgnoreCase("Symptom"))
            tvSearchname.setText(specialityResRow.getSymptomName());
        else
            tvSearchname.setText(specialityResRow.getName());

        tvSpeciality.setText(specialityResRow.getCategory());
        if (specialityResRow.getCategory().equalsIgnoreCase("Doctor") || specialityResRow.getCategory().equalsIgnoreCase("Symptom"))
            tvCaption.setText(specialityResRow.getSpecialityName());
        else if (specialityResRow.getCategory().equalsIgnoreCase("Clinic") )
            tvCaption.setText(specialityResRow.getHospitalloacation());

        tvSearchname.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvSpeciality.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        convertView.setTag(specialityResRow);
        final View finalConvertView = convertView;
        convertView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    actDoctors.onKeyPreIme(KeyEvent.KEYCODE_BACK,new KeyEvent(KeyEvent.KEYCODE_BACK,KeyEvent.KEYCODE_BACK));
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    DoctorSpecialityDO specialityResRow = (DoctorSpecialityDO) finalConvertView.getTag();
                 //   actDoctors.setText(specialityResRow.getName());
                    actDoctors.setText("");
                    if(specialityResRow.getCategory().equalsIgnoreCase("Clinic"))
                    {
                        Intent intent = new Intent(getContext(), HospitalInfo.class);
                        intent.putExtra("HospitalDetails",specialityResRow);
                        getContext().startActivity(intent);
                    }else {
                        Intent intent = new Intent(getContext(), BookAppointment.class);
                        if (specialityResRow.getCategory().toString().equalsIgnoreCase("Doctor"))
                            intent.putExtra("DOCTORNAME", specialityResRow.getName());
                        else if (specialityResRow.getCategory().toString().equalsIgnoreCase("Service")){
                            intent.putExtra("DoctorID", specialityResRow.getSpecialityID());
                            intent.putExtra("ServiceName", specialityResRow.getName());
                        }
                        else {
                            intent.putExtra("SPECIALITY", specialityResRow.getSpecialityID());
                            if (specialityResRow.getCategory().toString().equalsIgnoreCase("Symptom"))
                                intent.putExtra("SPECIALITY_NAME", specialityResRow.getSymptomName());
                            else
                                intent.putExtra("SPECIALITY_NAME", specialityResRow.getName());
                        }

                        getContext().startActivity(intent);
                    }
                }


                return false;
            }
        });
        return convertView;
    }

    public void refreshAdapter(ArrayList<DoctorSpecialityDO> values/*, Set<DoctorSpecialityDO> strings*/)
    {
        if(originalArrayList!= null)
            originalArrayList.clear();
//        originalArrayList.addAll(strings);
        originalArrayList.addAll(values);
        notifyDataSetChanged();
    }
}
