package hCue.Hcue.adapters;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hCue.Hcue.BookAppointment;
import hCue.Hcue.ChooseDateTime;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.DoctorProfileActivity;
import hCue.Hcue.R;
import hCue.Hcue.model.DoctorDetailsResponse;
import hCue.Hcue.model.GetDoctorDetailsRequest;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.LoginResponseSingleton;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.widget.RippleView;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 22/06/16.
 */
public class BookingAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<SpecialityResRow> specialityResRowArrayList ;
    private Double currentlat, currentlong ;
    public BookingAdapter(Context context)
    {
        this.context    = context;
    }

    public BookingAdapter(Context context, ArrayList<SpecialityResRow> specialityResRowArrayList)
    {
        Preference preference = new Preference(context);
        String latlong = preference.getStringFromPreference("CURRENTLATLONG","");
        if(latlong.contains(","))
        {
            currentlat  = Double.parseDouble(latlong.split(",")[0]);
            currentlong = Double.parseDouble(latlong.split(",")[1]);
        }
        this.context    = context;
        this.specialityResRowArrayList = specialityResRowArrayList ;
    }

    public void refresh(ArrayList<SpecialityResRow> vecSpecialityRows)
    {
        this.specialityResRowArrayList=vecSpecialityRows;
        notifyDataSetChanged();
    }

    @Override
    public int getCount()

    {
        if(specialityResRowArrayList == null)
            return 0;
        else
        return specialityResRowArrayList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {

        public ImageView       ivDoctorPic;
        public TextView        tvDoctorname;
        public TextView        tvDoctorQualification;
        //public TextView        tvspecialist;
        //public TextView        tvHospitalName;
        public TextView         tvDistance;
        public TextView        tvExperince;
        //public TextView        tvSymbol ;
        //public TextView        tvFee ;
        public TextView        tvExpLable;
        //public TextView        tvFeeLabel;
        public TextView        tvBookAppointment;
        public TextView        tvYrs;
        public TextView         tvArea ;
        public LinearLayout    llProfileData ;
        public LinearLayout    llBook;
        public RippleView rvClick;
        public RippleView      rvBook;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.doctor_list, parent, false);
            holder = new ViewHolder();

            holder.ivDoctorPic             =   (ImageView)     convertView.findViewById(R.id.ivDoctorPic);
            holder.tvDoctorname            =   (TextView)      convertView.findViewById(R.id.tvDoctorname);
            holder.tvDoctorQualification   =   (TextView)      convertView.findViewById(R.id.tvDoctorQualification);
            //holder.tvspecialist            =   (TextView)      convertView.findViewById(R.id.tvspecialist);
            //holder.tvHospitalName          =   (TextView)      convertView.findViewById(R.id.tvHospitalName);
            holder.tvDistance              =   (TextView)      convertView.findViewById(R.id.tvDistance);
            holder.tvExperince             =   (TextView)      convertView.findViewById(R.id.tvExperince);
            //holder.tvSymbol                =   (TextView)      convertView.findViewById(R.id.tvSymbol);
            //holder.tvFee                   =   (TextView)      convertView.findViewById(R.id.tvFee);
            //holder.tvFeeLabel              =   (TextView)      convertView.findViewById(R.id.tvFeeLabel);
            holder.tvExpLable              =   (TextView)      convertView.findViewById(R.id.tvExpLable);
            holder.tvBookAppointment       =   (TextView)      convertView.findViewById(R.id.tvBookAppointment);
            holder.tvYrs                   =   (TextView)      convertView.findViewById(R.id.tvYrs);
            holder.tvArea                  =   (TextView)      convertView.findViewById(R.id.tvArea);
            holder.llProfileData           =   (LinearLayout)  convertView.findViewById(R.id.llProfileData);
            holder.llBook                  =   (LinearLayout)  convertView.findViewById(R.id.llBook);
            holder.rvClick                 =   (RippleView)    convertView.findViewById(R.id.rvClick);
            holder.rvBook                  =   (RippleView)    convertView.findViewById(R.id.rvBook);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        holder.tvBookAppointment.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        holder.tvArea.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        //holder.tvSymbol.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        //holder.tvspecialist.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        holder.tvDoctorQualification.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        //holder.tvHospitalName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        holder.tvDistance.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        holder.tvExperince.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        holder.tvYrs.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        //holder.tvFee.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        //holder.tvFeeLabel.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        holder.tvExpLable.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);


        final SpecialityResRow specialityResRow = specialityResRowArrayList.get(position);
        if(specialityResRow.getProspect().equalsIgnoreCase("Y"))
        {
            holder.tvBookAppointment.setText("Call");
            holder.tvBookAppointment.setBackgroundResource(R.drawable.green_slot_bg);
            holder.tvBookAppointment.setTextColor(Color.parseColor("#4ebb50"));

        }
        else
        {
            holder.tvBookAppointment.setText("Book");
            holder.tvBookAppointment.setBackgroundResource(R.drawable.green_fullslot_bg);
            holder.tvBookAppointment.setTextColor(Color.parseColor("#FFFFFF"));
        }
        if(specialityResRow.getLocation() != null)
        {
            holder.tvArea.setText(specialityResRow.getLocation());
        }
        else
            holder.tvArea.setText("");

        if(specialityResRow.getProfileImage() != null && !specialityResRow.getProfileImage().isEmpty())
            Picasso.with(context).load(Uri.parse(specialityResRow.getProfileImage())).into(holder.ivDoctorPic);
        else
        {
            if(specialityResRow.getGender() == 'M')
            Picasso.with(context).load(R.drawable.male).into(holder.ivDoctorPic);
            else
            Picasso.with(context).load(R.drawable.female).into(holder.ivDoctorPic);
        }
        if(specialityResRow.getFullName().startsWith("Dr."))
            holder.tvDoctorname.setText(specialityResRow.getFullName());
        else
            holder.tvDoctorname.setText("Dr. "+specialityResRow.getFullName());
        if(specialityResRow.getExp()!=0)
        {
            holder.tvExperince.setText(specialityResRow.getExp() + "");
            holder.tvYrs.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.tvExperince.setText("N/A");
            holder.tvYrs.setVisibility(View.GONE);
        }

        if(specialityResRow.getFees() !=0)
        {
            //holder.tvSymbol.setVisibility(View.VISIBLE);
            //holder.tvFee.setText(specialityResRow.getFees()+"");

        }
        else
        {
            //holder.tvSymbol.setVisibility(View.GONE);
            //holder.tvFee.setText("N/A");

        }
        //holder.tvHospitalName.setText(specialityResRow.getClinicName());

        holder.tvDoctorQualification.setVisibility(View.GONE);

        //holder.tvspecialist.setText(specialityResRow.getSpecialityDesc());

        holder.rvClick.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener()
        {
            @Override
            public void onComplete(RippleView rippleView)
            {
                callgetDoctorDetails(specialityResRow.getDoctorID(), specialityResRow.getAddressID());
            }
        });

        String distance = specialityResRow.getDistance();
        if(TextUtils.isEmpty(distance))
            holder.tvDistance.setVisibility(View.INVISIBLE);
        else
            holder.tvDistance.setText(specialityResRow.getDistance());


        holder.rvBook.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView)
            {
                if(specialityResRow.getProspect().equalsIgnoreCase("Y"))
                {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+specialityResRow.getPhoneNumber()));
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", specialityResRow.getPhoneNumber(), null));
                    context.startActivity(intent);
                }
                else
                {
                    Intent slideactivity = new Intent(context, ChooseDateTime.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(context.getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                    slideactivity.putExtra("SELECTED_DATAILS",specialityResRow);
                    slideactivity.putExtra("ISFROMRESCHEDULE", false);
                    context.startActivity(slideactivity, bndlanimation);
                }
            }
        });

        holder.tvDistance.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri location = Uri.parse(String.format("http://maps.google.com/?daddr=%1s,%2s",specialityResRow.getLatitude(),specialityResRow.getLongitude()));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                PackageManager packageManager = context.getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                boolean isIntentSafe = activities.size() > 0;

                if (isIntentSafe) {
                    context.startActivity(mapIntent);
                }
                else
                {
                    ((BookAppointment) context).ShowAlertDialog("Whoops!","Doctor Latlongs not available",R.drawable.alert);
                }
            }
        });

        return convertView;
    }

    public void callgetDoctorDetails(int doctorID, final int addressID)
    {
        ((BookAppointment)context).showLoader("");
        GetDoctorDetailsRequest getDoctorDetailsRequest = new GetDoctorDetailsRequest();
        getDoctorDetailsRequest.setDoctorID(doctorID);
        getDoctorDetailsRequest.setUSRId(LoginResponseSingleton.getSingleton().getArrPatients().get(0).getPatientID());
        RestClient.getAPI(Constants.DOCTOR_CONSTANT_URL).getDoctorDetails(getDoctorDetailsRequest, new RestCallback<DoctorDetailsResponse>() {
            @Override
            public void failure(RestError restError) {
                ((BookAppointment)context).hideLoader();
                ((BookAppointment)context).ShowAlertDialog("Whoops!","Something went wrong.Please try again later.",R.drawable.worng);
            }

            @Override
            public void success(DoctorDetailsResponse doctorDetailsResponse, Response response)
            {
                ((BookAppointment)context).hideLoader();
                Intent slideactivity = new Intent(context, DoctorProfileActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(context.getApplicationContext(), R.anim.push_right_in,R.anim.push_right_out).toBundle();
                slideactivity.putExtra("ADDRESSID", addressID);
                if(doctorDetailsResponse != null)
                slideactivity.putExtra(Constants.DOCTOR_DETAILS, doctorDetailsResponse);
                context.startActivity(slideactivity, bndlanimation);
            }
        });

    }

    public void refreshAdapter(ArrayList<SpecialityResRow> specialityResRowArrayList) {
        this.specialityResRowArrayList = specialityResRowArrayList ;
        notifyDataSetChanged();
    }

    /*private String distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist)+"";
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
*/

}
