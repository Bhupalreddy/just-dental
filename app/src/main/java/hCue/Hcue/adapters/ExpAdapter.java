package hCue.Hcue.adapters;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import hCue.Hcue.DAO.CaseHistory;
import hCue.Hcue.DAO.CaseTreatmentDO;
import hCue.Hcue.DAO.PatientCasePrescribObj;
import hCue.Hcue.DAO.PatientLabTest;
import hCue.Hcue.DAO.PrintURLs;
import hCue.Hcue.DAO.Vitals;
import hCue.Hcue.MedicationSummary;
import hCue.Hcue.R;
import hCue.Hcue.VisitsSummary;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.FileDownloader;


/**
 * Created by Bharath on 10/6/2016.
 */

public class ExpAdapter extends BaseExpandableListAdapter
{
    private Context context;
    private ArrayList<CaseHistory> caseHistories;
    private ArrayList<CaseHistory> _listDataHeader;
    private  ArrayList<PrintURLs> printURLses;
    public ExpAdapter(VisitsSummary visitsSummary, ArrayList<CaseHistory> listCaseHistory, ArrayList<PrintURLs> printURLs)
    {
        this.caseHistories = listCaseHistory;
        this.context= visitsSummary;
        this._listDataHeader = listCaseHistory;
        this.printURLses = printURLs;
    }

    @Override
    public int getGroupCount()
    {
        return caseHistories.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return this.caseHistories.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return this._listDataHeader.get(i);
    }

    @Override
    public long getGroupId(int i)
    {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 1;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup)
    {

        if (view == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView tvConsultationDt = (TextView) view.findViewById(R.id.tvConsultationDt);
        TextView tvSpecializtion = (TextView) view.findViewById(R.id.tvSpecializtion);
        ImageView ivArrow           =   (ImageView) view.findViewById(R.id.ivArrow);

        if(b)
            ivArrow.setImageResource(R.drawable.down);
        else
            ivArrow.setImageResource(R.drawable.up);

        tvConsultationDt.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvSpecializtion.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyy");
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(caseHistories.get(i).getPatientCase().getConsultationDt());
        tvConsultationDt.setText(simpleDateFormat.format(calendar.getTime()));

        tvSpecializtion.setText(caseHistories.get(i).getPatientCase().getDoctorDetails().getClinicName());

        return view;
    }

    @Override
    public void onGroupExpanded(int groupPosition)
    {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public void onGroupCollapsed(int groupPosition)
    {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup)
    {

        TreatmentImageAdapter adapter = null;

        if (view == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.explistcell, null);
        }
        else
        {
            String prescriptionurl = null;
            String billingurl=null;
            String summaryurl=null;

            LinearLayout llVisitReson = (LinearLayout) view.findViewById(R.id.llVisitReson);
            TextView tvVisitReasonHeading = (TextView) view.findViewById(R.id.tvVisitReasonHeading);
            TextView tvVisitReason = (TextView) view.findViewById(R.id.tvVisitReason);
            TextView tvTemperature = (TextView) view.findViewById(R.id.tvTemperature);
            TextView tvBloodPressure = (TextView) view.findViewById(R.id.tvBloodPressure);
            TextView tvFasting = (TextView) view.findViewById(R.id.tvFasting);
            TextView tvWeight = (TextView) view.findViewById(R.id.tvWeight);
            TextView tvHeight = (TextView) view.findViewById(R.id.tvHeight);
            TextView tvHeadCircumference = (TextView) view.findViewById(R.id.tvHeadCircumference);
            TextView tvHeadCircumferenceUnit = (TextView) view.findViewById(R.id.tvHeadCircumferenceUnit);
            TextView tvBloodGroup = (TextView) view.findViewById(R.id.tvBloodGroup);
            TextView tvPulse = (TextView) view.findViewById(R.id.tvPulse);
            TextView tvLipidProfile = (TextView) view.findViewById(R.id.tvLipidProfile);
            TextView tvSugarRandom = (TextView) view.findViewById(R.id.tvSugarRandom);
            TextView tvSerumCreatinine = (TextView) view.findViewById(R.id.tvSerumCreatinine);
            TextView tvTemperatureUnit = (TextView) view.findViewById(R.id.tvTemperatureUnit);
            TextView tvBloodPressureUnit = (TextView) view.findViewById(R.id.tvBloodPressureUnit);
            TextView tvFastingUnit = (TextView) view.findViewById(R.id.tvFastingUnit);
            TextView tvWeightUnit = (TextView) view.findViewById(R.id.tvWeightUnit);
            TextView tvHeightUnit = (TextView) view.findViewById(R.id.tvHeightUnit);
            TextView tvPulseUnit = (TextView) view.findViewById(R.id.tvPulseUnit);
            TextView tvBMI = (TextView) view.findViewById(R.id.tvBMI);
            TextView tvBMIUnit = (TextView) view.findViewById(R.id.tvBMIUnit);
            TextView tvLipidProfileUnit = (TextView) view.findViewById(R.id.tvLipidProfileUnit);
            TextView tvSugarRandomUnit = (TextView) view.findViewById(R.id.tvSugarRandomUnit);
            TextView tvSerumCreatinineUnit = (TextView) view.findViewById(R.id.tvSerumCreatinineUnit);
            LinearLayout llDiagnosisNotes = (LinearLayout) view.findViewById(R.id.llDiagnosisNotes);
            TextView tvDiagnosis = (TextView) view.findViewById(R.id.tvDiagnosis);
            TextView tvDiagnosisNotes = (TextView) view.findViewById(R.id.tvDiagnosisNotes);
            LinearLayout llMedicinePrescribed = (LinearLayout) view.findViewById(R.id.llMedicinePrescribed);
            TextView tvPDF = (TextView) view.findViewById(R.id.tvPDF);
            LinearLayout llInvestigationNotes = (LinearLayout) view.findViewById(R.id.llInvestigationNotes);
            TextView tvInvestigation = (TextView) view.findViewById(R.id.tvInvestigation);

            TextView tvMedicinePrescribed = (TextView) view.findViewById(R.id.tvMedicinePrescribed);
            LinearLayout llMedicineAdd = (LinearLayout) view.findViewById(R.id.llMedicineAdd);
            LinearLayout llLabTestPrescribed = (LinearLayout) view.findViewById(R.id.llLabTestPrescribed);
            TextView tvLabTestHeading = (TextView) view.findViewById(R.id.tvLabTestHeading);
            LinearLayout llLabTestAdd = (LinearLayout) view.findViewById(R.id.llLabTestAdd);
            LinearLayout llFollowupdetails = (LinearLayout) view.findViewById(R.id.llFollowupdetails);

            LinearLayout llVisitReasonNotesContent = (LinearLayout) view.findViewById(R.id.llVisitReasonNotesContent);
            LinearLayout llDiganosisNotesContent = (LinearLayout) view.findViewById(R.id.llDiganosisNotesContent);
            LinearLayout llInvestigationNotesContent = (LinearLayout) view.findViewById(R.id.llInvestigationNotesContent);

            TextView tvFollowUpDetails = (TextView) view.findViewById(R.id.tvFollowUpDetails);
            TextView tvfollowupdays = (TextView) view.findViewById(R.id.tvfollowupdays);
            TextView tvfollowupnotes = (TextView) view.findViewById(R.id.tvfollowupnotes);
            TextView tvNotesHeader       =   (TextView)  view.findViewById(R.id.tvNotesHeader);
            TextView tvTreatmentTitle       =   (TextView)  view.findViewById(R.id.tvTreatmentTitle);
            TextView tvTreatment       =   (TextView)  view.findViewById(R.id.tvTreatment);
            RecyclerView rvTreatementImages       =   (RecyclerView)  view.findViewById(R.id.rvTreatementImages);
            LinearLayout llTreatment       =   (LinearLayout)  view.findViewById(R.id.llTreatment);
            LinearLayout llTreatmentContent       =   (LinearLayout)  view.findViewById(R.id.llTreatmentContent);

            tvfollowupdays.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvfollowupnotes.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvInvestigation.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvDiagnosisNotes.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvVisitReason.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvTemperature.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvBloodPressure.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvFasting.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvWeight.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvHeight.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvBloodGroup.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvPulse.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvLipidProfile.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvSerumCreatinine.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvSugarRandom.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvFollowUpDetails.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvLabTestHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvDiagnosis.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvVisitReasonHeading.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvBMI.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvBMIUnit.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            tvTreatment.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);


            if(caseHistories.get(i).getPatientCase().getOtherVistReason() != null && !caseHistories.get(i).getPatientCase().getOtherVistReason().trim().isEmpty())
            {
                llVisitReson.setVisibility(View.VISIBLE);
                tvVisitReason.setText(caseHistories.get(i).getPatientCase().getOtherVistReason());
            }else
            {
                llVisitReson.setVisibility(View.GONE);
            }
            Vitals vitals = caseHistories.get(i).getPatientCase().getVitals();
            if(vitals != null)
            {
                if(vitals.getTMP() != null && !vitals.getTMP().isEmpty())
                    tvTemperature.setText("" + vitals.getTMP() + (char) 0x00B0);
                else
                    tvTemperatureUnit.setText("");
                if(vitals.getBPL() != null && !vitals.getBPL().isEmpty())
                    tvBloodPressure.setText("" + vitals.getBPL()+"/"+vitals.getBPH());
                else
                    tvBloodPressureUnit.setText("");
                if(vitals.getSFT() != null && !vitals.getSFT().isEmpty())
                    tvFasting.setText(vitals.getSFT()+"/" + vitals.getSPP());
                else
                    tvFastingUnit.setText("");
                if(vitals.getWGT() != null && !vitals.getWGT().isEmpty())
                    tvWeight.setText("" + vitals.getWGT() );
                else
                    tvWeightUnit.setText("");
                if(vitals.getHGT() != null && !vitals.getHGT().isEmpty())
                    tvHeight.setText("" + vitals.getHGT());
                else
                    tvHeightUnit.setText("");
                if(vitals.getBLG() != null && !vitals.getBLG().isEmpty())
                    tvBloodGroup.setText("" + vitals.getBLG());
                if(vitals.getPLS() != null && !vitals.getPLS().isEmpty())
                    tvPulse.setText("" + vitals.getPLS());
                else
                    tvPulseUnit.setText("");
                if(vitals.getLPP() != null && !vitals.getLPP().isEmpty())
                    tvLipidProfile.setText("" + vitals.getLPP());
                else
                    tvLipidProfileUnit.setText("");
                if(vitals.getSCT() != null && !vitals.getSCT().isEmpty())
                    tvSerumCreatinine.setText("" + vitals.getSCT());
                else
                    tvSerumCreatinineUnit.setText("");
                if(vitals.getSRM() != null && !vitals.getSRM().isEmpty())
                    tvSugarRandom.setText("" + vitals.getSRM());
                else
                    tvSugarRandomUnit.setText("");
                if(vitals.getHCF() != null && !vitals.getHCF().isEmpty()){
                    tvHeadCircumferenceUnit.setText("cms");
                    tvHeadCircumference.setText("" + vitals.getHCF());
                }

                else{
                    tvHeadCircumference.setText("---");
                    tvHeadCircumferenceUnit.setText("");
                }


                calculateBMI(vitals, tvBMI, tvBMIUnit);
            }
            if(caseHistories.get(i).getPatientCase() != null && caseHistories.get(i).getPatientCase().getDiagonsticNotes()!= null
                    && !caseHistories.get(i).getPatientCase().getDiagonsticNotes().trim().isEmpty())
            {
                llDiagnosisNotes.setVisibility(View.VISIBLE);
                tvDiagnosisNotes.setText(caseHistories.get(i).getPatientCase().getDiagonsticNotes());
            }else
            {
                llDiagnosisNotes.setVisibility(View.GONE);
            }
            if(caseHistories.get(i).getListPatientCasePrescribObj() != null && !caseHistories.get(i).getListPatientCasePrescribObj().isEmpty())
            {
                View view1 = null;

                llMedicinePrescribed.setVisibility(View.VISIBLE);
                tvPDF.setVisibility(View.GONE);
                ArrayList<PatientCasePrescribObj> listPatientCasePrescribObj = new ArrayList<>();
                listPatientCasePrescribObj.clear();
                llMedicineAdd.removeAllViewsInLayout();
                listPatientCasePrescribObj = caseHistories.get(i).getListPatientCasePrescribObj();

                for(PatientCasePrescribObj patientCasePrescribObj : listPatientCasePrescribObj)
                {
                    view1 = null;
                    if (view1 == null)
                    {
                        LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view1 = infalInflater.inflate(R.layout.medicinecell, null);
                    }
                    TextView tvMedicineName = (TextView) view1.findViewById(R.id.tvMedicineName);
                    TextView tvDuration     = (TextView) view1.findViewById(R.id.tvDuration);
                    TextView tvDosage       = (TextView) view1.findViewById(R.id.tvDosage);
                    TextView tvWhen         = (TextView) view1.findViewById(R.id.tvWhen);
                    TextView tvNotes        = (TextView) view1.findViewById(R.id.tvNotes);
                    tvMedicineName.setText(patientCasePrescribObj.getMedicine());
                    tvMedicineName.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                    tvDuration.setText(patientCasePrescribObj.getNumberofDays()+" Days");
                    tvDosage.setText(patientCasePrescribObj.getDosage1()+"-"+patientCasePrescribObj.getDosage2()+"-"+patientCasePrescribObj.getDosage4());
                    if(MedicationSummary.checkIsMedicineType(patientCasePrescribObj.getMedicineType()))
                    {
                        if (!patientCasePrescribObj.isBeforeAfter())
                            tvWhen.setText("After Meal");
                        else
                            tvWhen.setText("Before Meal");
                    }else
                    {
                        tvWhen.setText("--");
                    }
                    if(llMedicineAdd!=null)
                        llMedicineAdd.addView(view1);
                    if(patientCasePrescribObj.getDiagnostic() != null)
                        tvNotes.setText("Dosage Notes : "+patientCasePrescribObj.getDiagnostic());

                }

            }else
            {
                llMedicinePrescribed.setVisibility(View.GONE);
                tvPDF.setVisibility(View.GONE);
            }


            if(caseHistories.get(i).getListpatientLabTest() != null && !caseHistories.get(i).getListpatientLabTest().isEmpty() && caseHistories.get(i).getListpatientLabTest().size() !=0)
            {
                View view2 = null;
                llLabTestPrescribed.setVisibility(View.VISIBLE);
                llLabTestAdd.removeAllViewsInLayout();
                ArrayList<PatientLabTest> listPatientLabTest = new ArrayList<>();
                listPatientLabTest.clear();
                listPatientLabTest  = caseHistories.get(i).getListpatientLabTest();
                for (PatientLabTest patientLabTest : listPatientLabTest)
                {
                    view2=null;
                    if (view2 == null)
                    {
                        LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view2 = infalInflater.inflate(R.layout.labcell, null);
                    }
                    TextView tvDisease = (TextView) view2.findViewById(R.id.tvDisease);
                    TextView tvDescription     = (TextView) view2.findViewById(R.id.tvDescription);
                    tvDisease.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
                    tvDescription.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
                    tvDisease.setText(patientLabTest.getLabTestName());
                    tvDescription.setText(patientLabTest.getLabNotes());
                    if(llLabTestAdd!=null)
                        llLabTestAdd.addView(view2);
                }

            }else
            {
                llLabTestPrescribed.setVisibility(View.GONE);
            }

            if(caseHistories.get(i).getPatientCase().getFollowUpDetails() != null)
            {

                tvfollowupdays.setText(caseHistories.get(i).getPatientCase().getFollowUpDetails().getFollowUpDay() != 0 ?caseHistories.get(i).getPatientCase().getFollowUpDetails().getFollowUpDay()+" days" : "N/A");
                if(caseHistories.get(i).getPatientCase().getFollowUpDetails().getFollowUpNotes() != null && !caseHistories.get(i).getPatientCase().getFollowUpDetails().getFollowUpNotes().equalsIgnoreCase("") ){
                    tvNotesHeader.setVisibility(View.VISIBLE);
                    tvfollowupnotes.setVisibility(View.VISIBLE);
                    tvfollowupnotes.setText(caseHistories.get(i).getPatientCase().getFollowUpDetails().getFollowUpNotes());
                }

                else{
                    tvNotesHeader.setVisibility(View.GONE);
                    tvfollowupnotes.setVisibility(View.GONE);
                }

                llFollowupdetails.setVisibility(View.VISIBLE);
            }else
            {
                llFollowupdetails.setVisibility(View.GONE);
            }

            if(printURLses!=null && printURLses.size()!=0)
            {
                for(int l=0;l<printURLses.size();l++)
                {
                    if(printURLses.get(l).getPrintType()!=null && printURLses.get(l).getPrintType().equalsIgnoreCase("PRESCRIPTN") &&printURLses.get(l).getStatus().equalsIgnoreCase("Completed"))
                    {
                        if(printURLses.get(l).getUrl()!=null)
                            prescriptionurl = printURLses.get(l).getUrl();
                    }
                    else if(printURLses.get(l).getPrintType()!=null && printURLses.get(l).getPrintType().equalsIgnoreCase("BILLING") &&printURLses.get(l).getStatus().equalsIgnoreCase("Completed"))
                    {
                        if(printURLses.get(l).getUrl()!=null)
                            billingurl = printURLses.get(l).getUrl();
                    }
                    else if(printURLses.get(l).getPrintType()!=null && printURLses.get(l).getPrintType().equalsIgnoreCase("SUMMARY") &&printURLses.get(l).getStatus().equalsIgnoreCase("Completed"))
                    {
                        if(printURLses.get(l).getUrl()!=null)
                            summaryurl = printURLses.get(l).getUrl();
                    }

                }
            }

            /*if(prescriptionurl==null && billingurl==null && summaryurl==null)
            {
                tvPDF.setVisibility(View.GONE);
            }
            else{
                tvPDF.setVisibility(View.VISIBLE);
            }*/


            final String finalPrescriptionurl = prescriptionurl;
            final String finalBillingurl = billingurl;
            final String finalSummaryurl = summaryurl;
            tvPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                View dialogView = LayoutInflater.from(context).inflate(R.layout.print_popup, null);
                dialogBuilder.setView(dialogView);
                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();

                LinearLayout llPrescription = (LinearLayout) dialogView.findViewById(R.id.llPrescription);
                LinearLayout llBilling = (LinearLayout) dialogView.findViewById(R.id.llBilling);
                LinearLayout llSummary = (LinearLayout) dialogView.findViewById(R.id.llSummary);
                TextView tvOk = (TextView) dialogView.findViewById(R.id.tvOk);

                if(finalPrescriptionurl ==null)
                    llPrescription.setVisibility(View.GONE);
                else
                    llPrescription.setVisibility(View.VISIBLE);
                if(finalBillingurl ==null)
                    llBilling.setVisibility(View.GONE);
                else
                    llBilling.setVisibility(View.VISIBLE);

                if(finalSummaryurl ==null)
                    llSummary.setVisibility(View.GONE);
                else
                    llSummary.setVisibility(View.VISIBLE);


                llPrescription.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        new DownloadFile().execute(finalPrescriptionurl, System.currentTimeMillis()+"Prescription.pdf");
                    }
                });

                llBilling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        new DownloadFile().execute(finalBillingurl, System.currentTimeMillis()+"Billing.pdf");

                    }
                });
                llSummary.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        new DownloadFile().execute(finalSummaryurl, System.currentTimeMillis()+"Summary.pdf");
                    }
                });
                tvOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alertDialog.hide();
                    }
                });
/*
                if (summaryURL != null && summaryURL.getPrescriptionURL() != null)
                    new DownloadFile().execute(summaryURL.getPrescriptionURL()*//*"http://maven.apache.org/maven-1.x/maven.pdf"*//*, "Prescription.pdf");
                else
                    Toast.makeText(context, "No PDF found", Toast.LENGTH_SHORT).show();*/
            }
        });

            if (caseHistories.get(i).getPatientCase().getNotes().getListVisitRsn() != null &&
                    caseHistories.get(i).getPatientCase().getNotes().getListVisitRsn().size() > 0){
                llVisitReson.setVisibility(View.VISIBLE);
                llVisitReasonNotesContent.removeAllViews();
                for (int j = 0; j < caseHistories.get(i).getPatientCase().getNotes().getListVisitRsn().size(); j++){
                    LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    LinearLayout llChild = (LinearLayout) infalInflater.inflate(R.layout.notes_cell, null);
                    addView(llVisitReasonNotesContent, llChild, caseHistories.get(i).getPatientCase().getNotes().getListVisitRsn().get(j));
                }

            }else{
                llVisitReson.setVisibility(View.GONE);
            }

            if (caseHistories.get(i).getPatientCase().getNotes().getListInvestigation() != null
                    && caseHistories.get(i).getPatientCase().getNotes().getListInvestigation().size() > 0){
                llInvestigationNotes.setVisibility(View.VISIBLE);
                llInvestigationNotesContent.removeAllViews();
                for (int j = 0; j < caseHistories.get(i).getPatientCase().getNotes().getListInvestigation().size(); j++){
                    LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    LinearLayout llChild = (LinearLayout) infalInflater.inflate(R.layout.notes_cell, null);
                    addView(llInvestigationNotesContent, llChild, caseHistories.get(i).getPatientCase().getNotes().getListInvestigation().get(j));
                }

            }else{
                llInvestigationNotes.setVisibility(View.GONE);
            }

            if (caseHistories.get(i).getPatientCase().getNotes().getListDiagnostic() != null && caseHistories.get(i).getPatientCase().getNotes().getListDiagnostic().size() > 0){
                llDiagnosisNotes.setVisibility(View.VISIBLE);
                llDiganosisNotesContent.removeAllViews();
                for (int j = 0; j < caseHistories.get(i).getPatientCase().getNotes().getListDiagnostic().size(); j++){
                    LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    LinearLayout llChild = (LinearLayout) infalInflater.inflate(R.layout.notes_cell, null);
                    addView(llDiganosisNotesContent, llChild, caseHistories.get(i).getPatientCase().getNotes().getListDiagnostic().get(j));
                }

            }else{
                llDiagnosisNotes.setVisibility(View.GONE);
            }

            if (caseHistories.get(i).getListCaseTreatment() != null && caseHistories.get(i).getListCaseTreatment().size() > 0){
                llTreatment.setVisibility(View.VISIBLE);
                llTreatmentContent.removeAllViews();
                for (int j = 0; j < caseHistories.get(i).getListCaseTreatment().size(); j++){
                    addTreatmentView(llTreatmentContent, adapter, caseHistories.get(i).getListCaseTreatment().get(j));
                }

            }else{
                llTreatment.setVisibility(View.GONE);
            }

        }

        return view;
    }

    private void calculateBMI(Vitals vitals, TextView tvBMI, TextView tvBMIUnit)
    {
                if(vitals.getHGT() != null && vitals.getWGT()!= null && !vitals.getHGT().isEmpty() && !vitals.getWGT().isEmpty())
                {
                    try {
                        double height = Float.parseFloat(vitals.getHGT())/100;
                        float weight = Float.parseFloat(vitals.getWGT());
                        double bmiVal = (weight/(height * height));;
                        String bmi = (weight/(height * height))+"";
                        if(bmi.length()>5)
                        {
                            tvBMI.setText(bmi.substring(0,5));
                        }else
                        {
                            tvBMI.setText(bmi);
                        }

                        if (bmiVal > 0 && bmiVal <=18.5){
                            tvBMIUnit.setText("Under Weight");
                        }else if (bmiVal >= 18.5 && bmiVal < 25){
                            tvBMIUnit.setText("Normal");
                        }if (bmiVal >= 25 && bmiVal < 30){
                            tvBMIUnit.setText("Over Weight");
                        }if (bmiVal >= 30 && bmiVal < 39){
                            tvBMIUnit.setText("Obese");
                        }if (bmiVal >= 40){
                            tvBMIUnit.setText("Morbidly Obese");
                        }else
                            tvBMIUnit.setVisibility(View.INVISIBLE);

                    }catch (Exception  e)
                    {
                        Log.e("Arithmethic exception",""+e.getMessage());
                    }
                }

    }

    public void addView(LinearLayout llContainer, LinearLayout llChild, String notes){
        TextView tvNotes = (TextView)  llChild.findViewById(R.id.tvVisitReason);
        tvNotes.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        tvNotes.setText(notes);
        llContainer.addView(llChild);
    }

    public void addTreatmentView(LinearLayout llContainer, TreatmentImageAdapter adapter, CaseTreatmentDO caseTreatmentDO){

        LayoutInflater infalInflater        = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout treatmentContainer     = (LinearLayout) infalInflater.inflate(R.layout.treatement_cell, null);
        TextView tvTreatmentTitle           = (TextView)  treatmentContainer.findViewById(R.id.tvTreatmentTitle);
        RecyclerView rvTreatementImages     = (RecyclerView)  treatmentContainer.findViewById(R.id.rvTreatementImages);

        String text = "";
        if(caseTreatmentDO != null && caseTreatmentDO.getTreatmentNotes() != null &&
                !caseTreatmentDO.getTreatmentNotes().equalsIgnoreCase("")){
            text = "<font color='#2A2A2A'>"+caseTreatmentDO.getTreatmentDesctription()+ " - " + "</font>"+ caseTreatmentDO.getTreatmentNotes();
        }else{
            text = "<font color='#2A2A2A'>"+caseTreatmentDO.getTreatmentDesctription();
        }

        tvTreatmentTitle.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        adapter    =   new TreatmentImageAdapter(context, caseTreatmentDO.getListTreatmentDocumentsURL());
        GridLayoutManager layoutManager= new GridLayoutManager(context, 2);
        rvTreatementImages.setHasFixedSize(true);
        rvTreatementImages.setLayoutManager(layoutManager);
        rvTreatementImages.setAdapter(adapter);

        tvTreatmentTitle.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
        llContainer.addView(treatmentContainer);

    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        private ProgressDialog dialog;
        String filename_;
        File pdfFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Downloading, please wait...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];
            filename_ = fileName;
            // -> maven.pdf

            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "hCueApp");
            if (!folder.exists()) {
                folder.mkdirs();
            }
            pdfFile = new File(folder, fileName);

            try {
                //pdfFile.createNewFile();
                if(pdfFile.createNewFile()){
                    Log.e("", "File created");
                }else {
                    Log.e("", "File doesn't created");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            filename_ = pdfFile.getPath();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            displayPDF(pdfFile);
        }

        public void displayPDF(File fileName) {
            Uri path;

            if (Build.VERSION.SDK_INT >= 24) {

                path = Uri.fromFile(fileName);
            } else {
                path = Uri.fromFile(fileName);
            }
            // -> filename = maven.pdf

            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(path,"application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open file with");
            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
            }
        }
    }
}
