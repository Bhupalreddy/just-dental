package hCue.Hcue.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hCue.Hcue.DAO.SpecialityDO;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;

public class CoverFlowAdapter extends BaseAdapter {
	
	private ArrayList<SpecialityDO> mData = new ArrayList<>(0);
	private Context mContext;

	public CoverFlowAdapter(Context context) {
		mContext = context;
	}
	
	public void setData(ArrayList<SpecialityDO> data) {
		mData = data;
	}
	
	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int pos) {
		return mData.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
		ViewHolder viewHolder;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_coverflow, null);

            viewHolder = new ViewHolder();
            //viewHolder.tvLabel = (TextView) rowView.findViewById(R.id.tvLabel);
			viewHolder.tvDesc = (TextView) rowView.findViewById(R.id.tvDesc);
			viewHolder.tvFindDoctors = (TextView) rowView.findViewById(R.id.tvFindDoctors);
            viewHolder.ivCategory = (ImageView) rowView.findViewById(R.id.ivCategory);

			//viewHolder.tvLabel.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
			viewHolder.tvDesc.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
			viewHolder.tvFindDoctors.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            rowView.setTag(viewHolder);
        }
		else{
			viewHolder = (ViewHolder) rowView.getTag();
		}

		viewHolder.tvFindDoctors.setBackgroundResource(mData.get(position).getBtnImage());
		viewHolder.ivCategory.setImageResource(mData.get(position).getImageResId());
		//viewHolder.tvLabel.setText(mData.get(position).getTitleResId());
//		String colorCode = ""+(mContext.getResources().getColor(mData.get(position).getTitleColor()));
		/*if (position%5 == 0){
			colorCode = "#F38164";
		}else if (position%5 == 1){
			colorCode = "#8E85BD";
		}else if (position%5 == 2){
			colorCode = "#EA4E52";
		}else if (position%5 == 3){
			colorCode = "#31BA99";
		}else if (position%5 == 4){
			colorCode = "#A3CA5A";
		}*/
		//viewHolder.tvLabel.setTextColor(Color.parseColor(mData.get(position).getTitleColor()));
		viewHolder.tvDesc.setText(mData.get(position).getSpecialityDesc());

		return rowView;
	}


    static class ViewHolder {
        //public TextView tvLabel, tvDesc;
		public TextView tvDesc;
        public ImageView ivCategory;
		public TextView tvFindDoctors;
    }

}