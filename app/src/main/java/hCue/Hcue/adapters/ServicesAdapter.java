package hCue.Hcue.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by shyamprasadg on 13/10/16.
 */

public class ServicesAdapter extends BaseAdapter
{
    private Context context;
    private boolean aBoolean;
    private ArrayList<String> services;

    public ServicesAdapter(Context context, boolean b, ArrayList<String> Services)
    {
        this.context    =   context;
        this.aBoolean   = b;
        this.services = Services;
    }

    @Override
    public int getCount()
    {
        if(aBoolean)
            return services.size();
        else {
            if(services.size()>5)
                return  5;
            else
                return services.size();
        }
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    private static class ViewHolder
    {
        public TextView  tvServices;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.services_cell, null);
            holder = new ViewHolder();

            holder.tvServices   =   (TextView)      convertView.findViewById(R.id.tvServices);
            holder.tvServices.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();

        }

        holder.tvServices.setText(services.get(position));
        return convertView;
    }
}
