package hCue.Hcue.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import hCue.Hcue.DAO.TreatmentDocumentsURLDO;
import hCue.Hcue.FullScreenSlideActivity;
import hCue.Hcue.R;

/**
 * Created by shyamprasadg on 16/11/16.
 */

public class TreatmentImageAdapter extends RecyclerView.Adapter<TreatmentImageAdapter.PrescriptionImageViewHolder>
{
    private Context context;
    ArrayList<TreatmentDocumentsURLDO> treatmentDocumentsURLDOs = new ArrayList<>();
    ArrayList<String> listTreatmentImageUrls = new ArrayList<>();

    public TreatmentImageAdapter(Context context, ArrayList<TreatmentDocumentsURLDO> treatmentDocumentsURLDOs)
    {
        this.context    =   context;
        this.treatmentDocumentsURLDOs = treatmentDocumentsURLDOs;
        listTreatmentImageUrls = convertTreatementImageDO2StringImageUrl(treatmentDocumentsURLDOs);
    }

    @Override
    public PrescriptionImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.treatement_image_files, parent, false);
        return new PrescriptionImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PrescriptionImageViewHolder holder, int position) {

        TreatmentDocumentsURLDO resUrldo = treatmentDocumentsURLDOs.get(position);
        holder.tvImageName.setTag(R.string.img_pos, position);
        holder.tvImageName.setTag(resUrldo.getFileURL());
        String imageName = resUrldo.getFileName().replace(" ", "_");
        holder.tvImageName.setText(imageName+ resUrldo.getFileExtn());

    }

    @Override
    public int getItemCount()
    {
        if (treatmentDocumentsURLDOs != null && treatmentDocumentsURLDOs.size() > 0)
            return treatmentDocumentsURLDOs.size();
        else
            return 0;
    }

    public class PrescriptionImageViewHolder extends RecyclerView.ViewHolder
    {
        private TextView   tvImageName;

        public PrescriptionImageViewHolder(View itemView)
        {
            super(itemView);
            tvImageName       =   (TextView)     itemView.findViewById(R.id.tvImageName);

            tvImageName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent SlideActivity = new Intent(context, FullScreenSlideActivity.class);
                    SlideActivity.putExtra("position", getAdapterPosition());
                    SlideActivity.putExtra("flag", "PresImgSumAdapter");
                    SlideActivity.putStringArrayListExtra("PRESCRIPTIONS_LIST",listTreatmentImageUrls);
                    context.startActivity(SlideActivity);
                }
            });
        }

    }
    /*public void refreshAdapters(ArrayList<String> list) {
        listPrescriptions = list;
        notifyDataSetChanged();

    }*/

    public void setLayoutParams(View v, int drawableId)
    {
        int [] dDimens = getDrawableDimensions(v.getContext(), drawableId);
        v.getLayoutParams().width = dDimens[0];
        v.getLayoutParams().height = dDimens[1];
    }

    public int[] getDrawableDimensions(Context context, int id)
    {
        int[] dimensions = new int[2];
        if(context == null)
            return dimensions;
        Drawable d = context.getResources().getDrawable(id);
        if(d != null)
        {
            dimensions[0] = d.getIntrinsicWidth();
            dimensions[1] = d.getIntrinsicHeight();
        }
        return dimensions;
    }
    public ArrayList<String> convertTreatementImageDO2StringImageUrl(ArrayList<TreatmentDocumentsURLDO> treatmentDocumentsURLDOs) {

        ArrayList<String> listImageUrls = new ArrayList<>();
        if (treatmentDocumentsURLDOs != null && treatmentDocumentsURLDOs.size() > 0)
            for (int i = 0; i < treatmentDocumentsURLDOs.size(); i++){
                if (treatmentDocumentsURLDOs.get(i).getFileURL() != null && !treatmentDocumentsURLDOs.get(i).getFileURL().equalsIgnoreCase("")){
                    listImageUrls.add(treatmentDocumentsURLDOs.get(i).getFileURL());
                }
            }
        return listImageUrls;
    }
}




