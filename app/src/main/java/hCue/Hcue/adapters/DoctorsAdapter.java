package hCue.Hcue.adapters;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hCue.Hcue.ChooseDateTime;
import hCue.Hcue.DAO.Doctor_Details;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.R;
import hCue.Hcue.model.SearchHospitalResponce;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.CircleTransform;
import hCue.Hcue.utils.Constants;

/**
 * Created by shyamprasadg on 13/10/16.
 */

public class DoctorsAdapter extends BaseAdapter
{
    private Context context;
    private boolean aBoolean;
    private ArrayList<Doctor_Details> doctor_detailses;
    SearchHospitalResponce searchHospitalResponce1;

    public DoctorsAdapter(Context context, boolean b, ArrayList<Doctor_Details> doctorInfo, SearchHospitalResponce searchHospitalResponce)
    {
        this.context    =   context;
        this.aBoolean = b;
        this.doctor_detailses = doctorInfo;
        this.searchHospitalResponce1 = searchHospitalResponce;
    }

    @Override
    public int getCount()
    {
        if(aBoolean)
        return doctor_detailses.size();
        else {
            if(doctor_detailses.size()>3)
                return  3;
            else
                return doctor_detailses.size();
        }
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public void refreshAdapterFilteredApplied(ArrayList<Doctor_Details> filteredPatients)
    {
        doctor_detailses = filteredPatients;
        notifyDataSetChanged();

    }

    private static class ViewHolder
    {
        public ImageView ivDoctorPic,ivBook;
        public TextView  tvDoctorname,tvspecialist,tvFees;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.clinic_doctors_cell, null);
            holder = new ViewHolder();

            holder.ivDoctorPic      =   (ImageView)     convertView.findViewById(R.id.ivDoctorPic);
            holder.ivBook           =   (ImageView)     convertView.findViewById(R.id.ivBook);
            holder.tvDoctorname     =   (TextView)      convertView.findViewById(R.id.tvDoctorname);
            holder.tvspecialist     =   (TextView)      convertView.findViewById(R.id.tvspecialist);
            holder.tvFees           =   (TextView)      convertView.findViewById(R.id.tvFees);

            holder.tvDoctorname.setTypeface(ApplicationConstants.WALSHEIM_SEMI_BOLD);
            holder.tvspecialist.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
            holder.tvFees.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        if (doctor_detailses.get(position).getGender().equalsIgnoreCase("M"))
        {
            holder.ivDoctorPic.setImageDrawable(context.getResources().getDrawable(R.drawable.male));
            if (doctor_detailses.get(position).getImageURL() != null)
            {
                Picasso.with(context).load(doctor_detailses.get(position).getImageURL())
                        .placeholder(R.drawable.male)
                        .resize(60, 60).transform(new CircleTransform()).into(holder.ivDoctorPic);
            }
        }
        else
        {
            holder.ivDoctorPic.setImageDrawable(context.getResources().getDrawable(R.drawable.female));
            if (doctor_detailses.get(position).getImageURL() != null)
            {
                Picasso.with(context).load(doctor_detailses.get(position).getImageURL())
                        .placeholder(R.drawable.male)
                        .resize(60, 60).transform(new CircleTransform()).into(holder.ivDoctorPic);
            }

        }

        if (doctor_detailses.get(position).getProspect().equalsIgnoreCase("Y"))
        {
            holder.ivBook.setBackgroundResource(R.drawable.clinic_call);
        }
        else
            holder.ivBook.setBackgroundResource(R.drawable.clinic_book);

        holder.tvDoctorname.setText("Dr. "+doctor_detailses.get(position).getFullName());

        if (doctor_detailses.get(position).getSpecialityCD() != null && !doctor_detailses.get(position).getSpecialityCD().isEmpty())
        {
            StringBuilder specialitiesstr = new StringBuilder();
            for (String speciality : doctor_detailses.get(position).getSpecialityCD().values())
                specialitiesstr.append(Constants.specialitiesMap.get(speciality) + "\n");
            holder.tvspecialist.setText(specialitiesstr.toString());
        }
            holder.ivBook.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (doctor_detailses.get(position).getProspect().equalsIgnoreCase("Y"))
                    {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + doctor_detailses.get(position).getMobile()));
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", String.valueOf(doctor_detailses.get(position).getMobile()), null));
                        context.startActivity(intent);
                    } else {
                        SpecialityResRow specialityResRow = new SpecialityResRow();
                        specialityResRow.setDoctorID(doctor_detailses.get(position).getDoctorID());
                        specialityResRow.setGender(doctor_detailses.get(position).getGender().charAt(0));
                        specialityResRow.setFullName(doctor_detailses.get(position).getFullName());
                        specialityResRow.setProfileImage(doctor_detailses.get(position).getImageURL());
                        specialityResRow.setClinicName(searchHospitalResponce1.getRows().get(0).getHospitalInfos().getHospitalDetails().getHospitalName());
                        specialityResRow.setAddress(searchHospitalResponce1.getRows().get(0).getHospitalInfos().getHospitalAddress().getAddress2());
                        if(doctor_detailses.get(position).getSpecialityCD()!=null)
                        specialityResRow.setSpecialityID(doctor_detailses.get(position).getSpecialityCD().toString());
                        else
                        specialityResRow.setSpecialityID("");
                        specialityResRow.setAddressID(doctor_detailses.get(position).getAddressID());
                        specialityResRow.setEmailID(doctor_detailses.get(position).getDoctorLoginID());
                        specialityResRow.setPhoneNumber(doctor_detailses.get(position).getMobile().toString());
                        specialityResRow.setLatitude(searchHospitalResponce1.getRows().get(0).getHospitalInfos().getHospitalAddress().getLatitude());
                        specialityResRow.setLongitude(searchHospitalResponce1.getRows().get(0).getHospitalInfos().getHospitalAddress().getLongitude());
                        Intent slideactivity = new Intent(context, ChooseDateTime.class);
                        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(context.getApplicationContext(), R.anim.push_right_in, R.anim.push_right_out).toBundle();
                        slideactivity.putExtra("SELECTED_DATAILS", specialityResRow);
                        slideactivity.putExtra("ISFROMRESCHEDULE", false);
                        context.startActivity(slideactivity, bndlanimation);
                    }
                }
            });
        return convertView;
    }
}

