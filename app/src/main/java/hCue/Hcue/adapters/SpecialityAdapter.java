package hCue.Hcue.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hCue.Hcue.DAO.SpecialityDAO;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by shyamprasadg on 22/06/16.
 */
public class SpecialityAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<SpecialityDAO> listSpecialityDAOs ;

    public SpecialityAdapter(Context context, ArrayList<SpecialityDAO> listSpecialityDAOs)
    {
        this.context    =   context;
        this.listSpecialityDAOs = listSpecialityDAOs ;
    }
    @Override
    public int getCount() {
        return listSpecialityDAOs.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView     = LayoutInflater.from(context).inflate(R.layout.speciality_cell,null);

        TextView    tvspecialityName    =   (TextView)  convertView.findViewById(R.id.tvspecialityName);

        ImageView   ivspecialityImg     =   (ImageView) convertView.findViewById(R.id.ivspecialityImg);
        ImageView   ivIcon              =   (ImageView) convertView.findViewById(R.id.ivIcon);

        SpecialityDAO specialityDAO = listSpecialityDAOs.get(position);

        ivspecialityImg.setBackgroundResource(specialityDAO.getSpecialityicon());
        ivIcon.setBackgroundResource(specialityDAO.getSpecialityimg());
        tvspecialityName.setText(specialityDAO.getSpecialityname());
        tvspecialityName.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);



        return convertView;
    }
}
