package hCue.Hcue.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hCue.Hcue.BookAppointment;
import hCue.Hcue.DAO.DoctorSpecialityDO;
import hCue.Hcue.DAO.HospitalInfo;
import hCue.Hcue.DoctorSearch;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.MyautoScroolView;

/**
 * Created by Bharath on 10/8/2016.
 */

public class SpecialityListAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<DoctorSpecialityDO> doctorSpecialityDOs;
    private MyautoScroolView actDoctors;
    private int Images[];

    public SpecialityListAdapter(DoctorSearch doctorSearch, ArrayList<DoctorSpecialityDO> values1, MyautoScroolView actDoctors, int[] images)
    {
        this.context = doctorSearch;
        this.doctorSpecialityDOs = values1;
        this.actDoctors = actDoctors ;
        this.Images = images;
    }

    @Override
    public int getCount() {
        return doctorSpecialityDOs.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        DoctorSpecialityDO specialityResRow = doctorSpecialityDOs.get(position);

        // Inflate your custom row layout as usual.
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.searchfilteritem, parent, false);

        TextView tvSearchname         =   (TextView)  convertView.findViewById(R.id.tvSearchname);
        TextView tvSpeciality         =   (TextView)  convertView.findViewById(R.id.tvSpeciality);
        ImageView ivImages  = (ImageView) convertView.findViewById(R.id.ivImages);
        ivImages.setVisibility(View.VISIBLE);

        tvSearchname.setText(specialityResRow.getName());
        tvSpeciality.setText(specialityResRow.getCategory());
        ivImages.setImageResource(Images[position]);
        tvSearchname.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvSpeciality.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        convertView.setTag(specialityResRow);
        final View finalConvertView = convertView;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoctorSpecialityDO specialityResRow = (DoctorSpecialityDO) finalConvertView.getTag();

                if(specialityResRow.getCategory().equalsIgnoreCase("Hospital"))
                {
                    Intent intent = new Intent(context, HospitalInfo.class);
                    intent.putExtra("HospitalDetails",specialityResRow);
                    context.startActivity(intent);
                }else
                {
                    Intent intent = new Intent(context, BookAppointment.class);
                    if (specialityResRow.getCategory().toString().equalsIgnoreCase("Doctor"))
                        intent.putExtra("DOCTORNAME", actDoctors.getText().toString());
                    else
                    {
                        intent.putExtra("SPECIALITY", specialityResRow.getSpecialityID());
                        intent.putExtra("SPECIALITY_NAME", specialityResRow.getName());
                    }
                    context.startActivity(intent);
                }
            }
        });
        return convertView;
    }
}
