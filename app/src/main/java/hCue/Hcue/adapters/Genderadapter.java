package hCue.Hcue.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hCue.Hcue.R;
import hCue.Hcue.RegisterActivity;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by User on 8/3/2016.
 */
public class Genderadapter extends BaseAdapter
{
    private Context context;
    private ArrayList<String> Gender;
    public Genderadapter(RegisterActivity registerActivity, ArrayList<String> gender)
    {
        this.context = registerActivity;
        this.Gender = gender;
    }

    @Override
    public int getCount() {
        return Gender.size() ;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView     = LayoutInflater.from(context).inflate(R.layout.spinner_gender,null);

        TextView  tvGender   =   (TextView)  convertView.findViewById(R.id.tvGender);
        tvGender.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvGender.setText(Gender.get(position));
        return convertView;
    }
}
