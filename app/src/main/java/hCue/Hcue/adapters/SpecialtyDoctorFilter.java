package hCue.Hcue.adapters;

import android.widget.Filter;

import java.util.ArrayList;

import hCue.Hcue.DAO.DoctorSpecialityDO;

/**
 * Created by Appdest on 25-06-2016.
 */
public class SpecialtyDoctorFilter extends Filter {

    private ArrayList<DoctorSpecialityDO> originalspecialityResRowArrayList ;
    private ArrayList<DoctorSpecialityDO> filterspecialityResRowArrayList ;
    private AutoCompleteAdapter autoCompleteAdapter ;

    public SpecialtyDoctorFilter(AutoCompleteAdapter autoCompleteAdapter, ArrayList<DoctorSpecialityDO> originalArrayList)
    {
        super();
        this.autoCompleteAdapter = autoCompleteAdapter ;
        this.originalspecialityResRowArrayList = originalArrayList ;
        this.filterspecialityResRowArrayList   = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filterspecialityResRowArrayList.clear();
        final FilterResults results = new FilterResults();

        /*if (constraint == null || constraint.length() == 0) {
            filterspecialityResRowArrayList.addAll(originalspecialityResRowArrayList);
        } else */
        {
            if (constraint.toString() != null){
                final String filterPattern = constraint.toString().toLowerCase().trim();
                // Your filtering logic goes in here
                for ( DoctorSpecialityDO specialityResRow : originalspecialityResRowArrayList) {
                    if (specialityResRow.getName().toLowerCase().contains(filterPattern)) {
                        if(filterspecialityResRowArrayList.indexOf(specialityResRow) == -1)
                            filterspecialityResRowArrayList.add(specialityResRow);
                    }else
                        filterspecialityResRowArrayList.add(specialityResRow);
                }
            }


        }
        results.values = filterspecialityResRowArrayList;
        results.count = filterspecialityResRowArrayList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        /*autoCompleteAdapter.filteredArrayList.clear();
        if(results.values != null)
        autoCompleteAdapter.filteredArrayList.addAll((ArrayList) results.values);*/
        autoCompleteAdapter.notifyDataSetChanged();
    }
}
