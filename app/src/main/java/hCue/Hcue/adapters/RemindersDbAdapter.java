
package hCue.Hcue.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import hCue.Hcue.DAO.ReminderDetailsDO;

import static android.R.id.list;

/**
 * Simple reminder database access helper class. 
 * Defines the basic CRUD operations (Create, Read, Update, Delete)
 * for the example, and gives the ability to list all reminders as well as
 * retrieve or modify a specific reminder.
 *
 */
public class RemindersDbAdapter {

    //
    // Databsae Related Constants
    //
    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "reminders";
    private static final int DATABASE_VERSION = 1;

    public static final String KEY_TITLE = "title";
    public static final String KEY_PRIMARY_ROWID = "primary_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_DATE_TIME = "reminder_date_time";
    public static final String KEY_TIME = "reminder_time";
    public static final String KEY_DAY_ID = "event_id";
    public static final String KEY_ROWID = "_id";
    public static final String KEY_CASE_ID = "case_id";


    private static final String TAG = "ReminderDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    /**
     * Database creation SQL statement
     */
    private static final String DATABASE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE + " ("
                    + KEY_ROWID + " integer primary key autoincrement, "
                    + KEY_TITLE + " text not null, "
                    + KEY_PRIMARY_ROWID + " text not null, "
                    + KEY_NAME + " text not null, "
                    + KEY_DAY_ID + " text not null, "
                    + KEY_TIME + " text not null, "
                    + KEY_CASE_ID + " text not null, "
                    + KEY_DATE_TIME + " text not null);";



    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param ctx the Context within which to work
     */
    public RemindersDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Open the database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public RemindersDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }


    /**
     * Create a new reminder using the title, body and reminder date time provided. 
     * If the reminder is  successfully created return the new rowId
     * for that reminder, otherwise return a -1 to indicate failure.
     *
     * @param title the title of the reminder
     * @param reminderDateTime the date and time the reminder should remind the user
     * @return rowId or -1 if failed
     */
    public long createReminder(String title, String reminderDateTime, long primaryRowID, String patientName, int dayID, String time, long caseID) {

        try{
            ContentValues initialValues = new ContentValues();
            initialValues.put(KEY_TITLE, title);
            initialValues.put(KEY_DATE_TIME, reminderDateTime);
            initialValues.put(KEY_PRIMARY_ROWID, primaryRowID);
            initialValues.put(KEY_NAME, patientName);
            initialValues.put(KEY_TIME, time);
            initialValues.put(KEY_DAY_ID, dayID);
            initialValues.put(KEY_CASE_ID, caseID);

            return mDb.insert(DATABASE_TABLE, null, initialValues);
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Delete the reminder with the given rowId
     *
     * @param rowId id of reminder to delete
     * @return true if deleted, false otherwise
     */
    public boolean deleteReminder(long caseID, long rowId, int dayID, String date) {

        try {
            return mDb.delete(DATABASE_TABLE,
                    KEY_CASE_ID + " =? AND " + KEY_ROWID + " =? AND " + KEY_DAY_ID + " =? AND "+ KEY_DATE_TIME + " =? ",
                    new String[] {caseID+"", rowId+"", dayID+"", date}) > 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public boolean deleteReminder(long rowId) {

        try {
            return mDb.delete(DATABASE_TABLE,
                    KEY_ROWID + " =? ",
                    new String[] {rowId+""}) > 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    /**
     * Return a Cursor over the list of all reminders in the database
     *
     * @return Cursor over all reminders
     */
    public Cursor fetchAllReminders() {

        return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TITLE,
                KEY_DATE_TIME, KEY_PRIMARY_ROWID, KEY_NAME, KEY_DAY_ID}, null, null, null, null, null);
    }

    /**
     * Return a Cursor positioned at the reminder that matches the given rowId
     *
     * @param rowId id of reminder to retrieve
     * @return Cursor positioned to matching reminder, if found
     * @throws SQLException if reminder could not be found/retrieved
     */
    public boolean fetchReminder(long rowId) throws SQLException {

        try {
            Cursor mCursor =

                    mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                                    KEY_TITLE, KEY_DATE_TIME, KEY_PRIMARY_ROWID, KEY_NAME, KEY_DAY_ID}, KEY_PRIMARY_ROWID + "=" + rowId, null,
                            null, null, null, null);
            if (mCursor != null && mCursor.getCount() > 0) {
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Update the reminder using the details provided. The reminder to be updated is
     * specified using the rowId, and it is altered to use the title, body and reminder date time
     * values passed in
     *
     * @param rowId id of reminder to update
     * @param title value to set reminder title to
     * @param reminderDateTime value to set the reminder time.
     * @return true if the reminder was successfully updated, false otherwise
     */
    public boolean updateReminder(long rowId,String title, String reminderDateTime, long primaryRowID, String patientName, int eventID) {

        try {

            ContentValues args = new ContentValues();
            args.put(KEY_TITLE, title);
            args.put(KEY_DATE_TIME, reminderDateTime);
            args.put(KEY_PRIMARY_ROWID, primaryRowID);
            args.put(KEY_NAME, patientName);
            args.put(KEY_DAY_ID, eventID);
            return mDb.update(DATABASE_TABLE, args, KEY_ROWID + " = " + rowId, null) > 0;

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;

    }

    public Cursor checkIfRecordExist(long caseID, long rowID, int dayID, String date) {

        try {
            String selectQuery = "SELECT  * FROM " + DATABASE_TABLE + " WHERE "  + KEY_CASE_ID + " = " + caseID + " AND "
                    + KEY_PRIMARY_ROWID + " = " + rowID + " AND "+ KEY_DAY_ID + " = "+  dayID+ " AND "+ KEY_DATE_TIME + " = '"+  date +"'";


            Cursor c = mDb.rawQuery(selectQuery, null);

            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                String mRowID = c.getString(c.getColumnIndex(KEY_ROWID));

            }
            return  c;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }

    public ArrayList<ReminderDetailsDO> getAllTodayRecords(long caseID, long rowID, String date) {

        ArrayList<ReminderDetailsDO> listReminders = new ArrayList<>();

        try {
            String selectQuery = "SELECT  * FROM " + DATABASE_TABLE + " WHERE "  + KEY_CASE_ID + " = " + caseID + " AND "
                    + KEY_PRIMARY_ROWID + " = " + rowID + " AND " + KEY_DATE_TIME + " = '"+  date +"' ";


            Cursor c = mDb.rawQuery(selectQuery, null);

            if (c != null && c.getCount() > 0) {
//                c.moveToFirst();
                while (c.moveToNext()){

                    ReminderDetailsDO do1 = new ReminderDetailsDO();

                    do1.setMedicineName(c.getString(c.getColumnIndex(RemindersDbAdapter.KEY_TITLE)));
                    do1.setPatientName(c.getString(c.getColumnIndex(RemindersDbAdapter.KEY_NAME)));
                    do1.setDayID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_DAY_ID)));
                    do1.setRowID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_ROWID)));
                    do1.setPrimaryID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_PRIMARY_ROWID)));
                    do1.setCaseID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_CASE_ID)));
                    do1.setTime(c.getString(c.getColumnIndex(RemindersDbAdapter.KEY_TIME)));
                    listReminders.add(do1);
                }
            }
            return  listReminders;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }

    public Cursor checkIfRecordExist(long rowID) {

        try {
            String selectQuery = "SELECT  * FROM " + DATABASE_TABLE + " WHERE "
                    + KEY_ROWID + " = " + rowID;


            Cursor c = mDb.rawQuery(selectQuery, null);

            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                String mRowID = c.getString(c.getColumnIndex(KEY_ROWID));

            }
            return  c;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }

    public ArrayList<ReminderDetailsDO> fetchAllRecordsAtSpecifiedTime(String date, String time) {

        try {
            String selectQuery = "SELECT  * FROM " + DATABASE_TABLE + " WHERE " + KEY_DATE_TIME + " = '"  +date+  "' AND "+ KEY_TIME + "  = '"+  time+ "'";

            ArrayList<ReminderDetailsDO> listReminders = new ArrayList<>();
            Cursor c = mDb.rawQuery(selectQuery, null);
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                if (c.moveToFirst()) {
                    while (c.isAfterLast() == false) {
                        ReminderDetailsDO do1 = new ReminderDetailsDO();
                        do1.setMedicineName(c.getString(c.getColumnIndex(RemindersDbAdapter.KEY_TITLE)));
                        do1.setPatientName(c.getString(c.getColumnIndex(RemindersDbAdapter.KEY_NAME)));
                        do1.setDayID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_DAY_ID)));
                        do1.setRowID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_ROWID)));
                        do1.setPrimaryID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_PRIMARY_ROWID)));
                        do1.setCaseID(c.getInt(c.getColumnIndex(RemindersDbAdapter.KEY_CASE_ID)));
                        do1.setTime(c.getString(c.getColumnIndex(RemindersDbAdapter.KEY_TIME)));

                        listReminders.add(do1);
                        c.moveToNext();
                    }
                }
                return  listReminders;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }

    public boolean updateReminderTime(long rowId,String title, String reminderDateTime, long primaryRowID, int eventID, String time) {

        try {

            ContentValues args = new ContentValues();
            args.put(KEY_TITLE, title);
            args.put(KEY_DATE_TIME, reminderDateTime);
            args.put(KEY_PRIMARY_ROWID, primaryRowID);
            args.put(KEY_DAY_ID, eventID);
            args.put(KEY_TIME, time);
            return mDb.update(DATABASE_TABLE, args, KEY_ROWID + " = " + rowId, null) > 0;

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;

    }

}
