package hCue.Hcue.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RadioButton;

import java.util.ArrayList;

import hCue.Hcue.DAO.PatientRow;
import hCue.Hcue.R;
import hCue.Hcue.utils.ApplicationConstants;

/**
 * Created by shyamprasadg on 08/07/16.
 */
public class ChoosePrimaryUserAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<PatientRow> patientRows ;
    private int selectedposition = -1;
    private Button btnContinue;
    public ChoosePrimaryUserAdapter(Context context, ArrayList<PatientRow> patientRows, Button btnContinue)
    {
        this.context = context;
        this.patientRows = patientRows ;
        this.btnContinue = btnContinue;
    }
    @Override
    public int getCount()
    {
        if(patientRows == null)
            return 0;
        else
            return patientRows.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        convertView = LayoutInflater.from(context).inflate(R.layout.people_cell,null);

        PatientRow patientRow = patientRows.get(position);
        RadioButton     rbPeople    =   (RadioButton) convertView.findViewById(R.id.rbPeople);
        rbPeople.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        rbPeople.setText(patientRow.getArrPatients().get(0).getFullName());
        if(getSelectedposition() == position)
        {
            rbPeople.setChecked(true);
        }else
        {
            rbPeople.setChecked(false);
        }

        rbPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               setSelectedposition(position);
                btnContinue.setVisibility(View.VISIBLE);
                btnContinue.setText("Continue");
                notifyDataSetChanged();


            }
        });

        return convertView;
    }

    public int getSelectedposition() {
        return selectedposition;
    }

    public void setSelectedposition(int selectedposition) {
        this.selectedposition = selectedposition;
    }
}
