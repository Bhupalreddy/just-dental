package hCue.Hcue;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import hCue.Hcue.DAO.DoctorSpecialityDO;
import hCue.Hcue.DAO.GlobalSearchDoctorsDO;
import hCue.Hcue.DAO.GlobalSearchRequest;
import hCue.Hcue.DAO.GlobalSearchResponseDO;
import hCue.Hcue.DAO.SpecialityResRow;
import hCue.Hcue.adapters.AutoCompleteAdapter;
import hCue.Hcue.adapters.SpecialityListAdapter;
import hCue.Hcue.rest.RestCallback;
import hCue.Hcue.rest.RestClient;
import hCue.Hcue.rest.RestError;
import hCue.Hcue.utils.ApplicationConstants;
import hCue.Hcue.utils.Connectivity;
import hCue.Hcue.utils.Constants;
import hCue.Hcue.utils.MyautoScroolView;
import hCue.Hcue.utils.Preference;
import hCue.Hcue.utils.SelectedConsSingleton;
import retrofit.client.Response;

/**
 * Created by shyamprasadg on 23/06/16.
 */
public class DoctorSearch extends BaseActivity
{
    private LinearLayout         llSearch;
    private TextView             tvSearch, tvNoResultFound;
    private MyautoScroolView actDoctors;
    private ArrayList<SpecialityResRow> specialityResRowArrayList ;
    private AutoCompleteAdapter autoCompleteAdapter ;
    public static Hashtable<String,String> specialitycode ;
    //    private ArrayList<String> values ;
    private ArrayList<DoctorSpecialityDO> values1 ;
    private ArrayList<DoctorSpecialityDO> values2 ;

    private LinearLayout llclose ;
    private int touchcount = 0;
    private Double currentlat, currentlong ;
    private ListView lvList;
    private String[]    specialityNames  = {"Dentist","General Practitioner","Physiotherapist","Dermatologist","Homeopathy","Cardiologist","Neurologist","Gastroenterologist","Gynecologist",
            "ENT","Paediatrician","Ophthalmologist","Diabetologist","Urologist","Orthopedist","Nephrologist","Plastic Surgeon","Acupuncture",
            "Ayurveda","Anesthesist"};

    private String[]    specialityCodes  = {"DNT","GEP","PST","DER","HYM","CAO","NEU","GAS","GYN",
            "ENT","PAE","OPT","DBT","URO","ORT","NEP","PLS","ACP",
            "AYU","ANE"};

    private int [] images =  {R.drawable.dentist, R.drawable.general_prac,R.drawable.phyiotherapist,R.drawable.dermatologist,R.drawable.homeo,
            R.drawable.cardiologist, R.drawable.neuroogist,R.drawable.gastrologist,R.drawable.gynecologist,R.drawable.ent,R.drawable.paediatrician,
            R.drawable.ophtahlmologist,R.drawable.diabetologist,R.drawable.urologist,R.drawable.orthopedist,R.drawable.nephrologist,
            R.drawable.pastic_surgeon,R.drawable.accupuncture,R.drawable.ayurveda,R.drawable.anesthetist };
    private Timer timer = new Timer();
    private final long DELAY = 1000;
    private ProgressBar pbHeaderProgress;



    @Override
    public void initialize()
    {
        setProgressBarIndeterminateVisibility(true);

        SelectedConsSingleton.getSingleton().getSpecialityDAO();
        Preference preference = new Preference(this);
        String latlong = preference.getStringFromPreference("CURRENTLATLONG","");
        if(latlong.contains(","))
        {
            currentlat  = Double.parseDouble(latlong.split(",")[0]);
            currentlong = Double.parseDouble(latlong.split(",")[1]);
        }

        llSearch    =   (LinearLayout)getLayoutInflater().inflate(R.layout.doctor_search,null,false);
        llBody.addView(llSearch,baseLayoutParams);

        actDoctors      =   (MyautoScroolView)  llSearch.findViewById(R.id.actDoctors);
        tvSearch        =   (TextView)              llSearch.findViewById(R.id.tvSearch);
        lvList          = (ListView) llSearch.findViewById(R.id.lvList);
        llclose         =   (LinearLayout)          llSearch.findViewById(R.id.llclose);
        ivClose         =   (ImageView)          llSearch.findViewById(R.id.ivClose);
        pbHeaderProgress = (ProgressBar) llSearch.findViewById(R.id.pbHeaderProgress);
        tvNoResultFound        =   (TextView)              llSearch.findViewById(R.id.tvNoResultFound);
        intitalisespecialitycode();
//        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        llLeftMenu.setVisibility(View.GONE);
        llBack.setVisibility(View.VISIBLE);
        flBottomBar.setVisibility(View.GONE);
        tvTopTitle.setText("Global Search");
        actDoctors.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);

        values1 =new ArrayList<DoctorSpecialityDO>();
        values2 = new ArrayList<DoctorSpecialityDO>();

        for(int i=0;i<specialityNames.length;i++) {

            DoctorSpecialityDO doctorSpecialityDO = new DoctorSpecialityDO();
            doctorSpecialityDO.setCategory("Speciality");
            doctorSpecialityDO.setName(specialityNames[i]);
            doctorSpecialityDO.setSpecialityID(specialityCodes[i]);
            values2.add(doctorSpecialityDO);
        }


        lvList.setAdapter(new SpecialityListAdapter(DoctorSearch.this,values2,actDoctors, images));
/*        actDoctors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpecialityResRow specialityResRow = (SpecialityResRow) view.getTag();
                actDoctors.setText(specialityResRow.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

//        values.addAll(specialitycode.keySet());
        autoCompleteAdapter = new AutoCompleteAdapter(DoctorSearch.this, 0, ApplicationConstants.listSpecialityDO, actDoctors);
        actDoctors.setAdapter(autoCompleteAdapter);
        tvSearch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DoctorSearch.this, BookAppointment.class);
                if(specialitycode.get(actDoctors.getText().toString()) == null)
                    intent.putExtra("DOCTORNAME",actDoctors.getText().toString());
                else
                    intent.putExtra("SPECIALITY",specialitycode.get(actDoctors.getText().toString()));
                startActivity(intent);
            }
        });

        llclose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                actDoctors.setText("");
                ivClose.setVisibility(View.GONE);
            }
        });

        actDoctors.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(timer != null)
                    timer.cancel();
                if (pbHeaderProgress.getVisibility() != View.VISIBLE && s.toString().length()>3){
                    pbHeaderProgress.setVisibility(View.VISIBLE);
                    ivClose.setVisibility(View.GONE);
                }/*else{
                    ivClose.setVisibility(View.VISIBLE);
                }*/
            }

            @Override
            public void afterTextChanged(final Editable s) {

                if (s.toString().length() > 0 && s.toString().length() < 3){
                    ivClose.setVisibility(View.VISIBLE);
                }else{
                    ivClose.setVisibility(View.GONE);
                }

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // TODO: do what you need here (refresh list)
                        // you will probably need to use
                        // runOnUiThread(Runnable action) for some specific
                        // actions
                        if (Connectivity.isConnected(DoctorSearch.this))
                        {
//                    callgetSpecialityDoctorsService(s.toString());
                            if(s.toString().length()>3) {
                                callGlobalSerachService(s.toString());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lvList.setVisibility(View.GONE);
                                    }
                                });

                            }
                            else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lvList.setVisibility(View.VISIBLE);
                                        tvNoResultFound.setVisibility(View.GONE);
                                        if(values1 != null){
                                            values1.clear();
                                            autoCompleteAdapter.refreshAdapter(new ArrayList<DoctorSpecialityDO>());
                                        }

                                        if (pbHeaderProgress.getVisibility() == View.VISIBLE){
                                            pbHeaderProgress.setVisibility(View.GONE);
                                            ivClose.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });

                            }

                        }else
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
                                }
                            });

                        }

                    }

                }, DELAY);

            }
        });

  /*      if (Connectivity.isConnected(DoctorSearch.this))
        {
//            callgetSpecialityDoctorsService("");
            callGlobalSerachService("");
        }else
        {
            ShowAlertDialog("Whoops!","No Internet connection found. Check your connection or try again.",R.drawable.no_internet);
        }*/



    }

    /*private void callgetSpecialityDoctorsService(String doctorname)
    {
       // showLoader("");
        SearchBySpecialityReq searchBySpecialityReq = new SearchBySpecialityReq();
        searchBySpecialityReq.setDoctorName(doctorname);
        // searchBySpecialityReq.setSort(exp);
        //searchBySpecialityReq.setSpecialityID(selectedSpecialityDAO.getSpeciality_code());
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).searchBySpeciality(searchBySpecialityReq, new RestCallback<SearchBySpecialityRes>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(SearchBySpecialityRes searchBySpecialityRes, Response response) {
                hideLoader();
                if(specialityResRowArrayList != null)
                    specialityResRowArrayList.clear();
                if(searchBySpecialityRes != null && searchBySpecialityRes.getSpecialityResRowArrayList()!= null)
                {
                    specialityResRowArrayList = searchBySpecialityRes.getSpecialityResRowArrayList();
                    for(SpecialityResRow specialityResRow : specialityResRowArrayList)
                    {
                            values.add(specialityResRow.getFullName());
                    }
                    //bookingAdapter.refreshAdapter(specialityResRowArrayList);
                }
                autoCompleteAdapter.refreshAdapter(values, specialitycode.keySet());
            }
        });
    }*/

    public  Hashtable<String,String> intitalisespecialitycode(){
        specialitycode=new Hashtable<String, String>();
        specialitycode.put("Acupuncture", "ACP");
        specialitycode.put("Anesthesiologist", "ANE");
        specialitycode.put("Ayurveda", "AYU");
        specialitycode.put("Cardiologist - Interventional", "CAI");
        specialitycode.put("Cardio Thoracic Surgeon", "CTS");
        specialitycode.put("Consultant Cardiologist", "CRD");
        specialitycode.put("Cosmetic Surgeon", "COS");
        specialitycode.put("Dentist", "DNT");
        specialitycode.put("Dermatologist", "DER");
        specialitycode.put("Diabetologist", "DBT");
        specialitycode.put("Ear Nose Throat Specialist", "ENT");
        specialitycode.put("Endocrinologist", "END");
        specialitycode.put("Gastroenterologist", "GAS");
        specialitycode.put("General & Laparoscopic Surgeon", "GLS");
        specialitycode.put("General Practitioner", "GEP");
        specialitycode.put("General Surgeon", "GNS");
        specialitycode.put("Geriatrician", "GTR");
        specialitycode.put("Gynaecologist", "GYN");
        specialitycode.put("Hematologist", "HEM");
        specialitycode.put("Hepatologist", "HEP");
        specialitycode.put("Homeopathy", "HYM");
        specialitycode.put("Immunologist", "IML");
        specialitycode.put("Infectious Diseases", "IFD");
        specialitycode.put("Internal Medicine", "IMD");
        specialitycode.put("Neonatologist", "NEG");
        specialitycode.put("Nephrologist", "NEP");
        specialitycode.put("Neurologist", "NEU");
        specialitycode.put("Neuro Surgeon", "NES");
        specialitycode.put("Obstetrician", "OBS");
        specialitycode.put("Occupational Therapist", "OCT");
        specialitycode.put("Oncologist", "ONC");
        specialitycode.put("Ophthalmologist", "OPT");
        specialitycode.put("Orthopedician", "ORT");
        specialitycode.put("Others", "OTH");
        specialitycode.put("Otolaryngologist", "OTO");
        specialitycode.put("Paediatrician", "PAE");
        specialitycode.put("Palliative Care", "PLC");
        specialitycode.put("Pathologist", "PAT");
        specialitycode.put("Pediatric Surgeon", "PES");
        specialitycode.put("Physiologist", "PHY");
        specialitycode.put("Physiotherapist", "PST");
        specialitycode.put("Plastic Surgeon", "PLS");
        specialitycode.put("Podiatrist", "PDT");
        specialitycode.put("Psychiatrist", "PCT");
        specialitycode.put("Psychologist", "PSY");
        specialitycode.put("Pulmonologist", "PML");
        specialitycode.put("Radiologist", "RAD");
        specialitycode.put("Rehabilitation Therapist", "RHT");
        specialitycode.put("Rheumatologist", "RHE");
        specialitycode.put("Sexual Health", "SHT");
        specialitycode.put("Siddha", "SID");
        specialitycode.put("Speech And Language Therapist", "SLT");
        specialitycode.put("Sports Medicine", "SMD");
        specialitycode.put("Unani", "UNI");
        specialitycode.put("Urologist", "URO");
        return specialitycode;
    }

    public void hideKeyBoard(View view) {
        View view1 = this.getCurrentFocus();
        if (view1 != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }
    }

    private void callGlobalSerachService(String doctorname)
    {
        // showLoader("");
        GlobalSearchRequest globalSearchRequest = new GlobalSearchRequest();
        globalSearchRequest.setLatitude(currentlat.toString());
        globalSearchRequest.setLongitude(currentlong.toString());
        globalSearchRequest.setRadius(30000);
        globalSearchRequest.setSearch_text(doctorname);

        // searchBySpecialityReq.setSort(exp);
        //searchBySpecialityReq.setSpecialityID(selectedSpecialityDAO.getSpeciality_code());
        RestClient.getAPI(Constants.PATIENT_CONSTANT_URL).globalSearch(globalSearchRequest, new RestCallback<GlobalSearchResponseDO>() {
            @Override
            public void failure(RestError restError) {
                hideLoader();
                ShowAlertDialog("Whoops!","Something went wrong. please try again.",R.drawable.worng);
            }

            @Override
            public void success(GlobalSearchResponseDO globalSearchResponseDO, Response response)
            {

                setProgressBarIndeterminateVisibility(false);

                System.out.println("VVVVVVVVVVVVVVVVVVVVVVV "+pbHeaderProgress.getVisibility());

                if (pbHeaderProgress.getVisibility() == View.VISIBLE){
                    pbHeaderProgress.setVisibility(View.GONE);
                }
                ivClose.setVisibility(View.VISIBLE);
                hideLoader();
                if(values1 != null)
                    values1.clear();
                if(globalSearchResponseDO != null && globalSearchResponseDO.getDoctors()!= null)
                {


                    //if (globalSearchResponseDO.getListSpecialities() != null && globalSearchResponseDO.getListSpecialities().size() > 0)
                        /*if (globalSearchResponseDO.getListSpecialities().size() > 0)
                        for(GlobalSpecialityDO specialityDO : globalSearchResponseDO.getListSpecialities())
                        {
                            try{
                                DoctorSpecialityDO do1 = new DoctorSpecialityDO();
                                do1.setCategory("Speciality");
                                do1.setName(specialityDO.getSpecialityName());
                                do1.setSpecialityID(specialityDO.getSpecialityID());
                                values1.add(do1);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }*/

                    /*if (globalSearchResponseDO.getListSymptoms() != null && globalSearchResponseDO.getListSymptoms().size() > 0)
                        for(GlobalSymptomsDO symptomsDO : globalSearchResponseDO.getListSymptoms())
                        {
                            try{
                                DoctorSpecialityDO do1 = new DoctorSpecialityDO();
                                do1.setCategory("Symptom");
                                do1.setName(symptomsDO.getSpecialityName());
                                do1.setSpecialityName(symptomsDO.getSpecialityName());
                                do1.setSpecialityID(symptomsDO.getSpecialityID());
                                do1.setSymptomName(symptomsDO.getSymptomName().replace("\t", ""));
                                values1.add(do1);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }*/

                   /* if (globalSearchResponseDO.getListServices() != null && globalSearchResponseDO.getListServices().size() > 0)

                        for(GlobalServicesDO servicesDO : globalSearchResponseDO.getListServices())
                        {
                            try{
                                DoctorSpecialityDO do1 = new DoctorSpecialityDO();
                                do1.setCategory("Service");
                                do1.setName(servicesDO.getServiceName());
                                do1.setSpecialityID(servicesDO.getDoctorIDs());
                                values1.add(do1);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }*/
                }

               /* if (globalSearchResponseDO.getHospitals() != null && globalSearchResponseDO.getHospitals().getHospitalsList().size() > 0)
                    for(HospitalDO doctorsDO : globalSearchResponseDO.getHospitals().getHospitalsList())
                    {
                        try{
                            DoctorSpecialityDO do1 = new DoctorSpecialityDO();
                            do1.setCategory("Clinic");
                            do1.setName(doctorsDO.getHospitalName());
                            do1.setHospitalid(doctorsDO.getHospitalID());
                            do1.setHospitalloacation(doctorsDO.getHospitalLocation());
                            values1.add(do1);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }*/

                System.out.println("ListDoctors values "+globalSearchResponseDO.getDoctors().getDoctorsList());

                System.out.println("ListDoctors values  size "+globalSearchResponseDO.getDoctors().getDoctorsList().size());

                if (globalSearchResponseDO.getDoctors().getDoctorsList() != null && globalSearchResponseDO.getDoctors().getDoctorsList().size() > 0)
                    for(GlobalSearchDoctorsDO doctorsDO : globalSearchResponseDO.getDoctors().getDoctorsList())
                    {
                        try{
                            DoctorSpecialityDO do1 = new DoctorSpecialityDO();
                            do1.setCategory("Doctor");
                            do1.setName(doctorsDO.getDoctorName());
                            do1.setSpecialityName(doctorsDO.getDoctorSpeciality().get(0));
                            values1.add(do1);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                if (values1.size() == 0){
                    tvNoResultFound.setVisibility(View.VISIBLE);
                    lvList.setVisibility(View.GONE);
                }else{
                    tvNoResultFound.setVisibility(View.GONE);
                }
                autoCompleteAdapter.refreshAdapter(values1/*, specialitycode.keySet()*/);

            }
        });
    }
}
