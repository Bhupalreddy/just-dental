package hCue.Hcue;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import hCue.Hcue.utils.ApplicationConstants;


/**
 * Created by shyamprasadg on 03/06/16.
 */
public class AddConditionActivity extends BaseActivity implements View.OnClickListener
{

    private ScrollView llAddCondition;
    private Button btnAdd;
    private TextInputLayout tvCondition, tvNotes;
    private EditText etCondition, etNotes;
    private RadioButton rbYes, rbNo;
    private boolean isHaving = false;

    @Override
    public void initialize() {


        llAddCondition = (ScrollView) getLayoutInflater().inflate(R.layout.add_condition, null, false);
        llBody.addView(llAddCondition, baseLayoutParams);
        flBottomBar.setVisibility(View.GONE);

        tvTopTitle.setVisibility(View.VISIBLE);
        llClose.setVisibility(View.VISIBLE);
        tvTopTitle.setText("Add Condition");
        llSave.setVisibility(View.GONE);
        llLeftMenu.setVisibility(View.GONE);
        ivSearch.setVisibility(View.INVISIBLE);
        initViewControls();
        bindControls();

    }

    public void initViewControls(){

        btnAdd                              = (Button) findViewById(R.id.btnAdd);
        tvCondition                         = (TextInputLayout) findViewById(R.id.tvCondition);
        tvNotes                             = (TextInputLayout) findViewById(R.id.tvNotes);
        etCondition                         = (EditText) findViewById(R.id.etCondition);
        etNotes                             = (EditText) findViewById(R.id.etNotes);
        rbYes                               = (RadioButton) findViewById(R.id.rbYes);
        rbNo                                = (RadioButton) findViewById(R.id.rbNo);

    }

    public void bindControls(){

        setSpecificTypeFace(llAddCondition, ApplicationConstants.WALSHEIM_MEDIUM);
        tvCondition.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        tvNotes.setTypeface(ApplicationConstants.WALSHEIM_MEDIUM);
        btnAdd.setOnClickListener(this);
        rbYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbYes.isChecked()){
                    rbNo.setChecked(false);
                    isHaving = true;
                }
            }
        });

        rbNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbNo.isChecked()){
                    rbYes.setChecked(false);
                    isHaving = false;
                }
            }
        });
        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishActivity();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnAdd:

                if (TextUtils.isEmpty(etCondition.getText().toString().trim())) {
                    ShowAlertDialog("Alert","Condition should not be empty.",R.drawable.alert);
                    return;
                } else{
                    hideKeyBoard(etCondition);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("condition",etCondition.getText().toString().trim());
                    returnIntent.putExtra("isHaving", isHaving);
                    returnIntent.putExtra("Notes", etNotes.getText().toString().trim());
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
                break;
        }
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        hideKeyBoard(null);
        finishActivity();
    }

    public void finishActivity(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
    }

}
